VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmServidor 
   Caption         =   "Servidor Multiplayer"
   ClientHeight    =   6270
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   6270
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton botEsperarJogs 
      Caption         =   "&Esperar por Jogadores"
      Enabled         =   0   'False
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   4920
      Width           =   4455
   End
   Begin VB.CommandButton botComecar 
      Caption         =   "&Come�ar o Jogo"
      Enabled         =   0   'False
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   5400
      Width           =   4455
   End
   Begin VB.CommandButton botVoltarMenu 
      Cancel          =   -1  'True
      Caption         =   "&Voltar ao menu principal"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   5880
      Width           =   4455
   End
   Begin VB.CommandButton botSelMapa 
      Caption         =   "Selecionar o &Mapa"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   4440
      Width           =   4455
   End
   Begin MSWinsockLib.Winsock Winsock 
      Left            =   4200
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      Protocol        =   1
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Jogadores conectados:"
      Height          =   195
      Left            =   120
      TabIndex        =   18
      Top             =   1560
      Width           =   1665
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   9
      Left            =   120
      TabIndex        =   17
      Top             =   3720
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   8
      Left            =   120
      TabIndex        =   16
      Top             =   3480
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   7
      Left            =   120
      TabIndex        =   15
      Top             =   3240
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   6
      Left            =   120
      TabIndex        =   14
      Top             =   3000
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   5
      Left            =   120
      TabIndex        =   13
      Top             =   2760
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   12
      Top             =   2520
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   11
      Top             =   2280
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   10
      Top             =   2040
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   1800
      Width           =   570
   End
   Begin VB.Label lblJogador 
      AutoSize        =   -1  'True
      Caption         =   "Jogador"
      Height          =   195
      Index           =   10
      Left            =   120
      TabIndex        =   8
      Top             =   3960
      Width           =   570
   End
   Begin VB.Label lblLocalPort 
      AutoSize        =   -1  'True
      Caption         =   "lblLocalPort"
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   825
   End
   Begin VB.Label lblRemotePort 
      AutoSize        =   -1  'True
      Caption         =   "lblRemotePort"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   990
   End
   Begin VB.Label lblRemoteHost 
      AutoSize        =   -1  'True
      Caption         =   "lblRemoteHost"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblLocalIP 
      AutoSize        =   -1  'True
      Caption         =   "lblLocalIP"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   690
   End
End
Attribute VB_Name = "frmServidor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Type typJogRede
    Conectado As Boolean
    IP As String
End Type
Private Enum enumEstatus
    EscolhendoOpcoes = 0
    EsperandoJogadores = 1
    Jogando = 2
End Enum
Dim pStatus As enumEstatus
Dim JRede(1 To M) As typJogRede

Private Sub Form_Load()
With Winsock
    .Protocol = sckUDPProtocol
    .RemoteHost = .LocalIP
    .RemotePort = cClientPorta
    .LocalPort = cServerPorta
    .Close
End With
AtualizaLabelsWinsock
AtualizaLabelsJogadores
Estatus = EscolhendoOpcoes
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If UnloadMode = vbFormControlMenu Then
    Inicio.Show
End If
End Sub

Sub AtualizaLabelsWinsock()
lblLocalIP.Caption = "Seu IP � " & Winsock.LocalIP
If Winsock.RemoteHostIP <> "" Then
    lblRemoteHost.Caption = "Enviando para " & Winsock.RemoteHostIP
Else
    lblRemoteHost.Caption = "Enviando para " & Winsock.RemoteHost
End If
lblRemotePort.Caption = "Enviando pela porta " & Winsock.RemotePort
lblLocalPort.Caption = "Ouvindo pela porta " & Winsock.LocalPort
End Sub

Sub AtualizaLabelsJogadores()
Dim i As Byte
For i = 1 To M
    If JRede(i).Conectado Then
        lblJogador(i).Caption = J(i).Nome
    Else
        lblJogador(i).Caption = "-"
    End If
Next
End Sub

Private Sub botSelMapa_Click()
On Error GoTo Erro
Mapa.Dialog.InitDir = DirDuelo + "mapas"
Mapa.Dialog.Filter = "Todos os arquivos|*.*|Mapas|*.map"
Mapa.Dialog.FilterIndex = 2
Mapa.Dialog.ShowOpen
If Mapa.Dialog.FileName <> "" Then
    If LCase(Mid(Mapa.Dialog.FileTitle, Len(Mapa.Dialog.FileTitle) - 2, 3)) <> "map" Then
        MsgMuda "Arquivo inv�lido."
    Else
        Arquivo$ = Mapa.Dialog.FileName
        If Not Carrega Then
            MsgMuda "Arquivo inv�lido.", , , , Me
        Else
            botSelMapa.Caption = "Selecionar o Mapa: " + Mapa.Dialog.FileTitle
        End If
    End If
End If

Exit Sub
Erro:
If Err.Number <> cdlCancel Then Err.Raise Err.Number
End Sub

Private Sub botEsperarJogs_Click()
If Estatus = EscolhendoOpcoes Then
    Winsock.Bind Winsock.LocalPort
    Estatus = EsperandoJogadores
Else
    Winsock.Close
    Estatus = EscolhendoOpcoes
End If
End Sub

Private Sub botVoltarMenu_Click()
Winsock.Close
Hide
ZeraJogadores
Inicio.Visible = True
End Sub

Sub MandaDado(strDado As String, strIP As String)
Winsock.RemoteHost = strIP
Winsock.SendData strDado
End Sub

Private Sub Winsock_DataArrival(ByVal bytesTotal As Long)
Dim strData As String
Dim strParte() As String
Dim strCodigo As String
Dim strIP As String, strNome As String
Dim resp As Boolean

Winsock.GetData strData
strParte = Split(strData, "&")
strCodigo = strParte(0)
Select Case strCodigo
    Case cConecta
        strIP = strParte(1)
        strNome = strParte(2)
        resp = ConectaJogador(strIP, strNome)
        MandaDado cConectado, strIP
    Case cDesconecta
        strIP = strParte(1)
        strNome = strParte(2)
        resp = DesConectaJogador(strIP, strNome)
        MandaDado cDesconectado, strIP
End Select

End Sub

Function ConectaJogador(strIP As String, strNome As String) As Boolean
Dim i As Byte

i = 1
Do
    If Not JRede(i).Conectado Then
        Exit Do
    'elseif jrede(i)
    End If
    
    i = i + 1
Loop While i <= M

If i <= M Then
    JRede(i).Conectado = True
    JRede(i).IP = strIP
    J(i).Nome = strNome
    ConectaJogador = True
Else 'overflow de jogadores
    ConectaJogador = False
End If

AtualizaLabelsJogadores
End Function

Function DesConectaJogador(strIP As String, strNome As String) As Boolean
Dim i As Byte

i = 1
Do
    If JRede(i).IP = strIP And J(i).Nome = strNome Then Exit Do
    i = 1 + 1
Loop While i <= M

If i <= M Then
    JRede(i).Conectado = False
    JRede(i).IP = ""
    J(i).Nome = "Jog " & a & vbNullChar
    DesConectaJogador = True
Else 'overflow de jogadores
    DesConectaJogador = False
End If

AtualizaLabelsJogadores
End Function

Private Property Get Estatus() As enumEstatus
Estatus = pStatus
End Property

Private Property Let Estatus(ByVal vNewValue As enumEstatus)
pStatus = vNewValue
Select Case Estatus
    Case EscolhendoOpcoes
        botSelMapa.Enabled = True
        botEsperarJogs.Enabled = True
        botEsperarJogs.Caption = "Esperar Jogadores"
        botComecar.Enabled = False
    Case EsperandoJogadores
        botSelMapa.Enabled = False
        botEsperarJogs.Enabled = True
        botEsperarJogs.Caption = "Parar de Esperar"
        botComecar.Enabled = False
    Case Jogando
        botSelMapa.Enabled = False
        botEsperarJogs.Enabled = False
        botComecar.Enabled = False
End Select
End Property
