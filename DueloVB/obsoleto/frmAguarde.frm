VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmAguarde 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Duelo"
   ClientHeight    =   1680
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6375
   Icon            =   "frmAguarde.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   1680
   ScaleWidth      =   6375
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton butCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   2280
      TabIndex        =   1
      Top             =   1200
      Width           =   1695
   End
   Begin MSComctlLib.ProgressBar Bar 
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label lblLabel 
      Alignment       =   2  'Center
      Caption         =   "lblLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   120
      Width           =   5895
   End
End
Attribute VB_Name = "frmAguarde"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Paint()
'AlwaysOnTop Me, True
End Sub

Private Sub butCancelar_Click()
Avacalhar
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Cancel = Not Avacalhar
End Sub

Public Function Avacalhar() As Boolean
If MsgBox("Isso ir� acabar com o jogo!" & vbCrLf & "Tem certeza que quer fazer isso?", vbOKCancel) = vbOK Then
    Avacalhar = True
    Fim
Else
    Avacalhar = False
End If
End Function

Private Sub Form_Resize()
If Me.WindowState = vbMinimized Then
    frmChat.Esconde
End If
End Sub
