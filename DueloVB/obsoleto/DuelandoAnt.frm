VERSION 5.00
Begin VB.Form Duelando 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Duelo"
   ClientHeight    =   7875
   ClientLeft      =   75
   ClientTop       =   630
   ClientWidth     =   3105
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "DuelandoAnt.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   525
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   207
   Begin VB.CommandButton BombaExpl 
      Caption         =   "Explodir"
      Height          =   255
      Left            =   1200
      TabIndex        =   67
      Top             =   1800
      Width           =   855
   End
   Begin VB.CommandButton Medkits 
      Caption         =   "Medkits: n"
      Height          =   255
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   1440
      Width           =   975
   End
   Begin VB.CommandButton Bomba 
      Caption         =   "Bombas: n"
      Height          =   255
      Left            =   1080
      TabIndex        =   10
      Top             =   1440
      Width           =   975
   End
   Begin VB.CommandButton Rel�mpago 
      Caption         =   "Rel�mpago"
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   66
      Top             =   3720
      Width           =   855
   End
   Begin VB.CommandButton AnimarMortos 
      Caption         =   "Animar Mortos"
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   65
      Top             =   3720
      Width           =   1095
   End
   Begin VB.CommandButton Rezar 
      Caption         =   "Prece"
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      TabIndex        =   64
      Top             =   4080
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Cura 
      Caption         =   "Cura"
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2040
      TabIndex        =   63
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton DescerMoto 
      Caption         =   " Descer da Moto"
      Height          =   495
      Left            =   1080
      TabIndex        =   61
      Top             =   2160
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton TrocarCorpo 
      Caption         =   " Trocar de       Corpo"
      Height          =   495
      Left            =   2040
      TabIndex        =   59
      Top             =   2520
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton RegT1000 
      Caption         =   "Regenera��o"
      Height          =   255
      Left            =   1800
      TabIndex        =   58
      Top             =   2760
      Width           =   1215
   End
   Begin VB.PictureBox ScannerQ 
      BackColor       =   &H0000C000&
      Height          =   255
      Left            =   2760
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   57
      Top             =   6120
      Width           =   255
   End
   Begin VB.CommandButton PararTempo 
      Caption         =   "Parar Tempo"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   56
      Top             =   3720
      Width           =   1095
   End
   Begin VB.PictureBox Sensor 
      BackColor       =   &H0000C000&
      Height          =   255
      Index           =   6
      Left            =   1680
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   53
      Top             =   7080
      Width           =   255
   End
   Begin VB.PictureBox Sensor 
      BackColor       =   &H0000C000&
      Height          =   255
      Index           =   4
      Left            =   1680
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   52
      Top             =   6600
      Width           =   255
   End
   Begin VB.PictureBox Sensor 
      BackColor       =   &H0000C000&
      Height          =   255
      Index           =   3
      Left            =   120
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   51
      Top             =   7080
      Width           =   255
   End
   Begin VB.PictureBox Sensor 
      BackColor       =   &H0000C000&
      Height          =   255
      Index           =   2
      Left            =   120
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   50
      Top             =   6840
      Width           =   255
   End
   Begin VB.PictureBox Sensor 
      BackColor       =   &H0000C000&
      Height          =   255
      Index           =   5
      Left            =   1680
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   49
      Top             =   6840
      Width           =   255
   End
   Begin VB.PictureBox Sensor 
      BackColor       =   &H0000C000&
      Height          =   255
      Index           =   1
      Left            =   120
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   13
      TabIndex        =   48
      Top             =   6600
      Width           =   255
   End
   Begin VB.CommandButton Centrar 
      Caption         =   "&Centra"
      Height          =   375
      Left            =   120
      TabIndex        =   47
      Top             =   7440
      Width           =   1095
   End
   Begin VB.CommandButton DesarmarMinas 
      Caption         =   "  Desarmar        Minas"
      Height          =   495
      Left            =   2040
      TabIndex        =   46
      Top             =   3120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton Fuma�a 
      Caption         =   "Bombas de Fuma�a: n"
      Height          =   495
      Left            =   2040
      TabIndex        =   45
      Top             =   2040
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton TiroRaj 
      Caption         =   "&Rajada"
      BeginProperty Font 
         Name            =   "System"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   5760
      Width           =   975
   End
   Begin VB.CommandButton TiroIns 
      Caption         =   "&Instan."
      BeginProperty Font 
         Name            =   "System"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1080
      TabIndex        =   23
      Top             =   5760
      Width           =   975
   End
   Begin VB.CommandButton TiroMir 
      Caption         =   "&Mirado"
      BeginProperty Font 
         Name            =   "System"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2040
      TabIndex        =   22
      Top             =   5760
      Width           =   975
   End
   Begin VB.CommandButton Pegar 
      Caption         =   "Pegar/Destruir"
      Height          =   375
      Left            =   600
      TabIndex        =   21
      Top             =   6120
      Width           =   1215
   End
   Begin VB.CommandButton PassaVez 
      Caption         =   "&Passar a Vez"
      Default         =   -1  'True
      Height          =   375
      Left            =   1320
      TabIndex        =   20
      Top             =   7440
      Width           =   1695
   End
   Begin VB.CommandButton TrocaArma 
      Caption         =   "&Trocar de Arma"
      Height          =   255
      Left            =   1440
      TabIndex        =   19
      Top             =   5160
      Width           =   1455
   End
   Begin VB.CommandButton AtirarGranada 
      Caption         =   "   Atirar Granada"
      Height          =   495
      Left            =   120
      TabIndex        =   18
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Corpos 
      Caption         =   "Corpos: n"
      Height          =   255
      Left            =   2040
      TabIndex        =   17
      Top             =   1800
      Width           =   975
   End
   Begin VB.CommandButton NMinas 
      Caption         =   "Minas: n"
      Height          =   255
      Left            =   2040
      TabIndex        =   15
      Top             =   1440
      Width           =   975
   End
   Begin VB.CommandButton Granadas 
      Caption         =   "Granadas: n"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1800
      Width           =   1095
   End
   Begin VB.PictureBox Embaixo 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      ScaleHeight     =   23
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   26
      TabIndex        =   13
      Top             =   6120
      Width           =   420
   End
   Begin VB.CommandButton CamuflBot�o 
      Caption         =   "On/Off"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   3240
      Width           =   615
   End
   Begin VB.CommandButton SniktBot�o 
      Caption         =   "On/Off"
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   4320
      Width           =   735
   End
   Begin VB.CommandButton Teletransporte 
      Caption         =   "Teletran."
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2040
      TabIndex        =   9
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton ControlePsi�nico 
      Caption         =   "Controle-Psi"
      Height          =   255
      Left            =   1920
      TabIndex        =   8
      Top             =   2160
      Width           =   1095
   End
   Begin VB.CommandButton Rastejar 
      Caption         =   "Rastejar"
      Height          =   255
      Left            =   1560
      TabIndex        =   7
      Top             =   1080
      Width           =   1455
   End
   Begin VB.CommandButton AtirarParasita 
      Caption         =   "  Atirar Parasita"
      Height          =   495
      Left            =   120
      TabIndex        =   6
      Top             =   2160
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton SobeAndar 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   960
      TabIndex        =   5
      Top             =   1080
      Width           =   255
   End
   Begin VB.CommandButton DesceAndar 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   4
      Top             =   1080
      Width           =   255
   End
   Begin VB.CommandButton BuracoZerg 
      Caption         =   "Enterrar-se"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   4440
      Width           =   1335
   End
   Begin VB.CommandButton AutoDestrui��o 
      Caption         =   "AUTO-DESTRUI��O"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   4200
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton Identifica��o 
      Caption         =   "Identificar"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   1
      Top             =   3720
      Width           =   855
   End
   Begin VB.CommandButton Holografia 
      Caption         =   "Holografias: n"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   2760
      Width           =   1455
   End
   Begin VB.CommandButton Imitar 
      Caption         =   "Imitar Forma"
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   60
      Top             =   4080
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label ManaLabel 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Mana: 100"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   405
      Left            =   120
      TabIndex        =   25
      Top             =   4440
      Width           =   2895
   End
   Begin VB.Label ScannerLabel 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Detector"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   1920
      TabIndex        =   62
      Top             =   6120
      Visible         =   0   'False
      Width           =   690
   End
   Begin VB.Label SensorNome 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Lun�tico"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   6
      Left            =   2280
      TabIndex        =   55
      Top             =   7080
      Visible         =   0   'False
      Width           =   585
   End
   Begin VB.Label SensorDirec 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   2040
      TabIndex        =   54
      Top             =   7080
      Width           =   255
   End
   Begin VB.Label Jogador 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Jogador"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   44
      Top             =   0
      Width           =   3075
   End
   Begin VB.Label SensorDirec 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   2040
      TabIndex        =   43
      Top             =   6600
      Width           =   255
   End
   Begin VB.Label SensorDirec 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   480
      TabIndex        =   42
      Top             =   7080
      Width           =   255
   End
   Begin VB.Label SensorDirec 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   480
      TabIndex        =   41
      Top             =   6840
      Width           =   255
   End
   Begin VB.Label SensorDirec 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   2040
      TabIndex        =   40
      Top             =   6840
      Width           =   255
   End
   Begin VB.Label SensorDirec 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   8.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Index           =   1
      Left            =   480
      TabIndex        =   39
      Top             =   6600
      Width           =   255
   End
   Begin VB.Label AtirarLabel 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Atirar"
      BeginProperty Font 
         Name            =   "System"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   0
      TabIndex        =   38
      Top             =   5520
      Width           =   3075
   End
   Begin VB.Label SensorNome 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Jairo"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   5
      Left            =   2280
      TabIndex        =   37
      Top             =   6840
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.Label SensorNome 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "F�bio"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   4
      Left            =   2280
      TabIndex        =   36
      Top             =   6600
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Label SensorNome 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Josu�"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   3
      Left            =   720
      TabIndex        =   35
      Top             =   7080
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.Label SensorNome 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Juarez"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   720
      TabIndex        =   34
      Top             =   6840
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label SensorNome 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Jo�o Paulo"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   720
      TabIndex        =   33
      Top             =   6600
      Width           =   795
   End
   Begin VB.Label Muni��o 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Muni��o:"
      Height          =   195
      Left            =   120
      TabIndex        =   32
      Top             =   5160
      Width           =   660
   End
   Begin VB.Label AndarLabel 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "1� Andar"
      Height          =   255
      Left            =   120
      TabIndex        =   31
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label ArmaNaM�o 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Nome da Arma"
      BeginProperty Font 
         Name            =   "Impact"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   0
      TabIndex        =   30
      Top             =   4800
      Width           =   3075
   End
   Begin VB.Shape TempoBorda 
      BackColor       =   &H00008000&
      BorderColor     =   &H00000000&
      BorderStyle     =   6  'Inside Solid
      Height          =   255
      Left            =   1440
      Top             =   720
      Width           =   1575
   End
   Begin VB.Shape TempoBarra 
      BackColor       =   &H00008000&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderStyle     =   6  'Inside Solid
      Height          =   255
      Left            =   1440
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label TempoLabel 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Tempo: "
      Height          =   195
      Left            =   120
      TabIndex        =   29
      Top             =   720
      Width           =   585
   End
   Begin VB.Shape ResBarra 
      BackColor       =   &H00008000&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderStyle     =   6  'Inside Solid
      Height          =   255
      Left            =   1440
      Top             =   360
      Width           =   1575
   End
   Begin VB.Shape ResBorda 
      BorderColor     =   &H00000000&
      Height          =   255
      Left            =   1440
      Top             =   360
      Width           =   1575
   End
   Begin VB.Label ResLabel 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Resist�ncia: "
      Height          =   195
      Left            =   120
      TabIndex        =   28
      Top             =   360
      Width           =   915
   End
   Begin VB.Label CamuflLabel 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Camuflagem: OFF"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   27
      Top             =   3240
      Width           =   1155
   End
   Begin VB.Label SniktLabel 
      BackStyle       =   0  'Transparent
      Caption         =   "Snikt: ON"
      Height          =   255
      Left            =   960
      TabIndex        =   26
      Top             =   4440
      Width           =   855
   End
   Begin VB.Menu JogoMenu 
      Caption         =   "&Jogo"
      Begin VB.Menu mnuSalvarPartida 
         Caption         =   "&Salvar Partida"
      End
      Begin VB.Menu Empatar 
         Caption         =   "&Empatar"
      End
      Begin VB.Menu FimJogo 
         Caption         =   "&Finalizar"
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu menOpcoes0 
      Caption         =   "&Op��es"
      Begin VB.Menu menOpcoes1 
         Caption         =   "&Op��es"
         Shortcut        =   ^O
      End
   End
   Begin VB.Menu HelpMenu 
      Caption         =   "&Ajuda"
      Begin VB.Menu HelpPodMenu 
         Caption         =   "&Poderes e Armas"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "Duelando"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
DefInt A-Z

Private Sub Form_Load()
Dim a
'Medkits.Caption = ""
'Medkits.Picture = Mapa.ObjetoH(15)
'Medkits.Height = Mapa.ObjetoH(15).Height
'Medkits.Width = Mapa.ObjetoH(15).Width

J(vz).RepXIni = J(vz).X
J(vz).RepYIni = J(vz).Y
J(vz).RepZIni = J(vz).Z
J(vz).RepVisaoIni = J(vz).Vis�o

ContSons = 0

Andar = J(vz).Z

Call MsgMuda("Venha, " + J(vz).Nome, , , False)

Call AtualizaFog
Call ReDesenha
Call Cursor(vz): Mapa.Borda.Refresh
J(vz).RepCursorIni = J(vz).Cur
Call ReEscreve
If J(vz).Poder = pAgente_do_Matrix Then
    For a = M + 1 To NMons + M
        'if � do time dele then
        JaVi(a) = 1
    Next
End If
Call TestaEncontro
Call Centra(Mapa, J(vz).X, J(vz).Y)
End Sub

Sub SorteiaOrdem()
Dim nomesor$(M), podersor$(M), corsor(M) As Byte, timesor(M) As Byte
Dim n, a, sor
    For a = 1 To NJogs
        nomesor$(a) = J(a).Nome
        J(a).Nome = ""
        podersor$(a) = J(a).Poder
        J(a).Poder = 0
        corsor(a) = J(a).Cor
        J(a).Cor = 255
        timesor(a) = J(a).Time
        J(a).Time = 0
    Next
    n = 0
    Do
        sor = Int(Rnd * NJogs) + 1
        If nomesor$(sor) <> "" And podersor$(sor) <> "" And corsor(sor) <> 255 And timesor(sor) <> 0 Then
            n = n + 1
            J(n).Nome = nomesor$(sor)
            nomesor$(sor) = ""
            J(n).Poder = podersor$(sor)
            podersor$(sor) = ""
            J(n).Cor = corsor(sor)
            corsor(sor) = 255
            J(n).Time = timesor(sor)
            timesor(sor) = 0
        End If
        If n = NJogs Then Exit Do
    Loop
End Sub

Sub EmbaralhaPoderes()
Dim podersor$(M)
Dim a, n, sor
For a = 1 To NJogs
    podersor$(a) = J(a).Poder
    J(a).Poder = ""
Next
n = 0
Do
    sor = Int(Rnd * NJogs) + 1
    If podersor$(sor) <> "" Then
        n = n + 1
        J(n).Poder = podersor$(sor)
        podersor$(sor) = ""
    End If
    If n = NJogs Then Exit Do
Loop

End Sub

Sub Posiciona()
Dim a As Byte, b As Byte, p As Byte, cont As Integer
Dim d!
'DMin = Sqr(xM * yM * (AndMax + 1)) / (NJogs + (NMons / 2))
DMin = Sqr(xM * yM * (AndMax + 1)) / (NJogs + (NMons / 2))
cont = 0
For a = 1 To NJogs
    Do
        cont = cont + 1
        If cont > 10000 Then
            DMin = DMin * 0.9
            cont = 0
        End If
        J(a).X = Int(Rnd * xM) + 1: J(a).Y = Int(Rnd * yM) + 1
        J(a).Z = CInt(Rnd * (AndMax + 1) - 0.5)
        J(a).Vis�o = Int(Rnd * 12)
        p = 0
        For b = 1 To NJogs
            'D! = Sqr((X(b) - J(a).X) ^ 2 + (Y(b) - J(a).Y) ^ 2 + ((CInt(Z(b)) - J(a).Z) * AlturaAndar) ^ 2)
            d! = Sqr(Distancia(J(b).X, J(b).Y, J(a).X, J(a).Y) + ((CInt(J(a).Z) - CInt(J(b).Z)) * AlturaAndar) ^ 2)
            If d! < DMin And b <> a Then p = 1
        Next
        For b = 1 + M To NMons + M
            d! = Sqr((J(b).X - J(a).X) ^ 2 + (J(b).Y - J(a).Y) ^ 2 + ((CInt(J(b).Z) - J(a).Z) * AlturaAndar) ^ 2)
            If d! < DMin / 2 Then p = 1
        Next
    Loop While Conte�do(J(a).X, J(a).Y, J(a).Z) <> 0 Or p = 1
Next
End Sub

Sub PoderAciona()
Dim a As Byte, b As Byte, ChanceTot As Integer
Dim peso(NPoderes) As Single
Dim i, RandomNumber As Single
Dim c%, arq$, somapesos, n, sominha
'quando criar poderes, n�o esque�a:
'-aumentar o npoderes
'-inclu�-lo nas estat�sticas e nas prefer�ncias
'-inclu�-lo no help
For i = 1 To 5
    ChanceTot = ChanceTot + Config.ChanceClasse(i)
Next
'For A = 1 To NJogs
a = 1
Do
    If J(a).Poder = pSorteado Then
        ' sorteia classe do poder
        RandomNumber = Rnd
        Select Case RandomNumber
            Case Is < Config.ChanceClasse(1) / ChanceTot
                c% = 1 'A
            Case Is < (Config.ChanceClasse(1) + Config.ChanceClasse(2)) / ChanceTot
                c% = 2 'B
            Case Is < (Config.ChanceClasse(1) + Config.ChanceClasse(2) + Config.ChanceClasse(3)) / ChanceTot
                c% = 3 'C
            Case Is < (Config.ChanceClasse(1) + Config.ChanceClasse(2) + Config.ChanceClasse(3) + Config.ChanceClasse(4)) / ChanceTot
                c% = 4 'D
            Case Else
                c% = 5 'E
        End Select

        'Select Case RandomNumber
        '    Case Is < 0.07: c% = 1 'A-7%
        '    Case Is < 0.28: c% = 2 'B-21%
        '    Case Is < 0.56: c% = 3 'C-28%
        '    Case Is < 0.86: c% = 4 'D-30%
        '    Case Else:      c% = 5 'E-14%
        'End Select
        
        ' carrega prefer�ncias do arquivo
        'Select Case J(a).Nome
        '    Case "Jairo": arq$ = "jairo.pre"
        '    Case "Jo�o Paulo": arq$ = "fau.pre"
        '    Case "Juarez": arq$ = "juarez.pre"
        '    Case "Josu�": arq$ = "josue.pre"
        '    Case "F�bio": arq$ = "fabio.pre"
        '    Case Else: arq$ = ""
        'End Select
        arq$ = ""
        For i = 1 To Estat.lstJogadores.ListItems.Count
            'If J(A).Nome = Estat.JogadorNome(i) Then
            If J(a).Nome = Estat.lstJogadores.ListItems(i).Text Then
                'arq$ = Estat.comArqPref(i).Caption
                arq$ = Estat.lstJogadores.ListItems(i).SubItems(8)
                Exit For
            End If
        Next
        If arq$ <> "" And arq$ <> "Nenhum" Then
            arq$ = DirDuelo + arq$
            Open arq$ For Input As #1
            Line Input #1, cod$
            Close #1
        End If
        somapesos = 0
        For n = 1 To NPoderes
            If Classe(n) <> c% Then
                peso(n) = 0
            ElseIf arq$ <> "" Then
                Select Case Mid(cod$, n, 1)
                    Case "P": peso(n) = 2 '3
                    Case "O": peso(n) = 1 / 2
                    Case "I": peso(n) = 1
                End Select
            Else
                peso(n) = 1
            End If
            somapesos = somapesos + peso(n)
        Next
        
        'sorteia poder
        RandomNumber = Rnd
        n = 0: sominha = 0
        Do
            n = n + 1
            sominha = sominha + peso(n)
        Loop While sominha <= somapesos * RandomNumber
        J(a).Poder = PoderStrByt(Pref.PoderNome(n).Caption)
        For b = 1 To a - 1
            If J(b).Poder = J(a).Poder And J(b).Poder = pCapangas Then
                J(a).Poder = pSorteado: Exit For
            End If
        Next
    End If
    
    With J(a)
        ' define poderes do poder
        Select Case .Poder
        Case pPM  ' tudo normal
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 1: .For�a = 15
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pAtirador_de_Elite
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 2.5: .For�a = 15
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1.5: .OlhoNatural = 1.5
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pVelocista
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 8.75:  .Mira = 1: .For�a = 15
            .Camufl = 0.9: .CamuflNatural = 0.9
            .Olho = 0.9: .OlhoNatural = 0.9
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pCasca_Grossa
            .Ht = 12: .HtMax = 12: .Reg = 1.05: .RegNatural = 1.05
            .TMax = 6.25: .Mira = 1: .For�a = 18: .Blindagem = 0.25
            .Camufl = 0.95: .CamuflNatural = 0.95
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pTroll
            .Ht = 8: .HtMax = 8: .Reg = 1.5: .RegNatural = 1.5
            .TMax = 6.25:  .Mira = 1: .For�a = 15
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pBoina_Verde
            .Ht = 8: .HtMax = 8: .Reg = 1.1: .RegNatural = 1.1
            .TMax = 6.75:  .Mira = 1.2: .For�a = 20
            .Camufl = 1.15: .CamuflNatural = 1.15
            .Olho = 1.15: .OlhoNatural = 1.15
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pPredador
            .Ht = 14: .HtMax = 14: .Reg = 1: .RegNatural = 1
            .TMax = 8.75:  .Mira = 1.4 ': .For�a = 15
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 2.5: .OlhoNatural = 2.5
            .Detec = True: .Olfato = 4
            .Mun(7) = 3: .ArmaP(7) = True        'Discos e tri-laser
            .ArmaM�o = 8: .Mun(8) = MunA(8): .ArmaP(8) = True
        Case pWolverine
            .Ht = 7: .HtMax = 7: .Reg = 1.15: .RegNatural = 1.15
            .TMax = 7.5:  .Mira = 1: .For�a = 20
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1.15: .OlhoNatural = 1.15
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
            .Snikt = False: .Olfato = 16
        Case pVoador
            .Ht = 8: .HtMax = 8: .Reg = 1: .RegNatural = 1
            .TMax = 7.5:  .Mira = 1: .For�a = 18
            .Camufl = 0.8: .CamuflNatural = 0.8
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pFantasma
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 6.75:  .Mira = 1: .For�a = 12
            .Camufl = 1.8: .CamuflNatural = 1.8
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pTarrasque
            .Ht = 40: .HtMax = 60: .Reg = 1: .RegNatural = 1
            .TMax = 5: .Blindagem = 0.5 '.For�a = 150 ':  .Mira = 1
            .Camufl = 0.3: .CamuflNatural = 0.3
            .Olho = 1.6: .OlhoNatural = 1.6
            .Olfato = 8
        Case pRobocop
            .Ht = 10: .HtMax = 10: .Reg = 1: .RegNatural = 1
            .TMax = 5.5: .Mira = 1: .Blindagem = 2.75 ': .For�a = 15
            .Camufl = 0.5: .CamuflNatural = 0.5
            .Olho = 0.9: .OlhoNatural = 0.9
            .Detec = True
            .ArmaM�o = 12: .Mun(12) = MunA(12): .ArmaP(12) = True: .ArmaP(26) = True
        Case pHomem_do_Raio_X
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 1: .For�a = 15
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pBalacau
            .Ht = 10: .HtMax = 10: .Reg = 1: .RegNatural = 1
            .TMax = 12: '.For�a = 15 ':  .Mira = 1
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 0.5: .OlhoNatural = 0.5: .Olfato = 12
        Case pMosca
            .Ht = 12: .HtMax = 12: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 0.8: .For�a = 30
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pDragao
            .Ht = 20: .HtMax = 20: .Reg = 1: .RegNatural = 1
            .TMax = 8:  .Mira = 1: .Blindagem = 0.5 ': .For�a = 15
            .Camufl = 0.5: .CamuflNatural = 0.5
            .Olho = 2: .OlhoNatural = 2: .Olfato = 16
            .ArmaM�o = 14: .Mun(14) = MunA(14): .ArmaP(14) = True
        Case pMago     ' ex-teletransportador
            .Ht = 5: .HtMax = 5: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 0.5: .For�a = 12
            .Camufl = 0.9: .CamuflNatural = 0.9
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
            .ArmaP(22) = True  'Bola de Fogo
            .ArmaP(26) = True
            .ManaF� = 100
            .Invis = -1
        Case pEthereal
            .Ht = 6: .HtMax = 6: .Reg = 1: .RegNatural = 1
            .TMax = 7: .Mira = 0.6: .For�a = 12
            .Camufl = 1.3: .CamuflNatural = 1.3
            .Olho = 4: .OlhoNatural = 4
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pJason
            .Ht = 15: .HtMax = 15: .Reg = 1: .RegNatural = 1
            .TMax = 12: '.For�a = 15 ':  .Mira = 1
            .Camufl = 0.8: .CamuflNatural = 0.8
            .Olho = 1.5: .OlhoNatural = 1.5: .Olfato = 4
            .ArmaM�o = 17: .ArmaP(17) = True
        Case pAlien 'forma de 'aranha'
            .Ht = 3: .HtMax = 3: .Reg = 1: .RegNatural = 1
            .TMax = 8: '.For�a = 15 ':  .Mira = 1
            .Camufl = 2: .CamuflNatural = 2
            .Olho = 0.8: .OlhoNatural = 0.8: .Olfato = 4
            .Raio = 10
        Case pDemolidor
            .Ht = 9: .HtMax = 9: .Reg = 1: .RegNatural = 1
            .Camufl = 0.9: .CamuflNatural = 0.9
            .Olho = 0: .OlhoNatural = 0
            .TMax = 7:  .Mira = 1.2: .For�a = 18
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pMagrao
            .Ht = 5.5: .HtMax = 5.5: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 1: .For�a = 12
            .Camufl = 2: .CamuflNatural = 2
            .Olho = 1: .OlhoNatural = 1
            .Raio = 15
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pZerg
            .Ht = 7: .HtMax = 7: .Reg = 1.1: .RegNatural = 1.1
            .TMax = 7: .Mira = 1: .For�a = 20
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 18: .Mun(18) = MunA(18): .ArmaP(18) = True
            .Parasitas = True
        Case pTanque
            .Ht = 15: .HtMax = 15: .Reg = 1: .RegNatural = 1
            .TMax = 8: .Mira = 1: .Blindagem = 5 ': .For�a = 15
            .Camufl = 0.3: .CamuflNatural = 0.3
            .Olho = 0.8: .OlhoNatural = 0.8
            .Detec = True
            .ArmaM�o = 21: .Canh�o = 21
            .Mun(19) = 900: .ArmaP(19) = True   'Metralhadora
            .Mun(20) = MunA(20): .ArmaP(20) = True   'Canh�o HE
            .Mun(21) = MunA(21): .ArmaP(21) = True   'Canh�o AP
        Case pSabata
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 5: .For�a = 16
            .Camufl = 0.9: .CamuflNatural = 0.9
            .Olho = 1.5: .OlhoNatural = 1.5
            .ArmaM�o = 1: .Mun(1) = 6 * MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pBlindado
            .Ht = 8: .HtMax = 8: .Reg = 1: .RegNatural = 1
            .TMax = 6: .Mira = 1: .For�a = 18: .Blindagem = 3
            .Camufl = 0.9: .CamuflNatural = 0.9
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pJedi
            .Ht = 8: .HtMax = 8: .Reg = 1: .RegNatural = 1
            .TMax = 8: .Mira = 1.5: .For�a = 20
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 2: .OlhoNatural = 2
            .ArmaM�o = 23: .ArmaP(23) = True
        Case pNinja
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 7.5: .Mira = 1: .For�a = 18
            .Camufl = 1.8: .CamuflNatural = 1.8
            .Olho = 1.2: .OlhoNatural = 1.2
            .ArmaM�o = 24: .Mun(24) = MunA(24): .ArmaP(24) = True   'Estrelinhas
            .ArmaP(25) = True  'Katana
            .Fuma� = 5 ': Raio(a) = 40
        Case pHorla
            .Ht = 6: .HtMax = 6: .Reg = 1: .RegNatural = 1
            .TMax = 6.75: .Mira = 0.5: .For�a = 10
            .Camufl = 15: .CamuflNatural = 15
            .Olho = 0.9: .OlhoNatural = 0.9
            .ArmaM�o = 26: .ArmaP(26) = True  '.ArmaM�o = 1: .ArmaP( 1) = True
        Case pCapangas
            If NJogs = M Then
                .Poder = pSorteado: a = a - 1
            Else
                .Ht = 6: .HtMax = 6: .Reg = 1: .RegNatural = 1
                .TMax = 6: .Mira = 0.8: .For�a = 14
                .Camufl = 0.85: .CamuflNatural = 0.85
                .Olho = 0.9: .OlhoNatural = 0.9
                .ArmaM�o = 3: .Mun(3) = MunA(3): .ArmaP(3) = True: .ArmaP(26) = True
                .HtAnt = .Ht
                NJogs = NJogs + 1
                b = M 'shift
                Do
                    J(b) = J(b - 1)
                    J(b).Imit = b
                    J(b).Time = b
                    b = b - 1
                Loop Until b < a + 2
                J(a + 1) = J(a)
                J(a + 1).Imit = a + 1
                J(a + 1).Time = a 'esta linha est� certa, n�o tem '+ 1'
                a = a + 1
            End If
        Case pDinamitador
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 6: .Mira = 0.9: .For�a = 20
            .Camufl = 0.9: .CamuflNatural = 0.9
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 26: .ArmaP(26) = True
            .Gran = 12: .Minas = 6: .Bombas = 3
        Case pT_1000
            .Ht = 15: .HtMax = 15: .Reg = 1: .RegNatural = 1: .Blindagem = 1.5
            .TMax = 9: .Mira = 1: .For�a = 30
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1.2: .OlhoNatural = 1.2
        Case pAgente_do_Matrix
            .Ht = 9: .HtMax = 9: .Reg = 1: .RegNatural = 1
            .TMax = 8:  .Mira = 1.5: .For�a = 20
            .Camufl = 1: .CamuflNatural = 1
            .Olho = 1.25: .OlhoNatural = 1.25
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        Case pClerigo_Maligno
            .Ht = 6: .HtMax = 6: .Reg = 1: .RegNatural = 1
            .TMax = 6.25: .Mira = 0.8: .For�a = 14
            .Camufl = 0.9: .CamuflNatural = 0.9
            .Olho = 1: .OlhoNatural = 1
            .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
            .ManaF� = 100
        Case Else: a = a - 1
        End Select
        .HtAnt = .Ht
    End With
'Next
    a = a + 1
Loop Until a > NJogs
End Sub

Sub MonstroAciona()
Dim a%, b%
For a% = 1 To NMons
    b% = a% + M
    With J(b%)
        Select Case TipoMons(b%)
        Case 1
            .Nome = "Pistoleiro"
            .Ht = 5: .HtMax = 5: .Reg = 1: .RegNatural = 1
            .TMax = 4.5: .Mira = 1
            .Olho = 1: .Camufl = 1
            .ArmaM�o = 1 'Revolver
        Case 2
            .Nome = "Agente"
            .Ht = 6: .HtMax = 6: .Reg = 1: .RegNatural = 1
            .TMax = 5.5: .Mira = 1.1
            .Olho = 1.1: .Camufl = 1
            .ArmaM�o = 5 'Rifle
        Case 3
            .Nome = "Fogueteiro"
            .Ht = 30: .HtMax = 30: .Reg = 1: .RegNatural = 1: .Blindagem = 0.7
            .TMax = 8: .Mira = 1.1
            .Olho = 1: .Camufl = 0.3
            .ArmaM�o = 4 'lan�a-fog.
        Case 4
            .Nome = "Velho Escopeteiro"
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 5.5: .Mira = 0.9
            .Olho = 0.9: .Camufl = 1
            .ArmaM�o = 2 'Espingarda
        Case 5
            .Nome = "Zumbi"
            .Ht = 10: .HtMax = 10: .Reg = 1: .RegNatural = 1
            .TMax = 4: .Mira = 0.6
            .Olho = 1: .Camufl = 0.8
            .ArmaM�o = 24 'estrelinhas
        Case 6
            .Nome = "Profissional"
            .Ht = 7: .HtMax = 7: .Reg = 1: .RegNatural = 1
            .TMax = 5.4: .Mira = 1
            .Olho = 1: .Camufl = 1
            .ArmaM�o = 3 'Metralhadora
        Case 7
            .Nome = "Medo"
            .Ht = 4: .HtMax = 4: .Reg = 1: .RegNatural = 1
            .TMax = 5: .Mira = 1
            .Olho = 1: .Camufl = 1
            .ArmaM�o = 28 'Dedo duro
        End Select
        .Cur = TipoMons(b%)
        .Nome = .Nome & vbNullChar
    End With
Next
End Sub

Sub ReEscreve()
Dim bobo As Single, sab As Single
Dim cont, des

Jogador.Caption = ArrumaNome(J(vz).Nome) & "-(" & J(vz).Time & ")-" & PoderBytStr(J(vz).Poder)
Jogador.FontSize = 14
If Len(Jogador.Caption) > 18 Then Jogador.FontSize = 12
If Len(Jogador.Caption) > 22 Then Jogador.FontSize = 10

Jogador.ForeColor = QBColor(J(vz).Cor)
ResLabel.Caption = "Resist�ncia: " + Format(J(vz).Ht, "#0.0#")
ResBarra.Width = 105 * J(vz).Ht / J(vz).HtMax
If J(vz).Ht / J(vz).HtMax <= 0.25 Then
    ResBarra.BackColor = RGB(192, 0, 0)  'Vermelho
ElseIf J(vz).Ht / J(vz).HtMax <= 0.5 Then
    ResBarra.BackColor = RGB(192, 192, 0) 'Amarelo
Else
    ResBarra.BackColor = RGB(0, 128, 0)  'Verde
End If
TempoLabel.Caption = "Tempo: " + Format(J(vz).T, "#0.00#")
If J(vz).T > 0 Then TempoBarra.Width = 105 * J(vz).T / J(vz).TMax
AndarLabel.Caption = str(J(vz).Z + 1) + "� Andar"

ArmaNaM�o.Caption = Trim(Arma$(J(vz).ArmaM�o))

'********************* Setor 1 - Bot�es de acess�rios (+- independente do Poder)
Muni��o.Caption = "Muni��o: " + str(J(vz).Mun(J(vz).ArmaM�o))
Medkits.Caption = "Medkits: " + str(J(vz).Med)
NMinas.Caption = "Minas: " + str(J(vz).Minas)
Granadas.Caption = "Granadas: " + str(J(vz).Gran)
Corpos.Caption = "Corpos: " + str(J(vz).Prot)
Bomba.Caption = "Bombas: " + str(J(vz).Bombas)
Holografia.Caption = "Holografias: " + str(J(vz).Hols)
Fuma�a.Caption = "Bombas de Fuma�a: " + str(J(vz).Fuma�)
If J(vz).Poder = pMago Then
    ManaLabel.Caption = "Mana: " + str(J(vz).ManaF�)
ElseIf J(vz).Poder = pClerigo_Maligno Then
    ManaLabel.Caption = "F�: " + str(J(vz).ManaF�)
End If

Holografia.FontUnderline = False
If J(vz).ZHol <> 255 Then Holografia.FontUnderline = True 'A holografia est� ativada
ManaLabel.Visible = False
If J(vz).Poder = pMago Or J(vz).Poder = pClerigo_Maligno Then ManaLabel.Visible = True
Holografia.Visible = True
If J(vz).Poder <> pMago Then
    If J(vz).Hols = 0 And J(vz).ZHol = 255 Then Holografia.Visible = False
    If J(vz).Hols = 0 And J(vz).ZHol <> 255 Then Holografia.Visible = True
Else
    Holografia.Caption = "Holografia"
End If
Medkits.Visible = IIf(J(vz).Med = 0, False, True)
NMinas.Visible = IIf(J(vz).Minas = 0, False, True)
Corpos.Visible = IIf(J(vz).Prot = 0, False, True)
Granadas.Visible = IIf(J(vz).Gran = 0, False, True)

'If J(vz).Bombas = 0 And J(vz).XBomba(1) = 666 Then
'    Bomba.Visible = False
'Else
'    Bomba.Visible = True
'    Bomba.FontUnderline = False
'    If J(vz).XBomba(1) <> 666 Then Bomba.FontUnderline = True 'A bomba est� ativada
'End If
BombaExpl.Visible = IIf(BombasAtivadas(vz) > 0, True, False)
Bomba.Visible = IIf(J(vz).Bombas > 0, True, False)

AtirarGranada.Visible = J(vz).AtvGran
DescerMoto.Visible = J(vz).Moto
'If J(vz).Rastejando = False Then
'    Rastejar.Caption = "Rastejar"
'ElseIf J(vz).Rastejando = True Then
'    Rastejar.Caption = "Levantar"
'End If
Rastejar.Caption = IIf(J(vz).Rastejando, "Levantar", "Rastejar")
If J(vz).Enterrado = False Then
    BuracoZerg.Caption = "Enterrar-se"
ElseIf J(vz).Enterrado = True Then
    BuracoZerg.Caption = "Sair"
End If

'Tiros
TiroIns.Visible = True
TiroMir.Visible = True
TiroRaj.Visible = True
If DIns(J(vz).ArmaM�o) = 0 Then TiroIns.Visible = False
If DMir(J(vz).ArmaM�o) = 0 Then TiroMir.Visible = False
If DAut(J(vz).ArmaM�o) = 0 Then TiroRaj.Visible = False

'********************* Setor 2 - Desativa��o bot�os�stico por falta de tempo(ou estar enterrado)
PassaVez.Default = True

TrocaArma.Enabled = True
CamuflBot�o.Enabled = True
Pegar.Enabled = True
Granadas.Enabled = True
AtirarGranada.Enabled = True
SobeAndar.Enabled = True: DesceAndar.Enabled = True
NMinas.Enabled = True
Medkits.Enabled = True
Corpos.Enabled = True
Bomba.Enabled = True: BombaExpl.Enabled = True
Rastejar.Enabled = True
DescerMoto.Enabled = True
Holografia.Enabled = True
ControlePsi�nico.Enabled = True
SniktBot�o.Enabled = True
AutoDestrui��o.Enabled = True
TrocarCorpo.Enabled = True
Rezar.Enabled = True: Cura.Enabled = True: AnimarMortos.Enabled = True: Rel�mpago.Enabled = True
RegT1000.Enabled = True: Imitar.Enabled = True
Identifica��o.Enabled = True: PararTempo.Enabled = True: Teletransporte.Enabled = True
AtirarParasita.Enabled = True: BuracoZerg.Enabled = True
Fuma�a.Enabled = True: DesarmarMinas.Enabled = True
TiroIns.Enabled = True: TiroRaj.Enabled = True: TiroMir.Enabled = True

If J(vz).Enterrado = True Then bobo = J(vz).T: J(vz).T = 0 'Desativa todos os bot�es

If J(vz).Poder = pT_1000 And J(vz).T < J(vz).HtMax - J(vz).Ht Then RegT1000.Enabled = False
If J(vz).T < 0.5 And J(vz).Invis <> 0 Then CamuflBot�o.Enabled = False
If J(vz).T < 0.5 And J(vz).Poder = pDinamitador Then Granadas.Enabled = False
If J(vz).T < 0.5 Then
    'If J(vz).XBomba(1) < 666 Then Bomba.Enabled = False 'Se a bomba estiver ativada
    BombaExpl.Enabled = False
End If
If J(vz).T < 1 Then
    Teletransporte.Enabled = False
    AtirarParasita.Enabled = False
    If J(vz).Poder = pTarrasque Or J(vz).Poder = pDragao Then Corpos.Enabled = False
    If J(vz).Poder <> pTanque Then TrocaArma.Enabled = False
    If J(vz).Invis = 0 Then CamuflBot�o.Enabled = False  'o tempo da camufl. do predador � diferente do tempo do capacete
    SniktBot�o.Enabled = False
    Pegar.Enabled = False
    Fuma�a.Enabled = False
    If J(vz).Poder = pDinamitador Then AutoDestrui��o.Enabled = False
End If
If J(vz).T < 1.5 Then PararTempo.Enabled = False
If J(vz).T < 2 Then
    Cura.Enabled = False
    ControlePsi�nico.Enabled = False
    Rastejar.Enabled = False
    Identifica��o.Enabled = False
    Holografia.Enabled = False
    DesarmarMinas.Enabled = False
    AtirarGranada.Enabled = False
    Imitar.Enabled = False
    DescerMoto.Enabled = False
    If Not J(vz).Poder = pDinamitador Then Granadas.Enabled = False
End If
If J(vz).T < 2.5 Then
    AnimarMortos.Enabled = False
    SobeAndar.Enabled = False
    DesceAndar.Enabled = False
End If
If J(vz).T < 3 Then
    Rel�mpago.Enabled = False
    TrocarCorpo.Enabled = False
    Teletransporte.Enabled = False
    NMinas.Enabled = False
    If J(vz).Poder = pPredador Then AutoDestrui��o.Enabled = False
    'If J(vz).XBomba(1) = 666 Then Bomba.Enabled = False
    Bomba.Enabled = False
End If
If J(vz).T < 3.5 Then Rezar.Enabled = False
If J(vz).T < 4 Then Medkits.Enabled = False
If J(vz).T < 5 And J(vz).Poder <> pTarrasque And J(vz).Poder <> pDragao Then Corpos.Enabled = False
If J(vz).Poder = pSabata Then sab = 0.7 Else sab = 1
If J(vz).T < TIns(J(vz).ArmaM�o) * sab Then TiroIns.Enabled = False
If J(vz).T < TAut(J(vz).ArmaM�o) * sab Then TiroRaj.Enabled = False
If J(vz).T < TMir(J(vz).ArmaM�o) * sab Then TiroMir.Enabled = False

If J(vz).Enterrado = True Then J(vz).T = bobo 'Rep�e o tempo
If J(vz).T < 3 And J(vz).Enterrado = False Then BuracoZerg.Enabled = False 'tempo para se enterrar
If J(vz).T < 2.5 And J(vz).Enterrado = True Then BuracoZerg.Enabled = False 'tempo para se desenterrar

If ControlePsi�nico.HelpContextID = 1 Then ControlePsi�nico.Enabled = False
If CamuflBot�o.HelpContextID = 1 Then CamuflBot�o.Enabled = False
If J(vz).Hols = 0 And J(vz).ZHol <> 255 Then Holografia.Enabled = False
If J(vz).Moto Then
    'TrocaArma.Enabled = False
    'CamuflBot�o.Enabled = False
    Pegar.Enabled = False
    Granadas.Enabled = False
    AtirarGranada.Enabled = False
    NMinas.Enabled = False
    Medkits.Enabled = False
    Corpos.Enabled = False
    Bomba.Enabled = False
    Rastejar.Enabled = False
    Holografia.Enabled = False
    'ControlePsi�nico.Enabled = False
    SniktBot�o.Enabled = False
    'AutoDestrui��o.Enabled = False
    TrocarCorpo.Enabled = False
    'RegT1000.Enabled = False
    Imitar.Enabled = False
    Identifica��o.Enabled = False: PararTempo.Enabled = False: Teletransporte.Enabled = False
    AtirarParasita.Enabled = False: BuracoZerg.Enabled = False
    Fuma�a.Enabled = False: DesarmarMinas.Enabled = False
    TiroIns.Enabled = False: TiroRaj.Enabled = False: TiroMir.Enabled = False
End If

'********************* Setor 3 - Desaparecimento bot�oz�stico pelo Poder
Pegar.Visible = True
Rastejar.Visible = True
TrocaArma.Visible = True
AtirarLabel.Visible = True: Muni��o.Visible = True
Fuma�a.Visible = True: DesarmarMinas.Visible = True
If J(vz).ArmaM�o = 0 Then
    AtirarLabel.Visible = False
    Muni��o.Visible = False
End If
If PP(J(vz).Poder).NaoPodePegarNada Then Pegar.Visible = False
If PP(J(vz).Poder).NaoPodeRastejar Then Rastejar.Visible = False
If PP(J(vz).Poder).NaoPodeTrocarDeArma Then TrocaArma.Visible = False
'Select Case J(vz).Poder
'Case pTarrasque, pBalacau, pAlien
'    Rastejar.Visible = False
'    TrocaArma.Visible = False
'Case pDragao
'    Rastejar.Visible = False
'    TrocaArma.Visible = False
'Case pRobocop
'    Pegar.Visible = False
'    Rastejar.Visible = False
'    TrocaArma.Visible = False
'Case pZerg
'    Pegar.Visible = False
'    TrocaArma.Visible = False
'Case pJason
'    Pegar.Visible = False
'    Rastejar.Visible = False
'    TrocaArma.Visible = False
'Case pTanque
'    Pegar.Visible = False
'    Rastejar.Visible = False
'End Select
If J(vz).Poder = pAgente_do_Matrix Then TrocarCorpo.Visible = True Else TrocarCorpo.Visible = False
If J(vz).Poder = pT_1000 Then
    RegT1000.Visible = True: Imitar.Visible = True
Else
    RegT1000.Visible = False: Imitar.Visible = False
End If
If J(vz).Poder = pMago And J(vz).ArmaM�o = 22 Then Muni��o.Visible = False 'Mago c/ Bola de fogo
If J(vz).Poder <> pMago Then
    Teletransporte.Visible = False: Identifica��o.Visible = False: PararTempo.Visible = False
Else
    Imitar.Visible = True 'O t-1000 tamb�m tem
    Teletransporte.Visible = True: Identifica��o.Visible = True: PararTempo.Visible = True
    If TempoParado Then PararTempo.Enabled = False
End If
If J(vz).Poder = pClerigo_Maligno Then
    Rezar.Visible = True: Cura.Visible = True: AnimarMortos.Visible = True: Rel�mpago.Visible = True
Else
    Rezar.Visible = False: Cura.Visible = False: AnimarMortos.Visible = False: Rel�mpago.Visible = False
End If
If J(vz).Poder <> pNinja Or J(vz).Fuma� = 0 Then Fuma�a.Visible = False
If J(vz).Poder <> pNinja Then DesarmarMinas.Visible = False

'Espadas, machados, soco, etc.
If Batida(J(vz).ArmaM�o) > 0 Or J(vz).ArmaM�o = 26 Then Muni��o.Visible = False: AtirarLabel.Visible = False
If J(vz).Poder = pZerg Then
    AtirarParasita.Visible = True
    BuracoZerg.Visible = True
    If J(vz).Parasitas = False Then AtirarParasita.Enabled = False
Else
    AtirarParasita.Visible = False
    BuracoZerg.Visible = False
End If
If J(vz).Poder = pWolverine Then
    SniktBot�o.Visible = True
    SniktLabel.Visible = True
    If J(vz).Snikt = True Then
        SniktLabel.Caption = "Snikt : ON"
    Else
        SniktLabel.Caption = "Snikt : OFF"
    End If
Else
    SniktBot�o.Visible = False
    SniktLabel.Visible = False
End If
If J(vz).Poder = pEthereal Then ControlePsi�nico.Visible = True Else ControlePsi�nico.Visible = False

AutoDestrui��o.Visible = False
If J(vz).Poder = pDinamitador Then AutoDestrui��o.Visible = True
If J(vz).Poder = pPredador Then
    CamuflBot�o.Visible = True
    CamuflLabel.Visible = True
    If J(vz).Camufl > J(vz).CamuflNatural Then
        CamuflLabel.Caption = "Camuflagem : ON"
    Else
        CamuflLabel.Caption = "Camuflagem : OFF"
    End If
    's� fica apert�vel se n�o tiver ativado a auto-destrui��o
    If J(vz).ContBomba <= 0 Then
        AutoDestrui��o.Visible = True
        AutoDestrui��o.Caption = "AUTO-DESTRUI��O"
    Else
        AutoDestrui��o.Visible = True: AutoDestrui��o.Enabled = False
        AutoDestrui��o.Caption = "Explos�o em" + str$(J(vz).ContBomba)
    End If
ElseIf J(vz).Poder = pBalacau Then
    CamuflBot�o.Visible = True
    CamuflLabel.Visible = True
    If J(vz).Camufl > J(vz).CamuflNatural Then
        CamuflLabel.Caption = "Camuflagem : ON"
    Else
        CamuflLabel.Caption = "Camuflagem : OFF"
    End If
ElseIf J(vz).Invis < 0 Then 'Capacete Desligado
    CamuflBot�o.Visible = True
    CamuflLabel.Visible = True
    CamuflLabel.Caption = "Capacete (" + LTrim$(str$(-J(vz).Invis)) + ") : OFF"
    If J(vz).Poder = pMago Then
        CamuflLabel.Caption = "Invisibilidade : OFF"
    End If
ElseIf J(vz).Invis > 0 Then 'Capacete Ligado e gastando
    CamuflBot�o.Visible = True
    CamuflLabel.Visible = True
    CamuflLabel.Caption = "Capecete (" + LTrim$(str$(J(vz).Invis)) + ") : ON"
    If J(vz).Poder = pMago Then
        CamuflLabel.Caption = "Invisibilidade : ON"
    End If
ElseIf J(vz).Invis = 0 Then 'N�o tem capacete
    CamuflBot�o.Visible = False
    CamuflLabel.Visible = False
End If

'Mostra o que esta embaixo do cara
cont = Conte�do(J(vz).X, J(vz).Y, J(vz).Z)
If cont <> 0 And cont <> 6 And cont <> 25 And cont <> 50 Then 'Objetos invis�veis
    If cont = 9 Then
        des = 5
    Else
        des = cont
    End If
    Embaixo.PaintPicture Mapa.ObjetoH(des).Picture, 0, 0
Else
    Embaixo.Line (0, 0)-(25, 25), QBColor(15), BF
End If

'Voador e drag�o sempre podem voar; Mosca e ethereal podem descer quando houver um buraco abaixo deles
If cont = 20 Or (J(vz).Poder = pVoador And Not J(vz).Moto) Or J(vz).Poder = pDragao Then
    SobeAndar.Enabled = True
    DesceAndar.Enabled = True
ElseIf cont = 52 Then
    SobeAndar.Enabled = True
    DesceAndar.Enabled = False
ElseIf cont = 53 Then
    SobeAndar.Enabled = False
    DesceAndar.Enabled = True
Else
    SobeAndar.Enabled = False
    DesceAndar.Enabled = False
    If cont = 26 And (J(vz).Poder = pMosca Or J(vz).Poder = pEthereal) Then DesceAndar.Enabled = True
End If

Call Scanner
Call Sentidos

AtualizaMiniMapa Mapa

End Sub

Sub Scanner()
Dim color As Long
Dim a, maisperto
Dim dmaisperto!, d!, angulo!, angobs!, difang!

ScannerQ.Visible = False
ScannerLabel.Visible = False
If Not J(vz).Detec Then Exit Sub 'linha escrita pelo Jairo
a = 0: dmaisperto! = 1000
Do
    a = a + 1
    If a > NJogs And a <= M Then 's� para iniciar a rodada dos monstros
        a = M + 1
    End If
    If (a > NJogs And NMons = 0) Or (a > NMons + M And NMons > 0) Then Exit Do
    If a <> vz And J(a).Morte = False And J(a).Poder <> pFantasma Then
        d! = Sqr((J(vz).X - J(a).X) ^ 2 + (J(vz).Y - J(a).Y) ^ 2 + ((CInt(J(vz).Z) - CInt(J(a).Z)) * AlturaAndar) ^ 2)
        'Angulo = -Atn((J(a).Y - J(vz).Y) / (J(a).X - J(vz).X + 0.0001))  ' de -pi/2 a pi/2
        'If J(a).X < J(vz).X Then Angulo = Angulo + pi
        angulo = -funAngulo(J(vz).X, J(vz).Y, J(a).X, J(a).Y)
        If angulo < 0 Then angulo = angulo + 2 * pi
        angobs = (pi / 6) * (12 - J(vz).Vis�o)
        difang = Abs(angulo - angobs) 'de 0 a 2*pi
        ' Dectecta num ang. de mais ou menos 90 graus
        If d! < dmaisperto! And (difang < Aleat�rio(pi / 4) Or difang > 2 * pi - Aleat�rio(pi / 4)) Then
            maisperto = a: dmaisperto! = d!
        End If
    End If
Loop
d! = dmaisperto!: a = maisperto
color = CorSensor(d!)

ScannerQ.Visible = True
ScannerLabel.Visible = True
ScannerQ.BackColor = color
End Sub

Sub Sentidos()
Dim color As Long
Dim se As Boolean
Dim a, b, n, naotem%, c
Dim dmperto!, dm!, d!, angulo!

For a = 1 To 6
    Sensor(a).Visible = False
    SensorNome(a).Visible = False
    SensorDirec(a).Visible = False
Next
b = 0
n = NJogs
'If J(NJogs).Poder = pCapangas Then
'    For A = 1 To NJogs - 1
'        If J(A).Poder = pCapangas And Not J(A).Morte Then n = NJogs - 1: Exit For
'    Next
'End If
If n > 6 Then n = 6 'Apenas se houver muitos jogadores
For a = 1 To n
    naotem% = 0  'JAIRO
    se = False
    If a = 0 Then 'a=o significa monstro
        se = True
    ElseIf a <> vz And J(a).Nome <> J(vz).Nome And J(a).Morte = False Then
        se = True
    Else
        se = False
    End If
    If se Then
        If a = 0 Then 'monstro
            dmperto! = 0
            For c = 1 + M To NMons + M
                If Not J(c).Morte Then
                    'dm! = Sqr((J(vz).X - X(c)) ^ 2 + (J(vz).Y - Y(c)) ^ 2 + ((CInt(J(vz).Z) - CInt(Z(c))) * AlturaAndar) ^ 2)
                    dm! = Sqr(Distancia(J(vz).X, J(vz).Y, J(c).X, J(c).Y) ^ 2 + ((CInt(J(vz).Z) - CInt(J(c).Z)) * AlturaAndar) ^ 2)
                    If dm! < dmperto! Or dmperto! = 0 Then a = c: dmperto! = dm!
                End If
                If a = 0 Then Exit Sub
            Next
        'ElseIf J(A).Poder = pCapangas Then
        '    'd1 = Sqr((J(vz).X - J(a).X) ^ 2 + (J(vz).Y - J(a).Y) ^ 2 + ((CInt(J(vz).Z) - CInt(J(a).Z)) * AlturaAndar) ^ 2)
        '    'd2 = Sqr((J(vz).X - X(NJogs)) ^ 2 + (J(vz).Y - Y(NJogs)) ^ 2 + ((CInt(J(vz).Z) - CInt(Z(NJogs))) * AlturaAndar) ^ 2)
        '     d1 = Sqr(Distancia(J(vz).X, J(vz).Y, J(A).X, J(A).Y) ^ 2 _
        '        + ((CInt(J(vz).Z) - CInt(J(A).Z)) * AlturaAndar) ^ 2)
        '    d2 = Sqr(Distancia(J(vz).X, J(vz).Y, J(NJogs).X, J(NJogs).Y) ^ 2 _
        '        + ((CInt(J(vz).Z) - CInt(J(NJogs).Z)) * AlturaAndar) ^ 2)
        '    swap = Empty
        '    If d2 < d1 And vz <> NJogs Then swap = A: A = NJogs
        End If
        'D! = Sqr((J(vz).X - J(a).X) ^ 2 + (J(vz).Y - J(a).Y) ^ 2 + ((CInt(J(vz).Z) - CInt(J(a).Z)) * AlturaAndar) ^ 2)
        d! = Sqr(Distancia(J(vz).X, J(vz).Y, J(a).X, J(a).Y) ^ 2 _
            + ((CInt(J(vz).Z) - CInt(J(a).Z)) * AlturaAndar) ^ 2)
        color = CorSensor(d!)
        'If (J(a).Poder = pFantasma Or J(a).Poder = pT_1000) And Not J(vz).Moto Then color = 0
        
        b = b + 1: If a > NJogs Then b = 6
        'If (D! <= J(vz).Olfato Or (J(a).Poder = pRobocop And D! <= 10) Or (J(a).Poder = pTanque And D! <= 30) Or (J(a).Moto And D! <= 20)) And (J(a).Poder <> pFantasma And J(a).Poder <> pT_1000) And J(vz).Z = J(a).Z Then
        If (d! <= J(vz).Olfato Or (J(a).Poder = pRobocop And d! <= 10) Or (J(a).Poder = pTanque And d! <= 30) Or (J(a).Moto And d! <= 20)) And J(vz).Z = J(a).Z Then
            If (J(a).Poder = pFantasma Or J(a).Poder = pT_1000) And Not J(a).Moto Then
                SensorDirec(b).Caption = " "
                naotem% = 1  'JAIRO
            Else
                'Angulo = -Atn((J(a).Y - J(vz).Y) / (J(a).X - J(vz).X + 0.0001))  ' de -pi/2 a pi/2
                'If J(a).X < J(vz).X Then Angulo = Angulo + pi
                angulo = -funAngulo(J(vz).X, J(vz).Y, J(a).X, J(a).Y)
                If angulo < 0 Then angulo = angulo + 2 * pi
                If J(a).Poder = pRobocop Or J(a).Poder = pTanque Or J(a).Moto Then
                    SensorDirec(b).ForeColor = RGB(255, 0, 0) 'Vermelho
                Else
                    SensorDirec(b).ForeColor = RGB(0, 0, 0) 'Preto
                End If
                If angulo < pi / 8 Then
                    SensorDirec(b).Caption = "�"  'E
                ElseIf angulo < 3 * pi / 8 Then
                    SensorDirec(b).Caption = "�"  'NE
                ElseIf angulo < 5 * pi / 8 Then
                    SensorDirec(b).Caption = "�"  'N
                ElseIf angulo < 7 * pi / 8 Then
                    SensorDirec(b).Caption = "�"  'NO
                ElseIf angulo < 9 * pi / 8 Then
                    SensorDirec(b).Caption = "�"  'O
                ElseIf angulo < 11 * pi / 8 Then
                    SensorDirec(b).Caption = "�"  'SO
                ElseIf angulo < 13 * pi / 8 Then
                    SensorDirec(b).Caption = "�"  'S
                ElseIf angulo < 15 * pi / 8 Then
                    SensorDirec(b).Caption = "�"  'SE
                Else
                    SensorDirec(b).Caption = "�"  'E
                End If
            End If
        Else
            SensorDirec(b).Caption = " "
            '''If Not J(vz).Detec Then color = 7
            naotem% = 1  'JAIRO
        End If
        If naotem% = 0 Then
            Sensor(b).Visible = True
            SensorNome(b).Visible = True
            SensorDirec(b).Visible = True
            Sensor(b).BackColor = color 'QBColor(color)
            If a <= NJogs Then SensorNome(b).ForeColor = QBColor(J(a).Cor) Else SensorNome(b).ForeColor = 0
            SensorNome(b).Caption = J(a).Nome
        End If
        'If J(A).Poder = pCapangas And swap <> 0 Then A = swap
    End If
    If b > 6 Then Exit For 'So ha espaco para 6 caras
    If a = n And NMons > 0 Then a = -1
Next
End Sub

Private Function CorSensor(d As Single) As Long
Dim red As Integer, green As Integer
red = 0
If d < 16 Then
    red = 255
ElseIf d < 32 Then
    red = ((32 - d) / 16) * 255
End If
green = 0
If d < 16 Then
    green = (d / 16) * 255
ElseIf d < 28 Then
    green = 255
ElseIf d < 40 Then
    green = ((40 - d) / 12) ^ 2 * 255
End If
green = CInt(green / 64) * 64
red = CInt(red / 64) * 64
CorSensor = RGB(red, green, 0)
End Function

Private Function BombasAtivadas(Q As Byte) As Byte
Dim i As Byte, n As Byte
For i = LBound(J(1).XBomba, 1) To UBound(J(1).XBomba, 1)
    If J(Q).XBomba(i) <> 666 Then n = n + 1
Next
BombasAtivadas = n
End Function

Private Sub Pegar_Click()
If J(vz).Poder = pJedi And J(vz).T >= 1 Then
    Tiro = conJediPega 'Eu n�o qeria criar uma outra vari�vel s� para este uso raro
    Mapa.Borda.MousePointer = 2
ElseIf J(vz).T >= 1 Then
    Call Pega(J(vz).X, J(vz).Y, J(vz).Z)
Else
    Call SemTempo("pegar isso")
End If
End Sub

Private Sub Centrar_Click()
Call Centra(Mapa, J(vz).X, J(vz).Y)
End Sub

'Private Sub Sensor_Click(Index As Integer)
'Dim a As Byte
'For a = 1 To NJogs
'    If SensorNome(Index) = J(a).Nome Then
'        If Est�Vendo(a) And ViHol(a) = False Then Call Centra(J(a).X, J(a).Y) 'Viu s� o cara
'        If Est�Vendo(a) = False And ViHol(a) Then Call Centra(J(a).XHol, J(a).YHol) 'Viu s� a holografia do cara
'        Exit Sub
'    End If
'Next
'End Sub

Sub Form_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case Asc("c"), Asc("C") 'Centrar
        Call Centra(Mapa, J(vz).X, J(vz).Y)
    Case Asc("1"), Asc("m"), Asc("M") 'Mirado
        If DMir(J(vz).ArmaM�o) > 0 Then
            Tiro = 2
            Mapa.Borda.MousePointer = 2
        End If
    Case Asc("2"), Asc("i"), Asc("I") 'Instant�neo
        If DIns(J(vz).ArmaM�o) > 0 Then
            Tiro = 1
            Mapa.Borda.MousePointer = 2
        End If
    Case Asc("3"), Asc("r"), Asc("R") 'Rajada
        If DAut(J(vz).ArmaM�o) > 0 Then
            Tiro = 3
            Mapa.Borda.MousePointer = 2
        End If
    Case vbKeyEscape
        Click = 2
End Select
End Sub

Private Sub Rastejar_Click()
If J(vz).Rastejando = False Then
    If J(vz).T >= 2 Then
        J(vz).Rastejando = True
        J(vz).T = J(vz).T - 2
        Rastejar.Caption = "Levantar"
        Call ReEscreve
        Call Cursor(vz)
    Else
        Call SemTempo("se abaixar")
    End If
ElseIf J(vz).Rastejando = True Then
    If J(vz).T >= 2 Then
        J(vz).Rastejando = False
        J(vz).T = J(vz).T - 2
        Rastejar.Caption = "Rastejar"
        Call ReEscreve
        Call Cursor(vz)
    Else
        Call SemTempo("se levantar")
    End If
End If
End Sub

Private Sub AutoDestrui��o_Click()
Dim Mensagem As String, Bot�es As VbMsgBoxStyle, T�tulo As String
Dim resposta As VbMsgBoxResult

If J(vz).Poder = pPredador Then
    Mensagem = "Quer mesmo ativar Auto-Destrui��o ?" + Chr$(13) + "Aten��o! Ap�s ativada, a auto-destrui��o � irrevers�vel!"
    Bot�es = vbYesNo '+ vbExclamation
    T�tulo = "Grosseria"
    'Resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
    resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
    If resposta = vbYes Then
        J(vz).T = J(vz).T - 3
        J(vz).ContBomba = 2
        AutoDestrui��o.Enabled = False
        AutoDestrui��o.Caption = "Explos�o em" + str$(J(vz).ContBomba)
    End If
ElseIf J(vz).Poder = pDinamitador Then
    Mensagem = "Quer mesmo destruir o mapa ?" + Chr$(13) + "Aten��o! Voc� certamente morrer�!"
    Bot�es = vbYesNo '+ vbExclamation
    T�tulo = "Grosseria"
    'Resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
    resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
    If resposta = vbYes Then
        J(M).DimiExplodiu = True
        MsgMuda ("Voc� EXPLODIU !! O mapa do RS ter� de ser alterado !")
        Call Explos�o(Abs(J(vz).X), Abs(J(vz).Y), 0, 0, J(vz).Z, 80, 15, 1)
        Call TestaMortes
        'J(vz).Ht = 0
        'Call TestaMortes
    End If
End If
End Sub

Private Sub RegT1000_Click()
Do
    If J(vz).Ht >= J(vz).HtMax Then J(vz).Ht = J(vz).HtMax: Exit Do
    J(vz).T = J(vz).T - 0.1
    J(vz).Ht = J(vz).Ht + 0.1
Loop
Call ReEscreve
End Sub

Private Sub Identifica��o_Click()
If J(vz).ManaF� < Mana("Identifica��o") Then
    'MsgMuda ("Voc� precisa de " + Trim(Mana("Identifica��o")) + " de mana para identificar.")
    MsgMuda "Voc� precisa de " + Trim(Mana("Identifica��o")) + " de mana para identificar."
    Exit Sub
End If
Comando = 3 'Identificar = True
Mapa.Borda.MousePointer = 2
Call ReEscreve
End Sub

Private Sub PararTempo_Click()
If J(vz).ManaF� < Mana("PararTempo") Then
    'MsgMuda ("Voc� precisa de " + Trim(Mana("PararTempo")) + " de mana para parar o tempo !")
    MsgMuda "Voc� precisa de " + Trim(Mana("PararTempo")) + " de mana para parar o tempo !"
    Exit Sub
End If
J(vz).ManaF� = J(vz).ManaF� - Mana("PararTempo"): J(vz).T = J(vz).TMax
Foto = Foto + 1
J(vz).RepA��oOQue(Foto) = 7
J(vz).RepA��oQ1(Foto) = J(vz).X
J(vz).RepA��oQ2(Foto) = J(vz).Y
GravaQuemViu False
TempoParado = True
Call ReEscreve
End Sub

Private Sub DescerMoto_Click()
J(vz).Moto = False
Conte�do(J(vz).X, J(vz).Y, J(vz).Z) = 59
J(vz).T = J(vz).T - 2
Call Cursor(vz)
Call ReEscreve
End Sub

Private Sub TiroIns_Click()
Tiro = 1
Mapa.Borda.MousePointer = 2
End Sub

Private Sub TiroMir_Click()
Tiro = 2
Mapa.Borda.MousePointer = 2
End Sub

Private Sub TiroRaj_Click()
Tiro = 3
Mapa.Borda.MousePointer = 2
End Sub

Private Sub SobeAndar_Click()
Dim Mensagem As String, Bot�es As VbMsgBoxStyle, T�tulo As String
Dim resposta As VbMsgBoxResult

If J(vz).T - 2.5 < 0 Then
    Call SemTempo("subir")
ElseIf Andar = AndMax Then
    Mensagem = "Voc� est� no �ltimo andar."
    Bot�es = vbOKOnly '+ vbExclamation
    T�tulo = "Duelo"
    'Resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
    resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
Else
    dand = 1
    Call MudandoDeAndar
End If
End Sub

Private Sub DesceAndar_Click()
Dim Mensagem As String, Bot�es As VbMsgBoxStyle, T�tulo As String
Dim resposta As VbMsgBoxResult

If J(vz).T - 2.5 < 0 Then
    Call SemTempo("descer")
ElseIf Andar = 0 Then
    Mensagem = "Voc� n�o pode descer mais !"
    Bot�es = vbOKOnly '+ vbExclamation
    T�tulo = "Duelo"
    'Resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
    resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
Else
    dand = -1
    Call MudandoDeAndar
End If

End Sub

Sub MudandoDeAndar()
Dim c1, a, c, d!

c1 = Conte�do(J(vz).X, J(vz).Y, J(vz).Z + dand)
If c1 = 47 Or c1 = 48 Then 'pared�o ou porta
    'If vz <= M Then MsgMuda ("N�o d� !!")
    If vz <= M Then MsgMuda "N�o d� !!"
    Exit Sub
End If
c1 = Conte�do(J(vz).X, J(vz).Y, J(vz).Z)
For a = 1 To NJogs
    If J(a).X = J(vz).X And J(a).Y = J(vz).Y And J(vz).Z + dand = J(a).Z And a <> vz Then
        If J(vz).Poder <> pVoador And J(vz).Poder <> pDragao Then
            'If vz <= M Then MsgMuda ("Tem alguma coisa trancando o elevador !!")
            If vz <= M Then MsgMuda "Tem alguma coisa trancando o elevador !!"
            Exit Sub
        Else
            'If vz <= M Then MsgMuda ("Tem algu�m no caminho !!")
            If vz <= M Then MsgMuda "Tem algu�m no caminho !!"
            Exit Sub
        End If
    End If
Next
If (J(vz).Poder = pVoador Or J(vz).Poder = pDragao) And dand = -1 And c1 <> 20 And c1 <> 53 Then Conte�do(J(vz).X, J(vz).Y, J(vz).Z) = 26
J(vz).Z = J(vz).Z + dand

Foto = Foto + 1
J(vz).RepA��oOQue(Foto) = 4
J(vz).RepA��oQ1(Foto) = 1
If dand = -1 Then J(vz).RepA��oQ1(Foto) = 2

Andar = Andar + dand
J(vz).T = J(vz).T - 2.5
If vz <= M Then
    If dand = 1 Then AndarLabel.Caption = "Subindo...": Refresh
    If dand = -1 Then AndarLabel.Caption = "Descendo...": Refresh
End If
Mapa.MousePointer = 11
Duelando.MousePointer = 11
c = Conte�do(J(vz).X, J(vz).Y, J(vz).Z)
If c = 1 Or c = 5 Or c = 9 Or c = 34 Then
    Conte�do(J(vz).X, J(vz).Y, J(vz).Z) = 0
    'If vz <= M Then MsgMuda ("Voc� bateu em uma parede !!")
    If vz <= M Then MsgMuda "Voc� bateu em uma parede !!"
    d = Dor(vz, 2, J(vz).Blindagem)
    Call ReEscreve
    Call TestaMortes
    
    If J(vz).Morte Then
        Mapa.MousePointer = 0
        Duelando.MousePointer = 0
        Call DueloMod.PassaVez
        Exit Sub
    End If
End If
If vz <= M Then
    Call ZeraFogBit
    Call ReDesenha
    Call Cursor(vz): Mapa.Borda.Refresh
    Call ReEscreve
    Call TestaEncontro
Else
    Call TestaEncontroHal
End If
'c1 52 = elevador que sobe
If (J(vz).Poder = pVoador Or J(vz).Poder = pDragao) And dand = 1 And c1 <> 20 And c1 <> 52 Then Conte�do(J(vz).X, J(vz).Y, J(vz).Z) = 26
If c = 6 And ((J(vz).Poder <> pVoador And J(vz).Poder <> pMosca And J(vz).Poder <> pEthereal) Or J(vz).Moto) Then 'testa se caiu em uma mina
    J(vz).Reg = J(vz).Reg - 0.2
    Call Explos�o(J(vz).X, J(vz).Y, 0, 0, J(vz).Z, 6, 3, 1)
    If vz <= M Then Call ReEscreve
ElseIf c = 32 Then 'lata
    Call Explos�o(J(vz).X, J(vz).Y, 0, 0, J(vz).Z, 6, 3.1, 1)
    If vz <= M Then Call ReEscreve
ElseIf c = 45 And J(vz).Parasitado = False Then 'Testa se pisou em um Parasita
    'If vz <= M Then MsgMuda ("Voc� foi atacado por um parasita !")
    If vz <= M Then MsgMuda "Voc� foi atacado por um parasita !"
    J(vz).Parasitado = True
    Conte�do(J(vz).X, J(vz).Y, J(vz).Z) = 0
End If

Call Buraco

If c = 35 Then 'Testa se pisou em um teletransporte
    If vz <= M Then MsgMuda ("Voc� foi teletransportado!")
    'Do
    '    Xtt = Int(Rnd * xM) + 1: Ytt = Int(Rnd * yM) + 1
    '    Ztt = CInt(Rnd * (AndMax + 1) - 0.5)
    '    Vis�ott = Int(Rnd * 8)
    '    p = Null
    '    For b = 1 To NJogs - 1
    '        d = Sqr((X(b) - Xtt) ^ 2 + (Y(b) - Ytt) ^ 2 + ((CInt(Z(b)) - Ztt) * AlturaAndar) ^ 2)
    '        If d < Sqr(xM * yM) / 5 Then p = True
    '        'Obs: ele mant�m dist�ncia at� de onde ele est�
    '    Next
    'Loop While Conte�do(Xtt, Ytt, Ztt) <> 0 Or p = True
    'J(vz).X = Xtt: J(vz).Y = Ytt: J(vz).Z = Ztt
    'J(vz).Vis�o = Vis�ott: Andar = J(vz).Z
    Call Teletransporta(vz)
    Foto = Foto + 1
    J(vz).RepA��oOQue(Foto) = 10
    J(vz).RepA��oQ1(Foto) = J(vz).X
    J(vz).RepA��oQ2(Foto) = J(vz).Y
    J(vz).RepA��oQ3(Foto) = J(vz).Vis�o
    J(vz).RepA��oQ4(Foto) = J(vz).Z
    If vz <= M Then
        Call ZeraFogBit
        Call ReDesenha
        Call ReEscreve
        Call Cursor(vz): Mapa.Borda.Refresh
        Call TestaEncontro
    Else
        Call TestaEncontro
    End If
End If
Mapa.MousePointer = 0
Duelando.MousePointer = 0
dand = Empty

End Sub

Private Sub TrocaArma_Click()

JanTrocaArma.Visible = True
Duelando.Enabled = False
Mapa.Enabled = False
'Call ReEscreve

End Sub

Private Sub CamuflBot�o_Click()
If J(vz).Poder = pPredador Then
    J(vz).T = J(vz).T - 1
    If J(vz).Camufl > J(vz).CamuflNatural Then
        J(vz).Camufl = J(vz).CamuflNatural   'desligou
    Else
        J(vz).Camufl = 5 * J(vz).CamuflNatural 'ligou
    End If
ElseIf J(vz).Poder = pBalacau Then
    J(vz).T = J(vz).T - 1
    If J(vz).Camufl > J(vz).CamuflNatural Then
        J(vz).Camufl = J(vz).CamuflNatural   'desligou
    Else
        J(vz).Camufl = 6 * J(vz).CamuflNatural 'ligou
    End If
Else    ' � o capacete de invisibilidade ou Mago
    If J(vz).Poder = pMago And J(vz).ManaF� < Mana("Invisibilidade") And J(vz).Invis < 0 Then
        MsgMuda ("Voc� precisa de " + Trim(Mana("Invisibilidade")) + " de mana ficar invis�vel !")
        Exit Sub
    End If
    J(vz).T = J(vz).T - 0.5
    If J(vz).Invis > 0 Then
        J(vz).Camufl = J(vz).CamuflNatural   'desligou
    Else
        J(vz).Camufl = 10 * J(vz).CamuflNatural 'ligou
        CamuflBot�o.HelpContextID = 1
    End If
    J(vz).Invis = -J(vz).Invis  'Se estava ON, fica OFF e vice-versa
End If
Call ReEscreve
Call Cursor(vz)
Call TestaEncontro
End Sub

Private Sub PassaVez_Click()
J(vz).T = 0
End Sub

Private Sub Granadas_Click()
Dim n

If Not J(vz).AtvGran Then
    n = 2: If J(vz).Poder = pDinamitador Then n = 0.5
    If J(vz).T - n >= 0 And J(vz).Gran > 0 Then
        J(vz).T = J(vz).T - n
        J(vz).Gran = J(vz).Gran - 1
        Call ReEscreve
        MsgMuda ("Voc� ativou uma granada.")
        J(vz).AtvGran = True
        Call Cursor(vz)
    ElseIf J(vz).T = J(vz).T - n >= 0 Then
        Call SemTempo("ativar uma granada.")
    End If
    Call ReEscreve
End If
End Sub

Private Sub AtirarGranada_Click()
Comando = 1 'Arremessar = True
Mapa.Borda.MousePointer = 2
Call ReEscreve
End Sub

Private Sub AtirarParasita_Click()
Comando = 1 'Arremessar = True
Mapa.Borda.MousePointer = 2
Call ReEscreve
End Sub

Private Sub TeleTransporte_Click()
If J(vz).ManaF� < Mana("Teletransporte") Then
    MsgMuda ("Voc� precisa de " + Trim(Mana("Teletransporte")) + " de mana para se teletransportar.")
    Exit Sub
End If

MsgMuda ("Voc� se teletransportou!")
Call Teletransporta(vz)
    
Foto = Foto + 1
J(vz).RepA��oOQue(Foto) = 10
J(vz).RepA��oQ1(Foto) = J(vz).X
J(vz).RepA��oQ2(Foto) = J(vz).Y
J(vz).RepA��oQ3(Foto) = J(vz).Vis�o
J(vz).RepA��oQ4(Foto) = J(vz).Z
    
J(vz).T = J(vz).T - 3
J(vz).ManaF� = J(vz).ManaF� - Mana("Teletransporte")
Call ReDesenha
Call Duelando.ReEscreve
Call Cursor(vz): Mapa.Borda.Refresh
Call TestaEncontro

End Sub

Private Sub Imitar_Click()
Dim a As Byte
Dim mens$, resp$

If J(vz).Poder = pMago And J(vz).ManaF� < Mana("Imitar") Then
    MsgMuda ("Voc� precisa de " + Trim(Mana("Imitar")) + " de mana esta magia.")
    Exit Sub
End If
mens$ = "Quem voc� quer imitar ?"
mens$ = mens$ + Chr$(13) + "0 - Normal"
For a = 1 To NJogs
    If Not J(a).Morte And J(vz).ViuSemblante(a) Then mens$ = mens$ + Chr$(13) + Trim(a) + " - " + ArrumaNome(J(a).Nome)  '+ " (" + Trim(J(a).X) + ", " + Trim(J(a).Y) + ", " + Trim(J(a).Z + 1) + ")"
Next
For a = 1 + M To NMons + M
    If Not J(a).Morte And J(vz).ViuSemblante(a) Then mens$ = mens$ + Chr$(13) + Trim(a) + " - " + ArrumaNome(J(a).Nome)  '+ " (" + Trim(J(a).X) + ", " + Trim(J(a).Y) + ", " + Trim(J(a).Z + 1) + ")"
Next
resp$ = InputBox(mens$)
a = Val(resp$)
If a > 0 And a <= NMons + M And resp$ <> "" Then
    If Not J(a).Morte And J(vz).ViuSemblante(a) Then
        If J(vz).Imit <> a Then
            J(vz).T = J(vz).T - 2: J(vz).Imit = a
            If J(vz).Poder = pMago Then J(vz).ManaF� = J(vz).ManaF� - Mana("Imitar")
            If a > M Then
                J(vz).Cur = J(a).Cur
                Foto = Foto + 1
                J(vz).RepA��oOQue(Foto) = 3: J(vz).RepA��oQ3(Foto) = J(vz).Cur
                GravaQuemViu False
            Else
                J(vz).Cur = 0
            End If
            Call Cursor(vz): Call ReEscreve
        Else
            MsgMuda ("Voc� j� est� com esta forma !")
        End If
    End If
ElseIf a = 0 And resp$ <> "" Then
    If J(vz).Imit <> vz Then
        J(vz).T = J(vz).T - 2: J(vz).Imit = vz: J(vz).Cur = 0
        If J(vz).Poder = pMago Then J(vz).ManaF� = J(vz).ManaF� - Mana("Imitar")
        Call Cursor(vz): Call ReEscreve
    Else
        If J(vz).Poder = pMago Then MsgMuda ("Voc� j� est� na forma normal !")
        If J(vz).Poder = pT_1000 Then MsgMuda ("Voc� j� est� na forma de policial !")
    End If
End If
End Sub

Private Sub TrocarCorpo_Click()
Dim mens$, a, resp$

mens$ = "Qual amigo voc� quer substituir ?"
For a = 1 + M To NMons + M
    If Not J(a).Morte And (J(a).Time = 0 Or J(a).Time = vz) Then mens$ = mens$ + Chr$(13) + Trim(a) + " - " + ArrumaNome(J(a).Nome) + " (" + Trim(J(a).X) + ", " + Trim(J(a).Y) + ", " + Trim(J(a).Z + 1) + ")"
Next
resp$ = InputBox(mens$)
For a = 1 + M To NMons + M
    If Val(resp$) = a And Not J(a).Morte And (J(a).Time = 0 Or J(a).Time = vz) Then 'troca
        Call TrocaCorpo(vz, CByte(a))
        
        Foto = Foto + 1
        J(vz).RepA��oOQue(Foto) = 10
        J(vz).RepA��oQ1(Foto) = J(vz).X
        J(vz).RepA��oQ2(Foto) = J(vz).Y
        J(vz).RepA��oQ3(Foto) = J(vz).Z
        J(vz).RepA��oQ4(Foto) = J(vz).Vis�o
        J(vz).T = J(vz).T - 3
        If J(a).Z <> J(vz).Z Then Call ReDesenha
        Call ReEscreve
        Call Cursor(vz): Mapa.Borda.Refresh
        Call TestaEncontro
    End If
Next
End Sub

Sub TrocaCorpo(Ag As Byte, Mo As Byte)
Dim a

J(Mo).Nome = "Agente": TipoMons(Mo) = 2
J(Mo).Ht = 7: J(Mo).HtMax = 7: J(Mo).Reg = 1.1: J(Mo).RegNatural = 1.1: J(Mo).Blindagem = 0
J(Mo).TMax = 5.5: J(Mo).Mira = 1.2
J(Mo).Olho = 1.15: J(Mo).Camufl = 1.15
J(Mo).ArmaM�o = 5 'Rifle
J(Mo).MonViuRep = False
For a = 1 To NJogs
    J(Mo).ViuRepX(a) = 0
    J(Mo).ViuRepY(a) = 0
Next
Troca J(Mo).X, J(Ag).X
Troca J(Mo).Y, J(Ag).Y
Troca J(Mo).Z, J(Ag).Z
J(Ag).Ht = J(Ag).HtMax: J(Ag).Reg = J(Ag).RegNatural
J(Ag).Parasitado = False
End Sub

Private Sub BuracoZerg_Click()
Dim a

If J(vz).Enterrado = True Then 'Desenterrando
    For a = 1 To NJogs
        If a <> vz And J(vz).X = J(a).X And J(vz).Y = J(a).Y And J(vz).Z = J(a).Z Then
            MsgMuda ("H� algu�m em cima de voc� !!")
            Exit Sub
        End If
    Next
    J(vz).Enterrado = False
    J(vz).T = J(vz).T - 2.5
    J(vz).Blindagem = 0
    J(vz).Reg = J(vz).RegNatural 'Enquanto est� enterrado a regenera��o fica maior (ver PassaVez)
    If J(vz).Z > 0 Then Conte�do(J(vz).X, J(vz).Y, J(vz).Z) = 25 'Buraco Inv.
    Call TestaEncontro
ElseIf J(vz).Enterrado = False Then 'Enterrando
    For a = 1 To NJogs
        If a <> vz And J(vz).X = J(a).X And J(vz).Y = J(a).Y And J(vz).Z = J(a).Z And J(a).Enterrado = True Then
            MsgMuda ("H� algu�m enterrado a� !!")
            Exit Sub
        End If
    Next
    J(vz).Enterrado = True
    J(vz).Rastejando = False
    J(vz).Blindagem = 2
    J(vz).T = J(vz).T - 3
    Call TestaEncontro
End If
Call Cursor(vz)
Call ReEscreve
End Sub

Private Sub Fuma�a_Click()
Call Explos�o(J(vz).X, J(vz).Y, 0, 0, J(vz).Z, 0, 4, 3)
J(vz).T = J(vz).T - 1
J(vz).Fuma� = J(vz).Fuma� - 1
Call Cursor(vz)
Call TestaEncontro
Call ReEscreve
End Sub

Private Sub DesarmarMinas_Click()
Dim resp$

resp = MsgMuda("Quer tentar desarmar alguma poss�vel mina" + Chr$(13) + "que esteja a sua frente ?", vbYesNo)
If resp = vbYes Then
    Select Case J(vz).Vis�o
    Case 0: dx = 1: dy = 0
    Case 1: dx = 1: dy = 1
    Case 2: dx = 0: dy = 1
    Case 3: dx = -1: dy = 1
    Case 4: dx = -1: dy = 0
    Case 5: dx = -1: dy = -1
    Case 6: dx = 0: dy = -1
    Case 7: dx = 1: dy = -1
    End Select
    If Conte�do(J(vz).X + dx, J(vz).Y + dy, J(vz).Z) = 6 Then
        If Rnd < 1 / 2 Then
            MsgMuda ("Havia uma mina ali, e voc� a desarmou !!")
            Conte�do(J(vz).X + dx, J(vz).Y + dy, J(vz).Z) = 0
        Else
            MsgMuda ("H� uma mina ali, mas voc� n�o consegui desarm�-la !")
        End If
    Else
        MsgMuda ("N�o h� nenhuma mina ali.")
    End If
    J(vz).T = J(vz).T - 2
    Call ReEscreve
End If
End Sub

Private Sub ControlePsi�nico_Click()
Comando = 2 'Controlar = True
Mapa.Borda.MousePointer = 2
Call ReEscreve
End Sub

Private Sub Cura_Click()
If J(vz).ManaF� < F�("Cura") Then
    MsgMuda ("Voc� precisa de " + Trim(F�("Cura")) + " de f� para se curar.")
    Exit Sub
End If
J(vz).T = J(vz).T - 2
J(vz).ManaF� = J(vz).ManaF� - F�("Cura")
MsgMuda ("Voc� curou-se completamente.")
J(vz).Ht = J(vz).HtMax
J(vz).Reg = J(vz).RegNatural
Call ReEscreve
Call TestaEncontro
End Sub

Private Sub Medkits_Click()
If J(vz).T - 4 >= 0 And J(vz).Med > 0 Then
    J(vz).T = J(vz).T - 4
    J(vz).Med = J(vz).Med - 1
    MsgMuda ("Voc� usou um kit-m�dico.")
    J(vz).Ht = J(vz).Ht + Aleat�rio(4)
    If J(vz).Ht > J(vz).HtMax Then J(vz).Ht = J(vz).HtMax
    If J(vz).Reg < 1 Then
        J(vz).Reg = J(vz).Reg + 0.2
        If J(vz).Reg > 1 Then J(vz).Reg = 1
    End If
    Call TestaEncontro
    Call ReEscreve
ElseIf J(vz).Med = 0 Then
    MsgMuda ("Voc� n�o tem medkits.")
ElseIf J(vz).T = J(vz).T - 4 >= 0 Then
    Call SemTempo("usar medkits.")
End If
End Sub

Private Sub NMinas_Click()
If J(vz).T - 3 >= 0 And J(vz).Minas > 0 Then
    J(vz).T = J(vz).T - 3
    J(vz).Minas = J(vz).Minas - 1
    Conte�do(J(vz).X, J(vz).Y, J(vz).Z) = 6
    Call ReEscreve
    MsgMuda ("Voc� armou uma mina.")
    Foto = Foto + 1
    J(vz).RepA��oOQue(Foto) = 8
    GravaQuemViu False
ElseIf J(vz).Minas = 0 Then
    MsgMuda ("Voc� n�o tem minas.")
ElseIf J(vz).T = J(vz).T - 3 >= 0 Then
    Call SemTempo("armar uma mina.")
End If

End Sub

Private Sub Holografia_Click()
Dim resp$

If J(vz).Poder = pMago And J(vz).ManaF� < Mana("Holografia") Then
    MsgMuda ("Voc� precisa de " + Trim(Mana("Holografia")) + " de mana para usar este poder.")
    Exit Sub
End If
If J(vz).ZHol = 255 Then 'A holografia est� desativada
    Comando = 4 'Holografar = True
    Mapa.Borda.MousePointer = 2
Else
    resp = MsgMuda("Voc� quer alterar a posi��o de sua holografia?", vbYesNo)
    If resp = vbYes Then
        Comando = 4 'Holografar = True
        Mapa.Borda.MousePointer = 2
    End If
End If
End Sub

Private Sub Bomba_Click()
Dim n As Byte
'If J(vz).XBomba(1) = 666 Then 'Desativada
    n = J(vz).BombasAtivadas + 1
    J(vz).XBomba(n) = J(vz).X
    J(vz).YBomba(n) = J(vz).Y
    J(vz).ZBomba(n) = J(vz).Z
    MsgMuda ("Voc� colocou uma bomba aqui.")
    J(vz).Bombas = J(vz).Bombas - 1
    J(vz).T = J(vz).T - 3
    Call TestaEncontro
    Call ReEscreve
    Foto = Foto + 1
    J(vz).RepA��oOQue(Foto) = 8
    GravaQuemViu True
'ElseIf J(vz).XBomba(1) < 666 Then 'Ativada
'    MsgMuda ("Voc� explodiu sua bomba !!")
'    Call Explos�o(J(vz).XBomba(1), J(vz).YBomba(1), 0, 0, J(vz).ZBomba(1), 20, 8, 1)
'    Call TestaMortes
'    If J(vz).Morte = True Then Call DueloMod.PassaVez
'    J(vz).XBomba(1) = 666 'Desativar
'    J(vz).T = J(vz).T - 0.5
'    Call ReEscreve
'End If
End Sub

Private Sub BombaExpl_Click()
Dim i As Byte
MsgMuda ("Voc� explodiu sua(s) bomba(s) !!")
For i = 1 To J(vz).BombasAtivadas
    Call Explos�o(J(vz).XBomba(i), J(vz).YBomba(i), 0, 0, J(vz).ZBomba(i), 20, 8, 1)
Next
Call TestaMortes
If J(vz).Morte = True Then Call DueloMod.PassaVez
J(vz).XBomba(1) = 666 'Desativar
J(vz).T = J(vz).T - 0.5
Call ReEscreve
End Sub

Private Sub Corpos_Click()
If J(vz).Prot > 0 Then
    If J(vz).Poder = pTarrasque Or J(vz).Poder = pDragao Then
        J(vz).T = J(vz).T - 1
    Else
        J(vz).T = J(vz).T - 5
    End If
    J(vz).Prot = J(vz).Prot - 1
    If J(vz).Ht < J(vz).HtMax / 4 Then
        J(vz).Ht = J(vz).HtMax / 4
    End If
    If J(vz).Poder = pPredador Then J(vz).Reg = J(vz).Reg + 0.5
    If J(vz).Poder = pWolverine Then J(vz).Reg = J(vz).Reg + 0.4
    If J(vz).Poder = pTarrasque Then J(vz).Reg = J(vz).Reg + 0.2
    If J(vz).Poder = pBalacau Or J(vz).Poder = pAlien Then J(vz).Reg = J(vz).Reg + 0.3
    If J(vz).Poder = pDragao Then J(vz).Reg = J(vz).Reg + 0.4
    MsgMuda ("Voc� ingeriu prote�nas.")
    Call TestaEncontro
    Call ReEscreve
ElseIf J(vz).Prot = 0 Then
    MsgMuda ("Voc� n�o tem prote�nas.")
End If
End Sub

Private Sub SniktBot�o_Click()
J(vz).Snikt = Not J(vz).Snikt
J(vz).T = J(vz).T - 1
Call ReEscreve
Call Cursor(vz)
End Sub

Private Sub Rel�mpago_Click()
If J(vz).ManaF� < F�("Rel�mpago") Then
    MsgMuda ("Voc� precisa de " + Trim(F�("Rel�mpago")) + " de f� para usar este poder.")
    Exit Sub
End If
Comando = 6 'rel�mpago
Mapa.Borda.MousePointer = 2
Call ReEscreve
End Sub

Private Sub AnimarMortos_Click()
If J(vz).ManaF� < F�("Animar") Then
    MsgMuda ("Voc� precisa de " + Trim(F�("Animar")) + " de f� para usar este poder.")
    Exit Sub
End If
Comando = 5 'animar mortos
Mapa.Borda.MousePointer = 2
Call ReEscreve
End Sub

Private Sub Rezar_Click()
J(vz).T = J(vz).T - 3.5
J(vz).ManaF� = J(vz).ManaF� - F�("Rezar")
If J(vz).ManaF� > 100 Then J(vz).ManaF� = 100
MsgMuda ("Am�m.")
Call ReEscreve
Call TestaEncontro
End Sub

Private Sub FimJogo_Click()
Dim Mensagem As String, Bot�es As VbMsgBoxStyle, T�tulo As String
Dim resposta As VbMsgBoxResult

Mensagem = "Voc� quer mesmo acabar com o jogo ?"
Bot�es = vbYesNo '+ vbExclamation
T�tulo = "Duelo"
resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
If resposta = vbYes Then End

End Sub

Private Sub Empatar_Click()
Dim Mensagem As String, Bot�es As VbMsgBoxStyle, T�tulo As String
Dim resposta As VbMsgBoxResult
Dim a

Mensagem = "Voc� quer mesmo considerar esta partida empatada ?"
Bot�es = vbYesNo '+ vbCritical
T�tulo = "Duelo"
resposta = MsgMuda(Mensagem, Bot�es, T�tulo)
If resposta = vbYes Then
    For a = 1 To NJogs
        J(a).Morte = True
    Next
    Mortes = NJogs
    Call TestaMortes
End If

End Sub

Private Sub mnuSalvarPartida_Click()
Call SalvaPartida
End Sub

Private Sub menOpcoes1_Click()
'Hide
frmOptions.Visible = True
End Sub

'Sub Form_KeyPress(key As Integer)
'If key = Chr$(0) + Chr$(59) Then
'   'Exibe o Help
'End If
'End Sub
'*******************************
'**                           **
'**  *   * ***** *     ****   **
'**  *   * *     *     *   *  **
'**  ***** ****  *     ****   **
'**  *   * *     *     *      **
'**  *   * ***** ***** *      **
'**                           **
'*******************************


Private Sub HelpPodMenu_Click()
Help.Visible = True
End Sub

Private Sub Rastejar_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then MsgMuda ("Gasta 2 de tempo.")
End Sub

Private Sub TiroIns_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Mg$
Mg$ = str$(TIns(J(vz).ArmaM�o)) + " - Tempo gasto."
Mg$ = Mg$ + Chr$(13) + str$(DIns(J(vz).ArmaM�o)) + " - Dist�ncia prov�vel de acerto."
If Button = 2 Then MsgMuda (Mg$)
End Sub

Private Sub TiroMir_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Mg$
Mg$ = str$(TMir(J(vz).ArmaM�o)) + " - Tempo gasto."
Mg$ = Mg$ + Chr$(13) + str$(DMir(J(vz).ArmaM�o)) + " - Dist�ncia prov�vel de acerto."
If Button = 2 Then MsgMuda (Mg$)
End Sub

Private Sub TiroRaj_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Mg$
Mg$ = str$(TAut(J(vz).ArmaM�o)) + " - Tempo gasto."
Mg$ = Mg$ + Chr$(13) + str$(DAut(J(vz).ArmaM�o)) + " - Dist�ncia prov�vel de acerto."
If Button = 2 Then MsgMuda (Mg$)
End Sub
