Attribute VB_Name = "HufExp"
Option Explicit
'************************
' Declarations
Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, _
    ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, _
    ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, _
    ByVal ySrc As Long, ByVal dwRop As Long) As Long
'************************
' User defined types
Private Type TreeNode
    Weight As Integer
    SavedWeight As Integer
    Child0 As Integer
    Child1 As Integer
End Type
Private Type BitFile
    Mask As Byte
    Rack As Byte
    ByteCount As Long      ' Unused, but could be used to indicate progress.
End Type
'**************************
' Constants
'Public Const SRCCOPY = &HCC0020
Private Const SRCCOPY = &HCC0020
Private Const EndOfStream = 256
Private Const EndWeightStream = &HFFF
'***************************
' Initializations
Dim Nodes(514) As TreeNode
Dim Bitio As BitFile

' The Function of this routine is to read the Node Weights from the
' compressed file.  EndWeightStream indicates all Weights have been
' read in.
'
Private Function InputCounts(hFileIn As Integer) As Integer
    Dim FirstNode As Integer
    Dim LastNode As Integer
    Dim Talley As Integer
    Dim w As Integer            ' Count (Weight)
    Dim i As Integer            ' Index
    
    If EOF(hFileIn) Then
        MsgBox "Fatal error reading counts", 48
        InputCounts = 0
        Exit Function
    Else
        Get #hFileIn, , FirstNode
        Talley = Talley + 2
    End If
    If EOF(hFileIn) Then
        MsgBox "Fatal error reading counts", 48
        InputCounts = 0
        Exit Function
    Else
        Get #hFileIn, , LastNode
        Talley = Talley + 2
    End If
    Do While 1
        For i = FirstNode To LastNode
            If EOF(hFileIn) Then
                MsgBox "Fatal error reading counts", 48
                InputCounts = 0
                Exit Function
            Else
                Get #hFileIn, , w
                Talley = Talley + 2
                Nodes(i).Weight = w
            End If
        Next
        If EOF(hFileIn) Then
            MsgBox "Fatal error reading counts", 48
            InputCounts = 0
            Exit Function
        Else
            Get #hFileIn, , FirstNode
            Talley = Talley + 2
        End If
        If FirstNode = EndWeightStream Then
            Exit Do
        End If
        If EOF(hFileIn) Then
            MsgBox "Fatal error reading counts", 48
            InputCounts = 0
            Exit Function
        Else
            Get #hFileIn, , LastNode
            Talley = Talley + 2
        End If
    Loop
    Nodes(EndOfStream).Weight = 1
    InputCounts = Talley
End Function

' The function of this routine is to build the decoding tree based on
' the InputCounts of the compressed file.
'
Private Function BuildTree() As Integer
    Dim iNextFree As Integer
    Dim i As Integer
    Dim Min1 As Integer
    Dim Min2 As Integer
    
    Nodes(513).Weight = &H7FFF
    For iNextFree = EndOfStream + 1 To 514
        Min1 = 513
        Min2 = 513
        For i = 0 To iNextFree - 1
            If Nodes(i).Weight <> 0 Then
                If Nodes(i).Weight < Nodes(Min1).Weight Then
                    Min2 = Min1
                    Min1 = i
                ElseIf Nodes(i).Weight < Nodes(Min2).Weight Then
                    Min2 = i
                End If
            End If
        Next
        If Min2 = 513 Then
            Exit For
        End If
        Nodes(iNextFree).Weight = Nodes(Min1).Weight + Nodes(Min2).Weight
        Nodes(Min1).SavedWeight = Nodes(Min1).Weight
        Nodes(Min1).Weight = 0
        Nodes(Min2).SavedWeight = Nodes(Min2).Weight
        Nodes(Min2).Weight = 0
        Nodes(iNextFree).Child0 = Min1
        Nodes(iNextFree).Child1 = Min2
    Next
    iNextFree = iNextFree - 1
    Nodes(iNextFree).SavedWeight = Nodes(iNextFree).Weight
    BuildTree = iNextFree
End Function

' The function of this routine is to walk the tree to a Leaf Node and then
' save the Leaf to the output File.
'
Private Sub ExpandData(hFileIn As Integer, hFileOut As Integer, _
    iRootNode As Integer, ProgressStep As Long)
    Dim node As Integer
    Dim Leaf As Byte
    Dim R As Boolean        ' 1 bit = True
    Dim i As Integer, j As Integer
    Dim Work As Single
    Dim Task As String
    
    
    'frmExpand!picVisible.Visible = True
    Task = "Expanding file "
    Bitio.Mask = &H80
    Do While 1
        node = iRootNode
        Do
            R = InputBit(hFileIn)
            If R = True Then
                node = Nodes(node).Child1
            Else
                node = Nodes(node).Child0
            End If
        Loop While node > EndOfStream
        If node = EndOfStream Then
            Exit Do
        Else
            Leaf = CByte(node)
            Put #hFileOut, , Leaf
            i = i + 1
            If i = ProgressStep Then
                Work = j / 100
                i = 0
                'ProgressDisplay Work, Task
                j = j + 1
                DoEvents
            End If
        End If
    Loop
    'frmExpand!picVisible.Visible = False
End Sub


' The function of this routine is to read the Huffman code bits and send
' them a bit at a time to theExpandData routine.
'
Private Function InputBit(hFileIn As Integer) As Boolean
    Dim Value As Integer
    
    If Bitio.Mask = &H80 Then
        Get #hFileIn, , Bitio.Rack
    End If
    Value = Bitio.Rack And Bitio.Mask
    Bitio.Mask = Bitio.Mask / 2
    If Bitio.Mask = 0 Then
        Bitio.Mask = &H80
    End If
    InputBit = IIf(Value <> 0, True, False)
End Function

'Public Sub ProgressDisplay(Work As Single, Task As String)
'    Dim OldScaleMode As Integer
'    Dim R As Long
'    Dim Msg As String
'
'    Msg = Task & Format(Work, "0%") & " Complete"
'    frmExpand!picVisible.Cls
'    frmExpand!picInvisible.Cls
'
'    frmExpand!picVisible.CurrentX = _
'        (frmExpand!picVisible.Width - frmExpand!picVisible.TextWidth(Msg)) / 2
'    frmExpand!picInvisible.CurrentX = frmExpand!picVisible.CurrentX
'
'    frmExpand!picVisible.CurrentY = _
'        (frmExpand!picVisible.Height - frmExpand!picVisible.TextHeight(Msg)) / 2
'    frmExpand!picInvisible.CurrentY = frmExpand!picVisible.CurrentY
'
'    frmExpand!picVisible.Print Msg
'    frmExpand!picInvisible.Print Msg
'
'    OldScaleMode = frmExpand!picVisible.Parent.ScaleMode
'    frmExpand!picVisible.Parent.ScaleMode = 3
'
'    R = BitBlt(frmExpand!picVisible.hDC, 0, 0, _
'        frmExpand!picInvisible.Width * Work, _
'        frmExpand!picInvisible.Height, _
'        frmExpand!picInvisible.hDC, 0, 0, SRCCOPY)
'
'    frmExpand!picVisible.Parent.ScaleMode = OldScaleMode
'End Sub


Public Function Expand(sPathIn As String, sPathOut As String) As Boolean
    Dim hFileIn As Integer
    Dim hFileOut As Integer
    'Dim sPathIn As String
    'Dim sPathOut As String
    Dim iRootNode As Integer
    Dim TreeCounts As Integer
    Dim ProgressStep As Single
    
    Expand = False
    'If FileExists(sPathOut) Then Kill sPathOut
    
    hFileIn = FreeFile
    'sPathIn = txtInputPath.Text
    Open sPathIn For Binary Access Read As #hFileIn
    If hFileIn = 0 Then
        MsgBox "Error opening input file", 48
        Exit Function
    End If
    'txtInFileLength = LOF(hFileIn)
    
    hFileOut = FreeFile
    'sPathOut = txtOutputPath.Text
    Open sPathOut For Binary Access Write As #hFileOut
    If hFileOut = 0 Then
        MsgBox "Error opening output file", 48
        Exit Function
    End If
    
    'picProgress.Cls
    'picProgress.Print "Reading Counts..."
    TreeCounts = InputCounts(hFileIn)
    If TreeCounts = 0 Then
        'picProgress.Cls
        'picProgress.Print "Fatal Error..Quiting..."
        Exit Function
    Else
        'ProgressStep = ((txtInFileLength * 1.6) - TreeCounts) / 100
        ProgressStep = ((FileLen(sPathIn) * 1.6) - TreeCounts) / 100
        ProgressStep = ProgressStep + 1
    End If
    'picProgress.Cls
    'picProgress.Print "Building Decoding Tree..."
    iRootNode = BuildTree
    'picProgress.Cls
    'picProgress.Print "Expanding Data..."
    ExpandData hFileIn, hFileOut, iRootNode, CLng(ProgressStep)
    'txtOutLength = LOF(hFileOut)
    Close
    'picProgress.Cls
    'picProgress.Print "Expansion Complete..."
    
    Expand = True
End Function

