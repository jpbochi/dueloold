Attribute VB_Name = "ArquivosMod"
Option Explicit
DefInt A-Z
Private Const strIDEst = "DUELOEST01"

Sub Carrega()
Dim n As Integer
Dim a, b, c As Integer
Open Arquivo$ For Input As #1
Line Input #1, cod$
Close #1

AndMax = Asc(Mid(cod$, 1, 1)) - 48
xM = Asc(Mid(cod$, 2, 1)) - 48
yM = Asc(Mid(cod$, 3, 1)) - 48
n = 4 'Esse � o primeiro d�gito a ser lido

For c = 0 To AndMax
    For a = 1 To xM
        For b = yM To 1 Step -1
            Conte�do(a, b, c) = Asc(Mid(cod$, n, 1)) - 48
            n = n + 1
        Next
    Next
Next
NMons = 0
If n < Len(cod$) Then
    Do
        NMons = NMons + 1
        TipoMons(NMons + M) = Asc(Mid(cod$, n, 1)) - 48
        J(NMons + M).X = Asc(Mid(cod$, n + 1, 1)) - 48
        J(NMons + M).Y = Asc(Mid(cod$, n + 2, 1)) - 48
        J(NMons + M).Z = Asc(Mid(cod$, n + 3, 1)) - 48
        n = n + 4
    Loop While n < Len(cod$)
End If
End Sub

Sub Salva()
Open Arquivo$ For Output As #1 Len = 255

Print #1, Chr$(48 + AndMax);
Print #1, Chr$(48 + xM);
Print #1, Chr$(48 + yM);

Dim a, b, c As Integer
For c = 0 To AndMax
    For a = 1 To xM
        For b = yM To 1 Step -1
            Print #1, Chr$(Conte�do(a, b, c) + 48);
        Next
    Next
Next
For a = 1 To NMons
    Print #1, Chr$(48 + TipoMons(a + M));
    Print #1, Chr$(48 + J(a + M).X);
    Print #1, Chr$(48 + J(a + M).Y);
    Print #1, Chr$(48 + J(a + M).Z);
Next

Close #1
End Sub

Sub CriaEstat�sticasNulas()
Dim a As Byte

Open ArqEstat$ For Output As #1 Len = 255
Print #1, strIDEst
Print #1, "05"
For a = 1 To 5
    Print #1, "Jogador " & a;
    Print #1, Chr$(13);: Print #1, Chr$(10);
    Print #1, "Nenhum";
    Print #1, Chr$(13);: Print #1, Chr$(10);
Next
For a = 1 To NPoderes 'Vit�rias
    Print #1, Chr$(48);
    Print #1, Chr$(48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);
For a = 1 To NPoderes 'Derrotas
    Print #1, Chr$(48);
    Print #1, Chr$(48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);
For a = 1 To NPoderes 'Kills
    Print #1, Chr$(48);
    Print #1, Chr$(48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);
For a = 1 To 5 'Vi�rias (jogadores)
    Print #1, Chr$(48);
    Print #1, Chr$(48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);
For a = 1 To 5 'Derrotas (jogadores)
    Print #1, Chr$(48);
    Print #1, Chr$(48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);
For a = 1 To 5 'Kills (jogadores)
    Print #1, Chr$(48);
    Print #1, Chr$(48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);
Print #1, Chr$(48) & Chr$(48); 'Total de partidas
Print #1, Chr$(13);: Print #1, Chr$(10);
Print #1, Chr$(48) & Chr$(48); 'Empates
Print #1, Chr$(13);: Print #1, Chr$(10);

Close #1

Exit Sub
DeuErro:
MsgMuda ("Arquivo inv�lido!")
Close #1
End Sub

Sub SalvaEstat�sticas(ByVal TimeVitorioso As Integer, MaisUm As Boolean, Empate As Boolean)
''On Error GoTo DeuErro
Dim n, dig1, dig2
Dim a As Byte, b As Byte
Dim NJogantes As Byte

'If MaisUm Then FileCopy ArqEstat$, DirDuelo + "backup.est"
If FileExists(ArqEstat$) Then FileCopy ArqEstat$, DirDuelo + "backup.est"

Open ArqEstat$ For Output As #1 Len = 255

NJogantes = Estat.lstJogadores.ListItems.Count

Print #1, strIDEst

Print #1, CStr(NJogantes)

For a = 1 To NJogantes
    'Print #1, Estat.JogadorNome(A).Text;
    Print #1, Estat.lstJogadores.ListItems(a).Text;
    Print #1, Chr$(13);: Print #1, Chr$(10);
    'Print #1, Estat.comArqPref(a).Caption;
    Print #1, Estat.lstJogadores.ListItems(a).SubItems(8);
    Print #1, Chr$(13);: Print #1, Chr$(10);
Next

For a = 1 To NPoderes 'Vit�rias
    'n = Val(Estat.Vit�rias(A).Caption)
    n = Val(Estat.lstPoderes.ListItems(a).SubItems(1))
    If a = pCapangas Then n = n * 2
    'If PoderBytStr(J(Vitorioso).Poder) = Estat.PoderNome(A).Caption Then n = n + 1
    If MaisUm Then
        For b = 1 To NJogs
            If J(b).Time = TimeVitorioso And J(b).Poder = a Then
                n = n + 1
            End If
        Next
    End If
    If a = pCapangas Then n = n / 2
    dig1 = Int(n / 200)
    dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
    Print #1, Chr$(dig1 + 48);
    Print #1, Chr$(dig2 + 48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);

For a = 1 To NPoderes 'Derrotas
    'n = Val(Estat.Derrotas(A).Caption)
    n = Val(Estat.lstPoderes.ListItems(a).SubItems(2))
    If a = pCapangas Then n = n * 2
    If MaisUm Then
        For b = 1 To NJogs
            If J(b).Poder = a And J(b).Time <> TimeVitorioso Then
                n = n + 1
            End If
        Next
    End If
    If a = pCapangas Then n = n / 2
    dig1 = Int(n / 200)
    dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
    Print #1, Chr$(dig1 + 48);
    Print #1, Chr$(dig2 + 48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);

For a = 1 To NPoderes 'Kills
    'n = Val(Estat.KillsPod(A).Caption)
    n = Val(Estat.lstPoderes.ListItems(a).SubItems(6))
    For b = 1 To NJogs
        'If PoderBytStr(J(B).Poder) = Estat.PoderNome(A).Caption Then n = n + J(B).Kills
        If MaisUm And J(b).Poder = a Then n = n + J(b).Kills
    Next
    dig1 = Int(n / 200)
    dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
    Print #1, Chr$(dig1 + 48);
    Print #1, Chr$(dig2 + 48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);

For a = 1 To NJogantes 'Vit�rias (jogadores)
    'n = Val(Estat.Vit�riasJog(A).Caption)
    n = Val(Estat.lstPoderes.ListItems(a).SubItems(1))
    For b = 1 To NJogs
        'If MaisUm And J(B).Nome = Estat.JogadorNome(A).Text And B = Vitorioso Then n = n + 1
        If MaisUm Then
            If ArrumaNome(J(b).Nome) = Estat.lstJogadores.ListItems(a).Text And J(b).Time = TimeVitorioso Then
                n = n + 1
                Exit For '� necess�rio por causa dos capangas
            End If
        End If
    Next
    dig1 = Int(n / 200)
    dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
    Print #1, Chr$(dig1 + 48);
    Print #1, Chr$(dig2 + 48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);

For a = 1 To NJogantes 'Derrotas (jogadores)
    'n = Val(Estat.DerrotasJog(A).Caption)
    n = Val(Estat.lstPoderes.ListItems(a).SubItems(2))
    For b = 1 To NJogs
        'If MaisUm And J(B).Nome = Estat.JogadorNome(A).Text And B <> Vitorioso Then n = n + 1
        If MaisUm Then
            If ArrumaNome(J(b).Nome) = Estat.lstJogadores.ListItems(a).Text And J(b).Time <> TimeVitorioso Then
                n = n + 1
                Exit For 'por causa dos capangas
            End If
        End If
    Next
    dig1 = Int(n / 200)
    dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
    Print #1, Chr$(dig1 + 48);
    Print #1, Chr$(dig2 + 48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);

For a = 1 To NJogantes 'Kills (jogadores)
    'n = Val(Estat.KillsJog(A).Caption)
    n = Val(Estat.lstPoderes.ListItems(a).SubItems(5))
    For b = 1 To NJogs
        'If MaisUm And J(B).Nome = Estat.JogadorNome(A).Text Then n = n + J(B).Kills
        If MaisUm Then
            If ArrumaNome(J(b).Nome) = Estat.lstJogadores.ListItems(a).Text Then
                n = n + J(b).Kills
            End If
        End If
    Next
    dig1 = Int(n / 200)
    dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
    Print #1, Chr$(dig1 + 48);
    Print #1, Chr$(dig2 + 48);
Next
Print #1, Chr$(13);: Print #1, Chr$(10);
n = Val(Estat.TotalPartidas.Caption)
If MaisUm Then n = n + 1 'Conta uma partida
'Print #1, Chr$(n + 48);
dig1 = Int(n / 200)
dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
Print #1, Chr$(dig1 + 48);
Print #1, Chr$(dig2 + 48);

n = Val(Estat.TotalEmpates.Caption)
If MaisUm And Empate Then n = n + 1 'Conta um empate
dig1 = Int(n / 200)
dig2 = 200 * (n / 200 - Int(n / 200)) 'Resto da divis�o
Print #1, Chr$(dig1 + 48);
Print #1, Chr$(dig2 + 48);

Print #1, Chr$(13);: Print #1, Chr$(10);

Close #1

Exit Sub
DeuErro:
MsgMuda ("Arquivo inv�lido!")
Close #1
End Sub

Sub CarregaEstat�sticas()
'Dim Potencia(NPoderes)
Dim n
Dim a As Byte, b As Byte
Dim ID$
Dim VitP$, DerP$, KilP$, VitJ$, DerJ$, KilJ$, Total$, Empates$
Dim PotenciaJ(32) As Single, NomeJ$(1 To 32), ArqPrefJ$(1 To 32)
Dim lstItmPod(1 To NPoderes) As ListItem
Dim strNJog$, NJogantes As Integer

''On Error GoTo DeuErro

If Not FileExists(ArqEstat$) Then
    CriaEstat�sticasNulas
End If

On Error Resume Next
Open ArqEstat$ For Input As #1
Line Input #1, ID$
If ID$ <> strIDEst Then
    Close #1
    MsgBox "A vers�o do arquivo '" & ArqEstat$ & "' n�o est� correta."
    ArqEstat$ = DirDuelo & "nulo.est"
    CriaEstat�sticasNulas
    CarregaEstat�sticas
    Exit Sub
End If
Line Input #1, strNJog$: NJogantes = Val(strNJog$)
For a = 1 To NJogantes '5
    Line Input #1, NomeJ$(a)
    Line Input #1, ArqPrefJ$(a)
Next
Line Input #1, VitP$
Line Input #1, DerP$
Line Input #1, KilP$
Line Input #1, VitJ$
Line Input #1, DerJ$
Line Input #1, KilJ$
Line Input #1, Total$
Line Input #1, Empates$
Close #1
If Err.Number <> 0 Then
    MsgBox "A vers�o do arquivo '" & ArqEstat$ & "' n�o est� correta."
    ArqEstat$ = DirDuelo & "nulo.est"
    CriaEstat�sticasNulas
    CarregaEstat�sticas
    Exit Sub
End If

On Error GoTo DeuErro

Estat.Caption = TiraPath(ArqEstat$)

'zera listas
Do While Estat.lstPoderes.ListItems.Count > 0
    Estat.lstPoderes.ListItems.Remove Estat.lstPoderes.ListItems.Count
Loop
Do While Estat.lstJogadores.ListItems.Count > 0
    Estat.lstJogadores.ListItems.Remove Estat.lstJogadores.ListItems.Count
Loop

'Estat.TotalPartidas.Caption = Val(Asc(Total$)) - 48
n = (Val(Asc(Mid(Total$, 1, 1))) - 48) * 200 '1� d�gito
n = n + Val(Asc(Mid(Total$, 2, 1))) - 48 '2� d�gito
Estat.TotalPartidas.Caption = n

n = (Val(Asc(Mid(Empates$, 1, 1))) - 48) * 200 '1� d�gito
n = n + Val(Asc(Mid(Empates$, 2, 1))) - 48 '2� d�gito
Estat.TotalEmpates.Caption = n

For a = 1 To NJogantes '5
    'Estat.JogadorNome(A).Text = NomeJ$(A)
    Estat.lstJogadores.ListItems.Add , , NomeJ$(a)
    'Estat.comArqPref(A).Caption = ArqPrefJ$(A)
    Estat.lstJogadores.ListItems(a).SubItems(8) = ArqPrefJ$(a)
Next

'********************PODERES
For a = 1 To NPoderes
    Set lstItmPod(a) = Estat.lstPoderes.ListItems.Add(, , PoderBytStr(CByte(a)))
Next

a = 0
b = 0
Do 'vit�rias
    a = a + 1
    If a / 2 <> Int(a / 2) Then 'impar
        n = (Val(Asc(Mid(VitP$, a, 1))) - 48) * 200 '1� d�gito
    Else
        b = b + 1
        n = n + Val(Asc(Mid(VitP$, a, 1))) - 48 '2� d�gito
        'Estat.Vit�rias(B).Caption = n
        lstItmPod(b).SubItems(1) = n
    End If
Loop While b < NPoderes

a = 0
b = 0
Do 'derrotas
    a = a + 1
    If a / 2 <> Int(a / 2) Then 'impar
        n = (Val(Asc(Mid(DerP$, a, 1))) - 48) * 200 '1� d�gito
    Else
        b = b + 1
        n = n + Val(Asc(Mid(DerP$, a, 1))) - 48 '2� d�gito
        'Estat.Derrotas(B).Caption = n
        lstItmPod(b).SubItems(2) = n
    End If
Loop While b < NPoderes

a = 0
b = 0
Do 'kills dos poderes
    a = a + 1
    If a / 2 <> Int(a / 2) Then 'impar
        n = (Val(Asc(Mid(KilP$, a, 1))) - 48) * 200 '1� d�gito
    Else
        b = b + 1
        n = n + Val(Asc(Mid(KilP$, a, 1))) - 48 '2� d�gito
        'Estat.KillsPod(B).Caption = n
        lstItmPod(b).SubItems(5) = n
    End If
Loop While b < NPoderes

For a = 1 To NPoderes 'total
    'Estat.TotalPoderes(A).Caption = Val(Estat.Vit�rias(A).Caption) + Val(Estat.Derrotas(A).Caption)
    lstItmPod(a).SubItems(3) = Val(lstItmPod(a).SubItems(1)) + Val(lstItmPod(a).SubItems(2))
    'If Estat.TotalPoderes(A).Caption = "0" Then
    '    Estat.Vit�rias(A).Caption = ""
    '    Estat.Derrotas(A).Caption = ""
    '    Estat.KillsPod(A).Caption = ""
    '    Estat.TotalPoderes(A).Caption = ""
    'End If
Next
For a = 1 To NPoderes 'aproveitamento (poderes)
    'If Val(Estat.TotalPoderes(A).Caption) > 0 Then
    If Val(lstItmPod(a).SubItems(3)) > 0 Then
        'Estat.AprovPod(A).Caption = Format(Val(Estat.Vit�rias(A).Caption) / Val(Estat.TotalPoderes(A).Caption), "##0.0%")
        lstItmPod(a).SubItems(4) = Format(Val(lstItmPod(a).SubItems(1)) / Val(lstItmPod(a).SubItems(3)), Estat.lstPoderes.ColumnHeaders(4 + 1).Tag)
        'Pot�ncia(A) = Val(Estat.Vit�rias(A).Caption) / Val(Estat.TotalPoderes(A).Caption)
        Pot�ncia(a) = Val(lstItmPod(a).SubItems(1)) / Val(lstItmPod(a).SubItems(3))
    Else
        'Estat.AprovPod(A).Caption = ""
    End If
Next

Open DirDuelo + "potencia.pot" For Random As #1
For a = 1 To NPoderes 'kills p/ partida (poderes)
    'If Val(Estat.TotalPoderes(A).Caption) > 0 Then
    If Val(lstItmPod(a).SubItems(3)) > 0 Then
        'Estat.KillsPPPod(A).Caption = Format(Val(Estat.KillsPod(A).Caption) / Val(Estat.TotalPoderes(A).Caption), "0.00")
        lstItmPod(a).SubItems(6) = Format(Val(lstItmPod(a).SubItems(5)) / Val(lstItmPod(a).SubItems(3)), Estat.lstPoderes.ColumnHeaders(6 + 1).Tag)
        'Pot�ncia(A) = Pot�ncia(A) + Val(Estat.KillsPod(A).Caption) / Val(Estat.TotalPoderes(A).Caption)
        Pot�ncia(a) = Pot�ncia(a) + Val(lstItmPod(a).SubItems(5)) / Val(lstItmPod(a).SubItems(3))
    Else
        'Estat.KillsPPPod(A).Caption = ""
        'Pot�ncia(a) = 1 'quando nunca jogou recebe o valor 1
        Get #1, a, Pot�ncia(a)
    End If
    lstItmPod(a).SubItems(7) = Format(Pot�ncia(a), Estat.lstPoderes.ColumnHeaders(7 + 1).Tag)
Next
Close #1

Estat.lstPoderes.SortKey = 7

'********************Jogadores
a = 0
b = 0
Do 'vit�rias
    a = a + 1
    If a / 2 <> Int(a / 2) Then 'impar
        n = (Val(Asc(Mid(VitJ$, a, 1))) - 48) * 200 '1� d�gito
    Else
        b = b + 1
        n = n + Val(Asc(Mid(VitJ$, a, 1))) - 48 '2� d�gito
        'Estat.Vit�riasJog(B).Caption = n
        Estat.lstJogadores.ListItems(b).SubItems(1) = n
    End If
Loop While b < NJogantes '5

a = 0
b = 0
Do 'derrotas
    a = a + 1
    If a / 2 <> Int(a / 2) Then 'impar
        n = (Val(Asc(Mid(DerJ$, a, 1))) - 48) * 200 '1� d�gito
    Else
        b = b + 1
        n = n + Val(Asc(Mid(DerJ$, a, 1))) - 48 '2� d�gito
        'Estat.DerrotasJog(B).Caption = n
        Estat.lstJogadores.ListItems(b).SubItems(2) = n
    End If
Loop While b < NJogantes '5

a = 0
b = 0
Do 'kills
    a = a + 1
    If a / 2 <> Int(a / 2) Then 'impar
        n = (Val(Asc(Mid(KilJ$, a, 1))) - 48) * 200 '1� d�gito
    Else
        b = b + 1
        n = n + Val(Asc(Mid(KilJ$, a, 1))) - 48 '2� d�gito
        'Estat.KillsJog(B).Caption = n
        Estat.lstJogadores.ListItems(b).SubItems(5) = n
    End If
Loop While b < NJogantes '5

For a = 1 To NJogantes '5 'total
    'Estat.TotalJog(A).Caption = Val(Estat.Vit�riasJog(A).Caption) + Val(Estat.DerrotasJog(A).Caption)
    Estat.lstJogadores.ListItems(a).SubItems(3) = Val(Estat.lstJogadores.ListItems(a).SubItems(1)) + Val(Estat.lstJogadores.ListItems(a).SubItems(2))
Next
'Estat.TotalVit�rias.Caption = "0"
'For a = 1 To NJogantes '5 'total de vit�rias de todos os jogadores
'    'Estat.TotalVit�rias.Caption = Val(Estat.TotalVit�rias.Caption) + Val(Estat.Vit�riasJog(A).Caption)
'    Estat.TotalVit�rias.Caption = Val(Estat.TotalVit�rias.Caption) + Val(Estat.lstJogadores.ListItems(a).SubItems(1))
'Next

For a = 1 To NJogantes '5 'aproveitamento (jogadores)
    With Estat.lstJogadores
        'If Val(Estat.TotalJog(A).Caption) > 0 Then
        If Val(.ListItems(a).SubItems(3)) > 0 Then
            'Estat.AprovJog(A).Caption = Format(Val(Estat.Vit�riasJog(A).Caption) / Val(Estat.TotalJog(A).Caption), "##0.0%")
            .ListItems(a).SubItems(4) = Format(Val(.ListItems(a).SubItems(1)) / Val(.ListItems(a).SubItems(3)), .ColumnHeaders(4 + 1).Tag)
            'PotenciaJ(A) = Val(Estat.Vit�riasJog(A).Caption) / Val(Estat.TotalJog(A).Caption)
            PotenciaJ(a) = Val(.ListItems(a).SubItems(1)) / Val(.ListItems(a).SubItems(3))
        Else
            'Estat.AprovJog(A).Caption = "--"
        End If
    End With
Next

For a = 1 To NJogantes '5 'kills p/ partida (jogadores)
    With Estat.lstJogadores
        'If Val(Estat.TotalJog(A).Caption) > 0 Then
        If Val(.ListItems(a).SubItems(3)) > 0 Then
            'Estat.KillsPPJog(A).Caption = Format(Val(Estat.KillsJog(A).Caption) / Val(Estat.TotalJog(A).Caption), "0.000")
            .ListItems(a).SubItems(6) = Format(Val(.ListItems(a).SubItems(5)) / Val(.ListItems(a).SubItems(3)), .ColumnHeaders(6 + 1).Tag)
            'PotenciaJ(A) = PotenciaJ(A) + Val(Estat.KillsJog(A).Caption) / Val(Estat.TotalJog(A).Caption)
            PotenciaJ(a) = PotenciaJ(a) + Val(.ListItems(a).SubItems(5)) / Val(.ListItems(a).SubItems(3))
        Else
            'Estat.KillsPPJog(A).Caption = "--"
        End If
        'Estat.PotenciaJog(A).Caption = Format(PotenciaJ(A), "0.000")
        .ListItems(a).SubItems(7) = Format(PotenciaJ(a), .ColumnHeaders(7 + 1).Tag)
    End With
Next

Exit Sub
DeuErro:
MsgMuda ("Arquivo inv�lido!")
End Sub

Public Sub SalvaPartida()
Dim i, a, b, c
Arquivo$ = "teste.sav"
Open Arquivo$ For Output As #1

'Dados gerais
Print #1, Chr$(48 + vz);
Print #1, Chr$(48 + NJogs);
Print #1, Chr$(48 + NMons);
Print #1, vbCrLf 'Chr$(13);: Print #1, Chr$(10);

'Mapa
Print #1, Chr$(48 + AndMax);
Print #1, Chr$(48 + xM);
Print #1, Chr$(48 + yM);
For c = 0 To AndMax
    For a = 1 To xM
        For b = yM To 1 Step -1
            Print #1, Chr$(Conte�do(a, b, c) + 48);
        Next
    Next
Next
Print #1, vbCrLf 'Chr$(13);: Print #1, Chr$(10);
Close #1

Arquivo$ = "teste.jog"
Open Arquivo$ For Random As #2 Len = Len(J(1))

'Jogadores
For i = 1 To NJogs
    Put #2, i, J(i)
Next
'Monstros
For i = M To M + NMons
    Put #2, i, J(i)
Next

Close #2
End Sub

Public Sub CarregarPartida()
Dim i, a, b, c, n, cod$, tppx As Single

Arquivo$ = "teste.sav"
Open Arquivo$ For Input As #1

Line Input #1, cod$
vz = Asc(Mid(cod$, 1, 1)) - 48
NJogs = Asc(Mid(cod$, 2, 1)) - 48
NMons = Asc(Mid(cod$, 3, 1)) - 48

Line Input #1, cod$
AndMax = Asc(Mid(cod$, 1, 1)) - 48
xM = Asc(Mid(cod$, 2, 1)) - 48
yM = Asc(Mid(cod$, 3, 1)) - 48
n = 4 'Esse � o primeiro d�gito a ser lido
For c = 0 To AndMax
    For a = 1 To xM
        For b = yM To 1 Step -1
            Conte�do(a, b, c) = Asc(Mid(cod$, n, 1)) - 48
            n = n + 1
        Next
    Next
Next

Close #1

Arquivo$ = "teste.jog"
Open Arquivo$ For Random As #1 Len = Len(J(1))

For i = 1 To NJogs
    Get #1, i, J(i)
Next
For i = M To M + NMons
    Get #1, i, J(i)
Next

Close #1

Call ReDimensiona(Mapa)
Unload Editor
Unload Inicio
tppx = Screen.TwipsPerPixelX
Mapa.Width = (Mapa.VScroll.Left + Mapa.VScroll.Width) * tppx + 100

Inicio.Hide
Duelando.Show
Mapa.Show
Duelando.Visible = True
Mapa.Visible = True
frmMiniMapa.Visible = True
Unload Editor
Unload Jogadores
Unload NovoJogo
End Sub

