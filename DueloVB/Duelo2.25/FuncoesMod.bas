Attribute VB_Name = "FuncoesMod"
Option Explicit
DefInt A-Z
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Const flagMonstro = 1
Public Const flagEscurecido = 2
Public Const flagCarinha = 4

'Public Sub DesHex(intX As Variant, intY As Variant, Onde As Object, Obj As Byte, Monstro As Boolean, CorBorda As Long, Optional Escurecido As Variant)
Public Sub DesHex(intX As Variant, intY As Variant, Onde As Object, Obj As Byte, Flags As Byte, Optional CorBorda As Long)
'Dim MeioL As Double, Alt As Double, L As Double
Dim rX As Double, rY As Double
'Dim dimX As Integer, dimY As Integer
'Dim i As Single, j As Single
'Dim di As Double, dj As Double, pix As Long
Dim Desenho As Object ', stepe As Single
Dim CorPix As Long

Dim Monstro As Boolean, Escurecido As Boolean, Carinha As Boolean

'If IsMissing(Escurecido) Then Escurecido = False
If IsMissing(CorBorda) Then CorBorda = 0
Monstro = CBool(Flags And flagMonstro)
Escurecido = CBool(Flags And flagEscurecido)
Carinha = CBool(Flags And flagCarinha)

'Arruma o centro
CentroHex intX, intY, rX, rY

'Desenha Obstaculo
If Carinha Then
    Set Desenho = Mapa.Carinha(Obj)
    
    If Escurecido Then Onde.PaintPicture Mapa.Fog2.Picture, rX - L, rY - Alt, 2.1 * L, , , , , , vbSrcAnd
    Onde.PaintPicture Desenho.Picture, rX - L, rY - Alt, 2.1 * L, , , , , , vbSrcAnd
ElseIf Obj <> 0 And Obj <> 255 Then
    If Not Monstro Then
        Set Desenho = Mapa.ObjetoH(Obj)
        'stepe = 0.95
    Else
        Set Desenho = Mapa.Desmonstro(Obj)
        'stepe = 1
    End If
    'object.PaintPicture Picture, X1, Y1, width1, height1, X2, Y2, width2, height2, opcode
    If Escurecido Then Onde.PaintPicture Mapa.Fog2.Picture, rX - L, rY - Alt, 2.1 * L, , , , , , vbSrcAnd
    Onde.PaintPicture Desenho.Picture, rX - L, rY - Alt, 2.1 * L, , , , , , vbSrcAnd
    If CorBorda = 0 Then
        CorPix = Desenho.Point(Desenho.Width / 2, Desenho.Height / 2)
        Onde.Parent.MiniMapa.PSet (intX, intY), CorPix
        frmMiniMapa.MiniMapa.PSet (intX, intY), CorPix
    End If
    'dimX = Desenho.ScaleWidth - 1
    'dimY = Desenho.ScaleHeight - 1
    'For i = 0 To dimX Step stepe
    '    For j = 0 To dimY
    '        pix = Desenho.Point(i, j)
    '        di = ((i / dimX) * (2 * L)) + (rX - L)
    '        dj = (j / dimY) + (rY - Alt)
    '        'If Abs(pix - QBColor(15)) > 768 And pix <> 0 And pix <> -1 Then
    '        If pix <> QBColor(15) And pix <> -1 Then
    '            Onde.PSet (di, dj), pix
    '        End If
    '    Next
    'Next
ElseIf intX > xM Or intX < 1 Or intY > yM Or intY < 1 Or Obj = 255 Then
    'Fora do mapa ou Dentro do Fog
    If Obj = 255 Then
        Set Desenho = Mapa.Fog1
    Else
        Set Desenho = Mapa.Nada
    End If
    
    'Onde.PaintPicture Desenho.Picture, CSng(rX - L), CSng(rY - Alt), CSng(2.1 * L), , , , , , vbSrcAnd
    Onde.PaintPicture Desenho.Picture, rX - L, rY - Alt, 2.1 * L, , , , , , vbSrcAnd
    If CorBorda = 0 Then
        CorPix = Desenho.Point(Desenho.Width / 2, Desenho.Height / 2) 'QBColor(8)
        Onde.Parent.MiniMapa.PSet (intX, intY), CorPix
        frmMiniMapa.MiniMapa.PSet (intX, intY), CorPix
    End If
ElseIf Obj = 0 Then
    If CorBorda = 0 Then
        Onde.Parent.MiniMapa.PSet (intX, intY), QBColor(15)
        frmMiniMapa.MiniMapa.PSet (intX, intY), QBColor(15)
    End If
End If

'Desenha hexagono
Onde.PSet (rX - L, rY), CorBorda
Onde.Line -(rX - MeioL, rY + Alt), CorBorda
Onde.Line -(rX + MeioL, rY + Alt), CorBorda
Onde.Line -(rX + L, rY), CorBorda
Onde.Line -(rX + MeioL, rY - Alt), CorBorda
Onde.Line -(rX - MeioL, rY - Alt), CorBorda
Onde.Line -(rX - L, rY), CorBorda

End Sub

Public Sub ApagaHex(intX As Variant, intY As Variant, Onde As Object, Cor As Long, Escurecido As Boolean)
'Dim MeioL As Double, Alt As Double, L As Double, Tam As Double
Dim rX As Double, rY As Double
Dim dimX As Integer, dimY As Integer
Dim i As Single, J As Single
Dim di As Double, dj As Double, pix As Long

'Arruma o centro
CentroHex intX, intY, rX, rY

'Apaga objeto anterior
Onde.PaintPicture Mapa.Nada.Picture, rX - L, rY - Alt, 2.1 * L, , , , , , vbMergePaint
'MergePaint - Combines the inverted source bitmap with the destination bitmap by using Or
If Escurecido Then
    Onde.PaintPicture Mapa.Fog2.Picture, rX - L, rY - Alt, 2.1 * L, , , , , , vbSrcAnd
    'SrcAnd - Combines pixels of the destination and source bitmaps by using And
End If
Onde.Parent.MiniMapa.PSet (intX, intY), QBColor(15)
frmMiniMapa.MiniMapa.PSet (intX, intY), QBColor(15)
'dimX = Mapa.ObjetoH(1).ScaleWidth - 1
'dimY = Mapa.ObjetoH(1).ScaleHeight - 1
'For i = 0 To dimX Step 0.95
'    For j = 0 To dimY Step 1
'        pix = Mapa.Nada.Point(i, j)
'        di = ((i / dimX) * (2 * L)) + (rX - L)
'        dj = (j / dimY) + (rY - Alt)
'        If pix = 0 Then
'            Onde.PSet (di, dj), QBColor(15)
'        End If
'    Next
'Next

'Desenha hexagono
Onde.PSet (rX - L, rY), Cor
Onde.Line -(rX - MeioL, rY + Alt), Cor
Onde.Line -(rX + MeioL, rY + Alt), Cor
Onde.Line -(rX + L, rY), Cor
Onde.Line -(rX + MeioL, rY - Alt), Cor
Onde.Line -(rX - MeioL, rY - Alt), Cor
Onde.Line -(rX - L, rY), Cor

End Sub
 
Public Sub CentroHex(ByVal x1 As Integer, ByVal y1 As Integer, ByRef x2 As Variant, ByRef y2 As Variant)
If x1 Mod 2 = 1 Then
    y2 = y1 + Alt
Else
    y2 = y1
End If
x2 = x1 * cR3S2
End Sub

Public Sub HexClick(X As Single, Y As Single, XHex As Integer, YHex As Integer)
'provaveis coordenadas do hexagono clicado
Dim pX1 As Integer, pY1 As Single
Dim pX2 As Integer, pY2 As Single
Dim Dist1 As Single, Dist2 As Single

pX1 = Int(X / cR3S2)
pX2 = Int(X / cR3S2) + 1
pY1 = Int(2 * Y) / 2
pY2 = Int(2 * Y) / 2 + 1 / 2

If pX1 Mod 2 = 0 And pY1 = Int(pY1) Then
    ' px1 e' par E py1 e' normal
ElseIf Not (pX1 Mod 2 = 0) And Not (pY1 = Int(pY1)) Then
    ' nenhum dos dois: (px1 e' par E py1 e' normal)
Else
    Troca pY1, pY2
End If
Dist1 = Sqr((pX1 - (X / cR3S2)) ^ 2 + (pY1 - Y) ^ 2)
Dist2 = Sqr((pX2 - (X / cR3S2)) ^ 2 + (pY2 - Y) ^ 2)
If Dist1 <= Dist2 Then
    XHex = pX1: YHex = Int(pY1)
Else
    XHex = pX2: YHex = Int(pY2)
End If
'Borda.Refresh
'Borda.AutoRedraw = False
'Call DesHex(pX1, Int(pY1), QBColor(12))
'Call DesHex(pX2, Int(pY2), QBColor(12))
'Borda.Line (pX1 * cR3S2, 0)-(pX1 * cR3S2, 10), QBColor(1)
'Borda.Line (pX2 * cR3S2, 0)-(pX2 * cR3S2, 10), QBColor(1)
'Borda.Line (0, pY1)-(10, pY1), QBColor(1)
'Borda.Line (0, pY2)-(10, pY2), QBColor(1)
'Borda.AutoRedraw = True
End Sub

Sub PoderAciona()
Dim a As Byte, b As Byte, ChanceTot As Integer
Dim peso(NPoderes) As Single
Dim i, RandomNumber As Single
Dim c%, arq$, somapesos, n, sominha
'quando criar poderes, n�o esque�a:
'-aumentar o npoderes
'-inclu�-lo nas estat�sticas e nas prefer�ncias
'-inclu�-lo no help
For i = 1 To 5
    ChanceTot = ChanceTot + Config.ChanceClasse(i)
Next
a = 1
Do
    If J(a).Poder = pSorteado Then
        ' sorteia classe do poder
        RandomNumber = Rnd
        Select Case RandomNumber
            Case Is < Config.ChanceClasse(1) / ChanceTot
                c% = 1 'A
            Case Is < (Config.ChanceClasse(1) + Config.ChanceClasse(2)) / ChanceTot
                c% = 2 'B
            Case Is < (Config.ChanceClasse(1) + Config.ChanceClasse(2) + Config.ChanceClasse(3)) / ChanceTot
                c% = 3 'C
            Case Is < (Config.ChanceClasse(1) + Config.ChanceClasse(2) + Config.ChanceClasse(3) + Config.ChanceClasse(4)) / ChanceTot
                c% = 4 'D
            Case Else
                c% = 5 'E
        End Select

        'Select Case RandomNumber
        '    Case Is < 0.07: c% = 1 'A-7%
        '    Case Is < 0.28: c% = 2 'B-21%
        '    Case Is < 0.56: c% = 3 'C-28%
        '    Case Is < 0.86: c% = 4 'D-30%
        '    Case Else:      c% = 5 'E-14%
        'End Select
        
        ' carrega prefer�ncias do arquivo
        'Select Case J(a).Nome
        '    Case "Jairo": arq$ = "jairo.pre"
        '    Case "Jo�o Paulo": arq$ = "fau.pre"
        '    Case "Juarez": arq$ = "juarez.pre"
        '    Case "Josu�": arq$ = "josue.pre"
        '    Case "F�bio": arq$ = "fabio.pre"
        '    Case Else: arq$ = ""
        'End Select
        arq$ = ""
        For i = 1 To Estat.lstJogadores.ListItems.Count
            'If J(A).Nome = Estat.JogadorNome(i) Then
            If J(a).Nome = Estat.lstJogadores.ListItems(i).Text Then
                'arq$ = Estat.comArqPref(i).Caption
                arq$ = Estat.lstJogadores.ListItems(i).SubItems(8)
                Exit For
            End If
        Next
        If arq$ <> "" And arq$ <> "Nenhum" Then
            arq$ = DirDuelo + arq$
            Open arq$ For Input As #1
            Line Input #1, cod$
            Close #1
        End If
        somapesos = 0
        For n = 1 To NPoderes
            If Classe(n) <> c% Then
                peso(n) = 0
            ElseIf arq$ <> "" Then
                Select Case Mid(cod$, n, 1)
                    Case "P": peso(n) = 2 '3
                    Case "O": peso(n) = 1 / 2
                    Case "I": peso(n) = 1
                End Select
            Else
                peso(n) = 1
            End If
            somapesos = somapesos + peso(n)
        Next
        
        'sorteia poder
        RandomNumber = Rnd
        n = 0: sominha = 0
        Do
            n = n + 1
            sominha = sominha + peso(n)
        Loop While sominha <= somapesos * RandomNumber
        J(a).Poder = PoderStrByt(Pref.PoderNome(n).Caption)
        For b = 1 To a - 1
            If J(b).Poder = J(a).Poder And J(b).Poder = pCapangas Then
                J(a).Poder = pSorteado: Exit For
            End If
        Next
    End If
    
    If J(a).Poder <> pCapangas Then
        AcionaPoder J(a)
    Else
        If NJogs = M Then
            J(a).Poder = pSorteado: a = a - 1
        Else
            AcionaPoder J(a)
            NJogs = NJogs + 1
            b = M 'shift
            Do
                J(b) = J(b - 1)
                J(b).Imit = b
                J(b).Time = b
                b = b - 1
            Loop Until b < a + 2
            J(a + 1) = J(a)
            J(a + 1).Imit = a + 1
            J(a + 1).Time = a 'esta linha est� certa, n�o tem '+ 1'
            a = a + 1
        End If
    End If
    a = a + 1
Loop Until a > NJogs
End Sub

Public Function AcionaPoder(Jog As typJogador)
ZeraJogador Jog
With Jog
    ' define poderes do poder
    Select Case .Poder
    Case pPM  ' tudo normal
        .Ht = 7: .HtMax = 7
        .TMax = 6.25: .For�a = 15
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pAtirador_de_Elite
        .Ht = 7: .HtMax = 7
        .TMax = 6.25: .Mira = 2.5: .For�a = 15
        .Olho = 1.5: .OlhoNatural = 1.5
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pVelocista
        .Ht = 7: .HtMax = 7
        .TMax = 8.75: .For�a = 15
        .Camufl = 0.9: .CamuflNatural = 0.9
        .Olho = 0.9: .OlhoNatural = 0.9
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pCasca_Grossa
        .Ht = 12: .HtMax = 12: .Reg = 1.05: .RegNatural = 1.05
        .TMax = 6.25: .For�a = 18: .Blindagem = 0.25
        .Camufl = 0.95: .CamuflNatural = 0.95
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pTroll
        .Ht = 8: .HtMax = 8: .Reg = 1.5: .RegNatural = 1.5
        .TMax = 6.25: .For�a = 15
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pBoina_Verde
        .Ht = 8: .HtMax = 8: .Reg = 1.1: .RegNatural = 1.1
        .TMax = 6.75:  .Mira = 1.2: .For�a = 20
        .Camufl = 1.15: .CamuflNatural = 1.15
        .Olho = 1.15: .OlhoNatural = 1.15
        .MagicRes = 1.1: .MagicResNatural = 1.1
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pPredador
        .Ht = 14: .HtMax = 14
        .TMax = 8.75:  .Mira = 1.4 ': .For�a = 15
        .Olho = 2.5: .OlhoNatural = 2.5
        .Detec = True: .Olfato = 4
        .Mun(7) = 3: .ArmaP(7) = True        'Discos e tri-laser
        .ArmaM�o = 8: .Mun(8) = MunA(8): .ArmaP(8) = True
    Case pWolverine
        .Ht = 7: .HtMax = 7: .Reg = 1.15: .RegNatural = 1.15
        .TMax = 7.5: .For�a = 20
        .Olho = 1.15: .OlhoNatural = 1.15
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        .Snikt = False: .Olfato = 16
    Case pVoador
        .Ht = 8: .HtMax = 8
        .TMax = 7.5: .For�a = 18
        .Camufl = 0.8: .CamuflNatural = 0.8
        .MagicRes = 1.1: .MagicResNatural = 1.1
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pFantasma
        .Ht = 7: .HtMax = 7
        .TMax = 6.75: .For�a = 12
        .Camufl = 1.8: .CamuflNatural = 1.8
        .MagicRes = 1.7: .MagicResNatural = 1.7
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pTarrasque
        .Ht = 40: .HtMax = 60
        .TMax = 5: .Blindagem = 0.5 '.For�a = 150 ':  .Mira = 1
        .Camufl = 0.3: .CamuflNatural = 0.3
        .Olho = 1.6: .OlhoNatural = 1.6
        .MagicRes = 1.2: .MagicResNatural = 1.2
        .Olfato = 8
    Case pRobocop
        .Ht = 10: .HtMax = 10
        .TMax = 5.5: .Blindagem = 2.75 ': .For�a = 15
        .Camufl = 0.5: .CamuflNatural = 0.5
        .Olho = 0.9: .OlhoNatural = 0.9
        .MagicRes = 0.9: .MagicResNatural = 0.9
        .Detec = True
        .ArmaM�o = 12: .Mun(12) = MunA(12): .ArmaP(12) = True: .ArmaP(26) = True
    Case pHomem_do_Raio_X
        .Ht = 7: .HtMax = 7
        .TMax = 6.25: .For�a = 15
        .MagicRes = 1.2: .MagicResNatural = 1.2
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pBalacau
        .Ht = 10: .HtMax = 10
        .TMax = 12: '.For�a = 15 ':  .Mira = 1
        .MagicRes = 1.3: .MagicResNatural = 1.3
        .Olho = 0.5: .OlhoNatural = 0.5: .Olfato = 12
    Case pMosca
        .Ht = 12: .HtMax = 12
        .TMax = 6.25: .Mira = 0.8: .For�a = 30
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pDragao
        .Ht = 20: .HtMax = 20
        .TMax = 8:  .Mira = 1: .Blindagem = 0.5 ': .For�a = 15
        .Camufl = 0.5: .CamuflNatural = 0.5
        .Olho = 2: .OlhoNatural = 2: .Olfato = 16
        .MagicRes = 1.2: .MagicResNatural = 1.2
        .ArmaM�o = 14: .Mun(14) = MunA(14): .ArmaP(14) = True
    Case pMago     ' ex-teletransportador
        .Ht = 5: .HtMax = 5
        .TMax = 6.25: .Mira = 0.5: .For�a = 12
        .Camufl = 0.9: .CamuflNatural = 0.9
        .MagicRes = 1.4: .MagicResNatural = 1.4
        .ArmaM�o = 22: .ArmaP(22) = True 'Bola de Fogo
        .ArmaP(26) = True
        .ManaF� = 100
        .Invis = -1
    Case pEthereal
        .Ht = 6: .HtMax = 6
        .TMax = 7: .Mira = 0.6: .For�a = 12
        .Camufl = 1.3: .CamuflNatural = 1.3
        .MagicRes = 2: .MagicResNatural = 2
        .Olho = 4: .OlhoNatural = 4
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pJason
        .Ht = 15: .HtMax = 15
        .TMax = 12: '.For�a = 15 ':  .Mira = 1
        .Camufl = 0.8: .CamuflNatural = 0.8
        .Olho = 1.5: .OlhoNatural = 1.5: .Olfato = 4
        .MagicRes = 1.1: .MagicResNatural = 1.1
        .ArmaM�o = 17: .ArmaP(17) = True
    Case pAlien 'forma de 'aranha'
        .Ht = 3: .HtMax = 3
        .TMax = 8: '.For�a = 15 ':  .Mira = 1
        .Camufl = 2: .CamuflNatural = 2
        .Olho = 0.8: .OlhoNatural = 0.8: .Olfato = 4
        .Raio = 10
    Case pDemolidor
        .Ht = 9: .HtMax = 9
        .Camufl = 0.9: .CamuflNatural = 0.9
        .Olho = 0: .OlhoNatural = 0
        .MagicRes = 1.2: .MagicResNatural = 1.2
        .TMax = 7:  .Mira = 1.2: .For�a = 18
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pMagrao
        .Ht = 5.5: .HtMax = 5.5
        .TMax = 6.25: .For�a = 12
        .Camufl = 2: .CamuflNatural = 2
        .Raio = 15
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pZerg
        .Ht = 7: .HtMax = 7: .Reg = 1.1: .RegNatural = 1.1
        .TMax = 7: .For�a = 20
        .MagicRes = 1.1: .MagicResNatural = 1.1
        .ArmaM�o = 18: .Mun(18) = MunA(18): .ArmaP(18) = True
        .Parasitas = True
    Case pTanque
        .Ht = 15: .HtMax = 15
        .TMax = 8: .Blindagem = 5 ': .For�a = 15
        .Camufl = 0.3: .CamuflNatural = 0.3
        .Olho = 0.8: .OlhoNatural = 0.8
        .Detec = True
        .ArmaM�o = 21: .Canh�o = 21
        .Mun(19) = 900: .ArmaP(19) = True   'Metralhadora
        .Mun(20) = MunA(20): .ArmaP(20) = True   'Canh�o HE
        .Mun(21) = MunA(21): .ArmaP(21) = True   'Canh�o AP
    Case pSabata
        .Ht = 7: .HtMax = 7
        .TMax = 6.25: .Mira = 5: .For�a = 16
        .Camufl = 0.9: .CamuflNatural = 0.9
        .Olho = 1.5: .OlhoNatural = 1.5
        .ArmaM�o = 1: .Mun(1) = 6 * MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pBlindado
        .Ht = 8: .HtMax = 8
        .TMax = 6: .For�a = 18: .Blindagem = 3
        .Camufl = 0.9: .CamuflNatural = 0.9
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pJedi
        .Ht = 8: .HtMax = 8
        .TMax = 8.5: .For�a = 20
        .Olho = 2: .OlhoNatural = 2
        .MagicRes = 1.4: .MagicResNatural = 1.4
        .ArmaM�o = 23: .ArmaP(23) = True
    Case pNinja
        .Ht = 7: .HtMax = 7
        .TMax = 7.5: .For�a = 18
        .Camufl = 1.8: .CamuflNatural = 1.8
        .Olho = 1.2: .OlhoNatural = 1.2
        .MagicRes = 1.2: .MagicResNatural = 1.2
        .ArmaM�o = 24: .Mun(24) = MunA(24): .ArmaP(24) = True   'Estrelinhas
        .ArmaP(25) = True  'Katana
        .Fuma� = 5 ': Raio(a) = 40
    Case pHorla
        .Ht = 6: .HtMax = 6
        .TMax = 6.75: .Mira = 0.5: .For�a = 10
        .Camufl = 15: .CamuflNatural = 15
        .Olho = 0.9: .OlhoNatural = 0.9
        .MagicRes = 2: .MagicResNatural = 2
        .ArmaM�o = 26: .ArmaP(26) = True  '.ArmaM�o = 1: .ArmaP( 1) = True
    Case pCapangas
        .Ht = 6: .HtMax = 6
        .TMax = 6: .Mira = 0.8: .For�a = 14
        .Camufl = 0.85: .CamuflNatural = 0.85
        .Olho = 0.9: .OlhoNatural = 0.9
        .MagicRes = 0.9: .MagicResNatural = 0.9
        .ArmaM�o = 3: .Mun(3) = MunA(3): .ArmaP(3) = True: .ArmaP(26) = True
    Case pDinamitador
        .Ht = 7: .HtMax = 7
        .TMax = 6: .Mira = 0.9: .For�a = 20
        .Camufl = 0.9: .CamuflNatural = 0.9
        .ArmaM�o = 26: .ArmaP(26) = True
        .Gran = 12: .Minas = 6: .Bombas = 3
    Case pT_1000
        .Ht = 15: .HtMax = 15: .Blindagem = 1.5
        .TMax = 9: .For�a = 30
        .Olho = 1.2: .OlhoNatural = 1.2
        .MagicRes = 1.5: .MagicResNatural = 1.5
    Case pAgente_do_Matrix
        .Ht = 9: .HtMax = 9
        .TMax = 8:  .Mira = 1.5: .For�a = 20
        .Olho = 1.25: .OlhoNatural = 1.25
        .MagicRes = 1.2: .MagicResNatural = 1.2
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
    Case pClerigo_Maligno
        .Ht = 6: .HtMax = 6
        .TMax = 6.25: .Mira = 0.8: .For�a = 14
        .Camufl = 0.9: .CamuflNatural = 0.9
        .MagicRes = 1.6: .MagicResNatural = 1.6
        .ArmaM�o = 1: .Mun(1) = MunA(1): .ArmaP(1) = True: .ArmaP(26) = True
        .ManaF� = 100
    Case Else
        'a = a - 1
        MsgBox "Ocorreu algum erro ao sortear os poderes. Por favor avise o programador. � aconselh�vel reiniciar o jogo."
    End Select
    .HtAnt = .Ht
End With
End Function

Public Function ZeraJogador(ByRef Jog As typJogador, Optional n As Byte)
Dim i As Integer, K As Integer, b As Integer
With Jog
    '.Poder = pSorteado
    .X = -100: .Y = -100
    For i = LBound(.XBomba, 1) To UBound(.XBomba, 1)
        .XBomba(i) = 666: .YBomba(i) = 666
    Next
    .ContBomba = -1
    .DimiExplodiu = False
    .ZHol = 255
    '*** Valores padr�o
    .Raio = 50
    .Mira = 1
    .Reg = 1: .RegNatural = 1
    .Camufl = 1: .CamuflNatural = 1
    .Olho = 1: .OlhoNatural = 1
    .MagicRes = 1: .MagicResNatural = 1
    '***
    For i = LBound(.Fog, 1) To UBound(.Fog, 1)
        For K = LBound(.Fog, 2) To UBound(.Fog, 2)
            For b = LBound(.Fog, 3) To UBound(.Fog, 3)
                .Fog(i, K, b) = 255
            Next
        Next
    Next
    If Not IsMissing(n) Then
        If n <> 0 Then
            .Nome = "Jog " & n & vbNullChar
            .Imit = n
            .Time = n
            .Cor = 255 'A cor vai ter que ser alterada depois
            If n > M Then .Cur = 666 'Jog � um mostro
        End If
    End If
End With
End Function

Public Function Distancia(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer) As Single
Dim Xc1 As Single, Yc1 As Single
Dim Xc2 As Single, Yc2 As Single

CentroHex x1, y1, Xc1, Yc1
CentroHex x2, y2, Xc2, Yc2

Distancia = Sqr((Xc1 - Xc2) ^ 2 + (Yc1 - Yc2) ^ 2)
End Function

Public Function funAngulo(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer) As Single
Dim Xc1 As Single, Yc1 As Single
Dim Xc2 As Single, Yc2 As Single

CentroHex x1, y1, Xc1, Yc1
CentroHex x2, y2, Xc2, Yc2

funAngulo = Atn((Yc2 - Yc1) / (Xc2 - Xc1 + 0.0001))
If Xc2 - Xc1 < 0 Then funAngulo = funAngulo + pi
End Function

Public Function ArrumaNome(str As String) As String
str = Trim(str)
If Asc(Right(str, 1)) = 0 Then str = Left(str, Len(str) - 1)
ArrumaNome = str
End Function

Public Function QuantosT�Vendo() As Byte
Dim v As Byte, hip As Byte
v = 0
For hip = 1 To NJogs
    If Est�Vendo(hip) Then v = v + 1
    If ViHol(hip) Then v = v + 1
Next
For hip = 1 + M To NMons + M
    If Est�Vendo(hip) Then v = v + 1
Next
QuantosT�Vendo = v
End Function

Public Function Est�Vendo(Quem As Byte) As Boolean
Est�Vendo = False
If JaVi(Quem) = 1 Or JaVi(Quem) = 2 Then Est�Vendo = True
'If vz > M And Poder(Quem) = pAgente_do_Matrix Then Est�Vendo = False
End Function

Public Function Est�SendoVistoPor(Quem As Byte) As Boolean
Est�SendoVistoPor = False
If JaVi(Quem) = 2 Or JaVi(Quem) = 3 Then Est�SendoVistoPor = True
End Function

Public Function Mana(Magia As enumMagiasArcanas) As Integer
Select Case Magia
    Case enuIdentificacao: Mana = 5
    Case enuPararTempo: Mana = 55
    Case enuInvisibilidade: Mana = 15
    Case enuTeletransporte: Mana = 15
    Case enuImitar: Mana = 20
    Case enuHolografia: Mana = 10
    Case enuBolaDeFogo: Mana = 25
    Case enuDardosMisticos: Mana = 12
End Select
End Function

Public Function F�(Magia As String) As Integer
Select Case Magia
Case "Rezar": F� = -25
Case "Cura": F� = 20
Case "Animar": F� = 15
Case "Rel�mpago": F� = 25
End Select
End Function

Public Static Sub SemTempo(oque$)
Dim Mensagem As String, Bot�es As VbMsgBoxStyle, T�tulo As String
Mensagem = "Voc� n�o tem tempo" & Chr$(13) & " para " + oque$
Bot�es = vbOKOnly '+ vbExclamation
T�tulo = "Duelo"
MsgMuda Mensagem, Bot�es, T�tulo
End Sub

Public Sub IniciaEspera(dt As Integer)
Mapa.Timer.Enabled = True
Mapa.Timer.Interval = dt
eveTimer = False
End Sub

Public Sub TerminaEspera()
Do
   DoEvents
Loop Until eveTimer
Mapa.Timer.Interval = 0
Mapa.Timer.Enabled = False
End Sub

Public Function MsgMuda(Mensagem As String, Optional Botoes As VbMsgBoxStyle, Optional Titulo As String, Optional FechaSozinho As Boolean, Optional frm As Object) As VbMsgBoxResult
If Botoes = 0 Then Botoes = vbOKOnly
If Titulo = "" Then Titulo = "DUELO"
If IsEmpty(FechaSozinho) Then FechaSozinho = True
frmMsg.Caption = Titulo
frmMsg.lblMens = Mensagem
Select Case Botoes
    Case vbOKOnly
        frmMsg.comOK.Visible = True
        frmMsg.comSim.Visible = False
        frmMsg.comNao.Visible = False
    Case vbYesNo
        frmMsg.comOK.Visible = False
        frmMsg.comSim.Visible = True
        frmMsg.comNao.Visible = True
End Select

If frm Is Nothing Then
    Duelando.Enabled = False
    Mapa.Enabled = False
ElseIf frm.Name = Estat.Name Then
    Estat.Enabled = False
Else
    Editor.Enabled = False
End If

frmMsg.resposta = 100
AlwaysOnTop frmMsg, True
frmMsg.Show
frmMsg.SetFocus
Do Until Not frmMsg.Visible
    DoEvents
Loop
AlwaysOnTop frmMsg, False
MsgMuda = frmMsg.resposta
frmMsg.resposta = 100
frmMsg.Visible = False

If frm Is Nothing Then
    Duelando.Enabled = True
    Mapa.Enabled = True
ElseIf frm.Name = Estat.Name Then
    Estat.Enabled = True
Else
    Editor.Enabled = True
End If

End Function

Public Function DifCircular(a As Integer, b As Integer, n As Integer) As Integer
'calcula a diferenca entre A e B, sendo que
'eles variam de 0 a (N-1) e ...(n�o sei explicar em palavras)
DifCircular = Min(Abs(a - b), Abs(Abs(a - b) - n))
End Function

Public Function Min(a, b)
Min = IIf(a < b, a, b)
End Function

Public Function PoderBytStr(n As Byte) As String
Dim p As String
    Select Case n
        Case pSorteado: p = "Sorteado"
        Case pPM: p = "PM"
        Case pAtirador_de_Elite: p = "Atirador de Elite"
        Case pVelocista: p = "Velocista"
        Case pCasca_Grossa: p = "Casca Grossa"
        Case pTroll: p = "Troll"
        Case pBoina_Verde: p = "Boina Verde"
        Case pPredador: p = "Predador"
        Case pWolverine: p = "Wolverine"
        Case pVoador: p = "Voador"
        Case pFantasma: p = "Fantasma"
        Case pTarrasque: p = "Tarrasque"
        Case pRobocop: p = "Robocop"
        Case pHomem_do_Raio_X: p = "Homem do Raio-X"
        Case pBalacau: p = "Balacau"
        Case pMosca: p = "Mosca"
        Case pDragao: p = "Drag�o"
        Case pMago: p = "Mago"
        Case pEthereal: p = "Ethereal"
        Case pJason: p = "Jason"
        Case pAlien: p = "Alien"
        Case pDemolidor: p = "Demolidor"
        Case pMagrao: p = "Magr�o"
        Case pZerg: p = "Zerg"
        Case pTanque: p = "Tanque"
        Case pSabata: p = "Sabata"
        Case pBlindado: p = "Blindado"
        Case pJedi: p = "Jedi"
        Case pNinja: p = "Ninja"
        Case pHorla: p = "Horla"
        Case pCapangas: p = "Capangas"
        Case pDinamitador: p = "Dinamitador"
        Case pT_1000: p = "T-1000"
        Case pAgente_do_Matrix: p = "Agente do Matrix"
        Case pClerigo_Maligno: p = "Cl�rigo Maligno"
    End Select
    PoderBytStr = p
End Function

Public Function PoderStrByt(str As String) As Byte
Dim p As Byte
    Select Case str
        Case "Sorteado": p = pSorteado
        Case "PM": p = pPM
        Case "Atirador de Elite": p = pAtirador_de_Elite
        Case "Velocista": p = pVelocista
        Case "Casca Grossa": p = pCasca_Grossa
        Case "Troll": p = pTroll
        Case "Boina Verde": p = pBoina_Verde
        Case "Predador": p = pPredador
        Case "Wolverine": p = pWolverine
        Case "Voador": p = pVoador
        Case "Fantasma": p = pFantasma
        Case "Tarrasque": p = pTarrasque
        Case "Robocop": p = pRobocop
        Case "Homem do Raio-X": p = pHomem_do_Raio_X
        Case "Balacau": p = pBalacau
        Case "Mosca": p = pMosca
        Case "Drag�o": p = pDragao
        Case "Mago": p = pMago
        Case "Ethereal": p = pEthereal
        Case "Jason": p = pJason
        Case "Alien": p = pAlien
        Case "Demolidor": p = pDemolidor
        Case "Magr�o": p = pMagrao
        Case "Zerg": p = pZerg
        Case "Tanque": p = pTanque
        Case "Sabata": p = pSabata
        Case "Blindado": p = pBlindado
        Case "Jedi": p = pJedi
        Case "Ninja": p = pNinja
        Case "Horla": p = pHorla
        Case "Capangas": p = pCapangas
        Case "Dinamitador": p = pDinamitador
        Case "T-1000": p = pT_1000
        Case "Agente do Matrix": p = pAgente_do_Matrix
        Case "Cl�rigo Maligno": p = pClerigo_Maligno
    End Select
    PoderStrByt = p
End Function

Public Function TiraPath(strFile As String) As String
strFile = Right(strFile, 12)
Do While (InStr(1, strFile, "\") <> 0)
    strFile = Right(strFile, Len(strFile) - 1)
Loop
TiraPath = strFile
End Function

Public Function FileExists(strArq As String) As Boolean
On Error GoTo TrataErro
Open ArqEstat$ For Input As #1
Close #1
FileExists = True
Exit Function
TrataErro:
If Err.Number = 53 Or Err.Number = 75 Then 'file not found || path/file access error
    FileExists = False
Else
    Err.Raise Err.Number
End If
End Function

Sub AlwaysOnTop(FrmID As Form, OnTop As Boolean)
Const SWP_NOMOVE = 2
Const SWP_NOSIZE = 1
Const Flags = SWP_NOMOVE Or SWP_NOSIZE
Const HWND_TOPMOST = -1
Const HWND_NOTOPMOST = -2

If OnTop Then
    OnTop = SetWindowPos(FrmID.hwnd, HWND_TOPMOST, 0, 0, 0, 0, Flags)
Else
    OnTop = SetWindowPos(FrmID.hwnd, HWND_TOPMOST, 0, 0, 0, 0, Flags)
End If
End Sub

Public Sub Troca(ByRef a As Variant, ByRef b As Variant)
Dim temp As Variant
temp = a
a = b
b = temp
End Sub

Function Aleat�rio(numero As Single) As Double
    ' obs: essa fun��o conserva o sinal (e fixa o zero)
    Aleat�rio = CDbl(numero) * Exp(superrnd(Log(1.2))) '1.5
End Function

Function superrnd(desvio As Double) As Double

' Calcula um n� aleatorio com media zero e desv. padrao = "desvio"
' Obs. p/ matem�ticos e outros espertos: a distribuicao e' normal.

superrnd = normali(Rnd, 0, desvio)

End Function

Function normali(p As Double, Md As Double, s As Double) As Double
Dim Q As Double, � As Double
' Distribiucao normal inversa
' fornece a ordenada x tq Probabilidade de -� a x = p
' md= media, s=desvio padrao

Q = Sqr(-2 * Log(1 - p))
� = Q - (cd0 + cd1 * Q + cd2 * Q ^ 2) / (ce0 + ce1 * Q + ce2 * Q ^ 2 + ce3 * Q ^ 3)
normali = Md + s * �

End Function
