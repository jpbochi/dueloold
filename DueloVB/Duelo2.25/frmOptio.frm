VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Op��es"
   ClientHeight    =   3615
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   5055
   Icon            =   "frmOptio.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3615
   ScaleWidth      =   5055
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab tbsOptions 
      Height          =   2895
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   5106
      _Version        =   393216
      TabsPerRow      =   4
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tiro"
      TabPicture(0)   =   "frmOptio.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "sldVelTiro"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "chkRastroTiro"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtVeltiro"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Geral"
      TabPicture(1)   =   "frmOptio.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "chkComSom"
      Tab(1).Control(1)=   "sldVelRot"
      Tab(1).Control(2)=   "sldVelReplay"
      Tab(1).Control(3)=   "Label1(2)"
      Tab(1).Control(4)=   "Label1(1)"
      Tab(1).ControlCount=   5
      TabCaption(2)   =   "Classes"
      TabPicture(2)   =   "frmOptio.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Slider(1)"
      Tab(2).Control(1)=   "Slider(2)"
      Tab(2).Control(2)=   "Slider(3)"
      Tab(2).Control(3)=   "Slider(4)"
      Tab(2).Control(4)=   "Slider(5)"
      Tab(2).Control(5)=   "Label(2)"
      Tab(2).Control(6)=   "Label(3)"
      Tab(2).Control(7)=   "Label(4)"
      Tab(2).Control(8)=   "Label(5)"
      Tab(2).Control(9)=   "Label(1)"
      Tab(2).Control(10)=   "Label2(0)"
      Tab(2).Control(11)=   "Label2(1)"
      Tab(2).Control(12)=   "Label2(2)"
      Tab(2).Control(13)=   "Label2(3)"
      Tab(2).Control(14)=   "Label2(4)"
      Tab(2).ControlCount=   15
      Begin VB.TextBox txtVeltiro 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   33
         Text            =   "###"
         Top             =   1320
         Width           =   855
      End
      Begin VB.CheckBox chkComSom 
         Caption         =   "Com som"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74640
         TabIndex        =   17
         Top             =   2040
         Value           =   1  'Checked
         Width           =   1575
      End
      Begin VB.CheckBox chkRastroTiro 
         Caption         =   "Rastro do tiro"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   1320
         Value           =   1  'Checked
         Width           =   1575
      End
      Begin MSComctlLib.Slider sldVelTiro 
         Height          =   495
         Left            =   2280
         TabIndex        =   10
         Top             =   720
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   873
         _Version        =   393216
         LargeChange     =   80
         SmallChange     =   5
         Min             =   200
         Max             =   600
         SelStart        =   200
         TickFrequency   =   20
         Value           =   200
         TextPosition    =   1
      End
      Begin MSComctlLib.Slider sldVelRot 
         Height          =   495
         Left            =   -72840
         TabIndex        =   13
         Top             =   720
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   873
         _Version        =   393216
         LargeChange     =   2
         Min             =   2
         Max             =   20
         SelStart        =   10
         Value           =   10
      End
      Begin MSComctlLib.Slider sldVelReplay 
         Height          =   495
         Left            =   -72840
         TabIndex        =   15
         Top             =   1320
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   873
         _Version        =   393216
         LargeChange     =   2
         Min             =   2
         Max             =   20
         SelStart        =   10
         Value           =   10
      End
      Begin MSComctlLib.Slider Slider 
         Height          =   255
         Index           =   1
         Left            =   -73800
         TabIndex        =   18
         Top             =   480
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   450
         _Version        =   393216
         LargeChange     =   1
         Min             =   1
         Max             =   20
         SelStart        =   1
         Value           =   1
      End
      Begin MSComctlLib.Slider Slider 
         Height          =   255
         Index           =   2
         Left            =   -73800
         TabIndex        =   19
         Top             =   960
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   450
         _Version        =   393216
         LargeChange     =   1
         Min             =   1
         Max             =   20
         SelStart        =   1
         Value           =   1
      End
      Begin MSComctlLib.Slider Slider 
         Height          =   255
         Index           =   3
         Left            =   -73800
         TabIndex        =   20
         Top             =   1440
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   450
         _Version        =   393216
         LargeChange     =   1
         Min             =   1
         Max             =   20
         SelStart        =   1
         Value           =   1
      End
      Begin MSComctlLib.Slider Slider 
         Height          =   255
         Index           =   4
         Left            =   -73800
         TabIndex        =   21
         Top             =   1920
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   450
         _Version        =   393216
         LargeChange     =   1
         Min             =   1
         Max             =   20
         SelStart        =   1
         Value           =   1
      End
      Begin MSComctlLib.Slider Slider 
         Height          =   255
         Index           =   5
         Left            =   -73800
         TabIndex        =   22
         Top             =   2400
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   450
         _Version        =   393216
         LargeChange     =   1
         Min             =   1
         Max             =   20
         SelStart        =   1
         Value           =   1
      End
      Begin VB.Label Label 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   195
         Index           =   2
         Left            =   -71160
         TabIndex        =   32
         Top             =   960
         Width           =   480
      End
      Begin VB.Label Label 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   195
         Index           =   3
         Left            =   -71160
         TabIndex        =   31
         Top             =   1440
         Width           =   480
      End
      Begin VB.Label Label 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   195
         Index           =   4
         Left            =   -71160
         TabIndex        =   30
         Top             =   1920
         Width           =   480
      End
      Begin VB.Label Label 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   195
         Index           =   5
         Left            =   -71160
         TabIndex        =   29
         Top             =   2400
         Width           =   480
      End
      Begin VB.Label Label 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   195
         Index           =   1
         Left            =   -71160
         TabIndex        =   28
         Top             =   480
         Width           =   480
      End
      Begin VB.Label Label2 
         Caption         =   "Classe A"
         Height          =   255
         Index           =   0
         Left            =   -74760
         TabIndex        =   27
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Classe B"
         Height          =   255
         Index           =   1
         Left            =   -74760
         TabIndex        =   26
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Classe C"
         Height          =   255
         Index           =   2
         Left            =   -74760
         TabIndex        =   25
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Classe D"
         Height          =   255
         Index           =   3
         Left            =   -74760
         TabIndex        =   24
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Classe E"
         Height          =   255
         Index           =   4
         Left            =   -74760
         TabIndex        =   23
         Top             =   2400
         Width           =   855
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Velocidade do replay:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   -74760
         TabIndex        =   16
         Top             =   1440
         Width           =   1875
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Velocidade de giro:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   -74760
         TabIndex        =   14
         Top             =   840
         Width           =   1665
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Velocidade do tiro:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   11
         Top             =   840
         Width           =   1620
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         Height          =   1785
         Left            =   2100
         TabIndex        =   8
         Top             =   840
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   2
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Sample 3"
         Height          =   1785
         Left            =   1545
         TabIndex        =   7
         Top             =   675
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample2 
         Caption         =   "Sample 2"
         Height          =   1785
         Left            =   645
         TabIndex        =   6
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Aplicar"
      Height          =   375
      Left            =   3840
      TabIndex        =   2
      Top             =   3135
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   2640
      TabIndex        =   1
      Top             =   3135
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Tudo Certo"
      Height          =   375
      Left            =   1410
      TabIndex        =   0
      Top             =   3135
      Width           =   1095
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    'center the form
    Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
    tbsOptions.Tab = 0
End Sub

Private Sub Form_Activate()
Dim i As Byte
    sldVelTiro.Value = Config.VelTiro
    txtVeltiro = Format(CInt(1.01 ^ sldVelTiro.Value), "###")
    chkRastroTiro.Value = Abs(CInt(Config.RastroTiro))
    sldVelRot.Value = 1000 / Config.VelRotacao
    sldVelReplay.Value = 5000 / Config.VelReplay
    chkComSom.Value = Abs(CInt(Config.ComSom))
    For i = 1 To 5
        Slider(i) = Config.ChanceClasse(i)
    Next
    CalcChances
    Refresh
End Sub

Private Sub cmdApply_Click()
    GravaConfig
End Sub

Private Sub cmdCancel_Click()
    LeConfig
    Hide
    'Unload Me
    'Inicio.Visible = True
End Sub

Private Sub cmdOK_Click()
    GravaConfig
    Hide
    'Unload Me
    'Inicio.Visible = True
End Sub

Private Sub sldVelTiro_Change()
Config.VelTiro = sldVelTiro.Value
txtVeltiro = Format(CInt(1.01 ^ sldVelTiro.Value), "###")
End Sub

Private Sub chkRastroTiro_Click()
Config.RastroTiro = chkRastroTiro.Value
End Sub

Private Sub sldVelRot_Change()
Config.VelRotacao = 1000 / sldVelRot.Value
End Sub

Private Sub sldVelReplay_Change()
Config.VelReplay = 5000 / sldVelReplay.Value
End Sub

Private Sub chkComSom_Click()
Config.ComSom = chkComSom.Value
End Sub

Private Sub Slider_Change(Index As Integer)
Config.ChanceClasse(Index) = Slider(Index).Value
CalcChances
End Sub

Private Sub CalcChances()
Dim ChanceTot As Integer, i As Byte
For i = 1 To 5
    ChanceTot = ChanceTot + Config.ChanceClasse(i)
Next
For i = 1 To 5
    Label(i) = Trim(Int(1000 * Config.ChanceClasse(i) / ChanceTot) / 10) + "%"
Next
End Sub
