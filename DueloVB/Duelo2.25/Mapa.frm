VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Mapa 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E0FFFF&
   Caption         =   "Mapa"
   ClientHeight    =   6795
   ClientLeft      =   3315
   ClientTop       =   345
   ClientWidth     =   8625
   ControlBox      =   0   'False
   DrawWidth       =   2
   ForeColor       =   &H00FF0000&
   Icon            =   "Mapa.frx":0000
   KeyPreview      =   -1  'True
   LinkMode        =   1  'Source
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   453
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   575
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   11
      Left            =   10920
      Picture         =   "Mapa.frx":000C
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   84
      Top             =   4080
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   10
      Left            =   10920
      Picture         =   "Mapa.frx":0414
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   83
      Top             =   3720
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   9
      Left            =   10920
      Picture         =   "Mapa.frx":082C
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   82
      Top             =   3360
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   8
      Left            =   10920
      Picture         =   "Mapa.frx":0C39
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   81
      Top             =   3000
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   7
      Left            =   10920
      Picture         =   "Mapa.frx":1048
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   80
      Top             =   2640
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   6
      Left            =   10920
      Picture         =   "Mapa.frx":145B
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   79
      Top             =   2280
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   5
      Left            =   10920
      Picture         =   "Mapa.frx":1869
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   78
      Top             =   1920
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   4
      Left            =   10920
      Picture         =   "Mapa.frx":1C70
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   77
      Top             =   1560
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   3
      Left            =   10920
      Picture         =   "Mapa.frx":2083
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   76
      Top             =   1200
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   2
      Left            =   10920
      Picture         =   "Mapa.frx":2498
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   75
      Top             =   840
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   1
      Left            =   10920
      Picture         =   "Mapa.frx":28A9
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   74
      Top             =   480
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Carinha 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   0
      Left            =   10920
      Picture         =   "Mapa.frx":2CC1
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   73
      Top             =   120
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Fog2 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   9600
      Picture         =   "Mapa.frx":30CF
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   72
      Top             =   0
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.VScrollBar VScroll 
      Height          =   6855
      Left            =   8295
      TabIndex        =   71
      Top             =   0
      Width           =   255
   End
   Begin VB.HScrollBar HScroll 
      Height          =   255
      Left            =   0
      TabIndex        =   70
      Top             =   6975
      Width           =   8295
   End
   Begin VB.PictureBox Fog1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   9240
      Picture         =   "Mapa.frx":3458
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   69
      Top             =   0
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Timer Timer 
      Left            =   7920
      Top             =   7560
   End
   Begin MSComDlg.CommonDialog Dialog 
      Left            =   8880
      Top             =   5760
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Duelo"
   End
   Begin VB.PictureBox Desmonstro 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   7
      Left            =   11400
      Picture         =   "Mapa.frx":37D3
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   68
      Top             =   2280
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   57
      Left            =   9960
      Picture         =   "Mapa.frx":3E99
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   67
      Top             =   5040
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   38
      Left            =   9960
      Picture         =   "Mapa.frx":421E
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   66
      Top             =   4680
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   11
      Left            =   9960
      Picture         =   "Mapa.frx":45CC
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   65
      Top             =   2160
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   27
      Left            =   9960
      Picture         =   "Mapa.frx":4941
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   64
      Top             =   4320
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   45
      Left            =   8880
      Picture         =   "Mapa.frx":4D13
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   63
      Top             =   4680
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   29
      Left            =   10320
      Picture         =   "Mapa.frx":5096
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   62
      Top             =   2880
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   59
      Left            =   9600
      Picture         =   "Mapa.frx":541E
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   61
      Top             =   3960
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   54
      Left            =   9600
      Picture         =   "Mapa.frx":57AB
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   60
      Top             =   4320
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   7
      Left            =   8880
      Picture         =   "Mapa.frx":5B41
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   59
      Top             =   4320
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   51
      Left            =   10320
      Picture         =   "Mapa.frx":5EB5
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   58
      Top             =   2520
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   33
      Left            =   9240
      Picture         =   "Mapa.frx":6279
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   57
      Top             =   4320
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   55
      Left            =   9960
      Picture         =   "Mapa.frx":65FD
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   56
      Top             =   1800
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   4
      Left            =   9960
      Picture         =   "Mapa.frx":6980
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   55
      Top             =   720
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   15
      Left            =   10320
      Picture         =   "Mapa.frx":6D1A
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   54
      Top             =   720
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   37
      Left            =   9960
      Picture         =   "Mapa.frx":70C6
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   53
      Top             =   3600
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   32
      Left            =   9600
      Picture         =   "Mapa.frx":749B
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   52
      Top             =   4680
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   8
      Left            =   9960
      Picture         =   "Mapa.frx":7840
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   51
      Top             =   1080
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   21
      Left            =   9960
      Picture         =   "Mapa.frx":7BFC
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   50
      Top             =   3240
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   14
      Left            =   9240
      Picture         =   "Mapa.frx":7F84
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   49
      Top             =   3960
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   13
      Left            =   8880
      Picture         =   "Mapa.frx":8313
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   48
      Top             =   3960
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   16
      Left            =   9240
      Picture         =   "Mapa.frx":86B1
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   47
      Top             =   4680
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   3
      Left            =   9960
      Picture         =   "Mapa.frx":8A7D
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   46
      Top             =   360
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   46
      Left            =   10320
      Picture         =   "Mapa.frx":8DF2
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   45
      Top             =   2160
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   17
      Left            =   9960
      Picture         =   "Mapa.frx":91AE
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   44
      Top             =   2520
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   56
      Left            =   10320
      Picture         =   "Mapa.frx":951F
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   43
      Top             =   1800
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   12
      Left            =   9960
      Picture         =   "Mapa.frx":98B7
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   42
      Top             =   2880
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   28
      Left            =   10320
      Picture         =   "Mapa.frx":9C3D
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   41
      Top             =   1080
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   30
      Left            =   10320
      Picture         =   "Mapa.frx":A064
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   40
      Top             =   1440
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   41
      Left            =   9960
      Picture         =   "Mapa.frx":A109
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   39
      Top             =   3960
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   19
      Left            =   9960
      Picture         =   "Mapa.frx":A4E8
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   38
      Top             =   1440
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   35
      Left            =   9600
      Picture         =   "Mapa.frx":A89F
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   37
      Top             =   3240
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   253
      Left            =   9600
      Picture         =   "Mapa.frx":AC2B
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   36
      Top             =   360
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   252
      Left            =   9240
      Picture         =   "Mapa.frx":AFA1
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   35
      Top             =   360
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   251
      Left            =   8880
      Picture         =   "Mapa.frx":B314
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   34
      Top             =   360
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   52
      Left            =   9600
      Picture         =   "Mapa.frx":B680
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   33
      Top             =   3600
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   53
      Left            =   9240
      Picture         =   "Mapa.frx":B9EE
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   32
      Top             =   3600
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   20
      Left            =   8880
      Picture         =   "Mapa.frx":BD5C
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   31
      Top             =   3600
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   40
      Left            =   9600
      Picture         =   "Mapa.frx":C0CF
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   30
      Top             =   2160
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   31
      Left            =   9240
      Picture         =   "Mapa.frx":C4C4
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   29
      Top             =   2160
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   42
      Left            =   9600
      Picture         =   "Mapa.frx":C894
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   28
      Top             =   2880
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   18
      Left            =   9240
      Picture         =   "Mapa.frx":CC6C
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   27
      Top             =   2880
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   2
      Left            =   8880
      Picture         =   "Mapa.frx":D021
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   26
      Top             =   2880
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   49
      Left            =   9600
      Picture         =   "Mapa.frx":D10E
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   25
      Top             =   1440
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Desmonstro 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   5
      Left            =   11400
      Picture         =   "Mapa.frx":D196
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   24
      Top             =   1560
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Desmonstro 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   4
      Left            =   11400
      Picture         =   "Mapa.frx":D85C
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   23
      Top             =   1200
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Desmonstro 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   3
      Left            =   11400
      Picture         =   "Mapa.frx":DF22
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   22
      Top             =   840
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Desmonstro 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   1
      Left            =   11400
      Picture         =   "Mapa.frx":E5E8
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   21
      Top             =   120
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Desmonstro 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   6
      Left            =   11400
      Picture         =   "Mapa.frx":ECAE
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   20
      Top             =   1920
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Desmonstro 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   2
      Left            =   11400
      Picture         =   "Mapa.frx":F374
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   19
      Top             =   480
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   43
      Left            =   9600
      Picture         =   "Mapa.frx":FA3A
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   18
      Top             =   2520
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   24
      Left            =   9240
      Picture         =   "Mapa.frx":FDDD
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   17
      Top             =   2520
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   26
      Left            =   8880
      Picture         =   "Mapa.frx":10172
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   16
      Top             =   2520
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   58
      Left            =   9240
      Picture         =   "Mapa.frx":10502
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   15
      Top             =   3240
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   39
      Left            =   8880
      Picture         =   "Mapa.frx":108B9
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   14
      Top             =   3240
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   48
      Left            =   9240
      Picture         =   "Mapa.frx":10C7B
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   13
      Top             =   1440
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   36
      Left            =   9240
      Picture         =   "Mapa.frx":1109E
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   12
      Top             =   1800
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   34
      Left            =   8880
      Picture         =   "Mapa.frx":1144E
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   11
      Top             =   1800
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox Nada 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   8880
      Picture         =   "Mapa.frx":1181F
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   10
      Top             =   0
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   22
      Left            =   9240
      Picture         =   "Mapa.frx":11B9A
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   9
      Top             =   720
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   44
      Left            =   8880
      Picture         =   "Mapa.frx":11F5F
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   8
      Top             =   2160
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   47
      Left            =   8880
      Picture         =   "Mapa.frx":122F5
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   7
      Top             =   1440
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   23
      Left            =   9960
      Picture         =   "Mapa.frx":12738
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   6
      Top             =   0
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   1
      Left            =   8880
      Picture         =   "Mapa.frx":12ABC
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   5
      Top             =   720
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   10
      Left            =   9240
      Picture         =   "Mapa.frx":12E94
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   4
      Top             =   1080
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox ObjetoH 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   5
      Left            =   8880
      Picture         =   "Mapa.frx":1329C
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   26
      TabIndex        =   3
      Top             =   1080
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.PictureBox MiniMapa 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      FillColor       =   &H00FF0000&
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   120
      ScaleHeight     =   23
      ScaleMode       =   0  'User
      ScaleWidth      =   27
      TabIndex        =   2
      Top             =   7320
      Width           =   405
   End
   Begin VB.PictureBox BordaInv 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   30060
      Left            =   8760
      ScaleHeight     =   91
      ScaleMode       =   0  'User
      ScaleWidth      =   109
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   6840
      Visible         =   0   'False
      Width           =   36000
   End
   Begin VB.PictureBox Borda 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   6960
      Left            =   0
      MouseIcon       =   "Mapa.frx":136A1
      ScaleHeight     =   21
      ScaleMode       =   0  'User
      ScaleWidth      =   25
      TabIndex        =   0
      Top             =   0
      Width           =   8280
   End
   Begin VB.Shape ReplayElip 
      BorderColor     =   &H000000FF&
      BorderWidth     =   2
      Height          =   495
      Left            =   1680
      Top             =   7440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Image Replay 
      Height          =   270
      Index           =   1
      Left            =   1800
      Picture         =   "Mapa.frx":137F3
      Top             =   7560
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.Image Replay 
      Height          =   270
      Index           =   2
      Left            =   2040
      Picture         =   "Mapa.frx":13D0D
      Stretch         =   -1  'True
      Top             =   7560
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.Image Replay 
      Height          =   270
      Index           =   3
      Left            =   2280
      Picture         =   "Mapa.frx":14227
      Stretch         =   -1  'True
      Top             =   7560
      Visible         =   0   'False
      Width           =   150
   End
End
Attribute VB_Name = "Mapa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim xanterior, yanterior

Private Sub Form_Activate()
Me.Left = Duelando.Left + Duelando.Width
End Sub

Private Sub Form_Resize()
Dim i As Integer
'574 x 543
If Me.ScaleWidth < MapaWidth Then
    Me.Width = (MapaWidth + 8) * Screen.TwipsPerPixelX
    Exit Sub
End If
If Me.ScaleHeight < MapaHeight Then
    Me.Height = (MapaHeight + 8 + 19) * Screen.TwipsPerPixelY
    Exit Sub
Else
    MiniMapa.Top = 488 + (Me.ScaleHeight - MapaHeight)
    ReplayElip.Top = 496 + (Me.ScaleHeight - MapaHeight)
    For i = Replay.LBound To Replay.UBound
        Replay(i).Top = 504 + (Me.ScaleHeight - MapaHeight)
    Next
End If
ReDimensiona Me
If vz <> 0 Then RePinta Me
End Sub

Private Sub Borda_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim XIni As Integer, YIni As Integer
Dim XCIni As Single, YCIni As Single
Dim XHex As Integer, YHex As Integer
Dim XCen As Single, YCen As Single

HexClick X, Y, XHex, YHex
Mapa.Borda.ToolTipText = NomeConteudo(Conte�do(XHex, YHex, Andar))
If Tiro > 0 Or Comando = CmdDardosMisticos Then
    'HexClick X, Y, XHex, YHex
    CentroHex XHex, YHex, XCen, YCen
    Call TestaScroll(XHex - Borda.ScaleLeft, YHex - Borda.ScaleTop, 1)
    Borda.AutoRedraw = False
    If xanterior <> XCen Or yanterior <> YCen Then
        If �ltPas = 0 Then
            XIni = Definicoes.J(vz).X
            YIni = Definicoes.J(vz).Y
        Else
            XIni = PassoX(�ltPas)
            YIni = PassoY(�ltPas)
        End If
        Borda.Refresh
        'D! = Sqr((DueloMod.J(vz).X - XCen) ^ 2 + (DueloMod.J(vz).Y - YCen) ^ 2)
        d! = Distancia(J(vz).X, J(vz).Y, XHex, YHex)
        If (d! > Alcance(J(vz).ArmaM�o) And Alcance(J(vz).ArmaM�o) <> 0) _
        Or (Tiro = conJediPega And d! > 5) _
        Or (Comando = CmdDardosMisticos And d! > 12) Then
            color = QBColor(12)
        Else
            color = QBColor(9)
        End If
        CentroHex XIni, YIni, XCIni, YCIni
        Mapa.Borda.Line (XCIni, YCIni)-(XCen, YCen), color
    End If
    xanterior = XCen
    yanterior = YCen
    Borda.AutoRedraw = True
End If
   
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
Call Duelando.Form_KeyPress(KeyAscii)
End Sub

Private Sub Borda_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Click = Button

If J(vz).ArmaM�o = 16 Then 'Blaster
    For n = 1 To 3
        Desenho = n + 250
        'Call Obst(Int(Desenho), PassoX(n), PassoY(n), True)
        Call DesHex(PassoX(n), PassoY(n), Mapa, CByte(Desenho), 0)
    Next
End If

'XMouse = Int(X) + 1
'YMouse = Int(Y) + 1
HexClick X, Y, XMouse, YMouse
If Shift = vbAltMask Then Strafe = True Else Strafe = False
End Sub

Private Sub MiniMapa_DblClick()
frmMiniMapa.Show
End Sub

Private Sub MiniMapa_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
MiniMapaMouseDown Me, Button, Shift, X, Y
'Dim XHex As Integer, YHex As Integer
'If Button = 1 Then
'    HexClick X, Y, XHex, YHex
'    SL = Int(XHex - (Borda.ScaleWidth / 2))
'    ST = Int(YHex - (Borda.ScaleHeight / 2))
'    If SL < 0 Then SL = 0
'    If ST < 0 Then ST = 0
'    If SL > HScroll.Max Then SL = HScroll.Max
'    If ST > VScroll.Max Then ST = VScroll.Max
'    HScroll.Value = SL
'    VScroll.Value = ST
'End If
End Sub

Private Sub ProxFoto_Click()
    Foto = Foto + 1
End Sub
'
'Public Sub ReDimensiona(Onde As Object)
''Ha um sub EXATAMENTE IGUAL a este no Mapa/Editor
''Eles devem permanecer iguais
'Dim LargMax As Single, Larg As Single
''LargMax = numero de hexagonos em 25 u(unidades da escala)
''Larg = largura em u do total de hexagonos do mapa
'If Onde.Name = "Editor" Then Editor.Dimens�es.Caption = str$(xM) + " x " + str$(yM)
'With Onde
'    'BordaInv.Width = xM * 22 + 2
'    'BordaInv.Height = yM * 18 + 2
'    'Borda.Width = 25 * 22 + 2
'    'Borda.Height = 25 * 18 + 2
'    'If xM < 25 Then Borda.Width = xM * 22 + 2
'    'If yM < 25 Then Borda.Height = yM * 18 + 2
'    LargMax = 25 / (1.5 * L) '((25 + 1) * 1.5 * L)
'    Larg = ((xM + 1) * 1.5 * L)
'    BordaInv.Width = Larg * 22 + 2
'    BordaInv.Height = (yM + 1) * 22 + 2
'    Borda.Width = 25 * 22 + 2
'    Borda.Height = 21 * 22 + 2
'    If xM < LargMax Then Borda.Width = BordaInv.Width
'    If yM < 21 Then Borda.Height = BordaInv.Height
'    HScroll.Value = 0
'    VScroll.Value = 0
'    HScroll.Top = Borda.Height + 1
'    If Onde.Name = "Editor" Then
'        VScroll.Left = Borda.Width + 1 + 136
'    Else
'        VScroll.Left = Borda.Width + 1
'    End If
'    'HScroll.Max = xM - 25
'    HScroll.Max = xM - Int(LargMax) '+ L
'    VScroll.Max = yM - 21 + 1
'    If HScroll.Max > 1 Then HScroll.LargeChange = (HScroll.Max - 1) * 28 / xM
'    If VScroll.Max > 1 Then VScroll.LargeChange = (VScroll.Max - 1) * 21 / yM
'    If HScroll.Max <= 0 Then
'        HScroll.Visible = False
'        HScroll.Max = 0
'        HScroll.Value = 0
'    Else
'        HScroll.Visible = True
'    End If
'    If VScroll.Max <= 0 Then
'        VScroll.Visible = False
'        VScroll.Max = 0
'        VScroll.Value = 0
'    Else
'        VScroll.Visible = True
'    End If
'    MiniMapa.Width = xM + 2
'    MiniMapa.Height = yM + 2
'End With
'End Sub

Sub TestaScroll(X, Y, Quanto)
Exit Sub 'este sub est� todo podre
CentroHex Int(X), Int(Y), X, Y

'If X > ((25 + 1) * 1.5 * L) Then '25
If X > 25 Then '25
    Borda.AutoRedraw = True
    novoh = (Borda.ScaleLeft / (1.5 * L)) + Quanto
    If novoh > HScroll.Max Then novoh = HScroll.Max
    HScroll.Value = novoh
    Borda.AutoRedraw = False
ElseIf Y > 20 Then '21
    Borda.AutoRedraw = True
    novov = Borda.ScaleTop + Quanto
    If novov > VScroll.Max Then novov = VScroll.Max
    VScroll.Value = novov
    Borda.AutoRedraw = False
ElseIf X < 3 Then
    Borda.AutoRedraw = True
    novoh = (Borda.ScaleLeft / (1.5 * L)) - Quanto
    If novoh < 0 Then novoh = 0
    HScroll.Value = novoh
    Borda.AutoRedraw = False
ElseIf Y < 2 Then
    Borda.AutoRedraw = True
    novov = Borda.ScaleTop - Quanto
    If novov < 0 Then novov = 0
    VScroll.Value = novov
    Borda.AutoRedraw = False
End If

End Sub

Private Sub VScroll_Change()
VScrollChange Me
'Borda.ScaleTop = VScroll.Value
'MiniMapa.Cls
'MiniMapa.PaintPicture BordaInv.Image, 0, 0, MiniMapa.ScaleWidth, MiniMapa.ScaleHeight
'MiniMapa.Line (Borda.ScaleLeft, Borda.ScaleTop)-(Borda.ScaleLeft + Borda.ScaleWidth, Borda.ScaleTop + Borda.ScaleHeight), , B
'Borda.PaintPicture BordaInv.Image, 0, 0 ', , , , , 25, 25
''Clipboard.SetData Borda.Image, 2
''Borda.PaintPicture Clipboard.GetData, 0, 0
''Call ReDesenha
End Sub

Private Sub HScroll_Change()
HScrollChange Me
'Borda.ScaleLeft = HScroll.Value * 1.5 * L
'MiniMapa.Cls
'MiniMapa.PaintPicture BordaInv.Image, 0, 0, MiniMapa.ScaleWidth, MiniMapa.ScaleHeight
'MiniMapa.Line (Borda.ScaleLeft, Borda.ScaleTop)-(Borda.ScaleLeft + Borda.ScaleWidth, Borda.ScaleTop + Borda.ScaleHeight), , B
'Borda.PaintPicture BordaInv.Image, 0, 0 ', , , , , 25, 25
''Clipboard.SetData Borda.Image, 2
''Borda.PaintPicture Clipboard.GetData, 0, 0
''Call ReDesenha
End Sub

Private Sub Timer_Timer()
eveTimer = True
Timer.Enabled = False
Timer.Interval = 0
End Sub

Public Function NomeConteudo(n As Byte)
Dim str As String
Select Case n
    Case 0: str = ""
    Case 1: str = "Parede"
    Case 2: str = "Sangue"
    Case 3: str = "Espingarda"
    Case 4: str = "Metralhadora"
    Case 5: str = "Parade de Tijolos"
    Case 6: str = "" '6 = Mina Ativada no jogo
    Case 7: str = "Pacote de Minas"
    Case 8: str = "Lan�a-Foguetes"
    Case 9: str = "" '9 = Passagem secreta no jogo
    Case 10: str = "Passagem Secreta" 'No editor
    Case 11: str = "Rifle"
    Case 12: str = "Degeneradora"
    Case 13: str = "Granada(s)"
    Case 14: str = "Granada Ativada" '14 = Granada Ativada
    Case 15: str = "Kit de 1�s Socorros"
    Case 16: str = "Corpo"
    Case 17: str = "Disco de Predador"
    Case 18: str = "Sangue de Predador"
    Case 19: str = "Canh�o-Auto"
    Case 20: str = "Elevador"
    Case 21: str = "Lan�a-Chamas"
    Case 22: str = "Fuma�a"
    Case 23: str = "Cartucho de Rev�lver"
    Case 24: str = "Buraco Invis�vel" 'No editor
    Case 25: str = "" '25 = Buraco invis�vel no jogo
    Case 26: str = "Buraco Vis�vel"
    Case 27: str = "Ricocheteadora"
    Case 28: str = "Capacete"
    Case 29: str = "�culos Infravermelho"
    Case 30: str = "Bomba"
    Case 31: str = "Fogo"
    Case 32: str = "Lata"
    Case 33: str = "Mina Ativada" 'No editor
    Case 34: str = "Vidro"
    Case 35: str = "Teletransporte"
    Case 36: str = "Escuro"
    Case 37: str = "Paralisante"
    Case 38: str = "Arma Teletransportadora"
    Case 39: str = "Caixa de Surpresas"
    Case 40: str = "Fogo Inapag�vel"
    Case 41: str = "LMT Ultra-Blaster"
    Case 42: str = "Sangue Alien�gena"
    Case 43: str = "Buraco Prov�vel" 'No editor
    Case 44: str = "Grade"
    Case 45: str = "" '45 = Parasita
    Case 46: str = "Colete"
    Case 47: str = "Parede Indestrut�vel"
    Case 48: str = "Porta"
    Case 49: str = "Chave"
    Case 50: str = "" '50 = porta aberta
    Case 51: str = "Aparelho de Holografia"
    Case 52: str = "Elevador (Sobe)"
    Case 53: str = "Elevador (Desce)"
    Case 54: str = "Campo Minado" 'No editor
    Case 55: str = "Metralhadora Militar"
    Case 56: str = "Detector de Dist�cia"
    Case 57: str = "Trabuco"
    Case 58: str = "Caixa de Armas"
    Case 59: str = "Moto"
End Select
NomeConteudo = str
End Function
