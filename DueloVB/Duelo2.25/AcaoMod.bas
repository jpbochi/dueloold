Attribute VB_Name = "AcaoMod"
Option Explicit
DefInt A-Z

Public Sub A��o()
Dim xal, yal, a, N, cont, ProxVisao, xe, ye, queda, ad, temp
Dim coisa$
Dim bobo As Single, d As Single, chance As Single, mir As Single, v As Single
Dim Mensagem As String, Bot�es As VbMsgBoxStyle, T�tulo As String
Dim angulo As Single
Dim xCvz As Single, yCvz As Single
Dim XCAlvo As Single, YCAlvo As Single
Dim XAlvo As Integer, YAlvo As Integer

    xal = XMouse: yal = YMouse
    
    If J(vz).Poder = pWolverine And J(vz).Snikt = True Then
        'MsgMuda ("Voc� est� com as garras pra fora.")
        MsgMuda "Voc� est� com as garras pra fora."
    ElseIf J(vz).AtvGran = True And Comando <> CmdArremessar Then 'Arremessar = False Then
        'MsgMuda ("Voc� tem uma granada na m�o.")
        MsgMuda "Voc� tem uma granada na m�o."
    ElseIf Comando = CmdControlar Then 'Controlar = True Then
        Controlar
    ElseIf Comando = CmdIdentificar Then 'Identificar = True Then
        Identificar
    ElseIf Comando = CmdHolografar Then 'Holografar Then
        Holografar
    ElseIf Comando = CmdDardosMisticos Then
        DardosMisticos
    ElseIf Comando = CmdAnimarMortos Then 'Animar Mortos
        AnimarMortos
    ElseIf Comando = CmdRelampago Then 'Rel�mpago
        Relampago
    ElseIf Comando = CmdArremessar Then 'Arremessar = True Then
        Arremessar
    ElseIf J(vz).T < Tempo Then
        Call SemTempo("atirar.")
    ElseIf J(vz).ManaF� - Mana(enuBolaDeFogo) < 0 And J(vz).ArmaM�o = 22 Then
        MsgMuda "Sem mana."
    ElseIf J(vz).Mun(J(vz).ArmaM�o) = 0 And J(vz).ArmaM�o <> 22 Then
        MsgMuda "Sem muni��o."
    ElseIf J(vz).Canh�o < 0 And (J(vz).ArmaM�o = 20 Or J(vz).ArmaM�o = 21) Then
        MsgMuda "O canh�o est� sendo recarregado."
    ElseIf J(vz).ArmaM�o = 27 And Trabuco = 1 Then
        MsgMuda "Seu trabuco est� descarregado."
    Else
        ContSons = ContSons + 1
        RepSom(ContSons) = SomPad(J(vz).ArmaM�o)
        If Aut > 1 Then RepSom(ContSons) = SomRaj(J(vz).ArmaM�o)
        If RepSom(ContSons) = 0 Then ContSons = ContSons - 1
        
        Do 'Para a rajada
            'Mapa.som.Action = 7 'Som do tiro
            If J(vz).ArmaM�o = 16 Then 'Blaster
                XAlvo = PassoX(1)
                YAlvo = PassoY(1)
                �ltPas = 1
            Else
                XAlvo = XMouse
                YAlvo = YMouse
            End If
            'D = Sqr((J(vz).X - XAlvo) ^ 2 + (J(vz).Y - YAlvo) ^ 2)
            CentroHex J(vz).X, J(vz).Y, xCvz, yCvz
            CentroHex XAlvo, YAlvo, XCAlvo, YCAlvo
            d = Sqr((xCvz - XCAlvo) ^ 2 + (yCvz - YCAlvo) ^ 2)
            angulo = Atn((YCAlvo - yCvz) / (XCAlvo - xCvz + 0.0001))
            If XCAlvo < xCvz Then angulo = angulo + pi
            ProxVisao = CInt(angulo / (pi / 6))
            If ProxVisao < 0 Then ProxVisao = 12 + ProxVisao
            
            Parou = False
            If ProxVisao <> J(vz).Vis�o And J(vz).Poder <> pRobocop And J(vz).Poder <> pMosca And J(vz).Poder <> pTanque And J(vz).ArmaM�o <> 7 Then Call Vira(False, ProxVisao)
            If Parou = True Then
                RepSom(ContSons) = 0 'Apaga o som do tiro
                ContSons = ContSons - 1
                Exit Sub 'se v� outro no meio do giro o tiro � cancelado
            End If
            
            If J(vz).T < Tempo Then
                Call SemTempo("atirar.")
                If ContSons > 0 Then
                    RepSom(ContSons) = 0 'Apaga o som do tiro
                    ContSons = ContSons - 1
                End If
                Exit Sub
            End If
            
            v = superrnd(0.8 / K)     'eis a mira!! (mude a const. 0.8 se quiser, mas mude tamb�m em outra linha mais abaixo)
            If J(vz).Poder = pSabata And J(vz).ArmaM�o = 1 And d <= 12 Then
            ElseIf J(vz).Poder = pSabata And J(vz).ArmaM�o = 5 Then
            Else
                angulo = angulo + v
            End If
            
            Foto = Foto + 1
            J(vz).RepA��oOQue(Foto) = 5
            J(vz).RepA��oQ3(Foto) = angulo
            J(vz).RepA��oQ4(Foto) = J(vz).ArmaM�o
            GravaQuemViu True
            
            Fogo angulo, J(vz).X, J(vz).Y, J(vz).Z, J(vz).ArmaM�o, CInt(d), False
            
            J(vz).Mun(J(vz).ArmaM�o) = J(vz).Mun(J(vz).ArmaM�o) - 1
            If J(vz).ArmaM�o = 22 Then J(vz).ManaF� = J(vz).ManaF� - Mana(enuBolaDeFogo) ' + 1
            If J(vz).ArmaM�o = 20 Or J(vz).ArmaM�o = 21 Then J(vz).Canh�o = -J(vz).Canh�o
            If J(vz).ArmaM�o = 27 Then Trabuco = 1
            If Aut >= 0 Then Aut = Aut - 1
            If J(vz).Mun(J(vz).ArmaM�o) = 0 And Aut > 0 Then
                MsgMuda "Sem muni��o."
                Aut = 0
            End If
            If Aut > 0 Then Call TestaEncontro
        Loop While Aut > 0
        J(vz).T = J(vz).T - Tempo
        Call TestaMortes
        If J(vz).Morte Then Call PassaVez
        Call Duelando.ReEscreve
        If J(vz).Poder = pPredador And J(vz).Camufl > J(vz).CamuflNatural Then
            ' qdo o predador atira, perde sua camuflagem
            J(vz).Camufl = J(vz).CamuflNatural
            Call ApagaCursor(vz)
            Call Cursor(vz): Mapa.Borda.Refresh
            Call Duelando.ReEscreve
            temp = J(vz).Camufl: J(vz).Camufl = J(vz).Camufl / DesCam(J(vz).ArmaM�o)
            Call TestaEncontro
            J(vz).Camufl = temp
        Else
            temp = J(vz).Camufl: J(vz).Camufl = J(vz).Camufl / DesCam(J(vz).ArmaM�o)
            Call TestaEncontro
            J(vz).Camufl = temp
        End If
    End If
    Comando = 0 'Arremessar = False
    K = 0: Aut = 0
    If J(vz).ArmaM�o = 16 Then 'blaster
        For N = 1 To 3
            'Call Obst(Conte�do(PassoX(n), PassoY(n), J(vz).Z), PassoX(n), PassoY(n), True)
            Call Obst(PassoX(N), PassoY(N), J(vz).Z, PassoX(N), PassoY(N), True)
        Next
        For N = 1 To 3
            PassoX(N) = 0
            PassoY(N) = 0
        Next
        �ltPas = 0
    End If

'End If
Call TestaMortes
End Sub

Private Sub Controlar()
Dim a, N
Dim bobo As Single, d As Single, chance As Single
    Comando = 0 'Controlar = False
    For a = 1 To NJogs
        If J(a).X = XMouse And J(a).Y = YMouse And J(a).Z = J(vz).Z And a <> vz And Est�Vendo(CByte(a)) Then
            bobo = Rnd
            d = Distancia(J(vz).X, J(vz).Y, J(a).X, J(a).Y) 'Sqr((J(vz).X - J(a).X) ^ 2 + (J(vz).Y - J(a).Y) ^ 2)
            chance = (0.25) ^ (d / 15) 'a 'B' de dist�ncia a chance � '0.A'
            '           A   ^ (d / B )
            'If J(a).Poder = pEthereal Then chance = chance / 2
            chance = chance / J(a).MagicRes
            If Mid(J(a).Nome, Len(J(a).Nome)) = "]" Then
                MsgMuda "O " + ArrumaNome(J(a).Nome) + " j� est� sendo controlado."
            ElseIf bobo <= chance Then
                MsgMuda "Voc� CONSEGUIU controlar o " + ArrumaNome(J(a).Nome) + " !" + vbLf + "Chance:" + CStr(Int(chance * 100) / 100)
                J(a).Nome = ArrumaNome(J(vz).Nome) + "[" + ArrumaNome(J(a).Nome) + "]" & vbNullChar
                N = N + 1 '?????? linha bizarra
                J(vz).T = J(vz).T - 2
                Duelando.ControlePsi�nico.HelpContextID = 1
                Foto = Foto + 1
                J(vz).RepA��oOQue(Foto) = 7: J(vz).RepA��oQ1(Foto) = J(a).X: J(vz).RepA��oQ2(Foto) = J(a).Y
                GravaQuemViu False
            Else
                MsgMuda "Voc� N�O conseguiu controlar o " + ArrumaNome(J(a).Nome) + " !" + vbLf + "Chance:" + CStr(Int(chance * 100) / 100)
                J(vz).T = J(vz).T - 2
                Duelando.ControlePsi�nico.HelpContextID = 1
                Foto = Foto + 1
                J(vz).RepA��oOQue(Foto) = 7: J(vz).RepA��oQ1(Foto) = J(a).X: J(vz).RepA��oQ2(Foto) = J(a).Y
                GravaQuemViu False
            End If
        End If
    Next
End Sub

Private Sub Identificar()
Dim a
Dim Mensagem As String
    Comando = 0 'Identificar = False
    For a = 1 To NJogs
        If J(a).X = XMouse And J(a).Y = YMouse And J(a).Z = J(vz).Z _
        And (JaVi(a) = 1 Or JaVi(a) = 2) Then
            Mensagem = "O " & ArrumaNome(J(a).Nome) & " � um " & PoderBytStr(J(a).Poder) & " !"
            Mensagem = Mensagem & " E est� resistindo " & CStr(J(a).Ht) & "."
            MsgMuda Mensagem
            J(vz).T = J(vz).T - 2
            J(vz).ManaF� = J(vz).ManaF� - Mana(enuIdentificacao)
            Foto = Foto + 1
            J(vz).RepA��oOQue(Foto) = 7
            J(vz).RepA��oQ1(Foto) = J(a).X
            J(vz).RepA��oQ2(Foto) = J(a).Y
            Call TestaEncontro 'GravaQuemViu
        End If
    Next
End Sub

Private Sub Holografar()
Dim a, cont
Dim d As Single
     Comando = 0 'Holografar = False
     d = Sqr((J(vz).X - XMouse) ^ 2 + (J(vz).Y - YMouse) ^ 2)
     If J(vz).Poder = pMago Then d = 0 'Mago tem alacnce ilimitado
     cont = Conte�do(XMouse, YMouse, J(vz).Z)
     If cont = 1 Or cont = 5 Or cont = 9 Or cont = 32 Or cont = 34 Or cont = 35 Or cont = 44 Or cont = 47 Or cont = 48 Then
         MsgMuda "Voc� n�o pode por sua holografia a�."
     ElseIf d > 6 Then
         MsgMuda "Fora do alcance."
     Else
         J(vz).XHol = XMouse
         J(vz).YHol = YMouse
         J(vz).ZHol = J(vz).Z
         MsgMuda "Voc� intalou sua holografia aqui."
         If J(vz).Poder = pMago Then
             J(vz).ManaF� = J(vz).ManaF� - Mana(enuHolografia)
         Else
             J(vz).Hols = J(vz).Hols - 1
         End If
         J(vz).T = J(vz).T - 2
         Call Duelando.ReEscreve
         Foto = Foto + 1
         J(vz).RepA��oOQue(Foto) = 7
         J(vz).RepA��oQ1(Foto) = XMouse
         J(vz).RepA��oQ2(Foto) = YMouse
         Call TestaEncontro 'GravaQuemViu
    End If

End Sub

Private Sub DardosMisticos()
Dim a As Byte, numDardos, maxDardos
Dim Mensagem As String
Dim xCvz As Single, yCvz As Single
Dim XCAlvo As Single, YCAlvo As Single
Dim XAlvo As Integer, YAlvo As Integer
Dim d!, angulo!

Comando = 0
For a = 1 To NJogs
    XAlvo = J(a).X: YAlvo = J(a).Y
    If J(a).X = XMouse And J(a).Y = YMouse And J(a).Z = J(vz).Z _
    And (JaVi(a) = 1 Or JaVi(a) = 2) Then
        CentroHex J(vz).X, J(vz).Y, xCvz, yCvz
        CentroHex XAlvo, YAlvo, XCAlvo, YCAlvo
        d = Sqr((xCvz - XCAlvo) ^ 2 + (yCvz - YCAlvo) ^ 2)
        'angulo = Atn((YCAlvo - yCvz) / (XCAlvo - xCvz + 0.0001))
        angulo = Atn((yCvz - YCAlvo) / (xCvz - XCAlvo + 0.0001))
        If d <= 12 Then
            maxDardos = Min((J(vz).ManaF� \ Mana(enuDardosMisticos)), 3)
            Mensagem = "Quantos dardos voc� quer lan�ar?" & vbCrLf _
                    & "O m�ximo � " & maxDardos _
                    & " (" & Mana(enuDardosMisticos) & " mana/dardo)"
            numDardos = Val(InputBox(Mensagem))
            If numDardos > 0 And numDardos <= maxDardos Then
                'TO DO: escrever c�digo
                
                GoSub FazBalaca
                
                Dor a, numDardos / J(a).MagicRes, 0
                      
                J(vz).T = J(vz).T - 2
                J(vz).ManaF� = J(vz).ManaF� - numDardos * Mana(enuDardosMisticos)
                
                Foto = Foto + 1
                J(vz).RepA��oOQue(Foto) = 5 'replay como se fosse um tiro
                J(vz).RepA��oQ1(Foto) = XAlvo
                J(vz).RepA��oQ2(Foto) = YAlvo
                J(vz).RepA��oQ3(Foto) = angulo
                J(vz).RepA��oQ4(Foto) = 2 'igual � espingarda
                Call TestaEncontro
            End If
        Else
            MsgMuda "Fora do alcance."
        End If
    End If
Next
Exit Sub
FazBalaca:
    Dim w As Single
    Dim Xt As Single, Yt As Single
    Dim intXt As Integer, intYt As Integer
    Dim Vel As Integer, passo As Integer
    Dim M As Single, i As Integer
    
    Mapa.Borda.AutoRedraw = False
    Vel = CInt(numDardos * (1.01 ^ Config.VelTiro))
    passo = 0
    For w = 0 To 1 Step 0.002 / d 'o mesmo do tiro
        Xt = (1 - w) * xCvz + w * XCAlvo
        Yt = (1 - w) * yCvz + w * YCAlvo
        
        i = -numDardos \ 2 'to numdardos \2
        Do
            If Not (numDardos Mod 2 = 0 And i = 0) Then
                M = i * (1.1 ^ d) * (w * (1 - w))
                Mapa.Borda.PSet (Xt + M * Cos(angulo + pi / 2), Yt + M * Sin(angulo + pi / 2)), QBColor(12)
            End If
            i = i + 1
        Loop Until i > numDardos \ 2
        
        passo = passo + 1: If passo = CInt(&H7FFF) Then passo = 0
        If passo Mod Vel = 0 Then TerminaEspera: IniciaEspera 1
    Next
    Mapa.Borda.AutoRedraw = True
    Mapa.Borda.Refresh
Return
End Sub

Private Sub AnimarMortos()
Dim a, N
Dim d As Single
    Comando = 0
    d = Sqr((J(vz).X - XMouse) ^ 2 + (J(vz).Y - YMouse) ^ 2)
    If d > 10 Then
        MsgMuda "Fora do alcance."
    ElseIf Conte�do(XMouse, YMouse, J(vz).Z) <> 16 Then
        MsgMuda "Voc� precisa de um corpo."
    Else
        N = 0
        For a = 1 + M To NMons + M
            If J(a).Morte Then N = a: Exit For
        Next
        If N = 0 Then
            If NMons = MM Then
                MsgMuda "Limite de monstros excedido!" + Chr$(13) + "Sinto muito."
                Exit Sub
            Else
                NMons = NMons + 1
                N = NMons + M
            End If
        End If
        J(vz).T = J(vz).T - 2.5
        J(vz).ManaF� = J(vz).ManaF� - F�("Animar")
        Conte�do(XMouse, YMouse, J(vz).Z) = 0
        JaVi(N) = 1: J(N).Time = vz
        J(N).Nome = "Zumbi": J(N).Cur = 5: J(N).Morte = False
        J(N).Ht = 10: J(N).HtMax = 10: J(N).Reg = 1: J(N).RegNatural = 1: J(N).Blindagem = 0
        J(N).TMax = 4: J(N).Mira = 0.6
        J(N).Olho = 1: J(N).Camufl = 0.8
        J(N).ArmaM�o = 24 'estrelinhas
        J(N).X = XMouse: J(N).Y = YMouse: J(N).Z = J(vz).Z
        MsgMuda "Aprecie o seu novo zumbi."
        Call Duelando.ReEscreve
        Foto = Foto + 1
        J(vz).RepA��oOQue(Foto) = 7
        J(vz).RepA��oQ1(Foto) = XMouse
        J(vz).RepA��oQ2(Foto) = YMouse
        Call TestaEncontro
    End If
End Sub

Private Sub Relampago()
Dim a, cont, xe, ye, queda, ad
Dim d As Single, angulo As Single, mir As Single
    Comando = 0
    d = Sqr((J(vz).X - XMouse) ^ 2 + (J(vz).Y - YMouse) ^ 2)
    cont = Conte�do(XMouse, YMouse, J(vz).Z)
    If d > 25 Then
        MsgMuda "Fora do alcance."
    Else
        angulo = Atn((YMouse - J(vz).Y) / (XMouse - J(vz).X + 0.0001))
        If XMouse - J(vz).X < 0 Then angulo = angulo + pi
        'ProxVisao = CInt(Angulo / (pi / 4))
        ProxVisao = CInt(angulo / (pi / 6))
        If ProxVisao < 0 Then ProxVisao = 12 + ProxVisao
        If ProxVisao <> J(vz).Vis�o And J(vz).Poder <> pMosca Then Call Vira(False, ProxVisao)
        If J(vz).T < 3 Then
            Call SemTempo("conjurar um rel�mpago.")
            Exit Sub
        End If
        Do
            mir = 0.04 'Quanto menor, maior � a precis�o
            xe = Int(superrnd(0.5 + d * mir) + 0.5)
            ye = Int(superrnd(0.5 + d * mir) + 0.5)
            'xe = Int(superrnd(0.5 + d * 0.1) + 0.5)
            'ye = Int(superrnd(0.5 + d * 0.1) + 0.5)
            If XMouse + xe <= xM And XMouse + xe > 0 And YMouse + ye <= yM And YMouse + ye > 0 Then
                queda = Conte�do(XMouse + xe, YMouse + ye, J(vz).Z)
                If J(vz).Poder = pZerg And (queda = 45 Or queda = 25 Or queda = 26) Then queda = 1 '45 � parasita;25, 26= buracos; parasitas n�o substituem parasitas; o "1" � s� para enganar
                If queda <> 5 And queda <> 9 And queda <> 34 And queda <> 47 And queda <> 48 Then
                    J(vz).T = J(vz).T - 3
                    J(vz).ManaF� = J(vz).ManaF� - F�("Rel�mpago")
                    
                    Foto = Foto + 1
                    J(vz).RepA��oOQue(Foto) = 7
                    J(vz).RepA��oQ1(Foto) = XMouse + xe
                    J(vz).RepA��oQ2(Foto) = YMouse + ye
                    Call TestaEncontro 'GravaQuemViu
                    For ad = 0 To AndMax
                        Call Explos�o(XMouse + xe, YMouse + ye, 0, 0, CByte(ad), 6, 3.1, 1)
                    Next
                    Call TestaEncontro 'GravaQuemViu
                    Call Duelando.ReEscreve
                    Call Cursor(vz)
                    Call TestaMortes
                    Exit Do
                End If
            End If
        Loop
    End If
End Sub

Private Sub Arremessar()
Dim a, cont, xe, ye, queda, ad
Dim d As Single, angulo As Single, mir As Single
Dim coisa$
    Comando = 0 'Arremessar = False
    d = Sqr((J(vz).X - XMouse) ^ 2 + (J(vz).Y - YMouse) ^ 2)
    cont = Conte�do(XMouse, YMouse, J(vz).Z)
    coisa$ = "a granada"
    If J(vz).Poder = pZerg Then coisa$ = "o parasita"
    
    If d > J(vz).For�a Then
        MsgMuda "Fora do alcance."
    ElseIf cont = 1 Or cont = 5 Or cont = 9 Or cont = 32 Or cont = 34 Or cont = 20 Or cont = 52 Or cont = 53 Or cont = 35 Or cont = 44 Or cont = 47 Or cont = 48 Then
        MsgMuda "Voc� n�o pode jogar " + coisa$ + " a�."
    ElseIf J(vz).Poder = pZerg And cont = 26 Then 'Buraco vis�vel
        MsgMuda "Voc� n�o pode jogar " + coisa$ + " a�."
    Else
        angulo = Atn((YMouse - J(vz).Y) / (XMouse - J(vz).X + 0.0001))
        If XMouse - J(vz).X < 0 Then angulo = angulo + pi
        ProxVisao = CInt(angulo / (pi / 6))
        If ProxVisao < 0 Then ProxVisao = 12 + ProxVisao
        If ProxVisao <> J(vz).Vis�o And J(vz).Poder <> pMosca Then Call Vira(False, ProxVisao)
        If J(vz).Poder = pZerg And J(vz).T < 1 Then
            Call SemTempo("atirar seu parasita a�.")
            Exit Sub
        ElseIf J(vz).Poder <> pZerg And J(vz).T < 2 Then
            Call SemTempo("atirar sua granada.")
            Exit Sub
        End If
        Do
            mir = 0.07 'Quanto menor, maior � a precis�o
            xe = Int(superrnd(0.5 + d * mir) + 0.5)
            ye = Int(superrnd(0.5 + d * mir) + 0.5)
            'xe = Int(superrnd(0.5 + d * 0.1) + 0.5)
            'ye = Int(superrnd(0.5 + d * 0.1) + 0.5)
            If XMouse + xe <= xM And XMouse + xe > 0 And YMouse + ye <= yM And YMouse + ye > 0 Then
                queda = Conte�do(XMouse + xe, YMouse + ye, J(vz).Z)
                If J(vz).Poder = pZerg And (queda = 45 Or queda = 25 Or queda = 26) Then queda = 1 '45 � parasita;25, 26= buracos; parasitas n�o substituem parasitas; o "1" � s� para enganar
                If queda <> 1 And queda <> 5 And queda <> 9 And queda <> 20 And queda <> 52 And queda <> 53 And queda <> 32 And queda <> 34 And queda <> 35 And queda <> 44 And queda <> 47 And queda <> 48 Then
                    If Not (J(vz).Poder = pZerg And (queda = 24 Or queda = 25)) Then
                        'Se cair dentro do mapa e n�o em uma parede indestrut�vel nem elevador, ent�o ela explode ali
                        Conte�do(XMouse + xe, YMouse + ye, J(vz).Z) = 14
                        If J(vz).Poder = pZerg Then Conte�do(XMouse + xe, YMouse + ye, J(vz).Z) = 45
                        
                        'Call Obst(Conte�do(XMouse + xe, YMouse + ye, J(vz).Z), XMouse + xe, YMouse + ye, True)
                        Call Obst(XMouse + xe, YMouse + ye, J(vz).Z, XMouse + xe, YMouse + ye, True)
                        If J(vz).Poder = pZerg Then
                            J(vz).T = J(vz).T - 1
                            J(vz).Parasitas = False
                        Else
                            J(vz).T = J(vz).T - 2
                            J(vz).AtvGran = False
                            Explodir = True
                        End If
                        
                        Foto = Foto + 1
                        J(vz).RepA��oOQue(Foto) = 6
                        J(vz).RepA��oQ1(Foto) = XMouse + xe
                        J(vz).RepA��oQ2(Foto) = YMouse + ye
                        J(vz).RepA��oQ3(Foto) = Conte�do(XMouse + xe, YMouse + ye, J(vz).Z)
                        GravaQuemViu False
                        
                        Call Duelando.ReEscreve
                        Call Cursor(vz)
                        Exit Do
                    End If
                End If
            End If
        Loop
    End If

End Sub

Public Sub Fogo(angulo As Single, XIni As Integer, YIni As Integer, Zini As Byte, ArmaUsada As Byte, Dist As Single, Monstro As Boolean)
Dim a, Alc, cont, N, p, xsort, ysort, zsort, i, dx, dy, perf, tip
Dim spalh As Single, chance As Single, r As Single, v As Single, Dano As Single, d As Single, raiodocara As Single, absor As Single, bl As Single
Dim BateuGrade As Boolean, mudan�a As Boolean, ExplFog As Boolean
Dim w As Double, wZero(1 To 9) As Single, stepe As Double, Vel As Single, passo(1 To 9) As Integer
Dim Apanhou(1 To 9) As Boolean, perto(1 To 9) As Byte, Desviou As Boolean
Dim Xt(1 To 9) As Integer, Yt(1 To 9) As Integer, Xtant(1 To 9) As Integer, Ytant(1 To 9) As Integer, Xtnint(1 To 9) As Double, Ytnint(1 To 9) As Double
Dim xTiro(1 To 9) As Single, yTiro(1 To 9) As Single, ixTiro As Integer, iyTiro As Integer
Dim XAlvo As Integer, YAlvo As Integer
Dim xRast As Single, yRast As Single, corRast As Long
Dim ric As Integer, angRic As Byte
Dim AngE(1 To 9) As Single, acabou(1 To 9) As Boolean, Estilha�os As Byte, e As Byte
Dim xCperto As Single, yCperto As Single
Dim Danos(M + MM) As Single
Dim tmr As Single
Erase Danos, passo, wZero
'xcOg = Xini - 0.5
'ycOg = Yini - 0.5
'xcAl = xal - 0.5
'ycAl = yal - 0.5
'zeta = 0
Estilha�os = 1 'alterar estilhacos => atualizar help
If ArmaUsada = 2 Then Estilha�os = 5: spalh = 1 / 8 'espingarda
If ArmaUsada = 10 Then Estilha�os = 3: spalh = 1 / 15 'lan�a-chamas
If ArmaUsada = 18 Then Estilha�os = 3: spalh = 1 / 15 'espinhos do zerg
If ArmaUsada = 27 Then Estilha�os = 9: spalh = 1 / 3 'trabuco
For a = 1 To Estilha�os
    'HexClick XIni, YIni, Xtant(a), Ytant(a)
    Xtant(a) = XIni
    Ytant(a) = YIni
Next
AngE(1) = angulo + superrnd(spalh / 4)
AngE(2) = angulo + superrnd(spalh / 4) - spalh
AngE(3) = angulo + superrnd(spalh / 4) + spalh
AngE(4) = angulo + superrnd(spalh / 4) - spalh / 2
AngE(5) = angulo + superrnd(spalh / 4) + spalh / 2
AngE(6) = angulo + superrnd(spalh / 8) - spalh / 4
AngE(7) = angulo + superrnd(spalh / 8) + spalh / 4
AngE(8) = angulo + superrnd(spalh / 16) - spalh / 8
AngE(9) = angulo + superrnd(spalh / 16) + spalh / 8
If Estilha�os = 1 Then AngE(1) = angulo
CentroHex XIni, YIni, xTiro(1), yTiro(1)
For e = 2 To Estilha�os
    xTiro(e) = xTiro(1)
    yTiro(e) = yTiro(1)
Next
'xtiro = XIni
'ytiro = YIni
ric = 0
'xtnintant = xtiro: ytnintant = ytiro
'If Disco = 1 Then J(vz).Mun( ArmaUsada) = J(vz).Mun( ArmaUsada) - 1
If Alcance(ArmaUsada) <> 0 Then  'armas q. tem curto alcance
    Alc = Alcance(ArmaUsada)
Else
    Alc = 90
End If
'stepe = 0.003 '* Sqr(Estilha�os) * (Config.VelTiro / 10) '0.03
stepe = 0.002 '0.003
Vel = CInt(Estilha�os * (1.01 ^ Config.VelTiro))
eveTimer = True

Mapa.Borda.AutoRedraw = False
For w = 0 To Alc Step stepe
    For e = 1 To Estilha�os Step 1
        If Not acabou(e) Then
            If passo(e) Mod Vel = 0 And Not Monstro Then TerminaEspera: IniciaEspera 1
            
            Xtnint(e) = xTiro(e) + (w + wZero(e)) * Cos(AngE(e)) '- 0.5
            Ytnint(e) = yTiro(e) + (w + wZero(e)) * Sin(AngE(e)) '- 0.5
            'Xt(e) = Int(Xtnint(e)) + 1
            'Yt(e) = Int(Ytnint(e)) + 1
            HexClick CSng(Xtnint(e)), CSng(Ytnint(e)), Xt(e), Yt(e)
            
            If Xt(e) <> Xtant(e) Or Yt(e) <> Ytant(e) Then 'Testa se mudou de quadrado
                If Xt(e) < 1 Or Xt(e) > xM Or Yt(e) < 1 Or Yt(e) > yM Then 'Testa se o tiro saiu do mapa
                    'If Xtant(e) <> XIni Or Ytant(e) <> YIni Then
                        cont = Conte�do(Xtant(e), Ytant(e), Zini)
                        'arma 14 = baforada
                        If ArmaUsada = 14 And cont <> 25 And cont <> 26 Then Conte�do(Xtant(e), Ytant(e), Zini) = 31
                        If ArmaUsada = 7 And cont <> 47 And cont <> 48 Then Conte�do(Xtant(e), Ytant(e), Zini) = 17 'Disco
                        
                        Mapa.Borda.AutoRedraw = True
                        'Call Obst(Conte�do(Xtant(e), Ytant(e), Zini), Xtant(e), Ytant(e), True)
                        Call Obst(Xtant(e), Ytant(e), Zini, Xtant(e), Ytant(e), True)
                        Cursor vz
                        Mapa.Borda.AutoRedraw = False
                        acabou(e) = True
                        'Xt(e) = Xtant(e): Yt(e) = Ytant(e)
                        Exit For
                    'End If
                End If
                
                Call Mapa.TestaScroll(Xt(e) - Mapa.Borda.ScaleLeft, Yt(e) - Mapa.Borda.ScaleTop, 20)
                'If xt(e) - Mapa.Borda.ScaleLeft < 1 Or xt(e) - Mapa.Borda.ScaleLeft > 25 Or yt(e) - Mapa.Borda.ScaleTop < 1 Or yt(e) - Mapa.Borda.ScaleTop > 25 Then
                '    Call Centra(xt(e), yt(e), xt(e), yt(e))
                'End If
                
                perto(e) = 0: Apanhou(e) = False 'Testa se chegou no quadrado de alguem
                For N = 1 To NJogs
                    If Xt(e) = J(N).X And Yt(e) = J(N).Y And Zini = J(N).Z Then
                        perto(e) = N
                        If J(perto(e)).Enterrado = True Then '� imposs�vel acertar um zerg enterrado
                            perto(e) = 0
                        ElseIf (J(perto(e)).Rastejando = True Or (J(perto(e)).Poder = pAlien And J(perto(e)).AlienGrande = False)) And J(vz).Poder <> pSabata Then
                            'H� uma chance de o tiro passar por cima de quem est� abaixado; O Alien na forma de aranha tamb�m � baixo
                            'd = Sqr((Xini - xt(e)) ^ 2 + (Yini - yt(e)) ^ 2)
                            'erro = superrnd(0.8 / K) 'eis a mira!! (mude a const. 0.8 se quiser)
                            'If (erro * d) > 0.001 Then perto = 0 'e=e+1
                            If Est�Vendo(perto(e)) Then chance = 0.4 Else chance = 0.6
                            If Rnd < chance Then perto(e) = 0
                        End If
                    ElseIf Xt(e) = J(N).XHol And Yt(e) = J(N).YHol And Zini = J(N).ZHol Then
                        J(N).ZHol = 255: ViHol(N) = False 'Desativa a holografia
                        If Not Monstro Then
                            MsgMuda "O tiro atingiu uma holografia !!"
                            Mapa.Borda.AutoRedraw = True
                            'Call Obst(Conte�do(Xt(e), Yt(e), Zini), Xt(e), Yt(e), True)
                            Call Obst(Xt(e), Yt(e), Zini, Xt(e), Yt(e), True)
                            Mapa.Borda.AutoRedraw = False
                        End If
                    End If
                Next
                For N = 1 + M To NMons + M
                    If Xt(e) = J(N).X And Yt(e) = J(N).Y And Zini = J(N).Z Then
                        perto(e) = N
                        Exit For
                        'GoSub AtingeMonstro
                        'perto = Empty
                    End If
                Next N
            End If
            
            If Monstro = False Then 'DESENHA TIRO
                passo(e) = passo(e) + 1
                If passo(e) Mod 10 = 0 Then
                    If passo(e) = 32767 Then passo(e) = 0
                    r = Dmg(ArmaUsada) / 10
                    If r < 0.1 Then r = 0.1
                    If w > r Then
                        xRast = xTiro(e) + (w + wZero(e) - r) * Cos(AngE(e)) '- 0.5
                        yRast = yTiro(e) + (w + wZero(e) - r) * Sin(AngE(e)) '- 0.5
                        Select Case Config.RastroTiro
                            Case True: corRast = QBColor(7)
                            Case False: corRast = Mapa.BordaInv.Point(xRast, yRast)
                        End Select
                        Mapa.Borda.PSet (xRast, yRast), corRast
                    End If
                    Mapa.Borda.PSet (Xtnint(e), Ytnint(e)), QBColor(12)
                    'zeta = zeta + 1
                    'If Int(zeta / 20) = zeta / 20 Then Mapa.Borda.Refresh
                End If
            End If
        
            'Se est� perto de algu�m, pode ter atingido
            If perto(e) <> 0 Then GoSub AtingePessoaSePoss�vel
            
            'Baforada - p�e fogo
            If ArmaUsada = 14 And (Xt(e) <> Xtant(e) Or Yt(e) <> Ytant(e)) Then
                cont = Conte�do(Xtant(e), Ytant(e), Zini)
                If cont <> 25 And cont <> 26 Then
                    Conte�do(Xtant(e), Ytant(e), Zini) = 31 'p�e fogo mas sem tapar buracos
                    Mapa.Borda.AutoRedraw = True
                    'Call Obst(Conte�do(Xtant(e), Ytant(e), Zini), Xtant(e), Ytant(e), True)
                    Call Obst(Xtant(e), Ytant(e), Zini, Xtant(e), Ytant(e), True)
                    If Xtant(e) = XIni And Ytant(e) = YIni Then Call Cursor(vz) 'Esta estava fora deste if mas entrou aqui por teste 'Desenha o cursor enquanto o tiro anda
                    Mapa.Borda.AutoRedraw = False
                End If
            End If
          
            'Teletransportadora
            cont = Conte�do(Xt(e), Yt(e), Zini)
            If ArmaUsada = 15 And (Xt(e) <> Xtant(e) Or Yt(e) <> Ytant(e)) And (cont = 1 Or cont = 5 Or cont = 9 Or cont = 20 Or cont = 52 Or cont = 53 Or cont = 32 Or cont = 34 Or cont = 35 Or cont = 44) Then
                ContSons = ContSons + 1
                RepSom(ContSons) = Sons.TeleTrans
                Do
                    Do
                        p = 0
                        xsort = Int(Rnd * xM + 1)
                        ysort = Int(Rnd * yM + 1)
                        zsort = Int(Rnd * (AndMax + 1))
                        For a = 1 To NJogs
                            If xsort = J(a).X And ysort = J(a).Y And zsort = J(a).Z Then p = 1
                        Next
                    Loop While p = 1
                    If Conte�do(xsort, ysort, zsort) = 0 Then
                        Conte�do(xsort, ysort, zsort) = Conte�do(Xt(e), Yt(e), Zini)
                        If zsort = Zini Then
                            Mapa.Borda.AutoRedraw = True
                            'Call Obst(0, Xt(e), Yt(e), True)
                            'Call Obst(Conte�do(xsort, ysort, zsort), Int(xsort), Int(ysort), True)
                            Call ApagaHex(Xt(e), Yt(e), Mapa, 0, False)
                            Call Obst(xsort, ysort, zsort, Int(xsort), Int(ysort), True)
                            Mapa.Borda.AutoRedraw = False
                        End If
                        Exit Do
                    End If
                Loop
                Conte�do(Xt(e), Yt(e), Zini) = 0
                acabou(e) = True 'w = 100
            End If
            
            'Blaster
            If ArmaUsada = 16 And w > Dist And �ltPas < 4 Then
                If XAlvo <> PassoX(�ltPas + 1) Or YAlvo <> PassoY(�ltPas + 1) Then
                    w = 0
                    'xTiro = CInt(Xtnint(e) + 0.5)
                    'yTiro = CInt(Ytnint(e) + 0.5)
                    'HexClick CSng(Xtnint(e)), CSng(Ytnint(e)), ixTiro, iyTiro
                    'xTiro = ixTiro: yTiro = iyTiro
                    xTiro(e) = Xtnint(e)
                    yTiro(e) = Ytnint(e)
                    �ltPas = �ltPas + 1
                    XAlvo = PassoX(�ltPas)
                    YAlvo = PassoY(�ltPas)
                    HexClick CSng(Xtnint(e)), CSng(Ytnint(e)), ixTiro, iyTiro
                    Dist = Distancia(ixTiro, iyTiro, XAlvo, YAlvo)
                        'Sqr((xTiro - XAlvo) ^ 2 + (yTiro - YAlvo) ^ 2)
                    AngE(e) = funAngulo(ixTiro, iyTiro, XAlvo, YAlvo)
                        'Atn((YAlvo - yTiro) / (XAlvo - xTiro + 0.0001))
                    'If XAlvo < CInt(xTiro) Then AngE(e) = AngE(e) + pi
                    v = superrnd(0.8 / K)     'eis a mira!! (mude a const. 0.8 se quiser, mas mude tamb�m em outras linhas)
                    AngE(e) = AngE(e) + v
                Else
                    �ltPas = 4
                End If
            End If
            
            'Testa se acertou um teletransporte
            If Conte�do(Xt(e), Yt(e), Zini) = 35 Then
                ContSons = ContSons + 1
                RepSom(ContSons) = Sons.TeleTrans
                Do
                    xTiro(e) = Rnd * xM + 1
                    yTiro(e) = Rnd * yM + 1
                    AngE(e) = Rnd * 2 * pi
                    CentroHex xTiro(e), yTiro(e), xTiro(e), yTiro(e)
                    'cont = Conte�do(Int(xTiro(e) - 0.5) + 1, Int(yTiro(e) - 0.5) + 1, Zini)
                    cont = Conte�do(CInt(xTiro(e)), CInt(yTiro(e)), Zini)
                    If cont <> 5 And cont <> 9 And cont <> 34 And cont <> 47 And cont <> 48 Then Exit Do
                Loop
                'If w > 1 Then
                If w + wZero(e) > 1 Then
                    'w = 0
                    For i = 1 To Estilha�os
                        If i <> e Then wZero(i) = wZero(i) + w
                    Next
                    w = 0
                End If
                r = Empty
                For r = 60 To 1 Step -1
                    Mapa.Borda.Circle (xTiro(e), yTiro(e)), r / 100, QBColor(9)
                    For a = 1 To 10000: Next
                    If r / 5 = Int(r / 5) Then Mapa.Borda.Refresh
                Next
                'Mapa.Borda.Refresh
            End If
            
            'Testa se atingiu uma parede (ou grade)
            If Xt(e) <> Xtant(e) Or Yt(e) <> Ytant(e) Then
                If Conte�do(Xt(e), Yt(e), Zini) = 44 Then
                    BateuGrade = False
                    If (Rnd <= 0.1 Or RaioExpl(ArmaUsada) > 2) Then BateuGrade = True
                    If J(vz).Poder = pSabata And Monstro = False Then BateuGrade = False
                End If
                cont = Conte�do(Xt(e), Yt(e), Zini)
                If cont = 5 Or cont = 34 Or cont = 47 Or cont = 48 Or BateuGrade = True Then 'Testa se acertou uma parede
                    If ArmaUsada = 11 And ric <= 20 Then 'Ricocheteadora
                        If Xt(e) <> Xtant(e) Or Yt(e) <> Ytant(e) Then
                            'Mapa.Borda.Refresh
                            ric = ric + 1
                            If w > 1 Then w = 0
                            mudan�a = True
                            'xTiro = Xtnint(e) + 0.5
                            'yTiro = Ytnint(e) + 0.5
                            'HexClick CSng(Xtnint(e)), CSng(Ytnint(e)), ixTiro, iyTiro
                            'xTiro = ixTiro: yTiro = iyTiro
                            xTiro(e) = Xtnint(e)
                            yTiro(e) = Ytnint(e)
                            dx = Xt(e) - Xtant(e)
                            dy = Yt(e) - Ytant(e)
                            'If dx = 1 And dy = 0 Then
                            '    Angulo = Angulo - 2 * (Angulo - pi / 2)
                            'ElseIf dx = 0 And dy = 1 Then
                            '    Angulo = Angulo + 2 * (pi - Angulo)
                            'ElseIf dx = -1 And dy = 0 Then
                            '    Angulo = Angulo - 2 * (Angulo - pi / 2)
                            'ElseIf dx = 0 And dy = -1 Then
                            '    Angulo = Angulo + 2 * (pi - Angulo)
                            'Else 'diagonal
                            '    Angulo = Angulo + pi + (Rnd - 0.5)
                            'End If
                            If Xtant(e) Mod 2 = 0 Then
                                If dx = 1 And dy = 0 Then angRic = 1
                                If dx = 1 And dy = 1 Then angRic = 255 'Impossivel
                                If dx = 0 And dy = 1 Then angRic = 3
                                If dx = -1 And dy = 1 Then angRic = 255 'Impossivel
                                If dx = -1 And dy = 0 Then angRic = 5
                                If dx = -1 And dy = -1 Then angRic = 7
                                If dx = 0 And dy = -1 Then angRic = 9
                                If dx = 1 And dy = -1 Then angRic = 11
                            Else
                                If dx = 1 And dy = 0 Then angRic = 11
                                If dx = 1 And dy = 1 Then angRic = 1
                                If dx = 0 And dy = 1 Then angRic = 3
                                If dx = -1 And dy = 1 Then angRic = 5
                                If dx = -1 And dy = 0 Then angRic = 7
                                If dx = -1 And dy = -1 Then angRic = 255 'Impossivel
                                If dx = 0 And dy = -1 Then angRic = 9
                                If dx = 1 And dy = -1 Then angRic = 255 'Impossivel
                            End If
                            'Mapa.Caption = "ang=" + CStr(angRic) + "; X=" + CStr(Xt(e)) + "; dx=" + CStr(dx) + "; dy=" + CStr(dy) + "; angtiro=" + CStr(AngE(e))
                            Select Case angRic
                                'Case n: AngE(e) = 2 * pi - AngE(e) + 2 * ang da parede
                                'Obs: angulos positivos em sentido HORARIO
                                Case 1, 7: AngE(e) = 2 * pi - AngE(e) - 2 * (pi / 3)
                                Case 3, 9: AngE(e) = 2 * pi - AngE(e)
                                Case 5, 11: AngE(e) = 2 * pi - AngE(e) - 4 * (pi / 3)
                            End Select
                        End If
                    ElseIf Xt(e) <> XIni Or Yt(e) <> YIni Then
                        acabou(e) = True 'w = 100
                        ExplFog = True
                        If cont <> 47 And cont <> 48 Then
                            If ArmaUsada = 7 Then 'Disco
                                Mapa.Borda.Refresh
                                Mapa.Borda.AutoRedraw = True
                                Conte�do(Xt(e), Yt(e), Zini) = 17
                                'Call Obst(Conte�do(Xt(e), Yt(e), Zini), Xt(e), Yt(e), True)
                                Call Obst(Xt(e), Yt(e), Zini, Xt(e), Yt(e), True)
                            ElseIf ArmaUsada = 21 Then 'AP
                                Mapa.Borda.Refresh
                                Mapa.Borda.AutoRedraw = True
                                Conte�do(Xt(e), Yt(e), Zini) = 22 'fuma�a
                                'Call Obst(Conte�do(Xt(e), Yt(e), Zini), Xt(e), Yt(e), True)
                                Call Obst(Xt(e), Yt(e), Zini, Xt(e), Yt(e), True)
                            End If
                        End If
                        Mapa.Borda.AutoRedraw = False
                    End If
                ElseIf cont = 1 Or cont = 9 Then 'Testa se acertou uma parede podre ou passagem
                    ExplFog = True
                    If RaioExpl(ArmaUsada) = 0 Then 'As explosivas demolem a parede depois
                        Conte�do(Xt(e), Yt(e), Zini) = 22 'Fuma�a
                        If ArmaUsada = 10 Then Conte�do(Xt(e), Yt(e), Zini) = 31 'Fogo
                        Mapa.Borda.Refresh
                        Mapa.Borda.AutoRedraw = True
                        'Call Obst(Conte�do(Xt(e), Yt(e), Zini), Xt(e), Yt(e), True)
                        Call Obst(Xt(e), Yt(e), Zini, Xt(e), Yt(e), True)
                    End If
                    If ArmaUsada = 7 And perf = 3 Then 'Disco
                        acabou(e) = True 'w = 100
                        Conte�do(Xt(e), Yt(e), Zini) = 17
                        'Call Obst(Conte�do(Xt(e), Yt(e), Zini), Xt(e), Yt(e), True)
                        Call Obst(Xt(e), Yt(e), Zini, Xt(e), Yt(e), True)
                    End If
                    If ArmaUsada = 21 And perf <= 2 Then
                        perf = perf + 1
                    ElseIf ArmaUsada = 7 And perf <= 2 Then 'Disco
                        perf = perf + 1
                    ElseIf ArmaUsada = 14 Then 'Nada, � baforada
                    Else
                        acabou(e) = True 'w = 100
                    End If
                    Mapa.Borda.AutoRedraw = False
                ElseIf Conte�do(Xt(e), Yt(e), Zini) = 32 Then 'Testa se acertou uma lata
                    ExplFog = True
                    Conte�do(Xt(e), Yt(e), Zini) = 0 'Some com a lata
                    Mapa.Borda.AutoRedraw = True
                    Mapa.Borda.Refresh
                    Call Explos�o(Xt(e), Yt(e), 0, 0, Zini, 6, 3.1, 1)
                    If ArmaUsada = 7 And perf = 3 Then 'Disco
                        acabou(e) = True 'w = 100
                        Mapa.Borda.AutoRedraw = False
                        Conte�do(Xt(e), Yt(e), Zini) = 17
                        'Call Obst(Conte�do(Xt(e), Yt(e), Zini), Xt(e), Yt(e), True)
                        Call Obst(Xt(e), Yt(e), Zini, Xt(e), Yt(e), True)
                    End If
                    If ArmaUsada = 21 And perf <= 2 Then
                        perf = perf + 1
                    ElseIf ArmaUsada = 7 And perf <= 2 Then 'disco
                        perf = perf + 1
                    Else
                        acabou(e) = True 'w = 100
                    End If
                    Mapa.Borda.AutoRedraw = False
                End If
            End If
            If RaioExpl(ArmaUsada) <> 0 And ExplFog = True Then 'Explosao
                Mapa.Borda.AutoRedraw = True
                Mapa.Borda.Refresh
                tip = 1
                If ArmaUsada = 22 Then
                    tip = 2
                End If
                Call Explos�o(Xt(e), Yt(e), Xtant(e), Ytant(e), Zini, Dmg(ArmaUsada), RaioExpl(ArmaUsada), Int(tip))
                acabou(e) = True 'w = 100
                Mapa.Borda.AutoRedraw = False
            End If
            
            Xtant(e) = Xt(e): Ytant(e) = Yt(e)
        End If
    Next e
Next w

If Estilha�os > 1 Then
    For a = 1 To NMons + M
        If Danos(a) <> 0 Then Dano = Dor(CByte(a), Danos(a), J(a).Blindagem)
    Next
End If

J(vz).RepA��oQ1(Foto) = Xt(1)
J(vz).RepA��oQ2(Foto) = Yt(1)

ExplFog = Empty
perf = Empty
Mapa.Borda.Refresh
Mapa.Borda.AutoRedraw = True
'If J(vz).Morte = True Then PassaVez

Exit Sub '#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@
'#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#
AtingePessoaSePoss�vel: '@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@
'D = Sqr((Xtnint(e) - (J(perto(e)).X - 0.5)) ^ 2 + (Ytnint(e) - (J(perto(e)).Y - 0.5)) ^ 2)
CentroHex J(perto(e)).X, J(perto(e)).Y, xCperto, yCperto
d = Sqr((Xtnint(e) - xCperto) ^ 2 + (Ytnint(e) - yCperto) ^ 2)
raiodocara = (J(perto(e)).Raio / 100) * 2 * cR3S2 * L

If Conte�do(Xt(e), Yt(e), Zini) = 9 Then raiodocara = 2 * L 'isso serve para eliminar um defeito: caras em pass. secretas s�o quase invulner�veis
If Xt(e) = J(perto(e)).X And Yt(e) = J(perto(e)).Y And Zini = J(perto(e)).Z And Apanhou(e) = False And d <= raiodocara Then
    Apanhou(e) = True: Desviou = False
    absor = 0
    If ArmaUsada = 7 Then 'Disco
        absor = (4 / 3) * perf
    ElseIf ArmaUsada = 21 Then 'Canh�o AP
        absor = 4 * perf
    End If
    If Conte�do(J(perto(e)).X, J(perto(e)).Y, Zini) = 9 Then
        If RaioExpl(ArmaUsada) = 0 Then Conte�do(J(perto(e)).X, J(perto(e)).Y, Zini) = 22
        absor = absor + 1.5
    End If
    
    If J(perto(e)).Poder = pAgente_do_Matrix And J(perto(e)).TMax >= 1.5 And Est�SendoVistoPor(perto(e)) And ArmaUsada <> 10 And ArmaUsada <> 14 Then
        If Not Monstro Then MsgMuda "Ele desviou do tiro !!!"
        J(perto(e)).TMax = J(perto(e)).TMax - 1.5
        Desviou = True: perto(e) = False
    ElseIf RaioExpl(ArmaUsada) = 0 Then
        If (ArmaUsada = 10 Or ArmaUsada = 14) And (J(perto(e)).Poder = pRobocop Or J(perto(e)).Poder = pDragao Or J(perto(e)).Poder = pTanque) Then
            'Nada, eles s�o imunes a fogo
        ElseIf J(perto(e)).Poder = pJedi And J(perto(e)).ArmaM�o = 23 And J(perto(e)).TMax >= 1.5 And Est�SendoVistoPor(perto(e)) And ArmaUsada <> 7 And ArmaUsada <> 10 And ArmaUsada <> 14 Then
            'If Not Monstro Then MsgMuda ("Ele defendeu o tiro com a espada !!!")
            'O jedi est� vendo; disco(7), lanca-chamas(10) e baforada(14) ele n�o defende
            If Not Monstro Then MsgMuda "Ele defendeu o tiro com a espada !!!"
            J(perto(e)).TMax = J(perto(e)).TMax - 1.5
        Else
            bl = 1
            'D = Sqr((XIni - J(perto(e)).X) ^ 2 + (YIni - J(perto(e)).Y) ^ 2)
            d = Distancia(XIni, YIni, J(perto(e)).X, J(perto(e)).Y)
            If ArmaUsada = 8 Then 'Tri-laser
                bl = 1 / 2
            ElseIf J(vz).Poder = pSabata And Monstro = False And ((ArmaUsada = 1 And d <= 12) Or ArmaUsada = 5) Then
                bl = 1 / 3
            End If
            If (ArmaUsada = 10 Or ArmaUsada = 14) And J(perto(e)).Poder = pT_1000 Then bl = bl * 2
            If ArmaUsada = 15 Then bl = 0
            If Estilha�os = 1 Then
                Dano = Dor(CInt(perto(e)), Dmg(ArmaUsada), absor + bl * J(perto(e)).Blindagem)
            Else
                Danos(perto(e)) = Danos(perto(e)) + Dmg(ArmaUsada)
            End If
        End If
    End If
    If ArmaUsada = 10 Then Conte�do(J(perto(e)).X, J(perto(e)).Y, J(perto(e)).Z) = 31 'lan�a-chamas p�e fogo
    If (ArmaUsada = 10 Or ArmaUsada = 14) And (J(perto(e)).Poder = pRobocop Or J(perto(e)).Poder = pDragao Or J(perto(e)).Poder = pTanque) Then
        'Nada, eles s�o imunes a fogo
    ElseIf Dano > 0 Then
        J(perto(e)).Reg = J(perto(e)).Reg - Degen(ArmaUsada)
    End If
    If ArmaUsada = 13 Then 'Paralizante; lembre-se que paralizante n�o d� dano
        J(perto(e)).TMax = J(perto(e)).TMax - 1
        If J(perto(e)).TMax < 1.25 Then J(perto(e)).TMax = 1.25
    End If
    If Dano > 0 And ArmaUsada = 15 Then
        'MsgMuda ("O " + Nome$(perto(e)) + " foi teletransportado!")
        MsgMuda "O " + ArrumaNome(J(perto(e)).Nome) + " foi teletransportado!"
        Call Teletransporta(perto(e))
        'Do
        '    Xtt = Int(Rnd * xM) + 1: Ytt = Int(Rnd * yM) + 1
        '    Ztt = CInt(Rnd * (AndMax + 1) - 0.5)
        '    Vis�ott = Int(Rnd * 8)
        '    p = 0
        '    For a = 1 To NJogs '- 1
        '        d = Sqr((J(a).X - Xtt) ^ 2 + (J(a).Y - Ytt) ^ 2 + ((CInt(J(a).Z) - Ztt) * AlturaAndar) ^ 2)
        '        If d < DMin Then p = 1
        '        'Obs: ele mant�m dist�ncia at� de onde ele est�
        '    Next
        'Loop Until Conte�do(Xtt, Ytt, Ztt) = 0 And p = 0
        'X(perto) = Xtt: Y(perto) = Ytt: Z(perto) = Ztt
        'Vis�o(perto) = Vis�ott
        JaVi(perto(e)) = 0: Call TestaEncontro
    End If
    
    If ArmaUsada <> 7 And Not Desviou Then acabou(e) = True: ExplFog = True
End If
Return

End Sub

