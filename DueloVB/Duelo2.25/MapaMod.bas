Attribute VB_Name = "MapaMod"
Option Explicit
DefInt A-Z
Dim bolRePinta As Boolean
Public Const EditorWidth = 750
Public Const EditorHeight = 540
Public Const MapaWidth = 574
Public Const MapaHeight = 543

Public Sub ReDimensiona(Onde As Object)
Dim LargMax As Single, Larg As Single
Dim ScaleX As Single, ScaleY As Single
'LargMax = numero de hexagonos em 25 u(unidades da escala)
'Larg = largura em u do total de hexagonos do mapa
If Onde.Name = Editor.Name Then Editor.Dimens�es.Caption = str$(xM) & " x " & str$(yM)
bolRePinta = False
With Onde
    If Onde.Name = Editor.Name Then
        .Borda.Height = 464 + (Onde.ScaleHeight - EditorHeight)
        .Borda.Width = 584 + (Onde.ScaleWidth - EditorWidth)
    Else
        .Borda.Height = 464 + (Onde.ScaleHeight - MapaHeight)
        .Borda.Width = 552 + (Onde.ScaleWidth - MapaWidth)
    End If
    ScaleX = .Borda.ScaleWidth '25
    ScaleY = .Borda.ScaleHeight '21
    '.Borda.Width = Int(ScaleX) * 22 + 2
    .Borda.Width = CInt((ScaleX / (1.5 * L))) * (1.5 * L) * 22 + 2
    .Borda.Height = Int(ScaleY) * 22 + 2
    ScaleX = .Borda.ScaleWidth
    ScaleY = .Borda.ScaleHeight
    LargMax = ScaleX / (1.5 * L)
    Larg = ((xM + 1) * 1.5 * L)
    .BordaInv.Width = Larg * 22 + 2
    .BordaInv.Height = (yM + 1) * 22 + 2
    If xM < LargMax Then .Borda.Width = .BordaInv.Width
    If yM < ScaleY Then .Borda.Height = .BordaInv.Height
    .VScroll.Height = .Borda.Height
    .HScroll.Width = .Borda.Width
    .HScroll.Top = .Borda.Height + 1 + .Borda.Top
    .VScroll.Left = .Borda.Width + 1 + .Borda.Left
    .HScroll.Min = Int(LargMax - 0.5)
    .VScroll.Min = ScaleY
    .HScroll.Max = xM '- Int(LargMax) '+ L
    .VScroll.Max = yM + 1 '- 21
    .HScroll.LargeChange = .HScroll.Min
    .VScroll.LargeChange = .VScroll.Min
    If .HScroll.Max <= .HScroll.Min Then
        .HScroll.Visible = False
        .HScroll.Max = .HScroll.Min
        .HScroll.Value = .HScroll.Min
    Else
        .HScroll.Visible = True
        .HScroll.Value = .HScroll.Min
    End If
    If .VScroll.Max <= .VScroll.Min Then
        .VScroll.Visible = False
        .VScroll.Max = .VScroll.Min
        .VScroll.Value = .VScroll.Min
    Else
        .VScroll.Visible = True
        .VScroll.Value = .VScroll.Min
    End If
    .MiniMapa.Width = xM
    .MiniMapa.Height = yM
    frmMiniMapa.MiniMapa.Width = xM * 2
    frmMiniMapa.MiniMapa.Height = yM * 2
    frmMiniMapa.Width = (2 * xM + 6) * Screen.TwipsPerPixelX
    frmMiniMapa.Height = (2 * yM + 19 + 6) * Screen.TwipsPerPixelY
End With
RePinta Onde
bolRePinta = True
End Sub

Public Sub VScrollChange(Onde As Object)
With Onde
    .Borda.ScaleTop = .VScroll.Value - .VScroll.Min
    RePinta Onde
End With
End Sub

Public Sub HScrollChange(Onde As Object)
With Onde
    .Borda.ScaleLeft = (.HScroll.Value - .HScroll.Min) * 1.5 * L
    RePinta Onde
End With
End Sub

Public Sub MiniMapaMouseDown(Onde As Object, Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim SL, ST
Dim XHex As Integer, YHex As Integer
If Button = 1 Then
    HexClick X, Y, XHex, YHex
    Centra Onde, XHex, YHex
End If
End Sub

Sub Centra(Onde As Object, xA As Integer, yA As Integer, Optional xB As Variant, Optional yB As Variant)
Dim SL, ST
Dim XC As Integer, YC As Integer
If IsMissing(xB) Then
    xB = xA: yB = yA
    'xB = J(vz).X: yB = J(vz).Y
End If
dx = Abs(xB - xA)
dy = Abs(yB - yA)
XC = xA: YC = yA
If dx <= 25 And dy <= 25 Then
    XC = (xA + xB) \ 2
    YC = (yA + yB) \ 2
End If

With Onde
    'SL = Int(XC + .HScroll.Min - (.Borda.ScaleWidth / 2))
    SL = Int(XC + .HScroll.Min - ((.Borda.ScaleWidth / (1.5 * L)) / 2))
    ST = Int(YC + .VScroll.Min - (.Borda.ScaleHeight / 2))
    If SL < .HScroll.Min Then SL = .HScroll.Min
    If ST < .VScroll.Min Then ST = .VScroll.Min
    If SL > .HScroll.Max Then SL = .HScroll.Max
    If ST > .VScroll.Max Then ST = .VScroll.Max
    bolRePinta = False
    .HScroll.Value = SL
    .VScroll.Value = ST
    bolRePinta = True
    RePinta Onde
End With
End Sub

Public Sub RePinta(Onde As Object)
Dim sngX As Single, sngY As Single
If bolRePinta Then
    With Onde
        AtualizaMiniMapa Onde
        'If Onde.Name = Mapa.Name Then
            'CentroHex J(vz).X, J(vz).Y, sngX, sngY
            '.Borda.PaintPicture .BordaInv.Image, sngX - 10, sngY - 10, , , , , sngX + 10, sngY + 10
        'Else
            .Borda.PaintPicture .BordaInv.Image, 0, 0 ', , , , , 25, 25
        'End If
    End With
End If
End Sub

Public Sub AtualizaMiniMapa(Onde As Object)
Dim a, b
With Onde
'    .MiniMapa.Cls
'    .MiniMapa.PaintPicture .BordaInv.Image, 0, 0, .MiniMapa.ScaleWidth, .MiniMapa.ScaleHeight
'    For A = 1 To xM
'        For B = 1 To yM
'            If Conte�do(A, B, Andar) > 0 And Conte�do(A, B, Andar) <> 50 Then 'Porta aberta e' invisivel
'                .MiniMapa.PSet (A, B), Mapa.ObjetoH(Conte�do(A, B, Andar)).Point(2, 10)
'            End If
'        Next
'    Next
'   .MiniMapa.Line (.Borda.ScaleLeft, .Borda.ScaleTop) _
        -(.Borda.ScaleLeft + .Borda.ScaleWidth, .Borda.ScaleTop + .Borda.ScaleHeight), , B
        
    .MiniMapa.Refresh
    .MiniMapa.AutoRedraw = False
    .MiniMapa.Line _
    (.HScroll.Value - .HScroll.LargeChange, .VScroll.Value - .VScroll.LargeChange)- _
    (.HScroll.Value - 2, .VScroll.Value - 2) _
    , 255, B
    .MiniMapa.AutoRedraw = True
    
    
    frmMiniMapa.MiniMapa.Refresh
    frmMiniMapa.MiniMapa.AutoRedraw = False
    frmMiniMapa.MiniMapa.DrawWidth = 1
    frmMiniMapa.MiniMapa.Line _
    (.HScroll.Value - .HScroll.LargeChange, .VScroll.Value - .VScroll.LargeChange)- _
    (.HScroll.Value - 0.5, .VScroll.Value - 1.5) _
    , 255, B
    frmMiniMapa.MiniMapa.DrawWidth = 2
    frmMiniMapa.MiniMapa.AutoRedraw = True
End With
End Sub
