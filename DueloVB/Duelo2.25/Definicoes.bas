Attribute VB_Name = "Definicoes"
Option Base 1

Declare Function PlaySound Lib "winmm.dll" Alias "PlaySoundA" (ByVal lpszName As String, ByVal hModule As Long, ByVal dwFlags As Long) As Long

Public DirDuelo As String
Public Const pi = 3.14159265358979
Public Const cR3S2 = 0.866025403784439
Public Const conJediPega = 4
Public Const cLimX = 60, cLimY = 70
Public Const CmdArremessar = 1, _
             CmdControlar = 2, _
             CmdIdentificar = 3, _
             CmdHolografar = 4, _
             CmdAnimarMortos = 5, _
             CmdRelampago = 6, _
             CmdDardosMisticos = 7

Public Enum enumMagiasArcanas
    enuIdentificacao = 100
    enuPararTempo = 101
    enuInvisibilidade = 102
    enuTeletransporte = 103
    enuImitar = 104
    enuHolografia = 105
    enuBolaDeFogo = 106
    enuDardosMisticos = 107
End Enum

'Const somRev = 1, RajMet = 2, Esping = 3, Soco = 4, Baf = 5
Type typSons
    Morte As Byte: MorteGrande As Byte
    Rev As Byte: RajMet As Byte: Esping As Byte: Baf As Byte: RajMG As Byte: Canhao As Byte: LFoguete As Byte: Laser As Byte: RajLaser As Byte
    Soco As Byte: Espada As Byte: Mordida As Byte: Tarrasque As Byte
    TeleTrans As Byte: queda As Byte: DestroiParede As Byte
    Explo1 As Byte: Explo2 As Byte
End Type
Public Sons As typSons, QuantSom(0 To 255) As Byte, ArqSom(0 To 255) As String
'Public Const SomMorte = 0, somQueda = 150, somExplo1 = 100, somExplo2 = 101

Public MeioL As Double, Alt As Double, L As Double
Enum typOQueFazer
    cAtirar = 1: cAndar = 2: cSubir = 3: cDescer = 4
End Enum
'Enum typEstatus
'    cInicio = 1: cEditor = 2: cNovoJogo = 3: cEstatisticas = 4: cJogadores = 5: cJogando = 6
'End Enum
Type typConfig
    ArqEstatPadrao As String
    VelTiro As Integer
    RastroTiro As Boolean
    VelRotacao As Integer
    VelReplay As Integer
    ComSom As Boolean
    ChanceClasse(1 To 5) As Byte
End Type

Public Const cp = 0.2316419, cc1 = 0.31938153, cc2 = -0.356563782, cc3 = 1.78147937, cc4 = -1.821255978, cc5 = 1.330274429, cd0 = 2.515517, cd1 = 0.802853, cd2 = 0.010328, ce0 = 1, ce1 = 1.432788, ce2 = 0.189269, ce3 = 0.00138
Public Const AlturaAndar = 3
Public Config As typConfig ', Estatus As typEstatus
Public eveTimer As Boolean

Public xM As Integer, yM As Integer, DMin As Single
'Public OrdemCarregar$
Public Arquivo$, ArqEstat$, cod$, Dir$, Tiro As Integer, K As Single, Tempo As Single, Aut As Integer, Strafe As Boolean, Parou As Boolean, TempoParado As Boolean
Public XMouse As Integer, YMouse As Integer, Click As Byte, Explodir As Boolean, AndMax As Byte, Andar As Integer, vz As Byte, PassoX(4) As Byte, PassoY(4) As Byte, �ltPas As Byte, dand As Integer, Trabuco As Integer
Public Conte�do(0 To cLimX, 0 To cLimY, 0 To 9) As Byte
'Public Arremessar As Boolean, Controlar As Boolean, Identificar As Boolean, Holografar As Boolean, Animar As Boolean
Public Comando As Byte
'Comando:
'1 - Arremessar
'2 - Controlar
'3 - Identificar
'4 - Holografar
'5 - Animar Mortos

'obs: deveriam ser booleans: explodir,arremessar,apanhou,...
Public Const NArmas = 28, NPoderes = 34, M = 10, MM = 12
Public Classe(NPoderes) As Byte
Public Pot�ncia(0 To NPoderes) As Single

'Poderes
Public Const pSorteado = 0
Public Const pPM = 1
Public Const pAtirador_de_Elite = 2
Public Const pVelocista = 3
Public Const pCasca_Grossa = 4
Public Const pTroll = 5
Public Const pBoina_Verde = 6
Public Const pPredador = 7
Public Const pWolverine = 8
Public Const pVoador = 9
Public Const pFantasma = 10
Public Const pTarrasque = 11
Public Const pRobocop = 12
Public Const pHomem_do_Raio_X = 13
Public Const pBalacau = 14
Public Const pMosca = 15
Public Const pDragao = 16
Public Const pMago = 17
Public Const pEthereal = 18
Public Const pJason = 19
Public Const pAlien = 20
Public Const pDemolidor = 21
Public Const pMagrao = 22
Public Const pZerg = 23
Public Const pTanque = 24
Public Const pSabata = 25
Public Const pBlindado = 26
Public Const pJedi = 27
Public Const pNinja = 28
Public Const pHorla = 29
Public Const pCapangas = 30
Public Const pDinamitador = 31
Public Const pT_1000 = 32
Public Const pAgente_do_Matrix = 33
Public Const pClerigo_Maligno = 34
Type typPoder
    Sangue As Byte
    NaoSangra As Byte
    NaoPodePegarNada As Boolean
    NaoPodeRastejar As Boolean
    NaoPodeTrocarDeArma As Boolean
    Voa As Boolean
    Flutua As Boolean
    AtravessaGrades As Boolean
    ImuneAFogo As Boolean
    ResistenteAFogo As Boolean
    TemInfravisao As Boolean
    NaoPrecisaSeVirarParaAtirar As Boolean
    'TemMagiasArcanas As Boolean
    'TemMagiasDivinas As Boolean
    'EnxergaAtravesDeParedes As Boolean
    'PodeImitarFormaDeOutros As Boolean
    'PodeExplodirOuImobilizarAoLevarTiro As Boolean
    'TemRegeneracaoT1000 As Boolean
    'VisaoDeMosca As Boolean
End Type
Public PP(0 To NPoderes) As typPoder

'Geral
Public NJogs As Byte, Mortes As Byte
Type typJogador
    'Caracter�sticas Iniciais
    Nome As String * 15
    Time As Byte
    Cor As Byte
    Poder As Byte
    Ht As Single
    HtMax As Single
    HtAnt As Single
    T As Single
    TMax As Single
    Mira As Single
    Reg As Single
    RegNatural As Single
    Camufl As Single
    CamuflNatural As Single
    Olho As Single
    OlhoNatural As Single
    MagicRes As Single
    MagicResNatural As Single
    For�a As Byte
    Blindagem As Single
    Raio As Byte
    Kills As Byte
    'Caracter�sticas Provis�rias (Booleans, em geral)
    Morte As Boolean
    AtvGran As Boolean
    Infra As Boolean
    Rastejando As Boolean
    Im�vel As Boolean
    Enterrado As Boolean
    PossuiChave As Boolean
    Invis As Integer
    Snikt As Boolean
    Moto As Boolean
    'Equipamentos
    Detec As Boolean
    Bombas As Byte
    BombasAtivadas As Byte
    Med As Byte
    Minas As Byte
    Gran As Byte
    Prot As Byte
    Hols As Byte
    Fuma� As Byte
    'S� da bomba
    ContBomba As Integer
    XBomba(5) As Integer
    YBomba(5) As Integer
    ZBomba(5) As Byte
    'S� da holografia
    XHol As Integer
    YHol As Integer
    ZHol As Byte
    'Armas
    ArmaM�o As Byte
    Mun(0 To NArmas) As Integer
    ArmaP(0 To NArmas) As Boolean
    'Posi��o e para onde est� olhando
    Vis�o As Byte
    X As Integer
    Y As Integer
    Z As Byte
    Cur As Long
    'Especiais de Poderes
    Canh�o As Integer
    AlienGrande As Boolean
    Ovo As Integer
    Injetor As Byte
    Parasitas As Boolean
    Parasitado As Boolean
    Olfato As Integer
    ManaF� As Integer
    Imit As Byte
    ViuSemblante(M + MM) As Boolean
    DimiExplodiu As Boolean
    
    'Grava a imagem inicial de cada cara
    RepXIni As Integer
    RepYIni As Integer
    RepZIni As Integer
    RepVisaoIni As Byte
    RepCursorIni As Long
    'Grava cada passo do cara
    RepA��oOQue(0 To 127) As Byte
    RepA��oQ1(0 To 127) As Byte
    RepA��oQ2(0 To 127) As Byte
    RepA��oQ3(0 To 127) As Double
    RepA��oQ4(0 To 127) As Byte
    'Grava se 'A' viu 'B' durante o passo 'n'
    ViuRepF(M + MM, 0 To 127) As Byte
    ViuRep(M + MM) As Boolean
    'PROBLEMAS COM REPLAY DOS MONSTROS
    ''Grava se o monstro 'A' viu algum replay
    MonViuRep As Boolean
    'Public MonViuRep(M + 1 To M + MM) As Boolean
    ''Grava o �ltimo passo que o monstro 'A' viu do 'B'
    ViuRepX(M) As Integer
    ViuRepY(M) As Integer
    'Public ViuRepX(M + 1 To M + MM, M) As Integer
    'Public ViuRepY(M + 1 To M + MM, M) As Integer
    'Fog of War
    Fog(0 To cLimX, 0 To cLimY, 0 To 9) As Byte
    FogBit(0 To cLimX, 0 To cLimY) As Byte
    'FogBitAnt(0 To cLimX, 0 To cLimY) As Byte
End Type

Public J(1 To M + MM) As typJogador
Public JogTmp As typJogador
Public Foto As Integer 'Em que passo do replay est�

''Caracter�sticas Iniciais
'Public Nome(0 To M + MM) As String, cor(M) As Byte, Poder(0 To M + MM) As String, Ht(M + MM) As Single, HtMax(M + MM) As Single, HtAnt(M) As Single, T(M + MM) As Single, TMax(M + MM) As Single, Mira(M + MM) As Single, Reg(M + MM) As Single, RegNatural(M + MM) As Single
'Public Camufl(M + MM) As Single, CamuflNatural(M) As Single, Olho(M + MM) As Single, OlhoNatural(M) As Single, For�a(M) As Byte, Blindagem(M + MM) As Single, Raio(M + MM) As Byte
'Public Kills(M) As Byte
''Caracter�sticas Provis�rias (Booleans, em geral)
'Public Morte(0 To M + MM) As Boolean, AtvGran(M) As Boolean, Infra(M + MM) As Boolean, Rastejando(M + MM) As Boolean, Im�vel(M) As Boolean, Enterrado(M + MM) As Boolean, PossuiChave(M) As Boolean, Invis(M) As Integer, Snikt(M) As Boolean, Moto(M + MM) As Boolean
''Equipamentos
'Public Detec(M) As Boolean, Bombas(M) As Byte, Med(M) As Byte, Minas(M) As Byte, Gran(M) As Byte, Prot(M) As Byte, Hols(M) As Byte, Fuma�(M) As Byte
''S� da bomba
'Public ContBomba(M) As Integer, XBomba(M, 5) As Integer, YBomba(M, 5) As Integer, ZBomba(M, 5) As Byte
''S� da holografia
'Public XHol(M) As Integer, YHol(M) As Integer, ZHol(M + MM) As Byte
''Armas
'Public ArmaM�o(M + MM) As Byte, Mun(M, 0 To NArmas) As Integer, ArmaP(M, 0 To NArmas) As Boolean
''Posi��o e para onde est� olhando
'Public Vis�o(M + MM) As Byte, X(M + MM) As Integer, Y(M + MM) As Integer, Z(M + MM) As Byte, Cur(M + MM) As Long
''Especiais de Poderes
'Public Canh�o(M) As Integer, AlienGrande(M) As Boolean, Ovo(M) As Integer, Injetor(M) As Byte, Parasitas(M) As Boolean, Parasitado(M + MM) As Boolean, Olfato(M) As Integer
'Public ManaF�(M) As Integer, Imit(M + MM) As Byte, ViuSemblante(M + MM, M + MM) As Boolean
'Public DimiExplodiu(M) As Boolean

'REPLAY
'Public Foto As Integer 'Em que passo do replay est�
''Grava a imagem inicial de cada cara
'Public RepXIni(M + MM) As Integer, RepYIni(M + MM) As Integer, RepZIni(M + MM) As Integer, RepVisaoIni(M + MM) As Byte, RepCursorIni(M + MM) As Long
''Grava cada passo do cara
'Public RepA��oOQue(M + MM, 0 To 50) As Byte, RepA��oQ1(M + MM, 0 To 50) As Byte, RepA��oQ2(M + MM, 0 To 50) As Byte, RepA��oQ3(M + MM, 0 To 50) As Double, RepA��oQ4(M + MM, 0 To 50) As Byte
''Grava se 'A' viu 'B' durante o passo 'n'
'Public ViuRepF(M, M + MM, 0 To 50) As Byte
'Public ViuRep(M, M + MM) As Boolean
''Grava se o monstro 'A' viu algum replay
'Public MonViuRep(M + 1 To M + MM) As Boolean
''Grava o �ltimo passo que o monstro 'A' viu do 'B'
'Public ViuRepX(M + 1 To M + MM, M) As Integer
'Public ViuRepY(M + 1 To M + MM, M) As Integer

'S O N S
Public ContSons As Integer, RepSom(1 To 256) As Byte

'MONSTROS
Public NMons As Byte, TipoMons(M + 1 To M + MM) As Byte ', Time(M + 1 To M + MM) As Byte

Public Arma(0 To NArmas) As String, MunA(0 To NArmas) As Integer, _
       KMir(0 To NArmas) As Single, KIns(0 To NArmas) As Single, KAut(0 To NArmas) As Single, _
       DMir(0 To NArmas) As Single, DIns(0 To NArmas) As Single, DAut(0 To NArmas) As Single, _
       TMir(0 To NArmas) As Single, TIns(0 To NArmas) As Single, TAut(0 To NArmas) As Single, _
       DesCam(0 To NArmas) As Single, Dmg(0 To NArmas) As Single, RaioExpl(0 To NArmas) As Single, Degen(0 To NArmas) As Single, Alcance(0 To NArmas), Batida(0 To NArmas), _
       SomPad(0 To NArmas) As Byte, SomRaj(0 To NArmas) As Byte

Public JaVi(M + MM) As Byte     'este array serve p/ evitar q janelas do tipo "Voce viu fulano" apare�am v�rias vezes seguidas
Public ViHol(M) As Boolean      'este array serve p/ distinguir um cara de sua holografia nos testes de encontro
Public Monstro As Boolean
Public dx, dy
Public Atropelar As Boolean        'vz vai bater em algu�m
Public ProxVisao As Integer        'para onde o cara est� se virando
Public ViuBoole As Boolean         'vz viu algu�m
'Public JaViMons(MM) As Byte

'Multi Player
Public Const cServerPorta = 10000
Public Const cClientPorta = 10001

Public Const cConecta = "CON"
Public Const cDesconecta = "DCON"
Public Const cConectado = "CONOK"
Public Const cDesconectado = "DCONOK"
