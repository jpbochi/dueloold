VERSION 5.00
Begin VB.Form frmMiniMapa 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   975
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1260
   Icon            =   "frmMiniMapa.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   65
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   84
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox MiniMapa 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      DrawWidth       =   2
      FillColor       =   &H00FF0000&
      ForeColor       =   &H80000008&
      Height          =   750
      Left            =   0
      ScaleHeight     =   25
      ScaleMode       =   0  'User
      ScaleWidth      =   25
      TabIndex        =   0
      Top             =   0
      Width           =   750
   End
End
Attribute VB_Name = "frmMiniMapa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Paint()
AlwaysOnTop Me, True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Hide
Cancel = True
End Sub

Private Sub MiniMapa_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Mapa.Visible Then
    MiniMapaMouseDown Mapa, Button, Shift, X, Y
Else
    MiniMapaMouseDown Editor, Button, Shift, X, Y
End If
End Sub

