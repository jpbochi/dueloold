VERSION 5.00
Begin VB.Form frmMsg 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DUELO"
   ClientHeight    =   1530
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   4440
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1530
   ScaleWidth      =   4440
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton comNao 
      Caption         =   "N�o"
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Top             =   1080
      Width           =   1215
   End
   Begin VB.CommandButton comSim 
      Caption         =   "Sim"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   1080
      Width           =   1215
   End
   Begin VB.CommandButton comOK 
      Caption         =   "Tudo Certo"
      Height          =   375
      Left            =   1560
      TabIndex        =   0
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label lblMens 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Bla, Bla"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4215
   End
End
Attribute VB_Name = "frmMsg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim varResposta As VbMsgBoxResult

Private Sub comNao_Click()
varResposta = vbNo
Hide
End Sub

Private Sub comSim_Click()
varResposta = vbYes
Hide
End Sub

Private Sub comOK_Click()
varResposta = vbOK
Hide
End Sub

Public Property Get Resposta() As VbMsgBoxResult
Resposta = varResposta
End Property

Public Property Let Resposta(ByVal vNewValue As VbMsgBoxResult)
varResposta = vNewValue
End Property

Private Sub Form_Activate()
If comOK.Visible Then
    comOK.SetFocus
ElseIf comSim.Visible Then
    comSim.SetFocus
End If
End Sub
