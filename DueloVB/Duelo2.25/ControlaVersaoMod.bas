Attribute VB_Name = "ControlaVersaoMod"
Option Explicit

Public Function Versao()
Versao = "2.25"

'2.1    - Foi adicionada esta fun��o de ver a vers�o
'2.2    - Foram substitu�dos os bot�es com textos das
'         janelas Duelando e TrocaArma por bot�es com desenhos
'2.21   - Todos os desehos foram convertidos para GIF
'2.22   - Mago > Magia inserida: Dardos M�sticos
'2.23   - Novas vari�veis inseridas: MagicRes e MagicResNatural
'2.24   - Dados do Help diretamente ligados aos valores usados no jogo
'2.25   - Consertado um bug na cria��o de estat�sticas nulas

End Function
