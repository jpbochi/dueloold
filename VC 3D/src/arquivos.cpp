
#include "arquivos.h"

namespace ARQ {
	const char *IconeMain	= "data\\weapons2.ico";
	const char *PessoaEquip = "data\\SRHumanMerc.gif";
	const char *IconeQ1		= "data\\quake1_logo.gif";
	const char *IconeQ2		= "data\\quake2_logo.gif";
	const char *IconeQ3		= "data\\quake3_logo.gif";
}

bool ExisteArquivo(FXApp *app, FXString arq, bool erro)
{
	return ExisteArquivo(app, arq.text(), erro);
}

bool ExisteArquivo(FXApp *app, const char *arq, bool erro)
{
 	FXFileStream File;
 	if (!File.open(arq, FX::FXStreamLoad)) {
 		if (erro) {
			//FXMessageBox::information(app,MBOX_OK,"A Message from the Baron","You just pressed the button");
			//fxerror("''%s'' n�o pode ser lido! Se certifique que este arquivo existe antes de rodar o %s!", arq, app->getAppName());
			fxwarning("''%s'' n�o pode ser lido! Se certifique que este arquivo existe antes de rodar o %s!", arq, app->getAppName());
			
			//Grande AFFF!
			//as fun��es acima podem causar erros malucos quando executam e acabam n�o mostrando mensagem nenhuma

			app->destroy();
			app->exit(1);
 		}
		return false; //na verdade, este return nunca � executado
 	}
 	File.close();
	return true;	
}

bool ExistemArquivos(FXApp *app)
{
 	if (!ExisteArquivo(app, ARQ::IconeMain)) return false;
 	if (!ExisteArquivo(app, ARQ::PessoaEquip)) return false;
	return true;	
}