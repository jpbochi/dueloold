#include "fx.h"
#include "fx3d.h"

#include "appDuelo.h"
#include "winMain.h"

//Zorn, Zank (Kzan)

#include "declarac.h"
#include "modelosgl.h"
#include "jogo.h"

FXuint gllist[GLList::NUM_LISTS];
FXuint textures[MAX_TEXTURAS];

cJogo Jogo;

#include "arquivos.h"

#ifdef WIN32

// Return 64-bit tick count (ms)
static inline FXlong getticktime(){
  FXlong now;
  GetSystemTimeAsFileTime((FILETIME*)&now);
  return now/10000;
  }

#endif

int main(int argc,char **argv)
{
	#ifdef WIN32
	//srand(timeGetTime()); //randomize
	srand((int)getticktime()); //randomize
	#endif

	appDuelo application("Duelo 3D");
	//FX::FXApp application("Duelo 3D");

	application.init(argc,argv);

	//Testa se todos os arquivos indispens�veis est�o presentes
	if (!ExistemArquivos(&application)) {
		return 1;
	}

	new winMain(&application);
	
	//winJogo *wJ = new winJogo(&application);
	//FXWindow *win = wJ->getParent();
	
	application.create();
	
	//application.addChore(this,ID_CHORE);

	return application.run();
}
