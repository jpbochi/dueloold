
#include "jogo@.h"

bool cJogo::TestaEncontro(bool atualizafog = true, double camvz)
{
	bool ret = false;
	FXString msg;

	if (atualizafog) AtualizaFog();

	//a chance de ver um cara deve depender de quanto tempo se gastou desde o ultimo teste
	short dt = J[vz].TAnt - J[vz].T;
	J[vz].TAnt = J[vz].T;

	if (dt == 0) return false; //otimiza��o, j� que n�o vai ver ningu�m se n�o gastar tempo

	for(int op=0; op<numjogs; op++)
	if (!J[op].GetFlag(Morto)) {
		if (J[op].GetFlag(Morto) || (op == vz)) {
			JaVi[op] = 0;
		} else {
			//TO DO: fazer uma passada extra se o jogador tiver holografia
			bool holografia = false;

			int xop = J[op].x;
			int yop = J[op].y;
			int zop = J[op].z;
			char javiu = holografia ? JaViHol[op] : JaVi[op];
			bool avisar = false;

			if (EstaSendoVistoPor(op)) camvz *= 0.65;

			if (Viu(vz, op, javiu, dt, xop, yop, zop)) {
				if (Viu(op, vz, 0, dt, -1, -1, -1, camvz)
				&&  !holografia
				&&  !TempoParado) { //op e vz se viram; a holografia � cega, s� um cara real pode ver
					if (!EstaVendo(op)) { //se n�o estava vendo, avise
						J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z,
											 J[op].x, J[op].y, J[op].z);
						avisar = true;
						ret = true;
					}
					JaVi[op] = 2;
					if (avisar) MsgDef(msg.format("Voc� viu o %s!", J[J[op].idImit].Nome));
					J[vz].Replay.VeFoto(op);
				} else { //apenas vz viu op
					if (!EstaVendo(op)) { //se n�o estava vendo, avise
						J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z,
											 J[op].x, J[op].y, J[op].z);
						avisar = true;
						ret = true;
					//MODIF: if modificado ver TestaEncontro no Duelo2D
					} else if (EstaSendoVistoPor(op)) { //op perdeu vz de vista
                        /*TO DO: Traduzir isto
						If j(vz).RepA��oOQue(Foto) = 1 Then 'O �ltimo passo era andar
                            Call V�Foto(op, vz)
                            'ViuRepF(op, vz, Foto) = 1
                        End If*/
						J[vz].Replay.VeFoto(op);
					}
					if (holografia) {
						JaViHol[op] = true; //vz viu uma holografia
					} else {
						JaVi[op] = 1;
					}
					if (avisar) MsgDef(msg.format("Voc� viu o %s!", J[J[op].idImit].Nome));
				}
			} else if ((vz != op) && !J[op].GetFlag(Morto)) {
				if (Viu(op, vz, 0, dt, -1, -1, -1, camvz)
				&&  !holografia
				&&  !TempoParado) { //apenas op viu vz
					if (!EstaSendoVistoPor(op) && PodAndaDevagarAoSerVisto(vz)) {
						MsgDef("Algu�m o viu!");
						ret = true;
					}
					JaVi[op] = 3;
					J[vz].Replay.VeFoto(op);
				} else {
					if (EstaSendoVistoPor(op)) { //op perdeu vz de vista
                        /*TO DO: Traduzir isto
						If j(vz).RepA��oOQue(Foto) = 1 Then 'O �ltimo passo era andar
                            Call V�Foto(op, vz)
                            'ViuRepF(op, vz, Foto) = 1
                        End If*/
					}
					JaVi[op] = 0;
				}
			}
		}
	}
	return ret;
}

bool cJogo::EstaVendo(int quem)
{
	return ((JaVi[quem] == 1) || (JaVi[quem] == 2));
}

bool cJogo::EstaSendoVistoPor(int quem)
{
	return ((JaVi[quem] == 2) || (JaVi[quem] == 3));
}

bool cJogo::Viu(int obsador, int obsado, char javiu, short dt,
				int xop, int yop, int zop, double camextra)
{
	bool ret = false;
	bool testar = true;

	if ((obsador == obsado) || J[obsador].GetFlag(Morto)) {
		ret = false;	testar = false;
	} else if (!J[obsador].Bot && J[obsado].Bot) {//pessoa observa bot
		if ((J[obsador].Time == J[obsado].Time)
		|| (PodSempreVeBotsNeutros(obsador) && (J[obsado].Time == -1))) {
			ret = true;	testar = false;
		}
	} else if (J[obsador].Bot && !J[obsado].Bot) {//bot observa pessoa
		/*//MODIF: Antes os bots n�o viam os agentes. Agora eles v�em, mas n�o devem atirar
		if ((J[obsador].Time == J[obsado].Time)
		||  (J[obsador].Time == J[J[obsado].idImit].Time)
		||  ((J[J[obsado].idImit].Poder==pAgente_do_Matrix) && (J[obsador].Time == -1))) {
			ret = false;
			testar = false;
		}*/
	}

	if ((J[obsador].Time == J[obsado].Time)
	||  (J[obsado].GetFlag(Parasitado) && PodSempreVeParasitados(obsador))) {
		ret = true;		testar = false;
	} else if (J[obsador].Posicao == posEnterrado
		   ||  (J[obsado].Posicao == posEnterrado && !PodVisaoRaioX(obsador))) {
		ret = false;	testar = false;
	}

	if ((javiu == 1) || (javiu == 2)) { //se j� estava vendo, continua vendo
		ret = true;		testar = false;
	}

	if (testar) {
		double dist;
		double fx, fy, fz;
		double fxop, fyop, fzop;
		double obsado_camufl;

		obsado_camufl = J[obsado].Camufl;
		if (J[obsado].Posicao == posNaMoto) obsado_camufl = 0.5;
		if (J[obsado].Posicao == posAbaixado) obsado_camufl *= 1.3;
		if (J[obsado].Posicao == posRastejando) obsado_camufl *= 1.6;
		if (J[obsado].Posicao == posEnterrado && PodVisaoRaioX(obsador)) obsado_camufl *= 1.5;
		if (J[obsador].GetFlag(TemIR) && !PodCorpoFrio(obsado)) obsado_camufl /= PodVisaoIR;;
		if (J[obsador].Posicao == posNaMoto) obsado_camufl *= 2.0;
		obsado_camufl *= camextra;

		if (xop == -1) xop = J[obsado].x;
		if (yop == -1) yop = J[obsado].y;
		if (zop == -1) zop = J[obsado].z;

		CentroHex3D(J[obsador].x, J[obsador].y, J[obsador].z, fx, fy, fz, 0.5);
		CentroHex3D(J[obsado].x, J[obsado].y, J[obsado].z, fxop, fyop, fzop, 0.5);
		//dist = Distancia3D(fxop, fyop, fzop, fx, fy, fz);
		cPontoAPonto P;
		P.IniciaAlvo(fx, fy, fz, fxop, fyop, fzop);
		dist = P.GetDistMax();

		bool soradar;
		bool radar = (dist < J[obsador].PodRadar(soradar));

		if (soradar && !radar) {
			return false; //o cara s� tem radar mas o radar n�o alcan�a
		}

		//TO DO: tra�ar v�rias linhas (ao inv�s de uma) para testar se pode ver cada oponente
		/*double dw = 0.2f / dist;
		double w, xw, yw, zw;
		int xi, yi, zi;
		int xiant=-1, yiant=-1, ziant=-1;
		bool ehchao, ehchaoant = true;*/
		cCont cont;
		cChao chao;
		cAfin afin, afinchao;
		bool parede, passaradar, primeirohex = true;
		bool temlinha = true;
		//for (w=0; w<1; w+=dw) {
			//xw = (1 - w) * fx + w * fxop;
			//yw = (1 - w) * fy + w * fyop;
			//zw = (1 - w) * fz + w * fzop;
		while (P.Avanca(0.2)) {
			//PontoToHex3D(xw, yw, zw, xi, yi, zi, ehchao);

			//if ((xi!=xiant) || (yi!=yiant) || (zi!=ziant) || (ehchao!=ehchaoant)) { //testa se mudou de hex.
			if (primeirohex || P.MudouHex()) {
				//descobre o conte�do do hex.
				cont	 = Mapa.Cont(P.xi, P.yi, P.zi);
				chao	 = Mapa.Chao(P.xi, P.yi, P.zi);
				afin	 = Mapa.Afin(P.xi, P.yi, P.zi, cMapa::NAO_CHAO);
				afinchao = Mapa.Afin(P.xi, P.yi, P.zi, cMapa::CHAO);
				if (P.ehchao) P.SetAfinamento(&afinchao);
				else 		  P.SetAfinamento(&afin);

				//cada fuma�a conta como 2 hex a mais de dist�ncia
				if (!P.ehchao && cont.EhEsfumacado()) dist += 2.0;//obsado_camufl *= 1.15;

				P.Valida();
			}
			if (P.AcertouParede()) {
				if (primeirohex) {
					parede = false;
				} else {
					if (!soradar) { //ajusta 'parede' de acordo com a VIS�O normal
						if (J[obsador].PodVeAtraves(cont, chao, P.ehchao, passaradar)) {
							parede = false;
						} else {
							parede = true;
							soradar = true;
						}
					}

					if (radar) { //ajusta 'parede' de acordo com o RADAR
						if (radar && passaradar) {
							parede = false;
						} else if (!radar && soradar) {
							parede = true;
						}
					}
				}

				//se n�o consegue mais ver ent�o termina linha
				if (parede && !primeirohex) {
					temlinha = false;
					break; //exit while
				}
				primeirohex = false;
			}
		}
		if (temlinha) {
			if (radar) {
				//radar nunca falha
				ret = true;
			} else {
				//vis�o normal depende de sorte

				double CosAng; //CosAng = cos do �ng. entre a vis�o do Observador e a dire��o q est� o Observado
				if (dist < 0.5f) {
					CosAng = 1; //se est�o no mesmo lugar, se olham de frente
				} else if (PodVisao360Graus(obsador) || J[obsador].Bot) {
					CosAng = 1; //monstro e mosca olham direto
				} else {
					double alfa = (PI/6) * J[obsador].Frente;
					CosAng = ((fxop - fx) * cos(alfa) + (fyop - fy) * sin(alfa)) / dist;
				}

				// A visao do Observador tem distribuicao exponencial-de-normal
				// Em condi��es normais (Visao=Camufl=1, olhando de frente), se enxerga em m�dia
				// uma dist�ncia de 15 quadrados. Olhando de lado (CosAng=0), a dist. m�d � 5 e de costas
				// (CosAng=-1), a d.m. � 1. Ajustei uma par�bola p/ estes valores.
				double VisaoMedia = (5.33f + 7.11f * CosAng + 2.73f * CosAng * CosAng) * J[obsador].Olho / obsado_camufl;

				//como ver � um processo estoc�stico, deve-se mutiplicar a prob. por dt
				VisaoMedia *= (double)dt / 10.0;
				
				ret = (Aleatorio(VisaoMedia) > dist);
			}
		} else {
			ret = false;
		}
	}

	//final
	if (ret) J[obsador].podeimitar[obsado] = true;
	return ret;
}

void cJogo::AtualizaFog()
{
	double xh, yh, zh;
	CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xh, yh, zh, 0.5);

	bool soradar;

	double radar = J[vz].PodRadar(soradar);
	double d0 = 0.1f;
	double teta0 = 0;

	const double deltaphi = PI / 36.0; //PI/12 = 15�
	const double deltateta = PI / 50.0; //0.025f => 2� quando seno(phi)=1

	//int linhas = 0;

	for (double phi=deltaphi; phi < PI; phi+= deltaphi)
	{
		double deltatetamod = deltateta / sin(phi);
		for (double teta=teta0; teta < 2*PI; teta+= deltatetamod)
		{
			LinhaFog(xh, yh, zh, vz, radar, soradar, teta, phi, d0);
			d0 += 0.1f;
			if (d0 > 0.4f) d0 = 0.1f;
			//linhas++;
		}
		teta0 += deltatetamod / 4.0;
		if (teta0 > deltatetamod) teta0 = 0;
	}
	//linhas=linhas; //1916 linhas
}

void cJogo::LinhaFog(double x0, double y0, double z0,
					 ushort obsador, double radar, bool soradar,
					 double teta, double phi, double d0)
{
	double w;
	/*double xw, yw, zw;
	int xi, yi, zi;
	int xiant=-1, yiant=-1, ziant=-1;
	bool ehchao, ehchaoant = true;
    double dx = sin(phi) * cos(teta);
    double dy = sin(phi) * sin(teta);
    double dz = cos(phi);*/
    cPontoLinha P;
    P.IniciaAngulos(teta, phi, x0, y0, z0);

	cCont cont;
	cChao chao;
	cAfin afin, afinchao;
	bool parede, passaradar;
	bool primeirohex = true;
	//bool testar_parede;

	for (w = d0; w<90; w+=0.2) {
		//xw = x0 + (w * dx);
		//yw = y0 + (w * dy);
		//zw = z0 + (w * dz);
		P.Avanca(w);

		if (primeirohex || P.MudouHex()) {
			//testa se saiu do mapa
			if (P.ForaDoMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ)) {
				break; //exit for
			}

			cont = Mapa.Cont(P.xi, P.yi, P.zi);
			chao = Mapa.Chao(P.xi, P.yi, P.zi);
			afin	 = Mapa.Afin(P.xi, P.yi, P.zi, cMapa::NAO_CHAO);
			afinchao = Mapa.Afin(P.xi, P.yi, P.zi, cMapa::CHAO);
			if (P.ehchao) P.SetAfinamento(&afinchao);
			else 		  P.SetAfinamento(&afin);

			//atualiza fog
			cCont fogbitcont = J[obsador].FogBit->Cont(P.xi, P.yi, P.zi);
			cChao fogbitchao = J[obsador].FogBit->Chao(P.xi, P.yi, P.zi);
			cCont fogcont =    J[obsador].Fog   ->Cont(P.xi, P.yi, P.zi);
			cChao fogchao =    J[obsador].Fog   ->Chao(P.xi, P.yi, P.zi);
			if (P.ehchao) {
				if (fogbitchao == 0 || fogchao != chao) {
					J[obsador].Fog->SetChao(P.xi, P.yi, P.zi, chao);
					J[obsador].FogBit->SetChao(P.xi, P.yi, P.zi, 1);
					//ele sempre ve os objetos dali tb
					J[obsador].Fog->SetResumoObj(P.xi, P.yi, P.zi, Mapa.GetResumoObj(P.xi, P.yi, P.zi));
				}
			} else {
				if (fogbitcont == 0 || fogcont != cont) {
					J[obsador].Fog->SetCont(P.xi, P.yi, P.zi, cont);
					J[obsador].FogBit->SetCont(P.xi, P.yi, P.zi, 1);
					//o cara sempre ve o chao se conseguir ver a parte de cima
					J[obsador].Fog->SetChao(P.xi, P.yi, P.zi, chao);
					J[obsador].FogBit->SetChao(P.xi, P.yi, P.zi, 1);
					//ele sempre ve os objetos dali tb
					J[obsador].Fog->SetResumoObj(P.xi, P.yi, P.zi, Mapa.GetResumoObj(P.xi, P.yi, P.zi));
				}
			}

			P.Valida();
		/*} else if (P.parede_fina_perto) {				
			//est� pr�ximo a uma parede fina, mas n�o acertou ela ainda => testar se acertou agora
			if (P.AcertouParede(afin)) {
				P.parede_fina_perto = false;
				testar_parede = true;
			}*/
		}

		if (P.AcertouParede()) {
			if (primeirohex) {
				parede = false;
			} else {
				if (!soradar) {	//ajusta 'parede' de acordo com a VIS�O normal
					if (J[obsador].PodVeAtraves(cont, chao, P.ehchao, passaradar)) {
						parede = false;
					} else {
						parede = true;
						soradar = true;
					}
				}

				if (radar > 0) { //ajusta 'parede' de acordo com o RADAR
					if (P.ehchao) {
						if (!chao.EhChaoTransparente() && chao.EhChaoDuro()) {
							radar -= 1;
						}
					} else {
						//cada parede (que atrapalha o movimento) no caminho 'abafa' o radar
						if (cont.EhParedeSolida() && !cont.EhGrade()) {
							radar -= 0.75;
							if (cont.EhParedeDura()) radar -= 0.75;
							if (cont.EhParedePedra()) radar -= 1.5;
						}
					}

					if ((radar >= w) && (passaradar)) {
						parede = false;
					} else if ((radar < w) && (soradar)) {
						parede = true;
					}
				}
			}

			//se n�o consegue mais ver ent�o termina linha
			if (parede && !primeirohex) break;
			primeirohex = false;
		}
	}
}
