
#ifndef FUNCOES_H
#define FUNCOES_H

//###################################################
//fun��es pequenas ##################################

void CorDistancia(char &R, char &G, char &B, double dist);

short Direcao(int xo, int yo, int xa, int ya, double angteta = 7.0);
inline short Direcao(double teta);
short DifAngi(short frente, short proxfrente);
bool DifAngb(short frente, short proxfrente, short angpermitido);

double funAngTetai(int xo, int yo, int xa, int ya);
double funAngTetaf(double xo, double yo, double xa, double ya);

double funAngPhii(int xo, int yo, int zo, int xa, int ya, int za, double alturanoandar);
double funAngPhif(double xo, double yo, double zo, double xa, double ya, double za);

void EsferToCart(double teta, double phi, double r,
				 double xo, double yo, double zo,
				 double &x, double &y, double &z);
void CartToEsferi(int xo, int yo, int zo,
				  int xa, int ya, int za,
				  double &teta, double &phi, double &r, double alturanoandar);
inline void CartToEsferf(double xho, double yho, double zho,
						 double xha, double yha, double zha,
						 double &teta, double &phi, double &r);

void CentroHex2D(int x, int y, double &xh, double &yh);
void CentroHex3D(int x, int y, int z, double &xh, double &yh, double &zh, double alturanoandar);

unsigned short AlturaToAndar(double zo, bool &chao);
double AndarToAltura(int z, double alturanoandar);
double AlturaSombra(double z);

void PontoToHex2D(double xo, double yo, int &xh, int &yh);
void PontoToHex3D(double xo, double yo, double zo, int &xh, int &yh, int &zh, bool &chao);

double Distancia2D(int x1, int y1, int x2, int y2);
double Distancia3D(int x1, int y1, int z1, int x2, int y2, int z2);

inline double Distancia2D(double x1, double y1, double x2, double y2);
inline double Distancia3D(double x1, double y1, double z1, double x2, double y2, double z2);

double Aleatorio(double num, double expo = 1.2);
double superrnd(double desvio);
double normali(double p, double md, double s);

#endif