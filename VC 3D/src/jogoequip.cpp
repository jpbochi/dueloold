
#include "jogo@.h"

bool cJogo::Pega(int mx, int my, int mz)//, cObjetoSimples *objsimp, cObjeto *obj)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	//TO DO: esta fun��o deveria pegar um objeto espec�fico, mas antes
	//TO DO: � preciso construir uma GUI para que o jogador possa escolher qual objeto pegar

	if (mx == -1) mx = J[vz].x;
	if (my == -1) my = J[vz].y;
	if (mz == -1) mz = J[vz].z;
	
	cObjeto *obj = 0;

	cListaPosObjetos* listaobjs = Mapa.GetListaPos(mx, my, mz);
	if (listaobjs != NULL) {
		//Aten��o: in�cio de gambiarra
		//TEMP: este c�digo s� servir� enquanto o usu�rio n�o puder escolher o objeto a pegar
		//*
		cObjetoSimples objsimpaux;
		if (listaobjs->numobjsimp > 0) {
			objsimpaux = listaobjs->GetObjSimp(0);
			obj = new cObjeto(objsimpaux.cod);
			if (!Pega(obj, listaobjs, true)) {
				delete obj; //se n�o pegou ent�o destr�i o objeto
				return false;
			} else {
				return true;
			}
		} else if (listaobjs->numobj > 0) {
			obj = listaobjs->GetObj();
			return Pega(obj, listaobjs, true);
		} else {
			MsgDef("N�o h� nada utiliz�vel no ch�o!");
			return false;
		}
		/*/
		cObjetoSimples objsimpaux;
		if (obj == NULL && objsimp == NULL) {
			if (listaobjs->numobjsimp > 0) {
				objsimpaux = listaobjs->GetObjSimp(0);
				objsimp = &objsimpaux;
			} else if (listaobjs->numobj > 0) {
				obj = listaobjs->GetObj();
			} else {
				MsgDef("N�o h� nada utiliz�vel no ch�o!");
				return;
			}
		}
		*/
		//fim de gambiarra
	} else {
		return false;
	}
}

bool cJogo::Pega(cObjeto *obj, cListaPosObjetos *listaobjs, bool blnMaoDir)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoPegar;

	if (tempo > J[vz].T) {
		MsgSemTempo("pegar", tempo);
		return false;
	}
	if (PodNaoPodePegarNada(vz)) {
		MsgDef("Voc� n�o tem capacidade de pegar coisas!");
		return false;
	}
	if (J[vz].Posicao == posNaMoto) {
		MsgDef("Voc� tem que descer da moto para pegar coisas!");
		return false;
	}

	//unsigned char &c = obj->cod;
	//ushort &c = obj->cod;

	if (PodSoPegaItensSimples(vz)) {
		//espingarda, rifle, trabuco, muni��o para o rev�lver, medi-kit e chave
		//if (c != 3 && c != 11 && c != 23 && c != 15 && c != 49 && c != 57) {
		/*if (c != OBJ_Espingarda
		&&  c != OBJ_Rifle
		&&  c != OBJ_Trabuco
		&&  c != OBJ_Revolver
		&&  c != OBJ_KitMedico
		&&  c != OBJ_Chave) {*/
		if (!obj->EhObjetoTrivial()) {
			MsgDef("Voc� n�o pode pegar isso!");
			return false;
		}
	}

	bool pegou = false;
	bool destruiu = false;
	char *msg = NULL;
	if (PodDestroiObjetos(vz)) {
		if (obj->Eh(aDisco, cObjeto::tipArma) && PodNaoDestroiDisco(vz)) {
			msg = "Voc� achou um disco de predador!!!";
			//J[vz].PegaArma(Armas[6]);
			pegou = true;
		} else if (obj->Eh(eEsqueleto, cObjeto::tipEquip) && PodComeCorpos(vz)) {
			msg = "Voc� achou um corpo com muitas prote�nas!!";
			//J[vz].Proteinas ++;
			//J[vz].Equip.AdObjSimp((unsigned char)OBJ_Esqueleto);
			//J[vz].Equip.AdObj(*obj);
			pegou = true;
		} else if (PodDestroiObjetosDuros(vz)) {
			msg = "Voc� destruiu um objeto in�til!";
			destruiu = true;
			pegou = true;
		} else if (obj->Eh(eColete, cObjeto::tipEquip)) { //se n�o for um objeto duro
			   //&&  obj->Eh(eMoto, cObjeto::tipEquip)) {
			msg = "Voc� destruiu um objeto in�til!";
			destruiu = true;
			pegou = true;
		} else {
			msg = "Voc� n�o pode destruir isto!";
		}
	} else {
		if (obj->Eh(eEsqueleto, cObjeto::tipEquip)) {
			if (PodComeCorpos(vz)) {
				pegou = true;
			} else {
				msg = "Voc� n�o pode pegar corpos!";
			}
		} else {
			pegou = true;
		}

		/*
		int m;
		switch (c) {
			case OBJ_Disco:
				msg = "Voc� achou um disco de predador!!!";
				J[vz].PegaArma(Armas[6]);
				pegou = true;
				break;
			case OBJ_Esqueleto:
				if (PodComeCorpos(vz)) {
					msg = "Voc� achou um corpo com muitas prote�nas!!";
					//J[vz].Proteinas ++;
					//J[vz].Equip.AdObjSimp(OBJ_Esqueleto);
					pegou = true;
				}
				break;
			//*************
			//*** ARMAS ***
			case OBJ_Espingarda:
				msg = "Voc� achou uma espingarda!";
				J[vz].PegaArma(Armas[1]);
				pegou = true;
				break;
			case OBJ_Metralhadora:
				msg = "Voc� achou uma metralhadora!";
				J[vz].PegaArma(Armas[2]);
				pegou = true;
				break;
			case OBJ_LancaFoguetes:
				msg = "Voc� achou um lan�a-foguetes!";
				J[vz].PegaArma(Armas[3]);
				pegou = true;
				break;
			case OBJ_Rifle:
				msg = "Voc� achou um rifle com mira telesc�pica!";
				J[vz].PegaArma(Armas[4]);
				pegou = true;
				break;
			case OBJ_Degeneradora:
				msg = "Voc� achou uma arma degeneradora!";
				J[vz].PegaArma(Armas[5]);
				pegou = true;
				break;
			case OBJ_MiniGun:
				msg = "Voc� achou uma minigun!";
				J[vz].PegaArma(Armas[8]);
				pegou = true;
				break;
			case OBJ_LancaChamas:
				msg = "Voc� achou um lan�a-chamas!";
				J[vz].PegaArma(Armas[9]);
				pegou = true;
				break;
			case OBJ_Revolver:
				msg = "Voc� achou uma pistola!";
				J[vz].PegaArma(Armas[0]);
				pegou = true;
				break;
			case OBJ_Ricocheteadora:
				msg = "Voc� achou uma ricocheteadora!";
				J[vz].PegaArma(Armas[10]);
				pegou = true;
				break;
			case OBJ_Paralisante:
				msg = "Voc� achou uma arma paralisante!";
				J[vz].PegaArma(Armas[12]);
				pegou = true;
				break;
			case OBJ_Teletransportadora:
				msg = "Voc� achou uma arma teletransportadora!";
				J[vz].PegaArma(Armas[14]);
				pegou = true;
				break;
			case OBJ_Blaster:
				msg = "Voc� achou um lan�ador de m�sseis tele-guiados!";
				J[vz].PegaArma(Armas[15]);
				pegou = true;
				break;
			case OBJ_MG42:
				msg = "Voc� achou uma metralhadora militar!";
				J[vz].PegaArma(Armas[18]);
				pegou = true;
				break;
			case OBJ_Trabuco:
				msg = "Voc� achou um trabuco!";
				J[vz].PegaArma(Armas[26]);
				pegou = true;
				break;
			//********************
			//*** EQUIPAMENTOS ***
			case OBJ_Detector:
				msg = "Voc� achou uma detector de dist�ncia!";
				J[vz].SetFlag(TemDetec, true);
				pegou = true;
				break;
			case OBJ_Minas:
				m = rndi(2, 4);
				if (m == 2)		msg = "Voc� achou um par de minas!";
				else if (m == 3)msg = "Voc� achou tr�s minas!";
				else if (m == 4)msg = "Voc� achou quatro minas!";
				//J[vz].Minas += m;
				for (;m>=2;m--) listaobjs->AdObjSimp(OBJ_Minas);
				pegou = true;
				break;
			case OBJ_Granada:
				m = rndi(1, 3);
				if (m == 1)		msg = "Voc� achou uma granada!";
				else if (m == 2)msg = "Voc� achou um par de granadas!";
				else if (m == 3)msg = "Voc� achou tr�s granadas!";
				//J[vz].Granadas += m;
				for (;m>=2;m--) listaobjs->AdObjSimp(OBJ_Granada);
				pegou = true;
				break;
			case OBJ_KitMedico:
				if (PodDestroiKitMedico(vz)) {
					msg = "Voc� destruiu um kit de pronto-socorro!";
					destruiu = true;
				} else {
					msg = "Voc� achou um kit de pronto-socorro!";
					//J[vz].MedKits ++;
					//J[vz].Equip.AdObjSimp(OBJ_KitMedico);
				}
				pegou = true;
				break;
			case OBJ_CapaceteInv:
				if (PodDestroiCapacete(vz)) {
					msg = "Voc� destruiu este capacete!";
					destruiu = true;
				} else {//if (J[vz].Invis == 0) {
					msg = "Voc� achou um capacete de invisibilidade!";
					//J[vz].Invis = -3;
					//J[vz].Equip.AdObjSimp(OBJ_CapaceteInv);
				}// else {
				//	msg = "Voc� trocou seu capacete de invisibilidade.";
				//	J[vz].Invis = 3 * (J[vz].Invis / J[vz].Invis); //Mat�m o sinal, i.e., n�o liga nem desliga
				//	//TO DO: soltar um capacete gasto
				//}
				pegou = true;
				break;
			case OBJ_OculosIR:
				if (PodDestroiOculosIR(vz)) {
					msg = "Voc� destruiu uns �culos de vis�o infravermelha!";
					destruiu = true;
				} else if (!J[vz].GetFlag(TemIR)) {
					msg = "Voc� achou �culos de vis�o infravermelha!";
					J[vz].SetFlag(TemIR, true);
				} else {
					//OBSOLETO
					msg = "Voc� j� tinha vis�o infravermelha, portanto destruiu estes �culos!";
					destruiu = true;
				}
				pegou = true;
				break;
			case OBJ_Bomba:
				msg = "Voc� achou uma bomba!";
				//J[vz].Bombas ++;
				//J[vz].Equip.AdObjSimp(OBJ_Bomba);
				pegou = true;
				break;
			case OBJ_Colete:
				if (PodNaoUsaColete(vz)) {
					msg = "Ninjas n�o podem usar coletes!";
				} else if (J[vz].Blindagem >= 1) {
					msg = "Voc� j� tem uma prote��o muito boa!";
				} else {
					msg = "Voc� achou um colete a prova de balas!";
					J[vz].Blindagem += 1.0;
					pegou = true;
				}
				break;
			case OBJ_Holografia:
				if (PodDestroiHolografia(vz)) {
					msg = "Voc� destruiu este aparelho holografador!";
					destruiu = true;
				} else {
					msg = "Voc� achou um aparelho holografador!";
					//J[vz].Holografias ++;
					//J[vz].Equip.AdObjSimp(OBJ_Holografia);
				}
				pegou = true;
				break;
			case OBJ_Chave:
				if (J[vz].GetFlag(TemChave)) {
					msg = "Voc� j� tinha uma chave, portanto destruiu esta!";
					destruiu = true;
				} else {
					msg = "Voc� achou uma chave mestra!";
					J[vz].SetFlag(TemChave, true);
				}
				pegou = true;
				break;
			case OBJ_Moto:
				//OBSOLETO ?
				//msg = "Agora, voc� tem uma moto!";
				//J[vz].Posicao = posNaMoto;
				//pegou = true;
				break;
		}//*/
	}
	if (pegou) {
		if (!destruiu) {
			if (blnMaoDir) {
				J[vz].objMaoDir = new cObjeto(*obj);
			} else {
				J[vz].objMaoEsq = new cObjeto(*obj);
			}
		}
		
		ushort mx = listaobjs->x;
		ushort my = listaobjs->y;
		ushort mz = listaobjs->z;

		//listaobjs->ApagaObjSimp(*objsimp);		//apaga o objeto
		//apaga o objeto
		listaobjs->ApagaObj(obj);
		//atualiza o mapa E for�a a atualiza��o do fog
		J[vz].FogBit->SetCont(mx, my, mz, Mapa.AtualizaResumoObjsPos(mx, my, mz));
		AtualizaFog();

		J[vz].SetFlag(Mirando, false);

		if (msg) MsgDef(msg);

		J[vz].Replay.GravaFoto(AcaoPega);
		GastaTempo(tempo);
		TestaEncontro(false);
	} else {
		if (msg) MsgDef(msg);
	}
	return pegou;
}

bool cJogo::Solta(bool blnMaoDir)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoSoltar;

	if (tempo > J[vz].T) {
		MsgSemTempo("soltar", tempo);
		return false;
	}
	
	cObjeto **pobj = blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	if (*pobj == 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de soltar item da m�o que est� vazia!");
		return false;
	} else if ((**pobj).EhSkill()) {
		MsgDef("Voc� n�o pode soltar skills!");
		return false;
	} else {
		ushort mx = J[vz].x;
		ushort my = J[vz].y;
		ushort mz = J[vz].z;
	
		//insere objeto no ch�o
		Mapa.AdObjPos(**pobj, mx, my, mz);
		//tira o objeto da m�o
		delete *pobj;
		*pobj = NULL;
		//atualiza o mapa E for�a a atualiza��o do fog
		J[vz].FogBit->SetCont(mx, my, mz, Mapa.AtualizaResumoObjsPos(mx, my, mz));
		AtualizaFog();
		
		//MsgDef("Item solto!");
		
		J[vz].Replay.GravaFoto(AcaoSolta);
		GastaTempo(tempo);
		TestaEncontro(false);

		return true;
	}
}

bool cJogo::Guarda(bool blnMaoDir)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoGuardar;

	if (tempo > J[vz].T) {
		MsgSemTempo("guardar", tempo);
		return false;
	}
	
	cObjeto **pobj = blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	if (*pobj == 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de guardar item da m�o que est� vazia!");
		return false;
	} else {
		ushort mx = J[vz].x;
		ushort my = J[vz].y;
		ushort mz = J[vz].z;
	
		if ((**pobj).EhSkill()) {
			//insere objeto nos skills
			J[vz].Skills.AdObj(**pobj);
		} else {
			//insere objeto nos equipamentos
			J[vz].Equip.AdObj(**pobj);
		}
		//tira o objeto da m�o
		delete *pobj;
		*pobj = NULL;
		
		J[vz].Replay.GravaFoto(AcaoEquip);
		GastaTempo(tempo);
		TestaEncontro(false);

		return true;
	}
}

bool cJogo::Saca(bool blnMaoDir, cObjeto *item)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoSacar;

	if (tempo > J[vz].T) {
		MsgSemTempo("sacar", tempo);
		return false;
	}
	
	cObjeto **pobj = blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	if (*pobj != 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de sacar item na m�o que n�o est� vazia!");
		return false;
	} else {
		ushort mx = J[vz].x;
		ushort my = J[vz].y;
		ushort mz = J[vz].z;
		
		//p�e o objeto na m�o
		*pobj = new cObjeto(*item);
		if ((**pobj).EhSkill()) {
			//tira o objeto dos skills
			J[vz].Skills.ApagaObj(item);
		} else {
			//tira o objeto dos equipamentos
			J[vz].Equip.ApagaObj(item);
		}
		
		J[vz].Replay.GravaFoto(AcaoEquip);
		GastaTempo(tempo);
		TestaEncontro(false);

		return true;
	}
}

bool cJogo::Recarrega()
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	//S� � poss�vel recarregar se houver uma arma em uma m�o e uma muni��o compat�vel na outra
	//Se a arma estiver carregada, ent�o a muni��o antiga trocar� de lugar com a nova (i.e. o nova vai pra arma e a velha vai pra m�o)

	//Testa se o cara est� usando uma arma
	cObjeto *arma = J[vz].ArmaMao();
	if (arma == 0) return false; //nem avisa
	ushort codarma = arma->GetCod();
	
	//Testa se tem uma muni��o na outra m�o
	cObjeto *munnova = NULL;
	if (arma == J[vz].objMaoDir) munnova = J[vz].objMaoEsq;
	else						 munnova = J[vz].objMaoDir;
	if (!munnova->EhMunicao()) return false; //nem avisa
	ushort codmunnova = munnova->GetCod();
	
	//Testa se a muni��o � compat�vel
	if (Municoes[codmunnova].ArmaDest != codarma) return false;
	
	//Testa se o cara tem tempo de recarregar
	short tempo = Armas[codarma].TempoRecarr;
	if (tempo > J[vz].T) {
		MsgSemTempo("recarregar esta arma", tempo);
		return false;
	}
	
	//Finalmente, recarrega
	if (!ArmaRecarregaComMira(codarma)) J[vz].SetFlag(Mirando, false);

	//Tira a muni��o antiga
	cObjeto *munvelha = NULL;
	if (arma->codmunicao != mSemMunicao) {
		munvelha = new cObjeto(arma->codmunicao,cObjeto::tipMunicao);
		munvelha->municao = arma->municao;
	}

	//P�e a muni��o nova
	arma->codmunicao = codmunnova;
	arma->municao = munnova->municao;
	delete munnova;
	munnova = NULL;
	
	//P�e a muni��o velha(se houver uma) na m�o
	if (arma == J[vz].objMaoDir) J[vz].objMaoEsq = munvelha;
	else						 J[vz].objMaoDir = munvelha;

	//Finaliza
	J[vz].Replay.GravaFoto(AcaoRecarrega);
	GastaTempo(tempo);
	TestaEncontro(false);
	
	return(true);
}

bool cJogo::Vestir(bool blnMaoDir, cObjeto::enmLugarUso lugar)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	int tempo = 0;
	switch (lugar) {
		case cObjeto::codCabeca:	tempo = TempoVestirCabeca; break;
		case cObjeto::codCorpo:		tempo = TempoVestirCorpo; break;
	}

	if (tempo > J[vz].T) {
		MsgSemTempo("vestir isto", tempo);
		return false;
	}
	
	cObjeto **pobjMao	= blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	cObjeto **pobjMao2	= blnMaoDir ? &J[vz].objMaoEsq :  &J[vz].objMaoDir;
	if (*pobjMao == 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de vestir item de uma m�o vazia!");
		return false;
	} else if ((lugar == cObjeto::codCorpo) && (*pobjMao2 == 0)) {
		MsgDef("Voc� precisar ter as duas m�os livres para poder vestir isto!");
		return false;
	} else {
		cObjeto **pobjLugar = NULL;
		switch (lugar) {
			case cObjeto::codCabeca:	pobjLugar = &J[vz].objCabeca; break;
			case cObjeto::codCorpo:		pobjLugar = &J[vz].objCorpo; break;
		}
		if (*pobjLugar != 0) {
			//erro que n�o pode acontecer, mas nunca se sabe
			MsgDef("Bug: Tentativa de vestir item em um lugar ocupado!");
			return false;
		} else if (!(**pobjMao).AplicaEfeitoVeste(&J[vz])) {
			MsgDef("Este item n�o foi feito para ser usado a�!");
			return false;
		} else {
			//p�e objeto no lugar
			*pobjLugar = *pobjMao;
			
			//tira o objeto da m�o (obs.: o objeto n�o � copiado, � transferido)
			*pobjMao = NULL;
			
			J[vz].Replay.GravaFoto(AcaoEquip);
			GastaTempo(tempo);
			TestaEncontro(false);

			return true;
		}
	}
}

bool cJogo::Desvestir(bool blnMaoDir, cObjeto::enmLugarUso lugar)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	int tempo = 0;
	switch (lugar) {
		case cObjeto::codCabeca:	tempo = TempoDesvestirCabeca; break;
		case cObjeto::codCorpo:		tempo = TempoDesvestirCorpo; break;
	}

	if (tempo > J[vz].T) {
		MsgSemTempo("desvestir isto", tempo);
		return false;
	}
	
	cObjeto **pobjMao	= blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	cObjeto **pobjMao2	= blnMaoDir ? &J[vz].objMaoEsq :  &J[vz].objMaoDir;
	if (*pobjMao != 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de desvestir item para uma m�o ocupada!");
		return false;
	} else if ((lugar == cObjeto::codCorpo) && (*pobjMao2 != 0)) {
		MsgDef("Voc� precisar ter as duas m�os livres para poder desvestir isto!");
		return false;
	} else {
		cObjeto **pobjLugar = NULL;
		switch (lugar) {
			case cObjeto::codCabeca:	pobjLugar = &J[vz].objCabeca; break;
			case cObjeto::codCorpo:		pobjLugar = &J[vz].objCorpo; break;
		}
		if (*pobjLugar == 0) {
			//erro que n�o pode acontecer, mas nunca se sabe
			MsgDef("Bug: Tentativa de desvestir item de um lugar vazio!");
			return false;
		} else {
			if (!(**pobjLugar).DesaplicaEfeitoVeste(&J[vz])) {
				//erro que n�o pode acontecer, mas nunca se sabe
				MsgDef("Bug: O cara conseguiu vestir um item sem efeito!");
				return false;
			}

			//p�e objeto na m�o
			*pobjMao = *pobjLugar;
			
			//tira o objeto do lugar (obs.: o objeto n�o � copiado, � transferido)
			*pobjLugar = NULL;
			
			J[vz].Replay.GravaFoto(AcaoEquip);
			GastaTempo(tempo);
			TestaEncontro(false);

			return true;
		}
	}
}

bool cJogo::UsaMao(bool blnMaoDir)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	cObjeto **pobjMao = blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	if (*pobjMao == 0) {
		//n�o h� objeto para ser usado!
		return false;
	} else {
		cObjeto *obj = *pobjMao;
		short codobj = obj->GetCod();
		
		if (!obj->EhEquip()) {
			//por enquanto, s� vai fazer sentido tentar usar os equipamentos
			//depois tem que fazer os skills
			return false;
		} else {
			int tempo;
			switch (codobj) {
				case eKitMedico: {
					tempo = 30;
					
					if (J[vz].Ht == J[vz].HtMax) {
						MsgDef("Voc� est� em perfeitamente saud�vel e n�o precisa se curar!");
						return false;
					}

					if (tempo > J[vz].T) {
						MsgSemTempo("se curar", tempo);
						return false;
					}
					
					//cura vida
					J[vz].Ht = Min(J[vz].HtMax, J[vz].Ht + Aleatorio(30));
					
					//cura regenera��o
					if (J[vz].Reg < J[vz].RegNat) {
						J[vz].Reg = Min(J[vz].RegNat, J[vz].Reg + 0.2);
					}
					
				} break;
				default: {
					return false; //TO DO: Implementar o resto dos itens
				} break;
			}

			//gasta objeto e apaga ele caso tenha acabado
			if (obj->GastaCarga()) *pobjMao = NULL;;

			J[vz].Replay.GravaFoto(AcaoEquip);
			GastaTempo(tempo);
			TestaEncontro(false);
			
			this->Reescreve();

			return true;
		}
	}
}

/*
Private Sub Medkits_Click()
If j(vz).T - 4 >= 0 And j(vz).Med > 0 Then
    j(vz).T = j(vz).T - 4
    j(vz).Med = j(vz).Med - 1
    MsgMuda ("Voc� usou um kit-m�dico.")
    j(vz).Ht = j(vz).Ht + Aleat�rio(4)
    If j(vz).Ht > j(vz).HtMax Then j(vz).Ht = j(vz).HtMax
    If j(vz).Reg < 1 Then
        j(vz).Reg = j(vz).Reg + 0.2
        If j(vz).Reg > 1 Then j(vz).Reg = 1
    End If
    Call TestaEncontro
    Call ReEscreve
ElseIf j(vz).Med = 0 Then
    MsgMuda ("Voc� n�o tem medkits.")
ElseIf j(vz).T = j(vz).T - 4 >= 0 Then
    Call SemTempo("usar medkits.")
End If
End Sub
*/