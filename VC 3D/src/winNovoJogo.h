
#ifndef WINNOVOJOGO_H
#define WINNOVOJOGO_H

#include "fx.h"
#include "fx3d.h"

class winNovoJogo : public FXDialogBox {

	FXDECLARE(winNovoJogo)

protected:
	int poder_cod[128]; //Era pra ser NUM_PODERES => Poss�vel bug nos pr�ximos 200 anos

	enum {Num_Jogs_Max = 12};
	FXCheckButton	*(chkJog[Num_Jogs_Max]);
	FXComboBox		*(cmbNome[Num_Jogs_Max]);
	//FXComboBox	*(cmbModelo[Num_Jogs_Max]);
	FXTreeListBox	*(cmbModelo[Num_Jogs_Max]);
	FXComboBox		*(cmbPoder[Num_Jogs_Max]);
	FXComboBox		*(cmbTime[Num_Jogs_Max]);
	
	class cJogo *pJogo;
	
	winNovoJogo(){}

public:
	FXushort num_jogs;

	enum{
		ID_CHORE=FXDialogBox::ID_LAST,
		ID_CANCELAR,
		ID_COMECAR,
		ID_CHKJOG
	};
	
	long onChore(FXObject*,FXSelector,void*);
	void CarregaModelos();
	
	void AtualizaNumJogs(FXuint key);
	
	long onCancelar(FXObject*,FXSelector,void*);
	long onComecar(FXObject*,FXSelector,void*);
	long onChkJog_Click(FXObject*,FXSelector,void*);
	
	bool IniciaJogo();

	winNovoJogo(FXWindow *owner, FXIcon *icoMain, class cJogo *);
	virtual ~winNovoJogo();
	virtual void create();
};


#endif