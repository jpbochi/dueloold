
#ifndef PROJETIL_H
#define PROJETIL_H

#define ERRO_PHI 0.5

/*
#define FLAG_MUDOU_X	0x01
#define FLAG_MUDOU_Y	0x02
#define FLAG_MUDOU_Z	0x04
#define FLAG_MUDOU_CHAO	0x08

#define SHIFT_MUDOU_X		0
#define SHIFT_MUDOU_Y		1
#define SHIFT_MUDOU_Z		2
#define SHIFT_MUDOU_CHAO	3
*/

class cPontoLinha {
	public:
	enum {
		FLAG_MUDOU_X	= 0x01,
		FLAG_MUDOU_Y	= 0x02,
		FLAG_MUDOU_Z	= 0x04,
		FLAG_MUDOU_CHAO	= 0x08,
		SHIFT_MUDOU_X	= 0,
		SHIFT_MUDOU_Y	= 1,
		SHIFT_MUDOU_Z	= 2,
		SHIFT_MUDOU_CHAO= 3,
	};
	void IniciaAngulos(double _teta, double _phi,
					   double _xo, double _yo, double _zo);
	void IniciaDeltas(double _dx, double _dy, double _dz,
					  double _xo, double _yo, double _zo);

	void Avanca(double _w);
	void Valida();			//iguala todos os @@@ant aos valores atuais
	short MudouHex();		//se mudou de hex. (para cima, para baixo ou pelos lados)
	bool ForaDoMapa(int dimX, int dimY, int dimZ);

	int xi, yi, zi;			//pos. hex. no momento
	int xiant, yiant, ziant;//pos. hex. anterior
	bool ehchao, ehchaoant;	//se o proj. esta atravessando o chao ou nao
	double xf, yf, zf;		//pos. do proj�til
	double xch, ych, zch;	//pos. do centro do hex�gono em que o proj�til est�
	
	void SetAfinamento(class cAfin *);
	bool AcertouParede();	//testa se acertou na parede (considerando o afinamento)

	protected:
	class cAfin *afin;		//afinamento da parede pr�xima
	bool parede_fina_perto; //marca se est� perto de uma parede fina E ainda n�o acertou ela

	void Inicia(double _xo, double _yo, double _zo);
	void AjustaDW();
	double xo, yo, zo;		//pos. inicial
	double w;				//dist. j� percorrida
	double teta, phi;		//angs. que especificam a dire��o
	double dx, dy, dz;		//vetor q especifica a dire��o
};

class cPontoAPonto : public cPontoLinha {
	public:
	void IniciaAlvo(double _xo, double _yo, double _zo,
					double xa, double ya, double za);
	
	bool Avanca(double dw);
	double GetDistMax(){return distmax;}

	protected:
	double distmax;
};

class cProjetil : public cPontoLinha {
	public:
	void Inicia(double _teta, double _phi,
				double _xo, double _yo, double _zo,
				double espalhamento,
				double _dano,
				short _idlinhatiro);
	void Inicia(double _dx, double _dy, double _dz,
				double _xo, double _yo, double _zo,
				double espalhamento,
				double _dano,
				short _idlinhatiro);
	
	void Desvia(double espalhamento);
	void Valida();			//iguala todos os @@@ant aos valores atuais

	bool parou;				//informa se o proj. precisa ser simulado ou n�o
	short jogperto;			//id do jogador que est� no mesmo hex�gono, ou -1
	double dano, danoant;	//dano do projetil; diminui ao atravessar paredes
	short idlinhatiro;		//id da linhatiro que est� sendo tra�ada por este proj�til

	protected:
	void Inicia(double espalhamento, double _dano, short _idlinhatiro);
};

#endif