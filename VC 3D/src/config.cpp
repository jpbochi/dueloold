
#include "config.h"
#include "fxkeys.h"

cConfig::cConfig()
{
	for (int i=0; i<TEC_COUNT; i++) teclas[i] = 0;
	
	teclas[TEC_CAM_FRENTE]		= FX::KEY_w;
	teclas[TEC_CAM_TRAS]		= FX::KEY_s;
	teclas[TEC_CAM_APROX]		= FX::KEY_z;
	teclas[TEC_CAM_AFASTA]		= FX::KEY_x;
	teclas[TEC_CAM_ESQ]			= FX::KEY_a;
	teclas[TEC_CAM_DIR]			= FX::KEY_d;
	teclas[TEC_CAM_CIMA]		= FX::KEY_e;
	teclas[TEC_CAM_BAIXO]		= FX::KEY_c;

	teclas[TEC_CAM_ANDAR_SOBE]	= FX::KEY_r;
	teclas[TEC_CAM_ANDAR_DESCE]	= FX::KEY_f;
	teclas[TEC_CAM_FILTRO_DE_ANDARES] = FX::KEY_q;
	
	teclas[TEC_CAM_ESCONDE_PAREDES] = FX::KEY_v;
	
	teclas[TEC_CENTRA_MOUSE] = FX::KEY_Control_L;
	
	mouse_sens = 0.01;
}
