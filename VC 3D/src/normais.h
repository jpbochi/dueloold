
#ifndef NORMAIS_H
#define NORMAIS_H

typedef float vertex[3];		// 3D vertex datatype definition

void RescaleNormal( vertex vector );
void CalcNormalfv( const vertex v[], vertex out );
void CalcNormalf( float x1, float y1, float z1, 
				  float x2, float y2, float z2, 
				  float x3, float y3, float z3, vertex out );

#endif