
//####################################
//########## CLASSE JOGADOR ##########
//####################################

#ifndef JOGADOR_H
#define JOGADOR_H

//#include <windows.h>
//#include <gl\gl.h>
#include "fx.h"
#include "fx3d.h"
#include "..\modelos\modelosmdx.h"

#include "declarac.h"
//#include "mapa.h"
#include "arma.h"
#include "replay.h"
#include "objetos.h"

class cCont;
class cChao;
class cModeloAnim;
class cMapa;

enum{
	pSorteado = 0,
	pPM,
	pAtirador_de_Elite,
	pVelocista,
	pCasca_Grossa,
	pTroll,
	pBoina_Verde,
	pPredador,
	pWolverine,
	pVoador,
	pFantasma,
	pTarrasque,
	pRobocop,
	pHomem_do_Raio_X,
	pBalacau,
	pMosca,
	pDragao,
	pMago,
	pEthereal,
	pJason,
	pAlienPequeno,
	pAlienGrande,
	pDemolidor,
	pMagrao,
	pZerg,
	pTanque,
	pSabata,
	pBlindado,
	pJedi,
	pNinja,
	pHorla,
	pCapangas,
	pDinamitador,
	pT_1000,
	pAgente_do_Matrix,
	pClerigo_Maligno,
	NUM_PODERES
};

#define PodT_1000_TempoParaReparar	1.0
#define PodJedi_DanoRetaliacao		20.0
#define PodVisaoIR					3.5

enum enuPosicao
{
	posNormal = 0,
	posAbaixado,
	posRastejando,
	posEnterrado,
	posNaMoto
};

class cJogador
{
public:

	cJogador();
	cJogador(const int);

	static void PoderStr(int, FXString &);
	void PoderStr(FXString &);

	void SetNome(char *);
	void SetId(GLubyte novo_id);
	void SetFlag(int, bool);
	int GetFlag(int);
	void ZeraFogBit();
	void ZeraReplay();
	void ZeraJogador();
	protected:
		void PegaArma(ushort, cArma *, cMunicao *);
		void PegaMun(ushort, cMunicao *);
		void PegaEquip(ushort);
		void PegaSkill(ushort);
	public:
	void AcionaPoder(cArma *, cMunicao *);

	char PodSangue();
	void PodSoltaCorpo(cMapa *);
	double PodDanoGolpe();
	short PodSomGolpe();
	double PodRadar(bool &soradar);
	bool PodVeAtraves(cCont cont, cChao chao, bool ehchao, bool &passaradar);

	cMapa *Fog, *FogBit;
	cReplay Replay;

	//modelo MDX
	cModeloAnim modelo;

	bool Bot;
	bool *podeimitar; //vetor de tam. vari�vel

	//arma
	short ArmaMaoCod();
	cObjeto *ArmaMao();
	//short ArmaMao, Mun[NUM_ARMAS], MunExtra[NUM_ARMAS];
	//bool PossuiArma[NUM_ARMAS];
	//mira
	short AlvoID;
	//short AlvoX[ARMA_MAX_ALVOS], AlvoY[ARMA_MAX_ALVOS], AlvoZ[ARMA_MAX_ALVOS];
	double AlvoTeta[ARMA_MAX_ALVOS], AlvoPhi[ARMA_MAX_ALVOS], AlvoDist[ARMA_MAX_ALVOS];
	double MiraAtual, MiraBonus;

	int id, idProx, idHost, idImit;
	FXString Nome;//char Nome[NOME_MAX];
	int x, y, z, cur;
	double Ht, HtAnt, HtMax,
		   Mira,
		   Reg, RegNat,
		   Camufl, CamuflNat,
		   Olho, OlhoNat, Olfato,
		   MagicRes, MagicResNat,
		   Forca,
		   Blindagem,
		   Raio;
	//unsigned char Cor[3];
	short //Textura,
		  Time, Poder,
		  Kills, Mortes, AtacanteDegen,
		  T, TAnt, TProxRodada, TMax,
		  Frente, //para onde est� olhando
		  //equipamentos
		  //Bombas, BombasAtivadas,
		  //MedKits, Minas, Granadas,
		  //Proteinas, Holografias,
		  //Fumaca,
		  //Invis,
		  //malucos
		  ManaFe,
		  Injetor, Incubacao;//contador regressivo para o ovo do alien
	enuPosicao Posicao;

	//equipamento
	cObjeto			*objMaoDir, *objMaoEsq, *objCabeca, *objCorpo;
	cListaObjetos	Equip, Skills;
	bool TemMaoLivre();
	void ApagaArma();

	DWORD flags;
		#define Morto			0x00000001
		#define AtvGran			0x00000002
		#define TemIR			0x00000004
		#define Imovel			0x00000008
		#define TemChave		0x00000010
		#define Invisivel		0x00000020
		#define TemDetec		0x00000040
		#define Parasitas		0x00000080
		#define Parasitado		0x00000100
		#define DinaExplodiu	0x00000200 //serve para evitar que o dinamitador exploda mais de uma vez recursivamente, j� que explode quando morre
		#define Mirando 		0x00000400
		#define MorreuSemVerRep	0x00000800
	//* - flags que devem ser retirados
};
/*
Type typJogador
    'Caracter�sticas Iniciais
    'Caracter�sticas Provis�rias (Booleans, em geral)
    'Equipamentos
    'S� da bomba
    ContBomba As Integer
    XBomba(5) As Integer
    YBomba(5) As Integer
    ZBomba(5) As Byte
    'S� da holografia
    XHol As Integer
    YHol As Integer
    ZHol As Byte
    'Armas
    ArmaM�o As Byte
    Mun(0 To NArmas) As Integer
    ArmaP(0 To NArmas) As Boolean
    'Especiais de Poderes
    Canh�o As Integer
    *AlienGrande As Boolean
    Ovo As Integer
    Injetor As Byte
    *Parasitas As Boolean
    *Parasitado As Boolean
    Olfato As Integer
    ManaF� As Integer
    *Imit As Byte
    ViuSemblante(M + MM) As Boolean
    *DimiExplodiu As Boolean
    
    'Grava a imagem inicial de cada cara
    RepXIni As Integer
    RepYIni As Integer
    RepZIni As Integer
    RepVisaoIni As Byte
    RepCursorIni As Long
    'Grava cada passo do cara
    RepA��oOQue(0 To 127) As Byte
    RepA��oQ1(0 To 127) As Byte
    RepA��oQ2(0 To 127) As Byte
    RepA��oQ3(0 To 127) As Double
    RepA��oQ4(0 To 127) As Byte
    'Grava se 'A' viu 'B' durante o passo 'n'
    ViuRepF(M + MM, 0 To 127) As Byte
    ViuRep(M + MM) As Boolean
    AcabouDeMorrer As Boolean 's� � util para mostrar replay aos caras que j� morreram
    'PROBLEMAS COM REPLAY DOS MONSTROS
    ''Grava se o monstro 'A' viu algum replay
    MonViuRep As Boolean
    'Public MonViuRep(M + 1 To M + MM) As Boolean
    ''Grava o �ltimo passo que o monstro 'A' viu do 'B'
    ViuRepX(M) As Integer
    ViuRepY(M) As Integer
    'Public ViuRepX(M + 1 To M + MM, M) As Integer
    'Public ViuRepY(M + 1 To M + MM, M) As Integer
    
    'FogID (server para saber que JFog usar)
    FogID As Integer
End Type
*/

#endif