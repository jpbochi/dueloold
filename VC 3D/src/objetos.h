
#ifndef OBJETOS_H
#define OBJETOS_H

#include "declarac.h"

class cArma;
class cMunicao;
class cJogador;

#define OBJ_Espingarda			3
#define OBJ_Metralhadora		4
#define OBJ_Minas				7
#define OBJ_LancaFoguetes		8
#define OBJ_Rifle				11
#define OBJ_Degeneradora		12
#define OBJ_Granada				13
#define OBJ_GranadaA			14
#define OBJ_KitMedico			15
#define OBJ_Esqueleto			16
#define OBJ_Disco				17
#define OBJ_MiniGun				19
#define OBJ_LancaChamas			21
#define OBJ_Revolver			23
#define OBJ_Ricocheteadora		27
#define OBJ_CapaceteInv			28
#define OBJ_OculosIR			29
#define OBJ_Bomba				30
#define OBJ_Paralisante			37
#define OBJ_Teletransportadora	38
#define OBJ_Caixa				39
#define OBJ_Blaster				41
#define OBJ_Colete				46
#define OBJ_Chave				49
#define OBJ_Holografia			51
#define OBJ_MG42				55
#define OBJ_Detector			56
#define OBJ_Trabuco				57
#define OBJ_CaixaDeArmas		58
#define OBJ_Moto				59

enum {	//equipamentos //Not(EhArma) && Not(EhMunicao) && Not(EhSkill) => EhEquip()
	eMina,
	eGranada,
	eKitMedico,
	eEsqueleto,
	eCapaceteInv,
	eCapaceteIR,
	eBomba,
	eColete,
	eChave,
	eHolografia,
	eDetector,
};

enum {	//Skills que n�o s�o armas
	sCamuflagem,
	
	sKitDeArrombamento,
	sEnterrarse,
	sDesarmarMinas,
	sControlePsionico,
	sRegT1000,
	sAutoDestruicao,

	sIdentificacao,
	sPararTempo,
	sInvisibilidade,
	sTeletransporte,
	sImitar,
	sHolografia,
	sDardosMisticos,
	
	sRezar,
	sCura,
	sAnimarMortos,
	sRelampago,
};

class cObjetoSimples //um objeto simples com c�digo antigo (ex.: sangue, esqueleto, equipamentos em geral)
{
public:
	unsigned char cod;
	cObjetoSimples();
	cObjetoSimples(const unsigned char);

	bool operator == (cObjetoSimples);
	void operator = (cObjetoSimples);
	
	char GetIco();
};

class cObjeto //um objeto com dono (ex.: minas, bombas e granadas ativadas)
{
protected:
	enum {
		maskCod		= 0x00ff,

		maskTipo	= 0x0700,
		flgEquip	= 0x0000,
		flgArma		= 0x0100,
		flgMunicao	= 0x0200,
			//maskTipoArma	= 0x1000,
			//subArmaNormal	= 0x0000,
			//subMun			= 0x1000,
			//subSkill		= 0x0200,
			//RESERVADO		= 0x0300,

		flgSkill	= 0x0300,
		flgArmaSkill= 0x0400,

		//tipEquip	= 0x0000,
		maskLugarUso= 0x3000,

	};
	ushort cod;

public:
	enum enmLugarUso {
		codMao		= 0x0000,
		codCabeca	= 0x1000,
		codCorpo	= 0x2000,
		//codOutro	= 0x3000,
	};
	enum enmTipo {
		tipArma		= flgArma,
		tipSkill	= flgSkill,
		tipArmaSkill= flgArmaSkill,
		tipMunicao	= flgMunicao,
		tipEquip	= flgEquip,
	};

	short dono;
	union {
		ushort info1;		//nome gen�rico para os dados abaixo
		short municao;		//quantidade de muni��o?
		ushort cargas;		//cargas em geral (e.g. do capacete de invisibilidade)
		ushort countdown;	//contagem regressiva para a bomba do predador
	};
	union {
		ushort codexten;	//c�digo do objeto que est� extendendo este (ex.: bomba_de_predador em um corpo)
		ushort codmunicao;	//c�digo da muni��o que est� carregada
	};
	cObjeto *prox;

	cObjeto(cObjeto&);
	cObjeto(const ushort, short _dono = -1, ushort info1 = 0);
	cObjeto(const ushort cod, const enmTipo, short _dono = -1);
	~cObjeto();
	
	void CarregaArma(cArma *Armas, cMunicao *Municoes);
	void RecarregaArmaDeUmTiroPorRodada(cArma *Armas);
	
	static ushort ConverteObj(char);//Legacy function
	
	char GetIco();
	
	//fun��es que acessam os flags/masks/etc.
	ushort GetCod();
	ushort GetLugarUso();
	bool Eh(ushort, enmTipo tipo);
	inline bool EhArma();
	inline bool EhSkill();
	inline bool EhArmaSkill();
	inline bool EhEquip();
	inline bool EhMunicao();
	
	bool EhObjetoTrivial(); //dos que o sabata usa
	bool UsaManaFe(); //ex.: aBolaDeFogo
	bool AplicaEfeitoVeste(cJogador *);
	bool DesaplicaEfeitoVeste(cJogador *);
	
	double PenalidadeDuasMaos();
	
	//enmLugarUso LugarUso();
	bool GastaCarga();
};

class cListaObjetos //lista de objetos
{
protected:
	cObjeto			*priobj, *ultobj;	//lista encadeada
	cObjetoSimples	*objsimp;			//vetor
	int lenobjsimp;

public:
	int numobj, numobjsimp;

	cListaObjetos();
	
	void RecarregaArmasDeUmTiroPorRodada(cArma *Armas);
	void ConverteObjs(cArma *Armas, cMunicao *Municoes);

	void AdObj(cObjeto &);
	void AdObjSimp(cObjetoSimples);

	bool ApagaObj(cObjeto *);
	bool ApagaObjSimp(cObjetoSimples);

	char GetResumo();

	cObjeto *		GetObj();
	cObjetoSimples	GetObjSimp(int indice);
};

class cListaPosObjetos : public cListaObjetos //lista de objetos em certa posi��o
{
protected:
public:
	ushort x, y, z;
	cListaPosObjetos(ushort px, ushort py, ushort pz);
	
	bool MatchPos(ushort px, ushort py, ushort pz);
};

class cListaListas //lista de listas de objetos em posi��es variadas
{
protected:
	cListaPosObjetos* * lista; //vetor ou lista encadeada ou (X)vetor de ponteiros?
	int numlistas, lenlista;
public:
	cListaListas();

	void AdObjPos(cObjeto &, ushort x, ushort y, ushort z);
	void AdObjSimpPos(cObjetoSimples &, ushort x, ushort y, ushort z);

	cListaPosObjetos* GetListaPos(ushort x, ushort y, ushort z);

protected:
	int AchaLista(ushort x, ushort y, ushort z);
	void AdLista(ushort x, ushort y, ushort z);
};

class cObjetos //objetos de um mapa inteiro
{
protected:
	ushort			numlistas;
	cListaListas	*listas;
public:
	cObjetos(ushort numerodelistas);
	~cObjetos();

	void AdObjPos(cObjeto &, uint vetpos, ushort x, ushort y, ushort z);
	void AdObjSimpPos(cObjetoSimples &, uint vetpos, ushort x, ushort y, ushort z);

	//cObjeto*		GetObj(ushort x, ushort y, ushort z);
	//cObjetoSimples* GetObjSimp(ushort x, ushort y, ushort z);
	cListaPosObjetos* GetListaPos(uint vetpos, ushort x, ushort y, ushort z);

protected:
	int PosLista(uint vetpos);
};

#endif