
#include "camera.h"
#include <math.h>
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "declarac.h"

#define EIXO_X	0
#define EIXO_Y	1
#define EIXO_Z	2

//glcanvas->makeCurrent() deve ser chamado antes de qualquer chamada da OpenGL
#include "glcontext.h"
/*//AtualizaMatrix() precisa chamar glcanvas->makeCurrent() pra funcionar
#include "fx.h"
#include "fx3d.h"
FXGLCanvas *cam_glcanvas = NULL;*/

cCamera::cCamera()
{
	teta = 0;		phi = MEIOPI;		angn = 0;

	up.v[EIXO_X] = 0;	up.v[EIXO_Y] = 0;		up.v[EIXO_Z] = 1;

	AtualizaVetores();
}

void cCamera::AtualizaVetores()
{
	double costeta = cos(teta);
	double sinteta = sin(teta);
	double cosphi = cos(phi);
	double sinphi = sin(phi);

	n.v[EIXO_X] = sinphi * costeta;
	n.v[EIXO_Y] = sinphi * sinteta;
	n.v[EIXO_Z] = cosphi;

	/*up.v[EIXO_X] = sin(angn) * -sinteta;
	up.v[EIXO_Y] = cos(angn);
	up.v[EIXO_Z] = sin(angn) * -costeta;*/

	u = n * up;
	u.normaliza();
	v = u * n;
}

void cCamera::Look()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixd(&matrix[0][0]);
}

void cCamera::RotHorizontal(double rot, bool atualizamatriz)
{
	teta -= rot;
    if (teta > DOIS_PI) teta -= DOIS_PI;
    if (teta < 0) teta += DOIS_PI;
	AtualizaVetores();
	if (atualizamatriz) AtualizaMatrix();
}
void cCamera::RotVertical(double rot, bool atualizamatriz)
{
    //*
	phi = Max(0.001f,
          Min(PI - 0.001f,
              phi - rot
          ));
	/*/
	phi -= rot;
    if (phi > DOIS_PI) phi -= DOIS_PI;
    if (phi < 0) phi += DOIS_PI;
	/**/
	AtualizaVetores();
	if (atualizamatriz) AtualizaMatrix();
}
/*void cCamera::RotEixo(double rot, bool atualizamatriz)
{
	angn += rot;
    if (angn > DOIS_PI) angn -= DOIS_PI;
    if (angn < 0) angn += DOIS_PI;
	AtualizaVetores();
	if (atualizamatriz) AtualizaMatrix();
}*/

void cCamera::MoveXY(double d, bool atualizamatriz)
{
	/*double dx = d * cos(teta);
	double dy = 0;
    double dz = d * sin(teta);
    cam_atx += dx;	cam_tox += dx;
    cam_aty += dy;	cam_toy += dy;
    cam_atz += dz;	cam_toz += dz;*/
	cVetor v(n[EIXO_X], n[EIXO_Y], 0);
	v.normaliza();
	c.v[EIXO_X] += d * v[EIXO_X];
	c.v[EIXO_Y] += d * v[EIXO_Y];
	if (atualizamatriz) AtualizaMatrix();
}
void cCamera::MoveXYZ(double d, bool atualizamatriz)
{
    /*double dx = d * sin(phi) * cos(teta);
    double dy = d * cos(phi);
    double dz = d * sin(phi) * sin(teta);
    cam_atx += dx;	cam_tox += dx;
    cam_aty += dy;	cam_toy += dy;
    cam_atz += dz;	cam_toz += dz;*/
	c.v[EIXO_X] += d * n[EIXO_X];
	c.v[EIXO_Y] += d * n[EIXO_Y];
	c.v[EIXO_Z] += d * n[EIXO_Z];
	if (atualizamatriz) AtualizaMatrix();
}
void cCamera::StrafeHorizontal(double d, bool atualizamatriz)
{
	/*double dx = d * cos(teta + MEIOPI);
    double dz = d * sin(teta + MEIOPI);
    cam_atx += dx;	cam_tox += dx;
    cam_atz += dz;	cam_toz += dz;*/
	c.v[EIXO_X] += d * u[EIXO_X];
	c.v[EIXO_Y] += d * u[EIXO_Y];
	c.v[EIXO_Z] += d * u[EIXO_Z];
	if (atualizamatriz) AtualizaMatrix();
}
void cCamera::StrafeVertical(double d, bool atualizamatriz)
{
	//cam_aty += d;	cam_toy += d;
	c.v[EIXO_X] += d * up[EIXO_X];
	c.v[EIXO_Y] += d * up[EIXO_Y];
	c.v[EIXO_Z] += d * up[EIXO_Z];
	if (atualizamatriz) AtualizaMatrix();
}

void cCamera::AtualizaMatrix()
{
	//M.Camera(c, u, v, n);
	//*
	GLContexto.Empilha();

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	gluLookAt(c[EIXO_X],			 c[EIXO_Y],				c[EIXO_Z],
			  c[EIXO_X] + n[EIXO_X], c[EIXO_Y] + n[EIXO_Y], c[EIXO_Z] + n[EIXO_Z],
			  up[EIXO_X],			 up[EIXO_Y],			up[EIXO_Z]);
	//glGetFloatv(GL_MODELVIEW_MATRIX, &matrix[0][0]);
	glGetDoublev(GL_MODELVIEW_MATRIX, &matrix[0][0]);
	glPopMatrix();
	
	GLContexto.DesEmpilha();
	//*/
}

void cCamera::SetPos(double x, double y, double z, bool atualizamatriz)
{
	//cam_atx = lookat.x;	cam_aty = lookat.y;	cam_atz = lookat.z;
	c.v[EIXO_X] = x;
	c.v[EIXO_Y] = y;
	c.v[EIXO_Z] = z;
	AtualizaVetores();
	if (atualizamatriz) AtualizaMatrix();
}

void cCamera::SetLookTo(double x, double y, double z, bool atualizamatriz)
{
    double dx = x - c[EIXO_X];//cam_atx;
    double dy = y - c[EIXO_Y];//cam_aty;
    double dz = z - c[EIXO_Z];//cam_atz;
    double r = sqrt(pow(dx, 2.0) + pow(dy, 2.0) + pow(dz, 2.0));
	dx /= r;
	dy /= r;
	dz /= r;

    if (r > 0.0) {
        phi = acos(dz);
        if (sin(phi) == 0.0){
            teta = 0.0;
        } else {
            teta = acos(dx / sin(phi));
            if (dy < 0) teta = DOIS_PI - teta;
        }
        AtualizaVetores();
    }
	if (atualizamatriz) AtualizaMatrix();
}

//---------------------------------------------------------

cVetor::cVetor()
{
	for (int i=0; i<3; i++) v[i] = 0;
	v[3] = 1;
}

cVetor::cVetor(double x, double y, double z)
{
	v[EIXO_X] = x;
	v[EIXO_Y] = y;
	v[EIXO_Z] = z;
	v[3] = 1;
}

inline double cVetor::operator [] (int i)
{
	return v[i];
}

cVetor cVetor::operator * (cVetor op)
{
	return cVetor(v[EIXO_Y] * op.v[EIXO_Z] - v[EIXO_Z] * op.v[EIXO_Y],
				  v[EIXO_Z] * op.v[EIXO_X] - v[EIXO_X] * op.v[EIXO_Z],
				  v[EIXO_X] * op.v[EIXO_Y] - v[EIXO_Y] * op.v[EIXO_X]);
}

void cVetor::normaliza()
{
	double l = (double)sqrt(pow(v[EIXO_X], 2) + pow(v[EIXO_Y], 2) + pow(v[EIXO_Z], 2));
	v[EIXO_X] /= l;
	v[EIXO_Y] /= l;
	v[EIXO_Z] /= l;
}

bool cVetor::DivPersp()
{
	if (v[3] == 0) {
		return 0;
	} else {
		double absw = (double)fabs(v[3]);
		if (fabs(v[EIXO_X]) > absw || fabs(v[EIXO_Y]) > absw || fabs(v[EIXO_Z]) > absw) {
			return 0;
		} else {
			v[EIXO_X] /= v[3];
			v[EIXO_Y] /= v[3];
			v[EIXO_Z] /= v[3];
			return 1;
		}
	}
}
