
#include "jogo@.h"

/*void cJogo::TrocaArma(short id)
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	if (J[vz].ArmaMao() == -1) {
		//o vz n�o tem armas
		return;
	}

	//TO DO: arrumar esta fun��o quando fro poss�vel escolher a arma para qual o cara quer trocar

	if (id == -1) {//TEMP:depois que o cara conseguir escolher a arma, esse if ficar� obsoleto
		//procura a pr�xima arma
		short i;
		for (i = (J[vz].ArmaMao + 1) % NUM_ARMAS; i != J[vz].ArmaMao; i = (i + 1) % NUM_ARMAS) {
			if (J[vz].PossuiArma[i]) {
				id = i;
				Reescreve();
				break;
			}
		}
	}

	if (id != -1) {
		//J[vz].ArmaMao = id;
	}
	Reescreve();
}*/

void cJogo::IniciaMira()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada
	
	//CameraMira();
}

void cJogo::CameraMira(int modo)
{
	//0   : normal
	//-1  : 1� pessoa
	//else: troca
	
	if (modo == modocam) {
		return;
	} else {
		modocam = ~modocam;
	}

	if (modocam == MODOCAM_PRI_PES) {
		Mapa.Cam = pMapa->Cam;

		double xo, yo, zo;
		CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xo, yo, zo, 0.5);
		pMapa->Cam.SetPos(xo, yo, zo, false);
		pMapa->Cam.SetLookTo(xo+1, yo, zo, false);
		pMapa->Cam.RotHorizontal(-30.0 * PI_180 * J[vz].Frente, true);
	} else {
		//pMapa->CentraCam(J[vz].x, J[vz].y, J[vz].z, mx, my, mz);
		pMapa->Cam = Mapa.Cam;
	}
}

bool cJogo::Mira(int mx, int my, int mz, double md)
{
	if (!NaoAnimando) return true; //est� mostrando anima��o => GUI bloqueada

	//Testa condi��es que proibam a a��o (mirar)
	if (J[vz].Posicao == posNaMoto) {
		MsgDef("S� em filmes algu�m consegue atirar andando de moto!");
		return true;
	}
	//short &armamao = J[vz].ArmaMao();
	cObjeto *armamao = J[vz].ArmaMao();
	if (armamao == 0) {
		//MsgDef("N�o faz sentido mirar!\nVoc� n�o est� carregando nenhuma arma!");
		MsgDef("Voc� quer mirar com que arma, rapaz? C� � doidio?!");
		return true;
	}
	ushort codarma = armamao->GetCod();
	if (Armas[codarma].TempoMirar == 0) {
		//s� se aplica a armas corpo-a-corpo
		MsgDef("N�o � necess�rio mirar com esta arma!");
		return true;
	}

	//Testa se tem muni��o, pois n�o adianta mirar com uma arma descarregada
	if (!TemMunicao(armamao)) return true;

	//Traduz o clique em um ponto em coordenadas esf�ricas
	double mphi, mteta, mdist;
	if (modocam == MODOCAM_PRI_PES) {
		mphi = J[vz].Fog->Cam.phi;
		mteta = J[vz].Fog->Cam.teta;
		mdist = md;//Distancia3D(mx, my, mz, J[vz].x, J[vz].y, J[vz].z);
		CameraMira(MODOCAM_NORMAL);
	} else {
		if (md == 0) {
			//s� � poss�vel mirar no centro de um hex�gono se o cara clicar em um hex�gono
			return true;
		}
		CartToEsferi(J[vz].x, J[vz].y, J[vz].z, mx, my, mz, mteta, mphi, mdist, 0.5);
		if (mdist == 0) return false; //o cara quer mirar nele mesmo
	}

	//Testa se o cara precisa se virar para a dire��o certa
	/*short proxFrente = Direcao(J[vz].x, J[vz].y, mx, my);
	if (proxFrente == -1) {
		if (J[vz].z == mz) {
			return false; //o cara quer mirar nele mesmo
		} else {
			proxFrente = J[vz].Frente; //o cara est� mirando para cima/baixo
		}
	}/*/
	short proxFrente = Direcao(mteta);
	if ((mphi < 0.2) || (abs(mphi - PI) < 0.2)) {
		proxFrente = J[vz].Frente; //o cara est� mirando para cima/baixo
	}	
	/**/
	if (proxFrente != J[vz].Frente && !PodNaoViraAoAtirar(vz) && !ArmaNaoViraAoAtirar(codarma)) {
		if (!Vira(proxFrente, 0)) {
			//se o cara n�o terminou de virar (viu alguem ou acabou o tempo), cancela a a��o
			return true;
		}
	}

	//Testa se o cara tem tempo de mirar
	short tempo = Armas[codarma].TempoMirar;
	if (J[vz].GetFlag(Mirando)) tempo /= 2; //se j� estava mirando, gasta metade
	if (tempo > J[vz].T) {
		if (J[vz].GetFlag(Mirando)) {
			MsgSemTempo("mudar de alvo esta arma", tempo);
		} else {
			MsgSemTempo("escolher um alvo com esta arma", tempo);
		}
		return true;
	}

	//TO DO: controlar o AlvoID para fazer o blaster funcionar
	J[vz].AlvoID = 0; //solu��o tempor�ria enquanto o blaster n�o � implementado

	//Finalmente, mira
	J[vz].AlvoPhi[J[vz].AlvoID]  = mphi;
	J[vz].AlvoTeta[J[vz].AlvoID] = mteta;
	J[vz].AlvoDist[J[vz].AlvoID] = mdist;

	if (J[vz].GetFlag(Mirando)) { //se j� estava mirando ent�o troca de alvo, mantendo parte da mira anterior
		short difang = DifAngi(J[vz].Frente, proxFrente);
		//BUG: Armas[codarma].CoiceMove pode ser 0! Isso s� pode dar merda
		//double coice = pow(Armas[codarma].CoiceMove, difang/2);
		double coice = Armas[codarma].CoiceMove;
		if (coice == 0)	coice = pow(0.9, (double)difang);
		else			coice = pow(coice, difang * 0.5);
		J[vz].MiraAtual = Armas[codarma].MultMira(J[vz].MiraAtual, coice);
	} else {
		J[vz].MiraAtual = Armas[codarma].MirIni;
	}

	J[vz].MiraBonus = J[vz].Mira;
	//o Sabata tem precis�o total (nem tanto) com o rev�lver e com o rifle
	if (PodMiraExtraPistolaERifle(vz) &&
		(codarma == aPistola || codarma == aRifle)) {
		J[vz].MiraBonus *= 2.5;
	}

	J[vz].SetFlag(Mirando, true);
	GastaTempo(tempo);
	TestaEncontro(false);
	
	Reescreve();
	return true;
}

void cJogo::MiraMais()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//Testa se o cara est� usando uma arma
	/*
	short &arma = J[vz].ArmaMao();
	if (arma == -1) return; //nem avisa
	/*/
	cObjeto *arma = J[vz].ArmaMao();
	if (arma == 0) return; //nem avisa
	ushort codarma = arma->GetCod();
	/**/

	//Testa se o vz mirou antes
	if (!J[vz].GetFlag(Mirando)) {
		MsgDef("Voc� precisa escolher um alvo antes de mirar!");
		return;
	}

	//Testa se a mira j� est� no m�ximo
	if (J[vz].MiraAtual == Armas[codarma].MirMax) {
		MsgDef("N�o adianta mirar mais que isto com esta com esta arma!");
		return;
	}

	//Testa se o cara tem tempo de mirar
	short tempo = TempoMirarMais;
	if (tempo > J[vz].T) {
		MsgSemTempo("mirar", tempo);
		return;
	}

	//Finalmente, mira mais
	J[vz].MiraAtual = Armas[codarma].MultMira(J[vz].MiraAtual, Armas[codarma].MirMais);
	GastaTempo(tempo);
	TestaEncontro(false);
	
	Reescreve();
}

void cJogo::Atira()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//Testa se o cara est� usando uma arma
	/*
	short &arma = J[vz].ArmaMao();
	if (arma == -1) return; //nem avisa
	/*/
	cObjeto *arma = J[vz].ArmaMao();
	if (arma == 0) return; //nem avisa
	ushort codarma = arma->GetCod();
	ushort codmun = arma->codmunicao;
	/**/

	//Testa se o vz mirou antes
	if (!J[vz].GetFlag(Mirando)) {
		MsgDef("Voc� precisa escolher um alvo antes de atirar!");
		return;
	}

	//Testa se o cara tem tempo de atirar
	short tempo = Armas[codarma].TempoAtirar;
	if (tempo > J[vz].T) {
		MsgSemTempo("atirar com esta arma", tempo);
		return;
	}

	//Testa se tem muni��o
	if (!TemMunicao(arma)) return;

	//Calcula par�metros para o tiro
	double xho, yho, zho;
	CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xho, yho, zho, 0.5);
	/*
	double xha, yha, zha;
	CentroHex3D(J[vz].AlvoX[0], J[vz].AlvoY[0], J[vz].AlvoZ[0], xha, yha, zha, 0.5);
	double d = Distancia3D(xho, yho, zho, xha, yha, zha);
	double teta = funAngTetaf(xho, yho, xha, yha);
	double phi = funAngPhif(xho, yho, zho, xha, yha, zha);
	/*/
	double d = J[vz].AlvoDist[0];
	double teta = J[vz].AlvoTeta[0];
	double phi = J[vz].AlvoPhi[0];
	/**/

	//BUG Possivel: teoricamente, o esquema de mirar garante que o vz estar� olhando na dire��o do alvo
	//double direcao = Direcao(xho, yho, xha, yha, teta);

	double k = 0.67427 / atan(0.5 / (J[vz].MiraAtual * J[vz].MiraBonus));
	double dteta = superrnd(0.8 / k);
	double dphi = ERRO_PHI * superrnd(0.8 / k);
	teta += dteta;
	phi += dphi;

	//FIRE!
	Fogo(teta, phi, xho, yho, zho, codarma, codmun, d);

	//Desliga camuflagem
	if (PodCamuflagemControlavel(vz) && J[vz].Camufl > J[vz].CamuflNat) {
		J[vz].Camufl = J[vz].CamuflNat;
	}

	//Aplica coice da arma
	if (Armas[codarma].CoiceTiro == 0) {
		J[vz].SetFlag(Mirando, false);
	} else {
		J[vz].MiraAtual = Armas[codarma].MultMira(J[vz].MiraAtual, Armas[codarma].CoiceTiro);
	}
	GastaTempo(tempo);
	//Gasta muni��o
	/*
	if (arma == aBolaDeFogo) { //bola de fogo gasta mana
		J[vz].ManaFe -= ManaBolaDeFogo;
	} else {
		J[vz].Mun[arma] -= 1;
	}
	/*/
	if (arma->UsaManaFe()) {
		J[vz].ManaFe -= Municoes[codmun].MunPac;
	} else if (Armas[codarma].SohUmTiroPorRodada()){
		arma->municao = -arma->municao;
	} else {
		arma->municao --;
		if (arma->municao == 0) { //acabou a muni��o
			if (Municoes[codmun].MunRec == -1) {
				//arma descart�vel (deve sumir quando a muni��o acaba) ex.:Disco e Shuriken
				J[vz].ApagaArma();
				arma = NULL; //para evitar BUG, este ponteiro n�o deve ser utilizado no resto desta fun��o!
			} else {
				arma->codmunicao = mSemMunicao;
			}
		}
	}
	/**/

	J[vz].Replay.GravaSom(Armas[codarma].SomPad);
	int ulttiro = contlinhatiro-1;
	J[vz].Replay.GravaFoto(AcaoAtira,
						   num(linhatiro[ulttiro].x2),
						   num(linhatiro[ulttiro].y2),
						   num(linhatiro[ulttiro].z2));

	Reescreve();

	TestaEncontro(true, Municoes[codmun].DesCam);

	TestaMortes(vz);
}

bool cJogo::TemMunicao(cObjeto* arma)
{
	/*
	if (arma == aBolaDeFogo) { //bola de fogo gasta mana
		if (J[vz].ManaFe < ManaBolaDeFogo) {
			MsgDef("Voc� n�o tem mana suficiente para mandar um bola de fogo!");
			return;
		}
	} else if (J[vz].Mun[arma] == 0) {
		if (arma == aBaforada) { //baforada
			MsgDef("Voc� s� pode dar uma baforada por rodada!");
		} else if (J[vz].MunExtra[arma] == 0) {
			MsgDef("Sua arma est� sem muni��o, jogue-a fora!");
		} else {
			MsgDef("Sua arma est� descarregada, recarregue-a!");
		}
		return;
	}
	/*/
	if (arma->UsaManaFe()) {
		if (J[vz].ManaFe < Municoes[arma->codmunicao].MunPac) {
			MsgDef("Voc� n�o tem mana suficiente para mandar um bola de fogo!");
			return false;
		}
	} else if (arma->municao == 0) {
		//if (arma->GetCod() == aBaforada) { //baforada
		//	MsgDef("Voc� s� pode dar uma baforada por rodada!");
		//} else {
			MsgDef("Sua arma est� descarregada, recarregue-a!");
		//}
		return false;
	} else if (arma->municao < 0) {
		MsgDef("Voc� s� pode dar um tiro por rodada com esta arma!");
		return false;
	}
	return true;
	/**/
}

void cJogo::Fogo(double teta, double phi,			//�ngulos
				 double xo, double yo, double zo,	//posi��o inicial
				 short codarma, short codmun,		//id da arma e da bala
				 double distalvo)					//dist�ncia at� o alvo: usado para que o tiro do blaster fa�a curvas
{
	static cProjetil Proj[MAX_PROJETEIS];
	short nproj = Municoes[codmun].Projeteis;
	double espalha = 0.01 * (nproj - 1);//espalhamento = 0.01 * (# de proj�teis - 1)
	double alcance = Municoes[codmun].Alcancef();
	int passo = 0;
	static int passos_entre_frames = 300;

	//inicia proj�teis e linhas (e sombras) dos tiros
	contlinhatiro = 0;
	contsombratiro = 0;
	for (short i=0; i < nproj; i++) {
		Proj[i].Inicia(teta, phi, xo, yo, zo, espalha, Municoes[codmun].Dano, contlinhatiro);
		linhatiro[contlinhatiro].Inicia(0.025, xo, yo, zo);
		contlinhatiro++;

		sombratiro[contsombratiro].Inicia(0.025, xo, yo, zo, &Mapa);
		contsombratiro++;
	}

	const double step = 0.002;
	for (double w=0; w<alcance; w+= step) {
		bool todospararam = true;
		//simula cada proj�til
		for (short i=0; i < nproj; i++) {
			cProjetil &P = Proj[i];
			if (!P.parou) {
				cCont cont;
				cChao contchao;
				cAfin afin, afinchao;
				bool deveexplodir = false;
				bool devericochetear = false;
				//bool testar_parede = false;

				todospararam = false;

				P.Avanca(w);
				linhatiro[P.idlinhatiro].x2 = P.xf;
				linhatiro[P.idlinhatiro].y2 = P.yf;
				linhatiro[P.idlinhatiro].z2 = P.zf;

				sombratiro[P.idlinhatiro].Move(P.xi, P.yi, P.xf, P.yf, P.zf);

                passo++;
                if (passo % passos_entre_frames == 0) {
					//drawScene(0);
					pwinMain->drawScene();
				}

				short mudouhex = P.MudouHex();

				if (mudouhex) {
					cont	 = Mapa.Cont(P.xi, P.yi, P.zi);
					contchao = Mapa.Chao(P.xi, P.yi, P.zi);
					afin	 = Mapa.Afin(P.xi, P.yi, P.zi, cMapa::NAO_CHAO);
					afinchao = Mapa.Afin(P.xi, P.yi, P.zi, cMapa::CHAO);
					if (P.ehchao) P.SetAfinamento(&afinchao);
					else 		  P.SetAfinamento(&afin);

					//Arma > Baforada > p�e fogo por onde passa
					if (codarma == aBaforada) {
						if (!P.ehchaoant && !contchao.EhChaoBuraco() && (!cont.EhParede() || cont.EhFumaca())) {
							cont.SetFogo();
							Mapa.SetCont(P.xiant, P.yiant, P.ziant, cont);
						}
					}

					if (P.ForaDoMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ)) {
						//Arma > Disco > cai no chao antes de sair do mapa
						if (codarma == aDisco) {
							Mapa.AdObjSimpPos(OBJ_Disco, P.xiant, P.yiant, P.ziant);
						}

						P.parou = true;
						break; //exit for
					}

					P.jogperto = -1;
					//Testa se o tiro chegou perto de algu�m
					for (short j=0; j<numjogs; j++)
					if (!J[j].GetFlag(Morto)) {
						//TO DO: testar se o chegou perto da holografia do [j]
						if (P.xi == J[j].x
						&&  P.yi == J[j].y
						&&  P.zi == J[j].z
						&&  P.ehchao == (J[j].Posicao == posEnterrado)) {
							P.jogperto = j;
							
							if (j == vz) MsgDef("BUG?: um tiro atingiu o J[vz]");
							
							break; //BUG: Sup�e-se que dois jogadores nunca ocupam o mesmo lugar
						}
					}

					//Arma > Teletransportadora > move parede (if EhParede && !EhEsfumacado)
					//Arma > Teletransportadora > move chao (if EhChaoElevador)

					//TO DO: testar se acertou um Teletransporte

					//Testa se acertou algum obst�culo
					//TO DO: aplicar formato diferente das paredes
					//TO DO: fazer as balas atravessarem paredes, perdendo for�a no caminho
					/*if (P.ehchao) {
						if (contchao.EhChaoPedra()) {
							P.dano -= 1000;
							devericochetear = true;
						} else if (contchao.EhChaoDuro()) {
							P.dano -= 45;
							devericochetear = true;
						} else if (contchao.EhChaoGrade()) {
							if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
							&& (rnd() > 0.1 || Armas[arma].RaioExplosao > 2)) {
								P.dano -= 50;
								devericochetear = true;
							}
						} else if (!contchao.EhChaoBuraco()) {
							P.dano -= 15;
						}
					} else if (!afin.EhAfinInteira() && !P.AcertouParede(afin)) {
						//� uma parede fina e o tiro ainda n�o acertou
						//o tiro pode acertar a parede antes de mudar de hex�gono
						P.parede_fina_perto = true;
					} else {
						testar_parede = true;
					}*/
				}/* else if (P.parede_fina_perto) {				
					//est� pr�ximo a uma parede fina, mas n�o acertou ela ainda => testar se acertou agora
					if (P.AcertouParede(afin)) {
						P.parede_fina_perto = false;
						testar_parede = true;
					}
				}*/
				
				if (P.AcertouParede()) {
					if (P.ehchao) {
						if (contchao.EhChaoPedra()) {
							P.dano -= DurezaPedra;
							devericochetear = true;
						} else if (contchao.EhChaoDuro()) {
							P.dano -= DurezaMedia;
							devericochetear = true;
						} else if (contchao.EhChaoGrade()) {
							if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
							&& (rnd() > 0.1 || Municoes[codmun].RaioExplosao > 2)) {
								P.dano -= DurezaGrade;
								devericochetear = true;
							}
						} else if (!contchao.EhChaoBuraco()) {
							P.dano -= DurezaFraca;
						}
					} else {
						if (cont.EhParedePedra()) {
							P.dano -= DurezaPedra;
							devericochetear = true;
						} else if (cont.EhParedeDura()) {
							P.dano -= DurezaMedia;
							devericochetear = true;
						} else if (cont.EhLata()) {
							//TO DO: explode
						} else if (cont.EhGrade()) {
							if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
							&& (rnd() > 0.1 || Municoes[codmun].RaioExplosao > 2)) {
								P.dano -= DurezaGrade;
								devericochetear = true;
							}
						} else if (cont.EhParedeSolida()) {
							P.dano -= DurezaFraca;
						}
					}
				}
				/*//mudou de hex ou acertou uma parede fina
				if (testar_parede) {
					if (cont.EhParedePedra()) {
						P.dano -= 1000;
						devericochetear = true;
					} else if (cont.EhParedeDura()) {
						P.dano -= 45;
						devericochetear = true;
					} else if (cont.EhLata()) {
						//TO DO: explode
					} else if (cont.EhGrade()) {
						if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
						&& (rnd() > 0.1 || Armas[arma].RaioExplosao > 2)) {
							P.dano -= 50;
							devericochetear = true;
						}
					} else if (cont.EhParedeSolida()) {
						P.dano -= 15;
					}
				}*/

				//Testa se o tiro atinge J[P.jogperto]
				if (P.jogperto != -1) {
					//dist�ncia do proj�til ao centro do hex.
					double distcentro = Distancia2D(P.xf, P.yf, P.xch, P.ych);
					double raiodocara = J[P.jogperto].Raio * LARGURAHEX;

					if (raiodocara > distcentro) {
						//SubPoderes > Defender tiro com sabre de luz
						if (Municoes[codmun].RaioExplosao == 0
						&&  PodDefendeComEspada(P.jogperto)
						&&  J[P.jogperto].ArmaMaoCod() == aSabreDeLuz
						&&  J[P.jogperto].TProxRodada >= TempoDefesaComEspada
						&&  EstaSendoVistoPor(P.jogperto)) {

							MsgDef("Ele defendeu o tiro com a espada!!");
							J[P.jogperto].TProxRodada -= TempoDefesaComEspada;
							
							//
							//devericochetear = true; //faz o tiro ricochetear se for uma arma ricochet.
							P.dano = 0; //tem o mesmo significado de bater em uma parede indestrut�vel

						} else if (PodSuperEsquiva(P.jogperto)
							   &&  J[P.jogperto].TProxRodada >= TempoEsquivaMatrix
							   &&  EstaSendoVistoPor(P.jogperto)) {

							MsgDef("Ele se esquivou do tiro!!");
							J[P.jogperto].TProxRodada -= TempoEsquivaMatrix;
						} else if (Municoes[codmun].RaioExplosao == 0) {
							double modblindagem = 1;

							if (ArmaEhIncendiaria(codarma)) {
								if (PodImuneAFogo(P.jogperto)) {
									P.dano = 0;
								} else if (PodResistenteAFogo(P.jogperto)) {
									modblindagem = 2;
								} else {
									modblindagem = 0.5; //blindagem normal � pior contra fogo
								}
							//Arma > Tri-Laser > Perfura blindagem
							} else if (codarma == aTriLaser) {
								modblindagem = 0.5;
							//SubPoderes > Mira extra > blindagem menor (a cara � t�o bom que atinge um ponto fraco na blindagem dos advers�rios)
							} else if (PodMiraExtraPistolaERifle(vz)
								   && (codarma == aPistola || codarma == aRifle)) {
								modblindagem = 0.4;
							//Arma > Teletransportadora > ignora blindagem
							} else if (codarma == aTeletrans) {
								modblindagem = 0;
							}

							//Arma > Paralisante > ao inv�s de dor ela deixa a pessoa lenta
							if (codarma == aParalisante) {
								J[P.jogperto].TMax = Max(25, J[P.jogperto].TMax - 15);
							}

							double blindagem = modblindagem * J[P.jogperto].Blindagem;
							double dor = CausaDor(P.jogperto, P.dano, blindagem);
							if (dor > 0) {
								//enfraquece o dano do proj�til,
								//caso ele n�o seja zerado o proj�til deve continuar andando
								P.dano -= blindagem + DurezaJog;
							
								//Arma > Degenera��o
								J[P.jogperto].Reg -= Municoes[codmun].Degen;

								//Arma > Teletransportadora > teletransp. pessoa
							}
						}
						P.jogperto = -1; //evita que o cara seja atingido duas vezes
					}
				}

				//Testes de impacto (atravessa, para ou ricocheteia)
				//Arma > Ricocheteadora > tiro ricocheteia
				/*if (arma == aRicocheteadora && devericochetear) { // # de ricocheteadas < limite (era 20)
					//TO DO: ricochetear o tiro
				} else*/ if (P.dano < P.danoant) {
					//TO DO: (OR NOT TO DO) armas incendi�rias devem atravessar paredes?

					if (P.dano > 0) {
						//Arma > Disco > quebra a parede/ch�o
						if (codarma == aDisco) {
							if (P.ehchao) {
								contchao.SetChaoBuraco();
								Mapa.SetChao(P.xi, P.yi, P.zi, contchao);
							} else {
								cont.SetFumaca();
								Mapa.SetCont(P.xi, P.yi, P.zi, cont);
							}
						}

						//TEMP: as paredes de madeira n�o devem ser destru�das � bala
						if (cont.EhParedeMadeira() && !P.ehchao) {
							cont.SetFumaca();
							Mapa.SetCont(P.xi, P.yi, P.zi, cont);
						}

						//Arma > Baforada > p�e fogo nas paredes por onde passa
						if (codarma == aBaforada) {
							if (!P.ehchao) {
								cont.SetFogo();
								Mapa.SetCont(P.xi, P.yi, P.zi, cont);
							}
						}
					} else {
						P.parou = true;
						deveexplodir = true;

						//Arma > Disco > cai no ch�o
						if (codarma == aDisco) Mapa.AdObjSimpPos(OBJ_Disco, P.xiant, P.yiant, P.ziant);
					}
				}

				if (deveexplodir) {
					//Arma > EhExplosiva > explode
				}

				//Arma > Blaster > tiro muda de dire��o para o pr�ximo alvo (if w > distalvo && tiver prox. alvo)

				P.Valida();
			}
		}
		if (todospararam) break; //exit for
	}
}

/*
Sub Explos�o(XCentro As Integer, YCentro As Integer, XL As Integer, YL As Integer, ZCentro As Byte, DanoMeio As Single, Raio As Single, Tipo As Integer)
'Tipo 1 = Normal
'Tipo 2 = Incendi�rio
'Tipo 3 = Fuma�a
Dim cont, n
Dim Dano As Single, danobruto As Single, danobrutoabaixo As Single
Dim bobo As Single, bl As Single
Dim xe, ye, ze, atingido As Integer
Dim DistCentro As Single
ReDim Paredes(-Int(Raio) - 2 To Int(Raio) + 2, -Int(Raio) - 2 To Int(Raio) + 2, -Int(Raio / AlturaAndar) - 1 To Int(Raio / AlturaAndar) + 1) As Integer
ContSons = ContSons + 1
Select Case DanoMeio
    Case Is >= 20: RepSom(ContSons) = Sons.Explo2
    Case Is > 1: RepSom(ContSons) = Sons.Explo1
    Case Else: ContSons = ContSons - 1
End Select

Raio = Abs(Raio)
If XL = 0 Then XL = XCentro
If YL = 0 Then YL = YCentro
For xe = -Int(Raio) - 2 To Int(Raio) + 2
    For ye = -Int(Raio) - 2 To Int(Raio) + 2
        For ze = -Int(Raio / AlturaAndar) - 1 To Int(Raio / AlturaAndar) + 1
            DistCentro = Distancia(XCentro, YCentro, XCentro + xe, YCentro + ye)
            If XCentro + xe < 1 Or XCentro + xe > xM Or YCentro + ye < 1 Or YCentro + ye > yM Or ZCentro + ze < 0 Or ZCentro + ze > AndMax Then
                'Fora do mapa
                Paredes(xe, ye, ze) = 1000
            ElseIf Sqr(DistCentro ^ 2 + (AlturaAndar * ze) ^ 2) > Raio Then
                'Fora do raio
                Paredes(xe, ye, ze) = 1000
            ElseIf Tipo <> 1 And ze <> 0 Then
                'explos�o de fogo ou fuma�a
                Paredes(xe, ye, ze) = 1000
            'ElseIf XCentro + xe = XL And YCentro + ye = YL And ze = 0 Then
            '    'bem no meio
            '    Paredes(xe, ye, ze) = 0
            '    Paredes(0, 0, 0) = 0
            Else
                Paredes(xe, ye, ze) = Linha(XL, YL, XCentro + xe, YCentro + ye, ZCentro + ze)
            End If
        Next
    Next
Next
Paredes(0, 0, 0) = 0
For xe = -Int(Raio) - 2 To Int(Raio) + 2
    For ye = -Int(Raio) - 2 To Int(Raio) + 2
        For ze = -Int(Raio / AlturaAndar) - 1 To Int(Raio / AlturaAndar) + 1
            'If Sqr(xe ^ 2 + ye ^ 2 + (AlturaAndar * ze) ^ 2) > Raio Then
            DistCentro = Distancia(XCentro, YCentro, XCentro + xe, YCentro + ye)
            If Sqr(DistCentro ^ 2 + (AlturaAndar * ze) ^ 2) > Raio Then
                GoTo SairNexte 'Next ze
            ElseIf Paredes(xe, ye, ze) >= 1000 Then
                GoTo SairNexte
            End If
            
            ' dano no atingido
            'danobruto = DanoMeio * (1 - Sqr(xe ^ 2 + ye ^ 2 + (AlturaAndar * ze) ^ 2) / Raio)
            danobruto = DanoMeio * (1 - Sqr(DistCentro ^ 2 + (AlturaAndar * ze) ^ 2) / Raio) _
                - Paredes(xe, ye, ze)
            If danobruto < 0 Then GoTo SairNexte

            ' p�e fuma�a, buraco ou sangue
            cont = Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze)
            If ((cont = 5 Or cont = 34 Or cont = 20 Or cont = 52 Or cont = 53 Or cont = 35 Or cont = 44) And danobruto < 7) Or cont = 26 Or cont = 47 Or cont = 48 Then
                '((acertou uma parede indestrutivel (5)ou vidro (34 ou grade(44))) e dano for baixo) ou
                'um buraco vis�vel (26) ou elevador (20) ou teletransporte(35) ou pared�o(47) - n�o p�e nada
            ElseIf cont = 25 Then
                'se acerta um buraco invis�vel (25), ele vira vis�vel (26)
                If Tipo = 1 Then Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze) = 26
            ElseIf cont = 32 Or cont = 14 Then 'Lata ou granada
                Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze) = 0
                Call Explos�o(XCentro + xe, YCentro + ye, 0, 0, ZCentro + ze, 6, 3.1, 1)
            Else
                'nesta situa�ao, p�e fuma�a ou fogo
                If cont <> 31 And cont <> 40 And cont <> 42 And danobruto > 0 Then 'n�o apaga fogo nem sangue alien
                    bobo = Rnd
                    'explos�o incendi�ria deixa pouca fuma�a e expl. de fuma�a sempre deixa fuma�a
                    If (Tipo = 1 And danobruto > 3 And bobo < 0.15) Or (Tipo = 2 And danobruto > 1 And bobo < 0.8) Then
                        bobo = Rnd
                        If bobo < 0.9 Then
                            Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze) = 31 'fogo
                        Else
                            Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze) = 40 'fogo perp�tuo
                        End If
                    Else 'inclui tipo 3 (fuma�a)
                        Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze) = 22 'fuma�a
                    End If
                End If
                If Tipo = 3 And cont = 0 Then
                    Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze) = 22 'fuma�a
                End If
                If (ZCentro + ze) > 0 Then
                    danobrutoabaixo = DanoMeio * (1 - Sqr(xe ^ 2 + ye ^ 2 + (AlturaAndar * (ze - 1)) ^ 2) / Raio) - Paredes(xe, ye, ze - 1)
                    'If Sqr(xe ^ 2 + ye ^ 2 + (AlturaAndar * (ze - 1)) ^ 2) <= Raio Then
                    If danobrutoabaixo > 0 Then
                        If Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze - 1) <> 47 And Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze - 1) <> 48 Then
                            'Se um andar abaixo, houver alcance, p�e buraco
                            If Tipo = 1 Then Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze) = 26
                        End If
                    End If
                End If
            End If
            ' v� quem foi atingido
            For n% = 1 To M + NMons
                If n% = NJogs + 1 And NJogs < M Then n% = M + 1
                If XCentro + xe = j(n%).X And YCentro + ye = j(n%).Y And ZCentro + ze = j(n%).Z Then
                    If j(n%).Enterrado = True Then
                        bl = 4
                    ElseIf Tipo = 2 And (j(n%).Poder = pRobocop Or j(n%).Poder = pDragao Or j(n%).Poder = pTanque) Then
                        bl = 100 'Eles s�o imunes a fogo
                    Else
                        bl = j(n%).Blindagem
                    End If
                    Dano = Dor(CByte(n%), danobruto, bl)
                End If
            Next
            'For n% = 1 To NMons
            '    If XCentro + xe = X(n% + M) And YCentro + ye = Y(n% + M) And ZCentro + ze = Z(n% + M) Then
            '        dano = Dor(n% + M, danobruto, 0)
            '    End If
            'Next
            
            If Andar = ZCentro + ze Then
                'Call Obst(Conte�do(XCentro + xe, YCentro + ye, ZCentro + ze), XCentro + xe, YCentro + ye, True)
                Call Obst(XCentro + xe, YCentro + ye, ZCentro + ze, XCentro + xe, YCentro + ye, True)
            End If
            atingido = 0
SairNexte: 'Serve para pular para o pr�ximo n�mero
        Next
    Next
Next

End Sub
*/