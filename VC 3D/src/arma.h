
#ifndef ARMA_H
#define ARMA_H

#define ARMA_MAX_ALVOS 4 //quantos alvos intermedi�rios o blaster pode ter

enum {
	aPistola = 0,
	aEspingarda,
	aMetralhadora,
	aLancaFog,
	aRifle,
	aDegen,
	aDisco,
	aTriLaser,
	aMiniGun,
	aLancaChamas,
	aRicochet,
	aRoboArma,
	aParalisante,
	aBaforada,
	aTeletrans,
	aBlaster,
	aMachado,
	aEspinhos,
	aMG34,
	aCanhao,
	aBolaDeFogo,
	aSabreDeLuz,
	aShuriken,
	aKatana,
	aSoco,
	aTrabuco,
	//aDedoDuro,
	aSnikt,
	NUM_ARMAS
};

enum {
	mSemMunicao = 255,
	mPistola = 0,
	mEspingarda,
	mMetralhadora,
	mLancaFog,
	mRifle,
	mDegen,
	mDisco,
	mTriLaser,
	mMiniGun,
	mLancaChamas,
	mRicochet,
	mRoboArma,
	mParalisante,
	mBaforada,
	mTeletrans,
	mBlaster,
	//aMachado,
	mEspinhos,
	mMG34,
	mCanhaoHE,
	mCanhaoAP,
	mBolaDeFogo, //usaMana
	//aSabreDeLuz,
	mShuriken, //??
	//aKatana,
	//aSoco,
	mTrabuco,
	//aDedoDuro,
	//aSnikt,
	NUM_MUN
};

class cArma
{
public:
	double MirIni, MirMin, MirMax, MirMais, CoiceTiro, CoiceMove;
	short TempoMirar, TempoAtirar, TempoRecarr;

	//short Municao, MunExtra, MunRec, Projeteis;
	//double Dano, DesCam, Alcance, RaioExplosao, Degen;
	short MunPadrao, MunExtra;
	double DanoGolpe;

	short SomPad;//, SomRaj;

	short id;
	char Nome[20];

	cArma();
	void Inicia(short _id,
				char *_Nome,
				/*
				short _Municao,
				short _MunRec,
				short _MunExtra,
				double _DesCam,
				short _Projeteis,
				double _Dano,
				double _RaioExplosao,
				double _Degen,
				double _Alcance,
				/*/
				short _MunExtra,
				/**/
				double _DanoGolpe,
				double _MirIni,
				double _MirMax,
				double _MirMais,
				short _TempoMirar,
				short _TempoAtirar,
				short _TempoRecarr,
				double _CoiceTiro,
				double _CoiceMove,
				short _MunPadrao,
				short _SomPad);
	
	bool SohUmTiroPorRodada();

	double MultMira(double mira, double mult);
};

class cMunicao
{
public:
	short MunPac, MunRec, Projeteis;
	double Dano, DesCam, Alcance, RaioExplosao, Degen;
	
	short ArmaDest; //para qual arma esta muni��o est� destinada

	short id;
	char Nome[20];

	cMunicao();
	void Inicia(short _id,
				char *_Nome,
				short _MunPac,
				short _MunRec,
				double _DesCam,
				short _Projeteis,
				double _Dano,
				double _RaioExplosao,
				double _Degen,
				double _Alcance,
				short _ArmaDest);

	bool NaoRecarregavel();

	double Alcancef();
};

#endif
