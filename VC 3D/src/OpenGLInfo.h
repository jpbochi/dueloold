
#ifndef OPENGLINFO_H
#define OPENGLINFO_H

#include "fx.h"
#include "fx3d.h"

// Settings dialog thanks to Sander Jansen <sxj@cfdrc.com>
class SettingsDialog : public FXDialogBox {
  FXDECLARE(SettingsDialog)
private:
  SettingsDialog(){}
public:
  SettingsDialog(FXWindow* owner,FXGLVisual * vis);
  virtual ~SettingsDialog();
  
	long onOK(FXObject *,FXSelector,void*);
  };

#endif