
#ifndef GLCONTEXT_H
#define GLCONTEXT_H

#ifndef FXGLCanvas
	#include "fx.h"
	#include "fx3d.h"
#endif

class cGLContexto
{
protected:
	FXGLCanvas *glcanvas;
	int pilha [32];
	int pos_pilha;
	
public:
	cGLContexto();

	void SetaContexto(FXGLCanvas *);
	
	void Empilha();
	void DesEmpilha();
};

#ifdef DECLARA_GLContexto
	cGLContexto GLContexto;
	#undef DECLARA_GLContexto
#else
	extern cGLContexto GLContexto;
#endif

#endif
