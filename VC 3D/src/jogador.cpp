
//####################################
//########## CLASSE JOGADOR ##########
//####################################

#include "jogador.h"
#include "declarac.h"
#include "geral.h"
#include "funcoes.h"
#include "conteudo.h"
#include "replay.h"
#include "mapa.h"

//#include <stdio.h>

cJogador::cJogador()
{
	Poder = pSorteado;
	idHost = -1;
	ZeraJogador();

	objMaoDir = 0;
	objMaoEsq = 0;
	objCabeca = 0;
	objCorpo = 0;
}

cJogador::cJogador(const int pid)
{
	SetId(pid);
	Nome.format("Jog %d", id);//sprintf(Nome, "Jog %d", id);
	Poder = pSorteado;
	ZeraJogador();
}

void cJogador::SetNome(char *val)
{
	Nome.format("Jog %d", val);//sprintf(Nome, "%s", val);
}

void cJogador::SetId(GLubyte novo_id)
{
	id = novo_id;
	idImit = id;
	Time = id;
}

void cJogador::SetFlag(int flag, bool val)
{
	if (val) flags |= flag;
	else	 flags &= ~flag;
}

int cJogador::GetFlag(int flag) {return flags & flag;}

void cJogador::ZeraFogBit()
{
	FogBit->Esvazia();
}

void cJogador::ZeraReplay()
{
	Replay.Zera(x, y, z, Frente);
}

void cJogador::ZeraJogador()
{
	Fog = NULL;
	FogBit = NULL;
	flags = 0;
	x = -100;
	y = -100;
	z = -100;
	Raio = 0.5;
	Mira = 1;
	Blindagem = 0;
	Forca = 30;
	Reg = 1; RegNat = 1;
	Camufl = 1; CamuflNat = 1; 
	Olho = 1; OlhoNat = 1;
	MagicRes = 1; MagicResNat = 1;

	//Cor[0] = 255;
	//Cor[1] = 255;
	//Cor[2] = 255;
	//Textura = 0;
	cur = 0;

	//para estat�sticas
	Time = -1;
	Kills = 0;
	Mortes = 0;
	AtacanteDegen = -1;

	//arma
	/*ArmaMao = -1;
	for (int i=0; i<NUM_ARMAS; i++) {
		Mun[i] = 0;
		MunExtra[i] = 0;
		PossuiArma[i] = false;
	}*///nenhuma destas vari�veis existe mais

	//estado
	SetFlag(Mirando, false);
	Posicao = posNormal;

	//equipamentos
	/*Bombas = 0;
	BombasAtivadas = 0;
	MedKits = 0;
	Minas = 0;
	Granadas = 0;
	Proteinas = 0;
	Holografias = 0;
	Fumaca = 0;
	Invis = 0;*/

	//malucos
	Injetor = -1;
	Incubacao = 0;
	ManaFe = 100;
}

bool cJogador::TemMaoLivre()
{
	return !objMaoDir || !objMaoEsq;
}

void cJogador::ApagaArma()
{
	cObjeto *arma = ArmaMao();
	if (arma == objMaoDir) {
		delete objMaoDir;
		objMaoDir = NULL;
		arma = NULL;
	} else if (arma == objMaoEsq) {
		delete objMaoEsq;
		objMaoEsq = NULL;
		arma = NULL;
	}
}

//### Estas fun��es s� s�o utilizadas para equipar o sujeito no in�cio do jogo
void cJogador::PegaArma(ushort codarma, cArma *Armas, cMunicao *Municoes)
{
	cObjeto obj(codarma, cObjeto::tipArma, id);
	obj.CarregaArma(Armas, Municoes);
	ushort codmunicao = obj.codmunicao;
	
	//algumas armas s�o skills tamb�m (ex.:aBolaDeFogo, aSnikt)
	cListaObjetos &compartimento = (obj.EhArmaSkill()) ? Skills : Equip;

	//adiciona a arma (caso seja a primeira, p�e na m�o)
	if (objMaoDir == 0)	objMaoDir = new cObjeto(obj);
	else				compartimento.AdObj(obj);

	//adiciona muni��es extras
	for (int i=0; i<Armas[codarma].MunExtra; i++) {
		if (Municoes[codmunicao].NaoRecarregavel()) {
			//nas armas descart�veis (disco) a muni��o extra � uma arma
			compartimento.AdObj(obj);
		} else {
			PegaMun(codmunicao, Municoes);
		}
	}
	
	/*
	short i = arma.id;
	ArmaMao = i;
	Mun[i] = arma.Municao;
	MunExtra[i] += arma.MunExtra;
	PossuiArma[i] = true;
	*/
}
void cJogador::PegaMun(ushort codmun, cMunicao *Municoes)
{
	cObjeto obj(codmun, cObjeto::tipMunicao, id);
	obj.municao = Municoes[codmun].MunPac;
	Equip.AdObj(obj);
}

void cJogador::PegaEquip(ushort codequip)
{
	cObjeto obj(codequip, cObjeto::tipEquip, id);
	Equip.AdObj(obj);
}
void cJogador::PegaSkill(ushort codskill)
{
	cObjeto obj(codskill, cObjeto::tipSkill, id);
	Skills.AdObj(obj);
}

void cJogador::AcionaPoder(cArma *Armas, cMunicao *Municoes)
{
	while (Poder == pSorteado) {
		//TO DO: � pra pegar das prefer�ncias (vide dueloVB->FuncoesMod.bas->PoderAciona())
		Poder = 1 + (GLubyte)((NUM_PODERES - 1) * rnd());
		if (Poder == pAlienGrande) Poder = pSorteado; // ningu�m pode come�ar como um alien grande
	}

	Ht = 0; //por causa do tarrasque (veja ap�s o switch)
	
	switch (Poder) {
		case pPM:
			HtMax = 100;
			TMax = 100;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pAtirador_de_Elite:
			HtMax = 100;
			TMax = 140;
			Mira = 2.5;
			OlhoNat = 1.5;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pVelocista:
			HtMax = 100;
			TMax = 100;
			OlhoNat = 0.9;	CamuflNat = 0.9;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pCasca_Grossa:
			HtMax = 170;	RegNat = 1.05;	Blindagem = 2.5;
			TMax = 100;
			Forca = 36;
			CamuflNat = 0.95;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pTroll:
			HtMax = 115;	RegNat = 1.5;
			TMax = 100;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pBoina_Verde:
			HtMax = 115;	RegNat = 1.1;
			TMax = 110;
			Mira = 1.2;
			Forca = 40;			
			OlhoNat = 1.15;	CamuflNat = 1.15;
			MagicResNat = 1.1;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pPredador:
			HtMax = 200;
			TMax = 140;
			Mira = 1.4;
			Forca = 50;
			OlhoNat = 2.5;	Olfato = 10;
			SetFlag(TemIR, true);
			SetFlag(TemDetec, true);
			PegaArma(aDisco, Armas, Municoes);// MunExtra[6] = 3; //disco
			PegaArma(aTriLaser, Armas, Municoes); //tri-laser
			PegaSkill(sCamuflagem); //camufl *= 5
			PegaSkill(sAutoDestruicao);
			break;
		case pWolverine:
			HtMax = 120;	RegNat = 1.15;
			TMax = 120;
			Forca = 40;
			OlhoNat = 1.15;	Olfato = 16;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aSnikt, Armas, Municoes); //snikt
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pVoador:
			HtMax = 110;
			TMax = 120;
			Forca = 36;
			CamuflNat = 0.8;
			MagicResNat = 1.1;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pFantasma:
			HtMax = 100;
			TMax = 110;
			Forca = 24;
			CamuflNat = 1.8;
			MagicResNat = 1.7;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pTarrasque:
			HtMax = 900;	Ht = 600;	Blindagem = 5;
			TMax = 80;
			Forca = 300;
			OlhoNat = 1.6;	CamuflNat = 0.3;	Olfato = 8;
			MagicResNat = 1.2;
			break;
		case pRobocop:
			HtMax = 140;	Blindagem = 27;
			TMax = 90;
			Forca = 0;
			OlhoNat = 0.85;	CamuflNat = 0.5;	Olfato = 0;
			MagicResNat = 0.9;
			SetFlag(TemDetec, true);
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aRoboArma, Armas, Municoes); //pistol�o do robocop
			break;
		case pHomem_do_Raio_X:
			HtMax = 100;
			TMax = 100;
			MagicResNat = 1.2;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pBalacau:
			HtMax = 140;
			TMax = 190;
			Forca = 45;
			OlhoNat = 0.5;	CamuflNat = 1.1;	Olfato = 12;
			MagicResNat = 1.3;
			PegaSkill(sCamuflagem); //camufl *= 6
			break;
		case pMosca:
			HtMax = 170;
			TMax = 100;
			Mira = 0.8;
			Olfato = 7;
			Forca = 60;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pDragao:
			HtMax = 250;	Blindagem = 5;
			TMax = 130;
			Forca = 200;
			OlhoNat = 2.0;	CamuflNat = 0.5;	Olfato = 16;
			MagicResNat = 1.6;
			PegaArma(aBaforada, Armas, Municoes); //baforada
			break;
		case pMago:
			HtMax = 70;
			TMax = 100;
			Mira = 0.65;
			Forca = 24;
			CamuflNat = 0.9;	Olfato = 2;
			MagicResNat = 1.4;
			PegaArma(aBolaDeFogo, Armas, Municoes); //bola de fogo
			PegaSkill(sIdentificacao);
			PegaSkill(sPararTempo);
			PegaSkill(sInvisibilidade);
			PegaSkill(sTeletransporte);
			PegaSkill(sImitar);
			PegaSkill(sHolografia);
			PegaSkill(sDardosMisticos);
			break;
		case pEthereal:
			HtMax = 85;
			TMax = 110;
			Mira = 0.6;
			Forca = 24;
			OlhoNat = 4.0;	CamuflNat = 1.3;
			MagicResNat = 2;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			PegaSkill(sControlePsionico);
			break;
		case pJason:
			HtMax = 210;
			TMax = 190;
			Forca = 0;
			OlhoNat = 1.5;	CamuflNat = 0.8;
			MagicResNat = 1.1;
			PegaArma(aMachado, Armas, Municoes); //machado
			break;
		case pAlienPequeno:
			HtMax = 40;
			TMax = 130;
			Forca = 0;
			OlhoNat = 0.8;	CamuflNat = 2.0;	Olfato = 9;
			Raio = 0.1;
			break;
		case pAlienGrande:
			HtMax = 210; Ht = 70;
			TMax = 175;
			Forca = 60;
			OlhoNat = 0.8;	CamuflNat = 1.1;	Olfato = 13;
			break;
		case pDemolidor:
			HtMax = 125;
			TMax = 110;
			Mira = 1.2;
			Forca = 36;
			OlhoNat = 0;	CamuflNat = 0.9;	Olfato = 5;
			MagicResNat = 1.2;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			PegaEquip(eGranada); //12
			PegaEquip(eMina); //6
			PegaEquip(eBomba); //3
			break;
		case pMagrao:
			HtMax = 80;
			TMax = 100;
			Forca = 24;
			CamuflNat = 2.0;
			Raio = 0.15;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pZerg:
			HtMax = 100;	RegNat = 1.1;
			TMax = 110;
			Forca = 40;
			MagicResNat = 1.1;
			PegaArma(aEspinhos, Armas, Municoes); //espinhos
			PegaSkill(sEnterrarse);
			break;
		case pTanque:
			HtMax = 210;	Blindagem = 50;
			TMax = 130;
			Forca = 0;
			OlhoNat = 0.8;
			CamuflNat = 0.3;	Olfato = 0;
			SetFlag(TemDetec, true);
			PegaArma(aMG34, Armas, Municoes); //mg-34
			//PegaArma(aCanhaoHE); //canh�o HE
			//PegaArma(aCanhaoAP); //canh�o AP
			PegaArma(aCanhao, Armas, Municoes); //canh�o, s� faltam as balas!
			PegaMun(mCanhaoHE, Municoes);
			break;
		case pSabata:
			HtMax = 100;
			TMax = 100;
			Mira = 5;
			Forca = 32;
			OlhoNat = 1.5;	CamuflNat = 0.9;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pBlindado:
			HtMax = 115;	Blindagem = 30;
			TMax = 95;
			Forca = 36;
			CamuflNat = 0.9;	Olfato = 3;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pJedi:
			HtMax = 115;
			TMax = 135;
			Forca = 40;
			OlhoNat = 2.0;	Olfato = 6;
			MagicResNat = 1.4;
			PegaArma(aSabreDeLuz, Armas, Municoes); //espada de luz
			break;
		case pNinja:
			HtMax = 100;
			TMax = 120;
			Forca = 36;
			OlhoNat = 1.2;	CamuflNat = 1.8;
			MagicResNat = 1.2;
			PegaArma(aShuriken, Armas, Municoes); //estrelinhas
			PegaArma(aKatana, Armas, Municoes); //katana
			PegaSkill(sKitDeArrombamento);
			PegaSkill(sDesarmarMinas);
			break;
		case pHorla:
			HtMax = 85;
			TMax = 110;
			Mira = 0.6;
			Forca = 20;
			OlhoNat = 0.9;	CamuflNat = 15.0;
			MagicResNat = 2;
			//PegaArma(aSoco, Armas, Municoes); //soco
			break;
		case pCapangas:
			HtMax = 85;
			TMax = 95;
			Mira = 0.8;
			Forca = 28;
			OlhoNat = 0.9;	CamuflNat = 0.85;	Olfato = 3.5;
			MagicResNat = 0.9;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aMetralhadora, Armas, Municoes);  //metralhadora
			break;
		case pDinamitador:
			HtMax = 100;
			TMax = 95;
			Mira = 0.9;
			Forca = 40;
			CamuflNat = 0.9;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaSkill(sAutoDestruicao);
			break;
		case pT_1000:
			HtMax = 210;	Blindagem = 15;
			TMax = 145;
			Forca = 60;
			OlhoNat = 1.2;	Olfato = 0;
			MagicResNat = 1.5;
			PegaSkill(sImitar); //N�o sei se n�o seria melhor criar um skill separado do mago
			PegaSkill(sRegT1000);
			break;
		case pAgente_do_Matrix:
			HtMax = 125;
			TMax = 130;
			Mira = 1.5;
			Forca = 40;
			OlhoNat = 1.25;
			MagicResNat = 1.2;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			break;
		case pClerigo_Maligno:
			HtMax = 85;
			TMax = 100;
			Mira = 0.8;
			Forca = 28;
			CamuflNat = 0.9;	Olfato = 3;
			MagicResNat = 1.6;
			//PegaArma(aSoco, Armas, Municoes); //soco
			PegaArma(aPistola, Armas, Municoes);  //pistola
			PegaSkill(sRezar);
			PegaSkill(sCura);
			PegaSkill(sAnimarMortos);
			PegaSkill(sRelampago);
			break;
		default:
			//bug!
			break;
	}
	TProxRodada = TMax;
	if (Ht == 0) Ht = HtMax;
	Olho = OlhoNat;
	Camufl = CamuflNat;
	Reg = RegNat;
	MagicRes = MagicResNat;
}

/*char* PoderStr(int poder)
{
	static char* listapod[] =
	{
		"Sorteado",
		//.......
	};
	char *ret = new char[strlen(listapod[poder]) + 1];
	sprintf(ret, "%s", listapod[poder]);
	return ret;
	//return PoderLista[Poder];
}
inline char* PoderStrOverloaded(int poder){return PoderStr(poder);}
*/

void cJogador::PoderStr(int poder, FXString &ret)
{
	static FXString listapod[] =
	{
		"Sorteado",
		"PM",
		"Atirador de Elite",
		"Velocista",
		"Casca Grossa",
		"Troll",
		"Boina Verde",
		"Predador",
		"Wolverine",
		"Voador",
		"Fantasma",
		"Tarrasque",
		"Robocop",
		"Homem do Raio X",
		"Balacau",
		"Mosca",
		"Dragao",
		"Mago",
		"Ethereal",
		"Jason",
		"AlienPequeno",
		"AlienGrande",
		"Demolidor",
		"Magr�o",
		"Zerg",
		"Tanque",
		"Sabata",
		"Blindado",
		"Jedi",
		"Ninja",
		"Horla",
		"Capangas",
		"Dinamitador",
		"T 1000",
		"Agente do Matrix",
		"Cl�rigo Maligno"
	};
	ret = listapod[poder];
}

void cJogador::PoderStr(FXString &ret)
{
	PoderStr(Poder, ret);
}

short cJogador::ArmaMaoCod()
{
	cObjeto *arma = ArmaMao();
	if (arma != 0) {
		return arma->GetCod();
	} else {
		return -1; //desarmado
	}
}

cObjeto *cJogador::ArmaMao()
{
	if (objMaoDir != 0) {
		if (objMaoDir->EhArma()) {
			return objMaoDir;	//Arma na m�o direita tem prioridade
		}
	}
	if (objMaoEsq != 0) {		//se n�o houver uma arma na m�o direita (outra coisa pode)
		if (objMaoEsq->EhArma()) {
			return objMaoEsq;	//ent�o vai a arma da m�o esquerda
		}
	}
	return 0; //desarmado
}

//char* cJogador::PoderStr()
//{
//	return PoderStrOverloaded(Poder);
//}

//-------------------------------------------------------------------
//CARACTER�STICAS DOS PODERES ---------------------------------------
//-------------------------------------------------------------------
char cJogador::PodSangue()
{
	//HARDCODE = newbie!!!
	switch (Poder) {
		case pPredador:		return 18; //sangue verde
		case pAlienPequeno:
		case pAlienGrande:	return 42; //sangue �cido
		case pT_1000:
		case pTanque:		return 0; //n�o sangram
		default:			return 2; //sangue normal
	}
}

//void cJogador::PodCorpo(char *parede, cObjetoSimples *objsimp, cObjeto *obj)
void cJogador::PodSoltaCorpo(cMapa *mapa)
{
	//TO DO: Se o morto estiver com a auto destrui��o ativada => Devolver um esqueleto modificado
	switch (Poder) {
		case pFantasma:
		case pRobocop:
		case pHorla:
		case pT_1000:			//nada
		return;
		case pTanque:{			//fogo permanente
			cCont corpo;
			corpo.SetFogoPermanente();
			mapa->SetCont(x, y, z, corpo);
		} return;
		default:				//esqueleto normal
			mapa->AdObjSimpPos(OBJ_Esqueleto, x, y, z);
		return;
	}
}

double cJogador::PodDanoGolpe()
{
	//HARDCODE = newbie!!!
	switch (Poder) {
		case pPredador:		return 30;
		case pTarrasque:	return 250;
		case pBalacau:		return 30;
		case pAlienPequeno:	return 10;
		case pAlienGrande:	return 30;
		case pDragao:		return 40;
		case pT_1000:		return 30;
		case pTanque:		return 30;
		default:			return 0;
	}
}
short cJogador::PodSomGolpe()
{
	switch (Poder) {
		case pPredador:
		case pT_1000:		return SOM_ESPADA;
		case pTarrasque:	return SOM_TARRASQ;
		case pBalacau:
		case pAlienPequeno:
		case pAlienGrande:
		case pDragao:		return SOM_MORDIDA;
		case pTanque:		return SOM_SOCO;
		default:			return 0;
	}
}

double cJogador::PodRadar(bool &soradar)
{
	soradar = false;
	if (Poder == pDemolidor) {
		soradar = true;
		return 12.5;//10.5;
	} else if (Poder == pJedi) {
		return 4.5;//3.5;
	} else if (Poder == pRobocop) {
		return 5.5;//4.5;
	} else if (Poder == pZerg && Posicao == posEnterrado) {
		soradar = true;
		return 3.5;//2.5;
	} else {
		return 0;
	}

}

bool cJogador::PodVeAtraves(cCont cont, cChao chao, bool ehchao, bool &passaradar)
{
	passaradar = true;
	if (ehchao) {
		if (chao.EhChaoTransparente()) {
			return true;
		} else {
			return (Poder == pHomem_do_Raio_X);
		}
	} else {
		if (cont.EhParedeMadeira() ||
			cont.EhParedeTijolo()) {
			return (Poder == pHomem_do_Raio_X);
		} else if (cont.EhEscuro()) {
			return (Poder == pHomem_do_Raio_X || Poder == pPredador || GetFlag(TemIR));
		} else if (cont.EhParedePedra()) {
			passaradar = false;
			return false;
		} else {
			return true;
		}
	}	
}