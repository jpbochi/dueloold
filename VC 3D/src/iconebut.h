
#ifndef ICONEBUT_H
#define ICONEBUT_H

#include "fx.h"

enum {
	icoVazio = 0,
	icoDesconhecido,
	//Armas
	icoPistola,			icoPistolaMun,
	icoEspingarda,		icoEspingardaMun,
	icoMetralhadora,	icoMetralhadoraMun,
	icoLancaFog,		icoLancaFogMun,
	icoRifle,			icoRifleMun,
	icoDegen,			icoDegenMun,
	icoDisco,			icoDiscoMun,
	icoTriLaser,		icoTriLaserMun,
	icoMiniGun,			icoMiniGunMun,
	icoLancaChamas,		icoLancaChamasMun,
	icoRicochet,		icoRicochetMun,
	icoRoboArma,		icoRoboArmaMun,
	icoParalisante,		icoParalisanteMun,
	icoBaforada,		icoBaforadaMun,
	icoTeletrans,		icoTeletransMun,
	icoBlaster,			icoBlasterMun,
	icoMachado,
	icoEspinhos,		icoEspinhosMun,
	icoMG34,			icoMG34Mun,
	icoCanhao,			icoCanhaoMunHE,		icoCanhaoMunAP,
	icoBolaDeFogo,		icoBolaDeFogoMun,
	icoSabreDeLuz,
	icoShuriken,		icoShurikenMun,
	icoKatana,
	icoSoco,
	icoTrabuco,			icoTrabucoMun,
	icoDedoDuro,
	icoSnikt,
	//Equipamentos
	icoMina,			icoMinaAtiv,
	icoGranada,			icoGranadaAtiv,
	icoKitMedico,
	icoEsqueleto,
	icoCapaceteInv,
	icoCapaceteIR,
	icoBomba,
	icoColete,
	icoChave,
	icoHolografia,
	icoDetector,
	//Skills
	icoCamuflagem,
	icoKitDeArrombamento,
	icoEnterrarse,
	icoDesarmarMinas,
	icoControlePsionico,
	icoRegT1000,
	icoAutoDestruicao,
	icoIdentificacao,
	icoPararTempo,
	icoInvisibilidade,
	icoTeletransporte,
	icoImitar,
	icoMagiaHolografia,
	icoDardosMisticos,
	icoRezar,
	icoCura,
	icoAnimarMortos,
	icoRelampago,
	NUM_ICOS
};

class cJogo;
class cObjeto;
class cListaObjetos;
class cListaPosObjetos;

class cIconeBut{
private:
	void CarregaIcones(FXApp *);
	cJogo *pJogo;
	
public:
	//cIconeBut();
	cIconeBut(FXApp *, cJogo *);
	~cIconeBut();
	
	FXIcon *icones[NUM_ICOS];

	void PreencheBut(FXButton *, class cObjeto *);

	void EsvaziaMat(FXMatrix *);
	void PreencheMat(FXMatrix *, cListaObjetos *);
	
};

#endif