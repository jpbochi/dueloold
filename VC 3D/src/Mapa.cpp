
//#include <windows.h>
//#include <gl\gl.h>
//#include <gl\glu.h>
#include "fx.h"
#include "fx3d.h"
#include <gl\glaux.h>
#include <math.h>
//#include <string>
#include "geral.h"
#include "funcoes.h"
#include "conteudo.h"
#include "modelosgl.h"
#include "objetos.h"

#include "mapa.h"

extern FXuint gllist[GLList::NUM_LISTS];
extern FXuint textures[MAX_TEXTURAS];

#define DEFAULT_ALFA 75

//TO DO: void cMapa::AplicaNewton(); //gravidade aos objetos e �s paredes

cMapa::cMapa(cMapa *mapa, char c)
{
	Cria(mapa->dimX, mapa->dimY, mapa->dimZ, c, mapa->afin, mapa->afinchao);
}

cMapa::cMapa(ushort x, ushort y, ushort z, char c, cAfin *_afin, cAfin *_afinchao)
{
	Cria(x, y, z, c, _afin, _afinchao);
}

void cMapa::Cria(ushort x, ushort y, ushort z, char c, cAfin *_afin, cAfin *_afinchao)
{
	cont = NULL;
	resumoobjetos = NULL;
	lencont = 0;
	contchao = NULL;
	lencontchao = 0;
	afin = NULL;
	afinchao = NULL;

	Objs = NULL;

	marca = false;

	FiltroDeAndares = 0;
	
	MostraParedes = true;

    Cam.teta = 0.25f * PI;
    Cam.phi = 0.75f * PI;
	Cam.SetPos(-5 * LARGURAHEX, -5, 0.2f * (x + y));

	Redimenciona(x, y, z, c);
	
	//copia afinamento
	if (_afin != NULL)
		for (int i=0; i<lencont; i++) afin[i] = _afin[i];
	if (_afinchao != NULL)
		for (int i=0; i<lencontchao; i++) afinchao[i] = _afinchao[i];
}

cMapa::~cMapa()
{
	//if (cont != NULL)			{delete [lencont] cont;			cont = NULL;}
	if (resumoobjetos != NULL)	{delete [lencont] resumoobjetos;resumoobjetos = NULL;}
	if (afin != NULL)			{delete [lencont] afin;			afin = NULL;}
	if (contchao != NULL)		{delete [lencontchao] contchao;	contchao = NULL;}
	lencont = 0;
	lencontchao = 0;
}

void cMapa::Redimenciona(ushort x, ushort y, ushort z, char c)
{
	//BUG: os deletes abaixo d�o erro, mas s� �S VEZES
	//if (cont != NULL)			{delete [lencont] cont;			cont = NULL;}
	if (resumoobjetos != NULL)	{delete [lencont] resumoobjetos;resumoobjetos = NULL;}
	if (afin != NULL)			{delete [lencont] afin;			afin = NULL;}
	
	if (contchao != NULL)		{delete [lencontchao] contchao;	contchao = NULL;}
	if (afinchao != NULL)		{delete [lencontchao] afinchao;	afinchao = NULL;}

	dimX = x;
	dimY = y;
	dimZ = z;

	lencont = dimX * dimY * dimZ;
	cont = new cCont[lencont];
	resumoobjetos = new cObjetoSimples[lencont];
	afin = new cAfin[lencont];

	lencontchao = dimX * dimY * (dimZ - 1);
	if (dimZ) contchao = new cChao[lencontchao];
	if (dimZ) afinchao = new cAfin[lencontchao];
	
	Objs = new cObjetos(lencont / 30);

	Esvazia(c);
	EsvaziaObjetos();

	selectedZ = dimZ - 1;

	Cam.AtualizaMatrix();
}

void cMapa::Esvazia(char c)
{
	int i;
	for (i=0; i<lencont; i++) cont[i] = c;
	for (i=0; i<lencontchao; i++) contchao[i] = c;
}

void cMapa::EsvaziaObjetos()
{
	int i;
	for (i=0; i<lencont; i++) resumoobjetos[i].cod = 0;
	//TO DO: esvaziar o 'Objs'
}

double cMapa::AlturaSombra(double x, double y, double z)
{
	int xi, yi;
	PontoToHex2D(x, y, xi, yi);
	return AlturaSombra(xi, yi, z);
}

double cMapa::AlturaSombra(int x, int y, double z)
{
	bool chao, desce;
	ushort andar_sombra = AlturaToAndar(z, chao);
	
	if (andar_sombra < 0
	||  andar_sombra >= dimZ) {
		return AndarToAltura(andar_sombra, 0);
	}
	
	do {
		desce = false;
		cCont cont     = Cont(x, y, andar_sombra, cMapa::NAO_CHAO);
		cChao contchao = Cont(x, y, andar_sombra, cMapa::CHAO);
		
		if ((!chao && cont.EhParede())
		||  (chao && contchao.EhChaoDuro())) {
			return AndarToAltura(andar_sombra, 0);
		} else if (contchao.EhChaoBuraco()) {
			desce = true;
			chao = false;
			andar_sombra--;
			if (andar_sombra < 0) return 0; //OBS.: isto n�o deve acontecer pq o andar zero n�o pode ter um ch�o buraco
		}
	} while (desce);
	
	return AndarToAltura(andar_sombra, 0);
}

uint cMapa::VetPos(ushort x, ushort y, ushort z)
{
	#define YXZ

	#if defined(XYZ)
		return x + (y * dimX) + (z * dimX * dimY);
	#elif defined(YXZ)
		#define DZ					 dimY * dimX
		return y + (x * dimY) + (z * DZ);
	#elif defined(ZYX)
		return z + (y * dimZ) + (x * dimZ * dimY);
	#endif
}

void cMapa::VetPosInv(uint pos, ushort &x, ushort &y, ushort &z)
{
	#if defined(XYZ)
		x = pos % dimX;
		pos = (pos - x) / dimX;
		y = pos % dimY;
		pos = (pos - y) / dimY;
		z = pos;
	#elif defined(YXZ)
		y = pos % dimY;
		pos = (pos - y) / dimY;
		x = pos % dimX;
		pos = (pos - x) / dimX;
		z = pos;
	#endif
}

cAfin cMapa::Afin(ushort x, ushort y, ushort z, bool chao)
{
	//testa se saiu do mapa
	if ((x<0)     || (y<0)     || (z<0)
	||	(x>=dimX) || (y>=dimY) || (z>=dimZ)) {
		return cAfin(cAfin::AFIN_MASK_DEFAULT);
	}
	if (chao) {
		uint i = VetPos(x, y, z - 1);
		if ((i >= 0) && (i < lencontchao)) {
			return afinchao[i];
		} else return cAfin(cAfin::AFIN_MASK_DEFAULT); //subscript out of range! Isto N�O deve acontecer desde o teste se saiu do mapa
	} else {
		uint i = VetPos(x, y, z);
		if ((i >= 0) && (i < lencont)) {
			return afin[i];
		} else return cAfin(cAfin::AFIN_MASK_DEFAULT); //subscript out of range! Isto N�O deve acontecer desde o teste se saiu do mapa
	}
}

cCont cMapa::Cont(ushort x, ushort y, ushort z)
{
	//testa se saiu do mapa
	if ((x<0)     || (y<0)     || (z<0)
	||	(x>=dimX) || (y>=dimY) || (z>=dimZ)) {
		return cCont(cCont::CONT_FORA_DO_MAPA);
	}
	uint i = VetPos(x, y, z);
	if ((i >= 0) && (i < lencont)) {
		return cont[i];
	} else {
		//subscript out of range! Isto N�O deve acontecer desde o teste se saiu do mapa
		return cCont(cCont::CONT_FORA_DO_MAPA);
	}
}

cChao cMapa::Chao(ushort x, ushort y, ushort z)
{
	//testa se saiu do mapa
	if ((x<0)     || (y<0)     || (z<0)
	||	(x>=dimX) || (y>=dimY) || (z>=dimZ)) {
		return cChao(cChao::CHAO_FORA_DO_MAPA);
	}
	uint i = VetPos(x, y, z - 1);
	if ((i >= 0) && (i < lencont)) {
		return contchao[i];
	} else {
		//subscript out of range! Isto N�O deve acontecer desde o teste se saiu do mapa
		return cChao(cChao::CHAO_FORA_DO_MAPA);
	}
}

char cMapa::Cont(ushort x, ushort y, ushort z, bool chao)
{
	if ((x<0)     || (y<0)     || (z<0)
	||	(x>=dimX) || (y>=dimY) || (z>=dimZ) || (z == 0 && chao)) {
		return cCont::CONT_FORA_DO_MAPA;
	}
	if (chao) {
		uint i = VetPos(x, y, z - 1);
		if ((i >= 0) && (i < lencontchao)) {
			return contchao[i].c;
		} else return cCont::CONT_FORA_DO_MAPA; //subscript out of range! Isto N�O deve acontecer desde o teste se saiu do mapa
	} else {
		uint i = VetPos(x, y, z);
		if ((i >= 0) && (i < lencont)) {
			return cont[i].c;
		} else return cCont::CONT_FORA_DO_MAPA; //subscript out of range! Isto N�O deve acontecer desde o teste se saiu do mapa
	}
}

bool cMapa::SetCont(ushort x, ushort y, ushort z, cCont val)
{
	uint i = VetPos(x, y, z);
	if ((i >= 0) && (i < lencont)) {
		cont[i] = val;
		return true;
	} else return false; //subscript out of range!
}

bool cMapa::SetChao(ushort x, ushort y, ushort z, cChao val)
{
	uint i = VetPos(x, y, z - 1);
	if ((i >= 0) && (i < lencontchao)) {
		contchao[i] = val;
		return true;
	} else return false; //subscript out of range!
}

char cMapa::GetResumoObj(ushort x, ushort y, ushort z)
{
	uint i = VetPos(x, y, z);
	if ((i >= 0) && (i < lencont)) {
		return resumoobjetos[i].cod;
	} else return -1; //subscript out of range!
}

bool cMapa::SetResumoObj(ushort x, ushort y, ushort z, char val)
{
	uint i = VetPos(x, y, z);
	if ((i >= 0) && (i < lencont)) {
		resumoobjetos[i].cod = val;
		return true;
	} else return false; //subscript out of range!
}

cListaPosObjetos* cMapa::GetListaPos(ushort x, ushort y, ushort z)
{
	uint pos = VetPos(x, y, z);
	return Objs->GetListaPos(pos, x, y, z);
}

unsigned char cMapa::AtualizaResumoObjsPos(ushort x, ushort y, ushort z)
{
	return AtualizaResumoObjs(VetPos(x, y, z), x, y, z);
}
unsigned char cMapa::AtualizaResumoObjs(uint vetpos, ushort x, ushort y, ushort z)
{
	cListaPosObjetos* listapos = Objs->GetListaPos(vetpos, x, y, z);
	if (listapos != NULL) {
		resumoobjetos[vetpos].cod = listapos->GetResumo();
	} else {
		resumoobjetos[vetpos].cod = 0;
	}
	return resumoobjetos[vetpos].cod;
}

//TO DO: aplicar Newton aos objetos adicionados
void cMapa::AdObjPos(cObjeto &obj, ushort x, ushort y, ushort z)
{
	if (Objs != NULL) {
		uint vetpos = VetPos(x, y, z);
		Objs->AdObjPos(obj, vetpos, x, y, z);
		AtualizaResumoObjs(vetpos, x, y, z);
	}
}
void cMapa::AdObjSimpPos(cObjetoSimples obj, ushort x, ushort y, ushort z)
{
	if (Objs != NULL) {
		uint vetpos = VetPos(x, y, z);
		Objs->AdObjSimpPos(obj, vetpos, x, y, z);
		AtualizaResumoObjs(vetpos, x, y, z);
	}
}

bool cMapa::CarregaMapa(const char* strmapa)
{
    FILE *fmap;
    char a, b, c;
	char format[10];

    //abre arquivo
	if((fmap = fopen(strmapa,"r")) == NULL) {return false;}          //erro: arquivo n�o pode ser aberto

	//l� cabe�alho
	if ((fscanf(fmap, "%c%c%c", &a, &b, &c)) == EOF) {return false;} //erro: EOF inesperado
	Redimenciona(b-48, c-48, a-47);

	//l� conte�do do mapa
	sprintf(format, "%%%ds", lencont);
	if ((fscanf(fmap, format, cont)) == EOF) {return false;} //erro: EOF inesperado
	for(int i=0; i<lencont; i++) {
		cont[i].c -= 48;
	}

	ConverteParaJogo();

	//------ convers�o para o novo formato
	//1� etapa: troca todos os 'conte�dos'
	for (i=0; i<lencont; i++) {
		cCont novocont;
		char chao;
		bool converteu = cont[i].ConverteConteudo(novocont, chao);

		if (!converteu) {
			ushort x, y, z;
			VetPosInv(i, x, y, z);
			resumoobjetos[i].cod = cont[i].c;
			Objs->AdObjSimpPos(resumoobjetos[i], i, x, y, z);
		} else if (novocont.EhTeletransporte()) {
		//BUG: este else parece obsoleto ou sua raz�o � desconhecida
			resumoobjetos[i].cod = novocont.ConteudoToTexID();
		}

		//substitui o valor do conteudo
		cont[i] = novocont;
		//se j� passou do 1� andar p�e o chao
		if (i >= DZ) contchao[i - DZ] = chao;
	}
	//2� etapa: tapa os buracos em cima das paredes
	for (short z=1; z<dimZ; z++)
	for (short y=0; y<dimY; y++)
	for (short x=0; x<dimX; x++) {
		cChao conteudochao =	Cont(x, y, z, CHAO);
		cCont conteudoabaixo =	Cont(x, y, z-1, NAO_CHAO);
		if (conteudochao.EhChaoBuraco() && conteudoabaixo.EhParedeSolida()) {
			SetCont(x, y, z, cCont::CONT_PAREDE_NULA);
			SetChao(x, y, z, conteudoabaixo.TipoParedeToTipoChao());
		}
	}
	//------ fim da convers�o
	
	//calcula o mapa de afinamento das paredes
	RecalculaAfinamento();

	//fecha arquivo
	fclose(fmap);
	return true;
}

void cMapa::ConverteParaEditor()
{
	for(int i=0; i<lencont; i++)
	{
		switch (cont[i].c) {
			case 9:  cont[i] = 10; break; //passagem secreta
			case 25: cont[i] = 24; break; //buraco
			case 6:  cont[i] = 33; break; //mina ativada
			//obs.: n�o � poss�vel reverter buraco% e campo minado
			default: break;
		}
	}
}

void cMapa::ConverteParaJogo()
{
	for(int i=0; i<lencont; i++)
	{
		switch (cont[i].c) {
			case 10: cont[i] = 9;  break; //passagem secreta
			case 24: cont[i] = 25; break; //buraco
			case 33: cont[i] = 6;  break; //mina ativada
			case 43: 					  //buraco %
				cont[i] = (rnd() < 0.2f) ? 25 : 0;
				break;
			case 54: 					  //campo minado
				cont[i] = (rnd() < 0.2f) ? 6 : 0;
				break;
			default: break;
		}
	}
}

void cMapa::MudaAndar(short delta)
{
	selectedZ = (selectedZ + delta < 0) ? 0 : selectedZ + delta;
	if (selectedZ >= dimZ) selectedZ = dimZ - 1;
}

void cMapa::RotCameraHorizontal(double rot)
{
	Cam.RotHorizontal(rot);
}
void cMapa::RotCameraVertical(double rot)
{
	Cam.RotVertical(rot);
}
void cMapa::MoveXYCamera(double d)
{
	Cam.MoveXY(d);
}
void cMapa::MoveXYZCamera(double d)
{
	Cam.MoveXYZ(d);
}
void cMapa::StrafeHorCamera(double d)
{
	Cam.StrafeHorizontal(d);
}
void cMapa::StrafeVertCamera(double d)
{
	Cam.StrafeVertical(d);
}

void cMapa::CentraCam(int x1, int y1, int z1,
					  int x2, int y2, int z2)
{
	double fx1, fy1, fz1, fx2, fy2, fz2;

	bool temponto2 = false;
	if (x2==-1){x2 = dimX/2; temponto2 = true;}
	if (y2==-1) y2 = dimY/2;
	if (z2==-1) z2 = 0;

	selectedZ = z1;

	if ((x1==x2) && (y1==y2)) {
		x2+=1;
		y2+=1;
	}

	CentroHex3D(x1, y1, z1, fx1, fy1, fz1, 0.5);
	CentroHex3D(x2, y2, z2, fx2, fy2, fz2, 0.5);

	fx2 += 0.01f;//fz2 -= 20.0;

	if (!temponto2) {
		Cam.SetPos(fx1, fy1, fz1 + 3, false);
		Cam.SetLookTo(fx2, fy2, fz2, false);
		Cam.MoveXYZ(-4);
	} else {
		Cam.SetPos(fx1, fy1, fz1, false);
		Cam.SetLookTo(fx2, fy2, fz2, false);
		Cam.phi = 0.82 * PI;//0.75 * PI;
		Cam.AtualizaVetores();
		Cam.MoveXYZ(-7);
	}
}

void cMapa::RecalculaAfinamento(ushort x, ushort y, ushort z)
{
	//### Ch�o
	cAfin afinamentochao(cAfin::AFIN_MASK_DEFAULT);
	if (z > 0) {
		int pos = VetPos(x, y, z - 1);
		cChao chao = contchao[pos];

		if (!chao.EhChaoBuraco()) {
			/*
			|     0
			| 5  ___  1
			|   /   \
			|   \___/
			| 4       2
			|     3
			*/
			//TO DO: falta o caso (0-2-3 OU 0-1-2-3)
			cChao cvizinho[6];
			short h = (x % 2);
			cvizinho[0] = Chao(    x, y + 1,		z);
			cvizinho[3] = Chao(    x, y - 1,		z);

			cvizinho[1] = Chao(x + 1, y + h,		z);
			cvizinho[2] = Chao(x + 1, y - 1 + h,	z);

			cvizinho[5] = Chao(x - 1, y + h,		z);
			cvizinho[4] = Chao(x - 1, y - 1 + h,	z);

			bool vizinho[6];
			int contvizinhos = 0;
			for (int i=0; i<6; i++) {
				if (cvizinho[i].EhForaDoMapa()) {
					vizinho[i] = 0;
				} else {
					vizinho[i] = !cvizinho[i].EhChaoBuraco(); //TO DO: testar se � do mesmo tipo de parede
				}
				if (vizinho[i]) contvizinhos++;
			}
			
			afinchao[pos] = cAfin::CriaPelosVizinhos(contvizinhos, vizinho);
			afinamentochao = afinchao[pos];
		} else {
			afinchao[pos] = cAfin::AFIN_MASK_DEFAULT;
		}
	}

	//### Cont
	int pos = VetPos(x, y, z);
	cCont conteudo = cont[pos];

	if (conteudo.EhParedeSolida()) {
		if (!afinamentochao.EhAfinInteira()) {
			//caso o ch�o seja afinado, a parede de cima herda
			afin[pos] = afinamentochao;
		} else {
			/*
			|     0
			| 5  ___  1
			|   /   \
			|   \___/
			| 4       2
			|     3
			*/
			//TO DO: falta o caso (0-2-3 OU 0-1-2-3)
			cCont cvizinho[6];
			short h = (x % 2);
			cvizinho[0] = Cont(    x, y + 1,		z);
			cvizinho[3] = Cont(    x, y - 1,		z);

			cvizinho[1] = Cont(x + 1, y + h,		z);
			cvizinho[2] = Cont(x + 1, y - 1 + h,	z);

			cvizinho[5] = Cont(x - 1, y + h,		z);
			cvizinho[4] = Cont(x - 1, y - 1 + h,	z);

			bool vizinho[6];
			int contvizinhos = 0;
			for (int i=0; i<6; i++) {
				if (cvizinho[i].EhForaDoMapa()) {
					vizinho[i] = 0;
				} else {
					vizinho[i] = cvizinho[i].EhParedeSolida(); //TO DO: testar se � do mesmo tipo de parede	
				}
				if (vizinho[i]) contvizinhos++;
			}
			
			afin[pos] = cAfin::CriaPelosVizinhos(contvizinhos, vizinho);
		}
	} else {
		afin[pos] = cAfin::AFIN_MASK_DEFAULT;
	}
}

void cMapa::RecalculaAfinamento()
{
	for (short z=0; z<dimZ; z++)
	for (short y=0; y<dimY; y++)
	for (short x=0; x<dimX; x++) {
		RecalculaAfinamento(x, y, z);
	}
}

bool cMapa::Renderiza(bool selection, bool comburacos)
{
	bool ret = true;
	ret &= RenderizaInicio(selection, comburacos);
	ret &= RenderizaMeio(selection, NULL, comburacos);
	ret &= RenderizaFim(selection, comburacos);
	return ret;
}

bool cMapa::RenderizaInicio(bool selection, bool comburacos)
{
	glGetFloatv(GL_LINE_WIDTH, &linewidth);
	glLineWidth(1);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	//if (!selection) glLoadIdentity();
	//gluPerspective(100, 1.3333, 0.1f, 50);

	//glFrustum(-asp, asp, -1, 1, 2.8, 100.0);
	//glFrustum(-1, 1, -1, 1, 2.8, 100.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	//glLoadMatrixf(&cam_matrix[0][0]);
	Cam.Look();

	return true;
}

void RendAfinamento(cAfin afinamento, int &gllista_hex, int &gllista_lado, bool ehchao = false)
{
	if (ehchao) {
		if (afinamento.EhAfinInteira()) {
			gllista_hex = gllist[GLList::HEXAGONO];
			gllista_lado = gllist[GLList::PILAR_CHAO];
		} else if (afinamento.EhAfinAtraves()) {
			gllista_hex = gllist[GLList::HEX_ATRAVES];
			gllista_lado = gllist[GLList::CHAO_ATRAVES];
			glRotatef(afinamento.AfinRotacao(), 0, 0, -1);
		} else if (afinamento.EhAfinMetade()) {
			gllista_hex = gllist[GLList::HEX_METADE];
			gllista_lado = gllist[GLList::CHAO_METADE];
			glRotatef(afinamento.AfinRotacao(), 0, 0, -1);
		}
	} else {
		if (afinamento.EhAfinInteira()) {
			gllista_hex = gllist[GLList::HEXAGONO];
			gllista_lado = gllist[GLList::PILAR];
		} else if (afinamento.EhAfinAtraves()) {
			gllista_hex = gllist[GLList::HEX_ATRAVES];
			gllista_lado = gllist[GLList::PAREDE_ATRAVES];
			glRotatef(afinamento.AfinRotacao(), 0, 0, -1);
		} else if (afinamento.EhAfinMetade()) {
			gllista_hex = gllist[GLList::HEX_METADE];
			gllista_lado = gllist[GLList::PAREDE_METADE];
			glRotatef(afinamento.AfinRotacao(), 0, 0, -1);
		}
	}
}

void RendParede(bool MostraParedes, cAfin afinamento, int textura, int texturachao = -1)
{
	int gllista_hex, gllista_lado;
	glPushMatrix();
	RendAfinamento(afinamento, gllista_hex, gllista_lado);

	if (MostraParedes) {
		/*if (afinamento.EhAfinInteira()) {
			glCallList(gllist[GLList::PILAR]);
			//teto
			glTranslatef(0, 0, ALTURAPAREDE);
			glCallList(gllist[GLList::HEXAGONO]);
		} else if (afinamento.EhAfinAtraves()) {
			glRotatef(afinamento.AfinRotacao(), 0, 0, -1);
			glCallList(gllist[GLList::PAREDE_ATRAVES]);
			//teto
			glTranslatef(0, 0, ALTURAPAREDE);
			glCallList(gllist[GLList::HEX_ATRAVES]);
		} else if (afinamento.EhAfinMetade()) {
			glRotatef(afinamento.AfinRotacao(), 0, 0, -1);
			glCallList(gllist[GLList::PAREDE_METADE]);
			//teto
			glTranslatef(0, 0, ALTURAPAREDE);
			glCallList(gllist[GLList::HEX_METADE]);
		}*/
		
		glCallList(gllista_lado);
		glTranslatef(0, 0, ALTURAPAREDE);
		glCallList(gllista_hex);
	} else if (texturachao != -1) {
		//ch�o
		glTranslatef(0, 0, 0.001f); //epsilon
		glCallList(gllista_hex);
	}
	glPopMatrix();
}

void RendChao(bool blnMostraTopo, cChao conteudochao, cAfin afinamento)
{
	int textura;
	int gllista_hex, gllista_lado;
	glPushMatrix();
	RendAfinamento(afinamento, gllista_hex, gllista_lado, true);

	//parte superior
	if (blnMostraTopo) {
		textura = conteudochao.ChaoCimaToTexID();
		glBindTexture(GL_TEXTURE_2D, textures[textura]);
		glCallList(gllista_hex);
	}

	//lados
	textura = conteudochao.ChaoLadoToTexID();
	glBindTexture(GL_TEXTURE_2D, textures[textura]);
	glCallList(gllista_lado);

	//parte inferior
	glRotatef(180, 1, 0, 0);
	glRotatef(180, 0, 0, -1);
	textura = conteudochao.ChaoCimaToTexID();
	glBindTexture(GL_TEXTURE_2D, textures[textura]);
	glCallList(gllista_hex);
	
	glPopMatrix();
}

void RendChaoTopo(cChao conteudochao, cAfin afinamento, char cor)
{
	//desenha ch�o
	int gllista_hex, gllista_lado;
	glPushMatrix();
	RendAfinamento(afinamento, gllista_hex, gllista_lado, true);

	if (conteudochao.EhChaoTransparente()) glColor4ub(cor, cor, cor, DEFAULT_ALFA);
	int texturachao = conteudochao.ChaoCimaToTexID();
	glBindTexture(GL_TEXTURE_2D, textures[texturachao]);
	glCallList(gllista_hex);
	
	glPopMatrix();
}

void RendObjetosNoChao(cObjetoSimples obj)
{
	if (obj.cod != 0) {
		//desenha objetos no ch�o
		glPushMatrix();
		glDisable(GL_CULL_FACE);

		glTranslatef(0, 0, 0.001f); //epsilon
		glScalef(.99f, .99f, .99f);
		glBindTexture(GL_TEXTURE_2D, textures[obj.cod]);
		glCallList(gllist[GLList::HEXAGONO]);
		
		glEnable(GL_CULL_FACE);
		glPopMatrix();
	}
}

bool cMapa::RenderizaMeio(bool selection, cMapa* Mask, bool comburacos)
{
	int contpos, contposchao;
	cCont conteudo;
	cChao conteudochao, conteudochao_acima;
	cAfin afinamento, afinamentochao;
	double xh, yh, zh;
	bool desenhaburacos;

	/*
	const GLfloat mat[2][2][4] = {{{.9f, .9f, .9f, 1}, {.9f, .9f, .9f, .3f}},
								  {{.6f, .6f, .6f, 1}, {.6f, .6f, .6f, .3f}}};
	const int MAT_OPACO = 0, MAT_TRANSP = 1;
	/*/
	const GLfloat mat[] = {1, 1, 1, 1};
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat);
	//*/

	if (selection) glDisable(GL_LIGHTING);

	desenhaburacos = comburacos && selection; //desenha se for um teste de click
	
	ushort x, y, z;
	ushort iniz, fimz;
	switch (FiltroDeAndares) {
		case 0:
			iniz = 0; fimz = selectedZ + 1;
		break;
		case 1:
			iniz = 0; fimz = dimZ;
			desenhaburacos = false; //n�o desenha buracos, mesmo se for um teste de click
		break;
		case 2:
			iniz = selectedZ; fimz = selectedZ + 1;
		break;
	}

	//*/ //desenha o mapa em certa ordem para que a transpar�ncia funcione (pelo menos engana)
	int i;
	ushort *pc[3] = {&x, &y, &z};
	ushort dc[3] =  {1, 1, 1};
	ushort ini[3] = {0, 0, iniz};
	ushort lim[3]=  {dimX, dimY, fimz};
	//double fd[3] = {cam_tox - cam_atx, cam_toy - cam_aty, cam_toz - cam_atz};
	double fd[3] = {Cam.n[0], Cam.n[1], Cam.n[2]};
	
	for (i=0; i<3; i++)
	{
		if (fd[i]>0) {
			ini[i] = lim[i]-1; dc[i] = -1;
		} else {
			fd[i] = -fd[i];
		}
	}

	//ordena
	ushort aux;
	double faux;
	ushort *auxp;
	#define TrocaTudo(A, B) Troca(pc[A], pc[B], auxp); Troca(dc[A], dc[B], aux); Troca(lim[A], lim[B], aux); Troca(ini[A], ini[B], aux);Troca(fd[A], fd[B], faux)

	if (fd[0] > fd[1]) {TrocaTudo(0, 1);}
	if (fd[1] > fd[2]) {TrocaTudo(1, 2);}
	if (fd[0] > fd[1]) {TrocaTudo(0, 1);}

	#undef TrocaTudo

	for (int transp=0; transp<=1; transp++)
	for (*pc[2]=ini[2]; *pc[2]<lim[2]; *pc[2]+=dc[2])
	for (*pc[1]=ini[1]; *pc[1]<lim[1]; *pc[1]+=dc[1])
	for (*pc[0]=ini[0]; *pc[0]<lim[0]; *pc[0]+=dc[0])
	/*/
	for (z=0; z<showZ; z++)
	for (y=0; y<dimY; y++)
	for (x=0; x<dimX; x++)
	/**/
	{
		contpos = VetPos(x, y, z);
		conteudo = cont[contpos];
		afinamento = afin[contpos];
		
		if (z == dimZ - 1) {
			conteudochao_acima = 0;
			conteudochao_acima.SetChaoBuraco();
		} else {
			conteudochao_acima = Cont(x, y, z+1, CHAO);
		}

		if (selection) glLoadName(contpos);

		bool posicionado = false;
		bool desenhaparede = true;

		//TO DO: a ordem de renderiza��o [ch�o|conteudo] deve depender de dz

		//TO DO: criar gllistas separadas para desenhar o ch�o e as paredes de alturas diferentes do m�ximo

		//############################################ Ch�o
		if (z == 0) {
			conteudochao.SetChaoPedra();
			afinamentochao = cAfin::AFIN_MASK_DEFAULT;
		} else {
			contposchao = VetPos(x, y, z - 1);
			conteudochao = contchao[contposchao];
			afinamentochao = afinchao[contposchao];

			if ((!conteudochao.EhFogNuncaViu()) //fog
			&& (!conteudochao.EhChaoBuraco() || desenhaburacos)) { //buraco
				glPushMatrix();
				CentroHex3D(x,y,z, xh,yh,zh, 0);
				glTranslated(xh, yh, zh);
				posicionado = true;

				//escurece lugares fora de vista de acordo com Mask(FogBit)
				char cor = (char)255;
				if (Mask != NULL) {
					if (Mask->contchao[contposchao] == 0) {
						cor = (char)175;
					}
				}
				
				//escurece quando h� um teto acima do lugar
				if (!conteudochao_acima.EhChaoTransparente()) cor -= 50;

				//transpar�ncia
				int desenhar;
				if (conteudochao.EhChaoTransparente()) {
					glColor4ub(cor, cor, cor, DEFAULT_ALFA);
					desenhar = transp;
				} else {
					glColor3ub(cor, cor, cor);
					desenhar = !transp;
				}

				//desenha chao entre andares
				if (desenhar) {
					if (selection) glDisable(GL_TEXTURE_2D);
					else		   glEnable(GL_TEXTURE_2D);

					RendChao(conteudo.EhFogNuncaViu(), conteudochao, afinamentochao);
				}
			} else {
				desenhaparede = false;
			}
		}

		//############################################ Parede
		if ((!conteudo.EhFogNuncaViu()) //fog
		//&& ((conteudo != 26) || desenhaburacos)) { //buraco
		&& desenhaparede) {
			if (!posicionado) {
				glPushMatrix();
				CentroHex3D(x,y,z, xh,yh,zh, 0);
				glTranslated(xh, yh, zh);
				posicionado = true;
			}

			//TO DO: a parede em que um cara estiver deve ficar transparente. ex.: H. do Raio-x ve um zerg enterrado ou fantasma numa parede
			/*
			//remapeia conte�do (TO DO: o Raio-X ve certas coisas que outros n�o v�em)
			if (conteudo == 9) {			//9 = passagem secreta
				conteudo = 5;
			} else if (conteudo == 24) {	//24 = buraco inv.
				conteudo = 0;
			} else if (conteudo == 33) {	//33 = mina ativada
				conteudo = 0;
			}else if (conteudo == 50) {		//50 = porta aberta
				conteudo = 0; 
			} //43 (buraco%) e 54 (mina%) n�o deveriam existir durante o jogo
			*/

			//escurece lugares fora de vista de acordo com Mask(FogBit)
			char cor = (char)255;
			if (Mask != NULL) {
				if (Mask->cont[contpos] == 0) {
					cor = (char)175;
				}
			}

			//escurece quando h� um teto acima do lugar
			if (!conteudochao_acima.EhChaoTransparente()) cor -= 50;

			//decide se � para usar textura e qual textura usar
			int textura;
			glEnable(GL_TEXTURE_2D);
			textura = conteudo.ConteudoToTexID();
			glBindTexture(GL_TEXTURE_2D, textures[textura]);
			glColor3ub(cor, cor, cor);
			//glMaterialfv(GL_FRONT, GL_AMBIENT, mat[mat_escuro][MAT_OPACO]);
			//glMaterialfv(GL_FRONT, GL_DIFFUSE, mat[mat_escuro][MAT_OPACO]);

			//rederiza o conte�do
			if (transp) {
				//s� as transparentes E s� quando n�o estiver fazendo sele��o
				if (!selection && conteudo.EhTransparente()) {
					if (conteudo.EhEsfumacado()) {
						glColor4ub(cor, cor, cor, DEFAULT_ALFA);
						//glMaterialfv(GL_FRONT, GL_AMBIENT, mat[mat_escuro][MAT_TRANSP]);
						//glMaterialfv(GL_FRONT, GL_DIFFUSE, mat[mat_escuro][MAT_TRANSP]);
						glCallList(gllist[GLList::PIRAMIDE]);
					} else {
						if (conteudo.EhGrade()) { //a grade � transparente, mas tem canal alfa
							glColor4ub(cor, cor, cor, 255);
							//glMaterialfv(GL_FRONT, GL_AMBIENT, mat[mat_escuro][MAT_OPACO]);
							//glMaterialfv(GL_FRONT, GL_DIFFUSE, mat[mat_escuro][MAT_OPACO]);
						} else {
							glColor4ub(cor, cor, cor, DEFAULT_ALFA);
							//glMaterialfv(GL_FRONT, GL_AMBIENT, mat[mat_escuro][MAT_TRANSP]);
							//glMaterialfv(GL_FRONT, GL_DIFFUSE, mat[mat_escuro][MAT_TRANSP]);
						}

						//aten��o: as paredes tranparentes s�o desenhadas duas vezes
						//         (uma por dentro e outra por fora)
						//prepara o lado de dentro, que � mais estreito
						glPushMatrix();
						glScalef(0.99f, 0.99f, 0.99f);
						glTranslatef(0, 0, 0.01f);

						glFrontFace(GL_CW);
						for (int b=0; b<2; b++) {//desenha duas vezes (uma em CW e outra em CCW)
							RendParede(MostraParedes, afinamento, textura);
							if (!b) { //volta para o normal
								glPopMatrix();
								glFrontFace(GL_CCW);
							}
						}
					}
				}
			} else {
				if (conteudo.EhTransparente()) {	//� transparente -> s� desenha o ch�o
					RendChaoTopo(conteudochao, afinamentochao, cor);
					
					RendObjetosNoChao(resumoobjetos[contpos]);
				} else if (conteudo.EhLata()) {
					if (conteudochao.EhChaoTransparente()) glColor4ub(cor, cor, cor, DEFAULT_ALFA);
					int texturachao = conteudochao.ChaoCimaToTexID();
					glBindTexture(GL_TEXTURE_2D, textures[texturachao]);
					glCallList(gllist[GLList::HEXAGONO]);

					glBindTexture(GL_TEXTURE_2D, textures[textura]);
					glScalef(0.5, 0.5, 0.5);
					glCallList(gllist[GLList::PILAR]);
					glTranslatef(0, 0, ALTURAPAREDE);
					glCallList(gllist[GLList::HEXAGONO]);
				} else if (conteudo.EhParede()) {
					RendParede(MostraParedes, afinamento, textura, conteudochao.ChaoCimaToTexID());
					if (!afinamento.EhAfinInteira()) {
						RendChaoTopo(conteudochao, afinamentochao, cor);
					}
				} else {
					RendChaoTopo(conteudochao, afinamentochao, cor);
					
					RendObjetosNoChao(resumoobjetos[contpos]);
				}
			}
		}
		if (posicionado) glPopMatrix();
	}

	if (selection){
		glEnable(GL_LIGHTING);
	} else if (marca) {
		glDisable(GL_LIGHTING);
		glPushMatrix();

		CentroHex2D(marcax,marcay, xh,yh);
		glTranslated(xh, yh, 0);
		for(z=0; z<=marcaz; z++)
		{
			double d = Distancia3D(xh, yh, z*ALTURAANDAR, Cam.c[0], Cam.c[1], Cam.c[2]);
			GLfloat lw = (float)( 15. * pow(0.89, d) );
			glLineWidth(lw);
			
			if (z>0) {
				glPushMatrix();
				glColor4ub(255, 255, 0, 255);
				glTranslatef(0, 0, -LARGURACHAO);
				glScalef(1, 1, LARGURACHAO/ALTURAPAREDE);
				glCallList(gllist[GLList::MARCA]);
				glPopMatrix();
			}

			glColor4ub(175, 175, 0, 200);
			glCallList(gllist[GLList::MARCA]);

			glTranslatef(0, 0, ALTURAANDAR);
		}

		glPopMatrix();
		glEnable(GL_LIGHTING);
	}

	//glDisable(GL_COLOR_MATERIAL);
	
	return true;
}

bool cMapa::RenderizaFim(bool selection, bool comburacos)
{
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glLineWidth(linewidth);

	return true;
}
