
#include "linhas.h"
#include "funcoes.h"
#include "fx.h"
#include "fx3d.h"
#include "modelosgl.h"
extern uint gllist[GLList::NUM_LISTS];

#include "projetil.h"

cLinhaTiro::cLinhaTiro()
{
	//nada...
}

cLinhaTiro::cLinhaTiro(double _largura, double _x1, double _y1, double _z1, double _x2, double _y2, double _z2)
{
	largura = _largura;
	x1 = _x1;
	y1 = _y1;
	z1 = _z1;
	x2 = _x2;
	y2 = _y2;
	z2 = _z2;
}

cLinhaTiro & cLinhaTiro::operator = (const cLinhaTiro &linha)
{
	largura = linha.largura;
	x1 = linha.x1;
	y1 = linha.y1;
	z1 = linha.z1;
	x2 = linha.x2;
	y2 = linha.y2;
	z2 = linha.z2;
	return *this;
}

bool cLinhaTiro::operator == (const cLinhaTiro &linha)
{
	return (largura == linha.largura &&
			x1 == linha.x1 &&
			y1 == linha.y1 &&
			z1 == linha.z1 &&
			x2 == linha.x2 &&
			y2 == linha.y2 &&
			z2 == linha.z2);
}

bool cLinhaTiro::operator != (const cLinhaTiro &linha)
{
	return !((*this) == linha);
}

void cLinhaTiro::Inicia(double _largura, double _x1, double _y1, double _z1)
{
	largura = _largura;
	x1 = _x1;
	y1 = _y1;
	z1 = _z1;
}

void cLinhaTiro::Move(double _x2, double _y2, double _z2)
{
	x2 = _x2;
	y2 = _y2;
	z2 = _z2;
}

void cLinhaTiro::Renderiza(double distcor)
{
	glPushMatrix();
	if (distcor != 0) glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	//posiciona
	//glTranslatef(x1 + x2 / 2, y1 + y2 / 2, z1 + z2 / 2);
	glTranslated(x1, y1, z1);

	//rotaciona
	double teta = funAngTetaf(x1, y1, x2, y2);
	glRotated(-90 + _180_PI * teta, 0, 0, 1);
	double phi = funAngPhif(x1, y1, z1, x2, y2, z2);
	glRotated(-90 + _180_PI * phi, 1, 0, 0);

	//calcula comprimento
	double compr = Distancia3D(x1, y1, z1, x2, y2, z2);

	if (distcor != 0) {
		double w = 0;
		const double passo = 0.3;
		
		glScaled(largura, passo, largura);
		do {
			//ajusta a cor
			char R, G, B;
			CorDistancia(G, R, B, w / distcor);
			glColor3ub(R, G, B);

			//rederiza
			glCallList(gllist[GLList::CILINDRO]);
			
			//avan�a
			glTranslated(0, 1, 0);
			w += passo;
		} while (w < compr);
	} else {
		//escala obs.:esta escala n�o causa erro nas normais do cilindro
		glScaled(largura, compr, largura);

		//rederiza
		glCallList(gllist[GLList::CILINDRO]);
	}

	glEnable(GL_LIGHTING);
	glPopMatrix();
}

cLinhaSombra::cLinhaSombra()
{
	pMapa = 0;
	ult_linha = 0;
}

void cLinhaSombra::Inicia(double largura, double x1, double y1, double z1, cMapa *_pMapa)
{
	pMapa = _pMapa;
	ult_linha = 0;
	linhas[ult_linha].largura = largura;
	linhas[ult_linha].x1 = x1;
	linhas[ult_linha].y1 = y1;
	linhas[ult_linha].z1 = pMapa->AlturaSombra(x1, y1, z1);
	linhas[ult_linha].z2 = linhas[ult_linha].z1;
}

void cLinhaSombra::Move(int xi, int yi, double x2, double y2, double z2)
{
	double altura_sombra = pMapa->AlturaSombra(xi, yi, z2);
	if (altura_sombra == linhas[ult_linha].z1) {
		linhas[ult_linha].x2 = x2;
		linhas[ult_linha].y2 = y2;
	//evita sombras subterr�neas e acima do mapa
	} else if (altura_sombra >= 0 && altura_sombra < pMapa->dimZ) {
		//linhas[ult_linha].x2 = x2;
		//linhas[ult_linha].y2 = y2;
		//se a sombra mudou de altura ent�o criar mais linhas de sombra
		//linha vertical
		short linha_original = ult_linha;
		ult_linha++;
		linhas[ult_linha].largura = linhas[linha_original].largura;
		linhas[ult_linha].x1 = linhas[linha_original].x2;//x2;
		linhas[ult_linha].y1 = linhas[linha_original].y2;//y2;
		linhas[ult_linha].z1 = linhas[linha_original].z2;
		linhas[ult_linha].x2 = x2;
		linhas[ult_linha].y2 = y2;
		linhas[ult_linha].z2 = altura_sombra;
		//linha horizontal
		ult_linha++;
		linhas[ult_linha].largura = linhas[linha_original].largura;
		linhas[ult_linha].x1 = x2;
		linhas[ult_linha].y1 = y2;
		linhas[ult_linha].z1 = altura_sombra;
		linhas[ult_linha].x2 = x2;
		linhas[ult_linha].y2 = y2;
		linhas[ult_linha].z2 = altura_sombra;
	}

}

void cLinhaSombra::Renderiza()
{
	for (int i=0; i<=ult_linha; i++) {
		linhas[i].Renderiza();
	}
}

void cLinhaSombra::Gera(cLinhaTiro &_linha_base, double largura, double x1, double y1, double z1, cMapa *_pMapa)
{
	if (linha_base != _linha_base) {
		Inicia(largura, x1, y1, z1, _pMapa);
		linha_base = _linha_base;
	
		cPontoAPonto p;
		p.IniciaAlvo(linha_base.x1, linha_base.y1, linha_base.z1,
					 linha_base.x2, linha_base.y2, linha_base.z2);
		
		do {
			Move(p.xi, p.yi, p.xf, p.yf, p.zf);
		} while (p.Avanca(0.15));//3));
	}
}