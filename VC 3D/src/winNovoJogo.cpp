
#include "winNovoJogo.h"
#include "arquivos.h"
#include "jogador.h"
#include "jogo.h"
#include "..\modelos\modelosmd2.h"
#include "..\modelos\modelosmd3.h"
#include "..\modelos\modelosmdx.h"
#include "geral.h"
#include "texturas.h"

FXDEFMAP(winNovoJogo) winNovoJogoMap[]={
   //________Message_Type_____________________ID____________Message_Handler_______
	FXMAPFUNC(SEL_CHORE,		winNovoJogo::ID_CHORE,		winNovoJogo::onChore),
	
	FXMAPFUNC(SEL_COMMAND,		winNovoJogo::ID_CANCELAR,	winNovoJogo::onCancelar),
	FXMAPFUNC(SEL_COMMAND,		winNovoJogo::ID_COMECAR,	winNovoJogo::onComecar),
	FXMAPFUNC(SEL_COMMAND,		winNovoJogo::ID_CHKJOG,		winNovoJogo::onChkJog_Click),
};

FXIMPLEMENT(winNovoJogo,FXDialogBox,winNovoJogoMap,ARRAYNUMBER(winNovoJogoMap))

winNovoJogo::winNovoJogo(FXWindow *owner, FXIcon *_icoMain, cJogo *_pJogo):FXDialogBox(owner,"Novo Jogo",DECOR_TITLE|DECOR_BORDER|DECOR_CLOSE)//,0,0,400,300)
{
	//######################### Jogo
	pJogo = _pJogo;

	//######################### Icone
 	/*FXFileStream File;
 	File.open(ARQ::IconeMain, FX::FXStreamLoad);

	icoMain = new FXICOIcon(getApp());
	icoMain->loadPixels(File);*/
	setIcon(_icoMain);

	//######################### ???
	FXString str;
	FXHorizontalFrame *frmH, *frmH2, *frmH3;
	FXVerticalFrame *frmV;
	//FXComposite *frm;

	//frmH = new FXHorizontalFrame(this,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);
	frmH = new FXHorizontalFrame(this,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);
	{
		frmH2 = new FXHorizontalFrame(frmH,FRAME_RAISED|LAYOUT_SIDE_LEFT|PACK_UNIFORM_HEIGHT,0,0,0,0,2,2,2,2);
		{
			FXVerticalFrame *frm_cmbNome =   new FXVerticalFrame(frmH2,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);
			FXVerticalFrame *frm_cmbModelo = new FXVerticalFrame(frmH2,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);
			FXVerticalFrame *frm_cmbPoder =  new FXVerticalFrame(frmH2,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);
			FXVerticalFrame *frm_cmbTime =   new FXVerticalFrame(frmH2,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);

			FXLabel *lbl;
			FXFont *fnt;
			fnt = new FXFont(getApp(), "", 10, FONTWEIGHT_BLACK);
			
			lbl = new FXLabel(frm_cmbNome,  "Nome",  NULL,LAYOUT_FILL|JUSTIFY_CENTER_X);lbl->setFont(fnt);
			lbl = new FXLabel(frm_cmbModelo,"Modelo",NULL,LAYOUT_FILL|JUSTIFY_CENTER_X);lbl->setFont(fnt);
			lbl = new FXLabel(frm_cmbPoder, "Poder", NULL,LAYOUT_FILL|JUSTIFY_CENTER_X);lbl->setFont(fnt);
			lbl = new FXLabel(frm_cmbTime,  "Time",  NULL,LAYOUT_FILL|JUSTIFY_CENTER_X);lbl->setFont(fnt);
			
			for (int j=0; j<NUM_PODERES; j++) {
				poder_cod[j] = j;
			}
			
			num_jogs = 2;
			for (int i=0; i<Num_Jogs_Max; i++) {
				frmH3 = new FXHorizontalFrame(frm_cmbNome,LAYOUT_FILL|LAYOUT_SIDE_LEFT,0,0,0,0,0,0,0,0);
				chkJog[i] = new FXCheckButton(frmH3, "", this, ID_CHKJOG, CHECKBUTTON_NORMAL);
				chkJog[i]->setKey(i);
			
				cmbNome[i] = new FXComboBox(frmH3, 15, 0, 0, COMBOBOX_NORMAL|LAYOUT_FILL);
				cmbNome[i]->setNumVisible(6);
				cmbNome[i]->appendItem(str.format("Jog %d", i+1));
				cmbNome[i]->appendItem("F�bio");
				cmbNome[i]->appendItem("Fau");
				cmbNome[i]->appendItem("Jairo");
				cmbNome[i]->appendItem("Josu�");
				cmbNome[i]->appendItem("Juarez");
				
				//cmbModelo[i] = new FXComboBox(frm_cmbModelo, 25, 0, 0, COMBOBOX_STATIC|LAYOUT_FILL_ROW);
				cmbModelo[i] = new FXTreeListBox(frm_cmbModelo, 0, 0, LAYOUT_FILL_ROW);//|LAYOUT_FIX_WIDTH|FRAME_SUNKEN|FRAME_THICK);
				cmbModelo[i]->setNumVisible(15);
				cmbModelo[i]->setWidth(250);
				cmbModelo[i]->setHSpacing(32);
				
				cmbPoder[i] = new FXComboBox(frm_cmbPoder, 20, 0, 0, COMBOBOX_STATIC|LAYOUT_FILL_ROW);
				cmbPoder[i]->setNumVisible(12);
				for (int j=0; j<NUM_PODERES; j++) {
					cJogador::PoderStr(j, str);
					cmbPoder[i]->appendItem(str, &poder_cod[j]);
				}
				
				cmbTime[i] = new FXComboBox(frm_cmbTime, 2, 0, 0, COMBOBOX_STATIC|LAYOUT_FILL_ROW);
				cmbTime[i]->setNumVisible(9);
				for (j=0; j<26; j++) {
					cmbTime[i]->appendItem(str.format("%c", 'A' + j));
				}
				cmbTime[i]->setCurrentItem(i);
			}
			//m�nimo de 2 jogadores
			chkJog[0]->setCheck();
			chkJog[0]->disable();
			chkJog[1]->setCheck();
			chkJog[1]->disable();
			AtualizaNumJogs(2);
		}
		frmV = new FXVerticalFrame(frmH,LAYOUT_SIDE_LEFT|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);
		{
			new FXButton(frmV,"Cancelar\tCancelar e voltar para a janela principal",NULL,this,ID_CANCELAR,FRAME_THICK|FRAME_RAISED|LAYOUT_TOP|LAYOUT_CENTER_X,0,0,0,0,10,10,5,5);
			FXButton *but = new FXButton(frmV,"Come�ar\tCome�ar o Jogo",NULL,this,ID_COMECAR,FRAME_THICK|FRAME_RAISED|LAYOUT_TOP|LAYOUT_CENTER_X,0,0,0,0,10,10,5,5);
			but->setDefault();
			but->setFocus();
		}
	}
}

winNovoJogo::~winNovoJogo()
{
	//delete ???
}

void winNovoJogo::create()
{
	FXDialogBox::create();
	
	//show(PLACEMENT_SCREEN);
	
	static bool carregou = false;
	if (!carregou) {
		carregou = true;
		
		getApp()->addChore(this,ID_CHORE); //agenda para carregar os modelos assim que a janela aparecer
	}
}

long winNovoJogo::onChore(FXObject*,FXSelector,void*)
{
	this->setTitle("Novo Jogo - Procurando Modelos...");
	this->forceRefresh();

	CarregaModelos();

	this->setTitle("Novo Jogo");
	this->forceRefresh();

	return 1;
}

FXint ListSortFunc(const FXListItem*a,const FXListItem*b)
{
	return a->getText() > b->getText();
}

void winNovoJogo::CarregaModelos()
{
	FXList *lstModelos = 0;
	lstModelos = ModeloMD2::ListaTexturas(this, "hellspawn", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "uberdog", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "pmarine", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "doomunls", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "fz", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "hunter", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "hobgoblin", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "morticus", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "caleb", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "ogro", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "sas", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "zeno", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "hayden", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "mcclane", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "gangster", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "bddroid", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "madpear", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "myles", lstModelos);
	//lstModelos = ModeloMD2::ListaTexturas(this, "alx_plx4", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "klauz", lstModelos);
	lstModelos = ModeloMD2::ListaTexturas(this, "supermale", lstModelos);
	//insere a for�a modelos MD3
	lstModelos = ModeloMD3Anim::ListaTexturas(this, "sonic", lstModelos);
	lstModelos = ModeloMD3Anim::ListaTexturas(this, "stormtrooper", lstModelos);
	lstModelos = ModeloMD3Anim::ListaTexturas(this, "r7", lstModelos);
	if (lstModelos == 0) {
		//nenhum dos modelos acima foi encontrado
		fxwarning("Nenhum dos modelos conhecidos foi encontrado no diret�rio '.\\Data'!");
	}
	
	//ordena a lista de modelos
	int num_modelos = lstModelos->getNumItems();
	lstModelos->setSortFunc(ListSortFunc);
	lstModelos->sortItems();
	
	//icones do quake (2 e 3)
	FXApp *app = getApp();
	double prop;

	FXIcon *q2p = Icone(app, ARQ::IconeQ2);
	FXIcon *q2g = Icone(app, ARQ::IconeQ2);
	prop = (double)q2p->getWidth() / (double)q2p->getHeight();
	q2p->scale((int)floor(0.5 + (16 * prop)), 16);
	q2g->scale((int)floor(0.5 + (48 * prop)), 48);

	FXIcon *q3p = Icone(app, ARQ::IconeQ3);
	FXIcon *q3g = Icone(app, ARQ::IconeQ3);
	prop = (double)q3p->getWidth() / (double)q3p->getHeight();
	q3p->scale((int)floor(0.5 + (16 * prop)), 16);
	q3g->scale((int)floor(0.5 + (48 * prop)), 48);

	//copia a lista para cada um dos combos
	for (int i=0; i<Num_Jogs_Max; i++) {

		FXTreeItem *root_item = cmbModelo[i]->getFirstItem();

		root_item = cmbModelo[i]->appendItem(root_item, "Sorteado");
		cmbModelo[i]->setCurrentItem(root_item);

		FXTreeItem *md2_item = cmbModelo[i]->appendItem(root_item, "MD2", q2g, q2p);
		FXTreeItem *md3_item = cmbModelo[i]->appendItem(root_item, "MD3", q3g, q3p);

		//cmbModelo[i]->appendItem("Sorteado");
		for (int j=0; j<num_modelos; j++) {
			//cmbModelo[i]->appendItem(lstModelos->getItemText(j));
			
			//Arruma os �cones
			FXIcon *icoP = 0;
			FXIcon *icoG = 0;
			icoP = lstModelos->getItemIcon(j);
			if (icoP != 0) {
				icoG = new FXIcon(app, icoP->getData(), 0, 0, icoP->getWidth(), icoP->getHeight());
				icoG->scale(48, 48);
				icoP->scale(16, 16);
			}
			
			//Classifica
			FXString modelo = lstModelos->getItemText(j);
			FXString tipo = modelo.left(3);
			modelo = modelo.mid(4, 4096);
			if (tipo == "MD2") {
				cmbModelo[i]->appendItem(md2_item, modelo, icoG, icoP);
			} else if (tipo == "MD3") {
				cmbModelo[i]->appendItem(md3_item, modelo, icoG, icoP);
			}
		}
		//cmbModelo[i]->setCurrentItem(rndi(0, num_modelos-1));
	}
	
	lstModelos->destroy();
	delete lstModelos;

	//linha necess�ria para mostrar os �cones
	FXDialogBox::create();
}

void winNovoJogo::AtualizaNumJogs(FXuint key)
{
	int i;
	//encontra num_jogs
	if (chkJog[key]->getCheck()) {
		num_jogs = key + 1;
	} else {
		num_jogs = key;
	}
	pJogo->numjogs = num_jogs;
	
	//atualiza controles
	for (i=0; i<Num_Jogs_Max; i++) {
		if (i < num_jogs) {
			chkJog[i]->setCheck();
			cmbNome[i]->enable();
			cmbModelo[i]->enable();
			cmbPoder[i]->enable();
			cmbTime[i]->enable();
		} else {
			chkJog[i]->setCheck(0);
			cmbNome[i]->disable();
			cmbModelo[i]->disable();
			cmbPoder[i]->disable();
			cmbTime[i]->disable();
		}
	}
}

long winNovoJogo::onCancelar(FXObject *obj,FXSelector sel,void* ptr)
{
	return FXDialogBox::onCmdCancel(obj, sel, ptr);
}


long winNovoJogo::onComecar(FXObject *obj,FXSelector sel,void* ptr)
{

	if (IniciaJogo()) {

		return FXDialogBox::onCmdAccept(obj, sel, ptr);

	} else {

		return FXDialogBox::onCmdCancel(obj, sel, ptr);

	}

}

long winNovoJogo::onChkJog_Click(FXObject*obj,FXSelector,void*)
{
	FXCheckButton *chk = (FXCheckButton *)obj;
	AtualizaNumJogs(chk->getKey());
	return 1;
}

bool winNovoJogo::IniciaJogo()
{
	extern FXuint textures[MAX_TEXTURAS];

	//Carrega modelos
	FXString strModelo;
	FXuint *strModeloHash = new FXuint[pJogo->numjogs];

	typedef ModQualq * ModQualqPtr;
	ModQualqPtr *modModelo = new ModQualqPtr[pJogo->numjogs];

	FXString *strTextura = new FXString[pJogo->numjogs];
	FXString strTipo;
	for (int i=0; i<pJogo->numjogs; i++) {
		//Sorteia modelos
		while (cmbModelo[i]->getCurrentItem()->getParent() == 0) { //Sorteado
			//cmbModelo[i]->setCurrentItem(rndi(0, cmbModelo[i]->getNumItems() - 1));
			FXTreeItem *item = cmbModelo[i]->getFirstItem();
			for (int pos = rndi(1, cmbModelo[i]->getNumVisible()); pos>0; pos--)
				item = item->getBelow();
			cmbModelo[i]->setCurrentItem(item);
		}
		while (cmbModelo[i]->getCurrentItem()->getParent()->getParent() == 0) { //Sorteado N�vel 2
			FXTreeItem *pai = cmbModelo[i]->getCurrentItem();
			FXTreeItem *item = pai;
			for (int pos = rndi(1, pai->getNumChildren()); pos>0; pos--)
				item = item->getBelow();
			cmbModelo[i]->setCurrentItem(item);
		}

		//separa string em modelo e textura
		strModelo = cmbModelo[i]->getCurrentItem()->getText().before('\\');
		strModeloHash[i] = strModelo.hash();
		strTextura[i] = cmbModelo[i]->getCurrentItem()->getText().after('\\');
		strTipo = cmbModelo[i]->getCurrentItem()->getParent()->getText();
		
		/*bool repetiu_modelo = false;
		//TO DO: por enquanto s� o formato MD2 tem reaproveitamento de modelos (podendo variar a textura)
		if (strTipo == "MD2") {
			for (int j=0; j<i; j++) {
				if (strModeloHash[j] == strModeloHash[i]) {
					modModelo[i] = modModelo[j];
					repetiu_modelo = true;
					break;
				}
			}
		}*/
		
		//if (!repetiu_modelo) {
			if (strTipo == "MD2") {
				modModelo[i] = new ModeloMD2;
				if (!modModelo[i]->Carrega(getApp(), strModelo)) {
					fxwarning("Erro ao carregar modelo ''%s''!", strModelo);
				}
			} else if (strTipo == "MD3") {
				modModelo[i] = new ModeloMD3Anim;
				modModelo[i]->nome_modelo = strModelo;
				//deixa para carregar depois
			} else {
				fxwarning("Erro inesperado: Tipo de modelo desconhecido: %s!", strModelo.left(4));
			}
		//}
	}
	
	//const FXString strMod = "hobgoblin"; const FXString strTex = "";
	//if (!modelo->Carrega(getApp(), strMod)) {
	//	fxwarning("Erro ao carregar modelo ''%s''!", strMod);
	//}
	
	/*
	1�) Sortear Ordem;
	2�) Embaralhar Poderes;
	3�) Acionar/Sorteiar Poderes [obsoleto?];
	4�) Posicionar;
	5�) Acionar Monstro [obsoleto];
	6�) Esconder conte�do que s� aparece no editor;
	7�) Iniciar vez do primeiro (zera replay, zera 'tempo');
	8�) Chamar o primeiro.
	*/
	//Cria jogadores
	cJogador* &J = pJogo->J;
	J = new cJogador[pJogo->numjogs];
	for (int i=0; i<pJogo->numjogs; i++) {
		J[i].id = i;
		J[i].idImit = i;

		//J[i].Nome.format("Jog %d", i+1);
		J[i].Nome = cmbNome[i]->getText();
				
		//J[i].Poder = pSorteado;
		J[i].Poder = *(int *)cmbPoder[i]->getItemData(cmbPoder[i]->getCurrentItem());

		J[i].Time = i;

		J[i].podeimitar = new bool[pJogo->numjogs];
		for (int j=0; j<pJogo->numjogs; j++) J[i].podeimitar[j] = false;

		//Sorteia a cor
		/*
		J[i].Cor[0] = rndi(0, 255);
		J[i].Cor[1] = rndi(0, 255);
		J[i].Cor[2] = rndi(0, 255);
		/* /
		double parte_r = rnd();
		double parte_g = rnd();
		double parte_b = rnd();
		double soma_partes = parte_r + parte_g + parte_b;
		parte_r /= soma_partes;
		parte_g /= soma_partes;
		parte_b /= soma_partes;
		short cortotal = rndi(200, 700); //255 * 3 = 765
		J[i].Cor[0] = (FXuint)Min(255, parte_r * cortotal);
		J[i].Cor[1] = (FXuint)Min(255, parte_g * cortotal);
		J[i].Cor[2] = (FXuint)Min(255, parte_b * cortotal);
		/**/

		//TEMP: � pro cara poder selecionar a textura, mas essa GUI (a antiga) n�o colabora
		//J[i].Textura = rndi(240, 245);

		J[i].Frente = rndi(0, 11);

		J[i].idProx = -1;

		J[i].Bot = false;

		J[i].AcionaPoder(pJogo->Armas, pJogo->Municoes);
		
		//modelo
		//ModeloMD2 *mod = 0;
		ModQualq *mod = 0;
		switch (J[i].Poder) {
			case pRobocop: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "robocop");
				strTextura[i] = "murphy";
				break;
			}
			case pZerg: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "hydralisk");
				strTextura[i] = "";
				break;
			}
			case pDragao: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "necromicus");
				strTextura[i] = "ctf_b";
				break;
			}
			case pAlienPequeno: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "ripper");
				strTextura[i] = "";
				break;
			}
			/*case pAlien: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "alien");
				strTextura[i] = "";
				break;
			}*/
			case pBalacau: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "alien");
				strTextura[i] = "";
				break;
			}
			case pTarrasque: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "awolf");
				strTextura[i] = "wolfskin";
				break;
			}
			case pTanque: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "humvee");
				strTextura[i] = "";
				break;
			}
			case pJason: {
				mod = new ModeloMD2;
				mod->Carrega(getApp(), "jasonv");
				strTextura[i] = "jason";
				break;
			}
			default: {
				mod = modModelo[i];
				break;
			}
		}
		//if (!J[i].modelo.Carrega(mod, getApp(), textures[511-i], strTextura[i])) {
		if (!J[i].modelo.Carrega(getApp(), mod, mod->nome_modelo, strTextura[i])) {
			fxwarning("Erro ao carregar textura ''%s'' do modelo ''%s''!", strTextura[i], mod->nome_modelo);
			return false;
		}
		//J[i].modelo.AnimInicia(ModeloMD2Anim::ANIM_STAND);
		J[i].modelo.AnimInicia(cModeloAnim::ANIM_STAND);
	}
	pJogo->JaVi = new char[pJogo->numjogs];
	for (i=0; i<pJogo->numjogs; i++) pJogo->JaVi[i] = 0;

	pJogo->JaViHol = new bool[pJogo->numjogs];
	for (i=0; i<pJogo->numjogs; i++) pJogo->JaViHol[i] = false;

	pJogo->SorteiaOrdem();
	pJogo->Posiciona();
	pJogo->pMapa->ConverteParaJogo();
	pJogo->CriaFogs();

	pJogo->MudaEstado(EST_JOGANDO);
	return true;
}
	//Lista de Modelos
		//const FXString strMod = "warvet"; const FXString strTex = ""; //sem bra�o
		//const FXString strMod = "pm"; const FXString strTex = "";//muito tosco
		//const FXString strMod = "cringe"; const FXString strTex = "bolted";//tosco, embora tenha v�rias texturas
		//const FXString strMod = "Witchblade"; const FXString strTex = "sara";//sem bra�o
		//const FXString strMod = "darkness"; const FXString strTex = ""; //~duro
		//const FXString strMod = "Johnny5"; const FXString strTex = "";//robo (descentralizado)
		//const FXString strMod = "lordmaul"; const FXString strTex = "";//darth maul (sem perna)
		//const FXString strMod = "GiJoe"; const FXString strTex = "Sgt'McKenzie";//textura defeituosa
		//const FXString strMod = "m"; const FXString strTex = "actionbond";//sem bra�o
		//const FXString strMod = "bobafett"; const FXString strTex = "esb_fett";//podre
		//const FXString strMod = "messiah"; const FXString strTex = "blade";//textura defeituosa

		//const FXString strMod = "robocop"; const FXString strTex = "murphy";			//robocop
		//const FXString strMod = "swindle"; const FXString strTex = "";				//carinha com pneus nas costas
		//const FXString strMod = "hydralisk"; const FXString strTex = "";				//zerg
		//const FXString strMod = "necromicus"; const FXString strTex = "ctf_b";		//drag�o
		//const FXString strMod = "forgottenone"; const FXString strTex = "";				//rapazola com asas
		//const FXString strMod = "forgottenone2"; const FXString strTex = "forgottenone";	//rapazola sem asas
		//const FXString strMod = "bauul"; const FXString strTex = "";					//fogueteiro
		//const FXString strMod = "devi"; const FXString strTex = "";					//magr�o
		//const FXString strMod = "alita"; const FXString strTex = "";					//mulher magrela
		//const FXString strMod = "ripper"; const FXString strTex = "";					//pAlienPequeno
		//const FXString strMod = "raptor"; const FXString strTex = "blue";				//tarrasque?
		//const FXString strMod = "ornithomimus"; const FXString strTex = "Reptyleon";	//tarrasque?
		//const FXString strMod = "awolf"; const FXString strTex = "wolfskin";			//tarrasque
		//const FXString strMod = "droideka"; const FXString strTex = "";				//robo que rola
		//const FXString strMod = "goblin"; const FXString strTex = "";					//um monstro
		//const FXString strMod = "humvee"; const FXString strTex = "";					//tanque tosc�o
		//const FXString strMod = "tankrat"; const FXString strTex = "";				//tanque
		//const FXString strMod = "alien"; const FXString strTex = "";					//alien &| balacau
		//const FXString strMod = "storm"; const FXString strTex = "";					//~ robo
		//const FXString strMod = "darthvader"; const FXString strTex = "";				//darth vader
		//const FXString strMod = "cheetor"; const FXString strTex = "";				//quadr�pede
		//const FXString strMod = "jasonv"; const FXString strTex = "jason";			//jason
		//const FXString strMod = "hueteotl"; const FXString strTex = "";				//zumbi esquisito
		//const FXString strMod = "mrfrost"; const FXString strTex = "";				//boneco de neve (dedo-duro?)

		//const FXString strMod = "hellspawn"; const FXString strTex = "spawn";
		//const FXString strMod = "uberdog"; const FXString strTex = "uber";
		//const FXString strMod = "pmarine"; const FXString strTex = "ctf_b";
		//const FXString strMod = "pmarine"; const FXString strTex = "usmc_claro";
		//const FXString strMod = "doomunls"; const FXString strTex = "blue";
		//const FXString strMod = "fz"; const FXString strTex = "blue";
		//const FXString strMod = "hobgoblin"; const FXString strTex = "";
		//const FXString strMod = "hunter"; const FXString strTex = "";
		//const FXString strMod = "morticus"; const FXString strTex = "ctf_b";
		//const FXString strMod = "caleb"; const FXString strTex = ""; //sabata
		//const FXString strMod = "ogro"; const FXString strTex = "gib";
		//const FXString strMod = "sas"; const FXString strTex = "";//ninja
		//const FXString strMod = "zeno"; const FXString strTex = "zenoblue";//e.t. magrela
		//const FXString strMod = "hayden"; const FXString strTex = "";
		//const FXString strMod = "mcclane"; const FXString strTex = "nakatomi1";
		//const FXString strMod = "gangster"; const FXString strTex = "petey";
		//const FXString strMod = "bdroid"; const FXString strTex = "infantry";
		//const FXString strMod = "madpear"; const FXString strTex = "blue";//monstr�o esquisito
		//const FXString strMod = "myles"; const FXString strTex = "";
		//const FXString strMod = "alx_plx4"; const FXString strTex = "default";//robo bob�o
		//const FXString strMod = "klauz"; const FXString strTex = "";//papai noel
		//const FXString strMod = "supermale"; const FXString strTex = "kw_blue";
