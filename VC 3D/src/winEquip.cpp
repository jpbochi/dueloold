
#include "winEquip.h"
#include "arquivos.h"

#include "texturas.h"

#include "jogo.h"

#include "iconebut.h"

FXDEFMAP(winEquip) winEquipMap[]={
   //________Message_Type_____________________ID____________Message_Handler_______
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_VOLTAR,		winEquip::onVoltar),
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_EQUIP,			winEquip::onInv),
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_SKILLS,		winEquip::onSkills),
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_CHAO,			winEquip::onChao),
	
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_CABECA,		winEquip::onCabeca),
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_CORPO,			winEquip::onCorpo),
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_MAODIR,		winEquip::onMaoDir),
	FXMAPFUNC(SEL_COMMAND,		winEquip::ID_MAOESQ,		winEquip::onMaoEsq),
};

FXIMPLEMENT(winEquip,FXDialogBox,winEquipMap,ARRAYNUMBER(winEquipMap))

winEquip::winEquip(FXWindow *owner, FXIcon *icoMain, cJogo *_pJogo):FXDialogBox(owner,"Equipamento",DECOR_TITLE|DECOR_BORDER|DECOR_CLOSE)//,0,0,400,300)
{
	//######################### Jogo
	pJogo = _pJogo;
	//pIc = pJogo->pIc;

	//######################### Icone
	setIcon(icoMain);

	//######################### Carinha
 	FXFileStream File;
 	File.open(ARQ::PessoaEquip, FX::FXStreamLoad);
	FXIcon *img = new FXGIFIcon(getApp());
	img->loadPixels(File);
	File.close();
	ChromaKey(img, FXRGB(255, 255, 255));

	//######################### ???
	FXString str;
	FXHorizontalFrame *frmH;//, *frmH2, *frmH3;
	FXVerticalFrame *frmV;
	//FXComposite *frm;
	
	FXIcon *ico = pJogo->pIc->icones[0];

	frmH = new FXHorizontalFrame(this,LAYOUT_SIDE_TOP|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);
	{
		frmV = new FXVerticalFrame(frmH,LAYOUT_SIDE_LEFT|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);
		{
			butCabeca = new FXButton(frmV, "Cabe�a"	 ,ico,this,ID_CABECA,TEXT_ABOVE_ICON|FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR);
			butMaoDir = new FXButton(frmV, "M�o Dir.",ico,this,ID_MAODIR,TEXT_ABOVE_ICON|FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR);
			butCorpo  = new FXButton(frmV, "Corpo"	 ,ico,this,ID_CORPO ,TEXT_ABOVE_ICON|FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR);
		}
		
		new FXLabel(frmH, "", img, FRAME_NONE);
		
		frmV = new FXVerticalFrame(frmH,LAYOUT_SIDE_LEFT|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);
		{
			//*
			//new FXFrame(frmV, 0);
			frmAcimaMaoEsq = new FXFrame(frmV, LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT, 0, 0, butMaoDir->getWidth(), butMaoDir->getHeight());
			/*/
			butMaoEsq = new FXButton(frmV, "",ico,this,0,TEXT_ABOVE_ICON|FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR);
			butMaoEsq->disable();//butMaoEsq->hide();
			/**/
			butMaoEsq = new FXButton(frmV, "M�o Esq.",ico,this,ID_MAOESQ,TEXT_ABOVE_ICON|FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR);
			
		}

		new FXVerticalSeparator(frmH,SEPARATOR_RIDGE|LAYOUT_FILL_Y);

		frmV = new FXVerticalFrame(frmH,LAYOUT_SIDE_LEFT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);
		{
			new FXLabel(frmV, "Invent�rio");

			matInv = new FXMatrix(frmV, 3, PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH|MATRIX_BY_ROWS); //MATRIX_BY_COLUMNS
			matInv->setHSpacing(0);
			matInv->setVSpacing(0);

			for (int i=0; i<18; i++) {
				new FXButton(matInv, "",ico,this,ID_EQUIP,FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR|TEXT_BELOW_ICON);
			}

			new FXHorizontalSeparator(frmV,SEPARATOR_RIDGE|LAYOUT_FILL_X);
			new FXLabel(frmV, "Skills");

			matSkills = new FXMatrix(frmV, 1, PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH|MATRIX_BY_ROWS); //MATRIX_BY_COLUMNS
			matSkills->setHSpacing(0);
			matSkills->setVSpacing(0);

			for (int i=0; i<6; i++) {
				new FXButton(matSkills, "",ico,this,ID_SKILLS,FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR|TEXT_BELOW_ICON);
			}
		}

		new FXVerticalSeparator(frmH,SEPARATOR_RIDGE|LAYOUT_FILL_Y);

		frmV = new FXVerticalFrame(frmH,LAYOUT_SIDE_LEFT|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);
		{
			lblDescr = new FXLabel(frmH, "Propriedades do item:\nDano: 10.0\nMuni��o: 6", NULL, JUSTIFY_LEFT|JUSTIFY_TOP);
		}
	}

	new FXHorizontalSeparator(this,SEPARATOR_RIDGE|LAYOUT_FILL_X);
	new FXLabel(this, "Ch�o", 0, LABEL_NORMAL|LAYOUT_FILL_X);

	matChao = new FXMatrix(this, 2, PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH|MATRIX_BY_ROWS);
	matChao->setHSpacing(1);
	matChao->setVSpacing(0);
	
	for (int i=0; i<30; i++) { //24
		new FXButton(matChao, "",ico,this,ID_CHAO,FRAME_THICK|FRAME_SUNKEN|BUTTON_TOOLBAR|TEXT_BELOW_ICON);
	}

	FXButton *but = new FXButton(this,"Voltar",NULL,this,ID_VOLTAR,FRAME_THICK|FRAME_RAISED|LAYOUT_TOP|LAYOUT_FILL,0,0,0,0,10,10,5,5);
	but->setDefault();
	but->setFocus();
}

winEquip::~winEquip()
{
	//pIc = 0; //foi criado pela winMain e deve ser esquecido, n�o apagado
	//delete ???
}

void winEquip::create()
{
	lugar_sel = ID_LAST;
	but_sel = 0;
	
	Preenche();

	FXDialogBox::create();
	
	frmAcimaMaoEsq->setHeight(butMaoEsq->getHeight());
	frmAcimaMaoEsq->setWidth(butMaoEsq->getWidth());
	FXDialogBox::create();
	
	//show(PLACEMENT_SCREEN);
}

void winEquip::Preenche()
{
	//Esvazia Tudo
	pJogo->pIc->EsvaziaMat(matChao);
	pJogo->pIc->EsvaziaMat(matInv);
	pJogo->pIc->EsvaziaMat(matSkills);
	
	//atualiza as listas de objetos (ch�o, equip, skills....)
	ColetaItens();
	
	pJogo->pIc->PreencheMat(matChao, Chao);
	pJogo->pIc->PreencheMat(matInv, Equip);
	pJogo->pIc->PreencheMat(matSkills, Skills);
	
	pJogo->pIc->PreencheBut(butCabeca, objCabeca);
	pJogo->pIc->PreencheBut(butCorpo,  objCorpo);
	pJogo->pIc->PreencheBut(butMaoDir, objMaoDir);
	pJogo->pIc->PreencheBut(butMaoEsq, objMaoEsq);
	
	DescreveItem();
	
	//FXDialogBox::create();
}

void winEquip::ColetaItens()
{
	short vz = pJogo->vz;
	cJogador *Jogvz = &pJogo->J[vz];
	
	Chao = pJogo->Mapa.GetListaPos(Jogvz->x, Jogvz->y, Jogvz->z);
	Equip = &Jogvz->Equip;
	Skills = &Jogvz->Skills;
	objMaoDir = Jogvz->objMaoDir;
	objMaoEsq = Jogvz->objMaoEsq;
	objCabeca = Jogvz->objCabeca;
	objCorpo  = Jogvz->objCorpo;
}

void winEquip::DescreveItem()
{
	FXString descr("");
	
	if (but_sel != 0) {
		cObjeto *obj_sel = (cObjeto *)but_sel->getUserData();
		if (obj_sel != 0) {
			//s� escreve alguma coisa se existir um objeto selecionado
			if (obj_sel->EhMunicao()) {
				short codmun	= obj_sel->GetCod();
				short codarma	= pJogo->Municoes[codmun].ArmaDest;
				short mun		= abs(obj_sel->municao);
				short maxmun	= pJogo->Municoes[codmun].MunPac;
				bool fired		= (obj_sel->municao < 0);
				descr.format("Muni��o\nArma: %s\nQuantidade: %d/%d",
							 pJogo->Armas[codarma].Nome,
							 mun, maxmun);
			} else if (obj_sel->EhArma()) {
				short codarma	= obj_sel->GetCod();
				short codmun	= obj_sel->codmunicao;
				short mun		= abs(obj_sel->municao);
				short maxmun	= pJogo->Municoes[codmun].MunPac;
				bool fired		= (obj_sel->municao < 0);
				if (codmun == mSemMunicao) {
					descr.format("Arma\nArma: %s\nSem muni��o",
								pJogo->Armas[codarma].Nome);
				} else {
					descr.format("Arma\n%s\nmuni��o %s: %d/%d",
								pJogo->Armas[codarma].Nome,
								pJogo->Municoes[codmun].Nome,
								mun, maxmun);
				}
			}
		}
	}
	
	lblDescr->setText(descr);

	FXDialogBox::create();
}

void winEquip::Clica(FXButton *but_tgt, int lugar_tgt)
{
	if (but_sel != 0) but_sel->setButtonStyle(BUTTON_TOOLBAR);

	//Trata situa��es estranhas
	if (but_tgt == but_sel) {
		//apenas deseleciona
		lugar_sel = ID_LAST;
		but_sel = 0;
		return;
		/*switch (lugar_tgt) {
			case ID_CHAO: {
				if (butMaoDir->getUserData() == 0) {
					lugar_tgt = ID_MAODIR;
					but_tgt = butMaoDir;
				} else {
					lugar_tgt = ID_MAOESQ;
					but_tgt = butMaoEsq;
				}
			} break;
		}*/
	} else if (lugar_tgt == lugar_sel) {
		//deseleciona o antigo e clica no novo
		lugar_sel = ID_LAST;
		but_sel = 0;
	}
	
	cObjeto *obj_sel = (but_sel == 0) ? 0 : (cObjeto *)but_sel->getUserData();
	cObjeto *obj_tgt = (but_tgt == 0) ? 0 : (cObjeto *)but_tgt->getUserData();
	
	bool deselecionar = true;

	#define ALVO_VAZIO	(obj_tgt == 0)
	#define ALVO_MAO	(lugar_tgt == ID_MAODIR || lugar_tgt == ID_MAOESQ)
	#define ALVO_EHARMA	((obj_tgt == 0) ? false : obj_tgt->EhArma())
	#define ORIG_EHMUN	((obj_sel == 0) ? false : obj_sel->EhMunicao())
	#define VAI_RECARR  (ALVO_MAO && ALVO_EHARMA && ORIG_EHMUN)
	
	if ((lugar_sel == ID_LAST) || (!ALVO_VAZIO && !VAI_RECARR)) {
		//Seleciona
		//	SE n�o tinha clicado em lugar nenhum antes
		//	OU clicou em um lugar n�o-vazio E n�o vai recarregar
		if ((obj_tgt != 0)) { //s� seleciona se existir um objeto no local clicado
			lugar_sel = lugar_tgt;
			but_sel = but_tgt;
			but_sel->setButtonStyle(FRAME_LINE);
			deselecionar = false;
			DescreveItem();
		}
	} else {
		if (!Move(obj_sel, lugar_sel, obj_tgt, lugar_tgt)) {
			fxwarning("Comando inv�lido!");
		} else {
			pJogo->Reescreve();
		}
	}

	#undef ALVO_VAZIO
	#undef ALVO_MAO
	#undef ALVO_EHARMA
	#undef ORIG_EHMUN
	#undef VAI_RECARR
	
	if (deselecionar) {
		lugar_sel = ID_LAST;
		but_sel = 0;
	}
	
}

bool winEquip::Move(cObjeto *obj_orig, int lugar_orig, cObjeto *obj_tgt, int lugar_tgt)
{
	//esta fun��o � recursiva para permitir tranfer�ncias m�ltiplas em dois cliques
	//ex.: equip -> chao = equip -> mao(qualquer uma) -> chao
	//     chao -> corpo = chao -> mao(as duas) -> corpo
	
	#define ALVO_VAZIO	(obj_tgt == 0)
	#define ALVO_MAO	(lugar_tgt == ID_MAODIR || lugar_tgt == ID_MAOESQ)
	#define ALVO_CHAO	(lugar_tgt == ID_CHAO)
	#define ALVO_EQUIP	(lugar_tgt == ID_EQUIP)
	#define ALVO_SKILLS	(lugar_tgt == ID_SKILLS)
	#define ALVO_CABECA	(lugar_tgt == ID_CABECA)
	#define ALVO_CORPO	(lugar_tgt == ID_CORPO)
	#define MAO_ALVO	(lugar_tgt == ID_MAODIR)
	#define MAO_SEL		(lugar_orig == ID_MAODIR)
	#define ALVO_EHARMA	((obj_tgt == 0) ? false : obj_tgt->EhArma())
	#define ORIG_EHMUN	((obj_orig == 0) ? false : obj_orig->EhMunicao())
	
	bool done = false;
	
	switch (lugar_orig) {
		case ID_CHAO: {
			if (ALVO_MAO) {
				//Ch�o -> M�o
				if (pJogo->Pega(obj_orig, Chao, MAO_ALVO)) Preenche();
				done = true;
			} else if (ALVO_EQUIP) {
				//Ch�o -> M�o -> Equip
				//procura uma m�o vazia
				cObjeto *obj_maodir = (cObjeto *)butMaoDir->getUserData();
				cObjeto *obj_maoesq = (cObjeto *)butMaoEsq->getUserData();
				int mao = -1;
				if (obj_maodir == 0)		mao = ID_MAODIR;
				else if (obj_maoesq == 0)	mao = ID_MAOESQ;
				
				if (mao == -1) {
					fxwarning("Voc� precisa de uma m�o vazia!");
					done = true;
				} else {
					if (pJogo->Pega(obj_orig, Chao, mao == ID_MAODIR)) {
						Preenche();
						if (MAO_ALVO) obj_orig = (cObjeto *)butMaoDir->getUserData();
						else		  obj_orig = (cObjeto *)butMaoEsq->getUserData();
						done = Move(obj_orig, mao, NULL, ID_EQUIP);
					} else {
						done = true;
					}
				}
			}
		} break;
		case ID_MAODIR:
		case ID_MAOESQ: {
			if (ALVO_CHAO) {
				//M�o -> Ch�o
				if (pJogo->Solta(MAO_SEL)) Preenche();
				done = true;
			} else if ((ALVO_EQUIP || ALVO_SKILLS)) {
				//M�o -> Equip.
				if (pJogo->Guarda(MAO_SEL)) Preenche();
				done = true;
			} else if (ALVO_MAO && ALVO_EHARMA && ORIG_EHMUN) {
				//Recarregar: Muni��o em uma M�o -> Arma na outra M�o
				if (pJogo->Recarrega()) Preenche();
				done = true;
			} else if (ALVO_CABECA) {
				//M�o -> Cabe�a
				if (obj_orig->GetLugarUso() == cObjeto::codCabeca) {
					if (pJogo->Vestir(MAO_SEL,cObjeto::codCabeca)) Preenche();
					done = true;
				}
			} else if (ALVO_CORPO) {
				//M�o -> Corpo
				if (obj_orig->GetLugarUso() == cObjeto::codCorpo) {
					if (pJogo->Vestir(MAO_SEL,cObjeto::codCorpo)) Preenche();
					done = true;
				}
			}
		} break;
		case ID_SKILLS:
		case ID_EQUIP: {
			if (ALVO_MAO) {
				//Equip. -> M�o
				if (pJogo->Saca(MAO_ALVO, obj_orig)) Preenche();
				done = true;
			}
		} break;
		case ID_CABECA: {
			if (ALVO_MAO) {
				//Cabe�a -> M�o
				if (pJogo->Desvestir(MAO_SEL,cObjeto::codCabeca)) Preenche();
				done = true;
			}
		} break;
		case ID_CORPO: {
			if (ALVO_MAO) {
				//Corpo -> M�o
				if (pJogo->Desvestir(MAO_SEL,cObjeto::codCorpo)) Preenche();
				done = true;
			}
		} break;
	}
	return done;

	#undef ALVO_VAZIO
	#undef ALVO_MAO
	#undef ALVO_CHAO
	#undef ALVO_EQUIP
	#undef ALVO_SKILLS
	#undef ALVO_CABECA
	#undef ALVO_CORPO
	#undef MAO_ALVO
	#undef MAO_SEL
	#undef ALVO_EHARMA
	#undef ORIG_EHMUN
}

long winEquip::onVoltar(FXObject *obj, FXSelector sel, void *ptr)
{
	return FXDialogBox::onCmdCancel(obj, sel, ptr);
	//return FXDialogBox::onCmdAccept(obj, sel, ptr);
}

long winEquip::onInv(FXObject *obj, FXSelector sel, void *ptr)
{
	Clica((FXButton *)obj, ID_EQUIP);
	return 1;
}

long winEquip::onSkills(FXObject *obj, FXSelector sel, void *ptr)
{
	Clica((FXButton *)obj, ID_SKILLS);
	return 1;
}

long winEquip::onChao(FXObject *obj, FXSelector sel, void *ptr)
{
	Clica((FXButton *)obj, ID_CHAO);
	return 1;
}

long winEquip::onCabeca(FXObject *obj, FXSelector sel, void *ptr)
{
	Clica((FXButton *)obj, ID_CABECA);
	return 1;
}

long winEquip::onCorpo(FXObject *obj, FXSelector sel, void *ptr)
{
	Clica((FXButton *)obj, ID_CORPO);
	return 1;
}

long winEquip::onMaoDir(FXObject *obj, FXSelector sel, void *ptr)
{
	Clica((FXButton *)obj, ID_MAODIR);
	return 1;
}

long winEquip::onMaoEsq(FXObject *obj, FXSelector sel, void *ptr)
{
	Clica((FXButton *)obj, ID_MAOESQ);
	return 1;
}
