
#include "openglinfo.h"

FXDEFMAP(SettingsDialog) SettingsDialogMap[]={
   //________Message_Type_____________________ID____________Message_Handler_______
	FXMAPFUNC(SEL_COMMAND,			SettingsDialog::ID_ACCEPT,	SettingsDialog::onOK),
};

// Settings dialog thanks to Sander Jansen <sxj@cfdrc.com>
//FXIMPLEMENT(SettingsDialog,FXDialogBox,NULL,0)
FXIMPLEMENT(SettingsDialog,FXDialogBox,SettingsDialogMap,ARRAYNUMBER(SettingsDialogMap))


// Construct a dialog box
SettingsDialog::SettingsDialog(FXWindow* owner,FXGLVisual * vis):FXDialogBox(owner,"OpenGL Info",DECOR_TITLE|DECOR_BORDER,0,600){
  FXVerticalFrame * master=new FXVerticalFrame(this,LAYOUT_FILL_X|LAYOUT_FILL_Y,0,0,0,0, 0,0,0,0);

  //gl version information
  FXGroupBox *versionbox=new FXGroupBox(master,"OpenGL Driver Information",GROUPBOX_NORMAL|FRAME_RIDGE|LAYOUT_FILL_X);
  FXMatrix *v_matrix=new FXMatrix(versionbox,2,MATRIX_BY_COLUMNS,0,0,0,0,0,0,0,0);

  new FXLabel(v_matrix,"Vendor: ",NULL,LABEL_NORMAL);
  new FXLabel(v_matrix,FXStringFormat("%s",glGetString(GL_VENDOR)),NULL,LABEL_NORMAL);

  new FXLabel(v_matrix,"Renderer: ",NULL,LABEL_NORMAL);
  new FXLabel(v_matrix,FXStringFormat("%s",glGetString(GL_RENDERER)),NULL,LABEL_NORMAL);

  new FXLabel(v_matrix,"GL Version: ",NULL,LABEL_NORMAL);
  new FXLabel(v_matrix,FXStringFormat("%s",glGetString(GL_VERSION)),NULL,LABEL_NORMAL);

  new FXLabel(v_matrix,"GLU Version: ",NULL,LABEL_NORMAL);
  new FXLabel(v_matrix,FXStringFormat("%s",gluGetString(GLU_VERSION)),NULL,LABEL_NORMAL);

  FXHorizontalFrame *options=new FXHorizontalFrame(master,LAYOUT_SIDE_TOP|FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y,0,0,0,0,0,0,0,0);

  //Display Mode
  FXGroupBox *dmbox=new FXGroupBox(options,"Display Mode",GROUPBOX_NORMAL|FRAME_RIDGE|LAYOUT_FILL_Y);
  FXMatrix *mat=new FXMatrix(dmbox,2,MATRIX_BY_COLUMNS);


  new FXLabel(mat,"Accelerated",NULL,LABEL_NORMAL);
  if(vis->isAccelerated())
    new FXLabel(mat,"enabled",NULL,LABEL_NORMAL);
  else
    new FXLabel(mat,"disabled",NULL,LABEL_NORMAL);

  new FXLabel(mat,"Double Buffering",NULL,LABEL_NORMAL);
  if(vis->isDoubleBuffer())
    new FXLabel(mat,"enabled",NULL,LABEL_NORMAL);
  else
    new FXLabel(mat,"disabled",NULL,LABEL_NORMAL);

  new FXLabel(mat,"Stereo View",NULL,LABEL_NORMAL);
  if(vis->isStereo())
    new FXLabel(mat,"enabled",NULL,LABEL_NORMAL);
  else
    new FXLabel(mat,"disabled",NULL,LABEL_NORMAL);

  new FXLabel(mat,"Color Depth",NULL,LABEL_NORMAL);
  new FXLabel(mat,FXStringFormat("%d",vis->getDepth()),NULL,LABEL_NORMAL);

  new FXLabel(mat,"Depth Buffer Size",NULL,LABEL_NORMAL);
  new FXLabel(mat,FXStringFormat("%d",vis->getActualDepthSize()),NULL,LABEL_NORMAL);

  new FXLabel(mat,"Stencil Buffer Size",NULL,LABEL_NORMAL);
  new FXLabel(mat,FXStringFormat("%d",vis->getActualStencilSize()),NULL,LABEL_NORMAL);

  new FXLabel(mat,"RGBA",NULL,LABEL_NORMAL);
  new FXLabel(mat,FXStringFormat("%d-%d-%d-%d",vis->getActualRedSize(),vis->getActualGreenSize(),vis->getActualBlueSize(),vis->getActualAlphaSize()),NULL,LABEL_NORMAL);

  new FXLabel(mat,"Accum RGBA",NULL,LABEL_NORMAL);
  new FXLabel(mat,FXStringFormat("%d-%d-%d-%d",vis->getActualAccumRedSize(),vis->getActualAccumGreenSize(),vis->getActualAccumBlueSize(),vis->getActualAccumAlphaSize()),NULL,LABEL_NORMAL);

  FXGroupBox *exbox= new FXGroupBox(options,"Available GL/GLU Extensions",GROUPBOX_NORMAL|FRAME_RIDGE|LAYOUT_FILL_Y|LAYOUT_FILL_X);

  FXVerticalFrame *listframe=new FXVerticalFrame(exbox,LAYOUT_FILL_X|LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0,0,0);

  FXList *pExtList=new FXList(listframe,NULL,0,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X|LAYOUT_FILL_Y);

  char *token,*text,*tmp;

  // Get GL extensions
  tmp=(char*)glGetString(GL_EXTENSIONS);
  if(tmp){
    text=strdup(tmp);
    token=strtok(text," ");
    while(token!=NULL){
      pExtList->appendItem(FXStringFormat("(GL) %s",token));
      token=strtok(NULL," ");
      }
    free(text);
    }

  // Get GLU extensions
#if defined(GLU_VERSION_1_1)
  tmp=(char*)gluGetString(GLU_EXTENSIONS);
  if(tmp){
    text=strdup(tmp);
    token=strtok(text," ");
    while(token!=NULL){
      pExtList->appendItem(FXStringFormat("(GLU) %s",token));
      token=strtok(NULL," ");
      }
    free(text);
    }
#endif

  new FXHorizontalSeparator(master);

  // Contents
  FXHorizontalFrame * control=new FXHorizontalFrame(master,LAYOUT_SIDE_TOP|FRAME_NONE|LAYOUT_FILL_X);

  // Accept
  new FXButton(control,"OK",NULL,this,ID_ACCEPT,FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X|LAYOUT_CENTER_Y,0,0,0,0, 20,20,3,3);
  }


// Must delete the menus
SettingsDialog::~SettingsDialog(){
  }
  
long SettingsDialog::onOK(FXObject *obj, FXSelector sel, void* ptr)
{
	return FXDialogBox::onCmdAccept(obj, sel, ptr);
}