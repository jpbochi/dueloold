
#ifndef WINEQUIP_H
#define WINEQUIP_H

#include "fx.h"
//#include "fx3d.h"
#include "iconebut.h"

class cObjeto;
class cListaObjetos;
class cListaPosObjetos;

class winEquip : public FXDialogBox {

	FXDECLARE(winEquip)

protected:
	class cJogo *pJogo;
	
	FXMatrix *matInv;
	FXMatrix *matSkills;
	FXMatrix *matChao;
	
	FXButton *butCabeca;
	FXButton *butCorpo;
	FXButton *butMaoDir;
	FXButton *butMaoEsq;
	
	FXFrame *frmAcimaMaoEsq;
	
	FXLabel *lblDescr;
	
	cObjeto			*objMaoDir, *objMaoEsq, *objCabeca, *objCorpo;
	cListaObjetos	*Equip, *Skills;
	cListaPosObjetos*Chao;

	//class cIconeBut *pIc;

	int lugar_sel;
	FXButton *but_sel;
	
	winEquip(){}

public:
	enum{
		ID_VOLTAR=FXDialogBox::ID_LAST,
		ID_EQUIP,
		ID_SKILLS,
		ID_CHAO,
		ID_CABECA,
		ID_CORPO,
		ID_MAODIR,
		ID_MAOESQ,
		ID_LAST,
	};

	private:
	void Preenche();
	void ColetaItens();
	void DescreveItem();

	void Clica(FXButton *, int);
	bool Move(cObjeto *obj_orig, int lugar_orig, cObjeto *obj_tgt, int lugar_tgt);
	public:

	long onVoltar(FXObject*,FXSelector,void*);

	long onInv(FXObject*,FXSelector,void*);
	long onSkills(FXObject*,FXSelector,void*);
	long onChao(FXObject*,FXSelector,void*);

	long onCabeca(FXObject*,FXSelector,void*);
	long onCorpo(FXObject*,FXSelector,void*);
	long onMaoDir(FXObject*,FXSelector,void*);
	long onMaoEsq(FXObject*,FXSelector,void*);
	
	winEquip(FXWindow *owner, FXIcon *icoMain, class cJogo *);
	virtual ~winEquip();
	virtual void create();
};


#endif