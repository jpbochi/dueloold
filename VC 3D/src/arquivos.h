
#ifndef ARQUIVOS_H
#define ARQUIVOS_H

#include "fx.h"

namespace ARQ {
	extern const char *IconeMain;
	extern const char *PessoaEquip;
	extern const char *IconeQ1;
	extern const char *IconeQ2;
	extern const char *IconeQ3;
};

bool ExisteArquivo(FXApp *app, FXString arq, bool erro=true);
bool ExisteArquivo(FXApp *app, const char *arq, bool erro=true);
bool ExistemArquivos(FXApp *app);

#endif