
#ifndef CONFIG_H
#define CONFIG_H

#include "fx.h"

class cConfig
{
public:
	enum{
		TEC_CAM_FRENTE = 0,
		TEC_CAM_TRAS,
		TEC_CAM_APROX,
		TEC_CAM_AFASTA,
		TEC_CAM_ESQ,
		TEC_CAM_DIR,
		TEC_CAM_CIMA,
		TEC_CAM_BAIXO,
		TEC_CENTRA_MOUSE,
		TEC_CAM_ANDAR_SOBE,
		TEC_CAM_ANDAR_DESCE,
		TEC_CAM_FILTRO_DE_ANDARES,
		TEC_CAM_ESCONDE_PAREDES,
		TEC_COUNT
	};
	
	FXuint teclas[TEC_COUNT];
	double mouse_sens;

	cConfig();	
};

#endif