
#ifndef WINMAIN_H
#define WINMAIN_H

#include "fx.h"
#include "fx3d.h"

#include "config.h"

//em ms (35 => 28,571428571428571428571428571429 por s)
const FXuint TIMER_FPS			= 512;
const FXuint FPS_AMOSTRAS		= 6;
const FXuint TIMER_INPUT		= 30;
const FXuint TEMPO_MOUSE_PARADO = 90;

class winMain : public FXMainWindow {

	FXDECLARE(winMain)

protected:
	class winNovoJogo *winNJ;
	class winEquip	  *winEq;

	FXIcon *icoMain;

	FXSplitter	*splitter;
	FXVerticalFrame *frmTemp;

	FXGLCanvas	*glcanvas;                  // GL Canvas to draw into
	FXGLVisual	*glvisual;                  // OpenGL visual
	
	FXMenuPane * filemenu;
	FXMenuPane * editmenu;
	FXMenuPane * subeditmenu;

	//FPS
	double	fps;
	FXuint	fps_amostra;
	FXuint	cont_frames[FPS_AMOSTRAS];
	//FXlong	tempo_amostra[FPS_AMOSTRAS];
	FXLabel	*lblFPS;

	//Teclado e Mouse
	cConfig Config;
	FXuint tecl_state, teclas[cConfig::TEC_COUNT], mouse_state;
	FXshort mouse_wheel;
	FXbool mouse_incanvas, mouse_centrado, moveumouse;
	
	double		 rot_z;
	FXDataTarget target_rot_z;
	FXDial		 *dial_rot_z;
	
	winMain(){}

public:
	//Jogando
	FXVerticalFrame *frmJogo;
	FXLabel *lblNome, *lblArma, *lblPoder;
	FXLabel *lblHT, *lblTempo;
	FXProgressBar *barHT, *barTempo;
	FXLabel *lblAndar;
	FXButton *butEquip, *butPassarVez;
	FXButton *butMirar, *butMirarMais, *butAtirar;//, *butRecarregar, *butTrocar;
	FXButton *butMaoDir, *butMaoEsq;
	
	enum{
		ID_BUTTON=FXMainWindow::ID_LAST,
		//constantes internas
		ID_CANVAS,
		ID_CHORE,
		ID_TIME_FPS,
		ID_TIME_INPUT,
		ID_TIME_ESPERA,
		ID_OPENGL,
		//Menu
		ID_NOVOJOGO,
		ID_ABRIRMAPA,
		ID_SALVARMAPA,
		ID_PREF,
		ID_ESTATISTICAS,
		ID_OPCOES,
		ID_SOBRE,
		//Jogando
		ID_EQUIP,
		ID_PASSARVEZ,
		ID_MIRAR,
		ID_MIRARMAIS,
		ID_ATIRAR,
		//ID_RECARREGAR,
		//ID_TROCAR,
		ID_MAODIR,
		ID_MAOESQ,
		//bobeiras
		ID_SPIN,
		ID_SPINFAST,
		ID_STOP
	};
	
	long onCmdOpenGLPressed(FXObject*,FXSelector,void*);
	long onChore(FXObject*,FXSelector,void*);
	long onExpose(FXObject*,FXSelector,void*);
	long onResize(FXObject*,FXSelector,void*);
	long onFocusIn(FXObject*,FXSelector,void*);
	long onFocusOut(FXObject*,FXSelector,void*);
	
	void AtualizaMarca(int mousex, int mousey);
	long onTimeFPS(FXObject*,FXSelector,void*);
	long onTimeInput(FXObject*,FXSelector,void*);
	
	long onMouseInCanvas(FXObject *,FXSelector,void*);
	long onMouseOutCanvas(FXObject *,FXSelector,void*);
	long onMouseMove(FXObject *,FXSelector,void*);
	long onMouseDown(FXObject *,FXSelector,void*);
	long onMouseUp(FXObject *,FXSelector,void*);
	long onMouseWheel(FXObject *,FXSelector,void*);
	long onCmdButtonPressed(FXObject *,FXSelector,void*);

	long onMenuAbrirMapa(FXObject *,FXSelector,void*);
	long onMenuNovoJogo(FXObject *,FXSelector,void*);
		
	long onPassarVez(FXObject *,FXSelector,void*);
	long onEquip(FXObject *,FXSelector,void*);
	long onMirar(FXObject *,FXSelector,void*);
	long onMirarMais(FXObject *,FXSelector,void*);
	long onAtirar(FXObject *,FXSelector,void*);
	//long onRecarregar(FXObject *,FXSelector,void*);
	//long onTrocar(FXObject *,FXSelector,void*);
	long onMaoDir(FXObject *,FXSelector,void*);
	long onMaoEsq(FXObject *,FXSelector,void*);
	
	void CentraMouse(FXint *dx = 0, FXint *dy = 0);
	void LiberaTeclas();
	
	void IniciaTimerEspera(int milisec, void*ptr);
	long onTimerEspera(FXObject *,FXSelector,void*);

	void drawScene();
	
	bool CarregaModelos();
	
	void MostraIcones();

	winMain(FXApp *);
	virtual ~winMain();
	virtual void create();

};


#endif