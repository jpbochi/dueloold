
#include "jogo@.h"

cJogo::cJogo()
{
	Estado = EST_INTRO;
	JogoComecou = false;
	Console = false;
	modocam = MODOCAM_NORMAL;
	Zoom = 1;
	
	pMapa = &Mapa;
	numjogs = 2;
	J = NULL;
	Armas = NULL;
	Municoes = NULL;
	CarregaArmasPadrao();
	
	vz = -1;
	JaVi = NULL;
	JaViHol = NULL;
	TempoParado = false;
	enuComando = CmdNada;
	
	NaoAnimando = true;
	pIc = NULL;

	fundopreto = false;
}

void cJogo::Reinicia()
{
	MudaEstado(EST_INTRO);

	delete [numjogs] J;
	delete [numjogs] JaVi;
	delete [numjogs] JaViHol;

	cJogo();
}

void cJogo::Reescreve()
{
	//char *tmp = new char[32];
	//char *tmp2 = new char[32];
	FXString str, str2;
	
	//andar
	if (pMapa != NULL) {
		char filtro = '?';
		switch (pMapa->FiltroDeAndares) {
			case 0: filtro = '�'; break;
			case 1: filtro = '|'; break;
			case 2: filtro = '�'; break;
		}
		pwinMain->lblAndar->setText(str.format("%d� andar [%c]", pMapa->selectedZ + 1, filtro));
	}
	//Nome
	pwinMain->lblNome->setText(J[vz].Nome);
	//Poder
	J[vz].PoderStr(str);
	pwinMain->lblPoder->setText(str);
	//Arma
	bool mostrabotoes = false;
	/*
	short armamao = J[vz].ArmaMao;
	if (armamao != -1) {
		if (Jogo.Armas[armamao].Dano == 0) {
			str.format("%s", Jogo.Armas[armamao].Nome);
		} else {
			if (armamao == aBolaDeFogo) { //bola de fogo gasta mana
				str2.format("%d", J[vz].ManaFe);
			} else {
				str2.format("%d|%d", J[vz].Mun[armamao], J[vz].MunExtra[armamao]);
			}
			if (J[vz].GetFlag(Mirando)) {
				str.format("%.1f - %s - %s", J[vz].MiraAtual * J[vz].MiraBonus,
											 Jogo.Armas[armamao].Nome,
											 str2);
				mostrabotoes = true;
			} else {
				str.format("%s - %s", Jogo.Armas[armamao].Nome, str2);
				mostrabotoes = true;
			}
		}
	/*/
	cObjeto* armamao = J[vz].ArmaMao();
	if (armamao != 0) {
		ushort codarma = armamao->GetCod();
		ushort codmun = armamao->codmunicao;
		if (codmun == mSemMunicao) {
			str.format("%s", Jogo.Armas[codarma].Nome);
		} else if (Jogo.Municoes[codmun].Dano == 0) {
			str.format("%s", Jogo.Armas[codarma].Nome);
		} else {
			if (armamao->UsaManaFe()) { //bola de fogo gasta mana
				str2.format("%d", J[vz].ManaFe);
			} else {
				str2.format("%d", armamao->municao);
			}
			if (J[vz].GetFlag(Mirando)) {
				str.format("%.1f - %s - %s", J[vz].MiraAtual * J[vz].MiraBonus,
											 Jogo.Armas[codarma].Nome,
											 str2);
				mostrabotoes = true;
			} else {
				str.format("%s - %s", Jogo.Armas[codarma].Nome, str2);
				mostrabotoes = true;
			}
		}
	/**/
	} else {
		str.format("");
	}
	pwinMain->lblArma->setText(str);
	if (mostrabotoes) {
		pwinMain->butMirar->enable();
		//pwinMain->butRecarregar->enable();
	} else {
		pwinMain->butMirar->disable();
		//pwinMain->butRecarregar->disable();
	}
	if (mostrabotoes && J[vz].GetFlag(Mirando)) {
		pwinMain->butMirarMais->enable();
		pwinMain->butAtirar->enable();
		pwinMain->butMirar->setTipText("Trocar alvo");
	} else {
		pwinMain->butMirarMais->disable();
		pwinMain->butAtirar->disable();
		pwinMain->butMirar->setTipText("Escolher alvo");
	}
	//Resist�ncia
	pwinMain->lblHT->setText(str.format("%.1f/%.1f", J[vz].Ht, J[vz].HtMax));
	pwinMain->barHT->setTotal(   (FXuint)floor(J[vz].HtMax * 100));
	pwinMain->barHT->setProgress((FXuint)floor(J[vz].Ht    * 100));
	
	//Tempo
	pwinMain->lblTempo->setText(str.format("%d/%d", J[vz].T, J[vz].TMax));
	pwinMain->barTempo->setTotal(   J[vz].TMax * 100);
	pwinMain->barTempo->setProgress(J[vz].T    * 100);
	
	//M�os
	pIc->PreencheBut(pwinMain->butMaoDir, J[vz].objMaoDir);
	pIc->PreencheBut(pwinMain->butMaoEsq, J[vz].objMaoEsq);
	pwinMain->MostraIcones();
	
	//TO DO: escrever os tempos para as a��es nos bot�es (ou n�o)

	//TO DO: esconder/mostrar os bot�es que podem (ou n�o) serem usados
	
	//pwinMain->frmJogo->forceRefresh();
}

void cJogo::CarregaArmasPadrao()
{
	Municoes = new cMunicao[NUM_MUN];
	#define MBF ManaBolaDeFogo
	//      ID										NOME        Mun  MunR DCam NPro Dano Expl Degn  Alc ArmaDest
	Municoes[mPistola].Inicia(mPistola,				"AP",		12,  12,  1.5, 1,   20,  0,   0,    0, aPistola);
	Municoes[mEspingarda].Inicia(mEspingarda,		"Shred",	8,   1,   2.5, 5,   15,  0,   0,    0, aEspingarda);
	Municoes[mMetralhadora].Inicia(mMetralhadora,	"AP",		30,  30,  2,   1,   30,  0,   0,    0, aMetralhadora);
	Municoes[mLancaFog].Inicia(mLancaFog,			"HE",		1,   1,   2.5, 1,   80,  3.5, 0,    0, aLancaFog);
	Municoes[mRifle].Inicia(mRifle,					"AP",		10,  10,  1.5, 1,   50,  0,   0,    0, aRifle);
	Municoes[mDegen].Inicia(mDegen,					"Degen",	5,   5,   1.2, 1,   10,  0,   0.6,  0, aDegen);
	Municoes[mDisco].Inicia(mDisco,					"Corte",	1,   -1,  1.1, 1,   50,  0,   0.2,  0, aDisco);
	Municoes[mTriLaser].Inicia(mTriLaser,			"AP",		64,  32,  2,   1,   50,  0,   0,    0, aTriLaser);
	Municoes[mMiniGun].Inicia(mMiniGun,				"HE",		20,  20,  2.5, 1,   40,  2,   0,    0, aMiniGun);
	Municoes[mLancaChamas].Inicia(mLancaChamas,		"Incend",	10,  10,  3,   3,   13,  0,   0.15, 4, aLancaChamas);
	Municoes[mRicochet].Inicia(mRicochet,			"Ric-AP",	15,  15,  1.5, 1,   20,  0,   0,    0, aRicochet);
	Municoes[mRoboArma].Inicia(mRoboArma,			"AP",		50,  50,  2,   1,   25,  0,   0,    0, aRoboArma);
	Municoes[mParalisante].Inicia(mParalisante,		"Paral",	6,   6,   1.3, 1,   0,   0,   0,    0, aParalisante);
	Municoes[mBaforada].Inicia(mBaforada,			"Incend",	1,   0,   4,   3,   25,  0,   0.1,  10,aBaforada);
	Municoes[mTeletrans].Inicia(mTeletrans,			"Teletrans",15,  15,  2,   1,   5,   0,   0,    0, aTeletrans);
	Municoes[mBlaster].Inicia(mBlaster,				"HE",		1,   1,   2.5, 1,   140, 5,   0,    0, aBlaster);
	Municoes[mEspinhos].Inicia(mEspinhos,			"Shred",	200, -1,  1.4, 3,   20,  0,   0,    18,aEspinhos);
	Municoes[mMG34].Inicia(mMG34,					"AP",		100, 100, 3,   1,   30,  0,   0,    0, aMG34);
	Municoes[mCanhaoHE].Inicia(mCanhaoHE,			"HE",		30,  0,   5,   1,   90,  2.5, 0,    0, aCanhao);
	Municoes[mCanhaoAP].Inicia(mCanhaoAP,			"AP",		30,  0,   5,   1,   180, 0,   0,    0, aCanhao);
	Municoes[mBolaDeFogo].Inicia(mBolaDeFogo,       "HE-Incend",MBF, 0,   1.2, 1,   60,  4,   0.1,  0, aBolaDeFogo);
	Municoes[mShuriken].Inicia(mShuriken,			"AP",		15,  -1,  1,   1,   20,  0,   0,    10,aShuriken);
	Municoes[mTrabuco].Inicia(mTrabuco,				"Shred",	1,   1,   3,   9,    8,  0,   0,    0, aTrabuco);
	#undef MBF
	
	Armas = new cArma[NUM_ARMAS];
	//*
	//    ID									NOME                  MunE Golp MIni MMax Mira TMir TAti TRec CTir CMov MUN SOM
	Armas[aPistola].Inicia(aPistola,			"Pistola",            2,   0,   4,   16,  1.4, 6,   12,  15,  .8,  .75, mPistola, SOM_TIRO);
	Armas[aEspingarda].Inicia(aEspingarda,		"Espingarda",         1,   0,   7,   10,  1.2, 10,  20,  10,  .9,  .9,  mEspingarda, SOM_ESPING);
	Armas[aMetralhadora].Inicia(aMetralhadora,	"Metralhadora",       2,   0,   3,   15,  1.8, 10,  8,   30,  .85, .85, mMetralhadora, SOM_MET);
	Armas[aLancaFog].Inicia(aLancaFog,			"Lan�a-Foguetes",     3,   0,   9,   25,  1.6, 20,  15,  30,  0,   0,   mLancaFog, SOM_FOGUETE);
	Armas[aRifle].Inicia(aRifle,				"Rifle",              2,   0,   2,   60,  2.2, 15,  20,  25,  0,   0,   mRifle, SOM_TIRO);
	Armas[aDegen].Inicia(aDegen,				"Degeneradora",       3,   0,   4,   14,  1.3, 8,   10,  25,  .75, .75, mDegen, SOM_TIRO);
	Armas[aDisco].Inicia(aDisco,				"Disco",              1,   0,   6,   8,   1.1, 5,   10,  10,  0,   1,   mDisco, SOM_NENHUM);
	Armas[aTriLaser].Inicia(aTriLaser,			"Tri-Laser",          2,   0,   5,   23,  1.6, 15,  15,  25,  .95, .7,  mTriLaser, SOM_LASER);
	Armas[aMiniGun].Inicia(aMiniGun,			"Mini Gun",           1,   0,   3,   12,  1.6, 15,  8,   30,  .8,  .6,  mMiniGun, SOM_MG);
	Armas[aLancaChamas].Inicia(aLancaChamas,	"Lan�a-Chamas",       1,   0,   7,   9,   1.2, 15,  15,  35,  1,   1,   mLancaChamas, SOM_BAFORADA);
	Armas[aRicochet].Inicia(aRicochet,			"Ricocheteadora",     2,   0,   4,   14,  1.3, 6,   8,   25,  .75, .75, mRicochet, SOM_TIRO);
	Armas[aRoboArma].Inicia(aRoboArma,			"Robo-Arma",          2,   0,   8,   40,  1.7, 15,  10,  35,  .99, 0,   mRoboArma, SOM_MET);
	Armas[aParalisante].Inicia(aParalisante,	"Paralisante",        1,   0,   4,   16,  1.4, 8,   10,  25,  .75, .75, mParalisante, SOM_TIRO);
	Armas[aBaforada].Inicia(aBaforada,			"Baforada",           0,   0,   8,   10,  1.1, 8,   20,  -1,   1,   1,   mBaforada, SOM_BAFORADA);
	Armas[aTeletrans].Inicia(aTeletrans,		"Teletrans",		  1,   0,   4,   14,  1.3, 6,   8,   25,  .85, .85, mTeletrans, SOM_LASER);
	Armas[aBlaster].Inicia(aBlaster,			"LMT-UltraBlaster",   2,   0,   10,  35,  1.6, 65,  25,  30,  0,   0,   mBlaster, SOM_FOGUETE);
	Armas[aMachado].Inicia(aMachado,			"Machado",            0,   34,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	Armas[aEspinhos].Inicia(aEspinhos,			"Espinhos",           0,   0,   8,   13,  1.2, 6,   25,  0,   .95, .95, mEspinhos, SOM_NENHUM);
	Armas[aMG34].Inicia(aMG34,					"MG-34",              1,   0,   4,   15,  1.4, 18,  5,   40,  .85, .7,  mMG34, SOM_MG);
	Armas[aCanhao].Inicia(aCanhao,				"Canh�o",             30,  0,   8,   45,  1.7, 25,  20,  -1,  .9,  .7,  mCanhaoHE, SOM_CANHAO);
	Armas[aBolaDeFogo].Inicia(aBolaDeFogo,		"Bola de Fogo",       0,   0,   15,  60,  1.4, 15,  15,  0,   0,   1,   mBolaDeFogo, SOM_NENHUM);
	Armas[aSabreDeLuz].Inicia(aSabreDeLuz,		"Sabre de Luz",       0,   44,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	Armas[aShuriken].Inicia(aShuriken,			"Estrelinhas",        0,   0,   7,   9,   1.1, 6,   6,   6,   0,   1,   mShuriken, SOM_NENHUM);
	Armas[aKatana].Inicia(aKatana,				"Katana",             0,   38,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	Armas[aSoco].Inicia(aSoco,					"Soco",               0,   -.3, 0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_SOCO);
	Armas[aTrabuco].Inicia(aTrabuco,			"Trabuco",            9,   0,   5,   9,   1.2, 10,  10,  50,  0,   .9,  mTrabuco, SOM_ESPING);
	//Armas[aDedoDuro].Inicia(aDedoDuro,		"Dedo Duro(Medo)",    0,   0,   10,  20,  1.4, 5,   25,  0,   1,   1,   mDedoDuro, SOM_NENHUM);
	Armas[aSnikt].Inicia(aSnikt,				"Snikt",              0,   42,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	/*/
	//              ID                  NOME                  Mun  MunR MunE DCam NPro Dano Expl Degn  Alc Golp MIni MMax Mira TMir TAti TRec CTir CMov SOM
	Armas[0].Inicia(aPistola,			"Pistola",            12,  12,  24,  1.5, 1,   20,  0,   0,    0,  0,   4,   16,  1.4, 6,   12,  25,  .75, .75, SOM_TIRO);
	Armas[1].Inicia(aEspingarda,		"Espingarda",         8,   1,   8,   2.5, 5,   15,  0,   0,    0,  0,   7,   10,  1.2, 10,  20,  15,  .9,  .9,  SOM_ESPING);
	Armas[2].Inicia(aMetralhadora,		"Metralhadora",       30,  30,  60,  2,   1,   30,  0,   0,    0,  0,   3,   15,  1.8, 10,  8,   30,  .85, .85, SOM_MET);
	Armas[3].Inicia(aLancaFoguetes,		"Lan�a-Foguetes",     1,   1,   3,   2.5, 1,   80,  3.5, 0,    0,  0,   9,   25,  1.6, 20,  15,  30,  0,   0,   SOM_FOGUETE);
	Armas[4].Inicia(aRifle,				"Rifle",              10,  10,  20,  1.5, 1,   50,  0,   0,    0,  0,   2,   60,  2.2, 15,  20,  25,  0,   0,   SOM_TIRO);
	Armas[5].Inicia(aDegeneradora,		"Degeneradora",       5,   5,   15,  1.2, 1,   10,  0,   0.6,  0,  0,   4,   14,  1.3, 8,   10,  25,  .75, .75, SOM_TIRO);
	Armas[6].Inicia(aDisco,				"Disco",              1,   1,   1,   1.1, 1,   50,  0,   0.2,  0,  0,   6,   8,   1.1, 5,   10,  10,  0,   0,   SOM_NENHUM);
	Armas[7].Inicia(aTriLaser,			"Tri-Laser",          64,  32,  128, 2,   1,   50,  0,   0,    0,  0,   5,   23,  1.6, 15,  15,  25,  .95, .7,  SOM_LASER);
	Armas[8].Inicia(aMiniGun,			"Mini Gun",           20,  20,  20,  2.5, 1,   40,  2,   0,    0,  0,   3,   12,  1.6, 15,  8,   30,  .8,  .6,  SOM_MG);
	Armas[9].Inicia(aLancaChamas,		"Lan�a-Chamas",       10,  10,  10,  3,   3,   13,  0,   0.15, 4,  0,   7,   9,   1.2, 15,  15,  50,  1,   1,   SOM_BAFORADA);
	Armas[10].Inicia(aRicocheteadora,	"Ricocheteadora",     15,  15,  30,  1.5, 1,   20,  0,   0,    0,  0,   4,   14,  1.3, 6,   8,   25,  .75, .75, SOM_TIRO);
	Armas[11].Inicia(aRoboArma,			"Robo-Arma",          50,  50,  100, 2,   1,   25,  0,   0,    0,  0,   8,   40,  1.7, 15,  10,  35,  .99, 0,   SOM_MET);
	Armas[12].Inicia(aParalisante,		"Paralisante",        6,   6,   6,   1.3, 1,   0,   0,   0,    0,  0,   4,   16,  1.4, 8,   10,  25,  .75, .75, SOM_TIRO);
	Armas[13].Inicia(aBaforada,			"Baforada",           1,   0,   0,   4,   3,   25,  0,   0.1,  10, 0,   8,   10,  1.1, 8,   20,  0,   1,   1,   SOM_BAFORADA);
	Armas[14].Inicia(aTeletrans,		"Teletrans",		  15,  15,  15,  2,   1,   5,   0,   0,    0,  0,   4,   14,  1.3, 6,   8,   25,  .85, .85, SOM_LASER);
	Armas[15].Inicia(aBlaster,			"LMT-UltraBlaster",   1,   1,   2,   2.5, 1,   140, 5,   0,    0,  0,   10,  35,  1.6, 65,  25,  30,  0,   0,   SOM_FOGUETE);
	Armas[16].Inicia(aMachado,			"Machado",            0,   0,   0,   0,   0,   0,   0,   0,    0,  34,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	Armas[17].Inicia(aEspinhos,			"Espinhos",           200, 0,   0,   1.4, 3,   20,  0,   0,    18, 0,   8,   13,  1.2, 6,   25,  0,   .95, .95, SOM_NENHUM);
	Armas[18].Inicia(aMG34,				"MG-34",              100, 100, 100, 3,   1,   30,  0,   0,    0,  0,   4,   15,  1.4, 18,  5,   40,  .85, .7,  SOM_MG);
	Armas[19].Inicia(aCanhaoHE,			"Canh�o (HE)",        1,   1,   30,  5,   1,   90,  2.5, 0,    0,  0,   8,   45,  1.7, 25,  20,  40,  .9,  .7,  SOM_CANHAO);
	Armas[20].Inicia(aCanhaoAP,			"Canh�o (AP)",        1,   1,   30,  5,   1,   180, 0,   0,    0,  0,   8,   45,  1.7, 25,  20,  40,  .9,  .7,  SOM_CANHAO);
	Armas[21].Inicia(aBolaDeFogo, "Bola de Fogo",ManaBolaDeFogo,   0,   -1,  1.2, 1,   60,  4,   0,    0,  0,   15,  60,  1.4, 15,  15,  0,   0,   0,   SOM_NENHUM);
	Armas[22].Inicia(aSabreDeLuz,		"Sabre de Luz",       0,   0,   0,   0,   0,   0,   0,   0,    0,  44,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	Armas[23].Inicia(aShuriken,			"Estrelinhas",        1,   1,   15,  1,   1,   20,  0,   0,    10, 0,   7,   9,   1.1, 6,   6,   6,   0,   0,   SOM_NENHUM);
	Armas[24].Inicia(aKatana,			"Katana",             0,   0,   0,   0,   0,   0,   0,   0,    0,  38,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	Armas[25].Inicia(aSoco,				"Soco",               0,   0,   0,   0,   0,   0,   0,   0,    0,  -.3, 0,   0,   0,   0,   0,   0,   0,   0,   SOM_SOCO);
	Armas[26].Inicia(aTrabuco,			"Trabuco",            1,   1,   9,   3,   9,   8,   0,   0,    0,  0,   5,   9,   1.2, 10,  10,  20,  0,   .9,  SOM_ESPING);
	Armas[27].Inicia(aDedoDuro,			"Dedo Duro(Medo)",    999, 0,   0,   3,   1,   1,   0,   0,    .1, 0,   10,  20,  1.4, 5,   25,  0,   1,   1,   SOM_NENHUM);
	Armas[28].Inicia(aSnikt,			"Snikt",              0,   0,   0,   0,   0,   0,   0,   0,    0,  42,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	/**/
	//TO DO: efeitos diferenciados das armas
	//X disco e estrelinhas > n�o � necess�rio se virar na dire��o do 'tiro'
	//X espingarda > regarrega de um em um cartucho
	//X canh�o do tanque, baforada > recarregam sozinhos a cada in�cio de rodada
	//X bola de fogo > gasta mana, i. e., n�o tem uma muni��o separada
}

void cJogo::Click(ushort mx, ushort my, ushort mz, double md, int button)
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//MODIF: o c�digo para posicionar os pontos do blaster foi retirado
	//TO DO: implementar posicionamento dos pontos do blaster

	if (button & RIGHTBUTTONMASK && enuComando != CmdNada) {
		//bot�o direto cancela qualquer comando, e vira quando n�o houver comando
		enuComando = CmdNada;
	} else {
		switch (enuComando) {
			case CmdMirar:
				if (Mira(mx, my, mz, md)) enuComando = CmdNada;
			break;
			case CmdJediPega:
				//
			break;
			case CmdArremessar:
				//
			break;
			case CmdControlar:
				//
			break;
			case CmdIdentificar:
				//
			break;
			case CmdHolografar:
				//
			break;
			case CmdAnimarMortos:
				//
			break;
			case CmdRelampago:
				//
			break;
			case CmdDardosMisticos:
				//
			break;
			default:
				if (md > 0) { //s� trata se o cara clicou em algum lugar no mapa
					bool sovirar = (button & RIGHTBUTTONMASK) ? true : false;
					bool strafe = (button & ALTMASK) ? true : false;
					Movimenta(mx, my, mz, sovirar, strafe);
				}
			break;
		}
	}
	Reescreve();
}

void cJogo::Movimenta(int mx, int my, int mz, bool sovirar, bool strafe)
{
	if (J[vz].Posicao == posNaMoto && sovirar) {
		//n�o d� pra s� virar de moto
		return;
	} else if (J[vz].Posicao == posEnterrado) {
		MsgDef("Voc� tem que sair do buraco antes!");
		return;
	} else if (J[vz].GetFlag(Imovel)) {
		MsgDef("Voc� foi imobilizado!");
		return;
	} else if (J[vz].GetFlag(Mirando) && strafe) {
		if (Armas[J[vz].ArmaMaoCod()].CoiceMove == 0) {
			MsgDef("N�o � poss�vel andar sem perder a mira com esta arma!");
			return;
		}
	}

	int dx = mx - J[vz].x;
	int dy = my - J[vz].y;
	int dz = mz - J[vz].z;
	short proxFrente = J[vz].Frente;
	bool podeandar = false;

	if (sovirar) strafe = false; //n�o faz sentido virar no strafe

	if (sovirar && ((dx != 0) || (dy != 0))) {
		/*
		double angteta = funAngTetai(J[vz].x, J[vz].y, mx, my);
		proxFrente = floor(angteta / (PI/6));
		if (proxFrente < 0) proxFrente += 12;/*/
		proxFrente = Direcao(J[vz].x, J[vz].y, mx, my);/**/
	} else {
		if (dz == 0) {
			if ((dx == 0) && (dy == 0)) return;
			if ((abs(dx) > 1) || (abs(dy) > 1)) return;
			if (J[vz].x % 2 == 0) {
				     if ((dx ==  1) && (dy ==  0)) proxFrente = 1;
				else if ((dx ==  1) && (dy ==  1)) return;
				else if ((dx ==  0) && (dy ==  1)) proxFrente = 3;
				else if ((dx == -1) && (dy ==  1)) return;
				else if ((dx == -1) && (dy ==  0)) proxFrente = 5;
				else if ((dx == -1) && (dy == -1)) proxFrente = 7;
				else if ((dx ==  0) && (dy == -1)) proxFrente = 9;
				else if ((dx ==  1) && (dy == -1)) proxFrente = 11;
			} else {
				     if ((dx ==  1) && (dy ==  0)) proxFrente = 11;
				else if ((dx ==  1) && (dy ==  1)) proxFrente = 1;
				else if ((dx ==  0) && (dy ==  1)) proxFrente = 3;
				else if ((dx == -1) && (dy ==  1)) proxFrente = 5;
				else if ((dx == -1) && (dy ==  0)) proxFrente = 7;
				else if ((dx == -1) && (dy == -1)) return;
				else if ((dx ==  0) && (dy == -1)) proxFrente = 9;
				else if ((dx ==  1) && (dy == -1)) return;
			}
			podeandar = true;
		} else if (dz == 1) {//subir
			cChao contchao = Mapa.Cont(mx, my, mz, cMapa::CHAO);

			if ((PodVoa(vz) || contchao.EhChaoElevador()) && J[vz].Posicao == posNormal) {
				proxFrente = J[vz].Frente;
				podeandar = true;
			}
		} else if (dz == -1) {//descer
			cChao contchao = Mapa.Cont(mx, my, mz + 1, cMapa::CHAO);

			if ((contchao.EhChaoBuraco() || contchao.EhChaoElevador()) && J[vz].Posicao == posNormal) {
				proxFrente = J[vz].Frente;
				podeandar = true;
			}
		}
	}
	if (J[vz].Posicao == posRastejando
	||  J[vz].Posicao == posNaMoto
	||  PodNaoUsaStrafe(vz)) {
		strafe = false;
	}
	if (PodAndaDeRe(vz)									//Se o cara for algu�m que anda de r� (Tanque e Michael Jackson)
	&&  !sovirar										//e querer andar
	&&  ((J[vz].Frente - proxFrente + 12) % 12 == 6)){	//para tr�s
		strafe = true;									//ent�o ele anda de r�
	}

	//Testes para possibilitar que o cara ande sem perder a mira
	bool strafemirando = false;
	if (!strafe && !PodNaoPerdeMiraAoAndar(vz)) {
		J[vz].SetFlag(Mirando, false); //perde a mira
	} else if (strafe && J[vz].GetFlag(Mirando)) {
		//strafe com mira sigfica andar sem perder a mira
		strafemirando = true;
		//ele tem que continuar olhando para o alvo depois de andar
		//BUG: � poss�vel que o cara n�o tenha tempo para andar depois de ter virado, assim ele vai estar olhando numa dire��o errada
		/*
		proxFrente = Direcao(mx, my, J[vz].AlvoX[0], J[vz].AlvoY[0]);
		/*/
		proxFrente = Direcao(J[vz].AlvoTeta[0]);//(short)floor(0.5 + (J[vz].AlvoTeta[0] / (-30.0 * PI_180)));
		/**/
	}
	
	//Testa se o cara vai se virar antes de andar
	if ((proxFrente != J[vz].Frente) && (!strafe || strafemirando)) {
		if (J[vz].Posicao == posNaMoto
		//&& ((J[vz].Frente - proxFrente + 12) % 12 > 1)) {
		&& DifAngb(J[vz].Frente, proxFrente, 1)) {
			return; //o cara n�o pode fazer curvas bruscas com a moto
		} else {
			short angpermitido = (!sovirar &&
								  !strafemirando &&
								  J[vz].Posicao != posNaMoto &&
								  !PodNaoUsaStrafe(vz)) ? 1 : 0;
			
			if (!Vira(proxFrente, angpermitido) && J[vz].Posicao != posNaMoto) {
				return; //se o cara n�o terminou de virar (viu alguem ou acabou o tempo), n�o continua andando no m�s
			}
		}
	} else if (strafe) {
		//testa se o cara pracisava mesmo do strafe ou n�o
		short angpermitido = (!strafemirando) ? 1 : 0;
		if (DifAngb(J[vz].Frente, proxFrente, angpermitido)) strafe = false;
	}
	if (sovirar || !podeandar) {
		return; //ele s� queria virar OU apertou onde n�o pode ir
	}

	//MODIF.: Havia um teste aqui para evitar que o cara sa�sse do mapa
	//        Agora � imposs�vel clicar fora do mapa
	//        Se eu ler de novo este coment�rio, acho que devo apag�-lo
	
	if (podeandar) {
		//if (dz == 0) {
			//some com as linhas dos tiros
			contlinhatiro = 0;
			contsombratiro = 0;

			short tempo;
			if (J[vz].Posicao == posNaMoto)				tempo = TempoAndarMoto;
			else if (J[vz].Posicao == posRastejando)	tempo = TempoAndarRast;
			else if (J[vz].Posicao == posAbaixado) {
				if (strafe)								tempo = TempoAndarAbaiStraf;
				else									tempo = TempoAndarAbai;
			} else if (strafe)							tempo = TempoAndarStraf;
			else										tempo = TempoAndar;

			if (PodAndaDevagarAoSerVisto(vz)) {
				for (short i=0; i < numjogs; i++)
				if (!J[i].GetFlag(Morto)) {
					if (EstaSendoVistoPor(i)) {
						tempo *= 3;
						break; //exit for
					}
				}
			}

			bool atropelar = false;
			short atropelado = -1;
			for (short i=0; i < numjogs; i++)
			if (!J[i].GetFlag(Morto)) {
				if (i != vz) {
					if ((mx == J[i].x) &&
						(my == J[i].y) &&
						(mz == J[i].z) &&
						J[i].Posicao != posEnterrado) {
						atropelar = true;
						atropelado = i;
					}
					//TO DO: testar se o cara bateu em um holografia
				}
			}
			
			if (atropelar) {
				if (!EstaVendo(atropelado)) {
					MsgDef("Se voc� n�o sabe, h� algu�m a�!");
					return;
				} else if (strafe && PodAndaDeRe(vz)) { //o tanque pode bater de r�
					MsgDef("Voc� n�o pode bater no strafe!");
					return;
				} else if (J[vz].Posicao == posNaMoto) {
					MsgDef("Voc� n�o pode bater com a sua moto!");
					return;
				} else {
					tempo = 0; //Ele n�o vai andar mesmo; e, se ele for golpear, haver� um teste depois
					if (!Vira(proxFrente, 0)) return;
				}
			}

			if (tempo > J[vz].T) {
				MsgSemTempo("se mover", tempo);
				return;
			}

			cCont cont = Mapa.Cont(mx, my, mz, cMapa::NAO_CHAO);
			cChao contchao = Mapa.Cont(mx, my, mz, cMapa::CHAO);

			if (cont.EhPortaTrancada()) {
				if (J[vz].GetFlag(TemChave)) {
					cont.AbrePorta();
					Mapa.SetCont(mx, my, mz, cont);
				} else if (PodSabeArrombarPortas(vz)) {
					FXuint resp = Msg(MBOX_YES_NO, "Voc� quer tentar arrombar a porta?");
					if (resp == MBOX_CLICKED_YES) {
						J[vz].SetFlag(Mirando, false); //perde a mira

						bool abriu = false;
						if (rnd() < 0.4) {
							abriu = true;
							cont.AbrePorta();
							Mapa.SetCont(mx, my, mz, cont);
							MsgDef("Voc� abriu a porta!");
						} else {
							MsgDef("Voc� n�o conseguiu abrir a porta!");
						}
						J[vz].Replay.GravaFoto(AcaoAbrePorta, num(mx), num(my), num(mz), num(abriu));
						GastaTempo(tempo);
						TestaEncontro(true);
						return;
					} else {
						return; //ele n�o quer tentar...
					}
				} else {
					MsgDef("Voc� precisa de uma chave para abrir portas trancadas!");
					return;
				}
			}

			if (cont.EhLata()) {
				/*TO DO: Traduzir isto:
				Call Explos�o(j(vz).X + dx, j(vz).Y + dy, 0, 0, j(vz).Z, 6, 3.1, 1) 'lata
				Call TestaMortes*/
				//if (J[vz].GetFlag(Morto)) Then Call PassaVez
			} else {
				//TO DO: o T-1000 atravessa grades, mas o equipamento dele n�o. Solu��o: n�o sei

				//aten��o: as pr�ximas linhas simulam um "OU"
				bool podeir = atropelar;
				if (!podeir) podeir = !cont.EhParedeSolida();
				if (!podeir) podeir = PodAtravessaTijolo(vz) && !cont.EhParedePedra() && J[vz].Posicao != posNaMoto;
				if (!podeir) podeir = (PodAtravessaMadeira(vz) || ArmaEhCorpoACorpo(J[vz].ArmaMaoCod())) && cont.EhParedeMadeira() && J[vz].Posicao != posNaMoto;
				if (!podeir) podeir = PodAtravessaGrade(vz) && cont.EhGrade() && J[vz].Posicao != posNaMoto;
				if (podeir) {
					if (J[vz].Posicao == posRastejando && atropelar) {
						MsgDef("Voc� precisa se levantar antes de bater!");
						return;
					}

					//MODIF.: antes o fatasma criava pass. secretas e os caras quebravam as grades
					if (PodQuebraParede(vz) && cont.EhParedeSolida() && !cont.EhParedePedra()) {
						cont.SetFumaca();
						Mapa.SetCont(mx, my, mz, cont);
						/*//TO DO: gravar o som
						ContSons = ContSons + 1
						RepSom(ContSons) = Sons.DestroiParede*/
					} else if (cont.EhParedeMadeira() && (PodAtravessaMadeira(vz) || ArmaEhCorpoACorpo(J[vz].ArmaMaoCod()))) {
						cont.SetFumaca();
						Mapa.SetCont(mx, my, mz, cont);
						/*//TO DO: gravar o som
						ContSons = ContSons + 1
						RepSom(ContSons) = Sons.DestroiParede*/
					}
					if (atropelar) {
						Atropela(atropelado);
					} else {
						if (PodRachaChao(vz) && (mz > 0) && (dz == 0) && contchao.EhChaoMadeira()) {
							if (rnd() < 0.02) {
								contchao.SetChaoBuracoInv();
								Mapa.SetChao(J[vz].x, J[vz].y, J[vz].z, contchao);
							} 
							if (rnd() < 0.15) {
								contchao.SetChaoBuracoInv();
								Mapa.SetChao(mx, my, mz, contchao);
							}
						}
						
						//Anima��o
						if (strafemirando) proxFrente = Direcao(J[vz].x, J[vz].y, mx, my);
						double ang = -(PI/6) * DifAngi(J[vz].Frente, proxFrente);
						NaoAnimando = false;
						J[vz].modelo.AnimInicia(cModeloAnim::ANIM_RUN);
						J[vz].modelo.SetAnimTransl(cos(ang), sin(ang), 0);
						pwinMain->getApp()->runUntil(NaoAnimando);
						J[vz].modelo.AnimInicia(cModeloAnim::ANIM_STAND);
						
						J[vz].x = mx;
						J[vz].y = my;
						J[vz].z = mz;

						//mira piora ao andar
						if (J[vz].GetFlag(Mirando)) {
							short arma = J[vz].ArmaMaoCod();
							J[vz].MiraAtual = Armas[arma].MultMira(J[vz].MiraAtual, Armas[arma].CoiceMove);
						}

						J[vz].Replay.GravaFoto(AcaoMove, num(mx), num(my), num(mz));
						GastaTempo(tempo);
						TestaEncontro(true);
						//OndePisa(vz);
					}
				//} else if (EhParedeSolida(cont) && !EhPortaNormal(cont) && !EhPortaTrancada(cont)) {
				} else if (cont.EhParedeSolida() && cont.EhPortaSecreta()) {
					FXuint resp = Msg(MBOX_YES_NO, "Quer procurar uma passagem secreta?");
					if (resp == MBOX_CLICKED_YES) {
						bool abriu = false;
						if (cont.EhPortaSecreta()) {
							abriu = true;
							cont.AbrePorta();
							Mapa.SetCont(mx, my, mz, cont);
							MsgDef("Voc� abriu a passagem!");
						} else {
							MsgDef("Voc� nao conseguiu abrir a porta!");
						}
						J[vz].Replay.GravaFoto(AcaoAbrePorta, num(mx), num(my), num(mz), num(abriu));
						GastaTempo(tempo);
						TestaEncontro(true);
					}
				}
			}
		/*} else { //voar
			//TO DO: fazer os caras voarem / usarem elevadores / usarem escadas / escalar paredes
		}*/
	}	
}

void cJogo::GastaTempo(short tempo)
{
	//esta fun��o deve ser utilizada sempre que o vz gastar tempo
	//aqui, a��es que dependem do passar do tempo ser�o executadas (ex.: dano por fogo)
	
	J[vz].T -= tempo;
	
	OndePisa(vz, tempo);
}

void cJogo::OndePisa(short quem, short tempo)
{
	if (!Buracos(quem)) {
		cCont cont;
		cont = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::NAO_CHAO);
		//char contchao = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::CHAO);

		if (cont.EhTeletransporte()) {
			//TO DO: Teletransporta (olhar duelo2d)

		//TO DO: else if (pisou em: uma mina; um parasita; sangue alien) {}
		} else {
			double danodefogo = 0;
			if (PodImuneAFogo(quem))		; //apenas evita testar se era fogo ou n�o
			else if (cont.EhFogo())			danodefogo = tempo;//= 20;
			else if (cont.EhFogoPermanente())danodefogo = 1.5 * tempo; //= 30;

			if (danodefogo > 0) {
				double blindagem_a_fogo = PodResistenteAFogo(quem) ? 2 : 0.5;
				blindagem_a_fogo *= tempo / 15.0;
				double dor = CausaDor(quem, danodefogo, J[quem].Blindagem * blindagem_a_fogo);

				if (quem == vz) {
					FXString msg;
					MsgDef(msg.format("Voc� se queimou! (%.1f)", dor));
				}

				if (TestaMortes(vz)) return;
			}
		}
	}
}

bool cJogo::Buracos(short quem)
{
	int quedas = 0;
	
	//quem flutua nunca cai
	if (!PodFlutua(quem)) {
		cChao contchao = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::CHAO);
		cCont cont;

		while (J[quem].z > 0 && (contchao.EhChaoBuraco() || contchao.EhChaoBuracoInv())) {
			quedas++;

			bool quebrouchao = contchao.EhChaoBuracoInv();

			contchao.SetChaoBuraco();
			Mapa.SetChao(J[quem].x, J[quem].y, J[quem].z, contchao);
			
			//quem voa e n�o flutua quebra buracos invis�veis, mas n�o cai
			if (PodVoa(quem) && quebrouchao) {
				if (quem == vz) MsgDef("O ch�o estava fr�gil e voc� o quebrou!");
				return false;
			}

			J[quem].z--;
			J[quem].Fog->selectedZ = J[quem].z;

			contchao = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::CHAO);
			cont = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::NAO_CHAO);

			if (quem == vz) MsgDef("Voc� caiu!");

			for (short i=0; i<numjogs; i++)
			if (!J[i].GetFlag(Morto)) {
				if (quem != i &&
				    J[quem].x == J[i].x &&
					J[quem].y == J[i].y &&
					J[quem].z == J[i].z) {
					double dor1 = CausaDor(i, 20, J[i].Blindagem);		//causa 20
					double dor2 = CausaDor(quem, 10, J[quem].Blindagem); //leva 10
					if (quem == vz) {
						FXString msg;
						MsgDef(msg.format("%s estava no caminho e levou %.1f de dano!\n Voc� levou %.1f!", J[i].Nome, dor1, dor2));
					}
				}
			}

			if (cont.EhLata()) {
				//TO DO: explos�o da lata
			}

			if (quem == vz) {
				/*//TO DO: replay som
				Foto = Foto + 1
				j(vz).RepA��oOQue(Foto) = 4
				j(vz).RepA��oQ1(Foto) = 3
				ContSons = ContSons + 1
				RepSom(ContSons) = Sons.queda*/
				//J[quem].T -= 4;
				J[vz].Replay.GravaFoto(AcaoMove, num(J[vz].x), num(J[vz].y), num(J[vz].z));
				TestaEncontro(true);
			}
			//if (TestaMortes(vz)) return true;
			//if (J[quem].GetFlag(Morto)) return true;
		}
		if (quedas > 0) {
			//s� agora d� o dano de toda a queda
			double dor = CausaDor(quem, 15.0 * pow((double)quedas, 1.5), J[quem].Blindagem);
			FXString msg;
			if (quem == vz) {
				MsgDef(msg.format("O dano total da queda foi %.1f!", dor));
			} else {
				if (quedas == 1) MsgDef(msg.format("%s caiu!", J[quem].Nome));
				else			 MsgDef(msg.format("%s caiu %d andares!", J[quem].Nome, quedas));
			}
			if (TestaMortes(vz)) return true;
			if (!J[quem].GetFlag(Morto)) OndePisa(quem);
		}
	}
	return (quedas > 0);
}

void cJogo::Atropela(short atrop)
{
	bool bate = false;
	short tempo;
	short arma = J[vz].ArmaMaoCod();
	
	//TO DO: o alien pequeno tem que ter op�ao de dar porrada

	if (PodBate(vz) || ArmaEhCorpoACorpo(arma)) {
		tempo = TempoAtropelar;

		if (J[vz].T >= tempo) {
			/*//TO DO: Replay
            Foto = Foto + 1: j(vz).RepA��oOQue(Foto) = 11
            j(vz).RepA��oQ1(Foto) = j(Atrop).X: j(vz).RepA��oQ2(Foto) = j(Atrop).Y
            GravaQuemViu False*/
			//� necess�rio gravar a foto antes porque a v�tima pode se defender e o atque deve aparecer mesmo assim
			J[vz].Replay.GravaFoto(AcaoGolpeia, num(J[atrop].x), num(J[atrop].y), num(J[atrop].z));

			if (!PodImpossibilitaEsquiva(vz)) {
				if (PodDefendeComEspada(atrop)
				&& J[atrop].ArmaMaoCod() == aSabreDeLuz
				&& J[atrop].TProxRodada >= TempoDefesaComEspada
				&& EstaSendoVistoPor(atrop)) {
					//o agredido gasta tempo da pr�xima rodada
					J[atrop].TProxRodada -= TempoDefesaComEspada;

					//o agressor leva dano
					double dor = CausaDor(vz, PodJedi_DanoRetaliacao, J[vz].Blindagem);

					FXString msg;
					MsgDef(msg.format("Ele defendeu o golpe com a espada!\nVoc� levou %.1f de dano!", dor));

					GastaTempo(tempo);
					return;
				} else if (PodSuperEsquiva(atrop)
					   &&  J[atrop].TProxRodada >= TempoEsquivaMatrix
					   &&  EstaSendoVistoPor(atrop)) {
					MsgDef("Ele se esquivou do golpe!");
					
					J[atrop].TProxRodada -= TempoEsquivaMatrix;
					GastaTempo(tempo);
					return;
				}
			}
			
			double dano = 0;
			short som = SOM_NENHUM;
			if (arma != -1) {
				if (ArmaEhCorpoACorpo(arma)) {
					//L� o dano da arma do cara
					dano = Armas[arma].DanoGolpe;
					som = Armas[arma].SomPad;
					if (dano < 0) { //Soco e outras armas cujo dano dependerem da for�a do cara
						dano *= -J[vz].Forca;
					}
				}
			} else {
				dano = J[vz].PodDanoGolpe();
				som = J[vz].PodSomGolpe();
			}

			J[vz].Replay.GravaSom(som);
			/*//TO DO: gravar som
			Som = J[vz].PodSomGolpe();
			ContSons = ContSons + 1
            RepSom(ContSons) = Som
            If RepSom(ContSons) = 0 Then ContSons = ContSons - 1*/

			double dor = CausaDor(atrop, dano, J[atrop].Blindagem);

			if (TestaMortes(vz)) return;

			/*//TO DO: espalhar sangue
			If (Dano > 1.5 Or j(Atrop).Ht = 0) And j(Atrop).Poder <> pTanque And j(Atrop).Poder <> pT_1000 And Atrop <= M Then
				...
			End If*/
			
			if (PodCamuflagemControlavel(vz) && J[vz].Camufl > J[vz].CamuflNat) {
				//qdo o predador ou balacau atacam, perdem a camuflagem
				J[vz].Camufl = J[vz].CamuflNat;
				//drawScene(0);
			}

			GastaTempo(tempo);
			TestaEncontro(true);

			if (J[atrop].GetFlag(Morto) && PodEngole(vz)) {
				//TO DO: engolir (destruir) todo o equipamento que o TestaMortes soltou
				//TO DO: retirar o objeto 'esqueleto' (que o TestaMortes colocou) da frente do tarrasque
				//TO DO: de algum jeito tirar o esqueleto do Fog tb

				J[vz].Ht = Max(J[vz].Ht, J[vz].HtMax / 4.0);
				J[vz].Reg += 0.2f;

				//TO DO: testar se o atropelado era um predador com a autodestrui��o ativada e fazer com que o tarrasque fique com autodestrui��o

				MsgDef("Voc� COMEU um corpo com muitas prote�nas!");
			}
		} else {
			if (PodEngole(vz)) {
				MsgSemTempo("engoli-lo", tempo);
			} else {
				MsgSemTempo("trucid�-lo", tempo);
			}
		}
	} else if (PodIncubacaoAlien(vz)) {
		tempo = TempoIncubacaoAlien;

		if (J[atrop].Bot) {
			MsgDef("Voc� n�o injetar ovos nos bots!");
		} else if (PodImuneIncubacaoAlien(atrop)) {
			MsgDef("Voc� n�o injetar ovos em um tanque!");
		} else if (J[vz].T >= tempo) {
			MsgDef("Voc� injetou um ovo nele!");
			if (!PodImuneOvo(atrop)) {
				J[atrop].Incubacao = 2; //o cara ter� 2 rodadas antes de se transformar num alien
				J[atrop].Injetor = vz;
			}
			J[vz].Ht = 0; //o alien que injetou morre
			if (TestaMortes(vz)) return;
		} else {
			MsgSemTempo("injetar ovos", tempo);
		}
	} else {
		MsgDef("Se voc� n�o sabe, h� algu�m a�!");
	}
}

bool cJogo::Vira(short proxFrente, short angpermitido)
{
	//retorna true se o cara terminou de virar

	short delta, step;
	short tempov;
	short difang = (J[vz].Frente - proxFrente + 12) % 12;
	
	if ((difang <= angpermitido) || (difang >= 12 - angpermitido)) {
		return true; // o cara n�o [pode/quer] virar mais do que isso
	}

	delta = proxFrente - J[vz].Frente;
	if (abs(delta) > 6) {
		step = -delta/abs(delta);
	} else {
		step = delta/abs(delta);
	}

	do {
		if (PodViraDevagar(vz))
			tempov = 12;
		else if (J[vz].Posicao == posRastejando)
			tempov = 12;
		else if (J[vz].Posicao == posAbaixado)
			tempov = 8;
		else
			tempov = 4;

		if (J[vz].T < tempov) {
			MsgSemTempo("se virar", tempov);
			return false;
		}

		J[vz].Frente = (J[vz].Frente + step + 12) % 12;
		GastaTempo(tempov);

		/*if (!J[J[vz].idImit].Bot) { //este if existe porque os bots n�o olhavam em nehuma dire��o
			TO DO: Gravar Foto
			Foto = Foto + 1
			j(vz).RepA��oOQue(Foto) = 2
			j(vz).RepA��oQ1(Foto) = j(vz).Vis�o
		}*/
		J[vz].Replay.GravaFoto(AcaoVira, num(J[vz].Frente));

		//TO DO: anima��o com timer para a rota��o
		Espera(200);

		if (TestaEncontro(false)) { //se o cara ver algu�m, p�ra
			return false;
		}
		difang = (J[vz].Frente - proxFrente + 12) % 12;
	} while ((difang > angpermitido) && (difang < 12 - angpermitido));

	return true;
}

double cJogo::CausaDor(short quem, double dano, double blindagem)//, short causa)
{
	double dor;

	if (quem == vz) MsgDef("BUG?: algo causou dor no J[vz]");

	dor = Aleatorio(dano) - Aleatorio(blindagem);
	if (dor <= 0) {
		dor = 0;
	} else {
		//efeitos estranhos exclusivos de tanques
		if (PodPodeExplodirTanque(quem)) {
			if (dor > Aleatorio(40) || dor >= J[quem].Ht) { //se um tanque recebeu uma dor alta, ou morreu ent�o explode
				J[quem].Ht = 0;
				//TO DO: Explosao
			} else if (dor > 20) { // recebeu dor alta o suficiente para causar efeitos diferentes
				double r = rnd();
				if (r < 0.15) {			//fica imobilizado
					J[quem].SetFlag(Imovel, true);
				} else if (r < 0.25) {	//perde canh�o
					//TO DO: zera a muni��o do canh�o
				} else if (r < 0.35) {	//perge MG
					//TO DO: zera a muni��o da metralhadora
				}			
			}
		}

		//causa a dor
		J[quem].Ht -= dor;

		Sangra(quem);

		//pode explodir a moto
		if (J[quem].Posicao == posNaMoto && rnd() < 0.1 + 0.005 * dor) {
			J[quem].Posicao = posNormal;
			MsgDef("L� se vai uma moto pelos ares!");
			//TO DO: Explosao
		}
		//pode explodir o dinamitador
		if (PodPodeExplodirDinamite(quem) && rnd() < 0.01 * dor) {
			if (!J[quem].GetFlag(DinaExplodiu)) {
				J[quem].SetFlag(DinaExplodiu, true);
				MsgDef("Booooom!!\nO mapa do estado vai ter que ser alterada por causa da explox�o de um dinamitador!");
				//TO DO: Grande Explosao
			}
		}

		//if (causa != -1) TestaMortes(causa);
	}
	return dor;
}

void cJogo::Sangra(short quem, short x, short y, short z)
{
	char sangue = J[quem].PodSangue();
	if (sangue != 0) {
		//se a posi��o n�o foi informada, usar a posi��o do quem
		if (x == -1) x = J[quem].x;
		if (y == -1) y = J[quem].y;
		if (z == -1) z = J[quem].z;

		Mapa.AdObjSimpPos(sangue, x, y, z);
		AtualizaFog();//TO DO: confirmar se isso � necess�rio
	}
}

bool cJogo::TestaMortes(short causamortis)
{
	// causamortis |  significado
	//   >=  0    | algum jogador
	//    = -1    |  degenera��o

	short assassino = -1;
	FXString msg;

	for (short i=0; i<numjogs; i++) {
		if (J[i].Ht <= 0 && !J[i].GetFlag(Morto)) {
			//TO DO: Replay Som de morte (depende do poder do falecido)
			
			J[i].SetFlag(Morto, true);
			J[i].Mortes ++;
			
			J[vz].PodSoltaCorpo(&Mapa);

			//TO DO: solta uma bomba, se for um predador com auto-destrui��o ativada

			//resolve quem foi o assassino
			if (causamortis == -1) {
				//o defunto morreu por degenera��o
				assassino = J[i].AtacanteDegen;
			} else {
				assassino = causamortis;
			}

			//manda mensagens e contabiliza morte para o assassino
			//TO DO: Broadcast (mensagem)
			if (assassino == -1) {
				MsgDef(msg.format("%s morreu sozinho!", J[i].Nome));
			} else if (assassino == i) {
				MsgDef(msg.format("%s se matou!", J[i].Nome));
			} else {
				J[assassino].Kills ++;

				static char *msgpad1 = "%s foi apagado por %s!";
				static char *msgpad2 = "%s foi queimado por %s!";
				if (rnd() < 0.5) {
					MsgDef(msg.format(msgpad1, J[i].Nome, J[assassino].Nome));
				} else {
					MsgDef(msg.format(msgpad2, J[i].Nome, J[assassino].Nome));
				}
			}

			//explode o dinamitador
			if (PodPodeExplodirDinamite(i) && !J[i].GetFlag(DinaExplodiu)) {
				J[i].SetFlag(DinaExplodiu, true);
				MsgDef(msg.format("Booooom!!\nO mapa do estado vai ter que ser alterada por causa da explox�o de um dinamitador!"));
				//TO DO: Grande Explosao
				if (TestaMortes(i)) return true;
				//BUG: o testa mortes acima pode roubar alguma morte do cara que matou o dinamitador
			}

			//ressucitamentos
			if (PodSubstituiBot(i)) {
				//TO DO: procura um Bot para substituir
			} else if (PodRessuscita(i)) {
				if (rnd() < 0.67) {
					//TO DO: ressucita o Jason
				}
			}

			if (J[i].GetFlag(Morto)) {
				//NET: se o jogador morto estiver em outro computador, ele deve poder ver seu replay imediatamente

				J[i].SetFlag(MorreuSemVerRep, true);
			}
		}
	}

	short timevencedor;
	if (FimDeJogo(timevencedor)) {
		//GAME OVER

		//Mostrar o replay para todos os jogadores que ainda n�o viram seu replay
		int ultimo = vz;
		vz = J[ultimo].idProx;
		while (vz != ultimo) {
			//Testa se o cara viu algum replay de algu�m
			bool viualguem = false;
			int op = J[vz].idProx;
			while (op != vz) {
				if (J[op].Replay.ViuAlgo(vz)) {
					viualguem = true;
					break;
				}
				op = J[op].idProx;
			}

			if (viualguem) {
				//Chama o cara
				HUDMostra(false);
				FXString msg;
				Msg(MBOX_OK, msg.format("Venha %s", J[vz].Nome));
				HUDMostra(true);

				pMapa = J[vz].Fog;
				J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);

				//Mostra o replay
				MostraReplay();
			}

			vz = J[vz].idProx;
		}

		if (timevencedor == -1) {
			//todos morreram: empate
			MsgDef("Ningu�m venceu!! Todos moreram!!");
		} else {
			//algu�m sobreviveu: temos um vencedor
			//TO DO: mostrar uma mensagem dizendo quem ganhou (o time todo)
		}

		FXuint resp = Msg(MBOX_YES_NO, "Deseja salvar esta partida nas estat�sticas?");
		if (resp == MBOX_CLICKED_YES) {
			//TO DO: salvar estat�sticas
		}

		Reinicia();

		return true;
	} else {
		return false;
	}
}

bool cJogo::FimDeJogo(short &timevencedor)
{
	timevencedor = -1;
	for (short i=0; i<numjogs; i++) {
		if (!J[i].GetFlag(Morto) && !J[i].Bot) {
			if (J[i].Injetor != -1) {
				//n�o acaba enquanto existir algu�m com alien na barriga
				return false;
			} else if (timevencedor == -1) {
				timevencedor = J[i].Time;
			} else if (timevencedor != J[i].Time) {
				//encontrou dois caras vivos de times diferentes => niguem ganhou ainda
				return false;
			}
		}
	}
	return true; //GAME OVER
}

void cJogo::MostraReplay()
{
	int op = J[vz].idProx;

	while (op != vz) {
		if (J[op].Replay.ViuAlgo(vz)) {
			FXString msg;
			FXuint resp = Msg(MBOX_YES_NO, msg.format("Voc� viu o %s fora da sua vez!\nQuer ver o replay?", J[J[op].idImit].Nome));
			while (resp == MBOX_CLICKED_YES) {
				MudaEstado(EST_VENDOREPLAY);

				//TEMP: por enquanto vou sempre mostrar o replay do pr�ximo cara (isso pode gerar bugs que n�o ir�o existir)

				bool viu = false;
				
				//Come�a a simular o replay do op
				sim.Inicia(vz, op, &J[op].Replay);

				do {
					if (viu) {
						J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z, 
											 sim.x, sim.y, sim.z);
						Espera(800);//400);
						pwinMain->drawScene();
					}
				} while (sim.Avanca(viu));
				//Fim da simula��o do replay do op
				
				MudaEstado(EST_JOGANDO);
				resp = Msg(MBOX_YES_NO, msg.format("Voc� gostaria de ver o replay do %s de novo?", J[J[op].idImit].Nome));
			}
		}
		op = J[op].idProx;
	}
}

void cJogo::PassaVez()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	while (J[vz].T > 0) {
		GastaTempo(Min(J[vz].T, 5));
		if (TestaEncontro(false)) return; //se viu algu�m desiste de passar a vez
		Reescreve();
		//drawScene(0);
	}

	if (Console) ToggleConsole();

	//ApagaConsole();

	//zera vari�veis da rodada (do jogo)
	TempoParado = false;
	enuComando = CmdNada;

	//TO DO: percorrer o mapa a procura de granadas e bombas-rel�gio (assim como corpos de predador)

	//if (TestaMortes(????)) return;

	J[vz].HtAnt = J[vz].Ht;

	//procura o pr�ximo vivo
	do {
		do {
			//vz++;
			//if (vz >= numjogs) vz = 0;
			vz = J[vz].idProx;

			//TO DO: fazer o sangue alien abrir buracos
			//Apaga fuma�as, fogos e fecha portas
			double chance = pow(0.4, numjogs);
			for (int ix = 0; ix < Mapa.dimX; ix ++)
			for (int iy = 0; iy < Mapa.dimY; iy ++)
			for (int iz = 0; iz < Mapa.dimZ; iz ++) {
				if (rnd() < chance) {
					cCont cont = Mapa.Cont(ix, iy, iz, cMapa::NAO_CHAO);

					if (cont.EhFumaca()) {
						cont.SetParedeNula();
						Mapa.SetCont(ix, iy, iz, cont);
					} else if (cont.EhFogo()) {
						cont.SetFumaca();
						Mapa.SetCont(ix, iy, iz, cont);
					} else if (cont.EhPortaAberta()) {
						bool porta_livre = true;
						for (short j=0; j<numjogs; j++)
						if (!J[j].GetFlag(Morto)) {//a porta n�o pode fechar em cima dos jogadores
							//TO DO (OR !): a porta deve ou n�o fechar em cima de holografias
							if (ix == J[j].x
							&&  iy == J[j].y
							&&  iz == J[j].z) {
								porta_livre = false;
								break;
							}
						}
						if (porta_livre) {
							cont.FechaPorta();
							Mapa.SetCont(ix, iy, iz, cont);
						}
					}
				}
			}

			//TO DO: diminuir a contagem da auto destrui��o do predador, isto fica aqui pq a contagem continua mesmo com o predador morto (obs.: seu corpo pode estar sendo carregado)

			//Mostra replay aos mortos
			if (J[vz].GetFlag(Morto) && J[vz].GetFlag(MorreuSemVerRep)) {
				J[vz].SetFlag(MorreuSemVerRep, false);

				//garante que o replay dos mortos esteja apagado
				J[vz].ZeraReplay();

				//Testa se o cara viu algum replay de algu�m
				bool viualguem = false;
				int op = J[vz].idProx;
				while (op != vz) {
					if (J[op].Replay.ViuAlgo(vz)) {
						viualguem = true;
						break;
					}
					op = J[op].idProx;
				}

				if (viualguem) {
					//Chama o cara
					HUDMostra(false);
					FXString msg;
					Msg(MBOX_OK, msg.format("Venha %s", J[vz].Nome));
					HUDMostra(true);

					pMapa = J[vz].Fog;
					J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);

					//Mostra o replay
					MostraReplay();
				}
			}

		} while (J[vz].GetFlag(Morto)); //obs: se todos estiverem mortos, n�o sai mais daqui

		//Ovo do Alien
		if (J[vz].Incubacao > 0) {
			J[vz].Incubacao --;
			if (J[vz].Incubacao == 0) {
				//char* txt = new char[strlen(J[vz].Nome) + 39];
				//sprintf(txt, "O %s morreu e se transformou em um alien!", J[vz].Nome);
				//MsgDef(txt);
				FXString msg;
				MsgDef(msg.format("O %s morreu e se transformou em um alien!", J[vz].Nome));
				//TO DO: Broadcast(msg);

				J[vz].PodSoltaCorpo(&Mapa);

				short incubador = J[vz].Injetor;

				//tranforma o alien pequeno no incubador, apenas para as estat�sticas
				short auxshort;
				Troca(J[vz].Poder, J[incubador].Poder, auxshort);
				Troca(J[vz].Kills, J[incubador].Kills, auxshort);
				Troca(J[vz].Time, J[incubador].Time, auxshort);
				//troca os nomes
				/*
				char *auxstr = NULL;
				strcpy(auxstr,				J[vz].Nome);
				strcpy(J[vz].Nome,			J[incubador].Nome);
				strcpy(J[incubador].Nome,	auxstr);
				*/
				FXString auxstr;
				auxstr			  = J[vz].Nome;
				J[vz].Nome		  = J[incubador].Nome;
				J[incubador].Nome = auxstr;

				J[vz].Kills ++;
				J[vz].Poder = pAlienGrande;
				J[vz].AcionaPoder(Armas, Municoes);

				//TO DO: soltar todo o equipamento

				J[vz].Posicao = posNormal;
			}
		}

		//a degenera��o m�xima � de 60%
		J[vz].Reg = Max(0.4f, J[vz].Reg);
		//alguns poderes n�o sofrem regenera�ao/degenera��o
		if (PodSemRegeneracao(vz)) J[vz].Reg = 1;
		//zerg enterrado tem regenera��o maior
		if (J[vz].Posicao == posEnterrado) J[vz].Reg *= 1.2f;
		//aplica reg. obs: � alat�rio, mas n�o muda de <1 p/ >1 ou vice-versa
		//MODIF.: no original era Aleat�rio e n�o superrnd, mas acho que estava errado
		J[vz].Ht = (J[vz].Ht + 0.5) * exp(superrnd(log(J[vz].Reg))) - 0.5;
		//n�o deixa a resist�ncia passar do m�ximo
		J[vz].Ht = Min(J[vz].HtMax, J[vz].Ht);
		if (J[vz].Ht <= 0) {
			//J[vz].Ht = -666;
			if (TestaMortes(-1)) return;
		}		
	} while (J[vz].GetFlag(Morto));

	//O novo valor de Reg � igual � m�dia geom�trica entre o valor antigo e RegNatural
	//Deste modo, Reg tende a RegNatural (se � houver influ�ncias externas, � claro)
	J[vz].Reg = sqrt(J[vz].Reg * J[vz].RegNat);

	//TO DO: sangra

	//Recarrega armas que s� d�o um tiro por rodada
	//if (J[vz].PossuiArma[13]) J[vz].Mun[13] = Armas[13].Municao;
	J[vz].objMaoDir->RecarregaArmaDeUmTiroPorRodada(Armas);
	J[vz].objMaoEsq->RecarregaArmaDeUmTiroPorRodada(Armas);
	J[vz].Equip.RecarregaArmasDeUmTiroPorRodada(Armas);
	J[vz].Skills.RecarregaArmasDeUmTiroPorRodada(Armas);

	//Recarrega mana
	if (PodUsaMagiasArcanas(vz)) J[vz].ManaFe += (short)floor(10 - (J[vz].ManaFe / 20.));

	//TO DO: Recarrega parasitas, canh�o, mana/f�, gasta capacete

	//TO DO: NET: se o computador do pr�ximo jogador for diferente deste ent�o enviar a vez sen�o:

	//TO DO: quando o ethereal controla, o duelo deve chamar o ethereal

	RecebeVez();
}

void cJogo::RecebeVez()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//Esconde Heads Up Display
	HUDMostra(false);

	//char* txt = new char[strlen(J[vz].Nome) + 7];
	//sprintf(txt, "Venha %s", J[vz].Nome);
	//Msg(txt, MSG_OKONLY);
	FXString msg;
	Msg(MBOX_OK, msg.format("Venha %s", J[vz].Nome));

	//zera vari�veis da rodada (do jogador)
	J[vz].ZeraReplay();
	J[vz].ZeraFogBit();
	//evita de avisar os jogadores que o cara sempre v�
	if (PodSempreVeBotsNeutros(vz)) {
		//Agente do Matrix sempre v� os bots neutros, al�m dos caras do time dele
		for (int i=0; i<numjogs; i++) JaVi[i] = (J[i].Time == J[vz].Time || (J[i].Bot && J[i].Time == -1)) ? 1 : 0;
	} else {
		for (int i=0; i<numjogs; i++) JaVi[i] = (J[i].Time == J[vz].Time) ? 1 : 0;
	}
	for (int i=0; i<numjogs; i++) JaViHol[i] = false;
	enuComando = CmdNada;
	contlinhatiro = 0;
	contsombratiro = 0;
	J[vz].T = J[vz].TProxRodada;
	J[vz].TProxRodada = J[vz].TMax;

	//Mostra HUD j� com valores corretos
	Reescreve();
	HUDMostra(true);

	//Grava uma primeira foto
	J[vz].Replay.GravaFoto(AcaoMove, num(J[vz].x), num(J[vz].y), num(J[vz].z));

	//Ajusta mapa a ser exibido
	pMapa = J[vz].Fog;
	J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);
	AtualizaFog();

	//avisa se o cara levou dano na rodada anterior
	if (J[vz].HtAnt > J[vz].Ht) MsgDef("Voc� sofreu dano fora da sua vez!");
	J[vz].HtAnt = J[vz].Ht;
	//avisa se o cara est� com alta degenera��o
	if (J[vz].Reg < 0.95) MsgDef("Voc� est�sangrando!");
	//TO DO: testar e avisar se o capacete acabou

	MostraReplay();

	//centra a camera de volta no jogador ap�s mostrar o replay
	J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);

	//Testa encontro no in�cio da vez (� necess�rio um certo dt falso para que se possa ver algu�m)
	J[vz].TAnt = J[vz].T + 20;
	TestaEncontro(true);

	if (PodRegeneracaoT1000(vz)) {
		//se o tempo para regenerar tudo o que ele apanhou for maior do que
		// o tempo que ele tem ent�o, o T-1000 � obrigado a regenerar
		if (J[vz].HtMax - J[vz].HtMax > J[vz].T / PodT_1000_TempoParaReparar) {
			MsgDef("Voc� est� muito mal!\nSer� obrigado a se reparar!");
			J[vz].Ht += J[vz].T / PodT_1000_TempoParaReparar;
			J[vz].T = 0;
		}
	}
}

void cJogo::SorteiaOrdem()
{
	int faltam, p1, p2;
	vz = rndi(0, numjogs-1);
	faltam = numjogs - 1;
	p1 = vz;
	while(faltam)
	{
		p2 = rndi(0, numjogs-1);
		if ((p2 != p1) && (J[p2].idProx == -1)) {
			J[p1].idProx = p2;
			faltam--;
			if (!faltam) {
				J[p2].idProx = vz;
			} else {
				p1 = p2;
			}
		}
	}
}

void cJogo::Posiciona()
{
	int i, j, c;
	bool p;
	double d;
	double dmin = pow((double)(Mapa.dimX * Mapa.dimY * Mapa.dimZ), 0.3333) / (double)numjogs;

	c = 0;
	for (i=0; i<numjogs; i++) {
		do
		{
			//se estiver demorando demais -> diminuir dmin
			c++;
			if (c >	10000) {
				dmin *= 0.9f;
				c = 0;
			}

			//sortear uma posi��o
			J[i].x = rndi(0, Mapa.dimX-1);
			J[i].y = rndi(0, Mapa.dimY-1);
			J[i].z = rndi(0, Mapa.dimZ-1);

			//testa se j� h� algu�m muito perto da posi��o sorteada
			p = false;
			for (j=0; j<i; j++) {
				d = Distancia3D(J[i].x, J[i].y, J[i].z,
								J[j].x, J[j].y, J[j].z);
				if (d < dmin) p = true;
			}
			p |= cCont(Mapa.Cont(J[i].x, J[i].y, J[i].z)).EhParede();
			if (J[i].z > 0) p |= cChao(Mapa.Cont(J[i].x, J[i].y, J[i].z, cMapa::CHAO)).EhChaoBuracoInvOuNao();
		} while (p);// || EhParede(Mapa.Cont(J[i].x, J[i].y, J[i].z))
					// || EhChaoBuracoInvOuNao(Mapa.Cont(J[i].x, J[i].y, J[i].z, CHAO)));
	}
}

void cJogo::CriaFogs()
{
	for (int i=0; i<numjogs; i++) {
		//J[i].Fog = new cMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ, cCont::CONT_FOG_NUNCA_VIU);
		J[i].Fog = new cMapa(&Mapa, cCont::CONT_FOG_NUNCA_VIU);
		J[i].FogBit = new cMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ);

		//TO DO (|| n�o): n�o precisa criar fogs separados para os aliados
	}
}