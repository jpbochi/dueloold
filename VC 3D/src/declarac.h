
#ifndef DECLARAC_H
#define DECLARAC_H

#ifndef PI
#define PI			3.141592f
#endif
#define MEIOPI		1.570796f
#define DOIS_PI		6.28318f
#define PI_180		0.01745f
#define _180_PI		57.29578f
#define RAIZ3		1.73205f
#define INVRAIZ3	0.57735f
#define LADOHEX		INVRAIZ3
#define LARGURAHEX	0.866025f	//1.5 * LADOHEX = RAIZ3/2

#define Troca(X,Y,AUX) AUX=X; X=Y; Y=AUX //n�o � necess�rio o �ltimo ";"
#define Max(X,Y) ((X>Y) ? X : Y)
#define Min(X,Y) ((X>Y) ? Y : X)

#define ALTURAANDAR		2.5f
#define LARGURACHAO		0.5f	
#define ALTURAPAREDE	(ALTURAANDAR - LARGURACHAO)

#define MAX_TEXTURAS	512

//#ifndef MK_ALT
//#define MK_ALT	0x0020
//#endif

typedef unsigned char ubyte;
typedef unsigned short ushort;
typedef unsigned int uint;

//#define NOME_MAX 15

//#define NUM_ARMAS 29

#endif