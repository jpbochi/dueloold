
#include "replay.h"

#include <windows.h>
// load in the multimedia library
#ifdef _MSC_VER
#pragma comment(lib,"winmm.lib")
#endif

cFoto::cFoto()
{
	prox = NULL;
	flagjogviu = 0;
}
cFoto::~cFoto()
{
	if (prox != NULL) {
		delete prox;
		prox = NULL;
	}
}

cReplay::cReplay()
{
	prif = NULL;
	ultf = NULL;
	flagjogviualgo = 0;
}

void cReplay::Zera(short pxini, short pyini, short pzini, short pfrenteini)
{
	//TO DO: receber o cursor tb
	xini = pxini;
	yini = pyini;
	zini = pzini;
	frenteini = pfrenteini;
	flagjogviualgo = 0;
	if (prif != NULL) {
		delete prif;
		prif = NULL;
		ultf = NULL; //n�o � necess�rio apagar o ultf pq ele foi apagado na rea��o em cadeia
	}
}

void cReplay::GravaSom(short id)
{
	if (id != SOM_NENHUM) {
		//TEMP: guando o replay estiver funcionando, o som n�o sair� na hora
		TocaSom(id);
	}
}

void cReplay::GravaFoto(typenuAcao acao, num inf1, num inf2, num inf3, num inf4)
{
	cFoto *pfoto = new cFoto;
	pfoto->acao = acao;
	pfoto->inf[0] = inf1;
	pfoto->inf[1] = inf2;
	pfoto->inf[2] = inf3;
	pfoto->inf[3] = inf4;
	AdicionaFoto(pfoto);
}

void cReplay::VeFoto(short idjog)
{
	if (ultf != NULL && idjog < 16) {
		unsigned int mask = 1 << idjog;
		ultf->flagjogviu |= mask;
		flagjogviualgo |= mask;
	}
}

bool cReplay::ViuAlgo(short idjog)
{
	return (flagjogviualgo & (1 << idjog)) != 0;
}

void cReplay::TocaSom(short id)
{
	switch (id) {
		case SOM_TIRO:		PlaySound(ARQSOM_TIRO, NULL, SND_ASYNC);break;
		case SOM_MET:		PlaySound(ARQSOM_MET, NULL, SND_ASYNC);break;
		case SOM_ESPING:	PlaySound(ARQSOM_ESPING, NULL, SND_ASYNC);break;
		case SOM_FOGUETE:	PlaySound(ARQSOM_FOGUETE, NULL, SND_ASYNC);break;
		case SOM_BAFORADA:	PlaySound(ARQSOM_BAFORADA, NULL, SND_ASYNC);break;
		case SOM_MG:		PlaySound(ARQSOM_MG, NULL, SND_ASYNC);break;
		case SOM_CANHAO:	PlaySound(ARQSOM_CANHAO, NULL, SND_ASYNC);break;
		case SOM_LASER:		PlaySound(ARQSOM_LASER, NULL, SND_ASYNC);break;
		case SOM_SOCO:		PlaySound(ARQSOM_SOCO, NULL, SND_ASYNC);break;
		case SOM_ESPADA:	PlaySound(ARQSOM_ESPADA, NULL, SND_ASYNC);break;
		case SOM_MORDIDA:	PlaySound(ARQSOM_MORDIDA, NULL, SND_ASYNC);break;
		case SOM_TARRASQ:	PlaySound(ARQSOM_TARRASQ, NULL, SND_ASYNC);break;
	}
}

void cReplay::AdicionaFoto(cFoto *pfoto)
{
	if (prif == NULL) {
		prif = pfoto;
		ultf = pfoto;
	} else {
		ultf->prox = pfoto;
		ultf = pfoto;
	}
}

cSimulaReplay::cSimulaReplay()
{
	idvz = -1;
	idop = -1;
	maskviu = -1;
	foto = NULL;
}

void cSimulaReplay::Inicia(short _idvz, short _idop, cReplay *replay)
{
	idvz = _idvz;
	idop = _idop;
	maskviu = 1 << idvz;
	x = replay->xini;
	y = replay->yini;
	z = replay->zini;
	frente = replay->frenteini;

	proxf = replay->prif;
	foto = NULL;
}

bool cSimulaReplay::Avanca(bool &viu)
{
	if (proxf == NULL) {
		return false;
	} else {
		foto = proxf;
		proxf = foto->prox;
		switch (foto->acao) {
			case AcaoMove:
				x = foto->inf[0].i;
				y = foto->inf[1].i;
				z = foto->inf[2].i;
			break;
			case AcaoVira:
				frente = foto->inf[0].i;
			break;
			/*
			case AcaoMudaCursor:
			break;
			case AcaoAtira:
			break;
			case AcaoRecarrega:
			break;
			case AcaoArremessa:
			break;
			case AcaoLancaMagia:
			break;
			case AcaoInstalaBomba:
			break;
			case AcaoPega:
			break;
			case AcaoSolta:
			break;
			case AcaoSeTeletransporta:
			break;
			case AcaoGolpeia:
			break;
			case AcaoAbrePorta:
			break;*/
		}
		viu = (maskviu & foto->flagjogviu) != 0;
		return true;
	}
}