
#ifndef JOGO_H
#define JOGO_H

#include "winMain.h"

#include "declarac.h"
#include "mapa.h"
#include "jogador.h"
#include "arma.h"
#include "modelosgl.h"
#include "linhas.h"

#define EST_INTRO		1
#define EST_EDITANDO	2
#define EST_NOVOJOGO	3
#define EST_JOGANDO		4
#define EST_MENSAGEM	5
#define EST_VENDOREPLAY	6

class cIconeBut;

enum typenuComando {
	CmdNada = 0,
	CmdMirar,
	CmdJediPega,
	CmdArremessar,
	CmdControlar,
	CmdIdentificar,
	CmdHolografar,
	CmdAnimarMortos,
	CmdRelampago,
	CmdDardosMisticos
};

class cJogo
{
	public:
	cJogo();
	void Reinicia();
	void CarregaArmasPadrao();
	
	//específicos da GUI
	winMain *pwinMain;
	cIconeBut *pIc;
	FXuint NaoAnimando;
	double Zoom;
	enum {
		MODOCAM_NORMAL = 0,
		MODOCAM_PRI_PES = -1,
		MODOCAM_TROCA = 1,
	}; int modocam;
	
	FXuint Msg(FXuint opts, FXString msg);
	FXuint MsgSemTempo(char *msg, short t);
	FXuint MsgDef(FXString msg);
	void Espera(int milisec);
	
	int Estado;
	bool JogoComecou;
	bool Console;
	cSimulaReplay sim;
	cMapa* pMapa;
	ushort numjogs;
	cJogador* J;
	cArma* Armas;
	cMunicao* Municoes;

	//double fps;

	//variáveis de rodada
	short vz;
	char *JaVi;
	bool *JaViHol;
	bool TempoParado;
	typenuComando enuComando;
	cLinhaSombra sombra_mira, sombra_poss_mira;
	cLinhaTiro linhatiro[64];
	short contlinhatiro;
	cLinhaSombra sombratiro[64];
	short contsombratiro;

	bool fundopreto;
	void HUDMostra(bool);
	void Reescreve();
	void MudaEstado(int);
	void ToggleConsole();
	void DesenhaJogador(short j, short x=-1, short y=-1, short z=-1, short frente=-1);
	void Renderiza(double fps, bool selection = false, bool comburacos = true);

	void Click(ushort, ushort, ushort, double, int);

	//void TrocaArma(short id);
	void IniciaMira();
	void CameraMira(int modo = MODOCAM_TROCA);
	bool Mira(int mx, int my, int mz, double md);
	void MiraMais();
	void Atira();
	protected:
		bool TemMunicao(cObjeto* arma);
		void Fogo(double teta, double phi,
				  double xo, double yo, double zo,
				  short arma, short codmun,
				  double distalvo);
		void Movimenta(int mx, int my, int mz, bool sovirar, bool strafe);
		void OndePisa(short quem, short tempo = 15);
		bool Buracos(short quem);
		void Atropela(short atrop);
		bool Vira(short proxFrente, short angpermitido);
		
		void GastaTempo(short);

		bool TestaEncontro(bool atualizafog, double camvz=1);
		bool EstaVendo(int quem);
		bool EstaSendoVistoPor(int quem);
		bool Viu(int obsador, int obsado, char javiu, short dt,
				int xop=-1, int yop=-1, int zop=-1, double camextra=1);
		void AtualizaFog();
		void LinhaFog(double x0, double y0, double z0,
					ushort obsador, double radar, bool soradar,
					double teta, double phi, double d0);

		double CausaDor(short quem, double dano, double blindagem);
		void Sangra(short quem, short x=-1, short y=-1, short z=-1);
		bool TestaMortes(short causamortis);
		bool FimDeJogo(short &timevencedor);

		void MostraReplay();
	public:

	bool Pega(int mx, int my, int mz);//, cObjetoSimples *objsimp = NULL, cObjeto *obj = NULL);
	bool Pega(cObjeto *obj, cListaPosObjetos *listaobjs, bool blnMaoDir);
	bool Solta(bool blnMaoDir);
	bool Guarda(bool blnMaoDir);
	bool Saca(bool blnMaoDir, cObjeto *item);
	bool Recarrega();
	bool Vestir(bool blnMaoDir, cObjeto::enmLugarUso);
	bool Desvestir(bool blnMaoDir, cObjeto::enmLugarUso);
	
	bool UsaMao(bool blnMaoDir);
	
	void PassaVez();
	void RecebeVez();
	void SorteiaOrdem();
	void Posiciona();
	void CriaFogs();

protected:
	friend class winEquip;
	cMapa Mapa;
};

#endif
