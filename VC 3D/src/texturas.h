
#ifndef TEXTURAS_H
#define TEXTURAS_H

#include "fx.h"

bool CarregaTextura(FXApp *, FXString, FXuint &, bool transp = true);
void ChromaKey(FXImage *img, FXColor cor);
FXImage *Imagem(FXApp *app, FXString strfile);
FXIcon *Icone(FXApp *app, FXString strfile);

#endif
