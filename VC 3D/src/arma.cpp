
#include "declarac.h"
#include "arma.h"
#include <stdio.h>
#include <math.h>

/*
Atributos da Arma:
Mira M�nima
Mira M�xima
MirMais
Coice ao Atirar *
Coice ao Andar **
Tempo p/ Mirar
Tempo p/ Atirar
Dano
Explos�o

* modificado pela sqrt(For�a) do jogador
** modificado pelo tipo de movimento (andar/correr)

- Mirar: gasta 5 de tempo e mutiplica a mira pela MirMais

TO DO: tiros de luz (como o do TripleLaser do Predador) passam pelo tele transporte
*/

cArma::cArma()
{
	//n�o faz nada, deixa para ser iniciado de fora
	//isto � necess�rio para declarar o array e depois inicializar cada item
}

void cArma::Inicia(short _id,
				   char *_Nome,
				   /*
				   short _Municao,
				   short _MunRec,
				   short _MunExtra,
				   double _DesCam,
				   short _Projeteis,
				   double _Dano,
				   double _RaioExplosao,
				   double _Degen,
				   double _Alcance,
				   /*/
				   short _MunExtra,
				   /**/
				   double _DanoGolpe,
				   double _MirIni,
				   double _MirMax,
				   double _MirMais,
				   short _TempoMirar,
				   short _TempoAtirar,
				   short _TempoRecarr,
				   double _CoiceTiro,
				   double _CoiceMove,
				   short _MunPadrao,
				   short _SomPad)
{
	sprintf(Nome, "%s", _Nome);

	id =			_id;
	/*
	Municao =		_Municao;
	MunRec =		_MunRec;
	MunExtra =		_MunExtra;
	DesCam =		_DesCam;
	Projeteis =		_Projeteis;
	Dano =			_Dano;
	RaioExplosao =	_RaioExplosao;
	Degen =			_Degen;
	Alcance =		_Alcance;
	/*/
	MunPadrao =		_MunPadrao;
	MunExtra =		_MunExtra;
	/**/
	DanoGolpe =		_DanoGolpe;
	MirIni =		_MirIni;
	MirMax =		_MirMax;
	MirMais =		sqrt(_MirMais); //???????
	TempoMirar =	_TempoMirar;
	TempoAtirar =	_TempoAtirar;
	TempoRecarr =	_TempoRecarr; //Se for -1 ent�o a arma s� pode dar um tiro por rodada
	CoiceTiro =		_CoiceTiro;
	CoiceMove =		_CoiceMove;
	SomPad =		_SomPad;

	//OBS.: Municao significa quanto de muni��o ser� gasta para as armas que usam Mana/F�

	//a mira m�nima � calculada
	MirMin =		MirIni * CoiceTiro * CoiceTiro;
}

bool cArma::SohUmTiroPorRodada()
{
	return (TempoRecarr == -1);
}

double cArma::MultMira(double mira, double mult)
{
	return Max(MirMin,
		   Min(MirMax, mira * mult));
}

cMunicao::cMunicao()
{
	//n�o faz nada, deixa para ser iniciado de fora
	//isto � necess�rio para declarar o array e depois inicializar cada item
}

void cMunicao::Inicia(short _id,
					  char *_Nome,
					  short _MunPac,
					  short _MunRec,
					  double _DesCam,
					  short _Projeteis,
					  double _Dano,
					  double _RaioExplosao,
					  double _Degen,
					  double _Alcance,
					  short _ArmaDest)
{
	sprintf(Nome, "%s", _Nome);
	
	ArmaDest = _ArmaDest;
	
	MunPac =		_MunPac;
	MunRec =		_MunRec; //-1 significa que � imposs�vel recarregar (arma descart�vel, some quando a muni��o acabar)
	DesCam =		_DesCam;
	Projeteis =		_Projeteis;
	Dano =			_Dano;
	RaioExplosao =	_RaioExplosao;
	Degen =			_Degen;
	Alcance =		_Alcance;
}

bool cMunicao::NaoRecarregavel()
{
	return (MunRec == -1);
}

double cMunicao::Alcancef()
{
	return (Alcance == 0) ? 1000 : Alcance;
}
