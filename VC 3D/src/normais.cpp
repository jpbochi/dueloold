
#include <math.h>
#include "normais.h"

//**************kglRescaleNormal()
// Reduces a normal vector specified as a set of three coordinates,
// to a unit normal vector of length one. Used internally by shadow and
// normal generation functions
void RescaleNormal( vertex vector )	{

	double length;
	
	// Calculate the length of the vector		
	length = sqrt((vector[0]*vector[0]) + 
				  (vector[1]*vector[1]) +
				  (vector[2]*vector[2]));

	// Keep the program from blowing up by providing an acceptable
	// value for vectors that may calculated too close to zero.
	if(length == 0.0) length = 1.0;

	// Dividing each element by the length will result in a
	// unit normal vector.
	vector[0] /= (float)length;
	vector[1] /= (float)length;
	vector[2] /= (float)length;
}

//**************kglCalcNormalfv()
// Vertexes v[] specified in counter clock-wise order (min 3 vertexes)
// note that you can pass more than 3 vertexes if you want, so it will
// work easily with polys with more than 3 vertexes stored in an array
void CalcNormalfv( const vertex v[], vertex out ) {
	vertex v1, v2;
	static const int x = 0;
	static const int y = 1;
	static const int z = 2;

	// Calculate two vectors from the three points
	v1[x] = v[0][x] - v[1][x];
	v1[y] = v[0][y] - v[1][y];
	v1[z] = v[0][z] - v[1][z];

	v2[x] = v[1][x] - v[2][x];
	v2[y] = v[1][y] - v[2][y];
	v2[z] = v[1][z] - v[2][z];

	// Take the cross product of the two vectors to get
	// the normal vector which will be stored in out
	out[x] = v1[y]*v2[z] - v1[z]*v2[y];
	out[y] = v1[z]*v2[x] - v1[x]*v2[z];
	out[z] = v1[x]*v2[y] - v1[y]*v2[x];

	// Normalize the vector (shorten length to one)
	RescaleNormal( out );
}

//**************kglCalcNormalf()
// Vertexes [xyz]1-3 specified in CCW order
// This is the "lazy boi" version of kglCalcNormal that
// can have the vertexes specified directly - good if
// you don't want to make an array of vertexes for a simple object
void CalcNormalf(float x1, float y1, float z1, 
				 float x2, float y2, float z2, 
				 float x3, float y3, float z3, vertex out ) {
	vertex v1, v2;
	static const int x = 0;
	static const int y = 1;
	static const int z = 2;

	// Calculate two vectors from the three points
	v1[x] = x1 - x2;
	v1[y] = y1 - y2;
	v1[z] = z1 - z2;

	v2[x] = x2 - x3;
	v2[y] = y2 - y3;
	v2[z] = z2 - z3;

	// Take the cross product of the two vectors to get
	// the normal vector which will be stored in out
	out[x] = v1[y]*v2[z] - v1[z]*v2[y];
	out[y] = v1[z]*v2[x] - v1[x]*v2[z];
	out[z] = v1[x]*v2[y] - v1[y]*v2[x];

	// Normalize the vector (shorten length to one)
	RescaleNormal( out );
}
