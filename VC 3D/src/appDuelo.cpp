
#include "fx.h"
#include "appduelo.h"

#include "jogo.h"
extern cJogo Jogo;
#include "iconebut.h"

// Map
FXDEFMAP(appDuelo) appDueloMap[]={
  FXMAPFUNC(SEL_SIGNAL,		appDuelo::ID_CLOSEALL,	appDuelo::onCmdCloseAll),
  FXMAPFUNC(SEL_COMMAND,	appDuelo::ID_CLOSEALL,	appDuelo::onCmdCloseAll),
  };


// Object implementation
FXIMPLEMENT(appDuelo,FXApp,appDueloMap,ARRAYNUMBER(appDueloMap))


// Make some windows
appDuelo::appDuelo(const FXString& name):FXApp(name,FXString::null){

	//definir variáveis "globais" aqui

	// Make some icons; these are shared between all text windows
	//bigicon=new FXGIFIcon(this,big_gif);
	//searchicon=new FXGIFIcon(this,search_gif,0,IMAGE_ALPHAGUESS);

	// File associations
	//associations=new FXFileDict(this);
}


// Initialize application
void appDuelo::init(int& argc,char** argv,FXbool connect)
{
	FXApp::init(argc,argv,connect);
	/*
	FXString syntaxfile;

	// After init, the registry has been loaded
	//FXApp::init(argc,argv,connect);

	// Now we know the icon search path
	associations->setIconPath(reg().readStringEntry("SETTINGS","iconpath",FXIconDict::defaultIconPath));

	// Hunt for the syntax file
	syntaxfile=FXFile::search(FXFile::getExecPath(),"Adie.stx");

	// Load syntax file
	if(!syntaxfile.empty()){
		loadSyntaxFile(syntaxfile);
	}
	//*/
}


// Exit application
void appDuelo::exit(FXint code){

	// Writes registry, and quits
	FXApp::exit(code);
	
	::exit(code);
}

// Close all windows
long appDuelo::onCmdCloseAll(FXObject*,FXSelector,void*)
{
	//while(0<windowlist.no() && windowlist[0]->close(TRUE));
	return 1;
}


// Clean up the mess
appDuelo::~appDuelo()
{
	if (Jogo.pIc) delete Jogo.pIc;
	Jogo.pIc = 0;
	//for(int i=0; i<syntaxes.no(); i++) delete syntaxes[i];
	//FXASSERT(windowlist.no()==0);
	//delete associations;
}

