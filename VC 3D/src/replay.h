
#ifndef REPLAY_H
#define REPLAY_H

#define SOM_NENHUM		0

#define ARQSOM_TIRO		".\\sons\\tiromib.wav"
#define SOM_TIRO		1
#define ARQSOM_MET		".\\sons\\metruim.wav"
#define SOM_MET			2
#define ARQSOM_ESPING	".\\sons\\c_shpump.wav"
#define SOM_ESPING		3
#define ARQSOM_FOGUETE	".\\sons\\foguete.wav"
#define SOM_FOGUETE		4
#define ARQSOM_BAFORADA	".\\sons\\baforada.wav"
#define SOM_BAFORADA	5
#define ARQSOM_MG		".\\sons\\MGun.wav"
#define SOM_MG			6
#define ARQSOM_CANHAO	".\\sons\\canon01.wav"
#define SOM_CANHAO		7
#define ARQSOM_LASER	".\\sons\\laser.wav"
#define SOM_LASER		8

#define ARQSOM_SOCO		".\\sons\\soc.wav"
#define SOM_SOCO		9
#define ARQSOM_ESPADA	".\\sons\\spear.wav"
#define SOM_ESPADA		10
#define ARQSOM_MORDIDA	".\\sons\\wolf.wav"
#define SOM_MORDIDA		11
#define ARQSOM_TARRASQ	".\\sons\\Tarrasqu.wav"
#define SOM_TARRASQ		12

enum typenuAcao {
	AcaoMove = 0,
	AcaoVira,
	AcaoMudaCursor,
	AcaoAtira,
	AcaoRecarrega,
	AcaoArremessa,
	AcaoLancaMagia,
	AcaoInstalaBomba,
	AcaoPega,
	AcaoSolta,
	AcaoEquip,
	AcaoSeTeletransporta,
	AcaoGolpeia,
	AcaoAbrePorta
};

union num
{
   bool	 b;	
   int	 i;
   double f;
   num()		{i=-1;}
   num(bool pb)	{b=pb;}
   num(int pi)	{i=pi;}
   num(double pf){f=pf;}
};

class cFoto
{
	public:
	cFoto();
	~cFoto();

	cFoto *prox;

	typenuAcao acao;
	num inf[4];
	unsigned int flagjogviu;
};

class cReplay
{
	public:
	cReplay();

	void Zera(short pxini, short pyini, short pzini, short pfrenteini);
	void GravaSom(short id);
	void GravaFoto(typenuAcao acao, num inf1 = num(),
									num inf2 = num(),
									num inf3 = num(),
									num inf4 = num());
	void VeFoto(short);
	bool ViuAlgo(short);

	private:
	void TocaSom(short);
	void AdicionaFoto(cFoto *);

	public:
	short xini, yini, zini, frenteini, cursorini;
	cFoto *prif, *ultf;
	unsigned int flagjogviualgo; //se viu alguma coisa
};

class cSimulaReplay
{
	public:
	short idvz, idop;
	unsigned int maskviu;
	short x, y, z, frente;
	cFoto *foto, *proxf;

	cSimulaReplay();
	void Inicia(short _idvz, short _idop, cReplay *);
	bool Avanca(bool &viu);
};

#endif
