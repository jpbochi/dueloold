
#include "jogo@.h"

void cJogo::HUDMostra(bool mostra)
{
	fundopreto = !mostra;
	if (mostra) pwinMain->frmJogo->show();
	else		pwinMain->frmJogo->hide();
	//pwinMain->forceRefresh();
	pwinMain->drawScene();
}

void cJogo::Espera(int milisec)
{
	NaoAnimando = false;
	pwinMain->IniciaTimerEspera(milisec, &NaoAnimando);
	pwinMain->getApp()->runUntil(NaoAnimando);
}

/*void ApagaConsole()
{
	//TO DO: implementar ApagaConsole
}

bool drawScene(double dTime)
{
	//TO DO: implementar drawScene
	return true;
}*/

FXuint cJogo::Msg(FXuint opts, FXString msg){
	pwinMain->LiberaTeclas();
	return FXMessageBox::information(pwinMain, opts, "Duelo 3D", msg.text());
}

FXuint cJogo::MsgSemTempo(char *msg, short t){
	pwinMain->LiberaTeclas();
	return FXMessageBox::information(pwinMain, MBOX_OK, "Duelo 3D", "Voc� n�o tem tempo para %s! (%d)", msg, t);
}

FXuint cJogo::MsgDef(FXString msg){
	pwinMain->LiberaTeclas();
	return FXMessageBox::information(pwinMain, MBOX_OK, "Duelo 3D", msg.text());
}

void cJogo::MudaEstado(int novoEstado)
{
	/*switch (Estado)			//antes de mudar
	{
		case EST_INTRO:		break;
		case EST_EDITANDO:	break;
		case EST_NOVOJOGO:	break;
		case EST_JOGANDO:	break;
		case EST_MENSAGEM:	break;
	}*/
	/*
	Estado = novoEstado;
	switch (Estado)			//depois de mudar
	{
		case EST_INTRO:		pMapa = &Mapa;loadIntroUI();break;
		case EST_EDITANDO:	pMapa = &Mapa;loadEditorUI();break;
		case EST_NOVOJOGO:	pMapa = &Mapa;loadNovoJogoUI();break;
		case EST_JOGANDO:
			JogoComecou=true;
			loadJogandoUI(Console, &J[vz]);
			break;
		case EST_MENSAGEM:		loadMensagemUI(NULL, MSG_OKONLY);break;
		case EST_VENDOREPLAY:	loadJogandoUI(false, &J[vz], false);
	}*/
	
	Estado = novoEstado;
	switch (Estado)			//depois de mudar
	{
		case EST_INTRO:		pMapa = &Mapa;break;
		case EST_EDITANDO:	pMapa = &Mapa;break;
		case EST_NOVOJOGO:	pMapa = &Mapa;break;
		case EST_JOGANDO:	JogoComecou=true;break;
		//case EST_MENSAGEM:		loadMensagemUI(NULL, MSG_OKONLY);break;
		case EST_VENDOREPLAY:	break;
	}
}

void cJogo::ToggleConsole()
{
	//TO DO: decidir o que fazer com isso
	/*
	if (Estado == EST_JOGANDO) {
		Console = !Console;
		loadJogandoUI(Console, &J[vz]);
	}*/
}

void cJogo::DesenhaJogador(short j, short x, short y, short z, short frente)
{
	if (x == -1) x = J[j].x;
	if (y == -1) y = J[j].y;
	if (z == -1) z = J[j].z;
	if (frente == -1) frente = J[j].Frente;

	glPushMatrix();

	//posiciona
	double xh, yh, zh;
	CentroHex3D(x, y, z, xh, yh, zh, 0);
	glTranslated(xh, yh, zh);

	//rotaciona
	glRotatef(30 * (float)frente, 0,0,1);

	//rederiza
	//glColor3ub(J[j].Cor[0], J[j].Cor[1], J[j].Cor[2]);
	
	//glEnable(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, textures[J[j].Textura]);
	//glCallList(gllist[GLLIST_JOG]);

	//teste
	/*glPushMatrix();
	glDisable(GL_LIGHT0);
	glDisable(GL_CULL_FACE);
	glColor3ub(255, 255, 255);

	glScalef(0.025f,0.025f,0.025f);
	glTranslated(0, 0, 24.0);
	glFrontFace(GL_CW);
	modelomd3.DrawModel();
	glFrontFace(GL_CCW);

	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHT0);
	glPopMatrix();*/
	//modelo.DisplayGL();
	
	J[j].modelo.DisplayGL();

	glPopMatrix();
}

void cJogo::Renderiza(double fps, bool selection, bool comburacos)
{
	if (!fundopreto) {
		static int tickantes, tickdepois, numframes = 0;
		if (numframes == 0)	tickantes = GetTickCount();

		if ((Estado == EST_JOGANDO)
		|| ((Estado == EST_MENSAGEM) && JogoComecou)) {
			J[vz].Fog->RenderizaInicio(selection, comburacos);

			//Ilumumina jogador
			if (!selection) {
				double xh, yh, zh;
				CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xh, yh, zh, 0.9);
				GLfloat pos[4] = {(float)xh, (float)yh, (float)zh, 1};
				glLightfv(GL_LIGHT0, GL_POSITION, pos);
			}

			//Rederiza jogadores
			for (int i=0; i<numjogs; i++)
			if (EstaVendo(i) || (i == vz && !selection)) {
				if (selection) glLoadName(Mapa.VetPos(J[i].x, J[i].y, J[i].z));
				if (fps > 0) {
					//Se estiver no meio de uma anima��o (NaoAnimando == 0)
					//Ent�o pode parar a anima��o quando a anima��o do modelo terminar
					//Sen�o mant�m NaoAnimando em 1
					FXuint loop = J[i].modelo.AnimAvanca(Aleatorio(1, 1.5) * 7.5 / fps);
					if (!NaoAnimando && loop && i == vz) NaoAnimando = 1;
				}
				DesenhaJogador(i);
			}

			if (!selection) {
				//Renderiza tiros
				for (i=0; i<contlinhatiro; i++) {
					cLinhaTiro &l = linhatiro[i];
					glColor3ub(50, 150, 50);
					//RenderizaLinha(l.largura, l.x1, l.y1, l.z1, l.x2, l.y2, l.z2);
					l.Renderiza();
				}

				//Renderiza sombra dos tiros
				glColor3ub(0, 0, 0);
				for (i=0; i<contsombratiro; i++) {
					//TO DO: As sombras devem ser faixas no ch�o, e n�o tubos
					sombratiro[i].Renderiza();
				}

				//Renderiza possivel alvo
				if (enuComando != CmdNada && J[vz].Fog->marca) {
					double xo, yo, zo, xa, ya, za;
					CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xo, yo, zo, 0.5);
					CentroHex3D(J[vz].Fog->marcax, J[vz].Fog->marcay, J[vz].Fog->marcaz, xa, ya, za, 0.5);
					
					glColor3ub(10, 10, 150);
					cLinhaTiro l(0.025, xo, yo, zo, xa, ya, za);
					l.Renderiza();

					glColor3ub(0, 0, 0);
					sombra_poss_mira.Gera(l, 0.025, xo, yo, zo, &Mapa);
					sombra_poss_mira.Renderiza();
				}

				//Renderiza alvo
				//TO DO: renderizar v�rias linhas para os passos do blaster
				//TO DO: fazer faixas na linha de mira (verde/amarelo/vermelho/preto) dependendo da mira atual do vz
				if (J[vz].GetFlag(Mirando)) {
					double xo, yo, zo, xa, ya, za;
					CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xo, yo, zo, 0.5);
					/*
					CentroHex3D(J[vz].AlvoX[0], J[vz].AlvoY[0], J[vz].AlvoZ[0], xa, ya, za, 0.5);
					/*/
					EsferToCart(J[vz].AlvoTeta[0], J[vz].AlvoPhi[0], J[vz].AlvoDist[0],
								xo, yo, zo,
								xa, ya, za);
					/**/
					
					double esccor = 0.15 * J[vz].MiraAtual * J[vz].MiraBonus;
					cLinhaTiro l(0.025, xo, yo, zo, xa, ya, za);
					l.Renderiza(esccor);
					
					glColor3ub(0, 0, 0);
					sombra_mira.Gera(l, 0.025, xo, yo, zo, &Mapa);
					sombra_mira.Renderiza();
				}
			}

			J[vz].Fog->RenderizaMeio(selection, J[vz].FogBit);
			J[vz].Fog->RenderizaFim(selection);

		} else if (Estado == EST_VENDOREPLAY && !selection) {
			J[vz].Fog->RenderizaInicio(selection, comburacos);

			//Ilumumina jogador
			if (!selection) {
				double xh, yh, zh;
				CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xh, yh, zh, 0.9);
				GLfloat pos[4] = {(float)xh, (float)yh, (float)zh, 1};
				glLightfv(GL_LIGHT0, GL_POSITION, pos);
			}

			//Rederiza jogadores
			DesenhaJogador(vz);
			DesenhaJogador(sim.idop, sim.x, sim.y, sim.z, sim.frente);

			//TO DO: mostrar efeitos para as a��es que o cara fez
			switch (sim.foto->acao) {
				//case AcaoMove:				break;
				//case AcaoVira:				break;
				case AcaoMudaCursor:		break;
				case AcaoAtira: {
				
					double xo, yo, zo;
					CentroHex3D(sim.x, sim.y, sim.z, xo, yo, zo, 0.5);

					glColor3ub(50, 150, 50);
					//RenderizaLinha(0.025, xo, yo, zo, sim.foto->inf[0].f, sim.foto->inf[1].f, sim.foto->inf[2].f);

					cLinhaTiro(0.025, xo, yo, zo,
									  sim.foto->inf[0].f, sim.foto->inf[1].f, sim.foto->inf[2].f)
									  .Renderiza();
				} break;
				case AcaoRecarrega:			break;
				case AcaoArremessa:			break;
				case AcaoLancaMagia:		break;
				case AcaoInstalaBomba:		break;
				case AcaoPega:				break;
				case AcaoSeTeletransporta:	break;
				case AcaoGolpeia:			break;
				case AcaoAbrePorta:			break;
			}

			J[vz].Fog->RenderizaMeio(selection, J[vz].FogBit);
			J[vz].Fog->RenderizaFim(selection);
		} else {
			Mapa.Renderiza(selection);
		}
	}
}
