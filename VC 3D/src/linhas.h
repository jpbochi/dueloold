
#ifndef LINHAS_H
#define LINHAS_H

#include "mapa.h"

class cLinhaTiro {
protected:
	friend class cLinhaSombra;
	friend class cJogo;
	double largura, x1, y1, z1, x2, y2, z2;

public:
	cLinhaTiro();
	cLinhaTiro(double _largura, double _x1, double _y1, double _z1, double _x2, double _y2, double _z2);
	
	cLinhaTiro &operator = (const cLinhaTiro &linha);
	bool        operator == (const cLinhaTiro &linha);
	bool        operator != (const cLinhaTiro &linha);

	void Inicia(double _largura, double _x1, double _y1, double _z1);
	void Move(double _x2, double _y2, double _z2);
	
	void Renderiza(double distcor = 0);
};

class cLinhaSombra
{
protected:
	cLinhaTiro linha_base;
	cMapa *pMapa;
	short ult_linha;
	cLinhaTiro linhas[32];
public:
	//BUG: poss�vel overflow nas linhas[]
	//     seria melhor uma lista encadeada, mas azar

	cLinhaSombra();
	
	void Inicia(double largura, double x1, double y1, double z1, cMapa *);
	void Move(int xi, int yi, double x2, double y2, double z2);
	
	void Renderiza();
	void Gera(cLinhaTiro &, double largura, double x1, double y1, double z1, cMapa *_pMapa);
};

#endif
