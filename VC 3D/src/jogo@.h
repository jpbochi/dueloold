
//#include "duelo3d.h"
#include "funcoes.h"
#include "conteudo.h"
#include "geral.h"
#include "projetil.h"
#include "jogo.h"
#include "iconebut.h"

#include <math.h>
//#include <stdio.h>

extern uint gllist[GLList::NUM_LISTS];
extern uint textures[MAX_TEXTURAS];
extern cJogo Jogo;

//teste
//#include "..\modelos\md3.h"
//extern CModelMD3 modelomd3;
#include "..\modelos\modelosmdx.h"
extern cModeloAnim modelo;

#define ManaIdentificacao	5
#define ManaPararTempo		55
#define ManaInvisibilidade	15
#define ManaTeletransporte	15
#define ManaImitar			20
#define ManaHolografia		10
#define ManaBolaDeFogo		25
#define ManaDardosMisticos	12

#define FeRezar				-25
#define FeCura				20
#define FeAnimar			15
#define FeRelampago			25

#define	TempoMirarMais		5
#define TempoPegar			15
#define	TempoSoltar			3
#define	TempoGuardar		10
#define	TempoSacar			10
#define	TempoVestirCabeca	20
#define	TempoVestirCorpo	50
#define	TempoDesvestirCabeca 10
#define	TempoDesvestirCorpo	30

#define	TempoAtropelar		15

#define	TempoAndarMoto		4
#define	TempoAndarRast		45
#define	TempoAndarAbai		25
#define	TempoAndarAbaiStraf	30
#define	TempoAndar			15
#define	TempoAndarStraf		20

#define TempoDefesaComEspada		20
#define TempoEsquivaMatrix			15
#define TempoIncubacaoAlien			30

#define DurezaPedra		1000
#define DurezaMedia		45
#define DurezaGrade		50
#define DurezaFraca		15
#define DurezaJog		30 //� somado com a blindagem

#define MAX_PROJETEIS	9

//atirar
#define PodNaoViraAoAtirar(K)		(J[K].Poder == pRobocop || J[K].Poder == pMosca || J[K].Poder == pTanque)
#define PodNaoPerdeMiraAoAndar(K)	(J[K].Poder == pRobocop || J[K].Poder == pTanque)
#define PodNaoAtingeGradesDist10(K) (J[K].Poder == pSabata)
#define PodMiraExtraPistolaERifle(K)(J[K].Poder == pSabata)

//pegar
#define PodNaoPodePegarNada(K)		(J[K].Poder == pRobocop || J[K].Poder == pJason || J[K].Poder == pZerg || J[K].Poder == pTanque || J[K].Poder == pAlienPequeno)
#define PodSoPegaItensSimples(K)	(J[K].Poder == pSabata)
#define PodDestroiObjetosDuros(K)	(J[K].Poder == pTarrasque || J[K].Poder == pDragao)
#define PodDestroiObjetos(K)		(PodDestroiObjetosDuros(K) || J[K].Poder == pBalacau || J[K].Poder == pAlienGrande || J[K].Poder == pPredador)
#define PodNaoDestroiDisco(K)		(J[K].Poder == pPredador)
#define PodDestroiKitMedico(K)		(J[K].Poder == pT_1000)
//#define PodDestroiCapacete(K)		(J[K].Poder == pMago)
//#define PodDestroiHolografia(K)		(J[K].Poder == pMago)
#define PodDestroiOculosIR(K)		(J[K].Poder == pMosca || J[K].Poder == pHomem_do_Raio_X)
#define PodNaoUsaColete(K)			(J[K].Poder == pNinja)
#define PodComeCorpos(K)			(J[K].Poder == pPredador || J[K].Poder == pWolverine || J[K].Poder == pTarrasque || J[K].Poder == pBalacau || J[K].Poder == pDragao || J[K].Poder == pAlienGrande)

//movimento
#define PodNaoUsaStrafe(K)			(J[K].Poder == pRobocop || J[K].Poder == pTanque)
#define PodAndaDeRe(K)				(J[K].Poder == pTanque)
#define PodAndaDevagarAoSerVisto(K) (J[K].Poder == pJason)
#define PodViraDevagar(K)			(J[K].Poder == pTanque)
#define PodAtravessaTijolo(K)		(J[K].Poder == pFantasma || J[K].Poder == pTarrasque || J[K].Poder == pDragao)
#define PodAtravessaMadeira(K)		(J[K].Poder == pRobocop || J[K].Poder == pBalacau || J[K].Poder == pAlienGrande || J[K].Poder == pPredador || J[K].Poder == pTanque || J[K].Poder == pT_1000)
#define PodAtravessaGrade(K)		(J[K].Poder == pT_1000 || J[K].Poder == pMagrao || J[K].Poder == pAlienPequeno)
#define PodQuebraParede(K)			(J[K].Poder == pTarrasque || J[K].Poder == pDragao)
//#define PodQuebraGrade(K)			(J[K].Poder == pT_1000) DESABILITADA
#define PodSabeArrombarPortas(K)	(J[K].Poder == pNinja)
#define PodRachaChao(K)				(J[K].Poder == pTarrasque)
#define PodFlutua(K)				(J[K].Poder == pVoador || J[K].Poder == pMosca || J[K].Poder == pEthereal || J[K].Poder == pFantasma)
#define PodVoa(K)					(J[K].Poder == pVoador || J[K].Poder == pDragao)
//2 observa��es falsas: PodFlutua(K) => PodVoa(K); PodVoa(K) => PodFlutua(K)

//corpo a corpo
#define PodBate(K)					(J[K].Poder == pAlienGrande || J[K].Poder == pPredador || J[K].Poder == pTarrasque || J[K].Poder == pBalacau || J[K].Poder == pDragao || J[K].Poder == pT_1000 || J[K].Poder == pTanque)
#define PodImpossibilitaEsquiva(K)	(J[K].Poder == pTarrasque || J[K].Poder == pDragao || J[K].Poder == pTanque)
#define PodDefendeComEspada(K)		(J[K].Poder == pJedi)
#define PodSuperEsquiva(K)			(J[K].Poder == pAgente_do_Matrix)
#define PodIncubacaoAlien(K)		(J[K].Poder == pAlienPequeno)
#define PodImuneIncubacaoAlien(K)	(J[K].Poder == pTanque)

//vis�o
#define PodSempreVeBotsNeutros(K)	(J[K].Poder == pAgente_do_Matrix)
#define PodSempreVeParasitados(K)	(J[K].Poder == pZerg)
#define PodVisaoRaioX(K)			(J[K].Poder == pHomem_do_Raio_X)
#define PodVisao360Graus(K)			(J[K].Poder == pMosca)

//outros
#define PodImuneAFogo(K)			(J[K].Poder == pRobocop || J[K].Poder == pDragao || J[K].Poder == pTanque)
#define PodResistenteAFogo(K)		(J[K].Poder == pT_1000)
#define PodImuneOvo(K)				(J[K].Poder == pT_1000 || J[K].Poder == pRobocop)
#define PodCamuflagemControlavel(K)	(J[K].Poder == pBalacau || J[K].Poder == pPredador)
#define PodEngole(K)				(J[K].Poder == pTarrasque)
#define PodSemRegeneracao(K)		(J[K].Poder == pT_1000 || J[K].Poder == pTanque || J[K].Poder == pRobocop)
#define PodRegeneracaoT1000(K)		(J[K].Poder == pT_1000)
#define PodCorpoFrio(K)				(J[K].Poder == pT_1000)
#define PodPodeExplodirTanque(K)	(J[K].Poder == pTanque)
#define PodPodeExplodirDinamite(K)	(J[K].Poder == pDinamitador)
#define PodSubstituiBot(K)			(J[K].Poder == pAgente_do_Matrix)
#define PodRessuscita(K)			(J[K].Poder == pJason)
#define PodUsaMagiasArcanas(K)		(J[K].Poder == pMago)
//TO DO: PodCausaMedo; PodNaoSenteMedo

//armas
//TO DO: Transformar estas coisa feias abaixo em flags
#define ArmaNaoViraAoAtirar(L)	(L == aDisco || L == aShuriken)
#define ArmaEhIncendiaria(L)	(L == aLancaChamas || L == aBaforada || L == aBolaDeFogo)
#define ArmaRecarregaComMira(L) (L == aCanhao || L == aCanhao)
#define ArmaEhCorpoACorpo(L)	(L >=0 && Armas[abs(L)].DanoGolpe > 0)
//#define ArmaEhExplosiva(L)		(L >=0 && Armas[abs(L)].RaioExplosao > 0)
