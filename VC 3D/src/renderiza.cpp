
#include "winmain.h"

#include "declarac.h"
#include "texturas.h"
extern FXuint textures[MAX_TEXTURAS];

#include "jogo.h"
extern cJogo Jogo;

//teste
//#include "..\modelos\md3.h"
//CModelMD3 modelomd3;
//#include "..\modelos\modelosmdx.h"
//#include "..\modelos\modelosmd3.h"
//cModeloAnim modelo;

bool CarregaTexturas(FXApp *app)
{
	const NAO_TRANSPARENTE = false;
	
	if (!CarregaTextura(app, "imagens\\tile_t10_64.pcx", textures[0], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\ground32.pcx", textures[255], NAO_TRANSPARENTE)) return false;

	//objetos
	//if (!CarregaTextura(app, "imagens\\0.bmp", textures[2])) return false;
	if (!CarregaTextura(app, "imagens\\plank2_t_128.pcx", textures[1], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\02-Sangue.bmp", textures[2])) return false;
	if (!CarregaTextura(app, "imagens\\03-Esping.bmp", textures[3])) return false;
	if (!CarregaTextura(app, "imagens\\04-Metralha.bmp", textures[4])) return false;
	if (!CarregaTextura(app, "imagens\\brick01_64.pcx", textures[5], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\06&33-MinaAtiv.BMP", textures[6])) return false;
	if (!CarregaTextura(app, "imagens\\07-Minas2.BMP", textures[7])) return false;
	if (!CarregaTextura(app, "imagens\\08-lancfog2.BMP", textures[8])) return false;
	if (!CarregaTextura(app, "imagens\\09&10-PassEdit.BMP", textures[9], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\11-Rifle.BMP", textures[11])) return false;
	if (!CarregaTextura(app, "imagens\\12-Degenera.BMP", textures[12])) return false;
	if (!CarregaTextura(app, "imagens\\13-Granada.BMP", textures[13])) return false;
	if (!CarregaTextura(app, "imagens\\14-GranadaA.BMP", textures[14])) return false;
	if (!CarregaTextura(app, "imagens\\15-Medkit2.BMP", textures[15])) return false;
	if (!CarregaTextura(app, "imagens\\16-Esquelet.BMP", textures[16])) return false;
	if (!CarregaTextura(app, "imagens\\17-Disco.BMP", textures[17])) return false;
	if (!CarregaTextura(app, "imagens\\18-SangPred.BMP", textures[18])) return false;
	if (!CarregaTextura(app, "imagens\\19-AutoCann.BMP", textures[19])) return false;
	if (!CarregaTextura(app, "imagens\\20-Elevador.BMP", textures[20])) return false;
	if (!CarregaTextura(app, "imagens\\21-LancaCha.BMP", textures[21])) return false;
	if (!CarregaTextura(app, "imagens\\22-Fumaca.BMP", textures[22])) return false;
	if (!CarregaTextura(app, "imagens\\23-Revolver.BMP", textures[23])) return false;
	if (!CarregaTextura(app, "imagens\\24&25-BuracInv.BMP", textures[24], NAO_TRANSPARENTE)) return false;
	//if (!CarregaTextura(app, "imagens\\24&25-BuracInv.BMP", textures[25])) return false;
	if (!CarregaTextura(app, "imagens\\26-BuracVis.BMP", textures[26], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\27-Ricochet.BMP", textures[27])) return false;
	if (!CarregaTextura(app, "imagens\\28-Capacete.BMP", textures[28])) return false;
	if (!CarregaTextura(app, "imagens\\29-Oculos2.BMP", textures[29])) return false;
	if (!CarregaTextura(app, "imagens\\30-Bomba.BMP", textures[30])) return false;
	if (!CarregaTextura(app, "imagens\\31-Fogo.BMP", textures[31])) return false;
	if (!CarregaTextura(app, "imagens\\32-Lata2.BMP", textures[32], NAO_TRANSPARENTE)) return false;
	//if (!CarregaTextura(app, "imagens\\06&33-MinaAtiv.BMP", textures[33])) return false;
	if (!CarregaTextura(app, "imagens\\lagoon_128.pcx", textures[34], NAO_TRANSPARENTE)) return false;//if (!CarregaTextura(app, "imagens\\34-Vidro.BMP", textures[34])) return false;
	if (!CarregaTextura(app, "imagens\\35-Teletran.BMP", textures[35])) return false;
	if (!CarregaTextura(app, "imagens\\36-Escuro.BMP", textures[36], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\37-LerdArma.BMP", textures[37])) return false;
	if (!CarregaTextura(app, "imagens\\38-TeleArma.BMP", textures[38])) return false;
	if (!CarregaTextura(app, "imagens\\39-Caixa.BMP", textures[39])) return false;
	if (!CarregaTextura(app, "imagens\\40-FogoInap.BMP", textures[40])) return false;
	if (!CarregaTextura(app, "imagens\\41-Blaster.BMP", textures[41])) return false;
	if (!CarregaTextura(app, "imagens\\42-SangAlien.BMP", textures[42])) return false;
	if (!CarregaTextura(app, "imagens\\43-Burac%.BMP", textures[43], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\44-Grade.png", textures[44])) return false;//if (!CarregaTextura(app, "imagens\\44-Grade.BMP", textures[44])) return false;
	if (!CarregaTextura(app, "imagens\\45-Parasita.BMP", textures[45])) return false;
	if (!CarregaTextura(app, "imagens\\46-Escudo.BMP", textures[46])) return false;
	if (!CarregaTextura(app, "imagens\\brik_t14_64.pcx", textures[47], NAO_TRANSPARENTE)) return false;//if (!CarregaTextura(app, "imagens\\47-Paredao.BMP", textures[47])) return false;
	if (!CarregaTextura(app, "imagens\\48-Porta.BMP", textures[48], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\49-Chave2.BMP", textures[49])) return false;
	//if (!CarregaTextura(app, "imagens\\???.BMP", textures[50])) return false; //porta aberta
	if (!CarregaTextura(app, "imagens\\51-Holog.BMP", textures[51])) return false;
	if (!CarregaTextura(app, "imagens\\52-ElevadSb.BMP", textures[52], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\53-ElevadDc.BMP", textures[53], NAO_TRANSPARENTE)) return false;
	if (!CarregaTextura(app, "imagens\\54-MinAtiv%.BMP", textures[54])) return false;
	if (!CarregaTextura(app, "imagens\\55-Mg42.BMP", textures[55])) return false;
	if (!CarregaTextura(app, "imagens\\56-Detec.BMP", textures[56])) return false;
	if (!CarregaTextura(app, "imagens\\57-Trabuco.BMP", textures[57])) return false;
	if (!CarregaTextura(app, "imagens\\58-Caixarm.BMP", textures[58])) return false;
	if (!CarregaTextura(app, "imagens\\59-Moto.BMP", textures[59])) return false;

	if (!CarregaTextura(app, "imagens\\agente.bmp", textures[240])) return false;
	if (!CarregaTextura(app, "imagens\\diablo2.bmp", textures[241])) return false;
	if (!CarregaTextura(app, "imagens\\medo.bmp", textures[242])) return false;
	if (!CarregaTextura(app, "imagens\\pistolei.bmp", textures[243])) return false;
	if (!CarregaTextura(app, "imagens\\profissi.bmp", textures[244])) return false;
	if (!CarregaTextura(app, "imagens\\velho.bmp", textures[245])) return false;
	
	return true;
}

bool InitGL()
{
	glShadeModel(GL_SMOOTH);                          // Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);             // Black Background
	glClearDepth(1.0f);                               // Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);                          // Enables Depth Testing
	glDepthFunc(GL_LEQUAL);                           // The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);// Really Nice Perspective Calculations
	glEnable(GL_COLOR_MATERIAL);					  // Enable Coloring Of Material
	glEnable(GL_TEXTURE_2D);						  // switch on texturing

	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	//glDisable(GL_DITHER);

	//Transpar�ncia
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	//Luz ambiente 
	GLfloat luzAmbiente0[4] = {0.5,0.5,0.5,1.0}; 
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente0);

	//Luz Zero
	GLfloat luzDifusa0[4] =		{.8f, .5f, .8f, 1.0};
    GLfloat luzEspecular0[4] =	{0.0, 0.0, 0.0, 1.0};
	GLfloat posicaoLuz0[4] =	{10.0, 10.0, 0.0, 1.0};
	GLfloat direcaoLuz0[4] =	{1.0, 1.0, 0.0, 1.0};
	//glLightfv(GL_LIGHT0, GL_AMBIENT,				luzAmbiente0);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,				luzDifusa0);
	//glLightfv(GL_LIGHT0, GL_SPECULAR,				luzEspecular0);
    glLightfv(GL_LIGHT0, GL_POSITION,				posicaoLuz0);
    //glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION,			direcaoLuz0);
    //glLightf(GL_LIGHT0, GL_SPOT_CUTOFF,				10.0);
    //glLightf(GL_LIGHT0, GL_SPOT_EXPONENT,			100.0);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION,		0.2f);
    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION,	0.0005f);
    
    glLightfv(GL_LIGHT1, GL_AMBIENT,				luzAmbiente0);

	//Liga as luzes
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	return true;
}


bool winMain::CarregaModelos()
{
	static criou = false;
	if (criou) {
		return true;
	} else {
		criou = true;

		CarregaTexturas(getApp());
		
		GeraGLListas();
		
		//Jogo.pMapa->CarregaMapa("mapas\\feudo.map");
		Jogo.pMapa->CarregaMapa("mapas\\pontos.map");
		Jogo.pMapa->CentraCam(Jogo.pMapa->dimX / 2, Jogo.pMapa->dimY / 2, Jogo.pMapa->dimZ / 2);
		
		//Jogo.pMapa->Cam.SetPos(10, 10, 0.5);
		//Jogo.pMapa->Cam.SetLookTo(10, 10, 0, true);
		
		Jogo.MudaEstado(EST_EDITANDO);
		
		//extern FXApp *md3_pApp;
		//md3_pApp = getApp();
		//modelomd3.LoadModel("Data\\md3\\sonic", "aquatic");
		//modelomd3.LoadWeapon("Data\\md3\\sonic", "railgun");
		//modelomd3.LoadModel("Data\\md3\\fang", "gibasaur");
		//modelomd3.LoadModel("Data\\md3\\stormtrooper", "default");
		//modelomd3.SetTorsoAnimation("TORSO_STAND");
		//modelomd3.SetLegsAnimation("LEGS_RUN");

		//if (!modelo.Carrega(getApp(), 0, "sonic", "aquatic")) fxwarning("afff!");

		//ModQualq *modqualq;
		//modqualq = new ModeloMD3Anim;
		//if (!modelo.Carrega(getApp(), modqualq, "sonic", "aquatic")) fxwarning("afff!");
		//modelo.AnimInicia(cModeloAnim::ANIM_STAND);
		//modelo.AnimInicia(cModeloAnim::ANIM_SWIN);
		//modelo.AnimInicia(cModeloAnim::ANIM_ATTACK1);

		//######################### Load the model file
		/*ModeloMD2 *modelo = new ModeloMD2;
		
		//modelo.Carrega(getApp(), "robocop", textures[511], "murphy");
		//modelo.Carrega(getApp(), "swindle", textures[511]);
		//Jogo.modelo.Carrega(getApp(), "uberdog", textures[511], "uber");
		//Jogo.modelo.Carrega(getApp(), "hunter", textures[511]);
		//Jogo.modelo.Carrega(getApp(), "ogro", textures[511], "cloth");
		
		const FXString strMod = "pmarine";
		const FXString strTex = "usmc_claro";
				
		if (!modelo->Carrega(getApp(), strMod)) {
			fxwarning("Erro ao carregar modelo ''%s''!", strMod);
			return false;
		}
		
		if (!Jogo.modelo.CarregaTextura(getApp(), textures[511], strTex)) {
			fxwarning("Erro ao carregar textura ''%s'' do modelo ''%s''!", strTex, strMod);
			return false;
		}
		
		Jogo.modelo.AnimInicia(ModeloMD2Anim::ANIM_STAND);*/
		//modelo.AnimInicia(ModeloMD2Anim::ANIM_RUN);
		//modelo.AnimInicia(ModeloMD2Anim::ANIM_CROUCH_STAND);
		//modelo.AnimInicia(ModeloMD2Anim::ANIM_CROUCH_WALK);

		return true;
	}
}
  
void winMain::drawScene()
{
	//const GLfloat lightPosition[]={15.,10.,5.,1.};
	const GLfloat lightPosition[]={0.,0.,5.,1.};
	const GLfloat lightAmbient[]={.5f,.5f,.5f,1.};
	const GLfloat lightDiffuse[]={1.9f,1.9f,1.9f,1.};
	const GLfloat redMaterial[]={1.,0.,0.,1.};
	const GLfloat blueMaterial[]={0.,0.,1.,1.};
	const GLfloat weissMaterial[]={0.9f,0.9f,0.9f,1.};


	// Make context current
	glcanvas->makeCurrent();

	//### Isto est� sendo feito no onResize()
	//GLdouble width = glcanvas->getWidth();
	//GLdouble height = glcanvas->getHeight();
	//GLdouble aspect = height>0 ? width/height : 1.0;
	//glViewport(0,0,glcanvas->getWidth(),glcanvas->getHeight());
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//gluPerspective(75.,aspect,0.1,50.);

	//### Limpa a imagem
	glClearColor(0.70f, 0.85f, 0.99f,1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	//if (fps > 0) Jogo.modelo.AnimAvanca(Aleatorio(1) * 7.5 / fps);
	Jogo.Renderiza(fps);
	
	/*
	Jogo.pMapa->RenderizaInicio();

	//glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	//glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	//glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	//glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHTING);

	double xh, yh, zh;
	CentroHex3D(10, 17, 0, xh, yh, zh, 0);

	GLfloat pos[4] = {(float)xh, (float)yh, (float)zh + 2.5f, 1};
	glLightfv(GL_LIGHT0, GL_POSITION, pos);
 
	glMaterialfv(GL_FRONT, GL_AMBIENT, weissMaterial);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, weissMaterial);

	glPushMatrix();

	//glTranslated(xh, yh, zh);
	//glRotated(rot_z, 0., 0., 1.);
	//glTranslated(-xh, -yh, -zh);

	Jogo.pMapa->RenderizaMeio();

	glPopMatrix();
	
	glColor3ub(255, 255, 255);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[0]);

	glPushMatrix();

	glTranslated(xh, yh, zh);
  
	glRotated(2*rot_z, 0., 0., 1.);

	glDisable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	modelo.DisplayGL();
	glDisable(GL_LIGHT1);
	glEnable(GL_LIGHT0);
	if (fps > 0) modelo.AnimAvanca(7.5 / fps);

	//for (int i=0; i<20; i++) {
	//	glTranslated(.5, .5, 0);
	//	modelo.DisplayGL();
	//}

	glPopMatrix();

	Jogo.pMapa->RenderizaFim();

	*/

	// Swap if it is double-buffered
	if(glvisual->isDoubleBuffer()){
		glcanvas->swapBuffers();
	}

	// Make context non-current
	glcanvas->makeNonCurrent();
  
	cont_frames[fps_amostra]++;
}