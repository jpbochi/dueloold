
#include "winMain.h"
#include "appduelo.h"
#include "OpenGLInfo.h"

#include "winNovoJogo.h"
#include "winEquip.h"

#include "renderiza.h"
#include "geral.h"
#include "mouse.h"
#include "declarac.h"

#include "texturas.h"

//#include "..\nehe\opengl.h"
//using namespace NeHe;
//printf("%c",isprint(tmp[i]) ? tmp[i] : '.');

#include "arquivos.h"
using namespace ARQ;

#include "jogo.h"
extern cJogo Jogo;

//glcanvas->makeCurrent() deve ser chamado antes de qualquer chamada da OpenGL
#include "glcontext.h"

#include "iconebut.h"

extern FXuint textures[MAX_TEXTURAS];

//Vers�es:
// v0.0.26 - ...
// v0.0.25 - 2-abr-2005 - testes e bugs
// v0.0.24 - 1�-abr-2005 - movimenta��o de c�mera pelo mouse iniciada
// v0.0.23 - 29-mar-2005 - MD3 usando normais; dano de fogo/�cido modificado (a testar)
// v0.0.22 - 24-mar-2005 - me livrei do Nehe de vez (descobri que o MD3 n�o usa normais!)
// v0.0.21 - 23-mar-2005 - me livrei do Nehe (s� falta acertar as anima��es de novo)
// v0.0.20 - 19-mar-2005 - modo de mira terminado (acho); afinamento do ch�o feito (bugs na sombra aumentaram)
// v0.0.19 - 9-mar-2005 - modo de mira modificado, faltam ajustes
// v0.0.18 - 8-mar-2005 - alvo da mira dos jogadores agora em coordenadas polares
// v0.0.17 - 7-mar-2005 - ...
// v0.0.16 - 4-mar-2005 - separa��o do arquivo jogo.cpp, entre outras coisas
// v0.0.15 - 3-mar-2005 - ...
// v0.0.14 - 3-mar-2005 - ...
// v0.0.13 - 2-mar-2005 - in�cio: melhoria no tratamento de cliques na tela de equipamentos
// v0.0.12 - 28-fev-2005 - desenhos improvisados dos cartuchos, entre outras coisas
// v0.0.11 - 27-fev-2005 - mira em 1� pessoa iniciada
// v0.0.10 - 25-fev-2005 - ...
// v0.0.09 - 24-fev-2005 - ...
// v0.0.08 - 23-fev-2005 - ...
// v0.0.07 - 22-fev-2005 - reforma no mecanismo das armas para objetos iniciada

/*
Tarefas:
Fun��es incompletas ou inexistentes:
	- cObjeto::AplicaEfeitoVeste(), PenalidadeDuasMaos();
	- cJogo::PassaVez() e RecebeVez(); v�rias regras
	- cJogo::Fogo(...); v�rias regras
		- quando tiro � defendido com a espada: ricochetear ou parar o proj�til?
	- cJogo::Teletransporta(?);
	- cJogo::OndePisa(...);  teste se pisou em minas, parasita, ...
Importantes:
	- Jedi leva menos dano ao defender tiro explosivo com a espada
	- tratar cliques fora do mapa
	- op��o decente para rever replay
	- arrumar as linhas de sombra
	- mira em 1� pessoa
		- op��o de usar a mira tradicional: como? ctrl pode alternar de um modo pra outro...
		- mostar um cilindro em volta dos oponentes que representa o formato real deles
	- implementar a utiliza��o dos itens
	- arranjar �cones para todos os itens
	- janela de equipamento:
		- trocar item de uma m�o para outra;
		- trocar o item selecionado (clicar em um item quando outro j� est� selecionado)
		- terminar o sistema de descri��o de itens
	- Alterar o m�todo de dano no fogo/sangue alien
	- arrumar a maneira como os itens s�o mostrados no mapa (�cone que resume os itens)
		- criar objetos nem-sempre-vis�veis:
			- parasita do zerg, mina ativada, bomba
			- bomba do predador morto: como grudar ele em um corpo
	- afinar paredes (falta: afinar ch�o);
	- anima��o: usar anima��es (se abaixar, andar abaixado, morrer, etc.);
	- terminar o HUD;
	- reformar esquema de equipamentos/invent�rio:
		- melhorar o tratamento de double-click na janela de equipamento
		- fazer os jogadores come�arem com alguma coisa na m�o
		- inserir peso nos itens e fazer ele afetar os jogadores
		- desabilitar os movimentos ilegais na janela de equipamentos
		- equipamento inicial (em cJogador::AcionaPoder())
			- falta o equipamento
		- possibilitar que armas possam ter v�rios tipos de muni��o (Dano, Expl e Pirotec(N�O existe ainda (flag?));
			- juntar canh�o AP e HE;
			- como mostrar que tipo de muni��o est� na arma?
			- icSoco � desnecess�rio, j� que o cara n�o precisa de um item aSoco;
			- como associar a muni��o � arma? que outros objetos podem ser associados?
				- arma e muni��o, corpo e bomba de predador;
			- onde guardar a informa��o sobre quanto de mana gasta uma arma/skill?
			- atributo extra: S�-Atira-Uma-Vez-Por-Rodada; serve para Baforada e Canh�o...
				- id�ia: usar o TRec! tb pode indicar se uma arma usa Mana/F�
		- problema: como fazer para pegar a distancia (Jedi)? e soltar a dist�ncia? Arremessando!
		- alguns skills ser�o transformados em equipamentos (ex.: Snikt) (um flag (EhSkill) os identificar�);
		- skills que s�o, ao mesmo tempo, armas podem gerar bugs estranhos => inventar uma boa solu��o
		- function pointers podem ser usados para caracterizar o funcionamento dos itens ???;
Demoradas ou de menor prioridade:
	- explos�es!
	- terminar cMapa::EsvaziaObjetos() (incluir um fun��o em cObjetos para apagar tudo)
	- como fazer para o tiro do Blaster ?!?!?
	- preview dos modelos
	- modelo do Alien (tem que mudar quando ele vira pAlienGrande)
	- variar a textura dos poderes com modelo especial (tem que ler as poss�veis texturas antes)
	- fazer o replay voltar a funcionar
	- mudar o formato da grade
	- elevador como no ufo (marcar de algum jeito, inclusive no ch�o)
	- encontrar imagens para os bot�es
	- anima��o: mostrar armas;
	- tra�ar v�rias linhas (ao inv�s de uma) para testar se pode ver cada oponente
	- help;
	- os caras n�o podem voar/flutuar se n�o estiverem na posNormal
	- a fun��o cReplay::TocaSom(short id) s� funciona em Windows
	- ao explorar, caras parasitados por um Zerg aumentam o Fog do Zerg tb
	X sombras: se ch�o do andar de cima n�o for buraco, escurecer
	- pulos!
	- fazer ricocheteadora voltar a funcionar
	- void cMapa::AplicaNewton(); //gravidade aos objetos e �s paredes
FEATURES (minor bugs):
	- (FIXED?)a perda de mira ao alterar o alvo est� esquisita
	- PodCorpoFrio poderia incluir Jason e Fantasma
	- o mosca n�o deveria poder usar capacete
	- "ele defendeu o tiro com a espada", mas o tiro continuou andando!
	- a quantidade de radar dos poderes est� 'hardcoded'
	- as mudan�as em cJogo::LinhaFog ...???
	- mudar o nome da granada para 'granada transdimensional'
	- a sombra vertical pode passar por uma parede que n�o existe
	- a linha de mira pode ficar preta (poss�vel confus�o com as sombras);
	- se o cara estava mirando e tenta andar, mas n�o tem tempo, perde a mira mesmo assim
	- avisar antes de deixar algu�m mirar com uma arma descarregada
	- estrelinhas tem que recarregar a cada tiro (??)
BUGs:
	- (FIXED?)se, ao pegar algo do chao e tentar por na mochila, s� houver tempo de pegar na m�o a tela de quipamentos n�o � atualizada
	- (FIXED?)zoom na roda do mouse deve voltar ao normal depois de mirar
	- a "marca" no mapa deveria ser desenhada antes do mapa em si pq os itens transparentes escondem ela e n�o deveriam
	- o indicador tosco (�, |, �) do filtro de andares n�o � atualizado corretamente
	- sangue, minas e outros objetos n�o deviam aparecer entre os equipamentos
	- a sombra da mira s� depende do centro do hex�gono mirado
	- filtro de andares no modo s�-andar-selecionado n�o funciona (mostra andares de baixa dependendo da c�mera
	- (FIXED?)"venha, %s!" e ainda mostra a tela do cara anterior
	- no fim do jogo, ele inseriu um corpo no lugar do vencedor e s� tinha sangue no lugar do morto
		- al�m disso, ele n�o sumiu com o hud
	- seq: 1) mire; 2) tente [mirar em outra dire��o/andar], mas n�o deve sobrar tempo pra mirar/andar
		- efeito: o cara se vira; reclama que n�o teve tempo; e fica mirando de lado!
	- quando mensagens aparecem e algum bot�o de movimento pelo mapa est� apertado shit happens!
	- o drag�o cai em buracos vis�veis e n�o devia
	- o afinamento n�o � apagado quando a parede � destru�da
	- ????: um cara come�ou a sangrar do nada rodada ap�s rodada
	- a pergunta "vc quer procurar passagem secreta?" s� aparece quando � passagem secreta!!!
	- anima��o ao subir elevador
	- (FIXED?)no strafe a cara sempre gasta mais tempo, mesmo que n�o precise;
	- (FIXED?)um balacau atravessou uma grade
	- (OLD?)quando algu�m morre, n�o some imediatamente da tela, nem aparece o corpo

Sugest�es:
	- j� que n�o existe mais soco, criar a Faca
	- Parede de espelho
	- Vampiro
		- se transforma em morcego
		- golpe especial
		- n�o tem reflexo no espelho
	- Poderes �nicos
		- pot�ncia das vers�es �nicas � inversamente proporcional � pot�ncia do poder original
*/

FXDEFMAP(winMain) winMainMap[]={
   //________Message_Type_____________________ID____________Message_Handler_______
	FXMAPFUNC(SEL_PAINT,			winMain::ID_CANVAS,		winMain::onExpose),
	FXMAPFUNC(SEL_CONFIGURE,		winMain::ID_CANVAS,		winMain::onResize),
	FXMAPFUNC(SEL_CHORE,			winMain::ID_CHORE,		winMain::onChore),
	FXMAPFUNC(SEL_TIMEOUT,			winMain::ID_TIME_FPS,	winMain::onTimeFPS),
	FXMAPFUNC(SEL_TIMEOUT,			winMain::ID_TIME_INPUT,	winMain::onTimeInput),
	FXMAPFUNC(SEL_TIMEOUT,			winMain::ID_TIME_ESPERA,winMain::onTimerEspera),

	FXMAPFUNC(SEL_FOCUSIN,			0,						winMain::onFocusIn),
	FXMAPFUNC(SEL_FOCUSOUT,			0,						winMain::onFocusOut),

	FXMAPFUNC(SEL_KEYPRESS,			0,						winMain::onCmdButtonPressed),
	FXMAPFUNC(SEL_KEYRELEASE,		0,						winMain::onCmdButtonPressed),

	FXMAPFUNC(SEL_ENTER,			winMain::ID_CANVAS,		winMain::onMouseInCanvas),
	FXMAPFUNC(SEL_LEAVE,			winMain::ID_CANVAS,		winMain::onMouseOutCanvas),
	FXMAPFUNC(SEL_MOTION,			winMain::ID_CANVAS,		winMain::onMouseMove),

	FXMAPFUNC(SEL_LEFTBUTTONPRESS,winMain::ID_CANVAS,		winMain::onMouseDown),
	FXMAPFUNC(SEL_MIDDLEBUTTONPRESS,winMain::ID_CANVAS,		winMain::onMouseDown),
	FXMAPFUNC(SEL_RIGHTBUTTONPRESS,winMain::ID_CANVAS,		winMain::onMouseDown),
	FXMAPFUNC(SEL_LEFTBUTTONRELEASE,winMain::ID_CANVAS,		winMain::onMouseUp),
	FXMAPFUNC(SEL_MIDDLEBUTTONRELEASE,winMain::ID_CANVAS,	winMain::onMouseUp),
	FXMAPFUNC(SEL_RIGHTBUTTONRELEASE,winMain::ID_CANVAS,	winMain::onMouseUp),

	FXMAPFUNC(SEL_MOUSEWHEEL,		winMain::ID_CANVAS,		winMain::onMouseWheel),

	FXMAPFUNC(SEL_COMMAND,			winMain::ID_OPENGL,		winMain::onCmdOpenGLPressed),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_BUTTON,		winMain::onCmdButtonPressed),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_ABRIRMAPA,	winMain::onMenuAbrirMapa),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_NOVOJOGO,	winMain::onMenuNovoJogo),
		
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_PASSARVEZ,	winMain::onPassarVez),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_EQUIP,		winMain::onEquip),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_MIRAR,		winMain::onMirar),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_MIRARMAIS,	winMain::onMirarMais),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_ATIRAR,		winMain::onAtirar),
	//FXMAPFUNC(SEL_COMMAND,			winMain::ID_RECARREGAR,	winMain::onRecarregar),
	//FXMAPFUNC(SEL_COMMAND,			winMain::ID_TROCAR,		winMain::onTrocar),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_MAODIR,		winMain::onMaoDir),
	FXMAPFUNC(SEL_COMMAND,			winMain::ID_MAOESQ,		winMain::onMaoEsq),
};

FXIMPLEMENT(winMain,FXMainWindow,winMainMap,ARRAYNUMBER(winMainMap))

#ifdef MODO_TOSCO
winMain::winMain(FXApp * a)	:FXMainWindow(a,"Duelo 3D",NULL,NULL,DECOR_ALL,0,0,200,150)
#else
winMain::winMain(FXApp * a)	:FXMainWindow(a,"Duelo 3D",NULL,NULL,DECOR_ALL,0,0,800,600)
#endif
{
	//FXAccelTable *accel = this->getAccelTable();
	//accel->addAccel(MKUINT(KEY_T,ALTMASK), this, 666, 667);
	
	//######################### Data Targets
	rot_z = 0;
	//Jogo.pMapa->Cam.teta;
	target_rot_z.connect(rot_z);

	//######################### Icone
 	FXFileStream File;
 	File.open(ARQ::IconeMain, FX::FXStreamLoad);

	icoMain = new FXICOIcon(getApp());
	icoMain->loadPixels(File);
	File.close();

	setIcon(icoMain);

	//######################### Menu
	FXMenuBar *menubar = new FXMenuBar(this,LAYOUT_SIDE_TOP|LAYOUT_FILL_X);

	filemenu = new FXMenuPane(this);
	new FXMenuCommand(filemenu,"&Novo Jogo\tCtl-N",NULL,this,			winMain::ID_NOVOJOGO);
	new FXMenuSeparator(filemenu);
	new FXMenuCommand(filemenu,"A&brir Mapa...\tCtl-O",NULL,this,		winMain::ID_ABRIRMAPA);
	new FXMenuCommand(filemenu,"&Salvar Mapa como...\tCtl-S",NULL,this,	winMain::ID_SALVARMAPA);
	new FXMenuSeparator(filemenu);
	new FXMenuCommand(filemenu,"&Prefer�ncias",NULL,this,				winMain::ID_PREF);
	new FXMenuCommand(filemenu,"&Estat�sticas",NULL,this,				winMain::ID_ESTATISTICAS);
	new FXMenuCommand(filemenu,"&Op��es",NULL,this,						winMain::ID_OPCOES);
	new FXMenuCommand(filemenu,"Sobre",NULL,this,						winMain::ID_SOBRE);
	new FXMenuSeparator(filemenu);
	new FXMenuCommand(filemenu,"Sai&r\tAlt-F4\tSai do jogo.",NULL,getApp(),	FXApp::ID_QUIT);
	
	editmenu = new FXMenuPane(this);
	new FXMenuCommand(editmenu,"Press Button",NULL,this,winMain::ID_BUTTON);
	
	new FXMenuTitle(menubar,"&Menu",NULL,filemenu);
	new FXMenuTitle(menubar,"&Editar",NULL,editmenu);
	
	//######################### SubMenu
	subeditmenu = new FXMenuPane(this);
	new FXMenuCommand(subeditmenu,"Press Button 2",NULL,this,winMain::ID_BUTTON);	

	new FXMenuCascade(editmenu,"Extra Options",NULL,subeditmenu);
  	
	//widget(parentwindow,.....,target,messageid,options,x,y,w,h,pl,pr,pt,pb,hs,vs);
	//new FXButton(this,"aaahh",FXStarwarsIco,this,winMain::ID_BUTTON,
	//			 FX::BUTTON_NORMAL | FX::LAYOUT_EXPLICIT,// | FX::BUTTON_INITIAL,
	//			 10,0, 100, 100);
	//new FXButton(this,"Bot�o",NULL,this,winMain::ID_BUTTON);

	splitter = new FXSplitter(this,LAYOUT_SIDE_TOP|LAYOUT_FILL_X|LAYOUT_FILL_Y|SPLITTER_REVERSED|SPLITTER_TRACKING|SPLITTER_VERTICAL);
	splitter->setBarSize(2);
	
	splitter->disable();
	
	//######################### OpenGL Frame
	FXVerticalFrame *glcanvasFrame;
	FXComposite *glpanel;
	//FXHorizontalFrame *frame;

	//frame=new FXHorizontalFrame(this,LAYOUT_SIDE_TOP|LAYOUT_FILL_X|LAYOUT_FILL_Y,0,0,0,0, 0,0,0,0);
	//frame=new FXHorizontalFrame(splitter,LAYOUT_SIDE_TOP|LAYOUT_FILL_X|LAYOUT_FILL_Y,0,0,0,0, 0,0,0,0);

	// LEFT pane to contain the glcanvas
	glcanvasFrame=new FXVerticalFrame(splitter,LAYOUT_FILL_X|LAYOUT_FILL_Y|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,2,2,0,0);

	// Drawing glcanvas
	//glpanel=new FXVerticalFrame(glcanvasFrame,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X|LAYOUT_FILL_Y|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0, 0,0,0,0);
	glpanel=new FXVerticalFrame(glcanvasFrame,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X|LAYOUT_FILL_Y|LAYOUT_TOP,0,0,0,0, 0,0,0,0);
	// A Visual to drag OpenGL
	//glvisual=new FXGLVisual(getApp(),VISUAL_DOUBLEBUFFER|VISUAL_STEREO);
	glvisual=new FXGLVisual(getApp(),VISUAL_DOUBLEBUFFER|VISUAL_SWAP_COPY);
	//glvisual=new FXGLVisual(getApp(),VISUAL_DOUBLEBUFFER|VISUAL_SWAP_COPY|VISUAL_NOACCEL);
	// Drawing glcanvas
	glcanvas=new FXGLCanvas(glpanel,glvisual,this,ID_CANVAS,LAYOUT_FILL_X|LAYOUT_FILL_Y|LAYOUT_TOP|LAYOUT_LEFT);
	
	//glcanvas->makeCurrent() deve ser chamado antes de qualquer chamada da OpenGL
	GLContexto.SetaContexto(glcanvas);

	/*//dial de rota��o
	dial_rot_z = new FXDial(glcanvasFrame,&target_rot_z,FXDataTarget::ID_VALUE,LAYOUT_FILL_X|DIAL_HORIZONTAL|DIAL_HAS_NOTCH|DIAL_CYCLIC);
	dial_rot_z->setRange(0, 360);
	dial_rot_z->setRevolutionIncrement(360);*/

	new FXHorizontalSeparator(glcanvasFrame,SEPARATOR_RIDGE|LAYOUT_FILL_X);

	//######################### Frame com o HUD
	//FXVerticalFrame *frmJogo;
	frmJogo=new FXVerticalFrame(splitter,LAYOUT_FILL_Y|LAYOUT_FILL_X|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,2,2,0,0);

	new FXHorizontalSeparator(frmJogo,SEPARATOR_RIDGE|LAYOUT_FILL_X);

	//FXMatrix *matrix;
	FXHorizontalFrame *frmH, *frmH2;
	FXVerticalFrame *frmV;
	FXComposite *frmRoot, *frmC;
	FXFont *fnt;

	fnt = new FXFont(a, "", 13);
	FXColor cor = FXRGB(0, 100, 0);
	
	//matrix = new FXMatrix(frmJogo,3,MATRIX_BY_COLUMNS|LAYOUT_SIDE_TOP|LAYOUT_FILL);
	//new FXFrame(matrix,LAYOUT_FILL_COLUMN|LAYOUT_FILL_ROW|LAYOUT_FILL); //frame vazio
	//lblJNome  = new FXLabel(matrix,"Jogador #",	NULL,LAYOUT_LEFT	|LAYOUT_CENTER_Y|LAYOUT_CENTER_X|LAYOUT_FILL_ROW|LAYOUT_FILL_COLUMN|LAYOUT_FILL);
	//---1� linha
	/*frmH = new FXHorizontalFrame(frmJogo,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);

		lblNome  = new FXLabel(frmH,"Jogador #",NULL,JUSTIFY_LEFT);
		lblNome->setFont(fnt);
		lblNome->setTextColor(cor);

		lblPoder = new FXLabel(frmH,"PM",NULL,JUSTIFY_LEFT);
		lblPoder->setFont(fnt);
		lblPoder->setTextColor(cor);*/

	//---2� linha
	frmRoot = new FXHorizontalFrame(frmJogo,LAYOUT_SIDE_TOP|LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);

	frmH = new FXHorizontalFrame(frmRoot,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0,0,0);
		frmV = new FXVerticalFrame(frmH,LAYOUT_SIDE_LEFT|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0,0,0);
			//lblNome  = new FXLabel(frmV,"Jogador #",NULL,JUSTIFY_LEFT);

	//frmH = new FXHorizontalFrame(frmJogo,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);
	//frmH = new FXHorizontalFrame(frmRoot,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0,0,0);
	frmH = new FXHorizontalFrame(frmRoot,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0,0,0);

		{frmV = new FXVerticalFrame(frmH,LAYOUT_SIDE_LEFT|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0,0,0);

			frmH2 = new FXHorizontalFrame(frmV,LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0,0,0);
				lblNome  = new FXLabel(frmH2,"Jogador #",NULL,JUSTIFY_LEFT);
				lblNome->setFont(fnt);
				lblNome->setTextColor(cor);

			frmH2 = new FXHorizontalFrame(frmV,LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0,0,0);
				new         FXLabel(frmH2,"Resist�ncia:",NULL,LAYOUT_FILL|JUSTIFY_LEFT);
				lblHT = new FXLabel(frmH2,"%.1f/%.1f",NULL,LAYOUT_FILL|JUSTIFY_RIGHT);
			
			frmH2 = new FXHorizontalFrame(frmV,LAYOUT_FILL|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0,0,0);
				new            FXLabel(frmH2,"Tempo:",NULL,LAYOUT_FILL|JUSTIFY_LEFT);
				lblTempo = new FXLabel(frmH2,"%d/%d",NULL,LAYOUT_FILL|JUSTIFY_RIGHT);
			
			new FXLabel(frmV,"Andar:",NULL,LAYOUT_FILL|JUSTIFY_LEFT);
		}
		
		{frmV = new FXVerticalFrame(frmH,PACK_UNIFORM_HEIGHT|LAYOUT_FIX_WIDTH,0,0,0,0,0,0,0,0,0,0);//|LAYOUT_FILL
			frmV->setWidth(300);

			lblPoder = new FXLabel(frmV,"PM",NULL,JUSTIFY_LEFT|LAYOUT_FILL);
			lblPoder->setFont(fnt);
			lblPoder->setTextColor(cor);

			frmC = new FXVerticalFrame(frmV,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
				new FXFrame(frmC,0); //frame vazio
				barHT = new FXProgressBar(frmC,0,0,FRAME_THICK|LAYOUT_FILL|LAYOUT_FIX_HEIGHT);
				barHT->setHeight(15);
				barHT->setProgress(75);
				barHT->setBarColor(FXRGB(200, 50, 50));
				new FXFrame(frmC,0); //frame vazio

			frmC = new FXVerticalFrame(frmV,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
				new FXFrame(frmC,0); //frame vazio
				barTempo = new FXProgressBar(frmC,0,0,FRAME_THICK|LAYOUT_FILL|LAYOUT_FIX_HEIGHT);
				barTempo->setHeight(15);
				barTempo->setProgress(45);
				barTempo->setBarColor(FXRGB(50, 200, 50));
				new FXFrame(frmC,0); //frame vazio

			lblAndar = new FXLabel(frmV,"1�",NULL,JUSTIFY_LEFT|LAYOUT_FILL);
		}

		//{frmC    = new FXVerticalFrame(frmRoot,PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0,0,0);//|LAYOUT_FILL
		{frmC    = new FXVerticalFrame(frmH,PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH|LAYOUT_FIX_WIDTH,0,0,0,0,0,0,0,0,0,0);//|LAYOUT_FILL
		frmC->setWidth(800);
		
			lblArma  = new FXLabel(frmC,"Rev�lver",0,LAYOUT_FILL);//,NULL,JUSTIFY_LEFT);
			lblArma->setFont(fnt);
			lblArma->setTextColor(cor);
			//bot�es de tiro
			butMirar	 = new FXButton(frmC,"� - Mirar\tMirar",NULL,this,			ID_MIRAR,		FRAME_THICK|FRAME_RAISED|LAYOUT_FILL);
			butMirarMais = new FXButton(frmC,"� - Mirar Mais\tMirar Mais",NULL,this,ID_MIRARMAIS,	FRAME_THICK|FRAME_RAISED|LAYOUT_FILL);
			butAtirar	 = new FXButton(frmC,"� - Atirar\tAtirar",NULL,this,		ID_ATIRAR,		FRAME_THICK|FRAME_RAISED|LAYOUT_FILL);
			//butRecarregar= new FXButton(frmH2,"�\tRecarregar",NULL,this,	ID_RECARREGAR,	FRAME_THICK|FRAME_RAISED);
			//butTrocar	 = new FXButton(frmH2,"�\tTrocar de Arma",NULL,this,ID_TROCAR,		FRAME_THICK|FRAME_RAISED);
		}
		
		//new FXFrame(frmH,LAYOUT_FILL_COLUMN|LAYOUT_FILL_ROW|LAYOUT_FILL); //frame vazio
		//new FXFrame(frmH,LAYOUT_FILL_COLUMN|LAYOUT_FILL_ROW|LAYOUT_FILL); //frame vazio
		//new FXFrame(frmH,LAYOUT_FILL_COLUMN|LAYOUT_FILL_ROW|LAYOUT_FILL); //frame vazio
		//new FXFrame(frmH,LAYOUT_FILL_COLUMN|LAYOUT_FILL_ROW|LAYOUT_FILL); //frame vazio

	//---3� linha
	//frmH = new FXHorizontalFrame(frmJogo,LAYOUT_SIDE_TOP|LAYOUT_FILL|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0);

	//frmV = new FXVerticalFrame(frmRoot,LAYOUT_LEFT,0,0,0,0,32,32,32,32,0,0);
	frmV = new FXVerticalFrame(frmRoot,LAYOUT_LEFT|LAYOUT_FILL_Y,0,0,0,0,0,0,0,0,0,0);

	//new FXFrame(frmV);//,LAYOUT_FILL_COLUMN|LAYOUT_FILL_ROW|LAYOUT_FILL); //frame vazio
	
		{frmC = new FXHorizontalFrame(frmV,LAYOUT_CENTER_Y|PACK_UNIFORM_HEIGHT|PACK_UNIFORM_WIDTH,0,0,0,0,0,0,0,0,0,0);

			butMaoDir = new FXButton(frmC, "M�o Dir.",0,this,ID_MAODIR,LAYOUT_FILL_Y|TEXT_ABOVE_ICON|FRAME_THICK|FRAME_SUNKEN);
			
			FXIcon *ico = Icone(getApp(), "imagens\\equipamento.gif");
			ChromaKey(ico, FXRGB(252, 2, 252));
			butEquip	 = new FXButton(frmC,"",ico,this,ID_EQUIP,LAYOUT_FILL_Y|FRAME_THICK|FRAME_RAISED);//|ICON_ABOVE_TEXT
			butEquip->setFont(fnt);

			butMaoEsq = new FXButton(frmC, "M�o Esq.",0,this,ID_MAOESQ,LAYOUT_FILL_Y|TEXT_ABOVE_ICON|FRAME_THICK|FRAME_SUNKEN);
			
			butPassarVez = new FXButton(frmC,"Passar Vez",NULL,this,ID_PASSARVEZ,FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_Y);
			butPassarVez->setFont(fnt);
		}

	//new FXFrame(frmV);//,LAYOUT_FILL_COLUMN|LAYOUT_FILL_ROW|LAYOUT_FILL); //frame vazio

	new FXHorizontalSeparator(frmJogo,SEPARATOR_RIDGE|LAYOUT_FILL_X);
	
	//frmJogo->hide();

	//######################### Frame com os bot�es do jogo
	//FXVerticalFrame *frmBobo;

	// RIGHT pane for the buttons
	//frmBobo=new FXVerticalFrame(frame,LAYOUT_FILL_Y|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,10,10,10,10);
	frmTemp=new FXVerticalFrame(splitter,LAYOUT_FILL_Y|LAYOUT_FILL_X|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,2,2,0,2);

	// Label above the buttons
	//new FXLabel(buttonFrame,"Button Frame",NULL,JUSTIFY_CENTER_X|LAYOUT_FILL_X);

	// Horizontal divider line
	new FXHorizontalSeparator(frmTemp,SEPARATOR_RIDGE|LAYOUT_FILL_X);
	lblFPS = new FXLabel(frmTemp,"OpenGL Canvas Frame",NULL,JUSTIFY_CENTER_X|LAYOUT_FILL_X);
	
	new FXButton(frmTemp,"&OpenGL Info\tDisplay OpenGL Capabilities",NULL,this,ID_OPENGL,FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,10,10,5,5);
	//new FXButton(frmTemp,"Spin &Timer\tSpin using interval timers\nNote the app blocks until the interal has elapsed...",NULL,this,ID_SPIN,FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,10,10,5,5);
	//new FXButton(frmTemp,"Spin &Chore\tSpin as fast as possible using chores\nNote even though the app is very responsive, it never blocks;\nthere is always something to do...",NULL,this,ID_SPINFAST,FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,10,10,5,5);
	//new FXButton(frmTemp,"&Stop Spin\tStop this mad spinning, I'm getting dizzy",NULL,this,ID_STOP,FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,10,10,5,5);

	// Exit button
	new FXButton(frmTemp,"Exit\tExit the application",NULL,getApp(),FXApp::ID_QUIT,FRAME_THICK|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_TOP|LAYOUT_LEFT,0,0,0,0,10,10,5,5);

	// Make a tooltip
	new FXToolTip(getApp());
}

winMain::~winMain()
{
	if (winNJ) delete winNJ;

	delete icoMain;

	delete filemenu;
	delete editmenu;
	delete subeditmenu;

	delete lblFPS;
	
	getApp()->removeTimeout(this,ID_TIME_FPS);
	getApp()->removeTimeout(this,ID_TIME_INPUT);
	getApp()->removeChore(this,ID_CHORE);
}

void winMain::create()
{
	FXMainWindow::create();

	glcanvas->makeCurrent();
	InitGL();
	
	CarregaModelos();
	glcanvas->makeNonCurrent();
	
	//janelas extras
	winNJ = NULL;
	winEq = NULL;
	
	//Jogo
	Jogo.pwinMain = this;
	Jogo.pIc = new cIconeBut(getApp(), &Jogo);
	
	//teclado e mouse
	for (int i=0; i<cConfig::TEC_COUNT; i++) teclas[i] = SEL_KEYRELEASE;
	mouse_incanvas = false;
	mouse_centrado = false;
	moveumouse = false;
	mouse_wheel = 0;
	Jogo.Zoom = 1;
	
	//FPS
	for (int i=0; i<FPS_AMOSTRAS; i++) cont_frames[i] = 0;
	//for (int i=0; i<FPS_AMOSTRAS; i++) {cont_frames[i] = 0; tempo_amostra[i] = 0;}
	fps = 0;
	fps_amostra = 0;
	getApp()->addChore(this,ID_CHORE);
	getApp()->addTimeout(this,ID_TIME_FPS,TIMER_FPS);
	getApp()->addTimeout(this,ID_TIME_INPUT,TIMER_INPUT);

	show(PLACEMENT_SCREEN);
#ifndef MODO_TOSCO
	maximize();
#endif
}

void winMain::MostraIcones()
{
	FXMainWindow::create();
}

long winMain::onCmdOpenGLPressed(FXObject*,FXSelector,void*)
{
	glcanvas->makeCurrent();
	SettingsDialog sd((FXWindow*)this,glvisual);
	glcanvas->makeNonCurrent();
	sd.execute();
	return 1;
}

long winMain::onChore(FXObject*,FXSelector,void*)
{
	if (!isMinimized() && this == getApp()->getFocusWindow())
		drawScene();
	
	getApp()->addChore(this,ID_CHORE);
	return 1;
}

long winMain::onExpose(FXObject*,FXSelector,void*)
{
	drawScene();
	return 1;
}

long winMain::onResize(FXObject*,FXSelector,void*)
{
	glcanvas->makeCurrent();

	GLdouble width = glcanvas->getWidth();
	GLdouble height = glcanvas->getHeight();
	GLdouble aspect = height>0 ? width/height : 1.0;
	glViewport(0,0,glcanvas->getWidth(),glcanvas->getHeight());

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(75.,aspect,0.1,50.);
	GLdouble fovy = 75 / Jogo.Zoom;
	GLdouble zNear = 0.1 * pow(Jogo.Zoom, 0.35);
	GLdouble zFar = 500 * zNear;
	gluPerspective(fovy,aspect,zNear,zFar);
		
	glcanvas->makeNonCurrent();
	
	return 1;
}

long winMain::onFocusIn(FXObject *, FXSelector, void*)
{
	getApp()->addChore(this,ID_CHORE);
	getApp()->addTimeout(this,ID_TIME_FPS,TIMER_FPS);
	return 1;
}

long winMain::onFocusOut(FXObject *obj, FXSelector sel, void* ptr)
{
	getApp()->removeChore(this,ID_CHORE);
	getApp()->removeTimeout(this,ID_TIME_FPS);
	//return onMouseOutCanvas(obj, sel, ptr);
	return 1;
}

long winMain::onTimeFPS(FXObject *, FXSelector, void*)
{
	static double INV_TEMPO_AMOSTRA = 1000. / (double)(TIMER_FPS * FPS_AMOSTRAS);
	static int REFRESH_AMOSTRA = (int)ceil((double)FPS_AMOSTRAS * INV_TEMPO_AMOSTRA);

	//agenda o pr�ximo clock
	getApp()->addTimeout(this,ID_TIME_FPS,TIMER_FPS);
	
	//avan�a posi��o
	fps_amostra = (fps_amostra + 1) % FPS_AMOSTRAS;
	
	//gradar o tempo do in�cio da amostra
	//tempo_amostra[fps_amostra] = getticktime();
	
	if (fps_amostra % REFRESH_AMOSTRA == 0) { //faz um refresh no label uma vez por segundo
		fps = 0;
		for (int i=0; i<FPS_AMOSTRAS; i++) fps += cont_frames[i];
		fps *= INV_TEMPO_AMOSTRA;
		//FXlong dif_tempo = tempo_amostra[fps_amostra]
		//				 - tempo_amostra[(fps_amostra + FPS_AMOSTRAS - 1) % FPS_AMOSTRAS];
		//fps *= 1000. / (double) dif_tempo;

		FXString strfps;
		lblFPS->setText(strfps.format("%g", fps));
	}

	cont_frames[fps_amostra] = 0;

	return 1;
}

long winMain::onTimeInput(FXObject*,FXSelector,void*)
{
	static int CLOCKS_MOUSE_PARADO = TEMPO_MOUSE_PARADO / TIMER_INPUT;

	//Navega��o pelo mapa s� deve funcionar quando "????"
	if (true) {
		//agenda o pr�ximo clock
		getApp()->addTimeout(this,ID_TIME_INPUT,TIMER_INPUT);

		//mouse: se o cara n�o mover o mouse por TEMPO_MOUSE_PARADO ent�o atualiza a marca (economiza cpu)
		static int mouseparado = 0;
		if (moveumouse) {
			mouseparado = 0;
			moveumouse = false;
		} else {
			mouseparado++;
			if (mouseparado == CLOCKS_MOUSE_PARADO) {
				//DONE: descobrir um jeito gen�rico de ajustar a posi��o do mouse
				//mx -= 2;	//glcanvasFrame->getPadLeft();
				//my -= 21;	//glcanvasFrame->getPadTop() + this->getY(); //getY() executado com a janela maximizada
				FXint mx, my;
				FXuint but;
				glcanvas->getCursorPosition(mx, my, but);
				AtualizaMarca(mx, my);
			}
		}

		//vari�veis que guardam velocidade e acelera��o da c�mera
		const float ac = 0.025f;
		const float max = 1.10f;
		static float vf = 0;
		static float v3d = 0;
		static float vs = 0;
		static float vz = 0;

		//mouse nas bordas => strafe
		bool MouseMoveHor = false;
		bool MouseMoveVer = false;
		if (this->isMaximized() && this == getApp()->getFocusWindow()) {
		
			int width = GetSystemMetrics(SM_CXSCREEN);
			int height = GetSystemMetrics(SM_CYSCREEN);
			
			//#include <gl\glx.h>
			//Display *disp = glXGetCurrentDisplay();
		
			FXint mx, my;
			FXuint but;
			this->getCursorPosition(mx, my, but);
			mx += this->getX();
			my += this->getY();
			if (mx == 0) {
				vs -= ac;
				MouseMoveVer = true;
			} else if (mx == width - 1) {
				vs += ac;
				MouseMoveVer = true;
			}
			if (my == 0) {
				vf += ac;
				MouseMoveHor = true;
			} else if (my == height - 1) {
				vf -= ac;
				MouseMoveHor = true;
			}
		}

		//teclado
		if ((Jogo.enuComando != CmdMirar)
		||  (!mouse_centrado)) {
			if (Jogo.Zoom != 1) {
				Jogo.Zoom = 1; //s� faz zoom quando estiver mirando e com o mouse centrado
				onResize(NULL,0,NULL);
			}
		
			short keyForward	= (teclas[cConfig::TEC_CAM_FRENTE] == SEL_KEYPRESS);
			short keyBack		= (teclas[cConfig::TEC_CAM_TRAS] == SEL_KEYPRESS);
			short keyZoomIn		= (teclas[cConfig::TEC_CAM_APROX] == SEL_KEYPRESS);
			short keyZoomOut	= (teclas[cConfig::TEC_CAM_AFASTA] == SEL_KEYPRESS);
			short keyMoveLeft	= (teclas[cConfig::TEC_CAM_ESQ] == SEL_KEYPRESS);
			short keyMoveRight	= (teclas[cConfig::TEC_CAM_DIR] == SEL_KEYPRESS);
			short keyMoveUp		= (teclas[cConfig::TEC_CAM_CIMA] == SEL_KEYPRESS);
			short keyMoveDown	= (teclas[cConfig::TEC_CAM_BAIXO] == SEL_KEYPRESS);

			if (!keyForward && !keyBack && !MouseMoveHor){vf = 0;}
			else if (keyForward)		{vf += ac;}
			else if (keyBack)			{vf -= ac;}
			vf = Max(Min(vf, max), -max);

			if (!keyZoomIn && !keyZoomOut)	{v3d = 0;}
			else if (keyZoomIn)				{v3d += ac;}
			else if (keyZoomOut)			{v3d -= ac;}
			v3d = Max(Min(v3d, max), -max);

			if (!keyMoveLeft && !keyMoveRight && !MouseMoveVer)	{vs = 0;}
			else if (keyMoveRight)				{vs += ac;}
			else if (keyMoveLeft)				{vs -= ac;}
			vs = Max(Min(vs, max), -max);

			if (!keyMoveUp && !keyMoveDown)	{vz = 0;}
			else if (keyMoveUp)				{vz += ac;}
			else if (keyMoveDown)			{vz -= ac;}
			vz = Max(Min(vz, max), -max);

			if (keyForward	|| keyBack ||
				keyMoveLeft	|| keyMoveRight ||
				keyZoomIn	|| keyZoomOut ||
				keyMoveUp	|| keyMoveDown //|| keyLevelUp || keyLevelDown
				) {
				//Mover o mapa equivale a mover o mouse, logo a marca deve ser atualizada
				mouseparado = 0;
			}

			glcanvas->makeCurrent();
			/*
			if (keyForward || keyBack)		Jogo.pMapa->MoveXYCamera(vf);
			if (keyZoomIn || keyZoomOut)	Jogo.pMapa->MoveXYZCamera(v3d);
			if (keyMoveLeft || keyMoveRight)Jogo.pMapa->StrafeHorCamera(vs);
			if (keyMoveUp || keyMoveDown)	Jogo.pMapa->StrafeVertCamera(vz);
			/*/
			if (vf != 0)Jogo.pMapa->MoveXYCamera(vf);
			if (v3d!= 0)Jogo.pMapa->MoveXYZCamera(v3d);
			if (vs != 0)Jogo.pMapa->StrafeHorCamera(vs);
			if (vz != 0)Jogo.pMapa->StrafeVertCamera(vz);
			//*/
			glcanvas->makeNonCurrent();
		}

		//Teclas sem repeti��o (sens�vel a borda de subida
		if (teclas[cConfig::TEC_CAM_ANDAR_SOBE] == SEL_KEYPRESS) {
			teclas[cConfig::TEC_CAM_ANDAR_SOBE] = SEL_NONE;
			Jogo.pMapa->MudaAndar(1);
		}
		if (teclas[cConfig::TEC_CAM_ANDAR_DESCE] == SEL_KEYPRESS) {
			teclas[cConfig::TEC_CAM_ANDAR_DESCE] = SEL_NONE;
			Jogo.pMapa->MudaAndar(-1);
		}
		if (teclas[cConfig::TEC_CAM_FILTRO_DE_ANDARES] == SEL_KEYPRESS) {
			teclas[cConfig::TEC_CAM_FILTRO_DE_ANDARES] = SEL_NONE;
			Jogo.pMapa->FiltroDeAndares = (Jogo.pMapa->FiltroDeAndares + 1) % 3;
		}
		
		//Roda do mouse: utilizar o valor de mouse_wheel e depois zer�-lo
		//a id�ia � usar para zoom real, e n�o o fajuto
		if (mouse_wheel != 0) {
			if ((Jogo.enuComando == CmdMirar) && (mouse_centrado)) {
				Jogo.Zoom *= pow(1.2, (double)mouse_wheel);
				Jogo.Zoom = Max(1, Min(40, Jogo.Zoom));
				onResize(NULL,0,NULL);
			} else if (tecl_state & SHIFTMASK) {
				//TO DO: calcular este n�mero de acordo com a posi��o da c�mera no mapa
				double d = Jogo.pMapa->Cam.c[2];
				Jogo.pMapa->MoveXYZCamera(d);
				Jogo.pMapa->RotCameraHorizontal(mouse_wheel * 16 * Config.mouse_sens);
				Jogo.pMapa->MoveXYZCamera(-d);
			} else {
				Jogo.pMapa->MoveXYZCamera(mouse_wheel * 0.5);
			}
			mouse_wheel = 0;
		}

		//Teclas ON/OFF
		//esconde/mostra paredes
		if (teclas[cConfig::TEC_CAM_ESCONDE_PAREDES] == SEL_KEYPRESS) {
			teclas[cConfig::TEC_CAM_ESCONDE_PAREDES] = SEL_NONE;
			Jogo.pMapa->MostraParedes = false;
		} else if (teclas[cConfig::TEC_CAM_ESCONDE_PAREDES] == SEL_KEYRELEASE) {
			teclas[cConfig::TEC_CAM_ESCONDE_PAREDES] = SEL_NONE;
			Jogo.pMapa->MostraParedes = true;
		}
		//centra mouse e rotaciona camera
		if (teclas[cConfig::TEC_CENTRA_MOUSE] == SEL_KEYPRESS) {
			teclas[cConfig::TEC_CENTRA_MOUSE] = SEL_NONE;
			if (Jogo.enuComando == CmdMirar) Jogo.CameraMira(cJogo::MODOCAM_PRI_PES);
			CentraMouse();
			mouse_centrado = true;
		} else if (teclas[cConfig::TEC_CENTRA_MOUSE] == SEL_KEYRELEASE) {
			teclas[cConfig::TEC_CENTRA_MOUSE] = SEL_NONE;
			if (Jogo.enuComando == CmdMirar) Jogo.CameraMira(cJogo::MODOCAM_NORMAL);
			mouse_centrado = false;
		}
	}
	
	return 1;
}

void winMain::LiberaTeclas()
{
	//Marca todas as teclas como tratadas
	for (int i=0; i<cConfig::TEC_COUNT; i++) {
		teclas[i] = SEL_NONE;
	}
}

long winMain::onMouseInCanvas(FXObject*,FXSelector,void*)
{
	mouse_incanvas = true;
	return 1;
}

long winMain::onMouseOutCanvas(FXObject*,FXSelector,void*)
{
	mouse_centrado = false;
	mouse_incanvas = false;
	return 1;
}

long winMain::onMouseMove(FXObject*,FXSelector,void*)
{
	mouse_incanvas = true;
	moveumouse = true;
	if (!(tecl_state & CONTROLMASK)) {
		mouse_centrado = false;
	} else if (mouse_centrado) {
		//Calcula a varia��o na posi��o do mouse e p�e ele de volta no centro
		FXint dx, dy;
		CentraMouse(&dx, &dy);
		
		//Rotaciona a c�mera
		glcanvas->makeCurrent();
		double sens = Config.mouse_sens / Jogo.Zoom;
		Jogo.pMapa->RotCameraHorizontal(sens * (float)dx);
		Jogo.pMapa->RotCameraVertical(-sens * (float)dy);
		glcanvas->makeNonCurrent();
	}
	
	return 1;
}

long winMain::onMouseDown(FXObject *obj, FXSelector sel, void *ptr)
{
	//Esta fun��o � necess�ria para que
	//possamos saber qual bot�o do mouse foi apertado
	//quando o evento onMouseUp aconte�a
	
	FXEvent* event=(FXEvent*)ptr;
	mouse_state = event->state;
	
	return 1;
}

long winMain::onMouseUp(FXObject *obj, FXSelector sel, void *ptr)
{
	/*
	Value	Description
	MK_CONTROL	Set if the CTRL key is down.
	MK_MBUTTON	Set if the middle mouse button is down.
	MK_RBUTTON	Set if the right mouse button is down.
	MK_SHIFT	Set if the SHIFT key is down.
	*/

	//Atualiza o estado das teclas especiais E Le a posi��o do mouse
	FXint mx, my;
	glcanvas->getCursorPosition(mx, my, tecl_state);
	
	//Junta as teclas (e bot�es do mouse) dos eventos onMouseDown e onMouseUp
	tecl_state |= mouse_state;

	glcanvas->makeCurrent();
	ushort x, y, z;
	double d;
	switch(Jogo.Estado)	{
		case EST_JOGANDO:
			Selection(mx, my, x, y, z, d, ((Jogo.enuComando != CmdMirar) || !mouse_centrado));
			Jogo.Click(x, y, z, d, tecl_state);
			//if (Selection(mx, my, x, y, z, d, ((Jogo.enuComando != CmdMirar) || !mouse_centrado))) {
				/* n�o quero mais usar diretamente APIs do windows
				//GetAsyncKeyState retorna 1 caso a tecla tenha sido apertada e ainda n�o tratada
				//mas eu s� quero ignorar se ela foi tratada ou n�o
				//s� me interessa se ela est� pressionada agora
				if (GetAsyncKeyState(VK_MENU) & ~1) button |= MK_ALT;
				*/
			//	Jogo.Click(x, y, z, d, tecl_state);
			//}			
			break;
		case EST_EDITANDO:
			if (Selection(mx, my, x, y, z, d)) {
				if (tecl_state & LEFTBUTTONMASK) {
					//Jogo.pMapa->SetCont(x, y, z, (Jogo.pMapa->Cont(x, y, z) + 1) % 60);
				} else if (tecl_state & RIGHTBUTTONMASK) {
					//Jogo.pMapa->SetCont(x, y, z, (Jogo.pMapa->Cont(x, y, z) + 59) % 60);
					//Jogo.pMapa->SetCont(x, y, z, (Jogo.pMapa->Cont(x, y, z, cMapa::CHAO) + 1) % 60, cMapa::CHAO);
				}
			}
			break;
		//este estado n�o � mais necess�rio
		//case EST_MENSAGEM:
		//	//nas mensagens sem bot�es, qualquer clique fecha a mensagem
		//	if (returnmsg < 0) returnmsg++;
		//	break;
		default:
			break;
	}
	glcanvas->makeNonCurrent();

	return 1;
}

long winMain::onMouseWheel(FXObject *obj, FXSelector sel, void *ptr)
{
	FXEvent* event=(FXEvent*)ptr;

	//soma as rota��es da roda do mouse (sei l� pq o fox s� p�e m�ltiplos de 120)	
	mouse_wheel += event->code / 120;
	
	return 1;
}

long winMain::onCmdButtonPressed(FXObject *obj, FXSelector sel, void *ptr)
{
	//FXMessageBox::information(this,MBOX_OK,"A Message from the Baron","You just pressed the button");
	//FXuint resp = FXMessageBox::information(this,MBOX_YES_NO_CANCEL,"A Message from the Baron","Pi = %g ?", 3.141592);
	
	FXEvent* event=(FXEvent*)ptr;

	FXString strfps;
	strfps.format("%d | %c | %x | %x | %d", event->type, event->code, event->code, event->state, event->time);
	//lblFPS->setText(strfps);
	
	/*if (event->code == Config.teclas[cConfig::TEC_CENTRA_MOUSE]) {
		if (mouse_incanvas) {
			if (event->type == SEL_KEYPRESS) {
				//Apertou a tecla (default: Left Ctrl) que centra o mouse e serve para rotacionar a c�mera
				CentraMouse();
				mouse_centrado = true;
			} else {
				//Soltou a tecla
				mouse_centrado = false;
			}
		}
	}*/
	for (int i=0; i<cConfig::TEC_COUNT; i++) {
		if ((event->code == Config.teclas[i])
		||  (
			(event->code == (Config.teclas[i] & (~0x20))) &&
			(event->state & SHIFTMASK)
			)) {
			teclas[i] = event->type;
			break;
		}
	}
	
	//Atualiza o estado das teclas especiais
	tecl_state = event->state;
	
	//FXint event.state=fxmodifierkeys(); //flags:
	// SHIFTMASK, CONTROLMASK, ALTMASK,
	// CAPSLOCKMASK, NUMLOCKMASK, SCROLLLOCKMASK,
	// LEFTBUTTONMASK, MIDDLEBUTTONMASK, RIGHTBUTTONMASK
	
	FXMainWindow::handle(obj, sel, ptr);

	return 0;
}

long winMain::onMenuAbrirMapa(FXObject *,FXSelector,void*)
{
	if (Jogo.JogoComecou) {
		if (MBOX_CLICKED_NO == Jogo.Msg(MBOX_YES_NO, "Um jogo est� em andamento!\nDeseja encerr�-lo?")) {
			return 1;
		} else {
			//TO DO: encerrar jogo
		}
	}
	
	FXFileDialog opendialog(this,"Abrir Mapa");
	opendialog.setSelectMode(SELECTFILE_EXISTING);
	opendialog.setPatternList("Todos os arquivos (*)\nMapas Duelo2D (*.map)");
	opendialog.setCurrentPattern(1);//getCurrentPattern());
	opendialog.setDirectory("mapas");//FXFile::directory(filename));
	if(opendialog.execute()){
		//setCurrentPattern(opendialog.getCurrentPattern());
		FXString file=opendialog.getFilename();
		
		Jogo.pMapa->CarregaMapa(file.text());
		Jogo.MudaEstado(EST_EDITANDO);
	}
	
	return 1;
}

long winMain::onMenuNovoJogo(FXObject *obj, FXSelector sel, void* ptr)
{
	//solta o ctrl (quando o cara aperta Ctr-N o evento de keyup n�o acontece)
	mouse_centrado = false;

	if (Jogo.JogoComecou) {
		if (MBOX_CLICKED_NO == Jogo.Msg(MBOX_YES_NO, "Um jogo est� em andamento!\nDeseja encerr�-lo?")) {
			return 1;
		}
	}

	//Mostra NovoJogo
	if (!winNJ) winNJ = new winNovoJogo((FXWindow*)this, icoMain, &Jogo);
	FXuint ret = winNJ->execute(PLACEMENT_SCREEN);
	
	if (ret) {
		frmTemp->hide();

		Jogo.RecebeVez();

		frmJogo->show();
	}

	return 1;
}

void winMain::IniciaTimerEspera(int milisec, void *ptr)
{
	getApp()->addTimeout(this,ID_TIME_ESPERA,milisec, ptr);
}

long winMain::onTimerEspera(FXObject *,FXSelector,void *ptr)
{
	*(int*)ptr = ~(*(int*)ptr);
	return 1;
}

void winMain::CentraMouse(FXint *dx, FXint *dy)
{
	FXint cx = glcanvas->getX() + (glcanvas->getWidth() / 2);
	FXint cy = glcanvas->getY() + (glcanvas->getHeight() / 2);
	if (dx != 0 && dy != 0) {
		FXuint but;
		glcanvas->getCursorPosition(*dx, *dy, but);
		*dx -= cx;
		*dy -= cy;
	}
	glcanvas->setCursorPosition(cx, cy);
}

void winMain::AtualizaMarca(int mousex, int mousey)
{
	GLContexto.Empilha();
	
	ushort x, y, z;
	double d;
	if (Selection(mousex, mousey, x, y, z, d, ((Jogo.enuComando != CmdMirar) || !mouse_centrado))) {
		Jogo.pMapa->marca = true;
		Jogo.pMapa->marcax = x;
		Jogo.pMapa->marcay = y;
		Jogo.pMapa->marcaz = z;
	} else {
		Jogo.pMapa->marca = false;
	}

	//FXString str;
	//lblFPS->setText(str.format("%g", d));//"%u", d));

	GLContexto.DesEmpilha();
}

//######################### HUD
long winMain::onPassarVez(FXObject *,FXSelector,void*)
{
	Jogo.PassaVez();
	return 1;
}

long winMain::onEquip(FXObject *,FXSelector,void*)
{
	if (!winEq) winEq = new winEquip((FXWindow*)this, icoMain, &Jogo);
	FXuint ret = winEq->execute(PLACEMENT_SCREEN);
	
	if (ret) {
		//
	}

	//Jogo.Pega(-1, -1, -1);
	Jogo.Reescreve();
	return 1;
}

long winMain::onMirar(FXObject *,FXSelector,void*)
{
	//TO DO: trocar o cursor do mouse para uma mira
	Jogo.enuComando = CmdMirar;

	Jogo.IniciaMira();
		
	return 1;
}

long winMain::onMirarMais(FXObject *,FXSelector,void*)
{
	Jogo.MiraMais();
	return 1;
}

long winMain::onAtirar(FXObject *,FXSelector,void*)
{
	Jogo.Atira();
	return 1;
}

long winMain::onMaoDir(FXObject *,FXSelector,void*)
{
	Jogo.UsaMao(true);
	return 1;
}

long winMain::onMaoEsq(FXObject *,FXSelector,void*)
{
	Jogo.UsaMao(false);
	return 1;
}

/*long winMain::onRecarregar(FXObject *,FXSelector,void*)
{
	Jogo.Recarrega();
	return 1;
}

long winMain::onTrocar(FXObject *,FXSelector,void*)
{
	//TO DO: possibilitar de alguma maneira que o cara escolha a arma que ele vai pegar
	Jogo.TrocaArma(-1);
	return 1;
}*/