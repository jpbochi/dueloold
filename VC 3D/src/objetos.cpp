
#include <stdio.h>
#include "objetos.h"
#include "arma.h"
#include "iconebut.h"
#include "geral.h"

//------------------------------------------------------------------------
//cObjetoSimples ---------------------------------------------------------
cObjetoSimples::cObjetoSimples()
{
	cod = 0; //nulo
}
cObjetoSimples::cObjetoSimples(unsigned char pcod)
{
	cod = pcod;
}

bool cObjetoSimples::operator == (cObjetoSimples op)
{
	return (cod == op.cod);
}
void cObjetoSimples::operator = (cObjetoSimples op)
{
	cod = op.cod;
}

char cObjetoSimples::GetIco()
{
	return cod;
}

//------------------------------------------------------------------------
//cObjeto ----------------------------------------------------------------
cObjeto::cObjeto(cObjeto &objref)
{
	cod = objref.cod;
	dono = objref.dono;
	info1 = objref.info1;
	codmunicao = objref.codmunicao;
	prox = NULL;
}

cObjeto::cObjeto(const unsigned short _cod, short _dono, ushort _info1)
{
	cod = _cod;
	dono = _dono;
	info1 = _info1;
	codmunicao = mSemMunicao;
	prox = NULL;
}

cObjeto::cObjeto(const unsigned short codpuro, const enmTipo tipo, short _dono)
{
	dono = _dono;
	info1 = 0;
	codmunicao = mSemMunicao;
	prox = NULL;
	switch (tipo) {
		case tipArma: {
			//newbice! coisa feia!
			if ((codpuro == aBolaDeFogo)
			||  (codpuro == aCanhao)
			||  (codpuro == aBaforada)
			||  (codpuro == aTriLaser)
			||  (codpuro == aRoboArma)
			||  (codpuro == aEspinhos)
			||  (codpuro == aSnikt)) {
				cod = codpuro | flgArmaSkill;
			} else {
				cod = codpuro | tipo;
			}
		} break;
		case tipSkill:
		case tipMunicao:
		case tipEquip: {
			cod = codpuro | tipo;
			if ((codpuro == eCapaceteInv)
			||  (codpuro == eCapaceteIR)) {
				cod |= codCabeca;
			} else if (codpuro == eColete) {
				cod |= codCorpo;
			}
			if (codpuro == eKitMedico) cargas = 2;
		} break;
	}
}

cObjeto::~cObjeto()
{
	prox = NULL; //evita uma rea��o em cadeia apagando todos os pr�ximos objetos
}

void cObjeto::CarregaArma(cArma *Armas, cMunicao *Municoes)
{
	if (EhArma()) {
		ushort codarma = GetCod();
		codmunicao = Armas[codarma].MunPadrao;
		municao = Municoes[codmunicao].MunPac;
	}
}

void cObjeto::RecarregaArmaDeUmTiroPorRodada(cArma *Armas)
{
	if (this != 0)
	if (EhArma())
	if (Armas[GetCod()].SohUmTiroPorRodada()
	&&  municao < 0) {
		municao = -municao;
	}
}

ushort cObjeto::ConverteObj(char c)//Legacy
{
	//caixas-surpresa
	if (c == OBJ_Caixa) {
		int r = rndi(1, 54);
		switch (r) {
			case 1:case 2:case 3:	c = OBJ_Espingarda; break;
			case 4:case 5:case 6:	c = OBJ_Metralhadora; break;
			case 7:case 8:case 9:	c = OBJ_Minas; break;
			case 10:case 11:		c = OBJ_LancaFoguetes; break;
			case 12:case 13:case 14:c = OBJ_Rifle; break;
			case 15:case 16:		c = OBJ_Degeneradora; break;
			case 17:case 18:case 19:c = OBJ_Granada; break;
			case 20:case 21:case 22:c = OBJ_KitMedico; break;
			case 23:				c = OBJ_Esqueleto; break;
			case 24:case 25:		c = OBJ_Disco; break;
			case 26:case 27:		c = OBJ_MiniGun; break;
			case 28:case 29:case 30:c = OBJ_LancaChamas; break;
			case 31:case 32:case 33:c = OBJ_Revolver; break;
			case 34:case 35:		c = OBJ_Ricocheteadora; break;
			case 36:				c = OBJ_CapaceteInv; break;
			case 37:case 38:		c = OBJ_OculosIR; break;
			case 39:case 40:case 41:c = OBJ_Bomba; break;
			case 42:case 43:		c = OBJ_Paralisante; break;
			case 44:case 45:		c = OBJ_Teletransportadora; break;
			case 46:				c = OBJ_Colete; break;
			case 47:case 48:		c = OBJ_Holografia; break;
			case 49:case 50:		c = OBJ_Detector; break;
			case 51:case 52:		c = OBJ_Trabuco; break;
			case 53:				c = OBJ_Blaster; break;
			case 54:				c = OBJ_MG42; break;
		}
	} else if (c == OBJ_CaixaDeArmas) {
		int r = rndi(1, 35);
		switch (r) {
			case 1:case 2:case 3:	c = OBJ_Espingarda; break;
			case 4:case 5:case 6:	c = OBJ_Metralhadora; break;
			case 7:case 8:			c = OBJ_LancaFoguetes; break;
			case 9:case 10:case 11:	c = OBJ_Rifle; break;
			case 12:case 13:		c = OBJ_Degeneradora; break;
			case 14:case 15:case 16:c = OBJ_Granada; break;
			case 17:case 18:		c = OBJ_Disco; break;
			case 19:case 20:		c = OBJ_MiniGun; break;
			case 21:case 22:		c = OBJ_LancaChamas; break;
			case 23:case 24:case 25:c = OBJ_Revolver; break;
			case 26:case 27:		c = OBJ_Ricocheteadora; break;
			case 28:case 29:		c = OBJ_Paralisante; break;
			case 30:case 31:		c = OBJ_Teletransportadora; break;
			case 32:case 33:		c = OBJ_Trabuco; break;
			case 34:				c = OBJ_Blaster; break;
			case 35:				c = OBJ_MG42; break;
		}
	}

	const static ushort arma = flgArma;
	switch (c) {
		case OBJ_Espingarda:		return aEspingarda | arma;
		case OBJ_Metralhadora:		return aMetralhadora | arma;
		case OBJ_Minas:				return eMina;
		case OBJ_LancaFoguetes:		return aLancaFog | arma;
		case OBJ_Rifle:				return aRifle | arma;
		case OBJ_Degeneradora:		return aDegen | arma;
		case OBJ_Granada:			return eGranada;
		//case OBJ_GranadaA:			return eGranada; //?!
		case OBJ_KitMedico:			return eKitMedico;
		case OBJ_Esqueleto:			return eEsqueleto;
		case OBJ_Disco:				return aDisco | arma;
		case OBJ_MiniGun:			return aMiniGun | arma;
		case OBJ_LancaChamas:		return aLancaChamas | arma;
		case OBJ_Revolver:			return aPistola | arma;
		case OBJ_Ricocheteadora:	return aRicochet | arma;
		case OBJ_CapaceteInv:		return eCapaceteInv | codCabeca;
		case OBJ_OculosIR:			return eCapaceteIR | codCabeca;
		case OBJ_Bomba:				return eBomba;
		case OBJ_Paralisante:		return aParalisante | arma;
		case OBJ_Teletransportadora:return aTeletrans | arma;
		//case OBJ_Caixa:				//?!
		case OBJ_Blaster:			return aBlaster | arma;
		case OBJ_Colete:			return eColete | codCorpo;
		case OBJ_Chave:				return eChave;
		case OBJ_Holografia:		return eHolografia;
		case OBJ_MG42:				return aMG34 | arma;
		case OBJ_Detector:			return eDetector;
		case OBJ_Trabuco:			return aTrabuco | arma;
		//case OBJ_CaixaDeArmas:		//?!
		//case OBJ_Moto:				//?!
		default:					return aPistola | arma; //?!
	}
}

char cObjeto::GetIco()
{
	if (EhArma()) {
		switch (GetCod()) {
			case aPistola:		return icoPistola;
			case aEspingarda:	return icoEspingarda;
			case aMetralhadora:	return icoMetralhadora;
			case aLancaFog:		return icoLancaFog;
			case aRifle:		return icoRifle;
			case aDegen:		return icoDegen;
			case aDisco:		return icoDisco;
			case aTriLaser:		return icoTriLaser;
			case aMiniGun:		return icoMiniGun;
			case aLancaChamas:	return icoLancaChamas;
			case aRicochet:		return icoRicochet;
			case aRoboArma:		return icoRoboArma;
			case aParalisante:	return icoParalisante;
			case aBaforada:		return icoBaforada;
			case aTeletrans:	return icoTeletrans;
			case aBlaster:		return icoBlaster;
			case aMachado:		return icoMachado;
			case aEspinhos:		return icoEspinhos;
			case aMG34:			return icoMG34;
			case aCanhao:		return icoCanhaoMunHE;
			//case aCanhaoAP:	return icoCanhaoMunAP;
			case aBolaDeFogo:	return icoBolaDeFogo;
			case aSabreDeLuz:	return icoSabreDeLuz;
			case aShuriken:		return icoShuriken;
			case aKatana:		return icoKatana;
			//case aSoco:		return icoSoco; //n�o existe um item aSoco, nem icoSoco
			case aTrabuco:		return icoTrabuco;
			//case aDedoDuro:		return icoDedoDuro;
			case aSnikt:		return icoSnikt;
		}
	} else if (EhMunicao()) {
		switch (GetCod()) {
			case mPistola:		return icoPistolaMun;
			case mEspingarda:	return icoEspingardaMun;
			case mMetralhadora:	return icoMetralhadoraMun;
			case mLancaFog:		return icoLancaFogMun;
			case mRifle:		return icoRifleMun;
			case mDegen:		return icoDegenMun;
			case mDisco:		return icoDiscoMun;
			case mTriLaser:		return icoTriLaserMun;
			case mMiniGun:		return icoMiniGunMun;
			case mLancaChamas:	return icoLancaChamasMun;
			case mRicochet:		return icoRicochetMun;
			case mRoboArma:		return icoRoboArmaMun;
			case mParalisante:	return icoParalisanteMun;
			case mBaforada:		return icoBaforadaMun;
			case mTeletrans:	return icoTeletransMun;
			case mBlaster:		return icoBlasterMun;
			case mEspinhos:		return icoEspinhosMun;
			case mMG34:			return icoMG34Mun;
			case mCanhaoHE:		return icoCanhaoMunHE;
			case mCanhaoAP:		return icoCanhaoMunAP;
			case mBolaDeFogo:	return icoBolaDeFogoMun;
			case mShuriken:		return icoShurikenMun;
			case mTrabuco:		return icoTrabucoMun;
		}
	} else if (EhSkill()) {
		switch (GetCod()) {
			case sCamuflagem:		return icoCamuflagem;
			case sKitDeArrombamento:return icoKitDeArrombamento;
			case sEnterrarse:		return icoEnterrarse;
			case sDesarmarMinas:	return icoDesarmarMinas;
			case sControlePsionico:	return icoControlePsionico;
			case sRegT1000:			return icoRegT1000;
			case sAutoDestruicao:	return icoAutoDestruicao;
			case sIdentificacao:	return icoIdentificacao;
			case sPararTempo:		return icoPararTempo;
			case sInvisibilidade:	return icoInvisibilidade;
			case sTeletransporte:	return icoTeletransporte;
			case sImitar:			return icoImitar;
			case sHolografia:		return icoMagiaHolografia;
			case sDardosMisticos:	return icoDardosMisticos;
			case sRezar:			return icoRezar;
			case sCura:				return icoCura;
			case sAnimarMortos:		return icoAnimarMortos;
			case sRelampago:		return icoRelampago;
		}
	} else if (EhEquip()) {
		switch (GetCod()) {
			case eMina:			return icoMina;
			case eGranada:		return icoGranada;
			case eKitMedico:	return icoKitMedico;
			case eEsqueleto:	return icoEsqueleto;
			case eCapaceteInv:	return icoCapaceteInv;
			case eCapaceteIR:	return icoCapaceteIR;
			case eBomba:		return icoBomba;
			case eColete:		return icoColete;
			case eChave:		return icoChave;
			case eHolografia:	return icoHolografia;
			case eDetector:		return icoDetector;
		}
	}
	return icoDesconhecido;
}

ushort cObjeto::GetCod()	{return cod & maskCod;}
ushort cObjeto::GetLugarUso() {return cod & maskLugarUso;}

bool cObjeto::Eh(ushort c, enmTipo tipo)
{
	return ((cod & maskCod) == c)
		&& ((cod & maskTipo) == tipo);
}

bool cObjeto::EhArma()		{return	((cod & maskTipo) == flgArma) || EhArmaSkill();}
bool cObjeto::EhSkill()		{return	((cod & maskTipo) == flgSkill) || EhArmaSkill();}
bool cObjeto::EhArmaSkill()	{return	(cod & maskTipo) == flgArmaSkill;}
bool cObjeto::EhEquip()		{return	(cod & maskTipo) == flgEquip;}
bool cObjeto::EhMunicao()	{return	(cod & maskTipo) == flgMunicao;}

bool cObjeto::EhObjetoTrivial()
{
	ushort c = GetCod();
	if (EhArma()) {
		return c == aEspingarda
			|| c == aRifle
			|| c == aTrabuco
			|| c == aPistola;
	} else if (EhEquip()) {
		return c == eKitMedico
			|| c == eChave;
	} else {
		return false;
	}
}

bool cObjeto::UsaManaFe()
{
	ushort c = GetCod();
	return c ==	aBolaDeFogo;
}

bool cObjeto::AplicaEfeitoVeste(cJogador *j)
{
	//TO DO:???????????
	return true;
}

bool cObjeto::DesaplicaEfeitoVeste(cJogador *)
{
	//TO DO:???????????
	return true;
}

double cObjeto::PenalidadeDuasMaos()
{
	//TO DO:???????????
	return 0;
}

bool cObjeto::GastaCarga()
{
	cargas--;
	return (cargas <= 0);
}

/*cObjeto::enmLugarUso cObjeto::LugarUso()
{
	return codMao;
}*/

//------------------------------------------------------------------------
//cListaObjetos ----------------------------------------------------------
cListaObjetos::cListaObjetos()
{
	priobj = NULL;
	ultobj = NULL;
	numobj = 0;

	objsimp = NULL;
	lenobjsimp = 0;
	numobjsimp = 0;
}

void cListaObjetos::RecarregaArmasDeUmTiroPorRodada(cArma *Armas)
{
	cObjeto *obj = priobj;
	while (obj != 0) {
		obj->RecarregaArmaDeUmTiroPorRodada(Armas);		
		obj = obj->prox;
	}
}

void cListaObjetos::ConverteObjs(cArma *Armas, cMunicao *Municoes)
{
	cObjeto obj(0);
	//transforma todos os objetos simples em objetos normais
	for (int i=0; i<numobjsimp; i++) {
		obj = cObjeto(cObjeto::ConverteObj(objsimp[i].cod));
		obj.CarregaArma(Armas, Municoes);
		AdObj(obj);
	}
	
	numobjsimp = 0;
}

void cListaObjetos::AdObj(cObjeto &objad)
{
	numobj++;
	//se a lista est� vazia
	if (ultobj == NULL) {
		priobj = new cObjeto(objad);
		ultobj = priobj;
		//proxacesso = priobj;
	} else {
		ultobj->prox = new cObjeto(objad);
		ultobj = ultobj->prox;
	}
}
void cListaObjetos::AdObjSimp(cObjetoSimples objad)
{
	if (numobjsimp == lenobjsimp) {
		//aumenta vetor
		if (lenobjsimp == 0)lenobjsimp = 4;
		else				lenobjsimp *= 2;

		//cria um vetor novo
		cObjetoSimples* tmp = new cObjetoSimples[lenobjsimp];

		//copia o vetor velho para cima do novo
		for (int i=0; i<numobjsimp; i++) tmp[i] = objsimp[i];

		//apaga vetor velho
		delete [numobjsimp] objsimp;

		//muda os ponteiros
		objsimp = tmp;
		tmp = NULL;
	}
	//s� adicionar
	objsimp[numobjsimp] = objad;
	numobjsimp++;
}

//TO DO: testar as duas fun��es de exclus�o de objetos
bool cListaObjetos::ApagaObj(cObjeto *objapg)
{
	if (objapg == priobj) {
		//o objeto a apagar � o primeiro

		//se o objeto a apagar � o �ltimo tamb�m, ent�o o �ltimo mudou
		if (objapg == ultobj) ultobj = NULL;

		//avan�a uma posi��o
		priobj = priobj->prox;

		//apaga objeto
		delete objapg;
		numobj--;
		
		//esquece os ponteiros e sai fora
		objapg = NULL;
		return true;
	} else {
		cObjeto *aux = priobj;
		cObjeto *auxprox = aux->prox;
		while (auxprox != NULL) {
			if (objapg == auxprox) {
				//se o obj. apagado era o �ltimo, ent�o o �ltimo mudou
				if (objapg == ultobj) {
					ultobj = aux;
				}

				//pula um item na lista
				aux->prox = auxprox->prox;

				//apaga o objeto
				delete auxprox;
				numobj--;

				//esquece os ponteiros e sai fora
				aux = NULL;
				auxprox = NULL;
				return true;
			}
			aux = auxprox;
			auxprox = aux->prox;
		}
	}
	return false;
}
bool cListaObjetos::ApagaObjSimp(cObjetoSimples objapg)
{
	//procura o �ltimo objejosimples igual ao que vai ser apagado
	for (int i=numobjsimp-1; i>=0; i--) {
		if (objapg == objsimp[i]) {
			//achou
			//substitui o obj a ser apagado pelo �ltimo da lista
			objsimp[i] = objsimp[numobjsimp-1];
			numobjsimp--;
			return true;
		}
	}
	return false;
}

char cListaObjetos::GetResumo()
{
	//TO DO: melhorar esta fun��o. � pra escolher o maior obj, ou mostrar um pacote, n�o sei ainda
	if (numobjsimp > 0) {
		return objsimp[0].GetIco();
	} else if (numobj > 0) {
		return priobj->GetIco();
	} else {
		return 0;
	}
}

cObjeto *cListaObjetos::GetObj()
{
	return priobj;
}

cObjetoSimples cListaObjetos::GetObjSimp(int indice)
{
	if (indice < numobjsimp && indice >= 0) {
		return objsimp[indice];
	} else {
		return cObjetoSimples();
	}
}

//------------------------------------------------------------------------
//cListaPosObjetos -------------------------------------------------------
cListaPosObjetos::cListaPosObjetos(ushort px, ushort py, ushort pz):cListaObjetos()
{
	x = px;
	y = py;
	z = pz;
}

bool cListaPosObjetos::MatchPos(ushort px, ushort py, ushort pz)
{
	return (x == px) && (y == py) && (z == pz);
}

//------------------------------------------------------------------------
//cListaListas -----------------------------------------------------------
cListaListas::cListaListas()
{
	lista = NULL;
	numlistas = 0;
	lenlista = 0;
}

void cListaListas::AdObjPos(cObjeto &obj, ushort x, ushort y, ushort z)
{
	int num = AchaLista(x, y, z);
	if (num == -1) { //n�o achou
		//insere lista na lista de listas
		AdLista(x, y, z);
		//adicionar o objeto � �ltima lista
		num = numlistas - 1;
	}
	lista[num]->AdObj(obj);
}

void cListaListas::AdObjSimpPos(cObjetoSimples &objsimp, ushort x, ushort y, ushort z)
{
	int num = AchaLista(x, y, z);
	if (num == -1) { //n�o achou
		//insere lista na lista de listas
		AdLista(x, y, z);
		//adicionar o objeto � �ltima lista
		num = numlistas - 1;
	}
	lista[num]->AdObjSimp(objsimp);
}

cListaPosObjetos* cListaListas::GetListaPos(ushort x, ushort y, ushort z)
{
	int num = AchaLista(x, y, z);
	if (num == -1) { //n�o achou
		return NULL;
	} else {
		return lista[num];
	}	
}

int cListaListas::AchaLista(ushort x, ushort y, ushort z)
{
	for (int i=0; i<numlistas; i++) {
		if (lista[i]->MatchPos(x, y, z)) {
			return i;
		}
	}
	return -1;
}

void cListaListas::AdLista(ushort x, ushort y, ushort z)
{
	if (numlistas == lenlista) {
		//aumenta vetor
		if (lenlista == 0)	lenlista = 4;
		else				lenlista *= 2;

		//cria um vetor novo
		cListaPosObjetos* *tmp = new cListaPosObjetos*[lenlista];

		//copia o vetor velho para cima do novo
		for (int i=0; i<numlistas; i++) tmp[i] = lista[i];
		//preenche o resto com NULL
		for (; i<lenlista; i++) tmp[i] = NULL;

		//apaga os ponteiros do vetor velho	
		for (i=0; i<numlistas; i++) lista[i] = NULL;
		//apaga vetor velho
		delete [numlistas] lista;

		//muda os ponteiros
		lista = tmp;
		tmp = NULL;
	}
	//s� adicionar
	lista[numlistas] = new cListaPosObjetos(x, y, z);
	numlistas++;
}

//------------------------------------------------------------------------
//cObjetos ---------------------------------------------------------------
cObjetos::cObjetos(ushort numerodelistas)
{
	numlistas = numerodelistas;
	listas = new cListaListas[numlistas];
}

cObjetos::~cObjetos()
{
	if (listas != NULL) {
		delete [numlistas] listas;
		listas = NULL;
		numlistas = 0;
	}

}

int cObjetos::PosLista(uint vetpos)
{
	if (numlistas == 0)	return -1; // � necess�rio criar algumas listas antes
	else				return vetpos % numlistas; //TO DO: inventar uma fun��o de hash melhor
}

void cObjetos::AdObjPos(cObjeto &obj, uint vetpos, ushort x, ushort y, ushort z)
{
	int pos = PosLista(vetpos);
	if (pos != -1) { 
		listas[pos].AdObjPos(obj, x, y, z);
	}
}

void cObjetos::AdObjSimpPos(cObjetoSimples &objsimp, uint vetpos, ushort x, ushort y, ushort z)
{
	int pos = PosLista(vetpos);
	if (pos != -1) { 
		listas[pos].AdObjSimpPos(objsimp, x, y, z);
	}
}

cListaPosObjetos* cObjetos::GetListaPos(uint vetpos, ushort x, ushort y, ushort z)
{
	int pos = PosLista(vetpos);
	if (pos != -1) { 
		return listas[pos].GetListaPos(x, y, z);
	} else {
		return NULL;
	}
}
