
#ifndef MODELOSGL_H
#define MODELOSGL_H

class GLList
{
public:
	enum{
		HEXAGONO = 0,

		MARCA,
		PIRAMIDE,
		CILINDRO,

		PILAR,
		PAREDE_ATRAVES,
		PAREDE_METADE,
		
		HEX_ATRAVES,
		HEX_METADE,

		PILAR_CHAO,
		CHAO_ATRAVES,
		CHAO_METADE,

		NUM_LISTS
	};
};

void GeraGLListas();

#endif