
#ifndef APPDUELO_H
#define APPDUELO_H

//#include "declarac.h"

// Version
#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_PATCH 0

//declarar classes existentes
//class winMain;


// Main Application class
class appDuelo : public FXApp {
	FXDECLARE(appDuelo)
	//friend class JanelaQuePodeVerVariaveisGlobais;
	//friend class winMain
protected:
	//Variaveis Globais

private:
	appDuelo(){}
public:
	enum{
		ID_CLOSEALL=FXApp::ID_LAST,
		ID_LAST
	};

	long onCmdCloseAll(FXObject*,FXSelector,void*);

	// Construct application object
	appDuelo(const FXString& name);

	// Initialize application
	virtual void init(int& argc,char** argv,FXbool connect=TRUE);

	// Exit application
	virtual void exit(FXint code=0);

	// Delete application object
	virtual ~appDuelo();
};

#endif

