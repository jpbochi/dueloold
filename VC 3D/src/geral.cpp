
#include <windows.h>

#include <stdlib.h>
//#include <malloc.h>
//#include <memory.h>
//#include <tchar.h>

#include <gl\gl.h>

#include <stdarg.h>			// Header File For Variable Argument Routines
#include <stdio.h>			// Header File For Standard Input/Output

/* Trecho de c�digo para testar as distribui��es aleat�rias
double mini = 9999;
double maxi = 0;
double minr = 9999;
double maxr = 0;
//do {
	double med = 0;
	double dp = 0;
	double medr = 0;
	double dpr = 0;
	double n = 100000;
	for (int i=1; i<n; i++) {
		//double al = superrnd(1);
		//double al = rndi(0, 1);
		double al = Aleatorio(1);
		med = med + al / n;
		dp = dp + fabs(al - 1.03) / n; //coloque a m�dia q vc espera aqui para calcular o desvio padr�o
		maxi = (al > maxi) ? al : maxi;
		mini = (al < mini) ? al : mini;

		al = rnd();
		medr = medr + al / n;
		dpr = dpr + fabs(al - 0.5) / n;
		maxr = (al > maxr) ? al : maxr;
		minr = (al < minr) ? al : minr;
	}
//} while(true); //<-- ponha um breakpoint aqui
//*/

/*
extern void EndApp();
void DoEvents()
{
	MSG msg;

	if (hwnd != GetForegroundWindow()) WaitMessage();
    if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
		TranslateMessage( &msg );
		DispatchMessage( &msg );
		if(msg.message == WM_QUIT) EndApp();
	}

	/*
	if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
	{
		if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
		{
			exit(0);//done=TRUE;							// If So done=TRUE
		}
		else									// If Not, Deal With Window Messages
		{
			TranslateMessage(&msg);				// Translate The Message
			DispatchMessage(&msg);				// Dispatch The Message
		}
	}* /
}*/

/*void MsgBox(char *fmt, ...)
{
	char *		text;						// Holds Our String
	va_list		ap;							// Pointer To List Of Arguments

	if (fmt == NULL) {
		//nada
	} else {
		text = new char[256];
		va_start(ap, fmt);						// Parses The String For Variables
			vsprintf(text, fmt, ap);			// And Converts Symbols To Actual Numbers
		va_end(ap);								// Results Are Stored In Text

		//MessageBox(hWnd, text, "Duelo 3d",MB_ICONEXCLAMATION);
		delete text;
	}
}*/

double rnd()
{
	/*double num, media, soma=0, min=666, max=0, cont=0;
	while (true) 
	{
		cont++;
		num = rnd();
		max = Max(max, num);
		min = Min(min, num);
		soma += num;
		media = soma / cont;
	}*/
	//srand; rand; RAND_MAX;
	//ceil; floor;
	//srand ~= randomize
	//RAND_MAX = 0x7fff
	/*
	return (double)rand() / (double)(RAND_MAX + 0.1);
	/*/
	double r = rand() + (rand() << 15);
	return r / ((double)0x3FFFFFFF + 1);
	/**/
}

double rndd(double min, double max)
{
	return ((max - min) * rnd()) + min;
}

int rndi(int min, int max)
{
	return (int)(((max - min + 1) * rnd()) + min);
}

void glCor(short id)
{
	switch (id)
	{
		case 0:			{glColor3ub(  0,  0,  0); break;}
		case 1:			{glColor3ub(  0,  0,127); break;}
		case 2:			{glColor3ub(  0,127,  0); break;}
		case 3:			{glColor3ub(  0,127,127); break;}
		case 4:			{glColor3ub(127,  0,  0); break;}
		case 5:			{glColor3ub(127,  0,127); break;}
		case 6:			{glColor3ub(127,127,  0); break;}
		case 7:			{glColor3ub(127,127,127); break;}
		case 8:			{glColor3ub( 64, 64, 64); break;}
		case 9:			{glColor3ub( 64, 64,255); break;}
		case 10:		{glColor3ub( 64,255, 64); break;}
		case 11:		{glColor3ub( 64,255,255); break;}
		case 12:		{glColor3ub(255, 64, 64); break;}
		case 13:		{glColor3ub(255, 64,255); break;}
		case 14:		{glColor3ub(255,255, 64); break;}
		case 15:		{glColor3ub(255,255,255); break;}
		//case VERMELHO:	{glColor3ub(255,  0,  0); break;}
		//case VERDE:		{glColor3ub(  0,255,  0); break;}
		//case AZUL:		{glColor3ub(  0,  0,255); break;}
		default:		{glColor3ub(255,255,255); break;}
	}
}
