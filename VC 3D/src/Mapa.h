#ifndef MAPA_H
#define MAPA_H

#include "declarac.h"
#include "camera.h"
class cAfin;
class cCont;
class cChao;

class cListaPosObjetos;
class cObjeto;
class cObjetoSimples;
class cObjetos;

#define MAPA_DEFAULT_DIMX	30
#define MAPA_DEFAULT_DIMY	30
#define MAPA_DEFAULT_DIMZ	2

class cMapa
{
	friend cMapa;
public:
	enum //enuChao
	{
		NAO_CHAO = false,
		CHAO = true
	};

	cMapa(class cMapa *mapa, char c=0);
	cMapa(ushort x=MAPA_DEFAULT_DIMX,
		  ushort y=MAPA_DEFAULT_DIMY,
		  ushort z=MAPA_DEFAULT_DIMZ,
		  char c=0,
		  cAfin *_afin=0,
		  cAfin *_afinchao=0);
	~cMapa();

	void Cria(ushort x, ushort y, ushort z, char c, cAfin *_afin, cAfin *_afinchao);

	void Redimenciona(ushort x, ushort y, ushort z, char c=0);
	void Esvazia(char c=0);
	void EsvaziaObjetos();

	double AlturaSombra(double x, double y, double z);
	double AlturaSombra(int x, int y, double z);

	uint VetPos(ushort x, ushort y, ushort z);
	void VetPosInv(uint pos, ushort& x, ushort& y, ushort& z);

	cAfin Afin(ushort x, ushort y, ushort z, bool chao);
	cCont Cont(ushort x, ushort y, ushort z);
	cChao Chao(ushort x, ushort y, ushort z);
	char Cont(ushort x, ushort y, ushort z, bool chao);

	bool SetCont(ushort x, ushort y, ushort z, cCont val);
	bool SetChao(ushort x, ushort y, ushort z, cChao val);

	char GetResumoObj(ushort x, ushort y, ushort z);
	bool SetResumoObj(ushort x, ushort y, ushort z, char val);
	unsigned char AtualizaResumoObjsPos(ushort x, ushort y, ushort z);

	cListaPosObjetos* GetListaPos(ushort x, ushort y, ushort z);
	void AdObjPos(cObjeto &, ushort x, ushort y, ushort z);
	void AdObjSimpPos(cObjetoSimples, ushort x, ushort y, ushort z);

	float linewidth;
	bool Renderiza(bool selection = false, bool comburacos = true);
	bool RenderizaInicio(bool selection = false, bool comburacos = true);
	bool RenderizaMeio(bool selection = false, cMapa* Mask = 0, bool comburacos = true);
	bool RenderizaFim(bool selection = false, bool comburacos = true);

	bool CarregaMapa(const char* strmapa);

	void ConverteParaEditor();
	void ConverteParaJogo();

	void RotCameraHorizontal(double rot);
	void RotCameraVertical(double rot);

	void MoveXYCamera(double);
	void MoveXYZCamera(double);
	void StrafeHorCamera(double);
	void StrafeVertCamera(double);
	void MudaAndar(short);

	void CentraCam(int x1,    int y1,    int z1,
				   int x2=-1, int y2=-1, int z2=-1);

	ushort dimX, dimY, dimZ;

	char FiltroDeAndares;
	ushort selectedZ;
	
	bool MostraParedes;

	bool marca;
	ushort marcax, marcay, marcaz;

	//camera
	cCamera Cam;

protected:
	unsigned char AtualizaResumoObjs(uint vetpos, ushort x, ushort y, ushort z);

	cCont *cont; //ushort (*cont)[][][];
	class cObjetoSimples *resumoobjetos;
	ushort lencont;
	cChao *contchao;
	ushort lencontchao;
	
	cAfin *afin;
	cAfin *afinchao;
	void RecalculaAfinamento(ushort x, ushort y, ushort z);
	void RecalculaAfinamento();

	//objetos
	cObjetos *Objs;
};

#endif