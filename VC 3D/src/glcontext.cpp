
#define DECLARA_GLContexto
#include "glcontext.h"

cGLContexto::cGLContexto()
{
	glcanvas = 0;
	pos_pilha = 0;
	pilha[0] = 0;
}

void cGLContexto::SetaContexto(FXGLCanvas *_glcanvas)
{
	glcanvas = _glcanvas;
}

void cGLContexto::Empilha()
{
	if (glcanvas != 0
	&&  pos_pilha < 32) {
		pos_pilha++;
		pilha[pos_pilha] = 0;
		if (!glcanvas->isCurrent()) {
			pilha[pos_pilha] = 1;
			glcanvas->makeCurrent();
		}
	}
}

void cGLContexto::DesEmpilha()
{
	if (pilha[pos_pilha]
	&&  pos_pilha > 0) {
		pos_pilha--;
		glcanvas->makeNonCurrent();
	}
}
