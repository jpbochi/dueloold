
//#include "duelo3d.h"
#include "funcoes.h"
#include "conteudo.h"
#include "geral.h"
#include "projetil.h"
#include "jogo.h"
#include "iconebut.h"

#include <math.h>
//#include <stdio.h>

extern uint gllist[GLList::NUM_LISTS];
extern uint textures[MAX_TEXTURAS];
extern cJogo Jogo;

//teste
//#include "..\modelos\md3.h"
//extern CModelMD3 modelomd3;
#include "..\modelos\modelosmdx.h"
extern cModeloAnim modelo;

#define ManaIdentificacao	5
#define ManaPararTempo		55
#define ManaInvisibilidade	15
#define ManaTeletransporte	15
#define ManaImitar			20
#define ManaHolografia		10
#define ManaBolaDeFogo		25
#define ManaDardosMisticos	12

#define FeRezar				-25
#define FeCura				20
#define FeAnimar			15
#define FeRelampago			25

#define	TempoMirarMais		5
#define TempoPegar			15
#define	TempoSoltar			3
#define	TempoGuardar		10
#define	TempoSacar			10
#define	TempoVestirCabeca	20
#define	TempoVestirCorpo	50
#define	TempoDesvestirCabeca 10
#define	TempoDesvestirCorpo	30

#define	TempoAtropelar		15

#define	TempoAndarMoto		4
#define	TempoAndarRast		45
#define	TempoAndarAbai		25
#define	TempoAndarAbaiStraf	30
#define	TempoAndar			15
#define	TempoAndarStraf		20

#define TempoDefesaComEspada		20
#define TempoEsquivaMatrix			15
#define TempoIncubacaoAlien			30

#define DurezaPedra		1000
#define DurezaMedia		45
#define DurezaGrade		50
#define DurezaFraca		15
#define DurezaJog		30 //� somado com a blindagem

#define MAX_PROJETEIS	9

//atirar
#define PodNaoViraAoAtirar(K)		(J[K].Poder == pRobocop || J[K].Poder == pMosca || J[K].Poder == pTanque)
#define PodNaoPerdeMiraAoAndar(K)	(J[K].Poder == pRobocop || J[K].Poder == pTanque)
#define PodNaoAtingeGradesDist10(K) (J[K].Poder == pSabata)
#define PodMiraExtraPistolaERifle(K)(J[K].Poder == pSabata)

//pegar
#define PodNaoPodePegarNada(K)		(J[K].Poder == pRobocop || J[K].Poder == pJason || J[K].Poder == pZerg || J[K].Poder == pTanque || J[K].Poder == pAlienPequeno)
#define PodSoPegaItensSimples(K)	(J[K].Poder == pSabata)
#define PodDestroiObjetosDuros(K)	(J[K].Poder == pTarrasque || J[K].Poder == pDragao)
#define PodDestroiObjetos(K)		(PodDestroiObjetosDuros(K) || J[K].Poder == pBalacau || J[K].Poder == pAlienGrande || J[K].Poder == pPredador)
#define PodNaoDestroiDisco(K)		(J[K].Poder == pPredador)
#define PodDestroiKitMedico(K)		(J[K].Poder == pT_1000)
//#define PodDestroiCapacete(K)		(J[K].Poder == pMago)
//#define PodDestroiHolografia(K)		(J[K].Poder == pMago)
#define PodDestroiOculosIR(K)		(J[K].Poder == pMosca || J[K].Poder == pHomem_do_Raio_X)
#define PodNaoUsaColete(K)			(J[K].Poder == pNinja)
#define PodComeCorpos(K)			(J[K].Poder == pPredador || J[K].Poder == pWolverine || J[K].Poder == pTarrasque || J[K].Poder == pBalacau || J[K].Poder == pDragao || J[K].Poder == pAlienGrande)

//movimento
#define PodNaoUsaStrafe(K)			(J[K].Poder == pRobocop || J[K].Poder == pTanque)
#define PodAndaDeRe(K)				(J[K].Poder == pTanque)
#define PodAndaDevagarAoSerVisto(K) (J[K].Poder == pJason)
#define PodViraDevagar(K)			(J[K].Poder == pTanque)
#define PodAtravessaTijolo(K)		(J[K].Poder == pFantasma || J[K].Poder == pTarrasque || J[K].Poder == pDragao)
#define PodAtravessaMadeira(K)		(J[K].Poder == pRobocop || J[K].Poder == pBalacau || J[K].Poder == pAlienGrande || J[K].Poder == pPredador || J[K].Poder == pTanque || J[K].Poder == pT_1000)
#define PodAtravessaGrade(K)		(J[K].Poder == pT_1000 || J[K].Poder == pMagrao || J[K].Poder == pAlienPequeno)
#define PodQuebraParede(K)			(J[K].Poder == pTarrasque || J[K].Poder == pDragao)
//#define PodQuebraGrade(K)			(J[K].Poder == pT_1000) DESABILITADA
#define PodSabeArrombarPortas(K)	(J[K].Poder == pNinja)
#define PodRachaChao(K)				(J[K].Poder == pTarrasque)
#define PodFlutua(K)				(J[K].Poder == pVoador || J[K].Poder == pMosca || J[K].Poder == pEthereal || J[K].Poder == pFantasma)
#define PodVoa(K)					(J[K].Poder == pVoador || J[K].Poder == pDragao)
//2 observa��es falsas: PodFlutua(K) => PodVoa(K); PodVoa(K) => PodFlutua(K)

//corpo a corpo
#define PodBate(K)					(J[K].Poder == pAlienGrande || J[K].Poder == pPredador || J[K].Poder == pTarrasque || J[K].Poder == pBalacau || J[K].Poder == pDragao || J[K].Poder == pT_1000 || J[K].Poder == pTanque)
#define PodImpossibilitaEsquiva(K)	(J[K].Poder == pTarrasque || J[K].Poder == pDragao || J[K].Poder == pTanque)
#define PodDefendeComEspada(K)		(J[K].Poder == pJedi)
#define PodSuperEsquiva(K)			(J[K].Poder == pAgente_do_Matrix)
#define PodIncubacaoAlien(K)		(J[K].Poder == pAlienPequeno)
#define PodImuneIncubacaoAlien(K)	(J[K].Poder == pTanque)

//vis�o
#define PodSempreVeBotsNeutros(K)	(J[K].Poder == pAgente_do_Matrix)
#define PodSempreVeParasitados(K)	(J[K].Poder == pZerg)
#define PodVisaoRaioX(K)			(J[K].Poder == pHomem_do_Raio_X)
#define PodVisao360Graus(K)			(J[K].Poder == pMosca)

//outros
#define PodImuneAFogo(K)			(J[K].Poder == pRobocop || J[K].Poder == pDragao || J[K].Poder == pTanque)
#define PodResistenteAFogo(K)		(J[K].Poder == pT_1000)
#define PodImuneOvo(K)				(J[K].Poder == pT_1000 || J[K].Poder == pRobocop)
#define PodCamuflagemControlavel(K)	(J[K].Poder == pBalacau || J[K].Poder == pPredador)
#define PodEngole(K)				(J[K].Poder == pTarrasque)
#define PodSemRegeneracao(K)		(J[K].Poder == pT_1000 || J[K].Poder == pTanque || J[K].Poder == pRobocop)
#define PodRegeneracaoT1000(K)		(J[K].Poder == pT_1000)
#define PodCorpoFrio(K)				(J[K].Poder == pT_1000)
#define PodPodeExplodirTanque(K)	(J[K].Poder == pTanque)
#define PodPodeExplodirDinamite(K)	(J[K].Poder == pDinamitador)
#define PodSubstituiBot(K)			(J[K].Poder == pAgente_do_Matrix)
#define PodRessuscita(K)			(J[K].Poder == pJason)
#define PodUsaMagiasArcanas(K)		(J[K].Poder == pMago)
//TO DO: PodCausaMedo; PodNaoSenteMedo

//armas
//TO DO: Transformar estas coisa feias abaixo em flags
#define ArmaNaoViraAoAtirar(L)	(L == aDisco || L == aShuriken)
#define ArmaEhIncendiaria(L)	(L == aLancaChamas || L == aBaforada || L == aBolaDeFogo)
#define ArmaRecarregaComMira(L) (L == aCanhao || L == aCanhao)
#define ArmaEhCorpoACorpo(L)	(L >=0 && Armas[abs(L)].DanoGolpe > 0)
//#define ArmaEhExplosiva(L)		(L >=0 && Armas[abs(L)].RaioExplosao > 0)

void cJogo::HUDMostra(bool mostra)
{
	fundopreto = !mostra;
	if (mostra) pwinMain->frmJogo->show();
	else		pwinMain->frmJogo->hide();
}

void cJogo::Reescreve()
{
	//char *tmp = new char[32];
	//char *tmp2 = new char[32];
	FXString str, str2;
	
	//andar
	if (pMapa != NULL) {
		switch (pMapa->FiltroDeAndares) {
			case 0:
				pwinMain->lblAndar->setText(str.format("%d� andar [%c]", pMapa->selectedZ + 1, '�'));
			break;
			case 1:
				pwinMain->lblAndar->setText(str.format("%d� andar [%c]", pMapa->selectedZ + 1, '|'));
			break;
			case 2:
				pwinMain->lblAndar->setText(str.format("%d� andar [%c]", pMapa->selectedZ + 1, '�'));
			break;
		}
	}
	//Nome
	pwinMain->lblNome->setText(J[vz].Nome);
	//Poder
	J[vz].PoderStr(str);
	pwinMain->lblPoder->setText(str);
	//Arma
	bool mostrabotoes = false;
	/*
	short armamao = J[vz].ArmaMao;
	if (armamao != -1) {
		if (Jogo.Armas[armamao].Dano == 0) {
			str.format("%s", Jogo.Armas[armamao].Nome);
		} else {
			if (armamao == aBolaDeFogo) { //bola de fogo gasta mana
				str2.format("%d", J[vz].ManaFe);
			} else {
				str2.format("%d|%d", J[vz].Mun[armamao], J[vz].MunExtra[armamao]);
			}
			if (J[vz].GetFlag(Mirando)) {
				str.format("%.1f - %s - %s", J[vz].MiraAtual * J[vz].MiraBonus,
											 Jogo.Armas[armamao].Nome,
											 str2);
				mostrabotoes = true;
			} else {
				str.format("%s - %s", Jogo.Armas[armamao].Nome, str2);
				mostrabotoes = true;
			}
		}
	/*/
	cObjeto* armamao = J[vz].ArmaMao();
	if (armamao != 0) {
		ushort codarma = armamao->GetCod();
		ushort codmun = armamao->codmunicao;
		if (Jogo.Municoes[codmun].Dano == 0) {
			str.format("%s", Jogo.Armas[codarma].Nome);
		} else {
			if (armamao->UsaManaFe()) { //bola de fogo gasta mana
				str2.format("%d", J[vz].ManaFe);
			} else {
				str2.format("%d", armamao->municao);
			}
			if (J[vz].GetFlag(Mirando)) {
				str.format("%.1f - %s - %s", J[vz].MiraAtual * J[vz].MiraBonus,
											 Jogo.Armas[codarma].Nome,
											 str2);
				mostrabotoes = true;
			} else {
				str.format("%s - %s", Jogo.Armas[codarma].Nome, str2);
				mostrabotoes = true;
			}
		}
	/**/
	} else {
		str.format("");
	}
	pwinMain->lblArma->setText(str);
	if (mostrabotoes) {
		pwinMain->butMirar->enable();
		//pwinMain->butRecarregar->enable();
	} else {
		pwinMain->butMirar->disable();
		//pwinMain->butRecarregar->disable();
	}
	if (mostrabotoes && J[vz].GetFlag(Mirando)) {
		pwinMain->butMirarMais->enable();
		pwinMain->butAtirar->enable();
		pwinMain->butMirar->setTipText("Trocar alvo");
	} else {
		pwinMain->butMirarMais->disable();
		pwinMain->butAtirar->disable();
		pwinMain->butMirar->setTipText("Escolher alvo");
	}
	//Resist�ncia
	pwinMain->lblHT->setText(str.format("%.1f/%.1f", J[vz].Ht, J[vz].HtMax));
	pwinMain->barHT->setTotal(   (FXuint)floor(J[vz].HtMax * 100));
	pwinMain->barHT->setProgress((FXuint)floor(J[vz].Ht    * 100));
	
	//Tempo
	pwinMain->lblTempo->setText(str.format("%d/%d", J[vz].T, J[vz].TMax));
	pwinMain->barTempo->setTotal(   J[vz].TMax * 100);
	pwinMain->barTempo->setProgress(J[vz].T    * 100);
	
	//M�os
	pIc->PreencheBut(pwinMain->butMaoDir, J[vz].objMaoDir);
	pIc->PreencheBut(pwinMain->butMaoEsq, J[vz].objMaoEsq);
	pwinMain->MostraIcones();
	
	//TO DO: escrever os tempos para as a��es nos bot�es (ou n�o)

	//TO DO: esconder/mostrar os bot�es que podem (ou n�o) serem usados
	
	//pwinMain->frmJogo->forceRefresh();
}

void Espera(int espera)
{
	//TO DO: implementar Espera
}

void ApagaConsole()
{
	//TO DO: implementar ApagaConsole
}

bool drawScene(double dTime)
{
	//TO DO: implementar drawScene
	return true;
}

cJogo::cJogo()
{
	Estado = EST_INTRO;
	JogoComecou = false;
	Console = false;
	pMapa = &Mapa;
	numjogs = 2;
	J = NULL;
	Armas = NULL;
	Municoes = NULL;
	CarregaArmasPadrao();
	
	vz = -1;
	JaVi = NULL;
	JaViHol = NULL;
	TempoParado = false;
	enuComando = CmdNada;
	
	NaoAnimando = true;
	pIc = NULL;

	fundopreto = false;
}

void cJogo::Reinicia()
{
	MudaEstado(EST_INTRO);

	delete [numjogs] J;
	delete [numjogs] JaVi;
	delete [numjogs] JaViHol;

	cJogo();
}

void cJogo::CarregaArmasPadrao()
{
	Municoes = new cMunicao[NUM_MUN];
	#define MBF ManaBolaDeFogo
	//      ID										NOME        Mun  MunR DCam NPro Dano Expl Degn  Alc ArmaDest
	Municoes[mPistola].Inicia(mPistola,				"AP",		12,  12,  1.5, 1,   20,  0,   0,    0, aPistola);
	Municoes[mEspingarda].Inicia(mEspingarda,		"Shred",	8,   1,   2.5, 5,   15,  0,   0,    0, aEspingarda);
	Municoes[mMetralhadora].Inicia(mMetralhadora,	"AP",		30,  30,  2,   1,   30,  0,   0,    0, aMetralhadora);
	Municoes[mLancaFog].Inicia(mLancaFog,			"HE",		1,   1,   2.5, 1,   80,  3.5, 0,    0, aLancaFog);
	Municoes[mRifle].Inicia(mRifle,					"AP",		10,  10,  1.5, 1,   50,  0,   0,    0, aRifle);
	Municoes[mDegen].Inicia(mDegen,					"Degen",	5,   5,   1.2, 1,   10,  0,   0.6,  0, aDegen);
	Municoes[mDisco].Inicia(mDisco,					"Corte",	1,   -1,   1.1, 1,   50,  0,   0.2,  0, aDisco);
	Municoes[mTriLaser].Inicia(mTriLaser,			"AP",		64,  32,  2,   1,   50,  0,   0,    0, aTriLaser);
	Municoes[mMiniGun].Inicia(mMiniGun,				"HE",		20,  20,  2.5, 1,   40,  2,   0,    0, aMiniGun);
	Municoes[mLancaChamas].Inicia(mLancaChamas,		"Incend",	10,  10,  3,   3,   13,  0,   0.15, 4, aLancaChamas);
	Municoes[mRicochet].Inicia(mRicochet,			"Ric-AP",	15,  15,  1.5, 1,   20,  0,   0,    0, aRicochet);
	Municoes[mRoboArma].Inicia(mRoboArma,			"AP",		50,  50,  2,   1,   25,  0,   0,    0, aRoboArma);
	Municoes[mParalisante].Inicia(mParalisante,		"Paral",	6,   6,   1.3, 1,   0,   0,   0,    0, aParalisante);
	Municoes[mBaforada].Inicia(mBaforada,			"Incend",	1,   0,   4,   3,   25,  0,   0.1,  10,aBaforada);
	Municoes[mTeletrans].Inicia(mTeletrans,			"Teletrans",15,  15,  2,   1,   5,   0,   0,    0, aTeletrans);
	Municoes[mBlaster].Inicia(mBlaster,				"HE",		1,   1,   2.5, 1,   140, 5,   0,    0, aBlaster);
	Municoes[mEspinhos].Inicia(mEspinhos,			"Shred",	200, -1,  1.4, 3,   20,  0,   0,    18,aEspinhos);
	Municoes[mMG34].Inicia(mMG34,					"AP",		100, 100, 3,   1,   30,  0,   0,    0, aMG34);
	Municoes[mCanhaoHE].Inicia(mCanhaoHE,			"HE",		30,  0,   5,   1,   90,  2.5, 0,    0, aCanhao);
	Municoes[mCanhaoAP].Inicia(mCanhaoAP,			"AP",		30,  0,   5,   1,   180, 0,   0,    0, aCanhao);
	Municoes[mBolaDeFogo].Inicia(mBolaDeFogo,       "HE-Incend",MBF, 0,   1.2, 1,   60,  4,   0.1,  0, aBolaDeFogo);
	Municoes[mShuriken].Inicia(mShuriken,			"AP",		15,  -1,  1,   1,   20,  0,   0,    10,aShuriken);
	Municoes[mTrabuco].Inicia(mTrabuco,				"Shred",	1,   1,   3,   9,    8,  0,   0,    0, aTrabuco);
	#undef MBF
	
	Armas = new cArma[NUM_ARMAS];
	//*
	//    ID									NOME                  MunE Golp MIni MMax Mira TMir TAti TRec CTir CMov MUN SOM
	Armas[aPistola].Inicia(aPistola,			"Pistola",            2,   0,   4,   16,  1.4, 6,   12,  15,  .8,  .75, mPistola, SOM_TIRO);
	Armas[aEspingarda].Inicia(aEspingarda,		"Espingarda",         1,   0,   7,   10,  1.2, 10,  20,  10,  .9,  .9,  mEspingarda, SOM_ESPING);
	Armas[aMetralhadora].Inicia(aMetralhadora,	"Metralhadora",       2,   0,   3,   15,  1.8, 10,  8,   30,  .85, .85, mMetralhadora, SOM_MET);
	Armas[aLancaFog].Inicia(aLancaFog,			"Lan�a-Foguetes",     3,   0,   9,   25,  1.6, 20,  15,  30,  0,   0,   mLancaFog, SOM_FOGUETE);
	Armas[aRifle].Inicia(aRifle,				"Rifle",              2,   0,   2,   60,  2.2, 15,  20,  25,  0,   0,   mRifle, SOM_TIRO);
	Armas[aDegen].Inicia(aDegen,				"Degeneradora",       3,   0,   4,   14,  1.3, 8,   10,  25,  .75, .75, mDegen, SOM_TIRO);
	Armas[aDisco].Inicia(aDisco,				"Disco",              1,   0,   6,   8,   1.1, 5,   10,  10,  0,   1,   mDisco, SOM_NENHUM);
	Armas[aTriLaser].Inicia(aTriLaser,			"Tri-Laser",          2,   0,   5,   23,  1.6, 15,  15,  25,  .95, .7,  mTriLaser, SOM_LASER);
	Armas[aMiniGun].Inicia(aMiniGun,			"Mini Gun",           1,   0,   3,   12,  1.6, 15,  8,   30,  .8,  .6,  mMiniGun, SOM_MG);
	Armas[aLancaChamas].Inicia(aLancaChamas,	"Lan�a-Chamas",       1,   0,   7,   9,   1.2, 15,  15,  35,  1,   1,   mLancaChamas, SOM_BAFORADA);
	Armas[aRicochet].Inicia(aRicochet,			"Ricocheteadora",     2,   0,   4,   14,  1.3, 6,   8,   25,  .75, .75, mRicochet, SOM_TIRO);
	Armas[aRoboArma].Inicia(aRoboArma,			"Robo-Arma",          2,   0,   8,   40,  1.7, 15,  10,  35,  .99, 0,   mRoboArma, SOM_MET);
	Armas[aParalisante].Inicia(aParalisante,	"Paralisante",        1,   0,   4,   16,  1.4, 8,   10,  25,  .75, .75, mParalisante, SOM_TIRO);
	Armas[aBaforada].Inicia(aBaforada,			"Baforada",           0,   0,   8,   10,  1.1, 8,   20,  -1,   1,   1,   mBaforada, SOM_BAFORADA);
	Armas[aTeletrans].Inicia(aTeletrans,		"Teletrans",		  1,   0,   4,   14,  1.3, 6,   8,   25,  .85, .85, mTeletrans, SOM_LASER);
	Armas[aBlaster].Inicia(aBlaster,			"LMT-UltraBlaster",   2,   0,   10,  35,  1.6, 65,  25,  30,  0,   0,   mBlaster, SOM_FOGUETE);
	Armas[aMachado].Inicia(aMachado,			"Machado",            0,   34,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	Armas[aEspinhos].Inicia(aEspinhos,			"Espinhos",           0,   0,   8,   13,  1.2, 6,   25,  0,   .95, .95, mEspinhos, SOM_NENHUM);
	Armas[aMG34].Inicia(aMG34,					"MG-34",              1,   0,   4,   15,  1.4, 18,  5,   40,  .85, .7,  mMG34, SOM_MG);
	Armas[aCanhao].Inicia(aCanhao,				"Canh�o",             30,  0,   8,   45,  1.7, 25,  20,  -1,  .9,  .7,  mCanhaoHE, SOM_CANHAO);
	Armas[aBolaDeFogo].Inicia(aBolaDeFogo,		"Bola de Fogo",       0,   0,   15,  60,  1.4, 15,  15,  0,   0,   1,   mBolaDeFogo, SOM_NENHUM);
	Armas[aSabreDeLuz].Inicia(aSabreDeLuz,		"Sabre de Luz",       0,   44,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	Armas[aShuriken].Inicia(aShuriken,			"Estrelinhas",        0,  0,   7,   9,   1.1, 6,   6,   6,   0,   1,   mShuriken, SOM_NENHUM);
	Armas[aKatana].Inicia(aKatana,				"Katana",             0,   38,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	Armas[aSoco].Inicia(aSoco,					"Soco",               0,   -.3, 0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_SOCO);
	Armas[aTrabuco].Inicia(aTrabuco,			"Trabuco",            9,   0,   5,   9,   1.2, 10,  10,  50,  0,   .9,  mTrabuco, SOM_ESPING);
	//Armas[aDedoDuro].Inicia(aDedoDuro,		"Dedo Duro(Medo)",    0,   0,   10,  20,  1.4, 5,   25,  0,   1,   1,   mDedoDuro, SOM_NENHUM);
	Armas[aSnikt].Inicia(aSnikt,				"Snikt",              0,   42,  0,   0,   0,   0,   0,   0,   0,   0,   mSemMunicao, SOM_ESPADA);
	/*/
	//              ID                  NOME                  Mun  MunR MunE DCam NPro Dano Expl Degn  Alc Golp MIni MMax Mira TMir TAti TRec CTir CMov SOM
	Armas[0].Inicia(aPistola,			"Pistola",            12,  12,  24,  1.5, 1,   20,  0,   0,    0,  0,   4,   16,  1.4, 6,   12,  25,  .75, .75, SOM_TIRO);
	Armas[1].Inicia(aEspingarda,		"Espingarda",         8,   1,   8,   2.5, 5,   15,  0,   0,    0,  0,   7,   10,  1.2, 10,  20,  15,  .9,  .9,  SOM_ESPING);
	Armas[2].Inicia(aMetralhadora,		"Metralhadora",       30,  30,  60,  2,   1,   30,  0,   0,    0,  0,   3,   15,  1.8, 10,  8,   30,  .85, .85, SOM_MET);
	Armas[3].Inicia(aLancaFoguetes,		"Lan�a-Foguetes",     1,   1,   3,   2.5, 1,   80,  3.5, 0,    0,  0,   9,   25,  1.6, 20,  15,  30,  0,   0,   SOM_FOGUETE);
	Armas[4].Inicia(aRifle,				"Rifle",              10,  10,  20,  1.5, 1,   50,  0,   0,    0,  0,   2,   60,  2.2, 15,  20,  25,  0,   0,   SOM_TIRO);
	Armas[5].Inicia(aDegeneradora,		"Degeneradora",       5,   5,   15,  1.2, 1,   10,  0,   0.6,  0,  0,   4,   14,  1.3, 8,   10,  25,  .75, .75, SOM_TIRO);
	Armas[6].Inicia(aDisco,				"Disco",              1,   1,   1,   1.1, 1,   50,  0,   0.2,  0,  0,   6,   8,   1.1, 5,   10,  10,  0,   0,   SOM_NENHUM);
	Armas[7].Inicia(aTriLaser,			"Tri-Laser",          64,  32,  128, 2,   1,   50,  0,   0,    0,  0,   5,   23,  1.6, 15,  15,  25,  .95, .7,  SOM_LASER);
	Armas[8].Inicia(aMiniGun,			"Mini Gun",           20,  20,  20,  2.5, 1,   40,  2,   0,    0,  0,   3,   12,  1.6, 15,  8,   30,  .8,  .6,  SOM_MG);
	Armas[9].Inicia(aLancaChamas,		"Lan�a-Chamas",       10,  10,  10,  3,   3,   13,  0,   0.15, 4,  0,   7,   9,   1.2, 15,  15,  50,  1,   1,   SOM_BAFORADA);
	Armas[10].Inicia(aRicocheteadora,	"Ricocheteadora",     15,  15,  30,  1.5, 1,   20,  0,   0,    0,  0,   4,   14,  1.3, 6,   8,   25,  .75, .75, SOM_TIRO);
	Armas[11].Inicia(aRoboArma,			"Robo-Arma",          50,  50,  100, 2,   1,   25,  0,   0,    0,  0,   8,   40,  1.7, 15,  10,  35,  .99, 0,   SOM_MET);
	Armas[12].Inicia(aParalisante,		"Paralisante",        6,   6,   6,   1.3, 1,   0,   0,   0,    0,  0,   4,   16,  1.4, 8,   10,  25,  .75, .75, SOM_TIRO);
	Armas[13].Inicia(aBaforada,			"Baforada",           1,   0,   0,   4,   3,   25,  0,   0.1,  10, 0,   8,   10,  1.1, 8,   20,  0,   1,   1,   SOM_BAFORADA);
	Armas[14].Inicia(aTeletrans,		"Teletrans",		  15,  15,  15,  2,   1,   5,   0,   0,    0,  0,   4,   14,  1.3, 6,   8,   25,  .85, .85, SOM_LASER);
	Armas[15].Inicia(aBlaster,			"LMT-UltraBlaster",   1,   1,   2,   2.5, 1,   140, 5,   0,    0,  0,   10,  35,  1.6, 65,  25,  30,  0,   0,   SOM_FOGUETE);
	Armas[16].Inicia(aMachado,			"Machado",            0,   0,   0,   0,   0,   0,   0,   0,    0,  34,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	Armas[17].Inicia(aEspinhos,			"Espinhos",           200, 0,   0,   1.4, 3,   20,  0,   0,    18, 0,   8,   13,  1.2, 6,   25,  0,   .95, .95, SOM_NENHUM);
	Armas[18].Inicia(aMG34,				"MG-34",              100, 100, 100, 3,   1,   30,  0,   0,    0,  0,   4,   15,  1.4, 18,  5,   40,  .85, .7,  SOM_MG);
	Armas[19].Inicia(aCanhaoHE,			"Canh�o (HE)",        1,   1,   30,  5,   1,   90,  2.5, 0,    0,  0,   8,   45,  1.7, 25,  20,  40,  .9,  .7,  SOM_CANHAO);
	Armas[20].Inicia(aCanhaoAP,			"Canh�o (AP)",        1,   1,   30,  5,   1,   180, 0,   0,    0,  0,   8,   45,  1.7, 25,  20,  40,  .9,  .7,  SOM_CANHAO);
	Armas[21].Inicia(aBolaDeFogo, "Bola de Fogo",ManaBolaDeFogo,   0,   -1,  1.2, 1,   60,  4,   0,    0,  0,   15,  60,  1.4, 15,  15,  0,   0,   0,   SOM_NENHUM);
	Armas[22].Inicia(aSabreDeLuz,		"Sabre de Luz",       0,   0,   0,   0,   0,   0,   0,   0,    0,  44,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	Armas[23].Inicia(aShuriken,			"Estrelinhas",        1,   1,   15,  1,   1,   20,  0,   0,    10, 0,   7,   9,   1.1, 6,   6,   6,   0,   0,   SOM_NENHUM);
	Armas[24].Inicia(aKatana,			"Katana",             0,   0,   0,   0,   0,   0,   0,   0,    0,  38,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	Armas[25].Inicia(aSoco,				"Soco",               0,   0,   0,   0,   0,   0,   0,   0,    0,  -.3, 0,   0,   0,   0,   0,   0,   0,   0,   SOM_SOCO);
	Armas[26].Inicia(aTrabuco,			"Trabuco",            1,   1,   9,   3,   9,   8,   0,   0,    0,  0,   5,   9,   1.2, 10,  10,  20,  0,   .9,  SOM_ESPING);
	Armas[27].Inicia(aDedoDuro,			"Dedo Duro(Medo)",    999, 0,   0,   3,   1,   1,   0,   0,    .1, 0,   10,  20,  1.4, 5,   25,  0,   1,   1,   SOM_NENHUM);
	Armas[28].Inicia(aSnikt,			"Snikt",              0,   0,   0,   0,   0,   0,   0,   0,    0,  42,  0,   0,   0,   0,   0,   0,   0,   0,   SOM_ESPADA);
	/**/
	//TO DO: efeitos diferenciados das armas
	//V disco e estrelinhas > n�o � necess�rio se virar na dire��o do 'tiro'
	//= espingarda > regarrega de um em um cartucho
	//= canh�o do tanque, baforada > recarregam sozinhos a cada in�cio de rodada
	//= bola de fogo > gasta mana, i. e., n�o tem uma muni��o separada
}

FXuint cJogo::Msg(FXuint opts, FXString msg){
	return FXMessageBox::information(pwinMain, opts, "Duelo 3D", msg.text());
}

FXuint cJogo::MsgSemTempo(char *msg, short t){
	return FXMessageBox::information(pwinMain, MBOX_OK, "Duelo 3D", "Voc� n�o tem tempo para %s! (%d)", msg, t);
}

FXuint cJogo::MsgDef(FXString msg){
	return FXMessageBox::information(pwinMain, MBOX_OK, "Duelo 3D", msg.text());
}

void cJogo::MudaEstado(int novoEstado)
{
	/*switch (Estado)			//antes de mudar
	{
		case EST_INTRO:		break;
		case EST_EDITANDO:	break;
		case EST_NOVOJOGO:	break;
		case EST_JOGANDO:	break;
		case EST_MENSAGEM:	break;
	}*/
	/*
	Estado = novoEstado;
	switch (Estado)			//depois de mudar
	{
		case EST_INTRO:		pMapa = &Mapa;loadIntroUI();break;
		case EST_EDITANDO:	pMapa = &Mapa;loadEditorUI();break;
		case EST_NOVOJOGO:	pMapa = &Mapa;loadNovoJogoUI();break;
		case EST_JOGANDO:
			JogoComecou=true;
			loadJogandoUI(Console, &J[vz]);
			break;
		case EST_MENSAGEM:		loadMensagemUI(NULL, MSG_OKONLY);break;
		case EST_VENDOREPLAY:	loadJogandoUI(false, &J[vz], false);
	}*/
	
	Estado = novoEstado;
	switch (Estado)			//depois de mudar
	{
		case EST_INTRO:		pMapa = &Mapa;break;
		case EST_EDITANDO:	pMapa = &Mapa;break;
		case EST_NOVOJOGO:	pMapa = &Mapa;break;
		case EST_JOGANDO:	JogoComecou=true;break;
		//case EST_MENSAGEM:		loadMensagemUI(NULL, MSG_OKONLY);break;
		case EST_VENDOREPLAY:	break;
	}
}

void cJogo::ToggleConsole()
{
	//TO DO: decidir o que fazer com isso
	/*
	if (Estado == EST_JOGANDO) {
		Console = !Console;
		loadJogandoUI(Console, &J[vz]);
	}*/
}

void cJogo::DesenhaJogador(short j, short x, short y, short z, short frente)
{
	if (x == -1) x = J[j].x;
	if (y == -1) y = J[j].y;
	if (z == -1) z = J[j].z;
	if (frente == -1) frente = J[j].Frente;

	glPushMatrix();

	//posiciona
	double xh, yh, zh;
	CentroHex3D(x, y, z, xh, yh, zh, 0);
	glTranslated(xh, yh, zh);

	//rotaciona
	glRotatef(30 * (float)frente, 0,0,1);

	//rederiza
	//glColor3ub(J[j].Cor[0], J[j].Cor[1], J[j].Cor[2]);
	
	//glEnable(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, textures[J[j].Textura]);
	//glCallList(gllist[GLLIST_JOG]);

	//teste
	/*glPushMatrix();
	glDisable(GL_LIGHT0);
	glDisable(GL_CULL_FACE);
	glColor3ub(255, 255, 255);

	glScalef(0.025f,0.025f,0.025f);
	glTranslated(0, 0, 24.0);
	glFrontFace(GL_CW);
	modelomd3.DrawModel();
	glFrontFace(GL_CCW);

	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHT0);
	glPopMatrix();*/
	//modelo.DisplayGL();
	
	J[j].modelo.DisplayGL();

	glPopMatrix();
}

void cJogo::Renderiza(double fps, bool selection)
{
	if (!fundopreto) {
		static int tickantes, tickdepois, numframes = 0;
		if (numframes == 0)	tickantes = GetTickCount();

		if ((Estado == EST_JOGANDO)
		|| ((Estado == EST_MENSAGEM) && JogoComecou)) {
			J[vz].Fog->RenderizaInicio(selection);

			//Ilumumina jogador
			if (!selection) {
				double xh, yh, zh;
				CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xh, yh, zh, 0.9);
				GLfloat pos[4] = {(float)xh, (float)yh, (float)zh, 1};
				glLightfv(GL_LIGHT0, GL_POSITION, pos);
			}

			//Rederiza jogadores
			for (int i=0; i<numjogs; i++)
			if (EstaVendo(i) || (i == vz && !selection))
			{
				if (selection) glLoadName(Mapa.VetPos(J[i].x, J[i].y, J[i].z));
				if (fps > 0) {
					//Se estiver no meio de uma anima��o (NaoAnimando == 0)
					//Ent�o pode parar a anima��o quando a anima��o do modelo terminar
					//Sen�o mant�m NaoAnimando em 1
					FXuint loop = J[i].modelo.AnimAvanca(Aleatorio(1, 1.5) * 7.5 / fps);
					if (!NaoAnimando && loop && i == vz) NaoAnimando = 1;
				}
				DesenhaJogador(i);
			}

			if (!selection) {
				//Renderiza tiros
				for (i=0; i<contlinhatiro; i++) {
					cLinhaTiro &l = linhatiro[i];
					glColor3ub(50, 150, 50);
					//RenderizaLinha(l.largura, l.x1, l.y1, l.z1, l.x2, l.y2, l.z2);
					l.Renderiza();
				}

				//Renderiza sombra dos tiros
				glColor3ub(0, 0, 0);
				for (i=0; i<contsombratiro; i++) {
					//TO DO: As sombras devem ser faixas no ch�o, e n�o tubos
					sombratiro[i].Renderiza();
				}

				//Renderiza possivel alvo
				if (enuComando != CmdNada && J[vz].Fog->marca) {
					double xo, yo, zo, xa, ya, za;
					CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xo, yo, zo, 0.5);
					CentroHex3D(J[vz].Fog->marcax, J[vz].Fog->marcay, J[vz].Fog->marcaz, xa, ya, za, 0.5);
					
					glColor3ub(10, 10, 150);
					cLinhaTiro l(0.025, xo, yo, zo, xa, ya, za);
					l.Renderiza();

					glColor3ub(0, 0, 0);
					sombra_poss_mira.Gera(l, 0.025, xo, yo, zo, &Mapa);
					sombra_poss_mira.Renderiza();
				}

				//Renderiza alvo
				//TO DO: renderizar v�rias linhas para os passos do blaster
				//TO DO: fazer faixas na linha de mira (verde/amarelo/vermelho/preto) dependendo da mira atual do vz
				if (J[vz].GetFlag(Mirando)) {
					double xo, yo, zo, xa, ya, za;
					CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xo, yo, zo, 0.5);
					CentroHex3D(J[vz].AlvoX[0], J[vz].AlvoY[0], J[vz].AlvoZ[0], xa, ya, za, 0.5);
					
					double esccor = 0.15 * J[vz].MiraAtual * J[vz].MiraBonus;
					cLinhaTiro l(0.025, xo, yo, zo, xa, ya, za);
					l.Renderiza(esccor);
					
					glColor3ub(0, 0, 0);
					sombra_mira.Gera(l, 0.025, xo, yo, zo, &Mapa);
					sombra_mira.Renderiza();
				}
			}

			J[vz].Fog->RenderizaMeio(selection, J[vz].FogBit);
			J[vz].Fog->RenderizaFim(selection);

		} else if (Estado == EST_VENDOREPLAY && !selection) {
			J[vz].Fog->RenderizaInicio(selection);

			//Ilumumina jogador
			if (!selection) {
				double xh, yh, zh;
				CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xh, yh, zh, 0.9);
				GLfloat pos[4] = {(float)xh, (float)yh, (float)zh, 1};
				glLightfv(GL_LIGHT0, GL_POSITION, pos);
			}

			//Rederiza jogadores
			DesenhaJogador(vz);
			DesenhaJogador(sim.idop, sim.x, sim.y, sim.z, sim.frente);

			//TO DO: mostrar efeitos para as a��es que o cara fez
			switch (sim.foto->acao) {
				//case AcaoMove:				break;
				//case AcaoVira:				break;
				case AcaoMudaCursor:		break;
				case AcaoAtira: {
				
					double xo, yo, zo;
					CentroHex3D(sim.x, sim.y, sim.z, xo, yo, zo, 0.5);

					glColor3ub(50, 150, 50);
					//RenderizaLinha(0.025, xo, yo, zo, sim.foto->inf[0].f, sim.foto->inf[1].f, sim.foto->inf[2].f);

					cLinhaTiro(0.025, xo, yo, zo,
									  sim.foto->inf[0].f, sim.foto->inf[1].f, sim.foto->inf[2].f)
									  .Renderiza();
				} break;
				case AcaoRecarrega:			break;
				case AcaoArremessa:			break;
				case AcaoLancaMagia:		break;
				case AcaoInstalaBomba:		break;
				case AcaoPega:				break;
				case AcaoSeTeletransporta:	break;
				case AcaoGolpeia:			break;
				case AcaoAbrePorta:			break;
			}

			J[vz].Fog->RenderizaMeio(selection, J[vz].FogBit);
			J[vz].Fog->RenderizaFim(selection);
		} else {
			Mapa.Renderiza(selection);
		}
	}
}

void cJogo::Click(ushort mx, ushort my, ushort mz, int button)
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//MODIF: o c�digo para posicionar os pontos do blaster foi retirado
	//TO DO: implementar posicionamento dos pontos do blaster

	if (button & RIGHTBUTTONMASK && enuComando != CmdNada) {
		//bot�o direto cancela qualquer comando, e vira quando n�o houver comando
		enuComando = CmdNada;
	} else {
		switch (enuComando) {
			case CmdMirar:
				if (Mira(mx, my, mz)) enuComando = CmdNada;
			break;
			case CmdJediPega:
				//
			break;
			case CmdArremessar:
				//
			break;
			case CmdControlar:
				//
			break;
			case CmdIdentificar:
				//
			break;
			case CmdHolografar:
				//
			break;
			case CmdAnimarMortos:
				//
			break;
			case CmdRelampago:
				//
			break;
			case CmdDardosMisticos:
				//
			break;
			default:
				bool sovirar = (button & RIGHTBUTTONMASK) ? true : false;
				bool strafe = (button & ALTMASK) ? true : false;
				Movimenta(mx, my, mz, sovirar, strafe);
			break;
		}
	}
	Reescreve();
}

void cJogo::TrocaArma(short id)
{
	/*if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	if (J[vz].ArmaMao() == -1) {
		//o vz n�o tem armas
		return;
	}

	//TO DO: arrumar esta fun��o quando fro poss�vel escolher a arma para qual o cara quer trocar

	if (id == -1) {//TEMP:depois que o cara conseguir escolher a arma, esse if ficar� obsoleto
		//procura a pr�xima arma
		short i;
		for (i = (J[vz].ArmaMao + 1) % NUM_ARMAS; i != J[vz].ArmaMao; i = (i + 1) % NUM_ARMAS) {
			if (J[vz].PossuiArma[i]) {
				id = i;
				Reescreve();
				break;
			}
		}
	}

	if (id != -1) {
		//J[vz].ArmaMao = id;
	}
	Reescreve();*/
}

void cJogo::IniciaMira()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	double xo, yo, zo;
	CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xo, yo, zo, 2.5);
	pMapa->Cam.SetPos(xo, yo, zo, false);
	pMapa->Cam.SetLookTo(xo+1, yo, zo, false);
	pMapa->Cam.RotHorizontal(-30.0 * J[vz].Frente, true);
}

bool cJogo::Mira(int mx, int my, int mz)
{
	if (!NaoAnimando) return true; //est� mostrando anima��o => GUI bloqueada

	//Testa condi��es que proibam a a��o (mirar)
	if (J[vz].Posicao == posNaMoto) {
		MsgDef("S� em filmes algu�m consegue atirar andando de moto!");
		return true;
	}
	//short &armamao = J[vz].ArmaMao();
	cObjeto *armamao = J[vz].ArmaMao();
	if (armamao == 0) {
		MsgDef("N�o faz sentido mirar!\nVoc� n�o est� carregando nenhuma arma!");
		return true;
	}
	ushort codarma = armamao->GetCod();
	if (Armas[codarma].TempoMirar == 0) {
		//s� se aplica a armas corpo-a-corpo
		MsgDef("N�o � necess�rio mirar com esta arma!");
		return true;
	}

	//Testa se tem muni��o, pois n�o adianta mirar com uma arma descarregada
	if (!TemMunicao(armamao)) return true;

	//Testa se o cara precisa se virar para a dire��o certa
	short proxFrente = Direcao(J[vz].x, J[vz].y, mx, my);
	if (proxFrente == -1) {
		if (J[vz].z == mz) {
			return false; //o cara quer mirar nele mesmo
		} else {
			proxFrente = J[vz].Frente; //o cara est� mirando para cima/baixo
		}
	}
	if (proxFrente != J[vz].Frente && !PodNaoViraAoAtirar(vz) && !ArmaNaoViraAoAtirar(codarma)) {
		if (!Vira(proxFrente, 0)) {
			//se o cara n�o terminou de virar (viu alguem ou acabou o tempo), cancela a a��o
			return true;
		}
	}

	//Testa se o cara tem tempo de mirar
	short tempo = Armas[codarma].TempoMirar;
	if (J[vz].GetFlag(Mirando)) tempo /= 2; //se j� estava mirando, gasta metade
	if (tempo > J[vz].T) {
		if (J[vz].GetFlag(Mirando)) {
			MsgSemTempo("mudar de alvo esta arma", tempo);
		} else {
			MsgSemTempo("escolher um alvo com esta arma", tempo);
		}
		return true;
	}

	//TO DO: controlar o AlvoID para fazer o blaster funcionar
	J[vz].AlvoID = 0; //solu��o tempor�ria enquanto o blaster n�o � implementado

	//Finalmente, mira
	J[vz].AlvoX[J[vz].AlvoID] = mx;
	J[vz].AlvoY[J[vz].AlvoID] = my;
	J[vz].AlvoZ[J[vz].AlvoID] = mz;

	if (J[vz].GetFlag(Mirando)) { //se j� estava mirando ent�o troca de alvo, mantendo parte da mira anterior
		short difang = DifAngi(J[vz].Frente, proxFrente);
		//BUG: Armas[codarma].CoiceMove pode ser 0! Isso s� pode dar merda
		//double coice = pow(Armas[codarma].CoiceMove, difang/2);
		double coice = Armas[codarma].CoiceMove;
		if (coice == 0)	coice = pow(0.9, (double)difang);
		else			coice = pow(coice, difang * 0.5);
		J[vz].MiraAtual = Armas[codarma].MultMira(J[vz].MiraAtual, coice);
	} else {
		J[vz].MiraAtual = Armas[codarma].MirIni;
	}

	J[vz].MiraBonus = J[vz].Mira;
	//o Sabata tem precis�o total (nem tanto) com o rev�lver e com o rifle
	if (PodMiraExtraPistolaERifle(vz) &&
		(codarma == aPistola || codarma == aRifle)) {
		J[vz].MiraBonus *= 2.5;
	}

	J[vz].SetFlag(Mirando, true);
	GastaTempo(tempo);
	TestaEncontro(false);
	
	Reescreve();
	return true;
}

void cJogo::MiraMais()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//Testa se o cara est� usando uma arma
	/*
	short &arma = J[vz].ArmaMao();
	if (arma == -1) return; //nem avisa
	/*/
	cObjeto *arma = J[vz].ArmaMao();
	if (arma == 0) return; //nem avisa
	ushort codarma = arma->GetCod();
	/**/

	//Testa se o vz mirou antes
	if (!J[vz].GetFlag(Mirando)) {
		MsgDef("Voc� precisa escolher um alvo antes de mirar!");
		return;
	}

	//Testa se a mira j� est� no m�ximo
	if (J[vz].MiraAtual == Armas[codarma].MirMax) {
		MsgDef("N�o adianta mirar mais que isto com esta com esta arma!");
		return;
	}

	//Testa se o cara tem tempo de mirar
	short tempo = TempoMirarMais;
	if (tempo > J[vz].T) {
		MsgSemTempo("mirar", tempo);
		return;
	}

	//Finalmente, mira mais
	J[vz].MiraAtual = Armas[codarma].MultMira(J[vz].MiraAtual, Armas[codarma].MirMais);
	GastaTempo(tempo);
	TestaEncontro(false);
	
	Reescreve();
}

void cJogo::Atira()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//Testa se o cara est� usando uma arma
	/*
	short &arma = J[vz].ArmaMao();
	if (arma == -1) return; //nem avisa
	/*/
	cObjeto *arma = J[vz].ArmaMao();
	if (arma == 0) return; //nem avisa
	ushort codarma = arma->GetCod();
	ushort codmun = arma->codmunicao;
	/**/

	//Testa se o vz mirou antes
	if (!J[vz].GetFlag(Mirando)) {
		MsgDef("Voc� precisa escolher um alvo antes de atirar!");
		return;
	}

	//Testa se o cara tem tempo de atirar
	short tempo = Armas[codarma].TempoAtirar;
	if (tempo > J[vz].T) {
		MsgSemTempo("atirar com esta arma", tempo);
		return;
	}

	//Testa se tem muni��o
	if (!TemMunicao(arma)) return;

	//Calcula par�metros para o tiro
	double xho, yho, zho, xha, yha, zha;

	CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xho, yho, zho, 0.5);
	CentroHex3D(J[vz].AlvoX[0], J[vz].AlvoY[0], J[vz].AlvoZ[0], xha, yha, zha, 0.5);

	double d = Distancia3D(xho, yho, zho, xha, yha, zha);

	double teta = funAngTetaf(xho, yho, xha, yha);
	double phi = funAngPhif(xho, yho, zho, xha, yha, zha);

	//BUG Possivel: teoricamente, o esquema de mirar garante que o vz estar� olhando na dire��o do alvo
	//double direcao = Direcao(xho, yho, xha, yha, teta);

	double k = 0.67427 / atan(0.5 / (J[vz].MiraAtual * J[vz].MiraBonus));
	double dteta = superrnd(0.8 / k);
	double dphi = ERRO_PHI * superrnd(0.8 / k);
	teta += dteta;
	phi += dphi;

	//FIRE!
	Fogo(teta, phi, xho, yho, zho, codarma, codmun, d);

	//Desliga camuflagem
	if (PodCamuflagemControlavel(vz) && J[vz].Camufl > J[vz].CamuflNat) {
		J[vz].Camufl = J[vz].CamuflNat;
	}

	//Aplica coice da arma
	if (Armas[codarma].CoiceTiro == 0) {
		J[vz].SetFlag(Mirando, false);
	} else {
		J[vz].MiraAtual = Armas[codarma].MultMira(J[vz].MiraAtual, Armas[codarma].CoiceTiro);
	}
	GastaTempo(tempo);
	//Gasta muni��o
	/*
	if (arma == aBolaDeFogo) { //bola de fogo gasta mana
		J[vz].ManaFe -= ManaBolaDeFogo;
	} else {
		J[vz].Mun[arma] -= 1;
	}
	/*/
	if (arma->UsaManaFe()) {
		J[vz].ManaFe -= Municoes[codmun].MunPac;
	} else if (Armas[codarma].SohUmTiroPorRodada()){
		arma->municao = -arma->municao;
	} else {
		arma->municao --;
		if (arma->municao == 0) { //acabou a muni��o
			if (Municoes[codmun].MunRec == -1) {
				//arma descart�vel (deve sumir quando a muni��o acaba) ex.:Disco e Shuriken
				J[vz].ApagaArma();
				arma = NULL; //para evitar BUG, este ponteiro n�o deve ser utilizado no resto desta fun��o!
			} else {
				arma->codmunicao = mSemMunicao;
			}
		}
	}
	/**/

	J[vz].Replay.GravaSom(Armas[codarma].SomPad);
	int ulttiro = contlinhatiro-1;
	J[vz].Replay.GravaFoto(AcaoAtira,
						   num(linhatiro[ulttiro].x2),
						   num(linhatiro[ulttiro].y2),
						   num(linhatiro[ulttiro].z2));

	Reescreve();

	TestaEncontro(true, Municoes[codmun].DesCam);

	TestaMortes(vz);
}

bool cJogo::TemMunicao(cObjeto* arma)
{
	/*
	if (arma == aBolaDeFogo) { //bola de fogo gasta mana
		if (J[vz].ManaFe < ManaBolaDeFogo) {
			MsgDef("Voc� n�o tem mana suficiente para mandar um bola de fogo!");
			return;
		}
	} else if (J[vz].Mun[arma] == 0) {
		if (arma == aBaforada) { //baforada
			MsgDef("Voc� s� pode dar uma baforada por rodada!");
		} else if (J[vz].MunExtra[arma] == 0) {
			MsgDef("Sua arma est� sem muni��o, jogue-a fora!");
		} else {
			MsgDef("Sua arma est� descarregada, recarregue-a!");
		}
		return;
	}
	/*/
	if (arma->UsaManaFe()) {
		if (J[vz].ManaFe < Municoes[arma->codmunicao].MunPac) {
			MsgDef("Voc� n�o tem mana suficiente para mandar um bola de fogo!");
			return false;
		}
	} else if (arma->municao == 0) {
		//if (arma->GetCod() == aBaforada) { //baforada
		//	MsgDef("Voc� s� pode dar uma baforada por rodada!");
		//} else {
			MsgDef("Sua arma est� descarregada, recarregue-a!");
		//}
		return false;
	} else if (arma->municao < 0) {
		MsgDef("Voc� s� pode dar um tiro por rodada com esta arma!");
		return false;
	}
	return true;
	/**/
}

void cJogo::Fogo(double teta, double phi,			//�ngulos
				 double xo, double yo, double zo,	//posi��o inicial
				 short codarma, short codmun,		//id da arma e da bala
				 double distalvo)					//dist�ncia at� o alvo: usado para que o tiro do blaster fa�a curvas
{
	static cProjetil Proj[MAX_PROJETEIS];
	short nproj = Municoes[codmun].Projeteis;
	double espalha = 0.01 * (nproj - 1);//espalhamento = 0.01 * (# de proj�teis - 1)
	double alcance = Municoes[codmun].Alcancef();
	int passo = 0;
	static int passos_entre_frames = 300;

	//inicia proj�teis e linhas (e sombras) dos tiros
	contlinhatiro = 0;
	contsombratiro = 0;
	for (short i=0; i < nproj; i++) {
		Proj[i].Inicia(teta, phi, xo, yo, zo, espalha, Municoes[codmun].Dano, contlinhatiro);
		linhatiro[contlinhatiro].Inicia(0.025, xo, yo, zo);
		contlinhatiro++;

		sombratiro[contsombratiro].Inicia(0.025, xo, yo, zo, &Mapa);
		contsombratiro++;
	}

	const double step = 0.002;
	for (double w=0; w<alcance; w+= step) {
		bool todospararam = true;
		//simula cada proj�til
		for (short i=0; i < nproj; i++) {
			cProjetil &P = Proj[i];
			if (!P.parou) {
				cCont cont;
				cChao contchao;
				cAfin afin;
				bool deveexplodir = false;
				bool devericochetear = false;
				//bool testar_parede = false;

				todospararam = false;

				P.Avanca(w);
				linhatiro[P.idlinhatiro].x2 = P.xf;
				linhatiro[P.idlinhatiro].y2 = P.yf;
				linhatiro[P.idlinhatiro].z2 = P.zf;

				sombratiro[P.idlinhatiro].Move(P.xi, P.yi, P.xf, P.yf, P.zf);

                passo++;
                if (passo % passos_entre_frames == 0) {
					//drawScene(0);
					pwinMain->drawScene();
				}

				short mudouhex = P.MudouHex();

				if (mudouhex) {
					cont	 = Mapa.Cont(P.xi, P.yi, P.zi, cMapa::NAO_CHAO);
					contchao = Mapa.Cont(P.xi, P.yi, P.zi, cMapa::CHAO);
					afin	 = Mapa.Afin(P.xi, P.yi, P.zi);
					//P.parede_fina_perto = false;
					P.SetAfinamento(&afin);

					//Arma > Baforada > p�e fogo por onde passa
					if (codarma == aBaforada) {
						if (!P.ehchaoant && !contchao.EhChaoBuraco() && (!cont.EhParede() || cont.EhFumaca())) {
							cont.SetFogo();
							Mapa.SetCont(P.xiant, P.yiant, P.ziant, cont);
						}
					}

					if (P.ForaDoMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ)) {
						//Arma > Disco > cai no chao antes de sair do mapa
						if (codarma == aDisco) {
							Mapa.AdObjSimpPos(OBJ_Disco, P.xiant, P.yiant, P.ziant);
						}

						P.parou = true;
						break; //exit for
					}

					P.jogperto = -1;
					//Testa se o tiro chegou perto de algu�m
					for (short j=0; j<numjogs; j++)
					if (!J[j].GetFlag(Morto)) {
						//TO DO: testar se o chegou perto da holografia do [j]
						if (P.xi == J[j].x
						&&  P.yi == J[j].y
						&&  P.zi == J[j].z
						&&  P.ehchao == (J[j].Posicao == posEnterrado)) {
							P.jogperto = j;
							
							if (j == vz) MsgDef("BUG?: um tiro atingiu o J[vz]");
							
							break; //BUG: Sup�e-se que dois jogadores nunca ocupam o mesmo lugar
						}
					}

					//Arma > Teletransportadora > move parede (if EhParede && !EhEsfumacado)
					//Arma > Teletransportadora > move chao (if EhChaoElevador)

					//TO DO: testar se acertou um Teletransporte

					//Testa se acertou algum obst�culo
					//TO DO: aplicar formato diferente das paredes
					//TO DO: fazer as balas atravessarem paredes, perdendo for�a no caminho
					/*if (P.ehchao) {
						if (contchao.EhChaoPedra()) {
							P.dano -= 1000;
							devericochetear = true;
						} else if (contchao.EhChaoDuro()) {
							P.dano -= 45;
							devericochetear = true;
						} else if (contchao.EhChaoGrade()) {
							if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
							&& (rnd() > 0.1 || Armas[arma].RaioExplosao > 2)) {
								P.dano -= 50;
								devericochetear = true;
							}
						} else if (!contchao.EhChaoBuraco()) {
							P.dano -= 15;
						}
					} else if (!afin.EhAfinInteira() && !P.AcertouParede(afin)) {
						//� uma parede fina e o tiro ainda n�o acertou
						//o tiro pode acertar a parede antes de mudar de hex�gono
						P.parede_fina_perto = true;
					} else {
						testar_parede = true;
					}*/
				}/* else if (P.parede_fina_perto) {				
					//est� pr�ximo a uma parede fina, mas n�o acertou ela ainda => testar se acertou agora
					if (P.AcertouParede(afin)) {
						P.parede_fina_perto = false;
						testar_parede = true;
					}
				}*/
				
				if (P.AcertouParede()) {
					if (P.ehchao) {
						if (contchao.EhChaoPedra()) {
							P.dano -= DurezaPedra;
							devericochetear = true;
						} else if (contchao.EhChaoDuro()) {
							P.dano -= DurezaMedia;
							devericochetear = true;
						} else if (contchao.EhChaoGrade()) {
							if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
							&& (rnd() > 0.1 || Municoes[codmun].RaioExplosao > 2)) {
								P.dano -= DurezaGrade;
								devericochetear = true;
							}
						} else if (!contchao.EhChaoBuraco()) {
							P.dano -= DurezaFraca;
						}
					} else {
						if (cont.EhParedePedra()) {
							P.dano -= DurezaPedra;
							devericochetear = true;
						} else if (cont.EhParedeDura()) {
							P.dano -= DurezaMedia;
							devericochetear = true;
						} else if (cont.EhLata()) {
							//TO DO: explode
						} else if (cont.EhGrade()) {
							if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
							&& (rnd() > 0.1 || Municoes[codmun].RaioExplosao > 2)) {
								P.dano -= DurezaGrade;
								devericochetear = true;
							}
						} else if (cont.EhParedeSolida()) {
							P.dano -= DurezaFraca;
						}
					}
				}
				/*//mudou de hex ou acertou uma parede fina
				if (testar_parede) {
					if (cont.EhParedePedra()) {
						P.dano -= 1000;
						devericochetear = true;
					} else if (cont.EhParedeDura()) {
						P.dano -= 45;
						devericochetear = true;
					} else if (cont.EhLata()) {
						//TO DO: explode
					} else if (cont.EhGrade()) {
						if (!(PodNaoAtingeGradesDist10(vz) && distalvo < 10)
						&& (rnd() > 0.1 || Armas[arma].RaioExplosao > 2)) {
							P.dano -= 50;
							devericochetear = true;
						}
					} else if (cont.EhParedeSolida()) {
						P.dano -= 15;
					}
				}*/

				//Testa se o tiro atinge J[P.jogperto]
				if (P.jogperto != -1) {
					//dist�ncia do proj�til ao centro do hex.
					double distcentro = Distancia2D(P.xf, P.yf, P.xch, P.ych);
					double raiodocara = J[P.jogperto].Raio * LARGURAHEX;

					if (raiodocara > distcentro) {
						//SubPoderes > Defender tiro com sabre de luz
						if (Municoes[codmun].RaioExplosao == 0
						&&  PodDefendeComEspada(P.jogperto)
						&&  J[P.jogperto].ArmaMaoCod() == aSabreDeLuz
						&&  J[P.jogperto].TProxRodada >= TempoDefesaComEspada
						&&  EstaSendoVistoPor(P.jogperto)) {

							MsgDef("Ele defendeu o tiro com a espada!!");
							J[P.jogperto].TProxRodada -= TempoDefesaComEspada;
							
							//
							//devericochetear = true; //faz o tiro ricochetear se for uma arma ricochet.
							P.dano = 0; //tem o mesmo significado de bater em uma parede indestrut�vel

						} else if (PodSuperEsquiva(P.jogperto)
							   &&  J[P.jogperto].TProxRodada >= TempoEsquivaMatrix
							   &&  EstaSendoVistoPor(P.jogperto)) {

							MsgDef("Ele se esquivou do tiro!!");
							J[P.jogperto].TProxRodada -= TempoEsquivaMatrix;
						} else if (Municoes[codmun].RaioExplosao == 0) {
							double modblindagem = 1;

							if (ArmaEhIncendiaria(codarma)) {
								if (PodImuneAFogo(P.jogperto)) {
									P.dano = 0;
								} else if (PodResistenteAFogo(P.jogperto)) {
									modblindagem = 2;
								} else {
									modblindagem = 0.5; //blindagem normal � pior contra fogo
								}
							//Arma > Tri-Laser > Perfura blindagem
							} else if (codarma == aTriLaser) {
								modblindagem = 0.5;
							//SubPoderes > Mira extra > blindagem menor (a cara � t�o bom que atinge um ponto fraco na blindagem dos advers�rios)
							} else if (PodMiraExtraPistolaERifle(vz)
								   && (codarma == aPistola || codarma == aRifle)) {
								modblindagem = 0.4;
							//Arma > Teletransportadora > ignora blindagem
							} else if (codarma == aTeletrans) {
								modblindagem = 0;
							}

							//Arma > Paralisante > ao inv�s de dor ela deixa a pessoa lenta
							if (codarma == aParalisante) {
								J[P.jogperto].TMax = Max(25, J[P.jogperto].TMax - 15);
							}

							double blindagem = modblindagem * J[P.jogperto].Blindagem;
							double dor = CausaDor(P.jogperto, P.dano, blindagem);
							if (dor > 0) {
								//enfraquece o dano do proj�til,
								//caso ele n�o seja zerado o proj�til deve continuar andando
								P.dano -= blindagem + DurezaJog;
							
								//Arma > Degenera��o
								J[P.jogperto].Reg -= Municoes[codmun].Degen;

								//Arma > Teletransportadora > teletransp. pessoa
							}
						}
						P.jogperto = -1; //evita que o cara seja atingido duas vezes
					}
				}

				//Testes de impacto (atravessa, para ou ricocheteia)
				//Arma > Ricocheteadora > tiro ricocheteia
				/*if (arma == aRicocheteadora && devericochetear) { // # de ricocheteadas < limite (era 20)
					//TO DO: ricochetear o tiro
				} else*/ if (P.dano < P.danoant) {
					//TO DO: (OR NOT TO DO) armas incendi�rias devem atravessar paredes?

					if (P.dano > 0) {
						//Arma > Disco > quebra a parede/ch�o
						if (codarma == aDisco) {
							if (P.ehchao) {
								contchao.SetChaoBuraco();
								Mapa.SetChao(P.xi, P.yi, P.zi, contchao);
							} else {
								cont.SetFumaca();
								Mapa.SetCont(P.xi, P.yi, P.zi, cont);
							}
						}

						//TEMP: as paredes de madeira n�o devem ser destru�das � bala
						if (cont.EhParedeMadeira() && !P.ehchao) {
							cont.SetFumaca();
							Mapa.SetCont(P.xi, P.yi, P.zi, cont);
						}

						//Arma > Baforada > p�e fogo nas paredes por onde passa
						if (codarma == aBaforada) {
							if (!P.ehchao) {
								cont.SetFogo();
								Mapa.SetCont(P.xi, P.yi, P.zi, cont);
							}
						}
					} else {
						P.parou = true;
						deveexplodir = true;

						//Arma > Disco > cai no ch�o
						if (codarma == aDisco) Mapa.AdObjSimpPos(OBJ_Disco, P.xiant, P.yiant, P.ziant);
					}
				}

				if (deveexplodir) {
					//Arma > EhExplosiva > explode
				}

				//Arma > Blaster > tiro muda de dire��o para o pr�ximo alvo (if w > distalvo && tiver prox. alvo)

				P.Valida();
			}
		}
		if (todospararam) break; //exit for
	}
}

bool cJogo::Pega(int mx, int my, int mz)//, cObjetoSimples *objsimp, cObjeto *obj)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	//TO DO: esta fun��o deveria pegar um objeto espec�fico, mas antes
	//TO DO: � preciso construir uma GUI para que o jogador possa escolher qual objeto pegar

	if (mx == -1) mx = J[vz].x;
	if (my == -1) my = J[vz].y;
	if (mz == -1) mz = J[vz].z;
	
	cObjeto *obj = 0;

	cListaPosObjetos* listaobjs = Mapa.GetListaPos(mx, my, mz);
	if (listaobjs != NULL) {
		//Aten��o: in�cio de gambiarra
		//TEMP: este c�digo s� servir� enquanto o usu�rio n�o puder escolher o objeto a pegar
		//*
		cObjetoSimples objsimpaux;
		if (listaobjs->numobjsimp > 0) {
			objsimpaux = listaobjs->GetObjSimp(0);
			obj = new cObjeto(objsimpaux.cod);
			if (!Pega(obj, listaobjs, true)) {
				delete obj; //se n�o pegou ent�o destr�i o objeto
				return false;
			} else {
				return true;
			}
		} else if (listaobjs->numobj > 0) {
			obj = listaobjs->GetObj();
			return Pega(obj, listaobjs, true);
		} else {
			MsgDef("N�o h� nada utiliz�vel no ch�o!");
			return false;
		}
		/*/
		cObjetoSimples objsimpaux;
		if (obj == NULL && objsimp == NULL) {
			if (listaobjs->numobjsimp > 0) {
				objsimpaux = listaobjs->GetObjSimp(0);
				objsimp = &objsimpaux;
			} else if (listaobjs->numobj > 0) {
				obj = listaobjs->GetObj();
			} else {
				MsgDef("N�o h� nada utiliz�vel no ch�o!");
				return;
			}
		}
		*/
		//fim de gambiarra
	} else {
		return false;
	}
}

bool cJogo::Pega(cObjeto *obj, cListaPosObjetos *listaobjs, bool blnMaoDir)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoPegar;

	if (tempo > J[vz].T) {
		MsgSemTempo("pegar", tempo);
		return false;
	}
	if (PodNaoPodePegarNada(vz)) {
		MsgDef("Voc� n�o tem capacidade de pegar coisas!");
		return false;
	}
	if (J[vz].Posicao == posNaMoto) {
		MsgDef("Voc� tem que descer da moto para pegar coisas!");
		return false;
	}

	//unsigned char &c = obj->cod;
	//ushort &c = obj->cod;

	if (PodSoPegaItensSimples(vz)) {
		//espingarda, rifle, trabuco, muni��o para o rev�lver, medi-kit e chave
		//if (c != 3 && c != 11 && c != 23 && c != 15 && c != 49 && c != 57) {
		/*if (c != OBJ_Espingarda
		&&  c != OBJ_Rifle
		&&  c != OBJ_Trabuco
		&&  c != OBJ_Revolver
		&&  c != OBJ_KitMedico
		&&  c != OBJ_Chave) {*/
		if (!obj->EhObjetoTrivial()) {
			MsgDef("Voc� n�o pode pegar isso!");
			return false;
		}
	}

	bool pegou = false;
	bool destruiu = false;
	char *msg = NULL;
	if (PodDestroiObjetos(vz)) {
		if (obj->Eh(aDisco, cObjeto::tipArma) && PodNaoDestroiDisco(vz)) {
			msg = "Voc� achou um disco de predador!!!";
			//J[vz].PegaArma(Armas[6]);
			pegou = true;
		} else if (obj->Eh(eEsqueleto, cObjeto::tipEquip) && PodComeCorpos(vz)) {
			msg = "Voc� achou um corpo com muitas prote�nas!!";
			//J[vz].Proteinas ++;
			//J[vz].Equip.AdObjSimp((unsigned char)OBJ_Esqueleto);
			//J[vz].Equip.AdObj(*obj);
			pegou = true;
		} else if (PodDestroiObjetosDuros(vz)) {
			msg = "Voc� destruiu um objeto in�til!";
			destruiu = true;
			pegou = true;
		} else if (obj->Eh(eColete, cObjeto::tipEquip)) { //se n�o for um objeto duro
			   //&&  obj->Eh(eMoto, cObjeto::tipEquip)) {
			msg = "Voc� destruiu um objeto in�til!";
			destruiu = true;
			pegou = true;
		} else {
			msg = "Voc� n�o pode destruir isto!";
		}
	} else {
		if (obj->Eh(eEsqueleto, cObjeto::tipEquip)) {
			if (PodComeCorpos(vz)) {
				pegou = true;
			} else {
				msg = "Voc� n�o pode pegar corpos!";
			}
		} else {
			pegou = true;
		}

		/*
		int m;
		switch (c) {
			case OBJ_Disco:
				msg = "Voc� achou um disco de predador!!!";
				J[vz].PegaArma(Armas[6]);
				pegou = true;
				break;
			case OBJ_Esqueleto:
				if (PodComeCorpos(vz)) {
					msg = "Voc� achou um corpo com muitas prote�nas!!";
					//J[vz].Proteinas ++;
					//J[vz].Equip.AdObjSimp(OBJ_Esqueleto);
					pegou = true;
				}
				break;
			//*************
			//*** ARMAS ***
			case OBJ_Espingarda:
				msg = "Voc� achou uma espingarda!";
				J[vz].PegaArma(Armas[1]);
				pegou = true;
				break;
			case OBJ_Metralhadora:
				msg = "Voc� achou uma metralhadora!";
				J[vz].PegaArma(Armas[2]);
				pegou = true;
				break;
			case OBJ_LancaFoguetes:
				msg = "Voc� achou um lan�a-foguetes!";
				J[vz].PegaArma(Armas[3]);
				pegou = true;
				break;
			case OBJ_Rifle:
				msg = "Voc� achou um rifle com mira telesc�pica!";
				J[vz].PegaArma(Armas[4]);
				pegou = true;
				break;
			case OBJ_Degeneradora:
				msg = "Voc� achou uma arma degeneradora!";
				J[vz].PegaArma(Armas[5]);
				pegou = true;
				break;
			case OBJ_MiniGun:
				msg = "Voc� achou uma minigun!";
				J[vz].PegaArma(Armas[8]);
				pegou = true;
				break;
			case OBJ_LancaChamas:
				msg = "Voc� achou um lan�a-chamas!";
				J[vz].PegaArma(Armas[9]);
				pegou = true;
				break;
			case OBJ_Revolver:
				msg = "Voc� achou uma pistola!";
				J[vz].PegaArma(Armas[0]);
				pegou = true;
				break;
			case OBJ_Ricocheteadora:
				msg = "Voc� achou uma ricocheteadora!";
				J[vz].PegaArma(Armas[10]);
				pegou = true;
				break;
			case OBJ_Paralisante:
				msg = "Voc� achou uma arma paralisante!";
				J[vz].PegaArma(Armas[12]);
				pegou = true;
				break;
			case OBJ_Teletransportadora:
				msg = "Voc� achou uma arma teletransportadora!";
				J[vz].PegaArma(Armas[14]);
				pegou = true;
				break;
			case OBJ_Blaster:
				msg = "Voc� achou um lan�ador de m�sseis tele-guiados!";
				J[vz].PegaArma(Armas[15]);
				pegou = true;
				break;
			case OBJ_MG42:
				msg = "Voc� achou uma metralhadora militar!";
				J[vz].PegaArma(Armas[18]);
				pegou = true;
				break;
			case OBJ_Trabuco:
				msg = "Voc� achou um trabuco!";
				J[vz].PegaArma(Armas[26]);
				pegou = true;
				break;
			//********************
			//*** EQUIPAMENTOS ***
			case OBJ_Detector:
				msg = "Voc� achou uma detector de dist�ncia!";
				J[vz].SetFlag(TemDetec, true);
				pegou = true;
				break;
			case OBJ_Minas:
				m = rndi(2, 4);
				if (m == 2)		msg = "Voc� achou um par de minas!";
				else if (m == 3)msg = "Voc� achou tr�s minas!";
				else if (m == 4)msg = "Voc� achou quatro minas!";
				//J[vz].Minas += m;
				for (;m>=2;m--) listaobjs->AdObjSimp(OBJ_Minas);
				pegou = true;
				break;
			case OBJ_Granada:
				m = rndi(1, 3);
				if (m == 1)		msg = "Voc� achou uma granada!";
				else if (m == 2)msg = "Voc� achou um par de granadas!";
				else if (m == 3)msg = "Voc� achou tr�s granadas!";
				//J[vz].Granadas += m;
				for (;m>=2;m--) listaobjs->AdObjSimp(OBJ_Granada);
				pegou = true;
				break;
			case OBJ_KitMedico:
				if (PodDestroiKitMedico(vz)) {
					msg = "Voc� destruiu um kit de pronto-socorro!";
					destruiu = true;
				} else {
					msg = "Voc� achou um kit de pronto-socorro!";
					//J[vz].MedKits ++;
					//J[vz].Equip.AdObjSimp(OBJ_KitMedico);
				}
				pegou = true;
				break;
			case OBJ_CapaceteInv:
				if (PodDestroiCapacete(vz)) {
					msg = "Voc� destruiu este capacete!";
					destruiu = true;
				} else {//if (J[vz].Invis == 0) {
					msg = "Voc� achou um capacete de invisibilidade!";
					//J[vz].Invis = -3;
					//J[vz].Equip.AdObjSimp(OBJ_CapaceteInv);
				}// else {
				//	msg = "Voc� trocou seu capacete de invisibilidade.";
				//	J[vz].Invis = 3 * (J[vz].Invis / J[vz].Invis); //Mat�m o sinal, i.e., n�o liga nem desliga
				//	//TO DO: soltar um capacete gasto
				//}
				pegou = true;
				break;
			case OBJ_OculosIR:
				if (PodDestroiOculosIR(vz)) {
					msg = "Voc� destruiu uns �culos de vis�o infravermelha!";
					destruiu = true;
				} else if (!J[vz].GetFlag(TemIR)) {
					msg = "Voc� achou �culos de vis�o infravermelha!";
					J[vz].SetFlag(TemIR, true);
				} else {
					//OBSOLETO
					msg = "Voc� j� tinha vis�o infravermelha, portanto destruiu estes �culos!";
					destruiu = true;
				}
				pegou = true;
				break;
			case OBJ_Bomba:
				msg = "Voc� achou uma bomba!";
				//J[vz].Bombas ++;
				//J[vz].Equip.AdObjSimp(OBJ_Bomba);
				pegou = true;
				break;
			case OBJ_Colete:
				if (PodNaoUsaColete(vz)) {
					msg = "Ninjas n�o podem usar coletes!";
				} else if (J[vz].Blindagem >= 1) {
					msg = "Voc� j� tem uma prote��o muito boa!";
				} else {
					msg = "Voc� achou um colete a prova de balas!";
					J[vz].Blindagem += 1.0;
					pegou = true;
				}
				break;
			case OBJ_Holografia:
				if (PodDestroiHolografia(vz)) {
					msg = "Voc� destruiu este aparelho holografador!";
					destruiu = true;
				} else {
					msg = "Voc� achou um aparelho holografador!";
					//J[vz].Holografias ++;
					//J[vz].Equip.AdObjSimp(OBJ_Holografia);
				}
				pegou = true;
				break;
			case OBJ_Chave:
				if (J[vz].GetFlag(TemChave)) {
					msg = "Voc� j� tinha uma chave, portanto destruiu esta!";
					destruiu = true;
				} else {
					msg = "Voc� achou uma chave mestra!";
					J[vz].SetFlag(TemChave, true);
				}
				pegou = true;
				break;
			case OBJ_Moto:
				//OBSOLETO ?
				//msg = "Agora, voc� tem uma moto!";
				//J[vz].Posicao = posNaMoto;
				//pegou = true;
				break;
		}//*/
	}
	if (pegou) {
		if (!destruiu) {
			if (blnMaoDir) {
				J[vz].objMaoDir = new cObjeto(*obj);
			} else {
				J[vz].objMaoEsq = new cObjeto(*obj);
			}
		}
		
		ushort mx = listaobjs->x;
		ushort my = listaobjs->y;
		ushort mz = listaobjs->z;

		//listaobjs->ApagaObjSimp(*objsimp);		//apaga o objeto
		//apaga o objeto
		listaobjs->ApagaObj(obj);
		//atualiza o mapa E for�a a atualiza��o do fog
		J[vz].FogBit->SetCont(mx, my, mz, Mapa.AtualizaResumoObjsPos(mx, my, mz));
		AtualizaFog();

		J[vz].SetFlag(Mirando, false);

		if (msg) MsgDef(msg);

		J[vz].Replay.GravaFoto(AcaoPega);
		GastaTempo(tempo);
		TestaEncontro(false);
	} else {
		if (msg) MsgDef(msg);
	}
	return pegou;
}

bool cJogo::Solta(bool blnMaoDir)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoSoltar;

	if (tempo > J[vz].T) {
		MsgSemTempo("soltar", tempo);
		return false;
	}
	
	cObjeto **pobj = blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	if (*pobj == 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de soltar item da m�o que est� vazia!");
		return false;
	} else if ((**pobj).EhSkill()) {
		MsgDef("Voc� n�o pode soltar skills!");
		return false;
	} else {
		ushort mx = J[vz].x;
		ushort my = J[vz].y;
		ushort mz = J[vz].z;
	
		//insere objeto no ch�o
		Mapa.AdObjPos(**pobj, mx, my, mz);
		//tira o objeto da m�o
		delete *pobj;
		*pobj = NULL;
		//atualiza o mapa E for�a a atualiza��o do fog
		J[vz].FogBit->SetCont(mx, my, mz, Mapa.AtualizaResumoObjsPos(mx, my, mz));
		AtualizaFog();
		
		//MsgDef("Item solto!");
		
		J[vz].Replay.GravaFoto(AcaoSolta);
		GastaTempo(tempo);
		TestaEncontro(false);

		return true;
	}
}

bool cJogo::Guarda(bool blnMaoDir)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoGuardar;

	if (tempo > J[vz].T) {
		MsgSemTempo("guardar", tempo);
		return false;
	}
	
	cObjeto **pobj = blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	if (*pobj == 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de guardar item da m�o que est� vazia!");
		return false;
	} else {
		ushort mx = J[vz].x;
		ushort my = J[vz].y;
		ushort mz = J[vz].z;
	
		if ((**pobj).EhSkill()) {
			//insere objeto nos skills
			J[vz].Skills.AdObj(**pobj);
		} else {
			//insere objeto nos equipamentos
			J[vz].Equip.AdObj(**pobj);
		}
		//tira o objeto da m�o
		delete *pobj;
		*pobj = NULL;
		
		J[vz].Replay.GravaFoto(AcaoEquip);
		GastaTempo(tempo);
		TestaEncontro(false);

		return true;
	}
}

bool cJogo::Saca(bool blnMaoDir, cObjeto *item)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	static const tempo = TempoSacar;

	if (tempo > J[vz].T) {
		MsgSemTempo("sacar", tempo);
		return false;
	}
	
	cObjeto **pobj = blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	if (*pobj != 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de sacar item na m�o que n�o est� vazia!");
		return false;
	} else {
		ushort mx = J[vz].x;
		ushort my = J[vz].y;
		ushort mz = J[vz].z;
		
		//p�e o objeto na m�o
		*pobj = new cObjeto(*item);
		if ((**pobj).EhSkill()) {
			//tira o objeto dos skills
			J[vz].Skills.ApagaObj(item);
		} else {
			//tira o objeto dos equipamentos
			J[vz].Equip.ApagaObj(item);
		}
		
		J[vz].Replay.GravaFoto(AcaoEquip);
		GastaTempo(tempo);
		TestaEncontro(false);

		return true;
	}
}

bool cJogo::Recarrega()
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	//S� � poss�vel recarregar se houver uma arma em uma m�o e uma muni��o compat�vel na outra
	//Se a arma estiver carregada, ent�o a muni��o antiga trocar� de lugar com a nova (i.e. o nova vai pra arma e a velha vai pra m�o)

	//Testa se o cara est� usando uma arma
	cObjeto *arma = J[vz].ArmaMao();
	if (arma == 0) return false; //nem avisa
	ushort codarma = arma->GetCod();
	
	//Testa se tem uma muni��o na outra m�o
	cObjeto *munnova = NULL;
	if (arma == J[vz].objMaoDir) munnova = J[vz].objMaoEsq;
	else						 munnova = J[vz].objMaoDir;
	if (!munnova->EhMunicao()) return false; //nem avisa
	ushort codmunnova = munnova->GetCod();
	
	//Testa se a muni��o � compat�vel
	if (Municoes[codmunnova].ArmaDest != codarma) return false;
	
	//Testa se o cara tem tempo de recarregar
	short tempo = Armas[codarma].TempoRecarr;
	if (tempo > J[vz].T) {
		MsgSemTempo("recarregar esta arma", tempo);
		return false;
	}
	
	//Finalmente, recarrega
	if (!ArmaRecarregaComMira(codarma)) J[vz].SetFlag(Mirando, false);

	//Tira a muni��o antiga
	cObjeto *munvelha = NULL;
	if (arma->codmunicao != mSemMunicao) {
		munvelha = new cObjeto(arma->codmunicao,cObjeto::tipMunicao);
		munvelha->municao = arma->municao;
	}

	//P�e a muni��o nova
	arma->codmunicao = codmunnova;
	arma->municao = munnova->municao;
	delete munnova;
	munnova = NULL;
	
	//P�e a muni��o velha(se houver uma) na m�o
	if (arma == J[vz].objMaoDir) J[vz].objMaoEsq = munvelha;
	else						 J[vz].objMaoDir = munvelha;

	//Finaliza
	J[vz].Replay.GravaFoto(AcaoRecarrega);
	GastaTempo(tempo);
	TestaEncontro(false);
	
	return(true);
}

bool cJogo::Vestir(bool blnMaoDir, cObjeto::enmLugarUso lugar)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	int tempo = 0;
	switch (lugar) {
		case cObjeto::codCabeca:	tempo = TempoVestirCabeca; break;
		case cObjeto::codCorpo:		tempo = TempoVestirCorpo; break;
	}

	if (tempo > J[vz].T) {
		MsgSemTempo("vestir isto", tempo);
		return false;
	}
	
	cObjeto **pobjMao	= blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	cObjeto **pobjMao2	= blnMaoDir ? &J[vz].objMaoEsq :  &J[vz].objMaoDir;
	if (*pobjMao == 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de vestir item de uma m�o vazia!");
		return false;
	} else if ((lugar == cObjeto::codCorpo) && (*pobjMao2 == 0)) {
		MsgDef("Voc� precisar ter as duas m�os livres para poder vestir isto!");
		return false;
	} else {
		cObjeto **pobjLugar = NULL;
		switch (lugar) {
			case cObjeto::codCabeca:	pobjLugar = &J[vz].objCabeca; break;
			case cObjeto::codCorpo:		pobjLugar = &J[vz].objCorpo; break;
		}
		if (*pobjLugar != 0) {
			//erro que n�o pode acontecer, mas nunca se sabe
			MsgDef("Bug: Tentativa de vestir item em um lugar ocupado!");
			return false;
		} else if (!(**pobjMao).AplicaEfeitoVeste(&J[vz])) {
			MsgDef("Este item n�o foi feito para ser usado a�!");
			return false;
		} else {
			//p�e objeto no lugar
			*pobjLugar = *pobjMao;
			
			//tira o objeto da m�o (obs.: o objeto n�o � copiado, � transferido)
			*pobjMao = NULL;
			
			J[vz].Replay.GravaFoto(AcaoEquip);
			GastaTempo(tempo);
			TestaEncontro(false);

			return true;
		}
	}
}

bool cJogo::Desvestir(bool blnMaoDir, cObjeto::enmLugarUso lugar)
{
	if (!NaoAnimando) return false; //est� mostrando anima��o => GUI bloqueada

	int tempo = 0;
	switch (lugar) {
		case cObjeto::codCabeca:	tempo = TempoDesvestirCabeca; break;
		case cObjeto::codCorpo:		tempo = TempoDesvestirCorpo; break;
	}

	if (tempo > J[vz].T) {
		MsgSemTempo("desvestir isto", tempo);
		return false;
	}
	
	cObjeto **pobjMao	= blnMaoDir ? &J[vz].objMaoDir :  &J[vz].objMaoEsq;
	cObjeto **pobjMao2	= blnMaoDir ? &J[vz].objMaoEsq :  &J[vz].objMaoDir;
	if (*pobjMao != 0) {
		//erro que n�o pode acontecer, mas nunca se sabe
		MsgDef("Bug: Tentativa de desvestir item para uma m�o ocupada!");
		return false;
	} else if ((lugar == cObjeto::codCorpo) && (*pobjMao2 != 0)) {
		MsgDef("Voc� precisar ter as duas m�os livres para poder desvestir isto!");
		return false;
	} else {
		cObjeto **pobjLugar = NULL;
		switch (lugar) {
			case cObjeto::codCabeca:	pobjLugar = &J[vz].objCabeca; break;
			case cObjeto::codCorpo:		pobjLugar = &J[vz].objCorpo; break;
		}
		if (*pobjLugar == 0) {
			//erro que n�o pode acontecer, mas nunca se sabe
			MsgDef("Bug: Tentativa de desvestir item de um lugar vazio!");
			return false;
		} else {
			if (!(**pobjLugar).DesaplicaEfeitoVeste(&J[vz])) {
				//erro que n�o pode acontecer, mas nunca se sabe
				MsgDef("Bug: O cara conseguiu vestir um item sem efeito!");
				return false;
			}

			//p�e objeto na m�o
			*pobjMao = *pobjLugar;
			
			//tira o objeto do lugar (obs.: o objeto n�o � copiado, � transferido)
			*pobjLugar = NULL;
			
			J[vz].Replay.GravaFoto(AcaoEquip);
			GastaTempo(tempo);
			TestaEncontro(false);

			return true;
		}
	}
}

bool cJogo::UsaMao(bool blnMaoDir)
{
	//??????????????????????????????????????????
	return true;
}

void cJogo::Movimenta(int mx, int my, int mz, bool sovirar, bool strafe)
{
	if (J[vz].Posicao == posNaMoto && sovirar) {
		//n�o d� pra s� virar de moto
		return;
	} else if (J[vz].Posicao == posEnterrado) {
		MsgDef("Voc� tem que sair do buraco antes!");
		return;
	} else if (J[vz].GetFlag(Imovel)) {
		MsgDef("Voc� foi imobilizado!");
		return;
	} else if (J[vz].GetFlag(Mirando) && strafe) {
		if (Armas[J[vz].ArmaMaoCod()].CoiceMove == 0) {
			MsgDef("N�o � poss�vel andar sem perder a mira com esta arma!");
			return;
		}
	}

	int dx = mx - J[vz].x;
	int dy = my - J[vz].y;
	int dz = mz - J[vz].z;
	short proxFrente = J[vz].Frente;
	bool podeandar = false;

	if (sovirar) strafe = false; //n�o faz sentido virar no strafe

	if (sovirar && ((dx != 0) || (dy != 0))) {
		/*
		double angteta = funAngTetai(J[vz].x, J[vz].y, mx, my);
		proxFrente = floor(angteta / (PI/6));
		if (proxFrente < 0) proxFrente += 12;/*/
		proxFrente = Direcao(J[vz].x, J[vz].y, mx, my);/**/
	} else {
		if (dz == 0) {
			if ((dx == 0) && (dy == 0)) return;
			if ((abs(dx) > 1) || (abs(dy) > 1)) return;
			if (J[vz].x % 2 == 0) {
				     if ((dx ==  1) && (dy ==  0)) proxFrente = 1;
				else if ((dx ==  1) && (dy ==  1)) return;
				else if ((dx ==  0) && (dy ==  1)) proxFrente = 3;
				else if ((dx == -1) && (dy ==  1)) return;
				else if ((dx == -1) && (dy ==  0)) proxFrente = 5;
				else if ((dx == -1) && (dy == -1)) proxFrente = 7;
				else if ((dx ==  0) && (dy == -1)) proxFrente = 9;
				else if ((dx ==  1) && (dy == -1)) proxFrente = 11;
			} else {
				     if ((dx ==  1) && (dy ==  0)) proxFrente = 11;
				else if ((dx ==  1) && (dy ==  1)) proxFrente = 1;
				else if ((dx ==  0) && (dy ==  1)) proxFrente = 3;
				else if ((dx == -1) && (dy ==  1)) proxFrente = 5;
				else if ((dx == -1) && (dy ==  0)) proxFrente = 7;
				else if ((dx == -1) && (dy == -1)) return;
				else if ((dx ==  0) && (dy == -1)) proxFrente = 9;
				else if ((dx ==  1) && (dy == -1)) return;
			}
			podeandar = true;
		} else if (dz == 1) {//subir
			cChao contchao = Mapa.Cont(mx, my, mz, cMapa::CHAO);

			if ((PodVoa(vz) || contchao.EhChaoElevador()) && J[vz].Posicao == posNormal) {
				proxFrente = J[vz].Frente;
				podeandar = true;
			}
		} else if (dz == -1) {//descer
			cChao contchao = Mapa.Cont(mx, my, mz + 1, cMapa::CHAO);

			if ((contchao.EhChaoBuraco() || contchao.EhChaoElevador()) && J[vz].Posicao == posNormal) {
				proxFrente = J[vz].Frente;
				podeandar = true;
			}
		}
	}
	if (J[vz].Posicao == posRastejando
	||  J[vz].Posicao == posNaMoto
	||  PodNaoUsaStrafe(vz)) {
		strafe = false;
	}
	if (PodAndaDeRe(vz)									//Se o cara for algu�m que anda de r� (Tanque e Michael Jackson)
	&&  !sovirar										//e querer andar
	&&  ((J[vz].Frente - proxFrente + 12) % 12 == 6)){	//para tr�s
		strafe = true;									//ent�o ele anda de r�
	}

	//Testes para possibilitar que o cara ande sem perder a mira
	bool strafemirando = false;
	if (!strafe && !PodNaoPerdeMiraAoAndar(vz)) {
		J[vz].SetFlag(Mirando, false); //perde a mira
	} else if (strafe && J[vz].GetFlag(Mirando)) {
		//strafe com mira sigfica andar sem perder a mira
		strafemirando = true;
		//ele tem que continuar olhando para o alvo depois de andar
		//BUG: � poss�vel que o cara n�o tenha tempo para andar depois de ter virado, assim ele vai estar olhando numa dire��o errada
		proxFrente = Direcao(mx, my, J[vz].AlvoX[0], J[vz].AlvoY[0]);
	}
	
	//Testa se o cara vai se virar antes de andar
	if ((proxFrente != J[vz].Frente) && (!strafe || strafemirando)) {
		if (J[vz].Posicao == posNaMoto
		//&& ((J[vz].Frente - proxFrente + 12) % 12 > 1)) {
		&& DifAngb(J[vz].Frente, proxFrente, 1)) {
			return; //o cara n�o pode fazer curvas bruscas com a moto
		} else {
			short angpermitido = (!sovirar &&
								  !strafemirando &&
								  J[vz].Posicao != posNaMoto &&
								  !PodNaoUsaStrafe(vz)) ? 1 : 0;
			
			if (!Vira(proxFrente, angpermitido) && J[vz].Posicao != posNaMoto) {
				return; //se o cara n�o terminou de virar (viu alguem ou acabou o tempo), n�o continua andando no m�s
			}
		}
	} else if (strafe) {
		//testa se o cara pracisava mesmo do strafe ou n�o
		short angpermitido = (!strafemirando) ? 1 : 0;
		if (DifAngb(J[vz].Frente, proxFrente, angpermitido)) strafe = false;
	}
	if (sovirar || !podeandar) {
		return; //ele s� queria virar OU apertou onde n�o pode ir
	}

	//MODIF.: Havia um teste aqui para evitar que o cara sa�sse do mapa
	//        Agora � imposs�vel clicar fora do mapa
	//        Se eu ler de novo este coment�rio, acho que devo apag�-lo
	
	if (podeandar) {
		//if (dz == 0) {
			//some com as linhas dos tiros
			contlinhatiro = 0;
			contsombratiro = 0;

			short tempo;
			if (J[vz].Posicao == posNaMoto)				tempo = TempoAndarMoto;
			else if (J[vz].Posicao == posRastejando)	tempo = TempoAndarRast;
			else if (J[vz].Posicao == posAbaixado) {
				if (strafe)								tempo = TempoAndarAbaiStraf;
				else									tempo = TempoAndarAbai;
			} else if (strafe)							tempo = TempoAndarStraf;
			else										tempo = TempoAndar;

			if (PodAndaDevagarAoSerVisto(vz)) {
				for (short i=0; i < numjogs; i++)
				if (!J[i].GetFlag(Morto)) {
					if (EstaSendoVistoPor(i)) {
						tempo *= 3;
						break; //exit for
					}
				}
			}

			bool atropelar = false;
			short atropelado = -1;
			for (short i=0; i < numjogs; i++)
			if (!J[i].GetFlag(Morto)) {
				if (i != vz) {
					if ((mx == J[i].x) &&
						(my == J[i].y) &&
						(mz == J[i].z) &&
						J[i].Posicao != posEnterrado) {
						atropelar = true;
						atropelado = i;
					}
					//TO DO: testar se o cara bateu em um holografia
				}
			}
			
			if (atropelar) {
				if (!EstaVendo(atropelado)) {
					MsgDef("Se voc� n�o sabe, h� algu�m a�!");
					return;
				} else if (strafe && PodAndaDeRe(vz)) { //o tanque pode bater de r�
					MsgDef("Voc� n�o pode bater no strafe!");
					return;
				} else if (J[vz].Posicao == posNaMoto) {
					MsgDef("Voc� n�o pode bater com a sua moto!");
					return;
				} else {
					tempo = 0; //Ele n�o vai andar mesmo; e, se ele for golpear, haver� um teste depois
					if (!Vira(proxFrente, 0)) return;
				}
			}

			if (tempo > J[vz].T) {
				MsgSemTempo("se mover", tempo);
				return;
			}

			cCont cont = Mapa.Cont(mx, my, mz, cMapa::NAO_CHAO);
			cChao contchao = Mapa.Cont(mx, my, mz, cMapa::CHAO);

			if (cont.EhPortaTrancada()) {
				if (J[vz].GetFlag(TemChave)) {
					cont.AbrePorta();
					Mapa.SetCont(mx, my, mz, cont);
				} else if (PodSabeArrombarPortas(vz)) {
					FXuint resp = Msg(MBOX_YES_NO, "Voc� quer tentar arrombar a porta?");
					if (resp == MBOX_CLICKED_YES) {
						J[vz].SetFlag(Mirando, false); //perde a mira

						bool abriu = false;
						if (rnd() < 0.4) {
							abriu = true;
							cont.AbrePorta();
							Mapa.SetCont(mx, my, mz, cont);
							MsgDef("Voc� abriu a porta!");
						} else {
							MsgDef("Voc� n�o conseguiu abrir a porta!");
						}
						J[vz].Replay.GravaFoto(AcaoAbrePorta, num(mx), num(my), num(mz), num(abriu));
						GastaTempo(tempo);
						TestaEncontro(true);
						return;
					} else {
						return; //ele n�o quer tentar...
					}
				} else {
					MsgDef("Voc� precisa de uma chave para abrir portas trancadas!");
					return;
				}
			}

			if (cont.EhLata()) {
				/*TO DO: Traduzir isto:
				Call Explos�o(j(vz).X + dx, j(vz).Y + dy, 0, 0, j(vz).Z, 6, 3.1, 1) 'lata
				Call TestaMortes*/
				//if (J[vz].GetFlag(Morto)) Then Call PassaVez
			} else {
				//TO DO: o T-1000 atravessa grades, mas o equipamento dele n�o. Solu��o: n�o sei

				//aten��o: as pr�ximas linhas simulam um "OU"
				bool podeir = atropelar;
				if (!podeir) podeir = !cont.EhParedeSolida();
				if (!podeir) podeir = PodAtravessaTijolo(vz) && !cont.EhParedePedra() && J[vz].Posicao != posNaMoto;
				if (!podeir) podeir = (PodAtravessaMadeira(vz) || ArmaEhCorpoACorpo(J[vz].ArmaMaoCod())) && cont.EhParedeMadeira() && J[vz].Posicao != posNaMoto;
				if (!podeir) podeir = PodAtravessaGrade(vz) && cont.EhGrade() && J[vz].Posicao != posNaMoto;
				if (podeir) {
					if (J[vz].Posicao == posRastejando && atropelar) {
						MsgDef("Voc� precisa se levantar antes de bater!");
						return;
					}

					//MODIF.: antes o fatasma criava pass. secretas e os caras quebravam as grades
					if (PodQuebraParede(vz) && cont.EhParedeSolida() && !cont.EhParedePedra()) {
						cont.SetFumaca();
						Mapa.SetCont(mx, my, mz, cont);
						/*//TO DO: gravar o som
						ContSons = ContSons + 1
						RepSom(ContSons) = Sons.DestroiParede*/
					} else if (cont.EhParedeMadeira() && (PodAtravessaMadeira(vz) || ArmaEhCorpoACorpo(J[vz].ArmaMaoCod()))) {
						cont.SetFumaca();
						Mapa.SetCont(mx, my, mz, cont);
						/*//TO DO: gravar o som
						ContSons = ContSons + 1
						RepSom(ContSons) = Sons.DestroiParede*/
					}
					if (atropelar) {
						Atropela(atropelado);
					} else {
						if (PodRachaChao(vz) && (mz > 0) && (dz == 0) && contchao.EhChaoMadeira()) {
							if (rnd() < 0.02) {
								contchao.SetChaoBuracoInv();
								Mapa.SetChao(J[vz].x, J[vz].y, J[vz].z, contchao);
							} 
							if (rnd() < 0.15) {
								contchao.SetChaoBuracoInv();
								Mapa.SetChao(mx, my, mz, contchao);
							}
						}
						
						//Anima��o
						if (strafemirando) proxFrente = Direcao(J[vz].x, J[vz].y, mx, my);
						double ang = -(PI/6) * DifAngi(J[vz].Frente, proxFrente);
						NaoAnimando = false;
						J[vz].modelo.AnimInicia(cModeloAnim::ANIM_RUN);
						J[vz].modelo.SetAnimTransl(cos(ang), sin(ang), 0);
						pwinMain->getApp()->runUntil(NaoAnimando);
						J[vz].modelo.AnimInicia(cModeloAnim::ANIM_STAND);
						
						J[vz].x = mx;
						J[vz].y = my;
						J[vz].z = mz;

						//mira piora ao andar
						if (J[vz].GetFlag(Mirando)) {
							short arma = J[vz].ArmaMaoCod();
							J[vz].MiraAtual = Armas[arma].MultMira(J[vz].MiraAtual, Armas[arma].CoiceMove);
						}

						J[vz].Replay.GravaFoto(AcaoMove, num(mx), num(my), num(mz));
						GastaTempo(tempo);
						TestaEncontro(true);
						OndePisa(vz);
					}
				//} else if (EhParedeSolida(cont) && !EhPortaNormal(cont) && !EhPortaTrancada(cont)) {
				} else if (cont.EhParedeSolida() && cont.EhPortaSecreta()) {
					FXuint resp = Msg(MBOX_YES_NO, "Quer procurar uma passagem secreta?");
					if (resp == MBOX_CLICKED_YES) {
						bool abriu = false;
						if (cont.EhPortaSecreta()) {
							abriu = true;
							cont.AbrePorta();
							Mapa.SetCont(mx, my, mz, cont);
							MsgDef("Voc� abriu a passagem!");
						} else {
							MsgDef("Voc� nao conseguiu abrir a porta!");
						}
						J[vz].Replay.GravaFoto(AcaoAbrePorta, num(mx), num(my), num(mz), num(abriu));
						GastaTempo(tempo);
						TestaEncontro(true);
					}
				}
			}
		/*} else { //voar
			//TO DO: fazer os caras voarem / usarem elevadores / usarem escadas / escalar paredes
		}*/
	}	
}

void cJogo::OndePisa(short quem)
{
	if (!Buracos(quem)) {
		cCont cont;
		cont = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::NAO_CHAO);
		//char contchao = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::CHAO);

		if (cont.EhTeletransporte()) {
			//TO DO: Teletransporta (olhar duelo2d)

		//TO DO: else if (pisou em: uma mina; um parasita; sangue alien) {}
		} else {
			double danodefogo = 0;
			if (PodImuneAFogo(quem))		; //apenas evita testar se era fogo ou n�o
			else if (cont.EhFogo())			danodefogo = 20;
			else if (cont.EhFogoPermanente())danodefogo = 30;

			if (danodefogo > 0) {
				double blindagem_a_fogo = PodResistenteAFogo(quem) ? 2 : 0.5;
				double dor = CausaDor(quem, danodefogo, J[quem].Blindagem * blindagem_a_fogo);

				if (quem == vz) {
					FXString msg;
					MsgDef(msg.format("Voc� se queimou! (%.1f)", dor));
				}

				if (TestaMortes(vz)) return;
			}
		}
	}
}

bool cJogo::Buracos(short quem)
{
	int quedas = 0;
	
	//quem flutua nunca cai
	if (!PodFlutua(quem)) {
		cChao contchao = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::CHAO);
		cCont cont;

		while (J[quem].z > 0 && (contchao.EhChaoBuraco() || contchao.EhChaoBuracoInv())) {
			quedas++;

			bool quebrouchao = contchao.EhChaoBuracoInv();

			contchao.SetChaoBuraco();
			Mapa.SetChao(J[quem].x, J[quem].y, J[quem].z, contchao);
			
			//quem voa e n�o flutua quebra buracos invis�veis, mas n�o cai
			if (PodVoa(quem) && quebrouchao) {
				if (quem == vz) MsgDef("O ch�o estava fr�gil e voc� o quebrou!");
				return false;
			}

			J[quem].z--;
			J[quem].Fog->selectedZ = J[quem].z;

			contchao = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::CHAO);
			cont = Mapa.Cont(J[quem].x, J[quem].y, J[quem].z, cMapa::NAO_CHAO);

			if (quem == vz) MsgDef("Voc� caiu!");

			for (short i=0; i<numjogs; i++)
			if (!J[i].GetFlag(Morto)) {
				if (quem != i &&
				    J[quem].x == J[i].x &&
					J[quem].y == J[i].y &&
					J[quem].z == J[i].z) {
					double dor1 = CausaDor(i, 20, J[i].Blindagem);		//causa 20
					double dor2 = CausaDor(quem, 10, J[quem].Blindagem); //leva 10
					if (quem == vz) {
						FXString msg;
						MsgDef(msg.format("%s estava no caminho e levou %.1f de dano!\n Voc� levou %.1f!", J[i].Nome, dor1, dor2));
					}
				}
			}

			if (cont.EhLata()) {
				//TO DO: explos�o da lata
			}

			if (quem == vz) {
				/*//TO DO: replay som
				Foto = Foto + 1
				j(vz).RepA��oOQue(Foto) = 4
				j(vz).RepA��oQ1(Foto) = 3
				ContSons = ContSons + 1
				RepSom(ContSons) = Sons.queda*/
				//J[quem].T -= 4;
				J[vz].Replay.GravaFoto(AcaoMove, num(J[vz].x), num(J[vz].y), num(J[vz].z));
				TestaEncontro(true);
			}
			//if (TestaMortes(vz)) return true;
			//if (J[quem].GetFlag(Morto)) return true;
		}
		if (quedas > 0) {
			//s� agora d� o dano de toda a queda
			double dor = CausaDor(quem, 15.0 * pow((double)quedas, 1.5), J[quem].Blindagem);
			FXString msg;
			if (quem == vz) {
				MsgDef(msg.format("O dano total da queda foi %.1f!", dor));
			} else {
				if (quedas == 1) MsgDef(msg.format("%s caiu!", J[quem].Nome));
				else			 MsgDef(msg.format("%s caiu %d andares!", J[quem].Nome, quedas));
			}
			if (TestaMortes(vz)) return true;
			if (!J[quem].GetFlag(Morto)) OndePisa(quem);
		}
	}
	return (quedas > 0);
}

void cJogo::Atropela(short atrop)
{
	bool bate = false;
	short tempo;
	short arma = J[vz].ArmaMaoCod();
	
	//TO DO: o alien pequeno tem que ter op�ao de dar porrada

	if (PodBate(vz) || ArmaEhCorpoACorpo(arma)) {
		tempo = TempoAtropelar;

		if (J[vz].T >= tempo) {
			/*//TO DO: Replay
            Foto = Foto + 1: j(vz).RepA��oOQue(Foto) = 11
            j(vz).RepA��oQ1(Foto) = j(Atrop).X: j(vz).RepA��oQ2(Foto) = j(Atrop).Y
            GravaQuemViu False*/
			//� necess�rio gravar a foto antes porque a v�tima pode se defender e o atque deve aparecer mesmo assim
			J[vz].Replay.GravaFoto(AcaoGolpeia, num(J[atrop].x), num(J[atrop].y), num(J[atrop].z));

			if (!PodImpossibilitaEsquiva(vz)) {
				if (PodDefendeComEspada(atrop)
				&& J[atrop].ArmaMaoCod() == aSabreDeLuz
				&& J[atrop].TProxRodada >= TempoDefesaComEspada
				&& EstaSendoVistoPor(atrop)) {
					//o agredido gasta tempo da pr�xima rodada
					J[atrop].TProxRodada -= TempoDefesaComEspada;

					//o agressor leva dano
					double dor = CausaDor(vz, PodJedi_DanoRetaliacao, J[vz].Blindagem);

					FXString msg;
					MsgDef(msg.format("Ele defendeu o golpe com a espada!\nVoc� levou %.1f de dano!", dor));

					GastaTempo(tempo);
					return;
				} else if (PodSuperEsquiva(atrop)
					   &&  J[atrop].TProxRodada >= TempoEsquivaMatrix
					   &&  EstaSendoVistoPor(atrop)) {
					MsgDef("Ele se esquivou do golpe!");
					
					J[atrop].TProxRodada -= TempoEsquivaMatrix;
					GastaTempo(tempo);
					return;
				}
			}
			
			double dano = 0;
			short som = SOM_NENHUM;
			if (arma != -1) {
				if (ArmaEhCorpoACorpo(arma)) {
					//L� o dano da arma do cara
					dano = Armas[arma].DanoGolpe;
					som = Armas[arma].SomPad;
					if (dano < 0) { //Soco e outras armas cujo dano dependerem da for�a do cara
						dano *= -J[vz].Forca;
					}
				}
			} else {
				dano = J[vz].PodDanoGolpe();
				som = J[vz].PodSomGolpe();
			}

			J[vz].Replay.GravaSom(som);
			/*//TO DO: gravar som
			Som = J[vz].PodSomGolpe();
			ContSons = ContSons + 1
            RepSom(ContSons) = Som
            If RepSom(ContSons) = 0 Then ContSons = ContSons - 1*/

			double dor = CausaDor(atrop, dano, J[atrop].Blindagem);

			if (TestaMortes(vz)) return;

			/*//TO DO: espalhar sangue
			If (Dano > 1.5 Or j(Atrop).Ht = 0) And j(Atrop).Poder <> pTanque And j(Atrop).Poder <> pT_1000 And Atrop <= M Then
				...
			End If*/
			
			if (PodCamuflagemControlavel(vz) && J[vz].Camufl > J[vz].CamuflNat) {
				//qdo o predador ou balacau atacam, perdem a camuflagem
				J[vz].Camufl = J[vz].CamuflNat;
				drawScene(0);
			}

			GastaTempo(tempo);
			TestaEncontro(true);

			if (J[atrop].GetFlag(Morto) && PodEngole(vz)) {
				//TO DO: engolir (destruir) todo o equipamento que o TestaMortes soltou
				//TO DO: retirar o objeto 'esqueleto' (que o TestaMortes colocou) da frente do tarrasque
				//TO DO: de algum jeito tirar o esqueleto do Fog tb

				J[vz].Ht = Max(J[vz].Ht, J[vz].HtMax / 4.0);
				J[vz].Reg += 0.2f;

				//TO DO: testar se o atropelado era um predador com a autodestrui��o ativada e fazer com que o tarrasque fique com autodestrui��o

				MsgDef("Voc� COMEU um corpo com muitas prote�nas!");
			}
		} else {
			if (PodEngole(vz)) {
				MsgSemTempo("engoli-lo", tempo);
			} else {
				MsgSemTempo("trucid�-lo", tempo);
			}
		}
	} else if (PodIncubacaoAlien(vz)) {
		tempo = TempoIncubacaoAlien;

		if (J[atrop].Bot) {
			MsgDef("Voc� n�o injetar ovos nos bots!");
		} else if (PodImuneIncubacaoAlien(atrop)) {
			MsgDef("Voc� n�o injetar ovos em um tanque!");
		} else if (J[vz].T >= tempo) {
			MsgDef("Voc� injetou um ovo nele!");
			if (!PodImuneOvo(atrop)) {
				J[atrop].Incubacao = 2; //o cara ter� 2 rodadas antes de se transformar num alien
				J[atrop].Injetor = vz;
			}
			J[vz].Ht = 0; //o alien que injetou morre
			if (TestaMortes(vz)) return;
		} else {
			MsgSemTempo("injetar ovos", tempo);
		}
	} else {
		MsgDef("Se voc� n�o sabe, h� algu�m a�!");
	}
}

bool cJogo::Vira(short proxFrente, short angpermitido)
{
	//retorna true se o cara terminou de virar

	short delta, step;
	short tempov;
	short difang = (J[vz].Frente - proxFrente + 12) % 12;
	
	if ((difang <= angpermitido) || (difang >= 12 - angpermitido)) {
		return true; // o cara n�o [pode/quer] virar mais do que isso
	}

	delta = proxFrente - J[vz].Frente;
	if (abs(delta) > 6) {
		step = -delta/abs(delta);
	} else {
		step = delta/abs(delta);
	}

	do {
		if (PodViraDevagar(vz))
			tempov = 12;
		else if (J[vz].Posicao == posRastejando)
			tempov = 12;
		else if (J[vz].Posicao == posAbaixado)
			tempov = 8;
		else
			tempov = 4;

		if (J[vz].T < tempov) {
			MsgSemTempo("se virar", tempov);
			return false;
		}

		J[vz].Frente = (J[vz].Frente + step + 12) % 12;
		GastaTempo(tempov);

		/*if (!J[J[vz].idImit].Bot) { //este if existe porque os bots n�o olhavam em nehuma dire��o
			TO DO: Gravar Foto
			Foto = Foto + 1
			j(vz).RepA��oOQue(Foto) = 2
			j(vz).RepA��oQ1(Foto) = j(vz).Vis�o
		}*/
		J[vz].Replay.GravaFoto(AcaoVira, num(J[vz].Frente));

		//TO DO: anima��o com timer para a rota��o
		drawScene(0);

		if (TestaEncontro(false)) { //se o cara ver algu�m, p�ra
			return false;
		}
		difang = (J[vz].Frente - proxFrente + 12) % 12;
	} while ((difang > angpermitido) && (difang < 12 - angpermitido));

	return true;
}

void cJogo::GastaTempo(short tempo)
{
	//esta fun��o deve ser utilizada sempre que o vz gastar tempo
	//aqui, a��es que dependem do passar do tempo ser�o executadas (ex.: dano por fogo)
	
	J[vz].T -= tempo;
}

bool cJogo::TestaEncontro(bool atualizafog = true, double camvz)
{
	bool ret = false;
	FXString msg;

	if (atualizafog) AtualizaFog();

	//a chance de ver um cara deve depender de quanto tempo se gastou desde o ultimo teste
	short dt = J[vz].TAnt - J[vz].T;
	J[vz].TAnt = J[vz].T;

	if (dt == 0) return false; //otimiza��o, j� que n�o vai ver ningu�m se n�o gastar tempo

	for(int op=0; op<numjogs; op++)
	if (!J[op].GetFlag(Morto)) {
		if (J[op].GetFlag(Morto) || (op == vz)) {
			JaVi[op] = 0;
		} else {
			//TO DO: fazer uma passada extra se o jogador tiver holografia
			bool holografia = false;

			int xop = J[op].x;
			int yop = J[op].y;
			int zop = J[op].z;
			char javiu = holografia ? JaViHol[op] : JaVi[op];
			bool avisar = false;

			if (EstaSendoVistoPor(op)) camvz *= 0.65;

			if (Viu(vz, op, javiu, dt, xop, yop, zop)) {
				if (Viu(op, vz, 0, dt, -1, -1, -1, camvz)
				&&  !holografia
				&&  !TempoParado) { //op e vz se viram; a holografia � cega, s� um cara real pode ver
					if (!EstaVendo(op)) { //se n�o estava vendo, avise
						J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z,
											 J[op].x, J[op].y, J[op].z);
						avisar = true;
						ret = true;
					}
					JaVi[op] = 2;
					if (avisar) MsgDef(msg.format("Voc� viu o %s!", J[J[op].idImit].Nome));
					J[vz].Replay.VeFoto(op);
				} else { //apenas vz viu op
					if (!EstaVendo(op)) { //se n�o estava vendo, avise
						J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z,
											 J[op].x, J[op].y, J[op].z);
						avisar = true;
						ret = true;
					//MODIF: if modificado ver TestaEncontro no Duelo2D
					} else if (EstaSendoVistoPor(op)) { //op perdeu vz de vista
                        /*TO DO: Traduzir isto
						If j(vz).RepA��oOQue(Foto) = 1 Then 'O �ltimo passo era andar
                            Call V�Foto(op, vz)
                            'ViuRepF(op, vz, Foto) = 1
                        End If*/
						J[vz].Replay.VeFoto(op);
					}
					if (holografia) {
						JaViHol[op] = true; //vz viu uma holografia
					} else {
						JaVi[op] = 1;
					}
					if (avisar) MsgDef(msg.format("Voc� viu o %s!", J[J[op].idImit].Nome));
				}
			} else if ((vz != op) && !J[op].GetFlag(Morto)) {
				if (Viu(op, vz, 0, dt, -1, -1, -1, camvz)
				&&  !holografia
				&&  !TempoParado) { //apenas op viu vz
					if (!EstaSendoVistoPor(op) && PodAndaDevagarAoSerVisto(vz)) {
						MsgDef("Algu�m o viu!");
						ret = true;
					}
					JaVi[op] = 3;
					J[vz].Replay.VeFoto(op);
				} else {
					if (EstaSendoVistoPor(op)) { //op perdeu vz de vista
                        /*TO DO: Traduzir isto
						If j(vz).RepA��oOQue(Foto) = 1 Then 'O �ltimo passo era andar
                            Call V�Foto(op, vz)
                            'ViuRepF(op, vz, Foto) = 1
                        End If*/
					}
					JaVi[op] = 0;
				}
			}
		}
	}
	return ret;
}

bool cJogo::EstaVendo(int quem)
{
	return ((JaVi[quem] == 1) || (JaVi[quem] == 2));
}

bool cJogo::EstaSendoVistoPor(int quem)
{
	return ((JaVi[quem] == 2) || (JaVi[quem] == 3));
}

bool cJogo::Viu(int obsador, int obsado, char javiu, short dt,
				int xop, int yop, int zop, double camextra)
{
	bool ret = false;
	bool testar = true;

	if ((obsador == obsado) || J[obsador].GetFlag(Morto)) {
		ret = false;	testar = false;
	} else if (!J[obsador].Bot && J[obsado].Bot) {//pessoa observa bot
		if ((J[obsador].Time == J[obsado].Time)
		|| (PodSempreVeBotsNeutros(obsador) && (J[obsado].Time == -1))) {
			ret = true;	testar = false;
		}
	} else if (J[obsador].Bot && !J[obsado].Bot) {//bot observa pessoa
		/*//MODIF: Antes os bots n�o viam os agentes. Agora eles v�em, mas n�o devem atirar
		if ((J[obsador].Time == J[obsado].Time)
		||  (J[obsador].Time == J[J[obsado].idImit].Time)
		||  ((J[J[obsado].idImit].Poder==pAgente_do_Matrix) && (J[obsador].Time == -1))) {
			ret = false;
			testar = false;
		}*/
	}

	if ((J[obsador].Time == J[obsado].Time)
	||  (J[obsado].GetFlag(Parasitado) && PodSempreVeParasitados(obsador))) {
		ret = true;		testar = false;
	} else if (J[obsador].Posicao == posEnterrado
		   ||  (J[obsado].Posicao == posEnterrado && !PodVisaoRaioX(obsador))) {
		ret = false;	testar = false;
	}

	if ((javiu == 1) || (javiu == 2)) { //se j� estava vendo, continua vendo
		ret = true;		testar = false;
	}

	if (testar) {
		double dist;
		double fx, fy, fz;
		double fxop, fyop, fzop;
		double obsado_camufl;

		obsado_camufl = J[obsado].Camufl;
		if (J[obsado].Posicao == posNaMoto) obsado_camufl = 0.5;
		if (J[obsado].Posicao == posAbaixado) obsado_camufl *= 1.3;
		if (J[obsado].Posicao == posRastejando) obsado_camufl *= 1.6;
		if (J[obsado].Posicao == posEnterrado && PodVisaoRaioX(obsador)) obsado_camufl *= 1.5;
		if (J[obsador].GetFlag(TemIR) && !PodCorpoFrio(obsado)) obsado_camufl /= PodVisaoIR;;
		if (J[obsador].Posicao == posNaMoto) obsado_camufl *= 2.0;
		obsado_camufl *= camextra;

		if (xop == -1) xop = J[obsado].x;
		if (yop == -1) yop = J[obsado].y;
		if (zop == -1) zop = J[obsado].z;

		CentroHex3D(J[obsador].x, J[obsador].y, J[obsador].z, fx, fy, fz, 0.5);
		CentroHex3D(J[obsado].x, J[obsado].y, J[obsado].z, fxop, fyop, fzop, 0.5);
		//dist = Distancia3D(fxop, fyop, fzop, fx, fy, fz);
		cPontoAPonto P;
		P.IniciaAlvo(fx, fy, fz, fxop, fyop, fzop);
		dist = P.GetDistMax();

		bool soradar;
		bool radar = (dist < J[obsador].PodRadar(soradar));

		if (soradar && !radar) {
			return false; //o cara s� tem radar mas o radar n�o alcan�a
		}

		//TO DO: tra�ar v�rias linhas (ao inv�s de uma) para testar se pode ver cada oponente
		/*double dw = 0.2f / dist;
		double w, xw, yw, zw;
		int xi, yi, zi;
		int xiant=-1, yiant=-1, ziant=-1;
		bool ehchao, ehchaoant = true;*/
		cCont cont;
		cChao chao;
		cAfin afin;
		bool parede, passaradar, primeirohex = true;
		bool temlinha = true;
		//for (w=0; w<1; w+=dw) {
			//xw = (1 - w) * fx + w * fxop;
			//yw = (1 - w) * fy + w * fyop;
			//zw = (1 - w) * fz + w * fzop;
		while (P.Avanca(0.2)) {
			//PontoToHex3D(xw, yw, zw, xi, yi, zi, ehchao);

			//if ((xi!=xiant) || (yi!=yiant) || (zi!=ziant) || (ehchao!=ehchaoant)) { //testa se mudou de hex.
			if (primeirohex || P.MudouHex()) {
				//descobre o conte�do do hex.
				cont = Mapa.Cont(P.xi, P.yi, P.zi);
				chao = Mapa.Chao(P.xi, P.yi, P.zi);
				afin = Mapa.Afin(P.xi, P.yi, P.zi);
				P.SetAfinamento(&afin);

				//cada fuma�a conta como 2 hex a mais de dist�ncia
				if (!P.ehchao && cont.EhEsfumacado()) dist += 2.0;//obsado_camufl *= 1.15;

				P.Valida();
			}
			if (P.AcertouParede()) {
				if (primeirohex) {
					parede = false;
				} else {
					if (!soradar) { //ajusta 'parede' de acordo com a VIS�O normal
						if (J[obsador].PodVeAtraves(cont, chao, P.ehchao, passaradar)) {
							parede = false;
						} else {
							parede = true;
							soradar = true;
						}
					}

					if (radar) { //ajusta 'parede' de acordo com o RADAR
						if (radar && passaradar) {
							parede = false;
						} else if (!radar && soradar) {
							parede = true;
						}
					}
				}

				//se n�o consegue mais ver ent�o termina linha
				if (parede && !primeirohex) {
					temlinha = false;
					break; //exit while
				}
				primeirohex = false;
			}
		}
		if (temlinha) {
			if (radar) {
				//radar nunca falha
				ret = true;
			} else {
				//vis�o normal depende de sorte

				double CosAng; //CosAng = cos do �ng. entre a vis�o do Observador e a dire��o q est� o Observado
				if (dist < 0.5f) {
					CosAng = 1; //se est�o no mesmo lugar, se olham de frente
				} else if (PodVisao360Graus(obsador) || J[obsador].Bot) {
					CosAng = 1; //monstro e mosca olham direto
				} else {
					double alfa = (PI/6) * J[obsador].Frente;
					CosAng = ((fxop - fx) * cos(alfa) + (fyop - fy) * sin(alfa)) / dist;
				}

				// A visao do Observador tem distribuicao exponencial-de-normal
				// Em condi��es normais (Visao=Camufl=1, olhando de frente), se enxerga em m�dia
				// uma dist�ncia de 15 quadrados. Olhando de lado (CosAng=0), a dist. m�d � 5 e de costas
				// (CosAng=-1), a d.m. � 1. Ajustei uma par�bola p/ estes valores.
				double VisaoMedia = (5.33f + 7.11f * CosAng + 2.73f * CosAng * CosAng) * J[obsador].Olho / obsado_camufl;

				//como ver � um processo estoc�stico, deve-se mutiplicar a prob. por dt
				VisaoMedia *= (double)dt / 10.0;
				
				ret = (Aleatorio(VisaoMedia) > dist);
			}
		} else {
			ret = false;
		}
	}

	//final
	if (ret) J[obsador].podeimitar[obsado] = true;
	return ret;
}

void cJogo::AtualizaFog()
{
	double xh, yh, zh;
	CentroHex3D(J[vz].x, J[vz].y, J[vz].z, xh, yh, zh, 0.5);

	bool soradar;

	double radar = J[vz].PodRadar(soradar);
	double d0 = 0.1f;
	double teta0 = 0;

	const double deltaphi = PI / 36.0; //PI/12 = 15�
	const double deltateta = PI / 50.0; //0.025f => 2� quando seno(phi)=1

	//int linhas = 0;

	for (double phi=deltaphi; phi < PI; phi+= deltaphi)
	{
		double deltatetamod = deltateta / sin(phi);
		for (double teta=teta0; teta < 2*PI; teta+= deltatetamod)
		{
			LinhaFog(xh, yh, zh, vz, radar, soradar, teta, phi, d0);
			d0 += 0.1f;
			if (d0 > 0.4f) d0 = 0.1f;
			//linhas++;
		}
		teta0 += deltatetamod / 4.0;
		if (teta0 > deltatetamod) teta0 = 0;
	}
	//linhas=linhas; //1916 linhas
}

void cJogo::LinhaFog(double x0, double y0, double z0,
					 ushort obsador, double radar, bool soradar,
					 double teta, double phi, double d0)
{
	double w;
	/*double xw, yw, zw;
	int xi, yi, zi;
	int xiant=-1, yiant=-1, ziant=-1;
	bool ehchao, ehchaoant = true;
    double dx = sin(phi) * cos(teta);
    double dy = sin(phi) * sin(teta);
    double dz = cos(phi);*/
    cPontoLinha P;
    P.IniciaAngulos(teta, phi, x0, y0, z0);

	cCont cont;
	cChao chao;
	cAfin afin;
	bool parede, passaradar;
	bool primeirohex = true;
	//bool testar_parede;

	for (w = d0; w<90; w+=0.2) {
		//xw = x0 + (w * dx);
		//yw = y0 + (w * dy);
		//zw = z0 + (w * dz);
		P.Avanca(w);

		if (primeirohex || P.MudouHex()) {
			//testa se saiu do mapa
			if (P.ForaDoMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ)) {
				break; //exit for
			}

			cont = Mapa.Cont(P.xi, P.yi, P.zi);
			chao = Mapa.Chao(P.xi, P.yi, P.zi);
			afin = Mapa.Afin(P.xi, P.yi, P.zi);
			P.SetAfinamento(&afin);

			//atualiza fog
			cCont fogbitcont = J[obsador].FogBit->Cont(P.xi, P.yi, P.zi);
			cChao fogbitchao = J[obsador].FogBit->Chao(P.xi, P.yi, P.zi);
			cCont fogcont =    J[obsador].Fog   ->Cont(P.xi, P.yi, P.zi);
			cChao fogchao =    J[obsador].Fog   ->Chao(P.xi, P.yi, P.zi);
			if (P.ehchao) {
				if (fogbitchao == 0 || fogchao != chao) {
					J[obsador].Fog->SetChao(P.xi, P.yi, P.zi, chao);
					J[obsador].FogBit->SetChao(P.xi, P.yi, P.zi, 1);
					//ele sempre ve os objetos dali tb
					J[obsador].Fog->SetResumoObj(P.xi, P.yi, P.zi, Mapa.GetResumoObj(P.xi, P.yi, P.zi));
				}
			} else {
				if (fogbitcont == 0 || fogcont != cont) {
					J[obsador].Fog->SetCont(P.xi, P.yi, P.zi, cont);
					J[obsador].FogBit->SetCont(P.xi, P.yi, P.zi, 1);
					//o cara sempre ve o chao se conseguir ver a parte de cima
					J[obsador].Fog->SetChao(P.xi, P.yi, P.zi, chao);
					J[obsador].FogBit->SetChao(P.xi, P.yi, P.zi, 1);
					//ele sempre ve os objetos dali tb
					J[obsador].Fog->SetResumoObj(P.xi, P.yi, P.zi, Mapa.GetResumoObj(P.xi, P.yi, P.zi));
				}
			}

			P.Valida();
		/*} else if (P.parede_fina_perto) {				
			//est� pr�ximo a uma parede fina, mas n�o acertou ela ainda => testar se acertou agora
			if (P.AcertouParede(afin)) {
				P.parede_fina_perto = false;
				testar_parede = true;
			}*/
		}

		if (P.AcertouParede()) {
			if (primeirohex) {
				parede = false;
			} else {
				if (!soradar) {	//ajusta 'parede' de acordo com a VIS�O normal
					if (J[obsador].PodVeAtraves(cont, chao, P.ehchao, passaradar)) {
						parede = false;
					} else {
						parede = true;
						soradar = true;
					}
				}

				if (radar > 0) { //ajusta 'parede' de acordo com o RADAR
					if (P.ehchao) {
						if (!chao.EhChaoTransparente() && chao.EhChaoDuro()) {
							radar -= 1;
						}
					} else {
						//cada parede (que atrapalha o movimento) no caminho 'abafa' o radar
						if (cont.EhParedeSolida() && !cont.EhGrade()) {
							radar -= 0.75;
							if (cont.EhParedeDura()) radar -= 0.75;
							if (cont.EhParedePedra()) radar -= 1.5;
						}
					}

					if ((radar >= w) && (passaradar)) {
						parede = false;
					} else if ((radar < w) && (soradar)) {
						parede = true;
					}
				}
			}

			//se n�o consegue mais ver ent�o termina linha
			if (parede && !primeirohex) break;
			primeirohex = false;
		}
	}
}

double cJogo::CausaDor(short quem, double dano, double blindagem)//, short causa)
{
	double dor;

	if (quem == vz) MsgDef("BUG?: algo causou dor no J[vz]");

	dor = Aleatorio(dano) - Aleatorio(blindagem);
	if (dor <= 0) {
		dor = 0;
	} else {
		//efeitos estranhos exclusivos de tanques
		if (PodPodeExplodirTanque(quem)) {
			if (dor > Aleatorio(40) || dor >= J[quem].Ht) { //se um tanque recebeu uma dor alta, ou morreu ent�o explode
				J[quem].Ht = 0;
				//TO DO: Explosao
			} else if (dor > 20) { // recebeu dor alta o suficiente para causar efeitos diferentes
				double r = rnd();
				if (r < 0.15) {			//fica imobilizado
					J[quem].SetFlag(Imovel, true);
				} else if (r < 0.25) {	//perde canh�o
					//TO DO: zera a muni��o do canh�o
				} else if (r < 0.35) {	//perge MG
					//TO DO: zera a muni��o da metralhadora
				}			
			}
		}

		//causa a dor
		J[quem].Ht -= dor;

		Sangra(quem);

		//pode explodir a moto
		if (J[quem].Posicao == posNaMoto && rnd() < 0.1 + 0.005 * dor) {
			J[quem].Posicao = posNormal;
			MsgDef("L� se vai uma moto pelos ares!");
			//TO DO: Explosao
		}
		//pode explodir o dinamitador
		if (PodPodeExplodirDinamite(quem) && rnd() < 0.01 * dor) {
			if (!J[quem].GetFlag(DinaExplodiu)) {
				J[quem].SetFlag(DinaExplodiu, true);
				MsgDef("Booooom!!\nO mapa do estado vai ter que ser alterada por causa da explox�o de um dinamitador!");
				//TO DO: Grande Explosao
			}
		}

		//if (causa != -1) TestaMortes(causa);
	}
	return dor;
}

void cJogo::Sangra(short quem, short x, short y, short z)
{
	char sangue = J[quem].PodSangue();
	if (sangue != 0) {
		//se a posi��o n�o foi informada, usar a posi��o do quem
		if (x == -1) x = J[quem].x;
		if (y == -1) y = J[quem].y;
		if (z == -1) z = J[quem].z;

		Mapa.AdObjSimpPos(sangue, x, y, z);
		AtualizaFog();//TO DO: confirmar se isso � necess�rio
	}
}

bool cJogo::TestaMortes(short causamortis)
{
	// causamortis |  significado
	//   >=  0    | algum jogador
	//    = -1    |  degenera��o

	short assassino = -1;
	FXString msg;

	for (short i=0; i<numjogs; i++) {
		if (J[i].Ht <= 0 && !J[i].GetFlag(Morto)) {
			//TO DO: Replay Som de morte (depende do poder do falecido)
			
			J[i].SetFlag(Morto, true);
			J[i].Mortes ++;
			
			J[vz].PodSoltaCorpo(&Mapa);

			//TO DO: solta uma bomba, se for um predador com auto-destrui��o ativada

			//resolve quem foi o assassino
			if (causamortis == -1) {
				//o defunto morreu por degenera��o
				assassino = J[i].AtacanteDegen;
			} else {
				assassino = causamortis;
			}

			//manda mensagens e contabiliza morte para o assassino
			//TO DO: Broadcast (mensagem)
			if (assassino == -1) {
				MsgDef(msg.format("%s morreu sozinho!", J[i].Nome));
			} else if (assassino == i) {
				MsgDef(msg.format("%s se matou!", J[i].Nome));
			} else {
				J[assassino].Kills ++;

				static char *msgpad1 = "%s foi apagado por %s!";
				static char *msgpad2 = "%s foi queimado por %s!";
				if (rnd() < 0.5) {
					MsgDef(msg.format(msgpad1, J[i].Nome, J[assassino].Nome));
				} else {
					MsgDef(msg.format(msgpad2, J[i].Nome, J[assassino].Nome));
				}
			}

			//explode o dinamitador
			if (PodPodeExplodirDinamite(i) && !J[i].GetFlag(DinaExplodiu)) {
				J[i].SetFlag(DinaExplodiu, true);
				MsgDef(msg.format("Booooom!!\nO mapa do estado vai ter que ser alterada por causa da explox�o de um dinamitador!"));
				//TO DO: Grande Explosao
				if (TestaMortes(i)) return true;
				//BUG: o testa mortes acima pode roubar alguma morte do cara que matou o dinamitador
			}

			//ressucitamentos
			if (PodSubstituiBot(i)) {
				//TO DO: procura um Bot para substituir
			} else if (PodRessuscita(i)) {
				if (rnd() < 0.67) {
					//TO DO: ressucita o Jason
				}
			}

			if (J[i].GetFlag(Morto)) {
				//NET: se o jogador morto estiver em outro computador, ele deve poder ver seu replay imediatamente

				J[i].SetFlag(MorreuSemVerRep, true);
			}
		}
	}

	short timevencedor;
	if (FimDeJogo(timevencedor)) {
		//GAME OVER

		//Mostrar o replay para todos os jogadores que ainda n�o viram seu replay
		int ultimo = vz;
		vz = J[ultimo].idProx;
		while (vz != ultimo) {
			//Testa se o cara viu algum replay de algu�m
			bool viualguem = false;
			int op = J[vz].idProx;
			while (op != vz) {
				if (J[op].Replay.ViuAlgo(vz)) {
					viualguem = true;
					break;
				}
				op = J[op].idProx;
			}

			if (viualguem) {
				//Chama o cara
				HUDMostra(false);
				FXString msg;
				Msg(MBOX_OK, msg.format("Venha %s", J[vz].Nome));
				HUDMostra(true);

				pMapa = J[vz].Fog;
				J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);

				//Mostra o replay
				MostraReplay();
			}

			vz = J[vz].idProx;
		}

		if (timevencedor == -1) {
			//todos morreram: empate
			MsgDef("Ningu�m venceu!! Todos moreram!!");
		} else {
			//algu�m sobreviveu: temos um vencedor
			//TO DO: mostrar uma mensagem dizendo quem ganhou (o time todo)
		}

		FXuint resp = Msg(MBOX_YES_NO, "Deseja salvar esta partida nas estat�sticas?");
		if (resp == MBOX_CLICKED_YES) {
			//TO DO: salvar estat�sticas
		}

		Reinicia();

		return true;
	} else {
		return false;
	}
}

bool cJogo::FimDeJogo(short &timevencedor)
{
	timevencedor = -1;
	for (short i=0; i<numjogs; i++) {
		if (!J[i].GetFlag(Morto) && !J[i].Bot) {
			if (J[i].Injetor != -1) {
				//n�o acaba enquanto existir algu�m com alien na barriga
				return false;
			} else if (timevencedor == -1) {
				timevencedor = J[i].Time;
			} else if (timevencedor != J[i].Time) {
				//encontrou dois caras vivos de times diferentes => niguem ganhou ainda
				return false;
			}
		}
	}
	return true; //GAME OVER
}

void cJogo::MostraReplay()
{
	int op = J[vz].idProx;

	while (op != vz) {
		if (J[op].Replay.ViuAlgo(vz)) {
			FXString msg;
			FXuint resp = Msg(MBOX_YES_NO, msg.format("Voc� viu o %s fora da sua vez!\nQuer ver o replay?", J[J[op].idImit].Nome));
			if (resp == MBOX_CLICKED_YES) {
				MudaEstado(EST_VENDOREPLAY);

				//TEMP: por enquanto vou sempre mostrar o replay do pr�ximo cara (isso pode gerar bugs que n�o ir�o existir)

				bool viu = false;
				
				//Come�a a simular o replay do op
				sim.Inicia(vz, op, &J[op].Replay);

				do {
					if (viu) {
						J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z, 
											 sim.x, sim.y, sim.z);
						//drawScene(0);
						//Espera(400);
						pwinMain->drawScene();
					}
				} while (sim.Avanca(viu));
				//Fim da simula��o do replay do op

				MudaEstado(EST_JOGANDO);
			}
		}
		op = J[op].idProx;
	}
}

void cJogo::PassaVez()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	while (J[vz].T > 0) {
		GastaTempo(Min(J[vz].T, 5));
		if (TestaEncontro(false)) return; //se viu algu�m desiste de passar a vez
		Reescreve();
		drawScene(0);
	}

	if (Console) ToggleConsole();

	ApagaConsole();

	//zera vari�veis da rodada (do jogo)
	TempoParado = false;
	enuComando = CmdNada;

	//TO DO: percorrer o mapa a procura de granadas e bombas-rel�gio (assim como corpos de predador)

	//if (TestaMortes(????)) return;

	J[vz].HtAnt = J[vz].Ht;

	//procura o pr�ximo vivo
	do {
		do {
			//vz++;
			//if (vz >= numjogs) vz = 0;
			vz = J[vz].idProx;

			//TO DO: fazer o sangue alien abrir buracos
			//Apaga fuma�as, fogos e fecha portas
			double chance = pow(0.4, numjogs);
			for (int ix = 0; ix < Mapa.dimX; ix ++)
			for (int iy = 0; iy < Mapa.dimY; iy ++)
			for (int iz = 0; iz < Mapa.dimZ; iz ++) {
				if (rnd() < chance) {
					cCont cont = Mapa.Cont(ix, iy, iz, cMapa::NAO_CHAO);

					if (cont.EhFumaca()) {
						cont.SetParedeNula();
						Mapa.SetCont(ix, iy, iz, cont);
					} else if (cont.EhFogo()) {
						cont.SetFumaca();
						Mapa.SetCont(ix, iy, iz, cont);
					} else if (cont.EhPortaAberta()) {
						bool porta_livre = true;
						for (short j=0; j<numjogs; j++)
						if (!J[j].GetFlag(Morto)) {//a porta n�o pode fechar em cima dos jogadores
							//TO DO (OR !): a porta deve ou n�o fechar em cima de holografias
							if (ix == J[j].x
							&&  iy == J[j].y
							&&  iz == J[j].z) {
								porta_livre = false;
								break;
							}
						}
						if (porta_livre) {
							cont.FechaPorta();
							Mapa.SetCont(ix, iy, iz, cont);
						}
					}
				}
			}

			//TO DO: diminuir a contagem da auto destrui��o do predador, isto fica aqui pq a contagem continua mesmo com o predador morto (obs.: seu corpo pode estar sendo carregado)

			//Mostra replay aos mortos
			if (J[vz].GetFlag(Morto) && J[vz].GetFlag(MorreuSemVerRep)) {
				J[vz].SetFlag(MorreuSemVerRep, false);

				//garante que o replay dos mortos esteja apagado
				J[vz].ZeraReplay();

				//Testa se o cara viu algum replay de algu�m
				bool viualguem = false;
				int op = J[vz].idProx;
				while (op != vz) {
					if (J[op].Replay.ViuAlgo(vz)) {
						viualguem = true;
						break;
					}
					op = J[op].idProx;
				}

				if (viualguem) {
					//Chama o cara
					HUDMostra(false);
					FXString msg;
					Msg(MBOX_OK, msg.format("Venha %s", J[vz].Nome));
					HUDMostra(true);

					pMapa = J[vz].Fog;
					J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);

					//Mostra o replay
					MostraReplay();
				}
			}

		} while (J[vz].GetFlag(Morto)); //obs: se todos estiverem mortos, n�o sai mais daqui

		//Ovo do Alien
		if (J[vz].Incubacao > 0) {
			J[vz].Incubacao --;
			if (J[vz].Incubacao == 0) {
				//char* txt = new char[strlen(J[vz].Nome) + 39];
				//sprintf(txt, "O %s morreu e se transformou em um alien!", J[vz].Nome);
				//MsgDef(txt);
				FXString msg;
				MsgDef(msg.format("O %s morreu e se transformou em um alien!", J[vz].Nome));
				//TO DO: Broadcast(msg);

				J[vz].PodSoltaCorpo(&Mapa);

				short incubador = J[vz].Injetor;

				//tranforma o alien pequeno no incubador, apenas para as estat�sticas
				short auxshort;
				Troca(J[vz].Poder, J[incubador].Poder, auxshort);
				Troca(J[vz].Kills, J[incubador].Kills, auxshort);
				Troca(J[vz].Time, J[incubador].Time, auxshort);
				//troca os nomes
				/*
				char *auxstr = NULL;
				strcpy(auxstr,				J[vz].Nome);
				strcpy(J[vz].Nome,			J[incubador].Nome);
				strcpy(J[incubador].Nome,	auxstr);
				*/
				FXString auxstr;
				auxstr			  = J[vz].Nome;
				J[vz].Nome		  = J[incubador].Nome;
				J[incubador].Nome = auxstr;

				J[vz].Kills ++;
				J[vz].Poder = pAlienGrande;
				J[vz].AcionaPoder(Armas, Municoes);

				//TO DO: soltar todo o equipamento

				J[vz].Posicao = posNormal;
			}
		}

		//a degenera��o m�xima � de 60%
		J[vz].Reg = Max(0.4f, J[vz].Reg);
		//alguns poderes n�o sofrem regenera�ao/degenera��o
		if (PodSemRegeneracao(vz)) J[vz].Reg = 1;
		//zerg enterrado tem regenera��o maior
		if (J[vz].Posicao == posEnterrado) J[vz].Reg *= 1.2f;
		//aplica reg. obs: � alat�rio, mas n�o muda de <1 p/ >1 ou vice-versa
		//MODIF.: no original era Aleat�rio e n�o superrnd, mas acho que estava errado
		J[vz].Ht = (J[vz].Ht + 0.5) * exp(superrnd(log(J[vz].Reg))) - 0.5;
		//n�o deixa a resist�ncia passar do m�ximo
		J[vz].Ht = Min(J[vz].HtMax, J[vz].Ht);
		if (J[vz].Ht <= 0) {
			//J[vz].Ht = -666;
			if (TestaMortes(-1)) return;
		}		
	} while (J[vz].GetFlag(Morto));

	//O novo valor de Reg � igual � m�dia geom�trica entre o valor antigo e RegNatural
	//Deste modo, Reg tende a RegNatural (se � houver influ�ncias externas, � claro)
	J[vz].Reg = sqrt(J[vz].Reg * J[vz].RegNat);

	//TO DO: sangra

	//Recarrega armas que s� d�o um tiro por rodada
	//if (J[vz].PossuiArma[13]) J[vz].Mun[13] = Armas[13].Municao;
	J[vz].objMaoDir->RecarregaArmaDeUmTiroPorRodada(Armas);
	J[vz].objMaoEsq->RecarregaArmaDeUmTiroPorRodada(Armas);
	J[vz].Equip.RecarregaArmasDeUmTiroPorRodada(Armas);
	J[vz].Skills.RecarregaArmasDeUmTiroPorRodada(Armas);

	//Recarrega mana
	if (PodUsaMagiasArcanas(vz)) J[vz].ManaFe += (short)floor(10 - (J[vz].ManaFe / 20.));

	//TO DO: Recarrega parasitas, canh�o, mana/f�, gasta capacete

	//TO DO: NET: se o computador do pr�ximo jogador for diferente deste ent�o enviar a vez sen�o:

	//TO DO: quando o ethereal controla, o duelo deve chamar o ethereal

	RecebeVez();
}

void cJogo::RecebeVez()
{
	if (!NaoAnimando) return; //est� mostrando anima��o => GUI bloqueada

	//Esconde Heads Up Display
	HUDMostra(false);

	//char* txt = new char[strlen(J[vz].Nome) + 7];
	//sprintf(txt, "Venha %s", J[vz].Nome);
	//Msg(txt, MSG_OKONLY);
	FXString msg;
	Msg(MBOX_OK, msg.format("Venha %s", J[vz].Nome));

	//zera vari�veis da rodada (do jogador)
	J[vz].ZeraReplay();
	J[vz].ZeraFogBit();
	//evita de avisar os jogadores que o cara sempre v�
	if (PodSempreVeBotsNeutros(vz)) {
		//Agente do Matrix sempre v� os bots neutros, al�m dos caras do time dele
		for (int i=0; i<numjogs; i++) JaVi[i] = (J[i].Time == J[vz].Time || (J[i].Bot && J[i].Time == -1)) ? 1 : 0;
	} else {
		for (int i=0; i<numjogs; i++) JaVi[i] = (J[i].Time == J[vz].Time) ? 1 : 0;
	}
	for (int i=0; i<numjogs; i++) JaViHol[i] = false;
	enuComando = CmdNada;
	contlinhatiro = 0;
	contsombratiro = 0;
	J[vz].T = J[vz].TProxRodada;
	J[vz].TProxRodada = J[vz].TMax;

	//Mostra HUD j� com valores corretos
	Reescreve();
	HUDMostra(true);

	//Grava uma primeira foto
	J[vz].Replay.GravaFoto(AcaoMove, num(J[vz].x), num(J[vz].y), num(J[vz].z));

	//Ajusta mapa a ser exibido
	pMapa = J[vz].Fog;
	J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);
	AtualizaFog();

	//avisa se o cara levou dano na rodada anterior
	if (J[vz].HtAnt > J[vz].Ht) MsgDef("Voc� sofreu dano fora da sua vez!");
	J[vz].HtAnt = J[vz].Ht;
	//avisa se o cara est� com alta degenera��o
	if (J[vz].Reg < 0.95) MsgDef("Voc� est�sangrando!");
	//TO DO: testar e avisar se o capacete acabou

	MostraReplay();

	//centra a camera de volta no jogador ap�s mostrar o replay
	J[vz].Fog->CentraCam(J[vz].x, J[vz].y, J[vz].z);

	//Testa encontro no in�cio da vez (� necess�rio um certo dt falso para que se possa ver algu�m)
	J[vz].TAnt = J[vz].T + 20;
	TestaEncontro(true);

	if (PodRegeneracaoT1000(vz)) {
		//se o tempo para regenerar tudo o que ele apanhou for maior do que
		// o tempo que ele tem ent�o, o T-1000 � obrigado a regenerar
		if (J[vz].HtMax - J[vz].HtMax > J[vz].T / PodT_1000_TempoParaReparar) {
			MsgDef("Voc� est� muito mal!\nSer� obrigado a se reparar!");
			J[vz].Ht += J[vz].T / PodT_1000_TempoParaReparar;
			J[vz].T = 0;
		}
	}
}

void cJogo::SorteiaOrdem()
{
	int faltam, p1, p2;
	vz = rndi(0, numjogs-1);
	faltam = numjogs - 1;
	p1 = vz;
	while(faltam)
	{
		p2 = rndi(0, numjogs-1);
		if ((p2 != p1) && (J[p2].idProx == -1)) {
			J[p1].idProx = p2;
			faltam--;
			if (!faltam) {
				J[p2].idProx = vz;
			} else {
				p1 = p2;
			}
		}
	}
}

void cJogo::Posiciona()
{
	int i, j, c;
	bool p;
	double d;
	double dmin = pow((double)(Mapa.dimX * Mapa.dimY * Mapa.dimZ), 0.3333) / (double)numjogs;

	c = 0;
	for (i=0; i<numjogs; i++) {
		do
		{
			//se estiver demorando demais -> diminuir dmin
			c++;
			if (c >	10000) {
				dmin *= 0.9f;
				c = 0;
			}

			//sortear uma posi��o
			J[i].x = rndi(0, Mapa.dimX-1);
			J[i].y = rndi(0, Mapa.dimY-1);
			J[i].z = rndi(0, Mapa.dimZ-1);

			//testa se j� h� algu�m muito perto da posi��o sorteada
			p = false;
			for (j=0; j<i; j++) {
				d = Distancia3D(J[i].x, J[i].y, J[i].z,
								J[j].x, J[j].y, J[j].z);
				if (d < dmin) p = true;
			}
			p |= cCont(Mapa.Cont(J[i].x, J[i].y, J[i].z)).EhParede();
			if (J[i].z > 0) p |= cChao(Mapa.Cont(J[i].x, J[i].y, J[i].z, cMapa::CHAO)).EhChaoBuracoInvOuNao();
		} while (p);// || EhParede(Mapa.Cont(J[i].x, J[i].y, J[i].z)) ||
					 // EhChaoBuracoInvOuNao(Mapa.Cont(J[i].x, J[i].y, J[i].z, CHAO)));
	}
}

void cJogo::CriaFogs()
{
	for (int i=0; i<numjogs; i++)
	{
		//J[i].Fog = new cMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ, cCont::CONT_FOG_NUNCA_VIU);
		J[i].Fog = new cMapa(&Mapa, cCont::CONT_FOG_NUNCA_VIU);
		J[i].FogBit = new cMapa(Mapa.dimX, Mapa.dimY, Mapa.dimZ);

		//TO DO (|| n�o): n�o precisa criar fogs separados para os aliados
	}
}