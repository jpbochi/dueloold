
#include "conteudo.h"
#include "geral.h"
#include "declarac.h"

#include <math.h>

#include "mapa.h"

//###################################################
//fun��es espec�ficas ###############################

char cAfin::CriaPelosVizinhos(int contvizinhos, bool vizinho[6])
{
	if (contvizinhos == 1) {
		for (char i=0; i<6; i++) {
			if (vizinho[i]) {
				return cAfin::AFIN_ATRAVES + i;
				break;
			}
		}
	} else if (contvizinhos == 2) {
		if (vizinho[0] && vizinho[3]) {
			return cAfin::AFIN_ATRAVES;
		} else if (vizinho[1] && vizinho[4]) {
			return cAfin::AFIN_ATRAVES + 1;
		} else if (vizinho[2] && vizinho[5]) {
			return cAfin::AFIN_ATRAVES + 2;

		} else if (vizinho[5] && vizinho[1]) {
			return cAfin::AFIN_METADE;
		} else if (vizinho[0] && vizinho[2]) {
			return cAfin::AFIN_METADE + 1;
		} else if (vizinho[1] && vizinho[3]) {
			return cAfin::AFIN_METADE + 2;
		} else if (vizinho[2] && vizinho[4]) {
			return cAfin::AFIN_METADE + 3;
		} else if (vizinho[3] && vizinho[5]) {
			return cAfin::AFIN_METADE + 4;
		} else if (vizinho[4] && vizinho[0]) {
			return cAfin::AFIN_METADE + 5;
		}
	} else if (contvizinhos == 3) {
		if (vizinho[5] && vizinho[0] && vizinho[1]) {
			return cAfin::AFIN_METADE;
		} else if (vizinho[0] && vizinho[1] && vizinho[2]) {
			return cAfin::AFIN_METADE + 1;
		} else if (vizinho[1] && vizinho[2] && vizinho[3]) {
			return cAfin::AFIN_METADE + 2;
		} else if (vizinho[2] && vizinho[3] && vizinho[4]) {
			return cAfin::AFIN_METADE + 3;
		} else if (vizinho[3] && vizinho[4] && vizinho[5]) {
			return cAfin::AFIN_METADE + 4;
		} else if (vizinho[4] && vizinho[5] && vizinho[0]) {
			return cAfin::AFIN_METADE + 5;
		}
	}
	return cAfin::AFIN_MASK_DEFAULT;
}

bool cAfin::EhAfinInteira(char c)	{return (c & AFIN_MASK_TIPO) == AFIN_INTEIRO;}
bool cAfin::EhAfinAtraves(char c)	{return (c & AFIN_MASK_TIPO) == AFIN_ATRAVES;}
bool cAfin::EhAfinMetade(char c)	{return (c & AFIN_MASK_TIPO) == AFIN_METADE;}
float cAfin::AfinRotacao(char c)	{return 60 * (float)(c & AFIN_MASK_ROT);}

bool cCont::ConverteConteudo(cCont &novocont, char &chao)
{
	//p�e o chao default
	chao = cChao::CHAO_DEFAULT;
	switch (c) {
		case 0: novocont = CONT_PAREDE_NULA;break;
		case 1: novocont = CONT_PAREDE_MADEIRA	| CONT_ALTURA_3;break;
		case 5: novocont = CONT_PAREDE_TIJOLO	| CONT_ALTURA_3;break;
		case 9:
		case 10:novocont = CONT_PAREDE_TIJOLO	| CONT_FLAG_PORTA | CONT_PORTA_SECRETA;break;
		case 22:novocont = CONT_PAREDE_FUMACA	| CONT_ALTURA_0;break;
		case 31:novocont = CONT_PAREDE_FOGO		| CONT_ALTURA_0;break;
		case 32:novocont = CONT_PAREDE_LATA		| CONT_ALTURA_0;break;
		case 34:novocont = CONT_PAREDE_VIDRO	| CONT_ALTURA_3;/*chao = CHAO_VIDRO;*/break;
		case 35:novocont = CONT_PAREDE_TELETRANS| CONT_ALTURA_3;break;
		case 36:novocont = CONT_PAREDE_ESCURO	| CONT_ALTURA_3;break;
		case 40:novocont = CONT_PAREDE_FOGO_PERM| CONT_ALTURA_0;break;
		case 44:novocont = CONT_PAREDE_GRADE	| CONT_ALTURA_3;break;
		case 47:novocont = CONT_PAREDE_PEDRA	| CONT_ALTURA_3;chao = cChao::CHAO_PEDRA;break;
		case 48:novocont = CONT_PAREDE_PEDRA	| CONT_FLAG_PORTA | CONT_PORTA_TRANCADA;chao = cChao::CHAO_PEDRA;break;
		case 50:novocont = CONT_PAREDE_PEDRA	| CONT_FLAG_PORTA | CONT_PORTA_TRANCADA | CONT_FLAG_PORTA_ABERTA;chao = cChao::CHAO_PEDRA;break;

		case 20: //elevador
		case 52: //   ||
		case 53: //   ||
			novocont = CONT_PAREDE_NULA; chao = cChao::CHAO_ELEVADOR; break;
		case 24: //buraco invis.
		case 25: //  ||     ||
			novocont = CONT_PAREDE_NULA; chao = cChao::CHAO_BURACO_INVISIVEL; break;
		case 26: //buraco vis.
			novocont = CONT_PAREDE_NULA; chao = cChao::CHAO_BURACO; break;
			/*novocont = CONT_PAREDE_NULA;
			switch (cont) {
				case 20:
				case 52:
				case 53: chao = CHAO_ELEVADOR; break;
				case 24:
				case 25: chao = CHAO_BURACO_INVISIVEL; break;
				case 26: chao = CHAO_BURACO; break;
			}
		break;*/
		default:
			novocont = CONT_PAREDE_NULA;
			return false;
		break;
	}
	return true;
}

//TO DO:carregar as texturas em posi��es que possibilitem otimizar as duas fun��es abaixo
int cCont::ConteudoToTexID(char c)
{
	char tipoparede = c & CONT_MASK_TIPO_PAREDE;
	char flagporta = c & CONT_FLAG_PORTA;
	if (flagporta && (c & CONT_FLAG_PORTA_ABERTA)) {
		return 0;
	} else {
		switch (tipoparede) {
			case CONT_PAREDE_NULA:		return 0;
			case CONT_PAREDE_MADEIRA:	return 1;
			case CONT_PAREDE_TIJOLO:
				if (flagporta)			return 9;
				else					return 5;
			case CONT_PAREDE_PEDRA:
				if (flagporta)			return 48;
				else					return 47;
			case CONT_PAREDE_VIDRO:		return 34;
			case CONT_PAREDE_GRADE:		return 44;
			case CONT_PAREDE_FUMACA:	return 22;
			case CONT_PAREDE_FOGO:		return 31;
			case CONT_PAREDE_FOGO_PERM:	return 40;
			case CONT_PAREDE_ESCURO:	return 36;
			case CONT_PAREDE_LATA:		return 32;
			case CONT_PAREDE_TELETRANS:	return 35;
			default:					return c;
		}
	}
}

int cChao::ChaoLadoToTexID(char c)
{
	char tipochao = c & CHAO_MASK_TIPO;
	switch (tipochao) {
		case CHAO_BURACO:			return 0;
		case CHAO_ELEVADOR:			return 34;//dos lados do elevador parece vidro
		case CHAO_MADEIRA:			return 1;
		case CHAO_TIJOLO:			return 5;
		case CHAO_PEDRA:			return 47;
		case CHAO_VIDRO:			return 34;
		case CHAO_GRADE:			return 44;
		case CHAO_BURACO_INVISIVEL:	return 24;
		default:					return 0;
	}
}
int cChao::ChaoCimaToTexID(char c)
{
	//return 0;
	char tipochao = c & CHAO_MASK_TIPO;
	switch (tipochao) {
		case CHAO_BURACO:			return 0;
		case CHAO_ELEVADOR:			return 20;
		case CHAO_MADEIRA:			return 0; //1;
		case CHAO_TIJOLO:			return 0; //5;
		case CHAO_PEDRA:			return 0; //47;
		case CHAO_VIDRO:			return 34;
		case CHAO_GRADE:			return 0; //44;
		case CHAO_BURACO_INVISIVEL:	return 24;
		default:					return 0;
	}
}

char cCont::TipoParedeToTipoChao(char c)
{
	//return 255;
	char tipoparede = c & CONT_MASK_TIPO_PAREDE;
	switch (tipoparede) {
		case CONT_PAREDE_NULA:		return cChao::CHAO_DEFAULT;
		case CONT_PAREDE_MADEIRA:	return cChao::CHAO_MADEIRA;
		case CONT_PAREDE_TIJOLO:	return cChao::CHAO_TIJOLO;
		case CONT_PAREDE_PEDRA:		return cChao::CHAO_PEDRA;
		case CONT_PAREDE_VIDRO:		return cChao::CHAO_VIDRO;
		case CONT_PAREDE_GRADE:		return cChao::CHAO_DEFAULT;//CHAO_GRADE;
		default:					return cChao::CHAO_DEFAULT;
	}
}

//conte�do
bool cCont::EhForaDoMapa(char c)	{return c == CONT_FORA_DO_MAPA;}
bool cCont::EhFogNuncaViu(char c)	{return c == CONT_FOG_NUNCA_VIU;}

bool cCont::EhParede(char c)		{return (c & CONT_MASK_TIPO_PAREDE) != CONT_PAREDE_NULA && !EhPortaAberta(c);}
bool cCont::EhParedeSolida(char c)
{
	if (EhPortaAberta(c)) return false;
	c &= CONT_MASK_TIPO_PAREDE;
	return c == CONT_PAREDE_MADEIRA ||
		   c == CONT_PAREDE_TIJOLO ||
		   c == CONT_PAREDE_PEDRA ||
		   c == CONT_PAREDE_VIDRO ||
		   c == CONT_PAREDE_GRADE;
}
bool cCont::EhParedeDura(char c)
{
	if (EhPortaAberta(c)) return false;
	c &= CONT_MASK_TIPO_PAREDE;
	return c == CONT_PAREDE_TIJOLO ||
		   c == CONT_PAREDE_PEDRA ||
		   c == CONT_PAREDE_VIDRO;
}
bool cCont::EhEsfumacado(char c)
{
	c &= CONT_MASK_TIPO_PAREDE;
	return c == CONT_PAREDE_FUMACA ||
		   c == CONT_PAREDE_FOGO ||
		   c == CONT_PAREDE_FOGO_PERM;
}
bool cCont::EhTransparente(char c)
{
	c &= CONT_MASK_TIPO_PAREDE;
	return c == CONT_PAREDE_FUMACA ||
		   c == CONT_PAREDE_FOGO ||
		   c == CONT_PAREDE_FOGO_PERM ||
		   c == CONT_PAREDE_VIDRO ||
		   c == CONT_PAREDE_GRADE ||
		   c == CONT_PAREDE_TELETRANS;
}

bool cCont::EhParedeMadeira(char c)		{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_MADEIRA && !EhPortaAberta(c);}
bool cCont::EhParedeTijolo(char c)		{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_TIJOLO && !EhPortaAberta(c);}
bool cCont::EhParedePedra(char c)		{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_PEDRA && !EhPortaAberta(c);}
bool cCont::EhParedeVidro(char c)		{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_VIDRO && !EhPortaAberta(c);}
bool cCont::EhGrade(char c)				{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_GRADE && !EhPortaAberta(c);}
bool cCont::EhFumaca(char c)			{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_FUMACA && !EhPortaAberta(c);}
bool cCont::EhFogo(char c)				{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_FOGO && !EhPortaAberta(c);}
bool cCont::EhFogoPermanente(char  c)	{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_FOGO_PERM && !EhPortaAberta(c);}
bool cCont::EhEscuro(char c)			{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_ESCURO && !EhPortaAberta(c);}
bool cCont::EhLata(char c)				{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_LATA && !EhPortaAberta(c);}
bool cCont::EhTeletransporte(char c)	{return (c & CONT_MASK_TIPO_PAREDE) == CONT_PAREDE_TELETRANS && !EhPortaAberta(c);}

bool cCont::EhPorta(char c)			{return (c & CONT_FLAG_PORTA) ? true : false;}
bool cCont::EhPortaAberta(char c)
{
	return (c & CONT_FLAG_PORTA) &&
		   (c & CONT_FLAG_PORTA_ABERTA);
}
bool cCont::EhPortaNormal(char c)
{
	return (c & CONT_FLAG_PORTA) &&
		   !(c & CONT_FLAG_PORTA_ABERTA) &&
		   ((c & CONT_MASK_TIPO_PORTA) == CONT_PORTA_NORMAL);
}
bool cCont::EhPortaTrancada(char c)
{
	return (c & CONT_FLAG_PORTA) &&
		   !(c & CONT_FLAG_PORTA_ABERTA) &&
		   ((c & CONT_MASK_TIPO_PORTA) == CONT_PORTA_TRANCADA);
}
bool cCont::EhPortaSecreta(char c)
{
	return (c & CONT_FLAG_PORTA) &&
		   !(c & CONT_FLAG_PORTA_ABERTA) &&
		   ((c & CONT_MASK_TIPO_PORTA) == CONT_PORTA_SECRETA);
}

//altera��o de conte�do
void cCont::SetParedeNula(char &c)
{
	c &= ~CONT_MASK_TIPO_PAREDE;
}
void cCont::SetFumaca(char &c)
{
	c &= ~CONT_MASK_TIPO_PAREDE;
	c |= CONT_PAREDE_FUMACA;
}
void cCont::SetFogo(char &c)
{
	c &= ~CONT_MASK_TIPO_PAREDE;
	c |= CONT_PAREDE_FOGO;
}
void cCont::SetFogoPermanente(char &c)
{
	c &= ~CONT_MASK_TIPO_PAREDE;
	c |= CONT_PAREDE_FOGO_PERM;
}

void cCont::AbrePorta(char &c)
{
	if (EhPorta(c)) c |= CONT_FLAG_PORTA_ABERTA;
}
void cCont::FechaPorta(char &c)
{
	if (EhPorta(c)) c &= ~CONT_FLAG_PORTA_ABERTA;
}

//ch�o
bool cChao::EhForaDoMapa(char c)	{return c == CHAO_FORA_DO_MAPA;}
bool cChao::EhFogNuncaViu(char c)	{return c == CHAO_FOG_NUNCA_VIU;}

bool cChao::EhChaoDuro(char c)
{
	c &= CHAO_MASK_TIPO;
	return (c == CHAO_TIJOLO) ||
		   (c == CHAO_PEDRA) ||
		   (c == CHAO_VIDRO);
}
bool cChao::EhChaoTransparente(char c)
{
	c &= CHAO_MASK_TIPO;
	return (c == CHAO_BURACO) ||
		   (c == CHAO_ELEVADOR) ||
		   (c == CHAO_VIDRO) ||
		   (c == CHAO_GRADE);
}
bool cChao::EhChaoBuracoInvOuNao(char c)
{
	c &= CHAO_MASK_TIPO;
	return (c == CHAO_BURACO) ||
		   (c == CHAO_BURACO_INVISIVEL);
}

bool cChao::EhChaoBuraco(char c)	{return (c & CHAO_MASK_TIPO) == CHAO_BURACO;}
bool cChao::EhChaoElevador(char c)	{return (c & CHAO_MASK_TIPO) == CHAO_ELEVADOR;}
bool cChao::EhChaoMadeira(char c)	{return (c & CHAO_MASK_TIPO) == CHAO_MADEIRA;}
bool cChao::EhChaoTijolo(char c)	{return (c & CHAO_MASK_TIPO) == CHAO_TIJOLO;}
bool cChao::EhChaoPedra(char c)		{return (c & CHAO_MASK_TIPO) == CHAO_PEDRA;}
bool cChao::EhChaoVidro(char c)		{return (c & CHAO_MASK_TIPO) == CHAO_VIDRO;}
bool cChao::EhChaoGrade(char c)		{return (c & CHAO_MASK_TIPO) == CHAO_GRADE;}
bool cChao::EhChaoBuracoInv(char c)	{return (c & CHAO_MASK_TIPO) == CHAO_BURACO_INVISIVEL;}

void cChao::SetChaoBuraco(char &c)
{
	c &= ~CHAO_MASK_TIPO;
	c |= CHAO_BURACO;
}
void cChao::SetChaoBuracoInv(char &c)
{
	c &= ~CHAO_MASK_TIPO;
	c |= CHAO_BURACO_INVISIVEL;
}
void cChao::SetChaoPedra(char &c)
{
	c &= ~CHAO_MASK_TIPO;
	c |= CHAO_PEDRA;
}
