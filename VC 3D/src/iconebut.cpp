
#include "iconebut.h"
#include "objetos.h"
#include "texturas.h"
#include "jogo.h"

cIconeBut::cIconeBut(FXApp *app, cJogo *_pJogo)
{
	pJogo = _pJogo;
	CarregaIcones(app);
}

cIconeBut::~cIconeBut()
{
	//evita apagar o jogo
	pJogo = NULL;
	
	//apaga icones;
	for (int i=0; i<NUM_ICOS; i++) {
		if (icones[i]) delete icones[i];
		icones[i] = NULL;
	}
}

void cIconeBut::CarregaIcones(FXApp *app)
{
	//typedef FXIcon * FXIconPtr;
	//icones = new FXIconPtr[OBJ_Moto + 1];

	/*
	for (int i=0; i<NUM_ICOS; i++) icones[i] = 0;
	/*///Enjambrção temporária!
	icones[0] = Icone(app, "imagens\\hex5_32x32.bmp");	//hex2_26x23.bmp");
	for (int i=1; i<NUM_ICOS; i++) icones[i] = icones[0];
	/**/

	icones[icoVazio] =				Icone(app, "imagens\\hex4_32x32.bmp");	//hex_26x23.bmp");
	icones[icoDesconhecido] =		Icone(app, "imagens\\hex5_32x32.bmp");	//hex2_26x23.bmp");
	icones[icoPistola] =			Icone(app, "imagens\\23-Revolver.BMP");
	icones[icoPistolaMun] =			Icone(app, "imagens\\23-Cartucho.bmp");
	icones[icoEspingarda] =			Icone(app, "imagens\\03-Esping.bmp");
	icones[icoEspingardaMun] =		Icone(app, "imagens\\03-Esping_M.bmp");
	icones[icoMetralhadora] =		Icone(app, "imagens\\04-Metralha.bmp");
	icones[icoMetralhadoraMun] =	Icone(app, "imagens\\04-Metralha_M.bmp");
	icones[icoLancaFog] =			Icone(app, "imagens\\08-lancfog2.BMP");
	icones[icoLancaFogMun] =		Icone(app, "imagens\\08-lancfog2_M.BMP");
	icones[icoRifle] =				Icone(app, "imagens\\11-Rifle.BMP");
	icones[icoRifleMun] =			Icone(app, "imagens\\11-Rifle_M.BMP");
	icones[icoDegen] =				Icone(app, "imagens\\12-Degenera.BMP");
	icones[icoDegenMun] =			Icone(app, "imagens\\12-Degenera_m.BMP");
	icones[icoDisco] =				Icone(app, "imagens\\17-Disco.BMP");	//icoDiscoMun
	//icones[icoTriLaser,
	//icoTriLaserMun,
	icones[icoMiniGun] =			Icone(app, "imagens\\19-AutoCann.BMP");
	icones[icoMiniGunMun] =			Icone(app, "imagens\\19-AutoCann_M.BMP");
	icones[icoLancaChamas] =		Icone(app, "imagens\\21-LancaCha.BMP");
	icones[icoLancaChamasMun] =		Icone(app, "imagens\\21-LancaCha_M.BMP");
	icones[icoRicochet] =			Icone(app, "imagens\\27-Ricochet.BMP");
	icones[icoRicochetMun] =		Icone(app, "imagens\\27-Ricochet_M.BMP");
	//icones[icoRoboArma,
	//icoRoboArmaMun
	icones[icoParalisante] =		Icone(app, "imagens\\37-LerdArma.BMP");
	icones[icoParalisanteMun] =		Icone(app, "imagens\\37-LerdArma_M.BMP");
	//icones[icoBaforada,
	//icoBaforadaMun
	icones[icoTeletrans] =			Icone(app, "imagens\\38-TeleArma.BMP");
	icones[icoTeletransMun] =		Icone(app, "imagens\\38-TeleArma_M.BMP");
	icones[icoBlaster] =			Icone(app, "imagens\\41-Blaster.BMP");
	icones[icoBlasterMun] =			Icone(app, "imagens\\41-Blaster_M.BMP");
	icones[icoMachado] =			Icone(app, "imagens2\\battle_axe.bmp");
	//icones[icoEspinhos,
	//icoEspinhosMun
	icones[icoMG34] =				Icone(app, "imagens\\55-Mg42.BMP");
	icones[icoMG34Mun] =			Icone(app, "imagens\\55-Mg42_M.BMP");
	icones[icoCanhao] =				Icone(app, "imagens2\\canhao.bmp");
	//icones[icoCanhaoMunHE,
	//icones[icoCanhaoMunAP,
	icones[icoBolaDeFogo] =			Icone(app, "imagens2\\boladefogo.bmp");	//icoBolaDeFogoMun
	icones[icoSabreDeLuz] =			Icone(app, "imagens2\\lightsaber-green_32.bmp");
	icones[icoShuriken] =			Icone(app, "imagens2\\shuriken.bmp");	//icoShurikenMun
	icones[icoKatana] =				Icone(app, "imagens2\\shuriken.bmp");
	//icones[icoSoco,
	icones[icoTrabuco] =			Icone(app, "imagens\\57-Trabuco.BMP");
	icones[icoTrabucoMun] =			Icone(app, "imagens\\57-Trabuco_M.BMP");
	//icones[icoDedoDuro,
	icones[icoSnikt] =				Icone(app, "imagens2\\snickt.bmp");
	
	//### Equipamentos
	icones[icoMina] =				Icone(app, "imagens\\07-Minas2.BMP");
	//icones[icoMinaAtiv,
	icones[icoGranada] =			Icone(app, "imagens\\13-Granada.BMP");
	icones[icoGranadaAtiv] =		Icone(app, "imagens\\14-GranadaA.BMP");
	icones[icoKitMedico] =			Icone(app, "imagens\\15-Medkit2.BMP");
	icones[icoEsqueleto] =			Icone(app, "imagens\\16-Esquelet.BMP");
	icones[icoCapaceteInv] =		Icone(app, "imagens\\28-Capacete.BMP");
	icones[icoCapaceteIR] =			Icone(app, "imagens\\29-Oculos2.BMP");
	icones[icoBomba] =				Icone(app, "imagens\\30-Bomba.BMP");
	icones[icoColete] =				Icone(app, "imagens\\46-Escudo.BMP");
	icones[icoChave] =				Icone(app, "imagens\\49-Chave2.BMP");
	icones[icoHolografia] =			Icone(app, "imagens\\51-Holog.BMP");
	icones[icoDetector] =			Icone(app, "imagens\\56-Detec.BMP");
	//Skills
	icones[icoCamuflagem] =			Icone(app, "imagens2\\invisibilidade2.bmp");
	icones[icoKitDeArrombamento] =	Icone(app, "imagens2\\arrombar.bmp");
	icones[icoEnterrarse] =			Icone(app, "imagens2\\enterrarse.bmp");
	icones[icoDesarmarMinas] =		Icone(app, "imagens2\\bomba2.bmp");
	icones[icoControlePsionico] =	Icone(app, "imagens2\\mente.bmp");
	icones[icoRegT1000] =			Icone(app, "imagens2\\cruz3.bmp");
	icones[icoAutoDestruicao] =		Icone(app, "imagens2\\boom.bmp");
	icones[icoIdentificacao] =		Icone(app, "imagens2\\identificar.bmp");
	icones[icoPararTempo] =			Icone(app, "imagens2\\timestop.bmp");
	icones[icoInvisibilidade] =		Icone(app, "imagens2\\invisibilidade.bmp");
	icones[icoTeletransporte] =		Icone(app, "imagens2\\teletransporte.bmp");
	icones[icoImitar] =				Icone(app, "imagens2\\imitar2.bmp");
	icones[icoMagiaHolografia] =	Icone(app, "imagens2\\imitar.bmp"); //achar um melhor
	icones[icoDardosMisticos] =		Icone(app, "imagens2\\magicmissile.bmp");
	icones[icoRezar] =				Icone(app, "imagens2\\cruz.bmp");
	icones[icoCura] =				Icone(app, "imagens2\\curarleve.bmp");
	icones[icoAnimarMortos] =		Icone(app, "imagens2\\conjesqueleto.bmp");
	icones[icoRelampago] =			Icone(app, "imagens2\\magicmissile.bmp");
}

void cIconeBut::EsvaziaMat(FXMatrix *mat)
{
	FXButton *but;
	but = (FXButton *)mat->getFirst();
	do {
		PreencheBut(but, 0);

		but = (FXButton *)but->getNext();
	} while (but != 0);
}

void cIconeBut::PreencheMat(FXMatrix *mat, cListaObjetos *lista)
{
	if (lista != NULL && pJogo != NULL) {
		FXButton *but = (FXButton *)mat->getFirst();
		//objetos simples
		/*{
			for (int i=0; i<lista->numobjsimp; i++) {
				cObjetoSimples obj = lista->GetObjSimp(i);
				
				PreencheBut(but, new cObjeto(obj.cod));
				
				but = (FXButton *)but->getNext();
				if (but == 0) break;
			}
		}*/
		lista->ConverteObjs(pJogo->Armas, pJogo->Municoes);
		//objetos
		{
			cObjeto  *obj = lista->GetObj();
			while (but != 0 && obj != 0) {
				PreencheBut(but, obj);
				
				obj = obj->prox;
				but = (FXButton *)but->getNext();
			}
		}
		if (but == 0) fxwarning("Overflow de objetos!\nMande o programador consertar isso!");
	}
}

void cIconeBut::PreencheBut(FXButton *but, cObjeto *obj)
{
	if (obj == 0) {
		but->setIcon(icones[0]);
		cObjeto *obj_ant = (cObjeto *)but->getUserData();
		if (obj_ant != 0) {
			//if (obj_ant->dono = -1)	delete obj_ant;
			but->setUserData(0);
		}
	} else {
		but->setIcon(icones[obj->GetIco()]);
		but->setUserData(obj);
	}
}
