
#include "projetil.h"
#include "funcoes.h"
#include "declarac.h"
#include "conteudo.h"
#include <math.h>

//######################### cPontoLinha
void cPontoLinha::IniciaAngulos(double _teta, double _phi,
								double _xo, double _yo, double _zo)
{
	teta =	_teta;
	phi =	_phi;

	AjustaDW();

	Inicia(_xo, _yo, _zo);
}

void cPontoLinha::IniciaDeltas(double _dx, double _dy, double _dz,
							   double _xo, double _yo, double _zo)
{
	dx = _dx;
	dy = _dy;
	dz = _dz;
	teta = 0;
	phi = 0;

	Inicia(_xo, _yo, _zo);
}

void cPontoLinha::Inicia(double _xo, double _yo, double _zo)
{
	xo =	_xo;
	yo =	_yo;
	zo =	_zo;
	Avanca(0); //ajusta as coordenadas de hex�gono
	Valida();

	afin = 0;
	//parede_fina_perto = false;
}

void cPontoLinha::AjustaDW()
{
	dx = sin(phi) * cos(teta);
	dy = sin(phi) * sin(teta);
	dz = cos(phi);//-cos(phi);
}

void cPontoLinha::Avanca(double _w)
{
	w = _w;
	xf = xo + (w * dx);
	yf = yo + (w * dy);
	zf = zo + (w * dz);

	PontoToHex3D(xf, yf, zf, xi, yi, zi, ehchao);
	CentroHex3D(xi, yi, zi, xch, ych, zch, 0);
}

void cPontoLinha::Valida()
{
	xiant = xi;
	yiant = yi;
	ziant = zi;
	ehchaoant = ehchao;
}

short cPontoLinha::MudouHex()
{
	short ret = ((xi != xiant) << SHIFT_MUDOU_X) |
				((yi != yiant) << SHIFT_MUDOU_Y) |
				((zi != ziant) << SHIFT_MUDOU_Z) |
				((ehchao != ehchaoant) << SHIFT_MUDOU_CHAO);
	return ret;
}

bool cPontoLinha::ForaDoMapa(int dimX, int dimY, int dimZ)
{
	return (xi<0)	  || (yi<0)		|| (zi<0) || (zi == 0 && ehchao) ||
		   (xi>=dimX) || (yi>=dimY) || (zi>=dimZ);
}

void cPontoLinha::SetAfinamento(class cAfin *_afin)
{
	afin = _afin;
	parede_fina_perto = true;
}

bool cPontoLinha::AcertouParede()
{
	bool ret;
	if (afin == 0 || !parede_fina_perto) {
		ret = false;
	/*} else if (ehchao) {
		//TO DO: o afinamento da camada de ch�o ainda n�o foi feito
		ret = true;*/
	} else if (afin->EhAfinInteira()) {
		ret = true;
	} else {
		const double MEIO_LADOHEX = LADOHEX / 2.0;
		
		//calcula a posi��o do ponto desfazendo a rota��o da parede (xr, yr)
		double xr, yr;
		{
			double xh, yh; //pos. do proj�til relativo ao centro do hex�gono
			xh = xf - xch;
			yh = yf - ych;
			
			double ang_afin = -PI_180 * afin->AfinRotacao(); //rota��o hor�ria

			//rotaciona os pontos no sentido inverso (anti-hor�ria)
			xr = cos(ang_afin) * xh + sin(ang_afin) * yh;
			yr = -sin(ang_afin)* xh + cos(ang_afin) * yh;
		}
		
		if (afin->EhAfinAtraves()) {
			ret = (xr > -MEIO_LADOHEX) && (xr < MEIO_LADOHEX);
		} else if (afin->EhAfinMetade()) {
			ret = (yr > 0);
		} else {
			//erro!
			ret = false;
		}
	}
	parede_fina_perto &= !ret; //zera parede_fina_perto caso tenha acertado em uma parede
	return ret;
}

//######################### cPontoAPonto
void cPontoAPonto::IniciaAlvo(double _xo, double _yo, double _zo,
							 double xa, double ya, double za)
{
	cPontoLinha::Inicia(_xo, _yo, _zo);

	distmax = Distancia3D(_xo, _yo, _zo, xa, ya, za);

	dx = (xa - xo) / distmax;
	dy = (ya - yo) / distmax;
	dz = (za - zo) / distmax;
	teta = 0;
	phi = 0;
}


bool cPontoAPonto::Avanca(double dw)
{
	w += dw;
	if (w > distmax) {
		return false;
	} else {
		cPontoLinha::Avanca(w);
		return true;
	}
}

//######################### cProjetil
void cProjetil::Inicia(double _teta, double _phi,
					   double _xo, double _yo, double _zo,
					   double espalhamento,
					   double _dano,
					   short _idlinhatiro)
{
	cPontoLinha::IniciaAngulos(_teta, _phi, _xo, _yo, _zo);
	Inicia(espalhamento, _dano, _idlinhatiro);
}

void cProjetil::Inicia(double _dx, double _dy, double _dz,
					   double _xo, double _yo, double _zo,
					   double espalhamento,
					   double _dano,
					   short _idlinhatiro)
{
	cPontoLinha::IniciaDeltas(_dx, _dy, _dz, _xo, _yo, _zo);
	Inicia(espalhamento, _dano, _idlinhatiro);
}

void cProjetil::Inicia(double espalhamento, double _dano, short _idlinhatiro)
{
	dano =	_dano;
	idlinhatiro = _idlinhatiro;
	Desvia(espalhamento);

	jogperto = -1;
	parou =	false;
}

void cProjetil::Desvia(double espalhamento)
{
	teta += superrnd(espalhamento);
	phi += superrnd(espalhamento);
	AjustaDW();
}

void cProjetil::Valida()
{
	cPontoLinha::Valida();
	danoant = dano;
}
