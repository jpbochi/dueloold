
//#include <windows.h>
//#include <gl\gl.h>
#include "fx.h"
#include "fx3d.h"
#include "normais.h"
#include "declarac.h"
//#include "jogo.h"
#include <math.h>
#include "modelosgl.h"

//#include "funcoes.h" //tempor�rio

extern uint gllist[GLList::NUM_LISTS];

void GLLineLoop4v(vertex v0, vertex v1, vertex v2, vertex v3)
{
	//glBegin(GL_LINE_LOOP);
	glBegin(GL_LINE_STRIP);
	glVertex3f(v0[0], v0[1], v0[2]);
	glVertex3f(v1[0], v1[1], v1[2]);
	glVertex3f(v2[0], v2[1], v2[2]);
	glVertex3f(v3[0], v3[1], v3[2]);
	glEnd();
}

void GLTrianglevSemNormal(float tv0x, float tv0y,
						  float tv1x, float tv1y,
						  float tv2x, float tv2y,
						  vertex v0, vertex v1, vertex v2)
{
	glTexCoord2f(tv0x, tv0y);	glVertex3f(v0[0], v0[1], v0[2]);
	glTexCoord2f(tv1x, tv1y);	glVertex3f(v1[0], v1[1], v1[2]);
	glTexCoord2f(tv2x, tv2y);	glVertex3f(v2[0], v2[1], v2[2]);
}

void subdivide(float tv0[2], float tv1[2], float tv2[2],
			   vertex v0, vertex v1, vertex v2, int depth)
{
	GLfloat v01[3], v12[3], v20[3];
	GLfloat tv01[2], tv12[2], tv20[2];
	GLint i;

	if (depth == 0) {
		GLTrianglevSemNormal(tv0[0], tv0[1], tv1[0], tv1[1], tv2[0], tv2[1], v0, v1, v2);
	} else {
		for (i = 0; i < 2; i++) {
			tv01[i] = (tv0[i] + tv1[i]) / 2.0f;
			tv12[i] = (tv1[i] + tv2[i]) / 2.0f;
			tv20[i] = (tv2[i] + tv0[i]) / 2.0f;
		}
		for (i = 0; i < 3; i++) {
			v01[i] = (v0[i] + v1[i]) / 2.f;
			v12[i] = (v1[i] + v2[i]) / 2.f;
			v20[i] = (v2[i] + v0[i]) / 2.f;
		}
		subdivide(tv0, tv01, tv20, v0,  v01,  v20, depth-1);
		subdivide(tv1, tv12, tv01, v1,  v12,  v01, depth-1);
		subdivide(tv2, tv20, tv12, v2,  v20,  v12, depth-1);
		subdivide(tv01, tv12, tv20, v01,  v12,  v20, depth-1);
	}
}

void GLQuadv(vertex v0, vertex v1, vertex v2, vertex v3)
{
	vertex normal;
	CalcNormalf(v0[0], v0[1], v0[2],
				v1[0], v1[1], v1[2],
				v2[0], v2[1], v2[2], normal);
	glNormal3fv(normal);
	glVertex3f(v0[0], v0[1], v0[2]);
	glVertex3f(v1[0], v1[1], v1[2]);
	glVertex3f(v2[0], v2[1], v2[2]);
	glVertex3f(v3[0], v3[1], v3[2]);
}

void GLQuadv(float tv0x, float tv0y,
			 float tv1x, float tv1y,
			 float tv2x, float tv2y,
			 float tv3x, float tv3y,
			 vertex v0, vertex v1, vertex v2, vertex v3)
{
	vertex normal;
	CalcNormalf(v0[0], v0[1], v0[2],
				v1[0], v1[1], v1[2],
				v2[0], v2[1], v2[2], normal);
	glNormal3fv(normal);
	/*
	glTexCoord2f(tv0x, tv0y);	glVertex3f(v0[0], v0[1], v0[2]);
	glTexCoord2f(tv1x, tv1y);	glVertex3f(v1[0], v1[1], v1[2]);
	glTexCoord2f(tv2x, tv2y);	glVertex3f(v2[0], v2[1], v2[2]);
	glTexCoord2f(tv3x, tv3y);	glVertex3f(v3[0], v3[1], v3[2]);
	/*/
	//GLTrianglevSemNormal(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, v0, v1, v2);
	//GLTrianglevSemNormal(tv0x, tv0y, tv2x, tv2y, tv3x, tv3y, v0, v2, v3);
	GLfloat tv0[2] = {tv0x, tv0y};
	GLfloat tv1[2] = {tv1x, tv1y};
	GLfloat tv2[2] = {tv2x, tv2y};
	GLfloat tv3[2] = {tv3x, tv3y};
	subdivide(tv0, tv1, tv2, v0, v1, v2, 1);
	subdivide(tv0, tv2, tv3, v0, v2, v3, 1);
	//*/
}

void GLTrianglev(float tv0x, float tv0y,
				 float tv1x, float tv1y,
				 float tv2x, float tv2y,
				 vertex v0, vertex v1, vertex v2)
{
	vertex normal;
	CalcNormalf(v0[0], v0[1], v0[2],
				v1[0], v1[1], v1[2],
				v2[0], v2[1], v2[2], normal);
	glNormal3fv(normal);
	/*
	glTexCoord2f(tv0x, tv0y);	glVertex3f(v0[0], v0[1], v0[2]);
	glTexCoord2f(tv1x, tv1y);	glVertex3f(v1[0], v1[1], v1[2]);
	glTexCoord2f(tv2x, tv2y);	glVertex3f(v2[0], v2[1], v2[2]);
	/*/
	GLTrianglevSemNormal(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, v0, v1, v2);
	//*/
}

void GeraGLListas()
{
	float meiolado = LADOHEX / 2.0f;

	//gera lista para desenhar hex�gono vazio 0
	glNewList(gllist[GLList::HEXAGONO]=glGenLists(1), GL_COMPILE);
		glNormal3f(0, 0, 1);
		glBegin(GL_TRIANGLE_FAN);
			glTexCoord2f(0.25, 0);	glVertex3f(-meiolado,	0.5,	0.0); //v2
			glTexCoord2f(0, 0.5);	glVertex3f(-LADOHEX,	0.0,	0.0); //v1
			glTexCoord2f(0.25, 1);	glVertex3f(-meiolado,	-0.5,	0.0); //v6
			glTexCoord2f(0.75, 1);	glVertex3f(meiolado,	-0.5,	0.0); //v5
			glTexCoord2f(1, 0.5);	glVertex3f(LADOHEX,		0.0,	0.0); //v4
			glTexCoord2f(0.75, 0);	glVertex3f(meiolado,	0.5,	0.0); //v3
		glEnd();
	glEndList();
	
	//pilar hex�gono
	glNewList(gllist[GLList::PILAR]=glGenLists(1), GL_COMPILE);
		glBegin(GL_TRIANGLES);//GL_QUADS);
			//tv#[xy] = texture vertex # (x ou y)
			/*
			float escalaz = LARGURACHAO/ALTURAPAREDE;
			float tv0x = 0;		float tv0y = escalaz;
			float tv1x = 0;		float tv1y = 1;
			float tv2x = 1;		float tv2y = 1;
			float tv3x = 1;		float tv3y = escalaz;
			/*/
			float escalaz = LARGURACHAO/ALTURAPAREDE;
			float tv0x = 0.25;		float tv0y = escalaz;
			float tv1x = 0.25;		float tv1y = 1.0;
			float tv2x = 0.75;		float tv2y = 1.0;
			float tv3x = 0.75;		float tv3y = escalaz;
			/**/
			vertex v1b = {-LADOHEX,		0.0,	0};
			vertex v1c = {-LADOHEX,		0.0,	ALTURAPAREDE};
			vertex v2b = {-meiolado,	0.5,	0};
			vertex v2c = {-meiolado,	0.5,	ALTURAPAREDE};
			vertex v3b = {meiolado,		0.5,	0};
			vertex v3c = {meiolado,		0.5,	ALTURAPAREDE};
			vertex v4b = {LADOHEX,		0.0,	0};
			vertex v4c = {LADOHEX,		0.0,	ALTURAPAREDE};
			vertex v5b = {meiolado,		-0.5,	0};
			vertex v5c = {meiolado,		-0.5,	ALTURAPAREDE};
			vertex v6b = {-meiolado,	-0.5,	0};
			vertex v6c = {-meiolado,	-0.5,	ALTURAPAREDE};

			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v1b, v1c, v2c, v2b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v2b, v2c, v3c, v3b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v3b, v3c, v4c, v4b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v4b, v4c, v5c, v5b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v5b, v5c, v6c, v6b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v6b, v6c, v1c, v1b);
		glEnd();
	glEndList();

	//parede que atravessa o hexagono
	glNewList(gllist[GLList::PAREDE_ATRAVES]=glGenLists(1), GL_COMPILE);
		glBegin(GL_TRIANGLES);
			GLQuadv(   0, tv0y,    0, tv1y,    1, tv2y,    1, tv3y,		v6b, v6c, v2c, v2b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v2b, v2c, v3c, v3b);
			GLQuadv(   0, tv0y,    0, tv1y,    1, tv2y,    1, tv3y,		v3b, v3c, v5c, v5b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v5b, v5c, v6c, v6b);
		glEnd();
	glEndList();
	glNewList(gllist[GLList::HEX_ATRAVES]=glGenLists(1), GL_COMPILE);
		glNormal3f(0, 0, 1);
		glBegin(GL_TRIANGLE_FAN);
			glTexCoord2f(0.25, 0);	glVertex3f(-meiolado,	0.5,	0.0); //v2
			//glTexCoord2f(0, 0.5);	glVertex3f(-LADOHEX,	0.0,	0.0); //v1
			glTexCoord2f(0.25, 1);	glVertex3f(-meiolado,	-0.5,	0.0); //v6
			glTexCoord2f(0.75, 1);	glVertex3f(meiolado,	-0.5,	0.0); //v5
			//glTexCoord2f(1, 0.5);	glVertex3f(LADOHEX,		0.0,	0.0); //v4
			glTexCoord2f(0.75, 0);	glVertex3f(meiolado,	0.5,	0.0); //v3
		glEnd();
	glEndList();

	//parede que ocupa a metade de cima do hexagono
	glNewList(gllist[GLList::PAREDE_METADE]=glGenLists(1), GL_COMPILE);
		glBegin(GL_TRIANGLES);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v1b, v1c, v2c, v2b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v2b, v2c, v3c, v3b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v3b, v3c, v4c, v4b);
			GLQuadv(   0, tv0y,    0, tv1y,    1, tv2y,    1, tv3y,		v4b, v4c, v1c, v1b);
		glEnd();
	glEndList();
	glNewList(gllist[GLList::HEX_METADE]=glGenLists(1), GL_COMPILE);
		glNormal3f(0, 0, 1);
		glBegin(GL_TRIANGLE_FAN);
			glTexCoord2f(0.25, 0);	glVertex3f(-meiolado,	0.5,	0.0); //v2
			glTexCoord2f(0, 0.5);	glVertex3f(-LADOHEX,	0.0,	0.0); //v1
			//glTexCoord2f(0.25, 1);	glVertex3f(-meiolado,	-0.5,	0.0); //v6
			//glTexCoord2f(0.75, 1);	glVertex3f(meiolado,	-0.5,	0.0); //v5
			glTexCoord2f(1, 0.5);	glVertex3f(LADOHEX,		0.0,	0.0); //v4
			glTexCoord2f(0.75, 0);	glVertex3f(meiolado,	0.5,	0.0); //v3
		glEnd();
	glEndList();

	glNewList(gllist[GLList::PILAR_CHAO]=glGenLists(1), GL_COMPILE);
		tv0x = 0.25;		tv0y = 0.0;
		tv1x = 0.25;		tv1y = escalaz;
		tv2x = 0.75;		tv2y = escalaz;
		tv3x = 0.75;		tv3y = 0.0;

		glTranslatef(0, 0, -LARGURACHAO);
		glScalef(0.99f, 0.99f, escalaz);
		glBegin(GL_TRIANGLES);//GL_QUADS);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v1b, v1c, v2c, v2b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v2b, v2c, v3c, v3b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v3b, v3c, v4c, v4b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v4b, v4c, v5c, v5b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v5b, v5c, v6c, v6b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v6b, v6c, v1c, v1b);
		glEnd();
	glEndList();
	glNewList(gllist[GLList::CHAO_ATRAVES]=glGenLists(1), GL_COMPILE);
		glTranslatef(0, 0, -LARGURACHAO);
		glScalef(0.99f, 0.99f, escalaz);
		glBegin(GL_TRIANGLES);
			GLQuadv(   0, tv0y,    0, tv1y,    1, tv2y,    1, tv3y,		v6b, v6c, v2c, v2b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v2b, v2c, v3c, v3b);
			GLQuadv(   0, tv0y,    0, tv1y,    1, tv2y,    1, tv3y,		v3b, v3c, v5c, v5b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v5b, v5c, v6c, v6b);
		glEnd();
	glEndList();
	glNewList(gllist[GLList::CHAO_METADE]=glGenLists(1), GL_COMPILE);
		glTranslatef(0, 0, -LARGURACHAO);
		glScalef(0.99f, 0.99f, escalaz);
		glBegin(GL_TRIANGLES);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v1b, v1c, v2c, v2b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v2b, v2c, v3c, v3b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v3b, v3c, v4c, v4b);
			GLQuadv(   0, tv0y,    0, tv1y,    1, tv2y,    1, tv3y,		v4b, v4c, v1c, v1b);
		glEnd();
	glEndList();

	glNewList(gllist[GLList::MARCA]=glGenLists(1), GL_COMPILE);
		glNormal3f(0, 0, 1);
		GLLineLoop4v(v1b, v1c, v2c, v2b);
		GLLineLoop4v(v2b, v2c, v3c, v3b);
		GLLineLoop4v(v3b, v3c, v4c, v4b);
		GLLineLoop4v(v4b, v4c, v5c, v5b);
		GLLineLoop4v(v5b, v5c, v6c, v6b);
		GLLineLoop4v(v6b, v6c, v1c, v1b);
	glEndList();

	glNewList(gllist[GLList::PIRAMIDE]=glGenLists(1), GL_COMPILE);
		tv0x = 0.25;		tv0y = escalaz;
		tv1x = 0.25;		tv1y = 1.0;
		tv2x = 0.75;		tv2y = 1.0;
		tv3x = 0.75;		tv3y = escalaz;

		float esc_topo = 0.4f;
		v1c[0] *= esc_topo;	v1c[1] *= esc_topo;
		v2c[0] *= esc_topo;	v2c[1] *= esc_topo;
		v3c[0] *= esc_topo;	v3c[1] *= esc_topo;
		v4c[0] *= esc_topo;	v4c[1] *= esc_topo;
		v5c[0] *= esc_topo;	v5c[1] *= esc_topo;
		v6c[0] *= esc_topo;	v6c[1] *= esc_topo;

		glScalef(0.8f, 0.8f, 0.7f);
		glBegin(GL_TRIANGLES);//GL_QUADS);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v1b, v1c, v2c, v2b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v2b, v2c, v3c, v3b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v3b, v3c, v4c, v4b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v4b, v4c, v5c, v5b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v5b, v5c, v6c, v6b);
			GLQuadv(tv0x, tv0y, tv1x, tv1y, tv2x, tv2y, tv3x, tv3y,		v6b, v6c, v1c, v1b);
		glEnd();
		glNormal3f(0, 0, 1);
		glBegin(GL_TRIANGLE_FAN);
			glTexCoord2f(1, 0.5);	glVertex3fv(v6c);
			glTexCoord2f(0.75, 0);	glVertex3fv(v5c);
			glTexCoord2f(0.25, 0);	glVertex3fv(v4c);
			glTexCoord2f(0, 0.5);	glVertex3fv(v3c);
			glTexCoord2f(0.25, 1);	glVertex3fv(v2c);
			glTexCoord2f(0.75, 1);	glVertex3fv(v1c);
		glEnd();
		/*
		//TO DO: arrumar as normais dessa piramide
		glNormal3f(0, 0, 1);
		glBegin(GL_TRIANGLE_FAN);
			glTexCoord2f(0.5, 1);	glVertex3f(0.0,			0.0,	ALTURAPAREDE);

			glTexCoord2f(0.0, 0);	glVertex3f(-meiolado,	0.5,	0.0);
			glTexCoord2f(0.2, 0);	glVertex3f(-LADOHEX,	0.0,	0.0);
			glTexCoord2f(0.4, 0);	glVertex3f(-meiolado,	-0.5,	0.0);
			glTexCoord2f(0.6, 0);	glVertex3f(meiolado,	-0.5,	0.0);
			glTexCoord2f(0.8, 0);	glVertex3f(LADOHEX,		0.0,	0.0);
			glTexCoord2f(1.0, 0);	glVertex3f(meiolado,	0.5,	0.0);
			glTexCoord2f(0.0, 0);	glVertex3f(-meiolado,	0.5,	0.0);
		glEnd();
		*/
	glEndList();

	glNewList(gllist[GLList::CILINDRO]=glGenLists(1), GL_COMPILE);
		vertex vIniA, vIniB, vFimA, vFimB;
		double stepang = 0.25;
		const float stepcomp = 0.025f;
		double ang;
		glBegin(GL_QUADS);
			for (ang = 0; ang <= DOIS_PI+stepang; ang += stepang) {
				vIniA[0] = (float)cos(ang);	vFimA[0] = vIniA[0];
				vIniA[2] = (float)sin(ang);	vFimA[2] = vIniA[2];
				
				if (ang > 0) {
					vIniA[1] = 0;				vFimA[1] = stepcomp;
					vIniB[1] = vIniA[1];		vFimB[1] = vFimA[1];

					while (vFimA[1] <= 1.0001) {
						//char R, G, B;
						//CorDistancia(R, G, B, (1.1 - vFimA[1]) * 32);
						//glColor3ub(R, G, B);

						GLQuadv(vIniA, vIniB, vFimB, vFimA);

						vIniA[1] += stepcomp;	vFimA[1] += stepcomp;
						vIniB[1] = vIniA[1];	vFimB[1] = vFimA[1];
					}
				}

				vIniB[0] = vIniA[0];	vFimB[0] = vFimA[0];
				vIniB[2] = vIniA[2];	vFimB[2] = vFimA[2];
			}
		glEnd();

		stepang *= 2.;
		glBegin(GL_TRIANGLE_FAN);
			glNormal3f(0, -1, 0); //n�o tenho certeza se estas normais est�o certas
			for (ang = 0; ang <= DOIS_PI+stepang; ang += stepang) {
				glVertex3f((float)cos(ang), 0, (float)sin(ang));
			}
		glEnd();
		glBegin(GL_TRIANGLE_FAN);
			glNormal3f(0, 1, 0);
			for (ang = 0; ang >= -DOIS_PI-stepang; ang -= stepang) {
				glVertex3f((float)cos(ang), 1, (float)sin(ang));
			}
		glEnd();
	glEndList();
}