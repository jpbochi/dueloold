

#include "winmain.h"

#include "declarac.h"
extern FXuint textures[MAX_TEXTURES];

#include "modelosmd2.h"

#include "arquivos.h"

#include "glcontext.h"

static Texture			texman;		// the texture manager
static MD2				md2;		// the MD2 model loader

#include "texturas.h"

const POS_ANIM_DEFAULT			= 39;
const POS_ANIM_CROUCH_DEFAULT	= 169;
const POS_ANIM_LOOP				= -1;
//POS_ANIM[i][0] = Frame inicial
//POS_ANIM[i][1] = Frame final
//POS_ANIM[i][2] = Frame padr�o do fim da anima��o (-1 significa anima��o em loop)
const POS_ANIM[ModeloMD2Anim::ANIM_COUNT][3] = {
						{0,   39,  POS_ANIM_LOOP},			//STAND
						{40,  45,  POS_ANIM_LOOP},			//RUN
						{46,  53,  POS_ANIM_DEFAULT},		//ATTACK
						{54,  57,  POS_ANIM_DEFAULT},		//PAIN_1
						{58,  61,  POS_ANIM_DEFAULT},		//PAIN_2
						{62,  65,  POS_ANIM_DEFAULT},		//PAIN_3
						{66,  71,  POS_ANIM_DEFAULT},		//JUMP
						{72,  83,  POS_ANIM_DEFAULT},		//FLIPOFF
						{84,  94,  POS_ANIM_DEFAULT},		//SALUTE
						{95,  111, POS_ANIM_DEFAULT},		//TAUNT
						{112, 122, POS_ANIM_DEFAULT},		//WAVE
						{123, 134, POS_ANIM_DEFAULT},		//POINT
						{135, 153, POS_ANIM_LOOP},			//CROUCH_STAND
						{154, 159, POS_ANIM_LOOP},			//CROUCH_WAK
						{160, 168, POS_ANIM_CROUCH_DEFAULT},//CROUCH_ATTACK
						{169, 172, POS_ANIM_CROUCH_DEFAULT},//CROUCH_PAIN
						{173, 177, 177},					//CROUCH_DEATH
						{178, 183, 183},					//DEATH_1
						{184, 189, 189},					//DEATH_2
						{190, 197, 197},					//DEATH_3
						};

FXList *ModeloMD2::ListaTexturas(FXComposite *comp, FXString nomemodelo, FXList *lista)
{
	FXString str, dir;
		
	//acha o diret�rio certo
	FXDirList *dirlist = new FXDirList(comp);//		dirlist->hide();
	dirlist->setCurrentFile(str.format("Data\\%s",nomemodelo));
	dir = dirlist->getDirectory();
	dirlist->destroy();
	delete dirlist;

	//testa se o diret�rio existe
	nomemodelo = nomemodelo.lower();
	FXString fim = dir.right(nomemodelo.length()).lower();
	if (fim != nomemodelo) {
		return lista;
	}
	
	//cria lista
	FXList *ret;
	if (lista == 0) {
		ret = new FXList(comp);
	} else {
		ret = lista;
	}
	ret->hide();
	
	//entra no diret�rio
	FXFileList *filelist = new FXFileList(comp);//	filelist->hide();
	filelist->setDirectory(dir);
	
	//percorre itens no diret�rio
	FXPCXImage *imagem = new FXPCXImage(comp->getApp());
	FXString arquivo;
	FXFileStream File;
	const FXuint pcxhash = FXString(".pcx").hash();
	
	int num_itens =filelist->getNumItems();
	for (int i=0; i<num_itens; i++) {
		//testa se o tipo do arquivo � pcx E se n�o � a textura de uma arma
		arquivo = filelist->getItemFilename(i);
		if (arquivo.right(4).lower().hash() == pcxhash
		&&  arquivo.left(2).lower() != "w_") {
			//abre a imagem
			str = dir + "\\" + arquivo;
			File.open(str, FX::FXStreamLoad);
			if (imagem->loadPixels(File)) {
				if (imagem->getHeight() == 256 && imagem->getWidth() == 256) {
					str = nomemodelo + "\\" + arquivo.left(arquivo.length() - 4);
					ret->appendItem(str);
				}
			}
			File.close();
		}
	}
	
	filelist->destroy();
	delete filelist;

	return ret;
}

ModeloMD2::ModeloMD2()
{
	tex = 0;
	numframes = 0;
}

bool ModeloMD2::Carrega(FXApp *app, FXString _nome_modelo)
{
	nome_modelo = _nome_modelo;

	FXString arq_modelo;
	arq_modelo.format("data\\%s\\tris.md2", nome_modelo);

	//testa se o arquivo existe
	ExisteArquivo(app, arq_modelo);

	//carrega o modelo
	if (!model.Load(arq_modelo.text(), md2, texman)) return false;

	// get the number of frames
	numframes = model.GetNumFrames();

	//faz a textura de todos os frames do modelo apontarem para a minha textura
	for (int i=0; i<numframes; i++) {
		ModelFrame *frm = model.GetFrame(i);
		if (frm == 0) return false;
		
		Mesh *msh = frm->GetMesh(0);
		if (msh == 0) return false;
		
		msh->SetTexture(&tex);
	}
	return true;
}

ModeloMD2Anim::ModeloMD2Anim()
{
	animando = false;
	anim_atual = ANIM_NENHUMA;
	framepos = 0;
	tex = 0;
	transl[0] = 0;
	transl[1] = 0;
	transl[2] = 0;
}

void ModeloMD2Anim::Carrega(ModeloMD2 *modelo)
{
	//ajusta o ponteiro para o modelo
	modeloMD2 = modelo;
}

bool ModeloMD2Anim::CarregaTextura(FXApp *app, FXuint &texid, FXString nome_textura)
{
	if (modeloMD2 == 0) return false;

	//identifica o nome do arquivo da textura
	FXString arq_tex;
	if (nome_textura.empty()) nome_textura = modeloMD2->nome_modelo;
	arq_tex.format("data\\%s\\%s.pcx", modeloMD2->nome_modelo, nome_textura);

	//testa se o arquivo existe
	ExisteArquivo(app, arq_tex);
	
	//carrega a textura
	GLContexto.Empilha();
	if (!::CarregaTextura(app, arq_tex, texid)) {
		GLContexto.DesEmpilha();
		return false;
	}
	GLContexto.DesEmpilha();
	
	//seta a textura do modelo
	tex = texid;
	
	return true;
}

void ModeloMD2Anim::TrocaTextura(FXuint texid)
{
	tex = texid;
}

void ModeloMD2Anim::AnimInicia(int anim_tipo)
{
	animando = true;
	anim_atual = anim_tipo;
	if (anim_atual == ANIM_NENHUMA) {
		framepos = 0;
		framepos_ant = POS_ANIM_DEFAULT;
		framepos_prox = POS_ANIM_DEFAULT;
	} else {
		framepos = 0;
		framepos_ant = POS_ANIM[anim_atual][0];
		framepos_prox = AnimProxFrame(framepos_ant);
	}
	//zera movimento
	transl[0] = 0;
	transl[1] = 0;
	transl[2] = 0;
}

void ModeloMD2Anim::SetAnimTransl(double x, double y, double z)
{
	transl[0] = x;
	transl[1] = y;
	transl[2] = z;
}

void ModeloMD2Anim::DisplayGL()
{
	//empilha
	glPushMatrix();
	glFrontFace(GL_CW);

	//aplica transla��o
	double l = AnimPos();
	glTranslated(l * transl[0], l * transl[1], l * transl[2]);

	//posiciona
	glScalef(0.025f,0.025f,0.025f);
	glTranslatef(0.0f,0.0f,24.0f); //faz o modelo ficar no ch�o
	glRotatef(90, 1, 0, 0); //faz o modelo ficar no eio Z e n�o no eixo Y
	
	//ajusta a textura
	modeloMD2->tex = tex;
	
	//renderiza
	/*//obsoleto?
	model.DrawTimeFrame(0, (float)framepos);
	/*/
	ModelFrame inter;
	inter.SetInterp(*modeloMD2->model.GetFrame(framepos_ant),
					*modeloMD2->model.GetFrame(framepos_prox),
					framepos);
	inter.Draw();
	//*/

	//desempilha
	glFrontFace(GL_CCW);
	glPopMatrix();
}

FXuint ModeloMD2Anim::AnimAvanca(double frames)
{
	//s� retorna 1 quando a anima��o termina ou faz um loop
	FXuint ret = false;
	if (animando) {
		framepos += (float)frames;
		do {
			//percorreu um frame inteiro
			if (framepos > 1) {
				framepos -= 1;
				
				framepos_ant = framepos_prox;
				framepos_prox = AnimProxFrame(framepos_ant);
				
				//retorna falso quando a anima��o faz um loop
				ret |= (framepos_prox == POS_ANIM[anim_atual][0]);
				
				//n�o h� mais frames para mostrar
				if (framepos_ant == framepos_prox) {
					framepos = 0;
					animando = false;
					//retorna falso quando a anima��o p�ra
					ret = 1;
				}
			}
		} while (animando && framepos > 1);
	}
	return ret;
}

int ModeloMD2Anim::AnimProxFrame(int frame)
{
	//terminou a anima��o?
	if (frame < POS_ANIM[anim_atual][1]) {
		//se n�o terminou, avan�a um frame
		return frame + 1;
	} else {
		//a anima��o � um loop?
		if (POS_ANIM[anim_atual][2] == POS_ANIM_LOOP) {
			//volta para o in�cio
			return POS_ANIM[anim_atual][0];
		} else {
			//para a anima��o
			return POS_ANIM[anim_atual][2];
		}
	}
}

double ModeloMD2Anim::AnimPos()
{
	if (POS_ANIM[anim_atual][2] == POS_ANIM_LOOP) {
		//anima��es em loop percorrem um frame extra (entre o �ltimo e o primeiro)
		return (framepos_ant + framepos - POS_ANIM[anim_atual][0])
			 / (1 + POS_ANIM[anim_atual][1] - POS_ANIM[anim_atual][0]);
	} else {
		return (framepos_ant + framepos - POS_ANIM[anim_atual][0])
			 / (POS_ANIM[anim_atual][1] - POS_ANIM[anim_atual][0]);
	}
}
