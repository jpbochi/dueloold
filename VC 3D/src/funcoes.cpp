
#include "funcoes.h"
#include "geral.h"
#include "declarac.h"

#include <math.h>

//#include "mapa.h"

//http://mathworld.wolfram.com/SphericalCoordinates.html

//###################################################
//fun��es pequenas ##################################

void CorDistancia(char &R, char &G, char &B, double dist)
{
	const double dr1 = 16, dr2 = 32;
	const double dg1 = 16, dg2 = 28, dg3 = 40;
	R = 0;
	G = 0;
	B = 0;
	if (dist < dr1) {
		R = (char)255;
	} else if (dist < dr2) {
		R = (char)floor(255 * (dr2 - dist) / (dr2 - dr1));
	}
	if (dist < dg1) {
		G = (char)floor(255 * dist / dg1);
	} else if (dist < dg2) {
		G = (char)255;
	} else if (dist < dg3) {
		G = (char)floor(255 * pow((dg3 - dist) / (dg3 - dg2), 2));
	}
}

short Direcao(int xo, int yo, int xa, int ya, double angteta)
{
	if (xo == xa && yo == ya) {
		return -1;
	} else {
		if (angteta == 7.0) angteta = funAngTetai(xo, yo, xa, ya);
		return Direcao(angteta);
	}
}
short Direcao(double teta)
{
	short ret = (short)floor(0.5 + (teta / (PI / 6.0)));
	ret = (ret + 12) % 12;//if (ret < 0) ret += 12;
	return ret;
}
short DifAngi(short frente, short proxfrente)
{
	return (frente - proxfrente + 12) % 12;
}
bool DifAngb(short frente, short proxfrente, short angpermitido)
{
	short difang = DifAngi(frente, proxfrente);
	return ((difang <= angpermitido) || (difang >= 12 - angpermitido));
}

double funAngTetai(int xo, int yo, int xa, int ya)
{
	double xho, yho, xha, yha;

	CentroHex2D(xo, yo, xho, yho);
	CentroHex2D(xa, ya, xha, yha);

	return funAngTetaf(xho, yho, xha, yha);
}
double funAngTetaf(double xo, double yo, double xa, double ya)
{
	double ret = atan((ya - yo) / (xa - xo + 0.000001));
	if (xa < xo) ret += PI;
	return ret;
}

double funAngPhii(int xo, int yo, int zo, int xa, int ya, int za, double alturanoandar)
{
	double xho, yho, zho, xha, yha, zha;
	CentroHex3D(xo, yo, zo, xho, yho, zho, alturanoandar);
	CentroHex3D(xa, ya, za, xha, yha, zha, alturanoandar);

	return funAngPhif(xho, yho, zho, xha, yha, zha);
}
double funAngPhif(double xo, double yo, double zo, double xa, double ya, double za)
{
    double dx = xo - xa;
    double dy = yo - ya;
    double dz = zo - za;
    double r = sqrt(pow(dx, 2.0) + pow(dy, 2.0) + pow(dz, 2.0));
	dz /= r;
	return acos(dz);
}

void EsferToCart(double teta, double phi, double r,
				 double xo, double yo, double zo,
				 double &x, double &y, double &z)
{
	x = xo + (r * sin(phi) * cos(teta));
	y = yo + (r * sin(phi) * sin(teta));
	z = zo + (r * cos(phi));
}
void CartToEsferi(int xo, int yo, int zo,
				  int xa, int ya, int za,
				  double &teta, double &phi, double &r, double alturanoandar)
{
	double xho, yho, zho, xha, yha, zha;
	CentroHex3D(xo, yo, zo, xho, yho, zho, alturanoandar);
	CentroHex3D(xa, ya, za, xha, yha, zha, alturanoandar);
	
	CartToEsferf(xho, yho, zho, xha, yha, zha, teta, phi, r);
}

void CartToEsferf(double xho, double yho, double zho,
				  double xha, double yha, double zha,
				  double &teta, double &phi, double &r)
{
	r = Distancia3D(xho, yho, zho, xha, yha, zha);
	if (r == 0) {
		teta = 0;
		phi = DOIS_PI;
	} else {
		teta = funAngTetaf(xho, yho, xha, yha);
		phi = funAngPhif(xho, yho, zho, xha, yha, zha);
	}
}

void CentroHex2D(int x, int y, double &xh, double &yh)
{
	yh = (x % 2) ? y + 0.5f : y;
	xh = LARGURAHEX * x;
}
void CentroHex3D(int x, int y, int z, double &xh, double &yh, double &zh, double alturanoandar)
{
	yh = (x % 2) ? y + 0.5f : y;
	xh = LARGURAHEX * x;
	zh = ALTURAANDAR * (z + alturanoandar);
}

unsigned short AlturaToAndar(double zo, bool &chao)
{
	int zh;
	double z = zo / ALTURAANDAR;
	zh = (int)floor(z);

	z = (z - floor(z)) * ALTURAANDAR;
	chao = (z > ALTURAPAREDE);
	if (chao) zh++;
	return zh;
}

double AndarToAltura(int z, double alturanoandar)
{
	return ALTURAANDAR * (z + alturanoandar);
}

double AlturaSombra(double z)
{
	bool chao;
	int andar_sombra = AlturaToAndar(z, chao);
	if (chao) andar_sombra--; //se estava no ch�o entre algum andar, faz sombra no de baixo
	
	return AndarToAltura(andar_sombra, 0);
}

void PontoToHex2D(double xo, double yo, int &xh, int &yh)
{
	double x1, y1, x2, y2, d1, d2, aux;

	x1 = floor(xo / LARGURAHEX);	y1 = floor(yo * 2) / 2;
	{x2 = x1 + 1;					y2 = y1 + 0.5f;}

	//if (!((x1 % 2 == 0) && (y1 == floor(y1)))
	//&&   ((x1 % 2 == 0) || (y1 == floor(y1)))) {
	if (((int)x1 % 2 == 0) != (y1 == floor(y1))) { //XOR
		Troca(y1, y2, aux);
	}

	d1 = Distancia2D(x1, y1, xo / LARGURAHEX, yo);
	d2 = Distancia2D(x2, y2, xo / LARGURAHEX, yo);
	
	if (d1<d2)	{xh = (int)x1; yh = (int)y1;}
	else		{xh = (int)x2; yh = (int)y2;}
}
void PontoToHex3D(double xo, double yo, double zo, int &xh, int &yh, int &zh, bool &chao)
{
	PontoToHex2D(xo, yo, xh, yh);
	
	double z = zo / ALTURAANDAR;
	zh = (int)floor(z);

	z = (z - floor(z)) * ALTURAANDAR;
	chao = (z > ALTURAPAREDE);
	if (chao) zh++;
}

double Distancia2D(int x1, int y1, int x2, int y2)
{
	double xh1, yh1, xh2, yh2;
	
	CentroHex2D(x1, y1, xh1, yh1);
	CentroHex2D(x2, y2, xh2, yh2);
	
	return Distancia2D(xh1, yh1, xh2, yh2);
}
double Distancia3D(int x1, int y1, int z1, int x2, int y2, int z2)
{
	double xh1, yh1, zh1, xh2, yh2, zh2;
	
	CentroHex3D(x1, y1, z1, xh1, yh1, zh1, 0);
	CentroHex3D(x2, y2, z2, xh2, yh2, zh2, 0);
	
	return Distancia3D(xh1, yh1, zh1, xh2, yh2, zh2);
}

inline double Distancia2D(double x1, double y1, double x2, double y2)
{
	double difx = x1 - x2;	double dify = y1 - y2;
	return sqrt((difx*difx) + (dify*dify));
}
inline double Distancia3D(double x1, double y1, double z1, double x2, double y2, double z2)
{
	double difx = x1 - x2;	double dify = y1 - y2;	double difz = z1 - z2;
	return sqrt((difx*difx) + (dify*dify) + (difz*difz));
}

double Aleatorio(double num, double expo)
{
    //obs: essa fun��o conserva o sinal (e fixa o zero)
	return num * exp(superrnd(log(expo)));
}
double superrnd(double desvio)
{
	// Calcula um n� aleatorio com media zero e desv. padrao = "desvio"
	// Obs. p/ matem�ticos e outros espertos: a distribuicao e' normal.
	return normali(rnd(), 0, desvio);
}

#define cd0	2.515517
#define cd1 0.802853
#define cd2 0.010328
#define ce0 1.0
#define ce1 1.432788
#define ce2 0.189269
#define ce3 0.00138
#define cjp 0.78

double normali(double p, double md, double s)
{
	//Distribui��o normal inversa
	//fornece a ordenada x tq Probabilidade de -� a x = p
	//md= media, s=desvio padrao
	s /= cjp;
	double Q = sqrt(-2.0 * log(1 - p));
	double u = Q - (cd0 + cd1 * Q + cd2 * Q*Q) / (ce0 + ce1 * Q + ce2 * Q*Q + ce3 * pow(Q, 3.0));
	return md + s * u;
}