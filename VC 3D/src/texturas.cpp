
#include "texturas.h"
#include "fx3d.h"

#include "arquivos.h"

#include "..\glpng\glpng.h"

#include "glcontext.h"

bool CarregaTextura(FXApp *app, FXString strfile, FXuint &texid, bool transp)
{
	FXImage *imagem;

	//######################### ignora nodraw (utilizado em alguns modelos md3
	if (strfile.right(6).lower() == "nodraw") {
		//TO DO: apontar para uma textura totalmente transparente
		texid = 1;
		return true;
	}

	//######################### Testa se o arquivo existe
	ExisteArquivo(app, strfile);

	//######################### Testa o tipo da imagem
	FXString tipo(strfile.right(4).lower());

	static const FXuint bmphash = FXString(".bmp").hash();
	static const FXuint jpghash = FXString(".jpg").hash();
	static const FXuint pcxhash = FXString(".pcx").hash();
	static const FXuint tgahash = FXString(".tga").hash();
	static const FXuint pnghash = FXString(".png").hash();
	
	FXuint tipohash = tipo.hash();

	if      (tipohash == bmphash) imagem = new FXBMPImage(app);
	//else if (tipohash == jpghash) imagem = new FXJPGImage(app); //jpg n�o est� funcionando (falta a biblioteca)
	else if (tipohash == pcxhash) imagem = new FXPCXImage(app);
	else if (tipohash == tgahash) imagem = new FXTGAImage(app);
	else if (tipohash == pnghash) {
		/*
		imagem = new FXPNGImage(app);
		/*/
		pngInfo info;

		GLContexto.Empilha();
		
		glGenTextures(1, &texid);
		glBindTexture(GL_TEXTURE_2D, texid);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

		if (!pngLoad(strfile.text(), PNG_NOMIPMAP, PNG_ALPHA, &info)) {
			// error opening file.- goto default shader
			//char *buf;
			//buf = strdup( "Unable to load texture : " );
			//strcat( buf, strFileName );
			//MessageBox( NULL, buf, "Failure", MB_OK );
			fxwarning("N�o foi poss�vel carregar a textura: ''%s''!", strfile, app->getAppName());

			GLContexto.DesEmpilha();
			return false;
		}
		GLContexto.DesEmpilha();
		return true; //eu sei que essa fun��o t� meio bagun�ada, mas azar
		//*/
	} else {  //outros tipos podem ser adicionados trivialmente
		//tipo desconhecido
		return false;
	}

	//######################### Carrega a textura
	FXFileStream File;
	File.open(strfile, FX::FXStreamLoad);

	imagem->loadPixels(File);

	//Edita o canal Alfa (j� que o FOX p�e tudo zero no alfa, eu tenho que arrumar)
	{
		FXint width = imagem->getWidth();
		FXint height = imagem->getHeight();
		FXColor *data = imagem->getData();
		
		register FXint i=width*height-1;
		do {
			if (transp
			&&	((FXuchar*)(data+i))[0] == 255
			&&  ((FXuchar*)(data+i))[1] == 255
			&&  ((FXuchar*)(data+i))[2] == 255)
				((FXuchar*)(data+i))[3] = 0;
			else
				((FXuchar*)(data+i))[3] = 255;
		} while(--i>=0);
	}

	GLContexto.Empilha();

	glGenTextures(1, &texid);
	glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, texid);

	//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagem->getWidth(), imagem->getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, imagem->getData());
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, imagem->getWidth(), imagem->getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, imagem->getData());
	
	//*
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);	
	/*/
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);
	//*/

	GLContexto.DesEmpilha();
	
	return true;
}

void ChromaKey(FXImage *img, FXColor cor)
{
	FXint width = img->getWidth();
	FXint height = img->getHeight();
	FXColor *data = img->getData();
	
	cor &= 0x00FFFFFF;
	
	register FXint i=width*height-1;
	do {
		if ((data[i] & 0x00FFFFFF) == cor)
			((FXuchar*)(data+i))[3] = 0;
		else
			((FXuchar*)(data+i))[3] = 255;
	} while(--i>=0);
}

FXImage *Imagem(FXApp *app, FXString strfile)
{
	//######################### Testa se o arquivo existe
	ExisteArquivo(app, strfile);

	//######################### Testa o tipo da imagem
	FXString tipo(strfile.right(4).lower());

	static const FXuint bmphash = FXString(".bmp").hash();
	static const FXuint jpghash = FXString(".jpg").hash();
	static const FXuint pcxhash = FXString(".pcx").hash();
	static const FXuint tgahash = FXString(".tga").hash();
	static const FXuint pnghash = FXString(".png").hash();
	
	FXuint tipohash = tipo.hash();

	//######################### Instancia a imagem
	FXImage *imagem;
	if      (tipohash == bmphash) imagem = new FXBMPImage(app);
	else if (tipohash == pcxhash) imagem = new FXPCXImage(app);
	else if (tipohash == tgahash) imagem = new FXTGAImage(app);
	else	return 0;

	//######################### Carrega a imagem
	FXFileStream File;
	File.open(strfile, FX::FXStreamLoad);

	imagem->loadPixels(File);

	File.close();
	
	return imagem;
}

FXIcon *Icone(FXApp *app, FXString strfile)
{
	//######################### Testa se o arquivo existe
	ExisteArquivo(app, strfile);

	//######################### Testa o tipo da imagem
	FXString tipo(strfile.right(4).lower());

	static const FXuint bmphash = FXString(".bmp").hash();
	static const FXuint jpghash = FXString(".jpg").hash();
	static const FXuint pcxhash = FXString(".pcx").hash();
	static const FXuint tgahash = FXString(".tga").hash();
	static const FXuint pnghash = FXString(".png").hash();
	static const FXuint gifhash = FXString(".gif").hash();
	
	FXuint tipohash = tipo.hash();

	//######################### Instancia a imagem
	FXIcon *ico;
	if      (tipohash == bmphash) ico = new FXBMPIcon(app);
	else if (tipohash == pcxhash) ico = new FXPCXIcon(app);
	else if (tipohash == tgahash) ico = new FXTGAIcon(app);
	else if (tipohash == gifhash) ico = new FXGIFIcon(app);
	else	return 0;

	//######################### Carrega a imagem
	FXFileStream File;
	File.open(strfile, FX::FXStreamLoad);

	ico->loadPixels(File);

	File.close();
	
	return ico;
	/*FXImage *imagem = Imagem(app, strfile);
	
	if (imagem == 0) {
		return 0;
	} else {	
		FXIcon *ico = ????????//new FXIcon(app);
		
		return ico;
	}*/
}
