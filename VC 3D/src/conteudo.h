
#ifndef CONTEUDO_H
#define CONTEUDO_H

//-------- Afinamento
class cAfin
{
public:
	enum{
		AFIN_MASK_DEFAULT	= 0,
	
		AFIN_MASK_TIPO	= (char)0x30, //0011 0000
		AFIN_INTEIRO	= (char)0x00,
		AFIN_ATRAVES	= (char)0x10,
		AFIN_METADE		= (char)0x20,

		AFIN_MASK_ROT	= (char)0x07, //0000 0111
		AFIN_ROT_0		= (char)0x00,
		AFIN_ROT_1		= (char)0x01,
		AFIN_ROT_2		= (char)0x02,
		AFIN_ROT_3		= (char)0x03,
		AFIN_ROT_4		= (char)0x04,
		AFIN_ROT_5		= (char)0x05,
	};
	
	char c;
	cAfin()			{}
	cAfin(char _c)	{c = _c;}

	void operator = (char _c){c = _c;}
	
	static char CriaPelosVizinhos(int contvizinhos, bool vizinho[6]);
	
	static bool EhAfinInteira(char);	bool EhAfinInteira(){return EhAfinInteira(c);}
	static bool EhAfinAtraves(char);	bool EhAfinAtraves(){return EhAfinAtraves(c);}
	static bool EhAfinMetade(char);		bool EhAfinMetade(){return EhAfinMetade(c);}
	static float AfinRotacao(char);		float AfinRotacao(){return AfinRotacao(c);}
};

//-------- Conte�do
class cCont
{
	friend class cMapa;
	friend class cJogo;
protected:
	enum{
		CONT_FORA_DO_MAPA		= (char)0xff,
		CONT_FOG_NUNCA_VIU		= (char)0xff,

		CONT_MASK_TIPO_PAREDE	= (char)0x0f, //0000 1111
		CONT_PAREDE_NULA		= (char)0x00,
		CONT_PAREDE_MADEIRA		= (char)0x01,
		CONT_PAREDE_TIJOLO		= (char)0x02,
		CONT_PAREDE_PEDRA		= (char)0x03,
		CONT_PAREDE_VIDRO		= (char)0x04,
		CONT_PAREDE_GRADE		= (char)0x05,
		CONT_PAREDE_FUMACA		= (char)0x06,
		CONT_PAREDE_FOGO		= (char)0x07,
		CONT_PAREDE_FOGO_PERM	= (char)0x08,
		CONT_PAREDE_ESCURO		= (char)0x09,
		CONT_PAREDE_LATA		= (char)0x0a,
		CONT_PAREDE_TELETRANS	= (char)0x0b, //0xc - 0xf

		CONT_FLAG_PORTA			= (char)0x10, //0001 0000
		CONT_FLAG_PORTA_ABERTA	= (char)0x20, //0010 0000

		CONT_MASK_TIPO_PORTA	= (char)0xc0, //1100 0000
		CONT_PORTA_NORMAL		= (char)(0x00 << 6),
		CONT_PORTA_SECRETA		= (char)(0x01 << 6),
		CONT_PORTA_TRANCADA		= (char)(0x02 << 6),
		//CONT_PORTA_????		(char)(0x03 << 6)
		//id�ias para a porta tipo 4: porta que s� abre 1 vez; precisa de outra chave; sei l�;

		CONT_MASK_ALTURA		= (char)0x60, //0110 0000
		CONT_ALTURA_0			= (char)(0x00 << 6),
		CONT_ALTURA_1			= (char)(0x01 << 6),
		CONT_ALTURA_2			= (char)(0x02 << 6),
		CONT_ALTURA_3			= (char)(0x03 << 6),
	};
	
public:
	char c;
	cCont()			{}
	cCont(char _c)	{c = _c;}
	
	void operator = (char _c)	{c = _c;}
	bool operator == (char _c)	{return (c == _c);}
	bool operator != (cCont C)	{return (c != C.c);}

	//convers�o
	bool ConverteConteudo(cCont &novocont, char &chao);
	static int ConteudoToTexID(char);		int ConteudoToTexID(){return ConteudoToTexID(c);}
	static char TipoParedeToTipoChao(char);	char TipoParedeToTipoChao(){return TipoParedeToTipoChao(c);}

	//conte�do
	static bool EhForaDoMapa(char);		bool EhForaDoMapa(){return EhForaDoMapa(c);}
	static bool EhFogNuncaViu(char);	bool EhFogNuncaViu(){return EhFogNuncaViu(c);}
	
	static bool EhParede(char);			bool EhParede(){return EhParede(c);}
	static bool EhParedeSolida(char);	bool EhParedeSolida(){return EhParedeSolida(c);}//pode atrapalhar o movimento
	static bool EhParedeDura(char);		bool EhParedeDura(){return EhParedeDura(c);}//um cara de espada nao consegue quebrar
	static bool EhEsfumacado(char);		bool EhEsfumacado(){return EhEsfumacado(c);}
	static bool EhTransparente(char);	bool EhTransparente(){return EhTransparente(c);}

	static bool EhParedeMadeira(char);	bool EhParedeMadeira(){return EhParedeMadeira(c);}
	static bool EhParedeTijolo(char);	bool EhParedeTijolo(){return EhParedeTijolo(c);}
	static bool EhParedePedra(char);	bool EhParedePedra(){return EhParedePedra(c);}
	static bool EhParedeVidro(char);	bool EhParedeVidro(){return EhParedeVidro(c);}
	static bool EhGrade(char);			bool EhGrade(){return EhGrade(c);}
	static bool EhFumaca(char);			bool EhFumaca(){return EhFumaca(c);}
	static bool EhFogo(char);			bool EhFogo(){return EhFogo(c);}
	static bool EhFogoPermanente(char);	bool EhFogoPermanente(){return EhFogoPermanente(c);}
	static bool EhEscuro(char);			bool EhEscuro(){return EhEscuro(c);}
	static bool EhLata(char);			bool EhLata(){return EhLata(c);}
	static bool EhTeletransporte(char);	bool EhTeletransporte(){return EhTeletransporte(c);}

	static bool EhPorta(char);			bool EhPorta(){return EhPorta(c);}
	static bool EhPortaAberta(char);	bool EhPortaAberta(){return EhPortaAberta(c);}
	static bool EhPortaNormal(char);	bool EhPortaNormal(){return EhPortaNormal(c);}
	static bool EhPortaTrancada(char);	bool EhPortaTrancada(){return EhPortaTrancada(c);}
	static bool EhPortaSecreta(char);	bool EhPortaSecreta(){return EhPortaSecreta(c);}

	//altera��o de conte�do
	void SetParedeNula(char &);			void SetParedeNula(){SetParedeNula(c);}
	void SetFumaca(char &);				void SetFumaca(){SetFumaca(c);}
	void SetFogo(char &);				void SetFogo(){SetFogo(c);}
	void SetFogoPermanente(char &);		void SetFogoPermanente(){SetFogoPermanente(c);}

	void AbrePorta(char &);				void AbrePorta(){AbrePorta(c);}
	void FechaPorta(char &);			void FechaPorta(){FechaPorta(c);}
};

//-------- Ch�o
class cChao
{
	friend class cMapa;
	friend class cCont;
protected:
	enum{
		CHAO_FORA_DO_MAPA		= (char)0xff,
		CHAO_FOG_NUNCA_VIU		= (char)0xff,

		CHAO_MASK_TIPO			= (char)0x07, //0000 0111
		CHAO_BURACO				= (char)0x00,
		CHAO_ELEVADOR			= (char)0x01,
		CHAO_MADEIRA			= (char)0x02,
		CHAO_TIJOLO				= (char)0x03,
		CHAO_PEDRA				= (char)0x04,
		CHAO_VIDRO				= (char)0x05,
		CHAO_GRADE				= (char)0x06,
		CHAO_BURACO_INVISIVEL	= (char)0x07,
		CHAO_DEFAULT			= CHAO_TIJOLO,
	};
public:
	char c;
	cChao()			{}
	cChao(char _c)	{c = _c;}
	
	void operator = (char _c)	{c = _c;}
	bool operator == (char _c)	{return (c == _c);}
	bool operator != (cChao C)	{return (c != C.c);}
	
	//convers�o
	static int ChaoCimaToTexID(char);		int ChaoCimaToTexID(){return ChaoCimaToTexID(c);}
	static int ChaoLadoToTexID(char);		int ChaoLadoToTexID(){return ChaoLadoToTexID(c);}

	//ch�o
	static bool EhForaDoMapa(char);			bool EhForaDoMapa(){return EhForaDoMapa(c);}
	static bool EhFogNuncaViu(char);		bool EhFogNuncaViu(){return EhFogNuncaViu(c);}

	static bool EhChaoDuro(char);			bool EhChaoDuro(){return EhChaoDuro(c);}
	static bool EhChaoTransparente(char);	bool EhChaoTransparente(){return EhChaoTransparente(c);}
	static bool EhChaoBuracoInvOuNao(char);	bool EhChaoBuracoInvOuNao(){return EhChaoBuracoInvOuNao(c);}

	static bool EhChaoBuraco(char);			bool EhChaoBuraco(){return EhChaoBuraco(c);}
	static bool EhChaoElevador(char);		bool EhChaoElevador(){return EhChaoElevador(c);}
	static bool EhChaoMadeira(char);		bool EhChaoMadeira(){return EhChaoMadeira(c);}
	static bool EhChaoTijolo(char);			bool EhChaoTijolo(){return EhChaoTijolo(c);}
	static bool EhChaoPedra(char);			bool EhChaoPedra(){return EhChaoPedra(c);}
	static bool EhChaoVidro(char);			bool EhChaoVidro(){return EhChaoVidro(c);}
	static bool EhChaoGrade(char);			bool EhChaoGrade(){return EhChaoGrade(c);}
	static bool EhChaoBuracoInv(char);		bool EhChaoBuracoInv(){return EhChaoBuracoInv(c);}

	//altera��o de ch�o
	static void SetChaoBuraco(char &);		void SetChaoBuraco(){SetChaoBuraco(c);}
	static void SetChaoBuracoInv(char &);	void SetChaoBuracoInv(){SetChaoBuracoInv(c);}
	static void SetChaoPedra(char &);		void SetChaoPedra(){SetChaoPedra(c);}
};


/*
//testes
#define EhParede(C)			((C == 1) || (C == 5) || (C == 9) || (C == 10) || (C == 34) || (C == 44) || (C == 47) || (C == 48))
#define EhParedeDura(C)		((C == 5) || (C == 9) || (C == 10) || (C == 34) || (C == 47) || (C == 48))
#define EhEsfumacado(C)		((C == 22) || (C == 31) || (C == 40))
#define EhTransparente(C)	((C == 34) || (C == 44))
#define EhChaoElevador(C)	((C == 20) || (C == 52) || (C == 53))

#define EhParedeMadeira(C)	(C == 1)
#define EhParedeTijolo(C)	((C == 5) || (C == 9) || (C == 10))
#define EhParedePedra(C)	((C == 47) || (C == 48))
#define EhGrade(C)			(C == 44)
#define EhLata(C)			(C == 32)
#define EhEscuro(C)			(C == 36)
#define EhPortaTrancada(C)	(C == 48)
#define EhPortaSecreta(C)	((C == 9) || (C == 10))

#define EhChaoBuraco(C)		(C == 26)
#define EhChaoMadeira(C)	(C == 1)

//n�o-teste
#define SetFumaca(C)		(C = 44)
#define SetBuracoInvis(C)	(C = 24)

#define AbrePorta(C)		(C = 50)
*/

#endif