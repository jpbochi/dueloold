
#ifndef MODELOSMD3_H
#define MODELOSMD3_H

#include "modqualq.h"

#include "fx.h"

#include "md3.h"

//ModeloMD3:
//Uma inst�ncia para cada modelo diferente (marine, zerg, dragao, etc.).

//ModeloMD3Anim:
//Uma inst�ncia para cada jogador (v�rios marines e um predador).
//Possui textura e anima��o separadas pra cada inst�ncia.

class ModeloMD3Anim : public ModQualq
{
public:
	CModelMD3 model;

	//int numframes;
	//FXuint tex; //cada frame tem um ponteiro para uma textura. todos apontam para esta vari�vel.
	//FXString nome_modelo;

	virtual bool Carrega(FXApp *, FXString, FXString);

	//fun��o que captura uma lista de poss�veis texturas(arquivos ".skin") para um modelo
	static FXList *ListaTexturas(FXComposite *, FXString nomemodelo, FXList *lista = 0);

public:
	enum{
		ANIM_NENHUMA = -1,
		BOTH_DEATH1 = 0,		// The first twirling death animation
		BOTH_DEAD1,				// The end of the first twirling death animation
		BOTH_DEATH2,			// The second twirling death animation
		BOTH_DEAD2,				// The end of the second twirling death animation
		BOTH_DEATH3,			// The back flip death animation
		BOTH_DEAD3,				// The end of the back flip death animation

		// The next block is the animations that the upper body performs

		TORSO_GESTURE,			// The torso's gesturing animation
		
		TORSO_ATTACK,			// The torso's attack1 animation
		TORSO_ATTACK2,			// The torso's attack2 animation

		TORSO_DROP,				// The torso's weapon drop animation
		TORSO_RAISE,			// The torso's weapon pickup animation

		TORSO_STAND,			// The torso's idle stand animation
		TORSO_STAND2,			// The torso's idle stand2 animation

		// The final block is the animations that the legs perform

		LEGS_WALKCR,			// The legs's crouching walk animation
		LEGS_WALK,				// The legs's walk animation
		LEGS_RUN,				// The legs's run animation
		LEGS_BACK,				// The legs's running backwards animation
		LEGS_SWIM,				// The legs's swimming animation
		
		LEGS_JUMP,				// The legs's jumping animation
		LEGS_LAND,				// The legs's landing animation

		LEGS_JUMPB,				// The legs's jumping back animation
		LEGS_LANDB,				// The legs's landing back animation

		LEGS_IDLE,				// The legs's idle stand animation
		LEGS_IDLECR,			// The legs's idle crouching animation

		LEGS_TURN,				// The legs's turn animation

		ANIM_COUNT
	};
	bool animando;
	int anim_atual;
	float framepos;
	double transl[3];
	int framepos_ant, framepos_prox;
	FXuint tex;

	ModeloMD3Anim();
	
	void AnimInicia(int anim_tipo);
	void SetAnimTransl(double x, double y, double z);
	void DisplayGL();
	FXuint AnimAvanca(double frames);

protected:
	int AnimProxFrame(int frame);
	double AnimPos(); //posi��o do frame atual na anima��o [0-1]
};

#endif
