
#ifndef MODELOSMD2_H
#define MODELOSMD2_H

#include "modqualq.h"

#include "fx.h"

/*
#include "..\nehe\texture.h"
#include "..\nehe\md2.h"
#include "..\nehe\milkshape.h"
#include "..\nehe\model.h"

using namespace NeHe;
*/
#include "md2.h"

//ModeloMD2:
//Uma inst�ncia para cada modelo diferente (marine, zerg, dragao, etc.).

//ModeloMD2Anim:
//Uma inst�ncia para cada jogador (v�rios marines e um predador).
//Possui textura e anima��o separadas pra cada inst�ncia.

class ModeloMD2 : public ModQualq
{
public:
	//Model model;
	CModelMD2 model;

	int numframes;
	FXuint tex; //cada frame tem um ponteiro para uma textura. todos apontam para esta vari�vel.
	//FXString nome_modelo;

	~ModeloMD2();
	ModeloMD2();
	virtual bool Carrega(FXApp *, FXString, FXString);

	//fun��o que captura uma lista de poss�veis texturas(imagem ".pcx" de tamanho 256x256 pixels) para um modelo
	static FXList *ListaTexturas(FXComposite *, FXString nomemodelo, FXList *lista = 0);

	void SetTextura(FXuint texid);
};

class ModeloMD2Anim
{
protected:
	ModeloMD2 *modeloMD2;

public:
	enum{
		ANIM_NENHUMA = -1,
		ANIM_STAND,
		ANIM_RUN,
		ANIM_ATTACK,
		ANIM_PAIN_1,
		ANIM_PAIN_2,
		ANIM_PAIN_3,
		ANIM_JUMP,
		ANIM_FLIPOFF,
		ANIM_SALUTE,
		ANIM_TAUNT,
		ANIM_WAVE,
		ANIM_POINT,
		ANIM_CROUCH_STAND,
		ANIM_CROUCH_WALK,
		ANIM_CROUCH_ATTACK,
		ANIM_CROUCH_PAIN,
		ANIM_CROUCH_DEATH,
		ANIM_DEATH_1,
		ANIM_DEATH_2,
		ANIM_DEATH_3,
		ANIM_COUNT
	};
	bool animando;
	int anim_atual;
	float framepos;
	double transl[3];
	int framepos_ant, framepos_prox;
	FXuint tex;

	ModeloMD2Anim();
	bool Carrega(ModeloMD2 *modelo, FXApp *, FXuint &texid, FXString nome_textura = "");
	void TrocaTextura(FXuint texid);
	
	void AnimInicia(int anim_tipo);
	void SetAnimTransl(double x, double y, double z);
	void DisplayGL();
	FXuint AnimAvanca(double frames);

protected:
	int AnimProxFrame(int frame);
	double AnimPos(); //posi��o do frame atual na anima��o [0-1]
};

#endif
