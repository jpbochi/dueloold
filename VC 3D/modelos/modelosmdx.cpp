
#include "modelosmdx.h"
#include "modelosmd3.h"
#include "modelosmd2.h"

#include "..\src\geral.h"

cModeloAnim::cModeloAnim()
{
	pMD2 = 0;
	pMD3 = 0;
	
	tipo = cModeloAnim::MDX;

	animando = false;
	anim_atual = ModeloMD3Anim::ANIM_NENHUMA;
	framepos = 0;
	tex = 0;
	transl[0] = 0;
	transl[1] = 0;
	transl[2] = 0;
}

bool cModeloAnim::Carrega(FXApp *app, ModQualq *modMDX, FXString _nome_modelo, FXString nome_textura)
{
	nome_modelo = _nome_modelo;
	
	//tipo � MD3 caso o modelopassado for nulo
	int tipoMDX = (modMDX == 0) ? ModQualq::TIPO_MD3 : modMDX->tipo;

	if (tipoMDX == ModQualq::TIPO_MD2) {
		tipo = cModeloAnim::MD2;
		
		if (pMD2 == 0) pMD2 = new ModeloMD2Anim;

		return pMD2->Carrega((ModeloMD2 *)modMDX, app, tex, nome_textura);
	} else if (tipoMDX == ModQualq::TIPO_MD3) {
		tipo = cModeloAnim::MD3;
		
		//tosquera do MD3 loader
		extern FXApp *md3_pApp;
		md3_pApp = app;

		if (pMD3 == 0)
			if (modMDX == 0)
				pMD3 = new ModeloMD3Anim;
			else
				pMD3 = (ModeloMD3Anim *)modMDX;

		return pMD3->Carrega(app, nome_modelo, nome_textura);
	} else {
		fxwarning("Erro inesperado em cModeloAnim::Carrega();");
		return 0;
	}
}
/*bool cModeloAnim::Carrega(FXApp *app, ModeloMD2 *modMD2, FXString _nome_modelo, FXString nome_textura)
{
	nome_modelo = _nome_modelo;
	
	tipo = cModeloAnim::MD2;
	
	if (pMD2 == 0) pMD2 = new ModeloMD2Anim;

	return pMD2->Carrega(modMD2, app, tex, nome_textura);
}

bool cModeloAnim::Carrega(FXApp *app, FXString _nome_modelo, FXString nome_textura)
{
	nome_modelo = _nome_modelo;
	
	tipo = cModeloAnim::MD3;
	
	//tosquera do MD3 loader
	extern FXApp *md3_pApp;
	md3_pApp = app;

	if (pMD3 == 0) pMD3 = new ModeloMD3Anim;

	return pMD3->Carrega(app, nome_modelo, nome_textura);
}*/

void cModeloAnim::AnimInicia(int anim_tipo)
{
	switch (tipo) {
		case cModeloAnim::MD2:
			switch (anim_tipo) {
				case ANIM_DEATH:
					switch (rndi(0, 2)) {
						case 0: pMD2->AnimInicia(ModeloMD2Anim::ANIM_DEATH_1); break;
						case 1: pMD2->AnimInicia(ModeloMD2Anim::ANIM_DEATH_2); break;
						case 2: pMD2->AnimInicia(ModeloMD2Anim::ANIM_DEATH_3); break;
					}
				break;
				case ANIM_STAND:	pMD2->AnimInicia(ModeloMD2Anim::ANIM_STAND);	break;
				case ANIM_STANDCR:	pMD2->AnimInicia(ModeloMD2Anim::ANIM_CROUCH_STAND);	break;
				case ANIM_WALK:		pMD2->AnimInicia(ModeloMD2Anim::ANIM_RUN);	break;
				case ANIM_WALKCR:	pMD2->AnimInicia(ModeloMD2Anim::ANIM_CROUCH_WALK);	break;
				case ANIM_BACK:		pMD2->AnimInicia(ModeloMD2Anim::ANIM_RUN);	break;
				case ANIM_RUN:
					if (pMD2->anim_atual == ModeloMD2Anim::ANIM_CROUCH_STAND)
						pMD2->AnimInicia(ModeloMD2Anim::ANIM_CROUCH_WALK);
					else
						pMD2->AnimInicia(ModeloMD2Anim::ANIM_RUN);
				break;
				case ANIM_ATTACK1:
				case ANIM_ATTACK2:
					if (pMD2->anim_atual == ModeloMD2Anim::ANIM_CROUCH_STAND)
						pMD2->AnimInicia(ModeloMD2Anim::ANIM_CROUCH_ATTACK);
					else
						pMD2->AnimInicia(ModeloMD2Anim::ANIM_ATTACK);
				break;
				case ANIM_JUMP:		pMD2->AnimInicia(ModeloMD2Anim::ANIM_JUMP);	break;
				case ANIM_JUMPBK:	pMD2->AnimInicia(ModeloMD2Anim::ANIM_JUMP);	break;
				case ANIM_GEST:
					switch (rndi(0, 4)) {
						case 0: pMD2->AnimInicia(ModeloMD2Anim::ANIM_FLIPOFF); break;
						case 1: pMD2->AnimInicia(ModeloMD2Anim::ANIM_POINT); break;
						case 2: pMD2->AnimInicia(ModeloMD2Anim::ANIM_SALUTE); break;
						case 3: pMD2->AnimInicia(ModeloMD2Anim::ANIM_TAUNT); break;
						case 4: pMD2->AnimInicia(ModeloMD2Anim::ANIM_WAVE); break;
					}
				break;

				case ANIM_SWIN:		pMD2->AnimInicia(ModeloMD2Anim::ANIM_NENHUMA);	break;
				case ANIM_TURN:		pMD2->AnimInicia(ModeloMD2Anim::ANIM_NENHUMA);	break;
				case ANIM_DROP:		pMD2->AnimInicia(ModeloMD2Anim::ANIM_NENHUMA);	break;
				case ANIM_RAISE:	pMD2->AnimInicia(ModeloMD2Anim::ANIM_NENHUMA);	break;
			}
		break;
		case cModeloAnim::MD3:
			//torso
			switch (anim_tipo) {
				case ANIM_DEATH:
					switch (rndi(0, 2)) {
						case 0: pMD3->AnimInicia(BOTH_DEATH1); break;
						case 1: pMD3->AnimInicia(BOTH_DEATH2); break;
						case 2: pMD3->AnimInicia(BOTH_DEATH3); break;
					}
				break;
				case ANIM_STAND:
				case ANIM_STANDCR:
				case ANIM_WALK:
				case ANIM_WALKCR:
				case ANIM_BACK:
				case ANIM_RUN:
				case ANIM_SWIN:
				case ANIM_TURN:
				case ANIM_JUMP:
				case ANIM_JUMPBK:
					switch (rndi(0, 1)) {
						case 0: pMD3->AnimInicia(TORSO_STAND); break;
						case 1: pMD3->AnimInicia(TORSO_STAND2); break;
					}
					//legs
					switch (anim_tipo) {
						case ANIM_STAND:	pMD3->AnimInicia(LEGS_IDLE);break;
						case ANIM_STANDCR:	pMD3->AnimInicia(LEGS_IDLECR);break;
						case ANIM_WALK:		pMD3->AnimInicia(LEGS_WALK);break;
						case ANIM_WALKCR:	pMD3->AnimInicia(LEGS_WALKCR);break;
						case ANIM_BACK:		pMD3->AnimInicia(LEGS_BACK);break;
						case ANIM_RUN:		pMD3->AnimInicia(LEGS_RUN);break;
						case ANIM_SWIN:		pMD3->AnimInicia(LEGS_SWIM);break;
						case ANIM_TURN:		pMD3->AnimInicia(LEGS_TURN);break;
						case ANIM_JUMP:		pMD3->AnimInicia(LEGS_JUMP);break;
						case ANIM_JUMPBK:	pMD3->AnimInicia(LEGS_JUMPB);break;
					}
				break;
				case ANIM_ATTACK1:
					pMD3->AnimInicia(TORSO_ATTACK);
				break;
				case ANIM_ATTACK2:
					pMD3->AnimInicia(TORSO_ATTACK2);
				break;
				case ANIM_GEST:
					pMD3->AnimInicia(TORSO_GESTURE);
				break;
				case ANIM_DROP:
					pMD3->AnimInicia(TORSO_DROP);
				break;
				case ANIM_RAISE:
					pMD3->AnimInicia(TORSO_RAISE);
				break;
			}
		break;
	}
}

void cModeloAnim::SetAnimTransl(double x, double y, double z)
{
	switch (tipo) {
		case cModeloAnim::MD2:
			pMD2->SetAnimTransl(x, y, z);
		break;
		case cModeloAnim::MD3:
			pMD3->SetAnimTransl(x, y, z);
		break;
	}
}

void cModeloAnim::DisplayGL()
{
	switch (tipo) {
		case cModeloAnim::MD2:
			pMD2->DisplayGL();
		break;
		case cModeloAnim::MD3:
			pMD3->DisplayGL();
		break;
	}
}

FXuint cModeloAnim::AnimAvanca(double frames)
{
	switch (tipo) {
		case cModeloAnim::MD2:
			return pMD2->AnimAvanca(frames);
		break;
		case cModeloAnim::MD3:
			return pMD3->AnimAvanca(frames);
		break;
	}
	return 0;//erro!
}