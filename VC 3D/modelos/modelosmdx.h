
#ifndef MODELOSMDX_H
#define MODELOSMDX_H

#include "fx.h"

class cModeloAnim
{
private:
	class ModeloMD2Anim *pMD2;
	class ModeloMD3Anim *pMD3;

	int tipo;
	
	bool animando;
	int anim_atual;
	float framepos;
	double transl[3];
	int framepos_ant, framepos_prox;
	FXuint tex;
public:
	enum{
		ANIM_NENHUMA = -1,
		
		ANIM_DEATH,
		
		ANIM_STAND,
		ANIM_STANDCR,
		
		ANIM_WALK,
		ANIM_WALKCR,
		ANIM_BACK,
		ANIM_RUN,
		ANIM_SWIN,
		
		ANIM_ATTACK1,
		ANIM_ATTACK2,
		
		ANIM_JUMP,
		ANIM_JUMPBK,
		ANIM_TURN,
		ANIM_GEST,
		
		ANIM_DROP,
		ANIM_RAISE,

		ANIM_COUNT,
		
		MDX,
		MD2,
		MD3
	};
	
	FXString nome_modelo;

	bool Carrega(FXApp *, class ModQualq *, FXString, FXString);//MDX
	//bool Carrega(FXApp *, class ModeloMD2 *, FXString, FXString);//MD2
	//bool Carrega(FXApp *, FXString, FXString);//MD3

	cModeloAnim();
	
	void AnimInicia(int anim_tipo);
	void SetAnimTransl(double x, double y, double z);
	void DisplayGL();
	FXuint AnimAvanca(double frames);
};

#endif
