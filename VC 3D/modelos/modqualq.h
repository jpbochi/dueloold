
#ifndef MODQUALQ_H
#define MODQUALQ_H

#include "fx.h"

class ModQualq
{
	public:
	enum {
		TIPO_MD2,
		TIPO_MD3
	};
	int tipo;
	
	FXString nome_modelo;
	virtual bool Carrega(FXApp *, FXString, FXString = ""){return false;}
	
	ModQualq(int _tipo) {tipo = _tipo;}
};

#endif
