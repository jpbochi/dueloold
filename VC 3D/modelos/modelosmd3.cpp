

#include "..\src\winmain.h"

#include "..\src\declarac.h"
extern FXuint textures[MAX_TEXTURAS];

#include "modelosmd3.h"

#include "..\src\arquivos.h"

#include "..\src\glcontext.h"

#include "..\src\texturas.h"

FXList *ModeloMD3Anim::ListaTexturas(FXComposite *comp, FXString nomemodelo, FXList *lista)
{
	FXApp *app = comp->getApp();

	FXString str, dir;
		
	//acha o diret�rio certo
	FXDirList *dirlist = new FXDirList(comp);//		dirlist->hide();
	dirlist->setCurrentFile(str.format("Data\\md3\\%s",nomemodelo));
	dir = dirlist->getDirectory();
	dirlist->destroy();
	delete dirlist;

	//testa se o diret�rio existe
	nomemodelo = nomemodelo.lower();
	FXString fim = dir.right(nomemodelo.length()).lower();
	if (fim != nomemodelo) {
		return lista;
	}
	
	//cria lista
	FXList *ret;
	if (lista == 0) {
		ret = new FXList(comp);
	} else {
		ret = lista;
	}
	ret->hide();
	
	//entra no diret�rio
	FXFileList *filelist = new FXFileList(comp);//	filelist->hide();
	filelist->setDirectory(dir);
	
	//percorre itens no diret�rio
	FXString arquivo, icone;
	static const FXuint headhash = FXString("head_").hash();
	static const FXuint skinhash = FXString(".skin").hash();
	FXFileStream File;
	
	int num_itens =filelist->getNumItems();
	for (int i=0; i<num_itens; i++) {
		//testa se o tipo do arquivo � pcx E se n�o � a textura de uma arma
		arquivo = filelist->getItemFilename(i);
		if (arquivo.right(5).lower().hash() == skinhash
		&&  arquivo.left(5).lower().hash() == headhash) { //come�a com "head_" e termina com ".skin"
			arquivo = arquivo.left(arquivo.length() - 5).mid(5, 4096);

			//abre �cone
			icone = dir + "\\icon_" + arquivo + ".tga";
			FXIcon *ico = 0;
			if (ExisteArquivo(app, icone, false)) {
				//abre o icone (se der erro, retorna null, mas n�o tem problema)
				ico = Icone(app, icone);
			}
			
			str = "MD3\\" + nomemodelo + "\\" + arquivo;
			ret->appendItem(str, ico);
		}
	}
	
	filelist->destroy();
	delete filelist;

	return ret;
}

bool ModeloMD3Anim::Carrega(FXApp *app, FXString _nome_modelo, FXString nome_textura)
{
	nome_modelo = _nome_modelo;

	FXString arq_modelo;
	arq_modelo.format("Data\\md3\\%s", nome_modelo);
	
	//carrega o modelo
	if (!model.LoadModel((char*)arq_modelo.text(), (char*)nome_textura.text())) return false;

	return true;
}

ModeloMD3Anim::ModeloMD3Anim():ModQualq(ModQualq::TIPO_MD3)
{
	animando = false;
	anim_atual = ANIM_NENHUMA;
	framepos = 0;
	tex = 0;
	transl[0] = 0;
	transl[1] = 0;
	transl[2] = 0;
}

void ModeloMD3Anim::AnimInicia(int anim_tipo)
{
	animando = true;
	switch (anim_tipo) {
		case ANIM_NENHUMA:
			animando=false;
			model.SetTorsoAnimation("TORSO_STAND");
			model.SetLegsAnimation("LEGS_IDLE");
		break;
		
		case BOTH_DEATH1:	model.SetTorsoAnimation("BOTH_DEATH1");	model.SetLegsAnimation("BOTH_DEATH1");break;
		case BOTH_DEAD1:	model.SetTorsoAnimation("BOTH_DEAD1");	model.SetLegsAnimation("BOTH_DEAD1");break;
		
		case BOTH_DEATH2:	model.SetTorsoAnimation("BOTH_DEATH2");	model.SetLegsAnimation("BOTH_DEATH2");break;
		case BOTH_DEAD2:	model.SetTorsoAnimation("BOTH_DEAD2");	model.SetLegsAnimation("BOTH_DEAD2");break;
		
		case BOTH_DEATH3:	model.SetTorsoAnimation("BOTH_DEATH3");	model.SetLegsAnimation("BOTH_DEATH3");break;
		case BOTH_DEAD3:	model.SetTorsoAnimation("BOTH_DEAD3");	model.SetLegsAnimation("BOTH_DEAD3");break;
		
		case TORSO_GESTURE:	model.SetTorsoAnimation("TORSO_GESTURE");break;
		case TORSO_ATTACK:	model.SetTorsoAnimation("TORSO_ATTACK");break;
		case TORSO_ATTACK2:	model.SetTorsoAnimation("TORSO_ATTACK2");break;
		case TORSO_DROP:	model.SetTorsoAnimation("TORSO_DROP");break;
		case TORSO_RAISE:	model.SetTorsoAnimation("TORSO_RAISE");break;
		case TORSO_STAND:	model.SetTorsoAnimation("TORSO_STAND");break;
		case TORSO_STAND2:	model.SetTorsoAnimation("TORSO_STAND2");break;
		
		case LEGS_WALKCR:	model.SetLegsAnimation("LEGS_WALKCR");break;
		case LEGS_WALK:		model.SetLegsAnimation("LEGS_WALK");break;
		case LEGS_RUN:		model.SetLegsAnimation("LEGS_RUN");break;
		case LEGS_BACK:		model.SetLegsAnimation("LEGS_BACK");break;
		case LEGS_SWIM:		model.SetLegsAnimation("LEGS_SWIM");break;
		case LEGS_JUMP:		model.SetLegsAnimation("LEGS_JUMP");break;
		case LEGS_LAND:		model.SetLegsAnimation("LEGS_LAND");break;
		case LEGS_JUMPB:	model.SetLegsAnimation("LEGS_JUMPB");break;
		case LEGS_LANDB:	model.SetLegsAnimation("LEGS_LANDB");break;
		case LEGS_IDLE:		model.SetLegsAnimation("LEGS_IDLE");break;
		case LEGS_IDLECR:	model.SetLegsAnimation("LEGS_IDLECR");break;
		case LEGS_TURN:		model.SetLegsAnimation("LEGS_TURN");break;
	}

	/*animando = true;
	anim_atual = anim_tipo;
	if (anim_atual == ANIM_NENHUMA) {
		framepos = 0;
		framepos_ant = POS_ANIM_DEFAULT;
		framepos_prox = POS_ANIM_DEFAULT;
	} else {
		framepos = 0;
		framepos_ant = POS_ANIM[anim_atual][0];
		framepos_prox = AnimProxFrame(framepos_ant);
	}*/
	//zera movimento
	transl[0] = 0;
	transl[1] = 0;
	transl[2] = 0;
}

void ModeloMD3Anim::SetAnimTransl(double x, double y, double z)
{
	transl[0] = x;
	transl[1] = y;
	transl[2] = z;
}

void ModeloMD3Anim::DisplayGL()
{
	glColor3ub(255, 255, 255);

	//empilha
	glPushMatrix();
	glFrontFace(GL_CW);

	//aplica transla��o
	double l = AnimPos();
	glTranslated(l * transl[0], l * transl[1], l * transl[2]);

	//posiciona
	glScalef(0.025f,0.025f,0.025f);
	glTranslated(0, 0, 24.0); //faz o modelo ficar no ch�o
	
	//renderiza
	model.DrawModel();

	//desempilha
	glFrontFace(GL_CCW);
	glPopMatrix();
}

FXuint ModeloMD3Anim::AnimAvanca(double frames)
{
	static double TICKS_POR_FRAME = 76.54321;
	return model.Anima(frames * TICKS_POR_FRAME); 
	/*//s� retorna 1 quando a anima��o termina ou faz um loop
	FXuint ret = false;
	if (animando) {
		framepos += (float)frames;
		do {
			//percorreu um frame inteiro
			if (framepos > 1) {
				framepos -= 1;
				
				framepos_ant = framepos_prox;
				framepos_prox = AnimProxFrame(framepos_ant);
				
				//retorna falso quando a anima��o faz um loop
				ret |= (framepos_prox == POS_ANIM[anim_atual][0]);
				
				//n�o h� mais frames para mostrar
				if (framepos_ant == framepos_prox) {
					framepos = 0;
					animando = false;
					//retorna falso quando a anima��o p�ra
					ret = 1;
				}
			}
		} while (animando && framepos > 1);
	}
	return ret;*/
}

int ModeloMD3Anim::AnimProxFrame(int frame)
{
	return 0;
	/*
	//terminou a anima��o?
	if (frame < POS_ANIM[anim_atual][1]) {
		//se n�o terminou, avan�a um frame
		return frame + 1;
	} else {
		//a anima��o � um loop?
		if (POS_ANIM[anim_atual][2] == POS_ANIM_LOOP) {
			//volta para o in�cio
			return POS_ANIM[anim_atual][0];
		} else {
			//para a anima��o
			return POS_ANIM[anim_atual][2];
		}
	}*/
}

double ModeloMD3Anim::AnimPos()
{
	return model.AnimPos();
	/*
	if (POS_ANIM[anim_atual][2] == POS_ANIM_LOOP) {
		//anima��es em loop percorrem um frame extra (entre o �ltimo e o primeiro)
		return (framepos_ant + framepos - POS_ANIM[anim_atual][0])
			 / (1 + POS_ANIM[anim_atual][1] - POS_ANIM[anim_atual][0]);
	} else {
		return (framepos_ant + framepos - POS_ANIM[anim_atual][0])
			 / (POS_ANIM[anim_atual][1] - POS_ANIM[anim_atual][0]);
	}*/
}
